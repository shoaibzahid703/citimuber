@extends('admin.layouts.app')
@section('title','Profile')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Update Profile</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <!--begin::Form-->
                        <form method="post" action="{{route('adminProfileUpdate')}}">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <label class="form-label" for="name">Name</label>
                                    <div class="form-control-wrap">
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="slug" placeholder="name" value="{{old('name') ? old('name') : auth()->user()->name}}" >
                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="email">Email</label>
                                    <div class="form-control-wrap">
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="email" value="{{old('email') ? old('email') : auth()->user()->email}}" >
                                        @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6 mt-2">
                                    <label class="form-label" for="password">Password</label>
                                    <div class="form-control-wrap">
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="password"  >
                                        @error('password')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- column -->
            </div>
        </div>
    </div>
@endsection
