@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Dashboard</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="nk-block">
                    <div class="row g-gs">

                        <div class="col-md-4">
                            <a href="#">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Total Users</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$total_users}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-users text-danger" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div><!-- .col -->
                        <div class="col-md-4">
                            <a href="{{route('all_candidates')}}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Total Candidates</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$total_candidates}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-user-alt text-primary" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div><!-- .col -->
                        <div class="col-md-4">
                            <a href=" {{ route('all_employers') }}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Total Employers</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$total_employers}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-user-c text-secondary" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div><!-- .col -->
                        <div class="col-md-4">
                            <a href=" {{ route('all_agencies') }}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Total Agencies</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$total_agencies}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-user-check text-purple" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div><!-- .col -->

                        <div class="col-md-4">
                            <a href="{{ route('all_jobs') }}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Total Jobs</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$total_jobs}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-layers-fill text-success" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="{{ route('all_transactions') }}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Today Employer Subscription Sales</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$today_employer_subscription}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-money text-para" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href=" {{  route('all_transactions') }}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Today Helper  Subscription Sales</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$today_helper_subscription}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-money text-blue" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href=" {{  route('all_transactions') }}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Today Agency Subscription Sales</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$today_agency_subscription}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-money text-azure" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href=" {{  route('all_transactions') }}">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title">Today Sales</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$today_sales}}</div>
                                                    <div class="nk-ecwg6-ck">
                                                       <span class="nk-menu-icon">
                                                            <em class="icon ni ni-money text-pink" style="font-size: 50px;"></em>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- .card-inner -->
                                    </div><!-- .nk-ecwg -->
                                </div><!-- .card -->
                            </a>
                        </div>
                    </div><!-- .row -->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush

