@extends('admin.layouts.app')
@section('title','Create Agency Service')
@push('css')
    <link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />
@endpush
@section('content')


    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Create Agency Service</h3>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-inner">
                        <form method="post" action="{{route('agency_service_store')}}" id="plans_store" class="form-validate" novalidate="novalidate">
                            @csrf
                            <div class="row g-gs">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name"> Name*</label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" required="" placeholder="Plan name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="no_of_weeks"> Duration (Weeks)*</label>
                                        <div class="form-control-wrap">
                                            <input type="number" name="no_of_weeks"
                                                   class="form-control @error('no_of_weeks') is-invalid @enderror" id="no_of_weeks"
                                                   required="" placeholder="0">
                                            @error('no_of_weeks')
                                            <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class="form-label" for="price">Default Price (HKD) *</label>
                                        <div class="form-control-wrap">
                                            <input type="number" name="price" class="form-control price-input" id="price"
                                                   required placeholder="0">
                                        </div>
                                    </div>
                                </div>





                                <div class="row mt-2">
                                    <div class="col-md-12">
                                        <div class="feature-input">
                                            <div class="form-group">
                                                <label class="form-label" for="description">  Description *</label>
                                                <div id="editor-container"></div>
                                                @error('feature_description')
                                                <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                          </span>
                                                @enderror
                                                <input type="hidden" name="description" id="description">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-5">
                                    <div class="col-md-12 ">
                                        <div class="form-group ">
                                            <button type="submit" class="btn btn-md btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>
    <script type="text/javascript">
        var toolbarOptions = [
            ['bold', 'italic', 'underline'],        // toggled buttons
            ['blockquote'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        ];

        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: toolbarOptions,
            },
            placeholder: 'description',
            theme: 'snow'
        });

        $("#plans_store").on("submit", function(){
            var myEditor = document.querySelector('#editor-container')
            var html = myEditor.children[0].innerHTML;
            $("#description").val(html);
        })
    </script>
@endpush
