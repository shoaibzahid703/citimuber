@extends('admin.layouts.app')
@section('title','All Employer Category')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Employer Category</h3>
                        </div><!-- .nk-block-head-content -->
                        <a href="{{route('employer_category_create')}}" class="btn btn-primary">Add</a>
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <div class="card-datatable table-responsive">
                            <table id="employer_categories" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Helper Apply Status</th>
                                    <th>Category</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")

    <script type="text/javascript">
        employer_categories_table =    $("#employer_categories").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('employer_categories')}}',
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'helper_apply_status', name: 'helper_apply_status'},
                {data: 'category', name: 'category'},
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>

@endpush
