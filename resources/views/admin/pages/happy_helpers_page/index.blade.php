@extends('admin.layouts.app')
@section('title','Happy Helpers Page')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Happy Helpers Page</h3>
          </div>
        </div>
      </div>
      @php
      $happyHelpersPage = App\Models\HappyHelpersPage::find(1);
      @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-happy-helpers')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading" required="" value="{{@$happyHelpersPage->first_heading}}" placeholder="Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_heading">Second Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_heading" class="form-control @error('second_heading') is-invalid @enderror" id="second_heading" required="" value="{{@$happyHelpersPage->second_heading}}" placeholder="Second Heading">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_text">First Text</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_text" class="form-control @error('first_text') is-invalid @enderror" id="first_text" required="" value="{{@$happyHelpersPage->first_text}}" placeholder="First Text">
                    @error('first_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_paragraph">First Paragraph</label>
                  <div class="form-control-wrap">

                    <textarea class="form-control @error('first_paragraph') is-invalid @enderror" name="first_paragraph" id="first_paragraph" required="">{{@$happyHelpersPage->first_paragraph}}</textarea>
                    @error('first_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="register_button">Register Button</label>
                  <div class="form-control-wrap">
                    <input type="text" name="register_button" class="form-control @error('register_button') is-invalid @enderror" id="register_button" required="" value="{{@$happyHelpersPage->register_button}}" placeholder="Register Button">
                    @error('register_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="second_paragraph">Second Paragraph</label>
                  <div class="form-control-wrap">

                    <textarea class="form-control @error('second_paragraph') is-invalid @enderror" name="second_paragraph" id="second_paragraph" required="">{{@$happyHelpersPage->second_paragraph}}</textarea>
                    @error('second_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="third_heading">Third Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="third_heading" class="form-control @error('third_heading') is-invalid @enderror" id="third_heading" required="" value="{{@$happyHelpersPage->third_heading}}" placeholder="Third Heading">
                    @error('third_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="third_paragraph">Third Paragraph</label>
                  <div class="form-control-wrap">

                    <textarea class="form-control @error('third_paragraph') is-invalid @enderror" name="third_paragraph" id="third_paragraph" required="">{{@$happyHelpersPage->third_paragraph}}</textarea>
                    @error('third_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="fourth_heading">Fourth Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="fourth_heading" class="form-control @error('fourth_heading') is-invalid @enderror" id="fourth_heading" required="" value="{{@$happyHelpersPage->fourth_heading}}" placeholder="Fourth Heading">
                    @error('fourth_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_text">Second Text</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_text" class="form-control @error('second_text') is-invalid @enderror" id="second_text" required="" value="{{@$happyHelpersPage->second_text}}" placeholder="Second Text">
                    @error('second_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="contact_us_button">Contact Us Button</label>
                  <div class="form-control-wrap">
                    <input type="text" name="contact_us_button" class="form-control @error('contact_us_button') is-invalid @enderror" id="contact_us_button" required="" value="{{@$happyHelpersPage->contact_us_button}}" placeholder="Contact Us Button">
                    @error('contact_us_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="bg_image">Bg Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="bg_image" class="form-control @error('bg_image') is-invalid @enderror" id="bg_image" placeholder="Country code">
                    @error('bg_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happyHelpersPage->bg_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_clients_page')}}/{{@$happyHelpersPage->bg_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="code">First Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="first_image" class="form-control @error('first_image') is-invalid @enderror" id="first_image" placeholder="Country code">
                    @error('first_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happyHelpersPage->first_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_clients_page')}}/{{@$happyHelpersPage->first_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="code">Second Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="sec_image" class="form-control @error('sec_image') is-invalid @enderror" id="sec_image" placeholder="Country code">
                    @error('sec_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happyHelpersPage->sec_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_clients_page')}}/{{@$happyHelpersPage->sec_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="app_store_image">App Store Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="app_store_image" class="form-control @error('app_store_image') is-invalid @enderror" id="third_image" placeholder="Country code">
                    @error('app_store_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happyHelpersPage->app_store_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_clients_page')}}/{{@$happyHelpersPage->app_store_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="google_play_image">App Store Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="google_play_image" class="form-control @error('google_play_image') is-invalid @enderror" id="third_image" placeholder="Country code">
                    @error('google_play_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happyHelpersPage->google_play_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_clients_page')}}/{{@$happyHelpersPage->google_play_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection