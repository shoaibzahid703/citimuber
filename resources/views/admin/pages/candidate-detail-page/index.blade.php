@extends('admin.layouts.app')
@section('title','Bg Image')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Candidate Detail Page</h3>
          </div>
        </div>
      </div>
        
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-candidate-detail-page')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="candidate_detail_page_image">Candidate Detail Page Bg-Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="candidate_detail_page_image" class="form-control @error('candidate_detail_page_image') is-invalid @enderror" id="candidate_detail_page_image">
                    @error('candidate_detail_page_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$detailPage->candidate_detail_page_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/candidate_detail_page_image')}}/{{@$detailPage->candidate_detail_page_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="emp_detail_page_image">Employee Detail Page Bg-Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="emp_detail_page_image" class="form-control @error('emp_detail_page_image') is-invalid @enderror" id="emp_detail_page_image">
                    @error('emp_detail_page_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$detailPage->emp_detail_page_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/candidate_detail_page_image')}}/{{@$detailPage->emp_detail_page_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
