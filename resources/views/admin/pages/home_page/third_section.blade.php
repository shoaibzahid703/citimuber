@extends('admin.layouts.app')
@section('title','Third Section')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Third Section</h3>
          </div>
        </div>
      </div>
        @php
        $homePage = App\Models\HomePageThirdSection::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-third-section')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Text</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_text" class="form-control @error('first_text') is-invalid @enderror" id="first_text"  required="" value="{{@$homePage->first_text}}" placeholder="First Text">
                    @error('first_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="heading">Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="heading" class="form-control @error('heading') is-invalid @enderror" id="heading"  required="" value="{{@$homePage->heading}}" placeholder="Heading">
                    @error('heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="paragraph">Paragraph</label>
                  <div class="form-control-wrap">
   
                    <textarea class="form-control @error('paragraph') is-invalid @enderror" name="paragraph" id="paragraph"  required="">{{@$homePage->paragraph}}</textarea>
                    @error('paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_button">First Button</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_button" class="form-control @error('first_button') is-invalid @enderror" id="first_button"  required="" value="{{@$homePage->first_button}}" placeholder="Second Button Name">
                    @error('first_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_button">Second Button</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_button" class="form-control @error('second_button') is-invalid @enderror" id="second_button"  required="" value="{{@$homePage->second_button}}" placeholder="Second Button Name">
                    @error('second_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="code">Side Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="side_image" class="form-control @error('side_image') is-invalid @enderror" id="side_image" placeholder="Country code">
                    @error('side_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->side_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_third_section')}}/{{@$homePage->side_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
