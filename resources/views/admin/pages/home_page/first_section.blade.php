@extends('admin.layouts.app')
@section('title','First Section')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">First Section</h3>
          </div>
        </div>
      </div>
        @php
        $homePage = App\Models\HomePageFirstSection::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-first-section')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{$homePage->first_heading}}" placeholder="First Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_heading">Second Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_heading" class="form-control @error('second_heading') is-invalid @enderror" id="second_heading"  required="" value="{{$homePage->second_heading}}" placeholder="Second Heading">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_button">First Button Name</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_button" class="form-control @error('first_button') is-invalid @enderror" id="first_button"  required="" value="{{$homePage->first_button}}" placeholder="First Button Name">
                    @error('first_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="sec_button">Second Button Name</label>
                  <div class="form-control-wrap">
                    <input type="text" name="sec_button" class="form-control @error('sec_button') is-invalid @enderror" id="sec_button"  required="" value="{{$homePage->sec_button}}" placeholder="Second Button Name">
                    @error('sec_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="code">Background Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="bg_image" class="form-control @error('bg_image') is-invalid @enderror" id="bg_image" placeholder="Country code">
                    @error('bg_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <img class="rounded-top mt-3" src="{{asset('storage/home_first_section')}}/{{$homePage->bg_image}}" alt="" width="20%" height="100px">

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
