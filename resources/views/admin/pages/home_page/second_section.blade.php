@extends('admin.layouts.app')
@section('title','Second Section')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Second Section</h3>
          </div>
        </div>
      </div>
        @php
        $homePage = App\Models\HomePageSecondSection::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-second-section')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
            
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="code">First Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="first_image" class="form-control @error('first_image') is-invalid @enderror" id="first_image" placeholder="Country code">
                    @error('first_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->first_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_second_section')}}/{{@$homePage->first_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_image">Second Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="second_image" class="form-control @error('second_image') is-invalid @enderror" id="second_image" placeholder="Country code">
                    @error('second_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->second_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_second_section')}}/{{@$homePage->second_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="third_image">Third Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="third_image" class="form-control @error('third_image') is-invalid @enderror" id="third_image" placeholder="Country code">
                    @error('third_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->third_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_second_section')}}/{{@$homePage->third_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="forth_image">Four Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="forth_image" class="form-control @error('forth_image') is-invalid @enderror" id="forth_image" placeholder="Country code">
                    @error('forth_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->forth_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_second_section')}}/{{@$homePage->forth_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="five_image">Five Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="five_image" class="form-control @error('five_image') is-invalid @enderror" id="five_image" placeholder="Country code">
                    @error('five_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->five_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_second_section')}}/{{@$homePage->five_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="six_image">Six Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="six_image" class="form-control @error('six_image') is-invalid @enderror" id="six_image" placeholder="Country code">
                    @error('six_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->six_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_second_section')}}/{{@$homePage->six_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="seven_image">Seven Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="seven_image" class="form-control @error('seven_image') is-invalid @enderror" id="seven_image" placeholder="Country code">
                    @error('seven_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->seven_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_second_section')}}/{{@$homePage->seven_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{@$homePage->first_heading}}" placeholder="First Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_heading">Second Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_heading" class="form-control @error('second_heading') is-invalid @enderror" id="second_heading"  required="" value="{{@$homePage->second_heading}}" placeholder="Second Heading">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
