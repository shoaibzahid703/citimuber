@extends('admin.layouts.app')
@section('title','Five Section')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Five Section</h3>
          </div>
        </div>
      </div>
        @php
        $homePage = App\Models\HomePageFiveSection::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-five-section')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{@$homePage->first_heading}}" placeholder="First Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_paragraph">First Paragraph</label>
                  <div class="form-control-wrap">
   
                    <textarea class="form-control @error('first_paragraph') is-invalid @enderror" name="first_paragraph" id="first_paragraph"  required="">{{@$homePage->first_paragraph}}</textarea>
                    @error('first_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="user_count_heading">User Count Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="user_count_heading" class="form-control @error('user_count_heading') is-invalid @enderror" id="user_count_heading"  required="" value="{{@$homePage->user_count_heading}}" placeholder="User Count Heading">
                    @error('user_count_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="user_text">User Text</label>
                  <div class="form-control-wrap">
                    <input type="text" name="user_text" class="form-control @error('user_text') is-invalid @enderror" id="user_text"  required="" value="{{@$homePage->user_text}}" placeholder="User Text">
                    @error('user_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="percentage_heading">Percentage Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="percentage_heading" class="form-control @error('percentage_heading') is-invalid @enderror" id="percentage_heading"  required="" value="{{@$homePage->percentage_heading}}" placeholder="Percentage Heading">
                    @error('percentage_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="percentage_text">Percentage Text</label>
                  <div class="form-control-wrap">
                    <input type="text" name="percentage_text" class="form-control @error('percentage_text') is-invalid @enderror" id="percentage_text"  required="" value="{{@$homePage->percentage_text}}" placeholder="Percentage Text">
                    @error('percentage_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="satisfaction_heading">Satisfaction Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="satisfaction_heading" class="form-control @error('satisfaction_heading') is-invalid @enderror" id="satisfaction_heading"  required="" value="{{@$homePage->satisfaction_heading}}" placeholder="Satisfaction Heading">
                    @error('satisfaction_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="satisfaction_text">Satisfaction Text</label>
                  <div class="form-control-wrap">
                    <input type="text" name="satisfaction_text" class="form-control @error('satisfaction_text') is-invalid @enderror" id="satisfaction_text"  required="" value="{{@$homePage->satisfaction_text}}" placeholder="Satisfaction Text">
                    @error('satisfaction_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="second_paragraph">Second Paragraph</label>
                  <div class="form-control-wrap">
   
                    <textarea class="form-control @error('second_paragraph') is-invalid @enderror" name="second_paragraph" id="second_paragraph"  required="">{{@$homePage->second_paragraph}}</textarea>
                    @error('second_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="register_button">Register Button</label>
                  <div class="form-control-wrap">
                    <input type="text" name="register_button" class="form-control @error('register_button') is-invalid @enderror" id="register_button"  required="" value="{{@$homePage->register_button}}" placeholder="Register Button">
                    @error('register_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="side_image">Side Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="side_image" class="form-control @error('side_image') is-invalid @enderror" id="side_image" placeholder="Country code">
                    @error('side_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->side_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/home_five_section')}}/{{@$homePage->side_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
