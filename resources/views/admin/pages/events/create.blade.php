@extends('admin.layouts.app')
@section('title','Add Event')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Add Event</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('store-events')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="title">Title</label>
                  <div class="form-control-wrap">
                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title"  required="" placeholder="Holiday Title">
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="country">Country</label>
                    <div class="form-control-wrap">
                        <select name="country_id" id="country" class="form-control  @error('country_id') is-invalid @enderror">
                            @foreach($country as $countries)
                                <option value="{{ $countries->id }}">{{$countries->name}}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                 </div>
            </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="organizer_name">Organizer Name</label>
                  <div class="form-control-wrap">
                    <input type="text" name="organizer_name" class="form-control @error('organizer_name') is-invalid @enderror" id="organizer_name"  placeholder="Organizer Name ">
                    @error('organizer_name')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="event_date">Event Date</label>
                  <div class="form-control-wrap">
                    <input type="date" name="event_date" class="form-control @error('event_date') is-invalid @enderror" id="event_date"  required >
                    @error('event_date')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="start_time">Start Time</label>
                  <div class="form-control-wrap">
                    <input type="time" name="start_time" class="form-control @error('start_time') is-invalid @enderror" id="start_time"  required >
                    @error('start_time')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="end_time">End Time</label>
                  <div class="form-control-wrap">
                    <input type="time" name="end_time" class="form-control @error('end_time') is-invalid @enderror" id="end_time"  required >
                    @error('end_time')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>


              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="audience">Audience </label>
                  <div class="form-control-wrap">
                   <select class="form-control @error('audience') is-invalid @enderror" name="audience" id="audience">
                        <option value="">Select Option</option>
                        <option value="Domestic Helper">Domestic Helper</option>
                        <option value="Driver">Driver</option>
                    </select>
                    @error('audience')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="fees">Fees Detail</label>
                  <div class="form-control-wrap">
                    <input type="text" name="fees" class="form-control @error('fees') is-invalid @enderror" id="fees" placeholder="Fees Detail"   required >
                    @error('fees')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>


              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="phone">Phone</label>
                  <div class="form-control-wrap">
                    <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="Phone"  required >
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="requirement">Requirement</label>
                  <div class="form-control-wrap">
                    <input type="text" name="requirement" class="form-control @error('requirement') is-invalid @enderror" id="requirement" placeholder="Requirement"  required >
                    @error('requirement')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="location">Location</label>
                  <div class="form-control-wrap">
                    <input type="text" name="location" class="form-control @error('location') is-invalid @enderror" id="location" placeholder="Location"  required >
                    @error('location')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="event_image">Event Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="event_image" class="form-control @error('event_image') is-invalid @enderror" id="event_image" required >
                    @error('event_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="event_image">Short Description</label>
                  <div class="form-control-wrap">
                    <textarea class="form-control @error('short_decription') is-invalid @enderror" name="short_decription" required></textarea>
                    @error('short_decription')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="comment">Details</label>
                  <div class="form-control-wrap">
   
                    <textarea id="letter_format" class="form-control form-control-sm"  name="description">{{@$homePage->description}}</textarea>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')

<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'letter_format', {
        filebrowserUploadMethod: 'form'
    });
    
</script>
@endpush