@extends('admin.layouts.app')
@section('title','Contact Us')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Contact Us Page</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-contact-us')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{@$contact_us->first_heading}}" placeholder="First Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_paragraph">First Paragraph</label>
                  <div class="form-control-wrap">

                    <textarea class="form-control @error('first_paragraph') is-invalid @enderror" name="first_paragraph" id="first_paragraph"  required="">{{@$contact_us->first_paragraph}}</textarea>
                    @error('first_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_text">First Text Line</label>
                  <div class="form-control-wrap">

                    <textarea class="form-control @error('first_text') is-invalid @enderror" name="first_text" id="first_text"  required="">{{@$contact_us->first_text}}</textarea>
                    @error('first_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="second_text">Second Text Line</label>
                  <div class="form-control-wrap">

                    <textarea class="form-control @error('second_text') is-invalid @enderror" name="second_text" id="second_text"  required="">{{@$contact_us->second_text}}</textarea>
                    @error('second_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="contact_number">Contact Number</label>
                  <div class="form-control-wrap">
                    <input type="text" name="contact_number" class="form-control @error('contact_number') is-invalid @enderror" id="contact_number"  required="" value="{{@$contact_us->contact_number}}" placeholder="Contact Number">
                    @error('contact_number')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_heading">Second Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_heading" class="form-control @error('second_heading') is-invalid @enderror" id="second_heading"  required="" value="{{@$contact_us->second_heading}}" placeholder="Second Heading">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="third_text">Third Text Line</label>
                  <div class="form-control-wrap">
                    <input type="text" name="third_text" class="form-control @error('third_text') is-invalid @enderror" id="third_text"  required="" value="{{@$contact_us->third_text}}" placeholder="Third Text Line">
                    @error('third_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="contact_us_button">Contact Us Button</label>
                  <div class="form-control-wrap">
                    <input type="text" name="contact_us_button" class="form-control @error('contact_us_button') is-invalid @enderror" id="contact_us_button"  required="" value="{{@$contact_us->contact_us_button}}" placeholder="Contact Us Button">
                    @error('contact_us_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="contact_us_map_src">Contact Us Map Src</label>
                  <div class="form-control-wrap">
                      <textarea name="contact_us_map_src"
                                class="form-control @error('contact_us_map_src') is-invalid @enderror"
                                id="contact_us_map_src"
                                required
                                placeholder="Contact us map src"
                      >{{@$contact_us->contact_us_map_src}}</textarea>
                    @error('contact_us_map_src')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
