@extends('admin.layouts.app')
@section('title','Employer Dashboard')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Employer Dashboard Page</h3>
          </div>
        </div>
      </div>
        @php
        $homePage = App\Models\EmployerDashboardPage::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-employer-dashboard-page')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">

            	<div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="code">First Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="first_image" class="form-control @error('first_image') is-invalid @enderror" id="first_image">
                    @error('first_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->first_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/employer_dashboard_page')}}/{{@$homePage->first_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_image">Second Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="second_image" class="form-control @error('second_image') is-invalid @enderror" id="sec_image" placeholder="Country code">
                    @error('second_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->second_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/employer_dashboard_page')}}/{{@$homePage->second_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="third_image">Third Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="third_image" class="form-control @error('third_image') is-invalid @enderror" id="third_image" placeholder="Country code">
                    @error('third_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->third_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/employer_dashboard_page')}}/{{@$homePage->third_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="fourth_image">Fourth Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="fourth_image" class="form-control @error('fourth_image') is-invalid @enderror" id="fourth_image">
                    @error('fourth_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->fourth_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/employer_dashboard_page')}}/{{@$homePage->fourth_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="five_image">Five Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="five_image" class="form-control @error('five_image') is-invalid @enderror" id="five_image">
                    @error('five_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->five_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/employer_dashboard_page')}}/{{@$homePage->five_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="six_image">Six Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="six_image" class="form-control @error('six_image') is-invalid @enderror" id="six_image">
                    @error('six_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->six_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/employer_dashboard_page')}}/{{@$homePage->six_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="seven_image">Seven Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="seven_image" class="form-control @error('seven_image') is-invalid @enderror" id="seven_image">
                    @error('seven_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$homePage->seven_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/employer_dashboard_page')}}/{{@$homePage->seven_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
