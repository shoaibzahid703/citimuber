@extends('admin.layouts.app')
@section('title','Edit Currency')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Edit Currency</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('currency_update', $currency)}}" class="form-validate" novalidate="novalidate">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="name"> Name</label>
                  <div class="form-control-wrap">
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"  required="" value="{{$currency->name}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label" for="sign">Sign</label>
                        <div class="form-control-wrap">
                            <input type="text" name="sign" class="form-control @error('sign') is-invalid @enderror" id="sign"
                                   required placeholder="Currency sign" value="{{$currency->sign}}">
                            @error('sign')
                            <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                    </div>
                </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
