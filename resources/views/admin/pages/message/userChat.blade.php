
@extends('admin.layouts.app')
@section('title','Messages')
@section('content')
    <style>
        .messages_div {
            height: 400px; /* or any other fixed height */
            overflow-y: auto;
        }
        @media (max-width: 768px) {
            .nk-chat-body {
                width: 100%;
                max-width: 100%;
                opacity: 1;
                pointer-events: auto;
            }
            .messages_div {
                height: 500px !important;
            }
        }
    </style>

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body p-0">
                <div class="nk-chat">
                    <div class="nk-chat-aside d-none d-sm-none d-md-block d-lg-block">
                        <div class="nk-chat-aside-head">
                            <div class="nk-chat-aside-user">
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle " data-bs-toggle="dropdown">
                                        <div class="user-avatar">
                                            {{get_name_first_letters( \Illuminate\Support\Facades\Auth::user()->name)}}
                                        </div>
                                        <div class="title">Users</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="nk-chat-aside-body" data-simplebar="init">
                            <div class="simplebar-wrapper" style="margin: 0px;">
                                <div class="simplebar-height-auto-observer-wrapper">
                                    <div class="simplebar-height-auto-observer"></div>
                                </div>
                                <div class="simplebar-mask">
                                    <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                        <div class="simplebar-content-wrapper" tabindex="0" role="region" aria-label="scrollable content" style="height: 100%; overflow: hidden scroll;">
                                            <div class="simplebar-content" style="padding: 0px;">
                                                <div class="nk-chat-aside-search">
                                                    <div class="form-group">
                                                        <div class="form-control-wrap">
                                                            <div class="form-icon form-icon-left">
                                                                <em class="icon ni ni-search"></em>
                                                            </div>
                                                            <input type="text" class="form-control form-round" id="search_by_name"
                                                                   placeholder="Search by name">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="nk-chat-list" id="userList">
                                                    <ul class="chat-list">
                                                        @foreach($users as $user)
                                                            <li class="chat-item @if($user->id == $receiver->id) is-unread @endif">
                                                                <a class="chat-link chat-open" href="{{ route('adminChatIndex',$user) }}">
                                                                    <div class="chat-media user-avatar bg-purple">
                                                                        <span>{{get_name_first_letters($user->name)}}</span>
                                                                    </div>
                                                                    <div class="chat-info">
                                                                        <div class="chat-from">
                                                                            <div class="name">{{$user->name}}</div>
                                                                        </div>
                                                                        <div class="chat-context">
                                                                            <div class="text">
                                                                                @if($user->role == \App\Models\User::ROLE_EMPLOYER)
                                                                                    <p>
                                                                                        EMPLOYER
                                                                                    </p>
                                                                                @else
                                                                                    <p>
                                                                                        HELPER
                                                                                    </p>
                                                                                @endif

                                                                            </div>
                                                                            <div class="status status delivered" id="user_{{$user->id}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                                <div class="chat-actions">
                                                                    <div class="dropdown">
                                                                        <a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-bs-toggle="dropdown">
                                                                            <em class="icon ni ni-more-h"></em>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-end">
                                                                            <ul class="link-list-opt no-bdr">
                                                                                <li>
                                                                                    <a href="{{ route('mark_as_read',$user) }}">Mark as read</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                <div class="simplebar-placeholder" style="width: auto; height: 664px;"></div>--}}
                            </div>
{{--                            <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">--}}
{{--                                <div class="simplebar-scrollbar simplebar-visible" style="width: 0px; display: none;"></div>--}}
{{--                            </div>--}}
{{--                            <div class="simplebar-track simplebar-vertical" style="visibility: visible;">--}}
{{--                                <div class="simplebar-scrollbar simplebar-visible" style="height: 33px; transform: translate3d(0px, 0px, 0px); display: block;"></div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="nk-chat-body">
                        <div class="nk-chat-head">
                            <ul class="nk-chat-head-info">
                                <li class="nk-chat-body-close">
                                    <a href="#" class="btn btn-icon btn-trigger nk-chat-hide ms-n1">
                                        <em class="icon ni ni-arrow-left"></em>
                                    </a>
                                </li>
                                <li class="nk-chat-head-user">
                                    <div class="user-card">
                                        <div class="user-avatar bg-purple">
                                            <span>{{ get_name_first_letters(auth()->user()->name) }}</span>
                                        </div>
                                        <div class="user-info">
                                            <div class="lead-text">{{ auth()->user()->name }}</div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="nk-chat-head-search">
                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-left">
                                            <em class="icon ni ni-search"></em>
                                        </div>
                                        <input type="text" class="form-control form-round" id="chat-search" placeholder="Search in Conversation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nk-chat-panel" data-simplebar="init">
                            <div class="simplebar-wrapper" style="margin: -20px -28px;">
                                <div class="simplebar-height-auto-observer-wrapper">
                                    <div class="simplebar-height-auto-observer"></div>
                                </div>
                                <div class="simplebar-mask">
                                    <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                        <div class="simplebar-content-wrapper" tabindex="0" role="region" aria-label="scrollable content" style="height: 100%; overflow: hidden scroll;">
                                            <div class="simplebar-content messages_div" style="padding: 20px 28px;">

                                                @if($messages->count() == 0)
                                                    <div class="no_message_text text-center h4">No Messages Found</div>
                                                @else
                                                    @foreach($messages as $message)
                                                        @if($message->from_user == auth()->user()->id)
                                                            <div class="chat is-you message">
                                                                <div class="chat-avatar">
                                                                    <div class="user-avatar bg-purple">
                                                                        <span>{{ get_name_first_letters($message->from->name) }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="chat-content">
                                                                    <div class="chat-bubbles">
                                                                        <div class="chat-bubble">
                                                                            <div class="chat-msg">
                                                                                {{$message->message}}
                                                                            </div>
                                                                            <ul class="chat-msg-more">
                                                                                <li>
                                                                                    <div class="dropdown">
                                                                                        <a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-bs-toggle="dropdown">
                                                                                            <em class="icon ni ni-more-h"></em>
                                                                                        </a>
                                                                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-end">
                                                                                            <ul class="link-list-opt no-bdr">
                                                                                                <li>
                                                                                                    <a href=" {{ route('admin_delete_msg',$message) }}">
                                                                                                        <em class="icon ni ni-trash-fill"></em> Remove </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <ul class="chat-meta">
                                                                        <li>{{$message->from->name}}</li>
                                                                        <li>{{\Carbon\Carbon::parse($message->created_at)->format('d-m-y h:i A')}} </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="chat is-me message">
                                                                <div class="chat-content">
                                                                    <div class="chat-bubbles">
                                                                        <div class="chat-bubble">
                                                                            <div class="chat-msg"> {{$message->message}}. </div>
                                                                            <ul class="chat-msg-more">
                                                                                <li>
                                                                                    <div class="dropdown">
                                                                                        <a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-bs-toggle="dropdown">
                                                                                            <em class="icon ni ni-more-h"></em>
                                                                                        </a>
                                                                                        <div class="dropdown-menu dropdown-menu-sm">
                                                                                            <ul class="link-list-opt no-bdr">
                                                                                                <li>
                                                                                                    <a href=" {{ route('admin_delete_msg',$message) }}">
                                                                                                        <em class="icon ni ni-trash-fill"></em> Remove </a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <ul class="chat-meta">
                                                                        <li>{{$message->from->name}}</li>
                                                                        <li>{{\Carbon\Carbon::parse($message->created_at)->format('d-m-y h:i A')}} </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="simplebar-placeholder" style="width: auto; height: 753px;"></div>
                            </div>
                            <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                                <div class="simplebar-scrollbar" style="width: 0px; display: none;"></div>
                            </div>
                            <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
                                <div class="simplebar-scrollbar" style="height: 25px; transform: translate3d(0px, 25px, 0px); display: block;"></div>
                            </div>
                        </div>
                        <div class="nk-chat-editor">
                            <div class="nk-chat-editor-form">
                                <form method="post" action="{{route('admin_send_msg')}}" class="ajax-form">
                                    @csrf
                                    <div class="form-control-wrap">
                                    <textarea class="form-control form-control-simple no-resize" rows="2"
                                              name="comment"
                                              id="comment"
                                              placeholder="Type your message to {{$receiver->name}} "
                                              required></textarea>
                                    </div>
                                    <input type="hidden" name="from_user" value="{{auth()->user()->id}}">
                                    <input type="hidden" name="to_user" value="{{$receiver->id}}">
                                </form>
                            </div>
                            <ul class="nk-chat-editor-tools g-2">
                                <li>
                                    <button class="btn btn-round btn-primary btn-icon post_message" id="post_message">
                                        <em class="icon ni ni-send-alt"></em>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@push("js")
    <script>
        $(document).ready(function() {
            $('#search_by_name').on('keyup', function() {
                var input = $(this).val().toLowerCase();

                $('.chat-item').filter(function() {
                    var userName = $(this).find('.name').text().toLowerCase();
                    $(this).toggle(userName.indexOf(input) !== -1);
                });
            });
        });
    </script>
    <script>
        $(document).on("click", ".post_message", function () {
            const comment_len = $('#comment').val().length;
            if (comment_len === 0){
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 2500,
                    timerProgressBar: true,
                });

                Toast.fire({
                    icon: 'error',
                    title: 'Please Enter message to send'
                })
            }else {
                $('.ajax-form').submit();
            }
        });
        // ajax forms
        $(".ajax-form").on("submit", function (e) {
            e.preventDefault();
            let current_url = $(this).attr("action");
            let request_type = $(this).attr("method");
            let payload = $(this).serializeArray();
            $.ajax({
                url: current_url,
                type: request_type,
                data: payload,
                beforeSend: () => {
                    $('.post_message').addClass('disabled');
                    $('#comment').addClass('disabled');
                },
                success: (data) => {
                    $('.post_message').removeClass('disabled');
                    $('#comment').removeClass('disabled');
                    $(this).trigger("reset");
                    if ($('.message').length === 0){
                        $('.no_message_text').addClass('d-none');
                    }
                    if(data.status === true){
                        $('.messages_div').append(data.message_html);
                        $('.messages_div').animate({
                            scrollTop: $('.messages_div')[0].scrollHeight
                        }, 500);
                    }
                }
            });
        });

    </script>
    <script>
        setInterval(function () {
            @if($users_ids->count() > 0)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route('get_admin_unread_messages_count')}}',
                data:{
                    'users_ids':  '{{implode(',',$users_ids->toArray())}}',
                },
                type: "POST",
                success: function (data) {
                    if(data.status){
                        const response_data = data.response_data;
                        $.map(response_data, function(value, index) {
                            const user_id = value.user_id;
                            const badge_id = '#user_badge_'+user_id;
                            const user = '#user_'+user_id;
                            const count_message = value.count_message;
                            if ($(badge_id).length) {
                                if(count_message > 0){
                                    if ($(badge_id).hasClass('d-none')){
                                        $(badge_id).removeClass('d-none');
                                    }
                                    $(badge_id).text(count_message);
                                }else {
                                    if (value.is_remove === true){
                                        $(user).addClass('d-none');
                                    }
                                }
                            }else {
                                if(count_message > 0){
                                    const span = '<div class="user-avatar-group" > ' +
                                        ' <div class="user-avatar xs bg-blue" >' +
                                        '<span id="user_badge_'+user_id+'">'+count_message+'</span>' +
                                        '</div> ' +
                                        '</div>'
                                    $(user).append(span)
                                }
                            }
                        });
                    }
                }
            });
            @endif
        }, 3000);
    </script>
    <script>
        setInterval(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route('get_received_messages')}}',
                data:{
                    'from_user':  {{ $receiver->id }},
                    'to_user':  {{   \Illuminate\Support\Facades\Auth::user()->id }},
                },
                type: "POST",
                success: function (data) {
                    if(data.status){
                        $('.messages_div').append(data.message_html);
                        $('.messages_div').animate({
                            scrollTop: $('.messages_div')[0].scrollHeight
                        }, 500);
                    }
                }
            });
        }, 3000);
    </script>
@endpush
