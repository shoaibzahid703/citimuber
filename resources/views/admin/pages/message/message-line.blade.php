@if( isset($received_message) && $received_message === true)
    @foreach($messages as $message)
        <div class="chat is-me message">
            <div class="chat-content">
                <div class="chat-bubbles">
                    <div class="chat-bubble">
                        <div class="chat-msg"> {{$message->message}}. </div>
                        <ul class="chat-msg-more">
                            <li>
                                <div class="dropdown">
                                    <a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-bs-toggle="dropdown">
                                        <em class="icon ni ni-more-h"></em>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-sm">
                                        <ul class="link-list-opt no-bdr">
                                            <li>
                                                <a href=" {{ route('admin_delete_msg',$message) }}">
                                                    <em class="icon ni ni-trash-fill"></em> Remove </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul class="chat-meta">
                    <li>{{$message->from->name}}</li>
                    <li>{{\Carbon\Carbon::parse($message->created_at)->format('d-m-y h:i A')}} </li>
                </ul>
            </div>
        </div>
    @endforeach
@else
    <div class="chat is-you">
        <div class="chat-avatar">
            <div class="user-avatar bg-purple">
                <span>{{ get_name_first_letters($message->from->name) }}</span>
            </div>
        </div>
        <div class="chat-content">
            <div class="chat-bubbles">
                <div class="chat-bubble">
                    <div class="chat-msg">
                        {{$message->message}}
                    </div>
                    <ul class="chat-msg-more">
                        <li>
                            <div class="dropdown">
                                <a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-bs-toggle="dropdown">
                                    <em class="icon ni ni-more-h"></em>
                                </a>
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-end">
                                    <ul class="link-list-opt no-bdr">
                                        <li>
                                            <a href="{{ route('admin_delete_msg',$message) }}">
                                                <em class="icon ni ni-trash-fill"></em> Remove </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="chat-meta">
                <li>{{$message->from->name}}</li>
                <li>{{\Carbon\Carbon::parse($message->created_at)->format('d-m-y h:i A')}} </li>
            </ul>
        </div>
    </div>
@endif



