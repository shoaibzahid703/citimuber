@extends('admin.layouts.app')
@section('title','News Landing Page')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">News Landing Page</h3>
          </div>
        </div>
      </div>
        @php
        @$homePage = App\Models\NewLandingPage::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-news-page')}}" class="form-validate" novalidate="novalidate">
            @csrf
            <div class="row g-gs">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{@$homePage->first_heading}}" placeholder="First Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="paragraph"> Paragraph</label>
                  <div class="form-control-wrap">
   
                     <input type="text" name="paragraph" class="form-control @error('paragraph') is-invalid @enderror" id="name"  required="" placeholder="Paragraph" value="{{@$homePage->paragraph}}">
                    @error('paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12" style="display: none;">
                <div class="form-group">
                  <label class="form-label" for="fv-message">Description</label>
                  <div class="form-control-wrap">
                    <textarea id="letter_format" class="form-control form-control-sm"  name="description">{{@$homePage->description}}</textarea>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">All News</h3>
          </div><!-- .nk-block-head-content -->
          <a href="{{route('add-news')}}" class="btn btn-primary">Add</a>
        </div><!-- .nk-block-between -->
      </div>
      <div class="card card-bordered ">
        <div class="card-inner" id="row">
          <div class="card-datatable table-responsive">
            <table id="mews_page" class="table table-hover" style="width:100%">
              <thead>
                <tr>

                  <th>Title</th>
                  <th>Date</th>
                  <!-- <th>Image</th> -->
                  <th>Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="descriptionModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="commentModalLabel">Full Details</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="fullDescription"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection


@push('js')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('admin/datatable/datatables.js')}}"></script>
<script type="text/javascript">
  languages_table = $("#mews_page").DataTable({
    serverSide: true,
    processing: true,
    "ajax": {
      "url": '{{route('news_page')}}',
    },
    columns: [{
        data: 'title',
        name: 'title'
      },
      {
        data: 'news_date',
        name: 'news_date'
      },
      // {
      //   data: 'news_image',
      //   name: 'news_image'
      // },
      {
        data: 'description',
        name: 'description'
      },
      
      {
        data: 'status',
        name: 'status'
      },
      {
        data: 'action',
        name: 'action',
        orderable: false,
        searchable: false
      }
    ]
  });
</script>

<script>
  $(document).ready(function() {
    $(document).on('click', '.see-more', function(e) {
      e.preventDefault();
      var fullDescription = $(this).data('description');
      $('#fullDescription').text(fullDescription);
      $('#descriptionModal').modal('show');
    });
  });
</script>
@endpush