@extends('admin.layouts.app')
@section('title','Add Holiday')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Add Holiday</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-holiday')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="title">Title</label>
                  <div class="form-control-wrap">
                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title"  required="" placeholder="Holiday Title">
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="holiday_type">Holiday Type</label>
                  <div class="form-control-wrap">
                    <input type="text" name="holiday_type" class="form-control @error('holiday_type') is-invalid @enderror" id="holiday_type"  required="" placeholder="Holiday Type">
                    @error('holiday_type')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="holiday_date">Holiday Date</label>
                  <div class="form-control-wrap">
                    <input type="date" name="holiday_date" class="form-control @error('holiday_date') is-invalid @enderror" id="holiday_date"  required >
                    @error('holiday_date')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="country">Country</label>
                    <div class="form-control-wrap">
                        <select name="country_id" id="country" class="form-control  @error('country_id') is-invalid @enderror">
                            @foreach($country as $countries)
                                <option value="{{ $countries->id }}">{{$countries->name}}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                 </div>
            </div>


              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="comment">Details</label>
                  <div class="form-control-wrap">
   
                    <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description"  required="">{{@$homePage->description}}</textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
