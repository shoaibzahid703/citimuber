@extends('admin.layouts.app')
@section('title','Public Holiday')
@section('content')


<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Public Holiday</h3>
          </div>
        </div>
      </div>
        @php
        $homePage = App\Models\PublicHolidayLandingPage::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-holiday-page')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_heading">Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{@$homePage->first_heading}}" placeholder="Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="description">Paragraph</label>
                  <div class="form-control-wrap">
                    <input type="text" name="description" class="form-control @error('description') is-invalid @enderror" id="description"  required="" value="{{@$homePage->description}}" placeholder="First Heading">
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid mt-4">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Add Public Holidays</h3>
                        </div><!-- .nk-block-head-content -->
                        <a href="{{route('add-holiday')}}" class="btn btn-primary">Add</a>
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <div class="card-datatable table-responsive">
                            <table id="clinet_says" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                	
                                    <th>Title</th>
                                    <th>Date</th>
                                    <th>Holiday Type</th>
                                    <th>Details</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="commentModalLabel">Full Comment</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="fullComment"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
 
@endsection
@push("js")
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('admin/datatable/datatables.js')}}"></script>
    <script type="text/javascript">
        languages_table =    $("#clinet_says").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('holiday_page')}}',
            },
            columns: [
                {data: 'title', name: 'title'},
                
                {data: 'holiday_date', name: 'holiday_date'},
                {data: 'holiday_type', name: 'holiday_type'},
                {data: 'description', name: 'description'},
                {data: 'status', name: 'status'},
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>

        <script>
    $(document).ready(function(){
        $(document).on('click', '.see-more', function(e){
            e.preventDefault();
            var fullComment = $(this).data('description');
            $('#fullComment').text(fullComment);
            $('#commentModal').modal('show');
        });
    });
</script>

@endpush
