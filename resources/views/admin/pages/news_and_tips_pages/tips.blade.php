@extends('admin.layouts.app')
@section('title','Tips Landing Page')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Tips Landing Page</h3>
          </div>
        </div>
      </div>
        @php
        @$homePage = App\Models\TipsLandingPage::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-tips-page')}}" class="form-validate" novalidate="novalidate">
            @csrf
            <div class="row g-gs">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{@$homePage->first_heading}}" placeholder="First Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="paragraph"> Paragraph</label>
                  <div class="form-control-wrap">
   
                     <input type="text" name="paragraph" class="form-control @error('paragraph') is-invalid @enderror" id="name"  required="" placeholder="Paragraph" value="{{@$homePage->paragraph}}">
                    @error('paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12" style="display: none;">
                <div class="form-group">
                  <label class="form-label" for="fv-message">Description</label>
                  <div class="form-control-wrap">
                    <textarea id="letter_format" class="form-control form-control-sm"  name="description">{{@$homePage->description}}</textarea>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@push('js')

<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'letter_format', {
        filebrowserUploadMethod: 'form'
    });
    
</script>
@endpush