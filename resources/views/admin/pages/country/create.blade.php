@extends('admin.layouts.app')
@section('title','Add Country')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Add Country</h3>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-inner">
                        <form method="post" action="{{route('save-country')}}" class="form-validate" novalidate="novalidate">
                            @csrf
                            <div class="row g-gs">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="name"> Name</label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"  required="" placeholder="Country name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="code">Country Code</label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="code" class="form-control @error('code') is-invalid @enderror" id="code"  required placeholder="Country code">
                                            @error('code')
                                            <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="nationality">Name</label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="nationality" class="form-control @error('nationality') is-invalid @enderror" id="nationality"  required="" placeholder="Country nationality">
                                            @error('nationality')
                                            <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group mt-4 pt-1">
                                        <button type="submit" class="btn btn-md btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
