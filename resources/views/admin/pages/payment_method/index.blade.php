@extends('admin.layouts.app')
@section('title','Payment Method')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Payment Method</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{ route('update.env') }}" class="form-validate" novalidate="novalidate">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="name"> Stripe Key</label>
                  <div class="form-control-wrap">
                    <input type="text" name="STRIPE_KEY" class="form-control"  value="{{  env('STRIPE_KEY') }}">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="code">Stripe Secret</label>
                  <div class="form-control-wrap">
                     <input type="text" class="form-control"  name="STRIPE_SECRET" value="{{  env('STRIPE_SECRET') }}" required>
                  
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
