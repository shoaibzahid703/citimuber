@extends('admin.layouts.app')
@section('title','Privacy Policy')
@section('content')

<style type="text/css">
	.cke_notifications_area{ display: none !important; }
</style>
<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Privacy Policy Page</h3>
          </div>
        </div>
      </div>
        @php
        @$privacy_policy = App\Models\PrivacyPolicy::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-privacy-policy')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">

            @csrf
            <div class="row g-gs">
        
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="fv-message">Description</label>
                  <div class="form-control-wrap">
                    <textarea id="letter_format" class="form-control form-control-sm"  name="description">{{@$privacy_policy->description}}</textarea>
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@push('js')

<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'letter_format', {
        filebrowserUploadMethod: 'form'
    });
    
</script>
@endpush