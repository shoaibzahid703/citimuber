@extends('admin.layouts.app')
@section('title','Edit Event')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Edit Event</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('update-happy-employee', $employee)}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="employee_name">Employee Name</label>
                  <div class="form-control-wrap">
                    <input type="text" name="employee_name" class="form-control @error('employee_name') is-invalid @enderror" id="employee_name"  required="" placeholder="Holiday Title" value="{{$employee->employee_name}}">
                    @error('employee_name')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>


              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="employee_image">Employee Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="employee_image" class="form-control @error('employee_image') is-invalid @enderror" id="employee_image" placeholder="Country code">
                    @error('employee_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  @if($employee->employee_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_employee_image')}}/{{$employee->employee_image}}" alt="" width="20%" height="100px">
                  @endif
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="description">Description</label>
                  <div class="form-control-wrap">
                    <textarea class="form-control @error('description') is-invalid @enderror" name="description" required>{{$employee->description}}</textarea>
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('js')

<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'letter_format', {
        filebrowserUploadMethod: 'form'
    });
    
</script>
@endpush