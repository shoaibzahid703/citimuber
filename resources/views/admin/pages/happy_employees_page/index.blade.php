@extends('admin.layouts.app')
@section('title','Happy Employees')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Happy Employees Page</h3>
          </div>
        </div>
      </div>
      @php
      $happy_employee_page = App\Models\HappyEmployeesPage::find(1);
      @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-happy-employees')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_heading">First Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading" required="" value="{{@$happy_employee_page->first_heading}}" placeholder="Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_text">First Text</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_text" class="form-control @error('first_text') is-invalid @enderror" id="first_text" required="" value="{{@$happy_employee_page->first_text}}" placeholder="First Text">
                    @error('first_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="second_heading">Second Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_heading" class="form-control @error('second_heading') is-invalid @enderror" id="second_heading" required="" value="{{@$happy_employee_page->second_heading}}" placeholder="Second Heading">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_paragraph">First Paragraph</label>
                  <div class="form-control-wrap">
                    <textarea name="first_paragraph" class="form-control @error('first_paragraph') is-invalid @enderror" id="first_paragraph" required="">{{@$happy_employee_page->first_paragraph}}</textarea>
                    @error('first_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="third_heading">Third Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="third_heading" class="form-control @error('third_heading') is-invalid @enderror" id="third_heading" required="" value="{{@$happy_employee_page->third_heading}}" placeholder="Third Heading">
                    @error('third_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="second_paragraph">Second Paragraph</label>
                  <div class="form-control-wrap">
                    <textarea name="second_paragraph" class="form-control @error('second_paragraph') is-invalid @enderror" id="second_paragraph" required="">{{@$happy_employee_page->second_paragraph}}</textarea>
                    @error('second_paragraph')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="fourth_heading">Fourth Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="fourth_heading" class="form-control @error('fourth_heading') is-invalid @enderror" id="fourth_heading" required="" value="{{@$happy_employee_page->fourth_heading}}" placeholder="Fourth Heading">
                    @error('fourth_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="second_text">Second Text Line</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_text" class="form-control @error('second_text') is-invalid @enderror" id="second_text" required="" value="{{@$happy_employee_page->second_text}}" placeholder="Second Text Line">
                    @error('second_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="five_heading">Five Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="five_heading" class="form-control @error('five_heading') is-invalid @enderror" id="five_heading" required="" value="{{@$happy_employee_page->five_heading}}" placeholder="Five Heading">
                    @error('five_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="third_text">Third Text Line</label>
                  <div class="form-control-wrap">
                    <input type="text" name="third_text" class="form-control @error('third_text') is-invalid @enderror" id="third_text" required="" value="{{@$happy_employee_page->third_text}}" placeholder="Third Text Line">
                    @error('third_text')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="contact_us_button">Contact Us Button</label>
                  <div class="form-control-wrap">
                    <input type="text" name="contact_us_button" class="form-control @error('contact_us_button') is-invalid @enderror" id="contact_us_button" required="" value="{{@$happy_employee_page->contact_us_button}}" placeholder="Contact Us Button">
                    @error('contact_us_button')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="bg_image">Background Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="bg_image" class="form-control @error('bg_image') is-invalid @enderror" id="bg_image" placeholder="Country code">
                    @error('bg_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happy_employee_page->bg_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employee_page->bg_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="first_image">First Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="first_image" class="form-control @error('first_image') is-invalid @enderror" id="first_image" placeholder="Country code">
                    @error('first_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happy_employee_page->first_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employee_page->first_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="sec_image">Second Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="sec_image" class="form-control @error('sec_image') is-invalid @enderror" id="sec_image" placeholder="Country code">
                    @error('sec_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happy_employee_page->sec_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employee_page->sec_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="app_store_image">App Store Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="app_store_image" class="form-control @error('app_store_image') is-invalid @enderror" id="app_store_image" placeholder="Country code">
                    @error('app_store_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happy_employee_page->app_store_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employee_page->app_store_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="google_play_image">Google Play Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="google_play_image" class="form-control @error('google_play_image') is-invalid @enderror" id="google_play_image" placeholder="Country code">
                    @error('google_play_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happy_employee_page->google_play_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employee_page->google_play_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="sec_bg_image">Last Background Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="sec_bg_image" class="form-control @error('sec_bg_image') is-invalid @enderror" id="sec_bg_image" placeholder="Country code">
                    @error('sec_bg_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  @if(@$happy_employee_page->sec_bg_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employee_page->sec_bg_image}}" alt="" width="20%" height="100px">
                  @endif

                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt-4">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">All Happy Employees</h3>
          </div><!-- .nk-block-head-content -->
          <a href="{{route('add-happy-employee')}}" class="btn btn-primary">Add</a>
        </div><!-- .nk-block-between -->
      </div>
      <div class="card card-bordered ">
        <div class="card-inner" id="row">
          <div class="card-datatable table-responsive">
            <table id="happy-employees" class="table table-hover" style="width:100%">
              <thead>
                <tr>

                  <th>Name</th>
                  <th>Image</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="descriptionModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="commentModalLabel">Full Details</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="fullDescription"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection
@push("js")

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('admin/datatable/datatables.js')}}"></script>
<script type="text/javascript">
  languages_table = $("#happy-employees").DataTable({
    serverSide: true,
    processing: true,
    "ajax": {
      "url": '{{route('happy-employees')}}',
    },
    columns: [{
        data: 'employee_name',
        name: 'employee_name'
      },
      {
        data: 'employee_image',
        name: 'employee_image'
      },
      {
        data: 'description',
        name: 'description'
      },
      {
        data: 'status',
        name: 'status'
      },
      {
        data: 'action',
        name: 'action',
        orderable: false,
        searchable: false
      }
    ]
  });
</script>

<script>
  $(document).ready(function() {
    $(document).on('click', '.see-more', function(e) {
      e.preventDefault();
      var fullDescription = $(this).data('description');
      $('#fullDescription').text(fullDescription);
      $('#descriptionModal').modal('show');
    });
  });
</script>
@endpush