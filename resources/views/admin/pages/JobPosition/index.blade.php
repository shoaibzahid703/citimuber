@extends('admin.layouts.app')
@section('title','All Job Positions')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Job Positions</h3>
                        </div><!-- .nk-block-head-content -->
                         <a href="{{route('job_position_create')}}" class="btn btn-primary">Add</a>
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                	<div class="card-inner" id="row">
                    <div class="card-datatable table-responsive">
                        <table id="job_position" class="table table-hover" style="width:100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")

    <script type="text/javascript">
        persnalities_table =    $("#job_position").DataTable({
                serverSide: true,
                processing: true,
                "ajax": {
                    "url": '{{route('job-position')}}',
                },
                columns: [
                    {data: 'name', name: 'name'},
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
    </script>

@endpush
