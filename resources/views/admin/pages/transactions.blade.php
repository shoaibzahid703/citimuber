@extends('admin.layouts.app')
@section('title','All Transactions')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Transactions</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <div class="card-datatable table-responsive">
                            <table id="transactions" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Plan</th>
                                    <th>Job</th>
                                    <th>Payment Id</th>
                                    <th>Amount</th>
                                    <th>Currency</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")

    <script type="text/javascript">
        transactions_table =    $("#transactions").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('all_transactions')}}',
            },
            columns: [
                {data: 'user_name', name: 'user.name'},
                {data: 'plan_name', name: 'plan_name',searchable: false},
                { data: 'job_name', name: 'job.job_title' },
                { data: 'payment_id', name: 'payment_id' },
                { data: 'amount', name: 'amount' },
                { data: 'currency', name: 'currency' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>

@endpush
