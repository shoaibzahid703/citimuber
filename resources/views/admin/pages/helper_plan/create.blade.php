@extends('admin.layouts.app')
@section('title','Create Helper Plan')
@push('css')
    <link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
@endpush
@section('content')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Create Helper Plan</h3>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-inner">
                        <form method="post" action="{{route('helper_plan_store')}}" id="plans_store" class="form-validate" novalidate="novalidate">
                            @csrf
                            <div class="row g-gs">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="name"> Name*</label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" required="" placeholder="Plan name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="name"> Post Duration (Days)*</label>
                                        <div class="form-control-wrap">
                                            <input type="number" name="post_duration" class="form-control @error('post_duration') is-invalid @enderror" id="post_duration" required="" placeholder="0">
                                            @error('post_duration')
                                            <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-check mt-4">
                                        <input type="checkbox" class="form-check-input" id="employer_contact" name="employer_contact" checked value="1">
                                        <label class="form-check-label" for="employer_contact">Employer Contact</label>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class="form-label" for="price">Default Price (HKD) *</label>
                                        <div class="form-control-wrap">
                                            <input type="number" name="price" class="form-control price-input" id="price" required placeholder="0">
                                        </div>
                                    </div>
                                </div>

                                <div class="input-group mt-2" id="dynamic-inputs-container">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="country">Currency Country *</label>
                                            <div class="form-control-wrap">
                                                <select name="country[]" class="form-control country-select" required>
                                                    <option value="" selected disabled>Please select</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{$country->code}}">{{$country->code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 ps-3">
                                        <div class="form-group">
                                            <label class="form-label" for="manual_price">Manual Price *</label>
                                            <div class="form-control-wrap">
                                                <input type="number" name="manual_price[]" class="form-control manual-price-input" required placeholder="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 mt-4 pt-1">
                                    <button type="button" id="add-more-button" class="btn btn-success"><i class="fas fa-plus"></i></button>
                                </div>

                                <div class="row mt-2">
                                    <div class="col-md-12">
                                        <div class="feature-input">
                                            <div class="form-group">
                                                <label class="form-label" for="features"> Feature Description *</label>
                                                <div id="editor-container"></div>
                                                @error('feature_description')
                                                <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                          </span>
                                                @enderror
                                                <input type="hidden" name="feature_description" id="feature_description">
                                                {{--                                                <div class="form-control-wrap">--}}
{{--                                                    <input type="text" name="features[]" class="form-control @error('features') is-invalid @enderror" id="features" required="" placeholder="Features">--}}
{{--                                                    @error('features')--}}
{{--                                                    <span class="invalid-feedback" role="alert">--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                          </span>--}}
{{--                                                    @enderror--}}
{{--                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-5">
                                    <div class="col-md-12 ">
                                        <div class="form-group ">
                                            <button type="submit" class="btn btn-md btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            const dynamicInputsContainer = document.getElementById('dynamic-inputs-container');
            const addMoreButton = document.getElementById('add-more-button');

            // Function to add a new set of input fields
            function addMoreInputs() {
                const newInputGroup = document.createElement('div');
                newInputGroup.classList.add('input-group');
                newInputGroup.innerHTML = `
            <div class="col-md-6 mt-2">
                <div class="form-group">
                    <label class="form-label" for="country">Currency Country *</label>
                    <div class="form-control-wrap">
                        <select name="country[]" class="form-control country-select" required>
                            <option value="" selected disabled>Please select</option>
                            @foreach($countries as $country)
                <option value="{{$country->code}}">{{$country->name}}</option>
                            @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-5 mt-2 ps-2">
        <div class="form-group">
            <label class="form-label" for="manual_price">Manual Price *</label>
            <div class="form-control-wrap">
                <input type="number" name="manual_price[]" class="form-control manual-price-input" required placeholder="0">
            </div>
        </div>
    </div>
    <div class="col-md-1 mt-4 pt-4 ps-4">
       <button type="button" class="btn btn-danger remove-button"><i class="fas fa-remove"></i></button>
    </div>
`;
                dynamicInputsContainer.appendChild(newInputGroup);
            }

            // Add event listener to "Add More" button
            addMoreButton.addEventListener('click', addMoreInputs);

            // Function to convert price and update manual price
            function convertAndUpdatePrice(priceInput, countrySelect, manualPriceInput) {
                const price = parseFloat(priceInput.value);
                const selectedCountryCode = countrySelect.value;

                if (price > 0 && selectedCountryCode) {
                    fetchCurrency(selectedCountryCode, price, manualPriceInput);
                } else {
                    manualPriceInput.value = ''; // Clear the manual price if price input or country is not selected
                }
            }

            // Fetch currency details based on country code
            function fetchCurrency(countryCode, price, manualPriceInput) {
                const apiUrl = `https://restcountries.com/v3.1/alpha/${countryCode}`;
                fetch(apiUrl)
                    .then(response => response.json())
                    .then(data => {
                        if (data.length > 0) {
                            const countryData = data[0];
                            const currencyKey = Object.keys(countryData.currencies)[0];
                            const currencyCode = countryData.currencies[currencyKey].code || currencyKey;
                            if (currencyCode) {
                                fetchConversionRate(currencyCode, price, manualPriceInput);
                            } else {
                                console.error('Currency not found for selected country.');
                            }
                        } else {
                            console.error('Invalid country code or no data returned.');
                        }
                    })
                    .catch(error => console.error('Error fetching currency information:', error));
            }

            // Fetch conversion rate and update manual price
            function fetchConversionRate(currency, price, manualPriceInput) {
                const apiKey = '804207b651131734780b029a';
                const apiUrl = `https://v6.exchangerate-api.com/v6/${apiKey}/latest/USD`;
                fetch(apiUrl)
                    .then(response => response.json())
                    .then(data => {
                        const usdToSelectedCurrencyRate = data.conversion_rates[currency];
                        if (usdToSelectedCurrencyRate) {
                            updateManualPrice(usdToSelectedCurrencyRate, price, manualPriceInput);
                        } else {
                            console.error('Conversion rate for selected currency not found.');
                        }
                    })
                    .catch(error => console.error('Error fetching conversion rate:', error));
            }

            // Update manual price field with the converted price
            function updateManualPrice(conversionRate, price, manualPriceInput) {
                const convertedPrice = (price * conversionRate).toFixed(2);
                manualPriceInput.value = convertedPrice;
            }

            // Initial input handling
            dynamicInputsContainer.addEventListener('input', function(event) {
                if (event.target.classList.contains('price-input')) {
                    const inputGroup = event.target.closest('.input-group');
                    const priceInput = inputGroup.querySelector('.price-input');
                    const countrySelect = inputGroup.querySelector('.country-select');
                    const manualPriceInput = inputGroup.querySelector('.manual-price-input');
                    convertAndUpdatePrice(priceInput, countrySelect, manualPriceInput);
                }
            });

            // Handling country selection in existing and dynamically added inputs
            dynamicInputsContainer.addEventListener('change', function(event) {
                if (event.target.classList.contains('country-select')) {
                    const inputGroup = event.target.closest('.input-group');
                    const priceInput = inputGroup.querySelector('.price-input');
                    const countrySelect = event.target;
                    const manualPriceInput = inputGroup.querySelector('.manual-price-input');
                    convertAndUpdatePrice(priceInput, countrySelect, manualPriceInput);
                }
            });

            // Handling dynamic inputs without price input
            dynamicInputsContainer.addEventListener('input', function(event) {
                if (event.target.classList.contains('country-select')) {
                    const inputGroup = event.target.closest('.input-group');
                    const countrySelect = event.target;
                    const manualPriceInput = inputGroup.querySelector('.manual-price-input');
                    const priceInput = document.querySelector('.price-input'); // Only one price input for initial price
                    if (priceInput) {
                        const price = parseFloat(priceInput.value);
                        convertAndUpdatePrice(priceInput, countrySelect, manualPriceInput);
                    }
                }
            });

            dynamicInputsContainer.addEventListener('click', function(event) {
                if (event.target.classList.contains('remove-button')) {
                    const inputGroup = event.target.closest('.input-group');
                    dynamicInputsContainer.removeChild(inputGroup);
                }
            });
        });
        var toolbarOptions = [
            ['bold', 'italic', 'underline'],        // toggled buttons
            ['blockquote'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        ];

        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: toolbarOptions,
            },
            placeholder: 'feature description',
            theme: 'snow'
        });

        $("#plans_store").on("submit", function(){
            var myEditor = document.querySelector('#editor-container')
            var html = myEditor.children[0].innerHTML;
            $("#feature_description").val(html);
        })
    </script>
@endpush
