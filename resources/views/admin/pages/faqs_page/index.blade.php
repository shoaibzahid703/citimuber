@extends('admin.layouts.app')
@section('title','Faqs')
@section('content')

<div class="container-fluid mt-4">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Faqs</h3>
                        </div><!-- .nk-block-head-content -->
                        <a href="{{route('add-faqs')}}" class="btn btn-primary">Add</a>
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <div class="card-datatable table-responsive">
                            <table id="faqs-page" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                	
                                    <th>Question</th>
                                    <th>Question Answer</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade" id="answerModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="commentModalLabel">Question Answer</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="fullAnswer"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
 
@endsection
@push("js")
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('admin/datatable/datatables.js')}}"></script>
    <script type="text/javascript">
        languages_table =    $("#faqs-page").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('faqs-page')}}',
            },
            columns: [
                {data: 'question', name: 'question'},
                {data: 'question_answer', name: 'question_answer'},
                {data: 'status', name: 'status'},
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>

   <script>
    $(document).ready(function(){
        $(document).on('click', '.see-more', function(e){
            e.preventDefault();
            var fullAnswer = $(this).data('question_answer');
            $('#fullAnswer').text(fullAnswer);
            $('#answerModal').modal('show');
        });
    });
</script>

@endpush
