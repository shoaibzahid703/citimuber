@extends('admin.layouts.app')
@section('title','Add Faqs')
@section('content')

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Add Faqs</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-faqs')}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="question">Question</label>
                  <div class="form-control-wrap">
                    <input type="text" name="question" class="form-control @error('question') is-invalid @enderror" id="question"  required="" placeholder="Question">
                    @error('question')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="question_answer">Question Answer</label>
                  <div class="form-control-wrap">
                    <textarea class="form-control @error('question_answer') is-invalid @enderror" name="question_answer" id="question_answer"  required=""></textarea>
                    @error('question_answer')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
            </div>
              <div class="row g-gs">
              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
