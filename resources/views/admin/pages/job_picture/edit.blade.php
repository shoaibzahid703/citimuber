@extends('admin.layouts.app')
@section('title','Edit Job Picture')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Edit Job Picture</h3>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-inner">
                        <form method="post" action="{{route('job_picture_update',$job_picture)}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
                            @csrf
                            <div class="row g-gs">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="image"> Image</label>
                                        <div class="form-control-wrap">
                                            <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" id="image"  required  accept="image/*">
                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 text-center">
                                    <img src="{{asset('storage/job_picture/'.$job_picture->img)}}" width="60%">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group mt-4 pt-1">
                                        <button type="submit" class="btn btn-md btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
