@extends('admin.layouts.app')
@section('title','Contact Us Setting')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Contact Us Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('contact_us_setting_update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">


                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="contact_us_email">Contact Us Email</label>
                                        <div class="form-control-wrap">
                                            <input type="email" class="form-control @error('contact_us_email') is-invalid @enderror" id="contact_us_email" name="contact_us_email"
                                                   placeholder="contact us email"
                                                   value="{{old('contact_us_email') ? old('contact_us_email') : $setting->contact_us_email ?? ''}}">
                                            @error('contact_us_email')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="contact_us_number">Contact Us Number</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('contact_us_number') is-invalid @enderror" id="contact_us_number"
                                                   name="contact_us_number"
                                                   placeholder="contact us number"
                                                   value="{{old('contact_us_number') ? old('contact_us_number') : $setting->contact_us_number ?? ''}}">
                                            @error('contact_us_number')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="contact_us_address">Contact Us Address</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('contact_us_address') is-invalid @enderror" id="contact_us_address" name="contact_us_address"
                                                   placeholder="contact_us_address"
                                                   value="{{old('contact_us_address') ? old('contact_us_address') : $setting->contact_us_address ?? ''}}">
                                            @error('contact_us_address')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
