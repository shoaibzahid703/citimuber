@extends('admin.layouts.app')
@section('title','All Agency Plans')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Agency Plans</h3>
                        </div><!-- .nk-block-head-content -->
                        <a href="{{route('agency_plan_create')}}" class="btn btn-primary">Add</a>
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <div class="card-datatable table-responsive">
                            <table id="plans" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Package Duration (Days)</th>
                                    <th>No of Jobs</th>
                                    <th>No of Candidates</th>
                                    <th>Price (HKD)</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")

    <script type="text/javascript">
        helper_plans = $("#plans").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('agency_plans')}}',
            },
            columns: [
                {data: 'name', name: 'name'},
                { data: 'no_of_days', name: 'no_of_days' },
                { data: 'no_of_jobs', name: 'no_of_jobs' },
                { data: 'no_of_candidates', name: 'no_of_candidates' },
                { data: 'price', name: 'price' },
                { data: 'status', name: 'status' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>


@endpush
