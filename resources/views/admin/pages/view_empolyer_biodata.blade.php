<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Citimuber Employment Bio data</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .border-bottom-2{
            border-bottom: 2px solid black !important;
        }
        .bg-info{
            background-color: #65ffff !important;
        }
    </style>

</head>
<body>
  <div class="container" >
<div id="printableArea">

    <div class="row" style="padding: 40px;"  >

<div class="col-6 offset-6 text-end">
    <h5 class="d-inline"><b>CNAHK | CNOPH | CNOOC | CRCF:</b></h5>
        <h6 class="border-bottom-2 d-inline">{{$biodata->cnahk}}</h6>
</div>
        <div class="col-12">
            <div class="row">
        <div class="col-6 mt-3">
            <div class="row">
                <div class="col-9">
                    <img src="{{asset('cdn-sub/logo.png')}}" alt="Logo" class="img-fluid bg-primary mb-1">
                    <p>
                        Shop 151, Lik Sang Plaza, 1/F, 269 Castle Peak Road, Tsuen Wan,<br>
                        New Territories,<br>
                        Hong Kong,<br>
                   TEL: +852-9739-8915</p>
                    <p > EMAIL: support@citimuberemployment.agency</p>
                </div>
                <div class="col-12">
                  <p class="d-inline w-25">  Contract Status 合約狀態:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->status}}</p>
                </div>
            </div>


        </div>
        <div class="col-md-4 offset-md-2 mt-3 text-end">
            <div class="row">
                <div class="col-md-3 offset-md-3">
                        <img src="{{asset("/uploads").'/'.$biodata->profile_image}}" alt="Logo" style="width: 250px !important; height: 250px !important;  border: 2px solid black !important;" >

                </div>
            </div>


        </div>
                <div class="col-md-12 mt-3">
                    <div class="row">
                        <div class="col-md-3">
                            <p class="d-inline w-35">Release Date 完約日期: </p> <p class="d-inline border-bottom-2 w-65" style="    font-size: 13px;">{{$biodata->release_date}}</p>
                        </div>
                        <div class="col-md-3">
                            <p class="d-inline w-35">  Expiry Date 簽證到期日:</p> <p class="d-inline border-bottom-2 w-65" style="    font-size: 13px;">{{$biodata->expiry_date}} </p>
                        </div>
                        <div class="col-md-3">
                            <p class="d-inline w-35"> Apply Date 登記日期:</p> <p class="d-inline border-bottom-2 w-65">{{$biodata->apply_date}}</p>
                        </div>
                        <div class="col-md-2">
                            <p class="d-inline w-35">  Ref #:</p> <p class="d-inline border-bottom-2 w-65">{{$biodata->ref_number}}</p>
                        </div>
                    </div>
                </div>
        </div>
        </div>
        <div class="col-md-12 bg-info mt-3">
            <h4 >Personal Particular 申請人履歷</h4>
        </div>
        <div class="col-ms-12">
            <div class="row">
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Name 姓名: </p> <p class="d-inline border-bottom-2 w-75">{{$biodata->name}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Date of Birth 出生日期:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->dob}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Age 年齡:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->age}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Education 教育水平:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->education}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Nationality 國籍:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->personal_nationality}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Hong Kong ID/Blue Card 證件號碼:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->id_number}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Religion 宗教:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->religion}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Place of Birth 出生地點:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->birthplace}}</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Height 身高:</p> <p class="d-inline border-bottom-2 w-75">f{{$biodata->height}}  CM</p>
                </div>
                <div class="col-md-6 mt-3">
                    <p class="d-inline w-25">Weight 體重:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->weight}} KG</p>
                </div>
                <div class="col-md-12 mt-3">
                    <p class="d-inline w-25">No. of Children:</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->no_children}}</p>
                </div>
                <div class="col-md-12 mt-3 d-inline-flex">
                    <label style="margin-right: 15px;">Marital Status</label>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="marital_status" value="single" id="single"  {{ ($biodata->marital_status === 'single') ? 'checked' : ''; }}>
                        <label class="form-check-label" for="single" style="margin-right: 15px;">Single</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="marital_status" value="married" id="married"  {{ ($biodata->marital_status === 'married') ? 'checked' : ''; }}>
                        <label class="form-check-label" for="married" style="margin-right: 15px;">Married</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="marital_status" value="separated" id="separated"  {{ ($biodata->marital_status === 'separated') ? 'checked' : ''; }}>
                        <label class="form-check-label" for="separated" style="margin-right: 15px;">Separated</label>
                    </div>
                </div>


                <div class="col-md-4 mt-3">
                    <p style="font-size: 18px">Other Professional Course:</p>
                </div>
                <div class="col-md-4 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="nursing"  {{ ($biodata->nursing === 'nursing') ? 'checked' : ''; }}>
                    <label for="nursing">Bachelor in Nursing 護士學位</label>
                </div>
                <div class="col-md-4 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="midwifery" {{ ($biodata->midwifery === 'midwifery') ? 'checked' : ''; }}>
                    <label for="midwifery">Midwifery Course Degree 助護學位</label>
                </div>
                {{-- ch--}}
                <div class="col-md-4 mt-3">
                    <p style="font-size: 18px">其他專業課程</p>
                </div>
                <div class="col-md-4 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="education_course" {{ ($biodata->education_course === 'education_course') ? 'checked' : ''; }}>
                    <label for="education_course">Bachelor in Education 教育學位</label>
                </div>
                <div class="col-md-4 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="caregiver_course" {{ ($biodata->caregiver_course === 'caregiver_course') ? 'checked' : ''; }}>
                    <label for="caregiver_course">Caregiver Course Degree 護理課程</label>
                </div>
            </div>
        </div>
        <div class="col-md-12 bg-info mt-3">
            <h4 >Skills and Work Preferences 技能及工作意願</h4>
        </div>
        <div class="col-md-12 ">
            <div class="row">
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="baby_care" {{ ($biodata->baby_care === 'baby_care') ? 'checked' : ''; }}>
                    <label for="baby_care">Caring for Baby 照顧嬰兒</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="housework" {{ ($biodata->housework === 'housework') ? 'checked' : ''; }}>
                    <label for="housework">General Housework 一般家務</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="child_care" {{ ($biodata->child_care === 'child_care') ? 'checked' : ''; }}>
                    <label for="child_care">Caring for Child 照顧小孩</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="cooking" {{ ($biodata->cooking === 'cooking') ? 'checked' : ''; }}>
                    <label for="cooking">Cooking 煮食</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="elderly_care" {{ ($biodata->elderly_care === 'elderly_care') ? 'checked' : ''; }}>
                    <label for="elderly_care">Caring for Elderly Person 照顧老人</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="washing" {{ ($biodata->washing === 'washing') ? 'checked' : ''; }}>
                    <label for="washing">Car Washing 洗車</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="disabled_care" {{ ($biodata->disabled_care === 'disabled_care') ? 'checked' : ''; }}>
                    <label for="disabled_care">Caring for Disabled Person 照顧殘障人士</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="driving" {{ ($biodata->driving === 'driving') ? 'checked' : ''; }}>
                    <label for="driving">Driving 駕駛</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="bedridden_care" {{ ($biodata->bedridden_care === 'bedridden_care') ? 'checked' : ''; }}>
                    <label for="bedridden_care">Caring for Bedridden 照顧臥床病人</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="gardening" {{ ($biodata->gardening === 'gardening') ? 'checked' : ''; }}>
                    <label for="gardening">Gardening 園藝</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="pets_care" {{ ($biodata->pets_care === 'pets_care') ? 'checked' : ''; }}>
                    <label for="pets_care">Caring for Pet 照顧寵物</label>
                </div>
                <div class="col-md-6 mt-3">
                    <input type="checkbox" class="check-box mt-2" id="special_needs" {{ ($biodata->special_needs === 'special_needs') ? 'checked' : ''; }}>
                    <label for="special_needs">Special Needs 特殊需要</label>
                </div>
            </div>
        </div>

        <div class="col-md-12 bg-info mt-3">
            <h4 >Languages 語言</h4>
        </div>
        <div class="col-md-12 mt-3 mb-5">
            <div class="row">
                <div class="col-md-4 mt-3">
                    <p></p>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <p>Acceptable 基本</p>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <p>Average 平</p>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <p>Good 好</p>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <p>None 不能</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mt-3 ">
                    <p>Speaking in English 能説英語</p>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="acceptable" {{ ($biodata->english_level === 'acceptable') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="average" {{ ($biodata->english_level === 'average') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="good" {{ ($biodata->english_level === 'good') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="none" {{ ($biodata->english_level === 'none') ? 'checked' : ''; }}>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mt-3 ">
                    <p>Speaking in Cantonese 能説廣東話</p>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="acceptable" {{ ($biodata->cantonese_level === 'acceptable') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="average" {{ ($biodata->cantonese_level === 'average') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="good" {{ ($biodata->cantonese_level === 'good') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="none" {{ ($biodata->cantonese_level === 'none') ? 'checked' : ''; }}>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mt-3 ">
                    <p>Speaking in Mandarin 能説國語</p>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="acceptable" {{ ($biodata->mandarin_level === 'acceptable') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="average" {{ ($biodata->mandarin_level === 'average') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="good" {{ ($biodata->mandarin_level === 'good') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-2 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="none" {{ ($biodata->mandarin_level === 'none') ? 'checked' : ''; }}>
                </div>
            </div>
        </div>
        <div class="col-md-12 bg-info mt-5">
            <h4>Other Questions 其他問題</h4>
        </div>
        <div class="col-md-12 mt-3">
            <div class="row">
                <div class="col-md-6 mt-3">
                    <p></p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <p>Yes 願意</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <p>No 不願意</p>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 mt-3">
                    <p>Willing to take care of newly born baby 願意照顧新生嬰兒</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="yes" {{ ($biodata->newborn_care === 'yes') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="no" {{ ($biodata->newborn_care === 'no') ? 'checked' : ''; }}>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 mt-3">
                    <p>Willing to take care of elderly 願意照顧長者</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="yes" {{ ($biodata->elderly_care_willing === 'yes') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="no" {{ ($biodata->elderly_care_willing === 'no') ? 'checked' : ''; }}>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 mt-3">
                    <p>Willing to take care of disabled/bedridden 願意照顧病患 / 長期臥床人士</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="yes" {{ ($biodata->disabled_care_willing === 'yes') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="no" {{ ($biodata->disabled_care_willing === 'no') ? 'checked' : ''; }}>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 mt-3">
                    <p>Willing to share a room 願意與人同房</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="yes" {{ ($biodata->share_room === 'yes') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="no" {{ ($biodata->share_room === 'no') ? 'checked' : ''; }}>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 mt-3">
                    <p>Willing to accept the weekly day off assigned by employer 願意接受僱主指定之假日</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="yes" {{ ($biodata->accept_day_off === 'yes') ? 'checked' : ''; }}>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <input type="checkbox" class="check-box mt-2" id="no" {{ ($biodata->accept_day_off === 'no') ? 'checked' : ''; }}>
                </div>

            </div>
            <div class="row">
                <div class="col-md-5 mt-3">
                    <p class="d-inline w-25">Total Years of Work Experience 總工作年資: </p> <p class="d-inline border-bottom-2 w-75">{{$biodata->work_experience_years}}</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <p class="d-inline w-25">Year(s)年</p> <p class="d-inline border-bottom-2 w-75">{{$biodata->work_experience_years}}</p>
                </div>
                <div class="col-md-4 mt-3 text-center">
                    <p class="d-inline w-25">Month(s)月 </p> <p class="d-inline border-bottom-2 w-75">{{$biodata->work_experience_months}} </p>
                </div>

            </div>
        </div>
        <?php
        $location = explode(',', $biodata->location ?? '');
        $date_from = explode(',', $biodata->date_from  ?? '');
        $date_to = explode(',', $biodata->date_to  ?? '');
        $salary = explode(',', $biodata->salary  ?? '');
        $district = explode(',', $biodata->district  ?? '');
        $language = explode(',', $biodata->language  ?? '');
        $size = explode(',', $biodata->size  ?? '');
        $employer_nationality = explode(',', $biodata->employer_nationality  ?? '');
        $family_members = explode(',', $biodata->family_members  ?? '');
        $num_adults = explode(',', $biodata->num_adults  ?? '');
        $num_babies = explode(',', $biodata->num_babies  ?? '');
        $num_children = explode(',', $biodata->num_children  ?? '');
        $num_elderly = explode(',', $biodata->num_elderly  ?? '');
        $employer_babyCare = explode(',', $biodata->employer_babyCare  ?? '');
        $employer_childCare = explode(',', $biodata->employer_childCare  ?? '');
        $employer_elderlyCare = explode(',', $biodata->employer_elderlyCare  ?? '');
        $employer_housework = explode(',', $biodata->employer_housework  ?? '');
        $employer_cooking = explode(',', $biodata->employer_cooking  ?? '');
        $employer_washing = explode(',', $biodata->employer_washing  ?? '');
        $employer_disabledCare = explode(',', $biodata->employer_disabledCare  ?? '');
        $employer_driving = explode(',', $biodata->employer_driving  ?? '');
        $employer_bedriddenCare = explode(',', $biodata->employer_bedriddenCare  ?? '');
        $employer_gardening = explode(',', $biodata->employer_gardening  ?? '');
        $employer_petsCare = explode(',', $biodata->employer_petsCare  ?? '');
        $employer_specialneeds = explode(',', $biodata->employer_specialneeds  ?? '');
        $reason_for_leaving = explode(',', $biodata->reason_for_leaving  ?? '');
        $years_experience = explode(',', $biodata->years_experience  ?? '');
        $months_experience = explode(',', $biodata->months_experience  ?? '');
        $srNo=0;
        ?>
        <?php foreach ($location as $index => $locations):
            if (empty($locations)) {
                continue;
            }
            $srNo=$srNo + 1;
            ?>

        <div class="col-md-12 bg-info mt-3">
            <h4 >Domestic Helper Working Experience {{$srNo}} 傭工工作記錄</h4>
        </div>
        <div class="col-md-12 mt-3">
        <div class="row">
            <div class="col-md-3 mt-3">
                <p class="d-inline w-25">Location 地區: </p> <p class="d-inline border-bottom-2 w-75">{{$locations}}</p>
            </div>
            <div class="col-md-3 mt-3 text-center">
                <p class="d-inline w-25">Date From 由:</p> <p class="d-inline border-bottom-2 w-75">{{$date_from[$index]}}</p>
            </div>
            <div class="col-md-3 mt-3 text-center">
                <p class="d-inline w-25">To 至:</p> <p class="d-inline border-bottom-2 w-75">{{$date_to[$index]}}</p>
            </div>
            <div class="col-md-3 mt-3 text-center">
                <p class="d-inline w-25">Salary 薪金:</p> <p class="d-inline border-bottom-2 w-75">{{$salary[$index]}}</p>
            </div>
            <div class="col-md-6 mt-3">
                <p class="d-inline w-25">District 區域:</p> <p class="d-inline border-bottom-2 w-75">{{$district[$index]}}</p>
            </div>
            <div class="col-md-6 mt-3 text-center">
                <p class="d-inline w-25">Language you spoke with your employer 每日語言:</p> <p class="d-inline border-bottom-2 w-75">{{$language[$index]}}</p>
            </div>
            <div class="col-md-6 mt-3 ">
                <div class="row">
                    <div class="col-md-12 mt-5">
                        <p class="d-inline w-25">Size of Flat / House 房屋面積:</p> <p class="d-inline border-bottom-2 w-75">{{$size[$index]}}</p>
                    </div>
                    <div class="col-md-12 mt-5">

                        <p class="d-inline w-25">Employer Nationality 僱主國籍:</p> <p class="d-inline border-bottom-2 w-75">{{$employer_nationality[$index]}}</p>
                    </div>
                    <div class="col-md-12 mt-5">
                        <p class="d-inline w-25">Number of Family Members 家庭人數:</p> <p class="d-inline border-bottom-2 w-75">{{$family_members[$index]}}</p>
                    </div>
                    <div class="col-md-12 mt-5">
                        <p class="d-inline w-25">No. of Adults 成人:</p> <p class="d-inline border-bottom-2 w-75">{{$num_adults[$index]}}</p>
                    </div>
                    <div class="col-md-12 mt-5">
                        <p class="d-inline w-25">No. of Babies 嬰兒:</p> <p class="d-inline border-bottom-2 w-75">{{$num_babies[$index]}}</p>
                    </div>
                    <div class="col-md-12 mt-5">
                        <p class="d-inline w-25">No. of Children 小孩:</p> <p class="d-inline border-bottom-2 w-75">{{$num_children[$index]}}</p>
                    </div>
                    <div class="col-md-12 mt-5">
                        <p class="d-inline w-25">No. of Elderly 長者:</p> <p class="d-inline border-bottom-2 w-75">{{$num_elderly[$index]}}</p>
                    </div>
                </div>

            </div>
            <div class="col-md-6 mt-3" style="border: 2px solid black !important;">
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h6>Main Duties 主要職責</h6>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_babyCare" {{ ($employer_babyCare[$index] === 'babyCare') ? 'checked' : ''; }}>
                        <label for="employer_babyCare" style="    font-size: 12px;">Caring for Baby 照顧嬰兒</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_housework" {{ ($employer_housework[$index] === 'housework') ? 'checked' : ''; }}>
                        <label for="employer_housework" style="    font-size: 12px;">General Housework 一般家務</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_childCare" {{ ($employer_childCare[$index] === 'childCare') ? 'checked' : ''; }}>
                        <label for="employer_childCare" style="    font-size: 12px;">Caring for Child 照顧小孩</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_cooking" {{ ($employer_cooking[$index] === 'cooking') ? 'checked' : ''; }}>
                        <label for="employer_cooking" style="    font-size: 12px;">Cooking 煮食</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_elderlyCare" {{ ($employer_elderlyCare[$index] === 'elderlyCare') ? 'checked' : ''; }}>
                        <label for="employer_elderlyCare" style="    font-size: 12px;">Caring for Elderly Person 照顧老人</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_washing" {{ ($employer_washing[$index] === 'washing') ? 'checked' : ''; }}>
                        <label for="employer_washing" style="    font-size: 12px;">Car Washing 洗車</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_disabledCare" {{ ($employer_disabledCare[$index] === 'disabledCare') ? 'checked' : ''; }}>
                        <label for="employer_disabledCare" style="    font-size: 12px;">Caring for Disabled Person 照顧殘障人士</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_driving" {{ ($employer_driving[$index] === 'driving') ? 'checked' : ''; }}>
                        <label for="employer_driving" style="    font-size: 12px;">Driving 駕駛</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_bedriddenCare" {{ ($employer_bedriddenCare[$index] === 'bedriddenCare') ? 'checked' : ''; }}>
                        <label for="employer_bedriddenCare" style="    font-size: 12px;">Caring for Bedridden 照顧臥床病人</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_gardening" {{ ($employer_gardening[$index] === 'gardening') ? 'checked' : ''; }}>
                        <label for="employer_gardening" style="    font-size: 12px;">Gardening 園藝</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_petsCare" {{ ($employer_petsCare[$index] === 'petsCare') ? 'checked' : ''; }}>
                        <label for="employer_petsCare" style="    font-size: 12px;">Caring for Pet 照顧寵物</label>
                    </div>
                    <div class="col-md-6 mt-3">
                        <input type="checkbox" class="check-box mt-2" id="employer_specialneeds" {{ ($employer_specialneeds[$index] === 'specialneeds') ? 'checked' : ''; }}>
                        <label for="employer_specialneeds" style="    font-size: 12px;">Special Needs 特殊需要</label>
                    </div>
                </div>
            </div>

                <div class="col-md-5 mt-3">
                    <p class="d-inline w-25">Reason for Leaving 離職原因: </p> <p class="d-inline border-bottom-2 w-75">{{$reason_for_leaving[$index]}}</p>
                </div>
                <div class="col-md-3 mt-3 text-center">
                    <p class="d-inline w-25">Year(s)年</p> <p class="d-inline border-bottom-2 w-75">{{$years_experience[$index]}}</p>
                </div>
                <div class="col-md-4 mt-3 text-center">
                    <p class="d-inline w-25">Month(s)月 </p> <p class="d-inline border-bottom-2 w-75">{{$months_experience[$index]}}</p>
                </div>
        </div>
    </div>
        <?php endforeach; ?>
    </div>
</div>
  </div>

</body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
<script>
    $(document).ready(function() {
        var printContents = document.getElementById('printableArea').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();
    });
    document.body.innerHTML = originalContents;
</script>
