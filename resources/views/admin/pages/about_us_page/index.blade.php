@extends('admin.layouts.app')
@section('title','About-us Landing Page')
@section('content')

<style type="text/css">
  .cke_notifications_area{ display: none !important; }
</style>

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">About Us Landing Page</h3>
          </div>
        </div>
      </div>
        @php
        @$homePage = App\Models\AboutUsLandingPage::find(1);
        @endphp
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('save-about-us-page')}}" class="form-validate" novalidate="novalidate">
            @csrf
            <div class="row g-gs">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="first_heading">About us Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="first_heading" class="form-control @error('first_heading') is-invalid @enderror" id="first_heading"  required="" value="{{@$homePage->first_heading}}" placeholder="Heading">
                    @error('first_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12" >
                <div class="form-group">
                  <label class="form-label" for="fv-message">Description</label>
                  <div class="form-control-wrap">
                    <textarea id="letter_format" class="form-control form-control-sm"  name="description">{{@$homePage->description}}</textarea>
                  </div>
                </div>
              </div>

               <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="second_heading">Second  Heading</label>
                  <div class="form-control-wrap">
                    <input type="text" name="second_heading" class="form-control @error('second_heading') is-invalid @enderror" id="second_heading"  required="" value="{{@$homePage->second_heading}}" placeholder="Second Heading">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="text_line">Text Line</label>
                  <div class="form-control-wrap">
                    <input type="text" name="text_line" class="form-control @error('text_line') is-invalid @enderror" id="text_line"  required="" value="{{@$homePage->text_line}}" placeholder="Second Heading">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="button_name">Button Name</label>
                  <div class="form-control-wrap">
                    <input type="text" name="button_name" class="form-control @error('button_name') is-invalid @enderror" id="button_name"  required="" value="{{@$homePage->button_name}}" placeholder="Button Name">
                    @error('second_heading')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@push('js')

<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'letter_format', {
        filebrowserUploadMethod: 'form'
    });
    
</script>
@endpush