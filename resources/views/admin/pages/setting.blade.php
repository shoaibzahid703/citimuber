@extends('admin.layouts.app')
@section('title','Site Setting')
@section('content')
    <style>
        .tagify{
            padding: 0.4375rem 1rem;
            color: #3c4d62;
            border: 1px solid #dbdfea !important;
        }
    </style>
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"> Site Settings</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered">
                    <div class="card-inner">
                        <form method="post" action="{{route('site_settings.update')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-md-12 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="site_description">Site Description</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('site_description') is-invalid @enderror" id="site_description" name="site_description" placeholder="site description"
                                                   value="{{old('site_description') ? old('site_description') : $setting->site_description ?? ''}}">
                                            @error('site_description')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="seo_tags">Seo Tags</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="w-100 @error('seo_tags') is-invalid @enderror" id="seo_tags" name="seo_tags" placeholder="seo tags"
                                                   value="{{old('seo_tags') ? old('seo_tags') : $setting->seo_tags ?? ''}}" >
                                            @error('seo_tags')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="site_logo">Site Logo</label>
                                        <div class="form-control-wrap">
                                            <input type="file" class="form-control @error('site_logo') is-invalid @enderror"
                                                   id="site_logo" name="site_logo" accept="image/*"  >
                                            @error('site_logo')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="site_fav_icon">Site Favicon</label>
                                        <div class="form-control-wrap">
                                            <input type="file" class="form-control @error('site_fav_icon') is-invalid @enderror"
                                                   id="site_fav_icon" name="site_fav_icon" accept="image/*"  >
                                            @error('site_fav_icon')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="facebook_profile">Facebook Profile</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('facebook_profile') is-invalid @enderror"
                                                   id="facebook_profile" name="facebook_profile" placeholder="facebook profile"
                                                   value="{{old('facebook_profile') ? old('facebook_profile') : $setting->facebook_profile ?? ''}}">
                                            @error('facebook_profile')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="twitter_profile">Twitter Profile</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('twitter_profile') is-invalid @enderror"
                                                   id="twitter_profile" name="twitter_profile" placeholder="twitter profile"
                                                   value="{{old('twitter_profile') ? old('twitter_profile') : $setting->twitter_profile ?? ''}}">
                                            @error('twitter_profile')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="tiktok_profile">Tiktok Profile</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('tiktok_profile') is-invalid @enderror"
                                                   id="tiktok_profile" name="tiktok_profile" placeholder="tiktok profile"
                                                   value="{{old('tiktok_profile') ? old('tiktok_profile') : $setting->tiktok_profile ?? ''}}">
                                            @error('tiktok_profile')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="pinterest_profile">Pinterest Profile</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('pinterest_profile') is-invalid @enderror"
                                                   id="pinterest_profile" name="pinterest_profile" placeholder="pinterest profile"
                                                   value="{{old('pinterest_profile') ? old('pinterest_profile') : $setting->pinterest_profile ?? ''}}">
                                            @error('pinterest_profile')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="instagram_profile">Instagram Profile</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('instagram_profile') is-invalid @enderror"
                                                   id="instagram_profile" name="instagram_profile" placeholder="instagram profile"
                                                   value="{{old('instagram_profile') ? old('instagram_profile') : $setting->instagram_profile ?? ''}}">
                                            @error('instagram_profile')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="youtube_profile">Youtube Profile</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('youtube_profile') is-invalid @enderror"
                                                   id="youtube_profile" name="youtube_profile" placeholder="youtube profile"
                                                   value="{{old('youtube_profile') ? old('youtube_profile') : $setting->youtube_profile ?? ''}}">
                                            @error('youtube_profile')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                                    <div class="form-group">
                                        <label class="form-label" for="linkedin_profile">Linkedin Profile</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control @error('linkedin_profile') is-invalid @enderror"
                                                   id="linkedin_profile" name="linkedin_profile" placeholder="linkedin profile"
                                                   value="{{old('linkedin_profile') ? old('linkedin_profile') : $setting->linkedin_profile ?? ''}}" >
                                            @error('linkedin_profile')
                                            <div class="invalid-feedback">{{ $message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <link href="https://cdn.jsdelivr.net/npm/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.jsdelivr.net/npm/@yaireo/tagify"></script>
    <script src="https://cdn.jsdelivr.net/npm/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
    <script>
        var inputElm = document.querySelector('input[name=seo_tags]');
        var tagify = new Tagify(inputElm,{
            originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(',')
        })
    </script>
@endpush
