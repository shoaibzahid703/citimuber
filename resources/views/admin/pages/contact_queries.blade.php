@extends('admin.layouts.app')
@section('title','Contact Queries')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Contact Queries</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner">
                        <table class="table datatable-init" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Subject</th>
                                <th>Message</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($queries as $query)
                                <tr>
                                    <td>
                                        {{$query->name}}
                                    </td>
                                    <td>
                                        {{$query->email}}
                                    </td>
                                    <td>
                                        {{$query->subject}}
                                    </td>
                                    <td>
                                        {{$query->message}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push("js")

@endpush
