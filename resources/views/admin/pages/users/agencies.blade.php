@extends('admin.layouts.app')
@section('title','All Agencies')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Agencies</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
{{--                        <a href=" {{route('add_employer')}}" class="btn btn-primary mb-2 ">Add Agency</a>--}}
                        <div class="card-datatable table-responsive">
                            <table id="employers" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email Address</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")

    <script type="text/javascript">
        employers_table =    $("#employers").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('all_agencies')}}',
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                { data: 'status', name: 'status' },
                // { data: 'employer_category_name', name: 'employer_category.name' },
                // { data: 'jobs_count', name: 'jobs_count' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>

@endpush
