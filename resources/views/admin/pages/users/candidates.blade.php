@extends('admin.layouts.app')
@section('title','All Candidates')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Candidates</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <a href=" {{route('add_candidate')}}" class="btn btn-primary mb-2 ">Add Candidate</a>
                        <div class="card-datatable table-responsive">

                            <table id="zero" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email Address</th>
                                    <th>Status</th>
                                    <th>Helper Category</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")

    <script type="text/javascript">
        expense_table =    $("#zero").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('all_candidates')}}',
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                { data: 'status', name: 'status' },
                { data: 'helper_category_name', name: 'helper_category.name' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>

@endpush
