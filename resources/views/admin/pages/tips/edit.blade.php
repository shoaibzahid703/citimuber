@extends('admin.layouts.app')
@section('title','Edit Tips')
@section('content')

<style type="text/css">
	.cke_notifications_area{ display: none !important; }
</style>

<div class="container-fluid">
  <div class="nk-content-inner">
    <div class="nk-content-body">
      <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">Edit Tips</h3>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-inner">
          <form method="post" action="{{route('update-tips', $tips)}}" class="form-validate" novalidate="novalidate" enctype="multipart/form-data">
            @csrf
            <div class="row g-gs">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="title">Title</label>
                  <div class="form-control-wrap">
                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title"  required="" placeholder="Title" value="{{$tips->title}}">
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="tips_date">Tips Date</label>
                  <div class="form-control-wrap">
                    <input type="date" name="tips_date" class="form-control @error('tips_date') is-invalid @enderror" id="tips_date"  required="" placeholder="Tips Date"  value="{{$tips->tips_date}}">
                    @error('tips_date')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="category_id">Category</label>
                    <div class="form-control-wrap">
                        <select name="category_id" id="category_id" class="form-control  @error('category_id') is-invalid @enderror">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if($category->id == $tips->category_id) selected @endif>{{$category->name}}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                 </div>
            </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="tips_image">News Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="tips_image" class="form-control @error('tips_image') is-invalid @enderror" id="tips_image">
                    @error('tips_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  @if($tips->tips_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/tips_image')}}/{{$tips->tips_image}}" alt="" width="20%" height="100px">
                  @endif
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="location_image">Location Image</label>
                  <div class="form-control-wrap">
                    <input type="file" name="location_image" class="form-control @error('location_image') is-invalid @enderror" id="location_image">
                    @error('location_image')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  @if($tips->location_image != NULL)
                  <img class="rounded-top mt-3" src="{{asset('storage/tips_image')}}/{{$tips->location_image}}" alt="" width="20%" height="100px">
                  @endif
                </div>
              </div>
              
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="title">Facebook Link</label>
                  <div class="form-control-wrap">
                    <input type="text" name="facebook_url" class="form-control @error('facebook_url') is-invalid @enderror" id="facebook_url" value="{{$tips->facebook_url}}"  required="" placeholder="Facebook Link">
                    @error('facebook_url')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="title">Twitter Link</label>
                  <div class="form-control-wrap">
                    <input type="text" name="twitter_url" class="form-control @error('twitter_url') is-invalid @enderror" id="twitter_url" value="{{$tips->twitter_url}}" required="" placeholder="Twitter Link">
                    @error('twitter_url')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="title">Whatsapp Link</label>
                  <div class="form-control-wrap">
                    <input type="text" name="whatsapp_url" class="form-control @error('whatsapp_url') is-invalid @enderror" id="whatsapp_url" value="{{$tips->whatsapp_url}}"  required="" placeholder="Whatsapp Link">
                    @error('whatsapp_url')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label" for="title">LinkedIn Link</label>
                  <div class="form-control-wrap">
                    <input type="text" name="linkedIn_url" class="form-control @error('linkedIn_url') is-invalid @enderror" id="linkedIn_url" value="{{$tips->linkedIn_url}}" required="" placeholder="LinkedIn Link">
                    @error('linkedIn_url')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label" for="full_details">Full Details</label>
                  <div class="form-control-wrap">
                    <textarea id="letter_format" class="form-control @error('full_details') is-invalid @enderror" name="full_details" required>{{$tips->full_details}}</textarea>
                    @error('full_details')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group mt-4 pt-1">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')

<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'letter_format', {
        filebrowserUploadMethod: 'form'
    });
    
</script>
@endpush