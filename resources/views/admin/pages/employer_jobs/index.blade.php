@extends('admin.layouts.app')
@section('title','All Jobs')
@section('content')

    <div class="container-fluid mt-4">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Jobs</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                    <form id="searchForm">
                        <div class="row">


                            <div class="col-md-4 mt-3">
                                <label for="user_id">Employer Name</label>
                                <select class="form-control" id="user_id" name="user_id">
                                    <option value="">Select Option</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="job_title">Job Title</label>
                                <input type="text" name="job_title" id="job_title" class="form-control" placeholder="Job Title*">
                            </div>
                            @php
                                $countries = App\Models\Country::all();
                            @endphp
                            <div class="col-md-4 mt-3">
                                <label for="offer_location">Country</label>
                                <select class="form-control" id="offer_location" name="offer_location">
                                    <option value="">Select Option</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4 mt-3">
                                <label for="position_offered">Position Offered</label>
                                <select class="form-control" id="position_offered" name="position_offered">
                                    <option value="">Select Option</option>
                                    <option value="Domestic Helper">Domestic Helper</option>
                                    <option value="Driver">Driver</option>
                                </select>
                            </div>
                            <div class="col-md-4 mt-3">
                                <button type="button" class="btn btn-success mt-4" id="searchButton">Search</button>
                                <button type="button" class="btn btn-danger mt-4" id="resetButton">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <div class="card-datatable table-responsive">
                            <table id="all_jobs" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Employer</th>
                                    <th>Title</th>
                                    <th>Country</th>
                                    <th>Employer type</th>
                                    <th>Position Offered</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="{{ asset('admin/datatable/datatables.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var languages_table = $("#all_jobs").DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: '{{route('all_jobs')}}',
                    data: function(d) {
                        d.user_id = $('#user_id').val();
                        d.job_title = $('#job_title').val();
                        d.offer_location = $('#offer_location').val();
                        d.position_offered = $('#position_offered').val();
                    }
                },
                columns: [
                    {data: 'user_id', name: 'user_id'},
                    {data: 'job_title', name: 'job_title', render: function (data, type, row) { return data.charAt(0).toUpperCase() + data.slice(1); }},
                    {data: 'offer_location', name: 'offer_location'},
                    {data: 'employer_type', name: 'employer_type', render: function (data, type, row) { return data.charAt(0).toUpperCase() + data.slice(1); }},
                    {data: 'position_offered', name: 'position_offered', render: function (data, type, row) { return data.charAt(0).toUpperCase() + data.slice(1); }},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            $('#searchButton').click(function() {
                languages_table.draw();
            });

            $('#resetButton').click(function() {
                $('#searchForm')[0].reset();
                languages_table.draw();
            });
        });
    </script>
@endpush
