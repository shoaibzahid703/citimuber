@extends('admin.layouts.app')
@section('title','Employer Bio Data')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Employer Bio Data</h3>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <table class="table datatable-init" >
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Ref Number</th>
                                <th>Education</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($boidata as $boidatas)
                                <tr>
                                    <td>
                                        {{$boidatas->name}}
                                    </td>
                                    <td>
                                        {{$boidatas->age}}
                                    </td>
                                    <td>
                                        {{$boidatas->ref_number}}
                                    </td>
                                    <td>
                                        {{$boidatas->education}}
                                    </td>
                                    <td>
                                        @if($boidatas->status == 'active')
                                            <span class="badge bg-success">{{$boidatas->status}}</span>
                                        @else
                                            <span class="badge bg-danger">{{$boidatas->status}}</span>
                                        @endif
                                    </td>
                                    <td>
{{--                                        @if($boidatas->status == 'active')--}}
{{--                                            <a href="javascript:void(0)" class="btn btn-warning btn-sm mr-2"--}}
{{--                                               data-toggle="tooltip"--}}
{{--                                               data-placement="bottom"--}}
{{--                                               title="Block this user"--}}
{{--                                               onclick="userStatus('.$boidatas->id.', 0)">--}}
{{--                                                Inactive--}}
{{--                                            </a>--}}
{{--                                        @else--}}
{{--                                            <a href="javascript:void(0)" class="btn btn-success btn-sm mr-2"--}}
{{--                                               data-toggle="tooltip"--}}
{{--                                               data-placement="bottom"--}}
{{--                                               title="Unblock this user"--}}
{{--                                               onclick="userStatus('.$boidatas->id.', 1)">--}}
{{--                                                Active--}}
{{--                                            </a>--}}
{{--                                        @endif--}}
{{--                                        <a href="javascript:void(0)" data-toggle="tooltip" data-id="'.$boidatas->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer ">Delete</a>--}}
                                        <a href="{{ route('employment_bio_data_pdf', $boidatas->id) }}" target="_blank"  class="btn btn-secondary btn-sm">Download as Pdf</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push("js")

@endpush
