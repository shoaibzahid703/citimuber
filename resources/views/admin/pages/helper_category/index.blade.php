@extends('admin.layouts.app')
@section('title','All Helper Category')
@section('content')
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">All Helper Category</h3>
                        </div><!-- .nk-block-head-content -->
                        <a href="{{route('helper_category_create')}}" class="btn btn-primary">Add</a>
                    </div><!-- .nk-block-between -->
                </div>
                <div class="card card-bordered ">
                    <div class="card-inner" id="row">
                        <div class="card-datatable table-responsive">
                            <table id="helper_categories" class="table table-hover" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Employer Apply Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push("js")

    <script type="text/javascript">
        hlper_categories_table =    $("#helper_categories").DataTable({
            serverSide: true,
            processing: true,
            "ajax": {
                "url": '{{route('helper_categories')}}',
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'employer_apply_status', name: 'employer_apply_status'},
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    </script>

@endpush
