@extends('admin.layouts.app')
@section('title','Edit Helper Category')
@section('content')

    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Edit Helper Category</h3>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-inner">
                        <form method="post" action="{{route('helper_category_update',$category)}}" class="form-validate" novalidate="novalidate">
                            @csrf
                            <div class="row g-gs">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name"> Name</label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"  value="{{$category->name}}" placeholder="Category name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-check mt-4">
                                        <input type="checkbox" class="form-check-input" id="employer_apply" name="employer_apply" @if($category->employer_apply_status == \App\Models\HelperCategory::Yes) checked @endif value="1">
                                        <label class="form-check-label" for="employer_apply">Employer Apply</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group mt-4 pt-1">
                                        <button type="submit" class="btn btn-md btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
