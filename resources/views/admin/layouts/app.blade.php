<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />


    @if(isset($settings->site_description))
        <meta name="description" content="{{$settings->site_description}}">
    @endif
    @if(isset($settings->seo_tags))
        <meta name="keywords" content="{{$settings->seo_tags}}">
    @endif

    <!-- Page Title  -->
    <title>@yield('title')</title>

    @if(isset($settings->site_fav_icon))
        <link href="{{asset('storage/site_fav_icon/'.$settings->site_fav_icon)}}" rel="shortcut icon" type="image/png">
    @else
        <link href="{{asset('cdn-sub/front-app/favicon.jpg')}}" rel="shortcut icon" type="image/png">
    @endif


    <link rel="stylesheet" href="{{asset('admin/css/dashboard.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/sweetalert.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/datatable/custom_dt_html5.css')}}">
    <link rel="stylesheet" href="{{asset('admin/vendor/libs/toastr/toastr.css')}}">


    @stack('css')
</head>

<body class="nk-body bg-lighter npc-default has-sidebar ">
<div class="nk-app-root">
    <!-- main @s -->
    <div class="nk-main ">
        @include('admin.includes.sidebar')
        <!-- wrap @s -->
        <div class="nk-wrap ">
            @include('admin.includes.header')
            <!-- content @s -->
            <div class="nk-content ">
                <br>
                @yield('content')
            </div>
            <!-- content @e -->
            @include('admin.includes.footer')

        </div>
        <!-- wrap @e -->
    </div>
    <!-- main @e -->
</div>
<!-- JavaScript -->

<script src="{{asset('admin/js/bundle.js')}}"></script>
<script src="{{asset('admin/js/scripts.js')}}"></script>
<script src="{{asset('admin/js/sweetalert.js')}}"></script>
<script src="{{asset('admin/vendor/libs/toastr/toastr.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.js"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ asset('admin/datatable/datatables.js')}}"></script>

@include('admin.includes.messages')

@stack('js')
</body>

</html>
