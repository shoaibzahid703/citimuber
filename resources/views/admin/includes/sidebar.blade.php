<!-- sidebar @s -->
<div class="nk-sidebar nk-sidebar-fixed is-light " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="{{url('/')}}" class="logo-link nk-sidebar-logo">
                @if(isset($settings->site_logo))
                    <img  src="{{asset('storage/site_logo/'.$settings->site_logo)}}"   class="logo-dark logo-img" >
                @else
                    <img class="logo-dark logo-img" src="{{asset('assets/admin/images/zina-logo.png')}}"   alt="logo-dark">
                @endif
            </a>
        </div>
        <div class="nk-menu-trigger me-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
            <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">
                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Bio Data</h6>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                        <a href="{{route('admin_dashboard')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-dashboard-fill"></em></span>
                            <span class="nk-menu-text">Dashboard</span>
                        </a>
                    </li>
{{--                    <li class="nk-menu-item">--}}
{{--                        <a href="{{route('admin_employer_bio_data')}}" class="nk-menu-link">--}}
{{--                            <span class="nk-menu-icon"><em class="icon ni ni-repeat-fill"></em></span>--}}
{{--                            <span class="nk-menu-text">Bio Data</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}




                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-users-fill"></em>
                            </span>
                            <span class="nk-menu-text">Users</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">

                            <li class="nk-menu-item">
                                <a href="{{route('all_candidates')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Candidates</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('all_employers')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Employer</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('all_agencies')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Agency</span>
                                </a>
                            </li>


                        </ul><!-- .nk-menu-sub -->
                    </li>


                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-sign-dollar"></em>
                            </span>
                            <span class="nk-menu-text">Plans</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('plans')}}" class="nk-menu-link">
                                    <span class="nk-menu-text">Employer Plans</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('helper_plans')}}" class="nk-menu-link">
                                    <span class="nk-menu-text">Helper Plans</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('agency_plans')}}" class="nk-menu-link">
                                    <span class="nk-menu-text">Agency Plans</span>
                                </a>
                            </li>
                        </ul><!-- .nk-menu-sub -->
                    </li>


                    <li class="nk-menu-item">
                        <a href="{{route('employer_categories')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-bar-c"></em></span>
                            <span class="nk-menu-text">Employer Category</span>
                        </a>
                    </li>


                    <li class="nk-menu-item">
                        <a href="{{route('helper_categories')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-bar-c"></em></span>
                            <span class="nk-menu-text">Helper Category</span>
                        </a>
                    </li>


                    <li class="nk-menu-item">
                        <a href="{{route('agency_strength')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-note-add"></em></span>
                            <span class="nk-menu-text">Agency Strength</span>
                        </a>
                    </li>



                    <li class="nk-menu-item">
                        <a href="{{route('agency_experts')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-check"></em></span>
                            <span class="nk-menu-text">Agency Expert</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('agency_services')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-setting-alt"></em></span>
                            <span class="nk-menu-text">Agency Services</span>
                        </a>
                    </li>


                    <li class="nk-menu-item">
                        <a href="{{route('all_transactions')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-money"></em></span>
                            <span class="nk-menu-text">Transactions</span>
                        </a>
                    </li>

                       <li class="nk-menu-item">
                        <a href="{{route('admin_messages')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-chat"></em></span>
                            <span class="nk-menu-text">Messages</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('job_pictures')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-camera"></em></span>
                            <span class="nk-menu-text">Job Pictures</span>
                        </a>
                    </li>
                  <li class="nk-menu-item">
                        <a href="{{route('all_jobs')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-layers-fill"></em></span>
                            <span class="nk-menu-text">All Jobs</span>
                        </a>
                    </li>

                    <li class="nk-menu-item">
                        <a href="{{route('countries')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-map-pin"></em></span>
                            <span class="nk-menu-text">Country</span>
                        </a>
                    </li>


                    <li class="nk-menu-item">
                        <a href="{{route('states')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-location"></em></span>
                            <span class="nk-menu-text">State</span>
                        </a>
                    </li>


                    <li class="nk-menu-item">
                        <a href="{{route('cities')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-map"></em></span>
                            <span class="nk-menu-text">City</span>
                        </a>
                    </li>


                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-setting-alt"></em>
                            </span>
                            <span class="nk-menu-text">Settings</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">

                            <li class="nk-menu-item">
                                <a href="{{route('languages')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Languages</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('main_skills')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Main skills</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('cooking_skills')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Cooking skills</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('other_skills')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Other skills</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('personality')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Personality</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('currency')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Currency</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('duties')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Job Duty</span>
                                </a>
                            </li>


                            <li class="nk-menu-item">
                                <a href="{{route('site_settings')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Site Setting</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('contact_us_settings')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Contact Us Setting</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('payment-method')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Payment Method</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('job-position')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Job Position</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('education')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Education</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('religions')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Religions</span>
                                </a>
                            </li>


                            <li class="nk-menu-item">
                                <a href="{{route('education_levels')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Education Levels</span>
                                </a>
                            </li>


                        </ul><!-- .nk-menu-sub -->
                    </li>

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-home"></em>
                            </span>
                            <span class="nk-menu-text">Home Page Setting</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('first-section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">First Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('second-section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Second Section</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('third-section')}}" class="nk-menu-link">
                                    <span class="nk-menu-text">Third Section</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('fourth-section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Fourth Section</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('five-section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Five Section</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('six-section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Six Section</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('seven-section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Seven Section</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('eight_section')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Eight Section</span>
                                </a>
                            </li>


                        </ul><!-- .nk-menu-sub -->
                    </li>



                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle" data-bs-original-title="" title="">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-package-fill"></em>
                            </span>
                            <span class="nk-menu-text">Landing Pages Setting</span>
                        </a>
                        <ul class="nk-menu-sub" style="display: none;">
                            <li class="nk-menu-item">
                                <a href="{{route('candidates-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Candidate Page</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('partner-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Partner Page</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('jobs-page')}}" class="nk-menu-link">
                                    <span class="nk-menu-text">Jobs Page</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('agencies-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Agency Services Page</span>
                                </a>
                            </li>

                            <!-- <li class="nk-menu-item">
                                <a href="{{route('training-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Training Landing Page</span>
                                </a>
                            </li> -->

                            <!-- <li class="nk-menu-item">
                                <a href="{{route('association-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Association Landing Page</span>
                                </a>
                            </li>


 -->
                            <li class="nk-menu-item">
                                <a href="{{route('categories')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Tips & News Category</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('news_page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">News Page</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('tips_page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Tips Page</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('about_us_page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">About Us Page</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('happy-employees')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Happy Employees Page</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('happy-helpers')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Happy Helpers Page</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('faqs-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Faq's Page</span>
                                </a>
                            </li>
                            <li class="nk-menu-item">
                                <a href="{{route('term-and-condition')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Terms & Condition Page</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('privacy-policy')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Privacy Policy Page</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('contact-us')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Contact Us Page</span>
                                </a>
                            </li>
                          <li class="nk-menu-item">
                                <a href="{{route('events')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Events</span>
                                </a>
                            </li>

                             <li class="nk-menu-item">
                                <a href="{{route('holiday_page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Public Holidays</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('employer-dashboard-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Employer Dashboard</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('candidate-dashboard-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Candidate Dashboard</span>
                                </a>
                            </li>

                            <li class="nk-menu-item">
                                <a href="{{route('candidate-detail-page')}}" class="nk-menu-link" >
                                    <span class="nk-menu-text">Detail Page Bg-image</span>
                                </a>
                            </li>

                        </ul>
                    </li>


                </ul>
            </div>
        </div>
    </div>
</div>
<!-- sidebar @e -->
