@extends('dashboard.layouts.app')
@section('title','EMPLOYMENT BIO DATA')
@section('content')
    <style>
        .post-resume {
            margin-top: 70px;
        }
    </style>
    <section class="agency news-section pb-5 custom-container page-section blog-ar">
        <div class="container">
            <div class="" style="margin-top: 140px !important;">
                <div class="row">
                    <div class="col-12">
                        <div class="top-banner-wrapper ng-star-inserted">
                            <div class="top-banner-content small-section">
                                <h1>Employment Bio Data</h1>
                                <p class="header_2">
                                    A lot of employers are ready to hire you. Choose your region and get full access to all our job offers. With more than 160,000 users and ethical values, we support you to find a better job and connect with
                                    our Employers. HelperPlace is totally free for all our job seekers, no placement fees and no salary deduction!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                .step {
                    display: none;
                }
                .step.active {
                    display: block;
                }
            </style>
            <div class="row mt-4">
                <div class="col-12">

                    <div class="container mt-5">
                        @if($message = Session::get("success"))
                            <div class="alert alert-success w-100">{{ $message }}</div>
                        @endif
                        <form id="multiStepForm" action="{{ route('employment_biodata.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="step active">
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <h3>Profile</h3>
                                    </div>

                                    <div class="col-md-12 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="file" class="form-control" name="profile_image" id="profileImage" />
                                            <label for="profileImage">Profile Image</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="text" class="form-control" name="cnahk" id="cnahk" placeholder="CNAHK | CNOPH | CNOOC | CRCF" />
                                            <label for="cnahk">CNAHK | CNOPH | CNOOC | CRCF:</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="text" class="form-control" name="phone_no" id="phone_no" placeholder="Phone No #" />
                                            <label for="phone_no">Phone No #</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Email Address"/>
                                            <label for="email">Email Address</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="date" class="form-control" name="release_date" id="releaseDate" placeholder="Release Date" />
                                            <label for="releaseDate">Release Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="date" class="form-control" name="expiry_date" id="expiryDate" placeholder="Expiry Date" />
                                            <label for="expiryDate">Expiry Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="date" class="form-control" name="apply_date" id="applyDate" placeholder="Apply Date" />
                                            <label for="applyDate">Apply Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="text" class="form-control" name="ref_number" id="refNumber" placeholder="Reference Number" />
                                            <label for="refNumber">Ref #</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="text" class="form-control" name="address" id="address" placeholder="Address" />
                                            <label for="address">Address</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input type="text" class="form-control" name="state" id="state" placeholder="State" />
                                            <label for="state">State</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <h3>Personal Particular</h3>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="text" name="name" id="name" placeholder="Name" />
                                            <label for="name">Name (姓名)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="number" name="age" id="age" placeholder="Age" />
                                            <label for="age">Age (年齡)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="date" name="dob" id="dob" placeholder="Date of Birth" />
                                            <label for="dob">Date of Birth (出生日期)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="text" name="education" id="education" placeholder="Education" />
                                            <label for="education">Education (教育水平)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="text" name="personal_nationality" id="personal_nationality" placeholder="Nationality" />
                                            <label for="personal_nationality">Nationality (國籍)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="text" name="id_number" id="idNumber" placeholder="ID Number" />
                                            <label for="idNumber">Hong Kong ID/Blue Card (證件號碼)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="text" name="religion" id="religion" placeholder="Religion" />
                                            <label for="religion">Religion (宗教)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="text" name="birthplace" id="birthplace" placeholder="Birthplace" />
                                            <label for="birthplace">Place of Birth (出生地點)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="text" name="height" id="height" placeholder="Height" />
                                            <label for="height">Height (身高) in CM</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="number" name="weight" id="weight" placeholder="Weight" />
                                            <label for="weight">Weight (體重) in KG</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="form-floating form-floating-outline">
                                            <input class="form-control" type="number" name="no_children" id="no_children" placeholder="No. of Children" />
                                            <label for="no_children">No. of Children</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3 d-inline-flex">
                                        <label style="margin-right: 15px;">Marital Status</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="marital_status" value="single" id="single" />
                                            <label class="form-check-label" for="single" style="margin-right: 15px;">Single</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="marital_status" value="married" id="married" />
                                            <label class="form-check-label" for="married" style="margin-right: 15px;">Married</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="marital_status" value="separated" id="separated" />
                                            <label class="form-check-label" for="separated" style="margin-right: 15px;">Separated</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Other Professional Courses (其他專業課程)</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="nursing" value="nursing" id="nursing" />
                                                <label class="form-check-label" for="nursing">Bachelor in Nursing (護士學位)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="midwifery" value="midwifery" id="midwifery" />
                                                <label class="form-check-label" for="midwifery">Midwifery Course Degree (助護學位)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="education_course" value="education_course" id="educationCourse" />
                                                <label class="form-check-label" for="educationCourse">Bachelor in Education (教育學位)</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="caregiver_course" value="caregiver_course" id="caregiverCourse" />
                                                <label class="form-check-label" for="caregiverCourse">Caregiver Course Degree (護理課程)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="step">
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <h3>Skills and Work Preferences</h3>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Skills and Work Preferences (技能及工作意願)</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="baby_care" value="baby_care" id="babyCare" />
                                                    <label class="form-check-label" for="babyCare">Caring for Baby (照顧嬰兒)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="child_care" value="child_care" id="childCare" />
                                                    <label class="form-check-label" for="childCare">Caring for Child (照顧小孩)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="elderly_care" value="elderly_care" id="elderlyCare" />
                                                    <label class="form-check-label" for="elderlyCare">Caring for Elderly Person (照顧老人)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="housework" value="housework" id="housework" />
                                                    <label class="form-check-label" for="housework">General Housework (一般家務)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="cooking" value="cooking" id="cooking" />
                                                    <label class="form-check-label" for="cooking">Cooking (煮食)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="washing" value="washing" id="washing" />
                                                    <label class="form-check-label" for="washing">Car Washing (洗車)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="disabled_care" value="disabled_care" id="disabledCare" />
                                                    <label class="form-check-label" for="disabledCare">Caring for Disabled Person (照顧殘障人士)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="driving" value="driving" id="driving" />
                                                    <label class="form-check-label" for="driving">Driving (駕駛)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="bedridden_care" value="bedridden_care" id="bedriddenCare" />
                                                    <label class="form-check-label" for="bedriddenCare">Caring for Bedridden (照顧臥床病人)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="gardening" value="gardening" id="gardening" />
                                                    <label class="form-check-label" for="gardening">Gardening (園藝)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="pets_care" value="pets_care" id="petsCare" />
                                                    <label class="form-check-label" for="petsCare">Caring for Pets (照顧寵物)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="special_needs" value="special_needs" id="specialneeds" />
                                                    <label class="form-check-label" for="specialneeds">Special Needs (特殊需要)</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <h3>Languages (語言)</h3>
                                    </div>
                                    <div class="col-md-12 mt-3 text-center">
                                        <div class="form-row">
                                            <div class="col">
                                                <div></div>
                                            </div>
                                            <div class="col">
                                                <div>Acceptable 基本</div>
                                            </div>
                                            <div class="col">
                                                <div>Average 平</div>
                                            </div>
                                            <div class="col">
                                                <div>Good 好</div>
                                            </div>
                                            <div class="col">
                                                <div>None 不能</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3 text-center">
                                        <div class="form-row">
                                            <div class="col text-start">
                                                <div>Speaking in English 能說英語</div>
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="english_level" id="englishAcceptable" value="acceptable" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="english_level" id="englishAverage" value="average" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="english_level" id="englishGood" value="good" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="english_level" id="englishNone" value="none" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3 text-center">
                                        <div class="form-row">
                                            <div class="col text-start">
                                                <div>Speaking in Cantonese 能説廣東話</div>
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="cantonese_level" id="cantoneseAcceptable" value="acceptable" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="cantonese_level" id="cantoneseAverage" value="average" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="cantonese_level" id="cantoneseGood" value="good" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="cantonese_level" id="cantoneseNone" value="none" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3 text-center">
                                        <div class="form-row">
                                            <div class="col text-start">
                                                <div>Speaking in Mandarin 能説國語</div>
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="mandarin_level" id="mandarinAcceptable" value="acceptable" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="mandarin_level" id="mandarinAverage" value="average" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="mandarin_level" id="mandarinGood" value="good" />
                                            </div>
                                            <div class="col">
                                                <input class="form-check-input" type="radio" name="mandarin_level" id="mandarinNone" value="none" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <h3>Other Questions (其他問題)</h3>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="col-sm-6 offset-sm-6 mt-3 text-center">
                                            <div class="form-row">
                                                <div class="col">
                                                    <div>Yes 願意</div>
                                                </div>
                                                <div class="col">
                                                    <div>No 願意</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-6 col-form-label">Willing to take care of newly born baby 願意照顧新生嬰兒</label>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="newborn_care" id="newbornYes" value="yes" />
                                                </div>
                                            </div>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="newborn_care" id="newbornNo" value="no" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="form-group row">
                                            <label class="col-sm-6 col-form-label">Willing to take care of elderly 願意照顧長者</label>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="elderly_care_willing" id="elderlyYes" value="yes" />
                                                </div>
                                            </div>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="elderly_care_willing" id="elderlyNo" value="no" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="form-group row">
                                            <label class="col-sm-6 col-form-label">Willing to take care of disabled/bedridden 願意照顧病患 / 長期臥床人士</label>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="disabled_care_willing" id="disabledYes" value="yes" />
                                                </div>
                                            </div>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="disabled_care_willing" id="disabledNo" value="no" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="form-group row">
                                            <label class="col-sm-6 col-form-label">Willing to share a room 願意與人同房</label>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="share_room" id="shareRoomYes" value="yes" />
                                                </div>
                                            </div>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="share_room" id="shareRoomNo" value="no" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="form-group row">
                                            <label class="col-sm-6 col-form-label">Willing to accept the weekly day off assigned by employer 願意接受僱主指定之假日</label>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="accept_day_off" id="dayOffYes" value="yes" />
                                                </div>
                                            </div>
                                            <div class="col text-start">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="accept_day_off" id="dayOffNo" value="no" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="form-group row">
                                            <label class="col-sm-6 col-form-label">Total Years of Work Experience 總工作年資</label>
                                            <div class="col-sm-3">
                                                <input type="number" class="form-control" name="work_experience_years" id="workExperienceYears" placeholder="Years(年)" />
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="number" class="form-control" name="work_experience_months" id="workExperienceMonths" placeholder="Months(月)" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="step">
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <h3>Domestic Helper Working Experience 傭工工作記錄</h3>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="row">
                                            <div class="col-md-4 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="text" class="form-control" id="location" name="location[]" placeholder="Location" />
                                                    <label for="location">Location 地區:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="date" class="form-control" id="dateFrom" name="dateFrom[]" placeholder="Date From" />
                                                    <label for="dateFrom">Date From 由:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="date" class="form-control" id="dateTo" name="dateTo[]" placeholder="Date To" />
                                                    <label for="dateTo">To 至:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="text" class="form-control" id="salary" name="salary[]" placeholder="Salary" />
                                                    <label for="salary">Salary 薪金:</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="row">
                                            <div class="col-md-4 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="text" class="form-control" id="district" name="district[]" placeholder="District" />
                                                    <label for="district">District 區域:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-8 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="text" class="form-control" id="language" name="language[]" placeholder="Language" />
                                                    <label for="language">Language you spoke with your employer 每日語言:</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 mt-3">
                                        <div class="row">
                                            <div class="col-md-12 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="size" name="size[]" placeholder="Size of Flat / House" />
                                                    <label for="size">Size of Flat / House 屋層面積 (SQFT):</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="text" class="form-control" id="employer_nationality" name="employer_nationality[]" placeholder="Employer Nationality" />
                                                    <label for="employer_nationality">Employer Nationality 僱主國籍:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="familyMembers" name="family_members[]" placeholder="Number of Family Members" />
                                                    <label for="familyMembers">Number of Family Members 家庭人數:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="numAdults" name="num_adults[]" placeholder="Number of Adults" />
                                                    <label for="numAdults">No. of Adults 成人:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="numBabies" name="num_babies[]" placeholder="Number of Babies" />
                                                    <label for="numBabies">No. of Babies 嬰兒:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="numChildren" name="num_children[]" placeholder="Number of Children" />
                                                    <label for="numChildren">No. of Children 小孩:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="numElderly" name="num_elderly[]" placeholder="Number of Elderly" />
                                                    <label for="numElderly">No. of Elderly 長者:</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 mt-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label><b>Main Duties 主要職責</b></label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_babyCare[]" id="babyCare" value="babyCare" />
                                                    <label class="form-check-label" for="babyCare">Caring for Baby (照顧嬰兒)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_childCare[]" id="childCare" value="childCare" />
                                                    <label class="form-check-label" for="childCare">Caring for Child (照顧小孩)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_elderlyCare[]" id="elderlyCare" value="elderlyCare" />
                                                    <label class="form-check-label" for="elderlyCare">Caring for Elderly Person (照顧老人)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_housework[]" id="housework" value="housework" />
                                                    <label class="form-check-label" for="housework">General Housework (一般家務)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_cooking[]" id="cooking" value="cooking" />
                                                    <label class="form-check-label" for="cooking">Cooking (煮食)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_washing[]" id="washing" value="washing" />
                                                    <label class="form-check-label" for="washing">Car Washing (洗車)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_disabledCare[]" id="disabledCare" value="disabledCare" />
                                                    <label class="form-check-label" for="disabledCare">Caring for Disabled Person (照顧殘障人士)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_driving[]" id="driving" value="driving" />
                                                    <label class="form-check-label" for="driving">Driving (駕駛)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_bedriddenCare[]" id="bedriddenCare" value="bedriddenCare" />
                                                    <label class="form-check-label" for="bedriddenCare">Caring for Bedridden (照顧臥床病人)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_gardening[]" id="gardening" value="gardening" />
                                                    <label class="form-check-label" for="gardening">Gardening (園藝)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_petsCare[]" id="petsCare" value="petsCare" />
                                                    <label class="form-check-label" for="petsCare">Caring for Pets (照顧寵物)</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check mt-3 mb-3">
                                                    <input class="form-check-input" type="checkbox" name="employer_specialneeds[]" id="specialneeds" value="specialneeds" />
                                                    <label class="form-check-label" for="specialneeds">Special Needs (特殊需要)</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                        <div class="row">
                                            <div class="col-md-6 mt-3">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="text" class="form-control" id="reason" name="reason[]" placeholder="Reason for Leaving" />
                                                    <label for="reason">Reason for Leaving 離職原因:</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mt-3">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="yearsExperience" name="years_experience[]" placeholder="Total Years of Work Experience" />
                                                    <label for="yearsExperience">Total Years of Work Experience 總工作年資 (Year(s) 年):</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 mt-3">
                                                <div class="form-floating form-floating-outline">
                                                    <input type="number" class="form-control" id="monthsExperience" name="months_experience[]" placeholder="Total Months of Work Experience" />
                                                    <label for="monthsExperience">Total Months of Work Experience 總工作月資 (Month(s) 月):</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 mt-3 mb-3">
                                        <button
                                            class="btn next_button text-uppercase js-btn-next"
                                            type="button"
                                            onclick="working_experience_fields();"
                                            style="float: right; background-color: darkgreen !important; border-color: darkgreen !important;"
                                        >
                                            ADD <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                                <div id="working_experience_fields"></div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-6">
                                    <button type="button" class="btn next_button text-uppercase mt-3 js-btn-next" id="prevBtn" onclick="changeStep(-1)">Previous</button>
                                </div>
                                <div class="col-6 text-right">
                                    <button type="button" class="btn next_button text-uppercase mt-3 js-btn-next" id="nextBtn" onclick="changeStep(1)">Next</button>
                                    <button type="submit" class="btn next_button text-uppercase mt-3 js-btn-next" id="submitBtn" style="display: none; background-color: #000 !important; border-color: #000 !important;">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        let currentStep = 0;

        function showStep(step) {
            const steps = document.querySelectorAll(".step");
            steps.forEach((el, index) => {
                el.classList.remove("active");
                if (index === step) el.classList.add("active");
            });
            document.getElementById("prevBtn").style.display = step === 0 ? "none" : "inline";
            document.getElementById("nextBtn").style.display = step === steps.length - 1 ? "none" : "inline";
            document.getElementById("submitBtn").style.display = step === steps.length - 1 ? "inline" : "none";
        }

        function changeStep(n) {
            const steps = document.querySelectorAll(".step");
            if (currentStep + n < 0 || currentStep + n >= steps.length) return;
            currentStep += n;
            showStep(currentStep);
        }

        showStep(currentStep);
        var room1 = 1;
        function working_experience_fields() {
            room1++;
            var objTo = document.getElementById("working_experience_fields");
            var divtest1 = document.createElement("div");
            divtest1.setAttribute("class", " removeclass" + room1);
            var rdiv = "removeclass" + room1;
            divtest1.innerHTML =
                '<div class="row"><div class="col-md-12 mt-3"><h3>Domestic Helper Working Experience 傭工工作記錄</h3></div><div class="col-md-12 mt-3"><div class=" row"><div class="col-md-4 mt-2"><div class="form-floating form-floating-outline"><input type="text" class="form-control" id="location" name="location[]" placeholder="Location"><label for="location">Location 地區:</label></div></div><div class="col-md-2 mt-2"><div class="form-floating form-floating-outline"><input type="date" class="form-control" id="dateFrom" name="dateFrom[]" placeholder="Date From"><label for="dateFrom">Date From 由:</label></div></div><div class="col-md-2 mt-2"><div class="form-floating form-floating-outline"><input type="date" class="form-control" id="dateTo" name="dateTo[]" placeholder="Date To"><label for="dateTo">To 至:</label></div></div><div class="col-md-4 mt-2"><div class="form-floating form-floating-outline"><input type="text" class="form-control" id="salary" name="salary[]" placeholder="Salary"><label for="salary">Salary 薪金:</label></div></div></div></div><div class="col-md-12 mt-3"><div class="row"><div class="col-md-4 mt-2"><div class="form-floating form-floating-outline"><input type="text" class="form-control" id="district" name="district[]" placeholder="District"><label for="district">District 區域:</label></div></div><div class="col-md-8 mt-2"><div class="form-floating form-floating-outline"><input type="text" class="form-control" id="language" name="language[]" placeholder="Language"><label for="language">Language you spoke with your employer 每日語言:</label></div></div></div></div><div class="col-md-4 mt-3"><div class="row"><div class="col-md-12 mt-2"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="size" name="size[]" placeholder="Size of Flat / House"><label for="size">Size of Flat / House 屋層面積 (SQFT):</label></div></div><div class="col-md-12 mt-2"><div class="form-floating form-floating-outline"><input type="text" class="form-control" id="employer_nationality" name="employer_nationality[]" placeholder="Employer Nationality"><label for="employer_nationality">Employer Nationality 僱主國籍:</label></div></div><div class="col-md-12 mt-2"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="familyMembers" name="family_members[]" placeholder="Number of Family Members"><label for="familyMembers">Number of Family Members 家庭人數:</label></div></div><div class="col-md-12 mt-2"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="numAdults" name="num_adults[]" placeholder="Number of Adults"><label for="numAdults">No. of Adults 成人:</label></div></div><div class="col-md-12 mt-2"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="numBabies" name="num_babies[]" placeholder="Number of Babies"><label for="numBabies">No. of Babies 嬰兒:</label></div></div><div class="col-md-12 mt-2"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="numChildren" name="num_children[]" placeholder="Number of Children"><label for="numChildren">No. of Children 小孩:</label></div></div><div class="col-md-12 mt-2"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="numElderly" name="num_elderly[]" placeholder="Number of Elderly"><label for="numElderly">No. of Elderly 長者:</label></div></div></div></div><div class="col-md-8 mt-3"><div class="row"><div class="col-md-12"><label><b>Main Duties 主要職責</b></label></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_babyCare[]" id="babyCare" value="babyCare"><label class="form-check-label" for="babyCare">Caring for Baby (照顧嬰兒)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_childCare[]" id="childCare" value="childCare"><label class="form-check-label" for="childCare">Caring for Child (照顧小孩)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_elderlyCare[]" id="elderlyCare" value="elderlyCare"><label class="form-check-label" for="elderlyCare">Caring for Elderly Person (照顧老人)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_housework[]" id="housework" value="housework"><label class="form-check-label" for="housework">General Housework (一般家務)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_cooking[]" id="cooking" value="cooking"><label class="form-check-label" for="cooking">Cooking (煮食)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_washing[]" id="washing" value="washing"><label class="form-check-label" for="washing">Car Washing (洗車)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_disabledCare[]" id="disabledCare" value="disabledCare"><label class="form-check-label" for="disabledCare">Caring for Disabled Person (照顧殘障人士)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_driving[]" id="driving" value="driving"><label class="form-check-label" for="driving">Driving (駕駛)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_bedriddenCare[]" id="bedriddenCare" value="bedriddenCare"><label class="form-check-label" for="bedriddenCare">Caring for Bedridden (照顧臥床病人)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_gardening[]" id="gardening" value="gardening"><label class="form-check-label" for="gardening">Gardening (園藝)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_petsCare[]" id="petsCare" value="petsCare"><label class="form-check-label" for="petsCare">Caring for Pets (照顧寵物)</label></div></div><div class="col-md-6"><div class="form-check mt-3 mb-3"><input class="form-check-input" type="checkbox" name="employer_specialneeds[]" id="specialneeds" value="specialneeds"><label class="form-check-label" for="specialneeds">Special Needs (特殊需要)</label></div></div></div></div><div class="col-md-12 mt-3"><div class="row"><div class="col-md-6 mt-3"><div class="form-floating form-floating-outline"><input type="text" class="form-control" id="reason" name="reason[]" placeholder="Reason for Leaving"><label for="reason">Reason for Leaving 離職原因:</label></div></div><div class="col-md-3 mt-3"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="yearsExperience" name="years_experience[]" placeholder="Total Years of Work Experience"><label for="yearsExperience">Total Years of Work Experience 總工作年資 (Year(s) 年):</label></div></div><div class="col-md-3 mt-3"><div class="form-floating form-floating-outline"><input type="number" class="form-control" id="monthsExperience" name="months_experience[]" placeholder="Total Months of Work Experience"><label for="monthsExperience">Total Months of Work Experience 總工作月資 (Month(s) 月):</label></div></div></div></div></div> <div class="col-sm-12 mb-4"><div class="col-sm-12"> <div class="contact_10"><button class="btn next_button text-uppercase mt-3 js-btn-next" type="button" onclick="remove_working_experience_fields(' +
                room1 +
                ');" style="float: right">DELETE  <span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></div></div></div></div><div class="clear"></div>';

            objTo.appendChild(divtest1);
        }
        function remove_working_experience_fields(rid) {
            $(".removeclass" + rid).remove();
        }
        document.getElementById('multiStepForm').addEventListener('submit', function(event) {
            var emailInput = document.getElementById('email');
            if (emailInput.value.trim() === '') {
                alert('Email address cannot be empty.');
                emailInput.focus();
                event.preventDefault(); // Prevent form submission
            }
        });
    </script>
@endsection
