@extends('dashboard.layouts.app')
@section('title','Profile')

@section('content')
    <style>
        .blog-ar{
            margin-top: 97px;
        }
    </style>
    <section _ngcontent-serverapp-c3727738402 class="agency news-section pb-5 custom-container page-section blog-ar">
        <div _ngcontent-serverapp-c3727738402 class="container">
                <div _ngcontent-serverapp-c116635558 class="row mt-4">
                        <div _ngcontent-serverapp-c116635558 class="col-12">
                            <div class="container mt-5">
                                <!-- Tabs Navigation -->
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true" style="font-weight: 400 !important;
        font-size: 18px !important;
        color: rgba(0, 0, 0, .6) !important; border: 0 !important; border-bottom: 1px solid black !important;">Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="false" style="font-weight: 400 !important;
        font-size: 18px !important;
        color: rgba(0, 0, 0, .6) !important;  border: 0 !important; border-bottom: 1px solid black !important">Change Password</a>
                                    </li>
                                </ul>

                                <!-- Tabs Content -->
                                <div class="tab-content" id="myTabContent">
                                    <!-- Profile Tab -->
                                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                        <form class="mt-3">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-floating form-floating-outline">
                                                        <input type="text" class="form-control" id="role" placeholder="Helper" readonly/>
                                                        <label for="role">Role</label>
                                                    </div>

                                                </div>
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">
                                                    <input type="text" class="form-control" id="name" placeholder="Name">
                                                        <label for="name">Name</label>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                    <input type="email" class="form-control" id="email" placeholder="rvillaroman@widebizz.com">
                                                        <label for="email">Email</label>
                                                </div>
                                                </div>
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                    <input type="text" class="form-control" id="mobile" placeholder="+852 97826445">
                                                        <label for="mobile">Mobile No.</label>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                    <select id="location" class="form-control">
                                                        <option selected>Hong Kong</option>
                                                        <!-- Add other options as necessary -->
                                                    </select>
                                                        <label for="location">Location</label>
                                                </div>
                                                </div>
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                    <select id="language" class="form-control">
                                                        <option selected>English</option>
                                                        <!-- Add other options as necessary -->
                                                    </select>
                                                        <label for="language">Language Preference</label>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                    <input type="text" class="form-control" id="passport">
                                                        <label for="passport">Passport No</label>
                                                </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn next_button text-uppercase mt-3">Update</button>
                                            <button type="button" class="btn next_button text-uppercase mt-3">Delete Account</button>
                                        </form>
                                    </div>
                                    <!-- Change Password Tab -->
                                    <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
                                        <form class="mt-3">

                                            <div class=" row mt-3">
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                <input type="password" class="form-control" id="oldPassword" placeholder="Old Password">
                                                        <label for="oldPassword">Old Password</label>
                                            </div>
                                            </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                <input type="password" class="form-control" id="newPassword" placeholder="New Password">
                                                        <label for="newPassword">New Password</label>
                                            </div>
                                            </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class=" col-md-6">
                                                    <div class="form-floating form-floating-outline">

                                                <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password">
                                                        <label for="confirmPassword">Confirm Password</label>
                                            </div>
                                            </div>
                                            </div>
                                            <button type="submit" class="btn next_button text-uppercase mt-3">Change Password</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </section>
@endsection
