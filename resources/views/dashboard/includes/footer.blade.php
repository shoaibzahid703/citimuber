
    <footer _ngcontent-serverapp-c2340635486 class="resume copyright bg-secondary inner-pages-footer pb-3" style="background-color:#1E3F66 !important;">
        <div _ngcontent-serverapp-c2340635486 class="container">
            <div _ngcontent-serverapp-c2340635486 class="row">
                <div _ngcontent-serverapp-c2340635486 class="col-12 col-sm-4 col-md-4 col-lg-2 col-xl-2 mb-5 custom-text-align">
                    <div _ngcontent-serverapp-c2340635486 class="link link-horizontal text-center mb-3">
                        <picture _ngcontent-serverapp-c2340635486>
                            <source _ngcontent-serverapp-c2340635486 height="130px" width="36px" media="(max-width:1024px)" class="img-fluid footer-logo" srcset="{{asset('cdn-sub/logo.png')}}">
                            <source _ngcontent-serverapp-c2340635486 height="200px" width="56px" media="(max-width:768px)" class="img-fluid footer-logo" srcset="{{asset('cdn-sub/logo.png')}}">
                            <img _ngcontent-serverapp-c2340635486 loading="lazy" alt="CitiMuber logo" height="160px" width="44px" class="img-fluid footer-logo" src="{{asset('cdn-sub/logo.png')}}">
                        </picture>
                    </div>
                    <p _ngcontent-serverapp-c2340635486 class="light"><b _ngcontent-serverapp-c2340635486> Free of charges for any Domestic Helpers and Job seekers.</b></p>
                </div>
                <div _ngcontent-serverapp-c2340635486 class="col-12 col-sm-3 col-md-4 col-lg-2 col-xl-2 mb-5 custom-text-align">
                    <div _ngcontent-serverapp-c2340635486 class="link">
                        <ul _ngcontent-serverapp-c2340635486 class="justify-content-center spacer_26">
                            <li _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 routerlinkactive="router-link-active" class="text-offwhite" href="/about"><b _ngcontent-serverapp-c2340635486>About Us</b></a></li>
                            <li _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/contact"><b _ngcontent-serverapp-c2340635486> Contact Us</b></a></li>
                            <li _ngcontent-serverapp-c2340635486 class="ng-star-inserted"><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/happy_helpers"><b _ngcontent-serverapp-c2340635486>Happy Helpers</b></a></li>
                            <!---->
                            <li _ngcontent-serverapp-c2340635486 class="ng-star-inserted"><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/happy_employers"><b _ngcontent-serverapp-c2340635486>Happy Employers</b></a></li>
                            <!---->
                            <li _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/news"><b _ngcontent-serverapp-c2340635486>News & Tips </b></a></li>
                        </ul>
                    </div>
                </div>
                <div _ngcontent-serverapp-c2340635486 class="col-12 col-sm-5 col-md-4 col-lg-3 col-xl-3 mb-5 custom-text-align">
                    <div _ngcontent-serverapp-c2340635486 class="link">
                        <ul _ngcontent-serverapp-c2340635486 class="justify-content-center spacer_26">
                            <li _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/jobs"><b _ngcontent-serverapp-c2340635486>Search & find A Job</b></a></li>
                            <li _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/candidates"><b _ngcontent-serverapp-c2340635486>Find Helpers, Maids or Drivers</b></a></li>
                            <li _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/agency&services"><b _ngcontent-serverapp-c2340635486>Find a Domestic Helper Agency</b></a></li>
                            <li _ngcontent-serverapp-c2340635486 class="ng-star-inserted"><a _ngcontent-serverapp-c2340635486 target="_blank" class="text-offwhite" href="/candidates"><b _ngcontent-serverapp-c2340635486>Available Helpers in Hong Kong</b></a></li>
                            <!---->
                            <li _ngcontent-serverapp-c2340635486 class="ng-star-inserted"><a _ngcontent-serverapp-c2340635486 target="_blank" class="text-offwhite" href="/candidates"><b _ngcontent-serverapp-c2340635486>Available Maids in Singapore</b></a></li>
                            <!---->
                            <li _ngcontent-serverapp-c2340635486 class="ng-star-inserted"><a _ngcontent-serverapp-c2340635486 target="_blank" class="text-offwhite" href="/candidates"><b _ngcontent-serverapp-c2340635486>Housemaids in Saudi Arabia</b></a></li>
                            <!---->
                        </ul>
                    </div>
                </div>
                <div _ngcontent-serverapp-c2340635486 class="col-12 col-sm-4 col-md-5 col-lg-2 col-xl-2 mb-5 custom-text-align">
                    <div _ngcontent-serverapp-c2340635486 class="link ng-star-inserted">
                        <ul _ngcontent-serverapp-c2340635486 class="justify-content-center">
                            <li _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 href="/register" class="text-offwhite btn border rounded bg-transparent"><b _ngcontent-serverapp-c2340635486>Register Now</b></a></li>
                            <li _ngcontent-serverapp-c2340635486 class="mt-4 spacer_26"><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/login"><b _ngcontent-serverapp-c2340635486>Be one of our partner</b></a></li>
                        </ul>
                    </div>
                    <!---->
                </div>
                <div _ngcontent-serverapp-c2340635486 class="col-12 col-sm-8 col-md-7 col-lg-3 col-xl-3 mb-5 custom-text-align">
                    <div _ngcontent-serverapp-c2340635486 class="socials-lists mb-3">
                        <ul _ngcontent-serverapp-c2340635486 class="socials-horizontal">
                            <li _ngcontent-serverapp-c2340635486>
                                <a _ngcontent-serverapp-c2340635486 href="https://www.facebook.com/h" target="_blank" aria-label="CitiMuber facebook">
                                    <i _ngcontent-serverapp-c2340635486 aria-hidden="true" class="icon_social_color center-content">
                                        <app-icons _ngcontent-serverapp-c2340635486 _nghost-serverapp-c4219164779 class="icons" ngh="0">
                                            <svg _ngcontent-serverapp-c4219164779="" class="icons" height="15" width="17">
                                                <use _ngcontent-serverapp-c4219164779="" height="15" width="15" href="https://www.helperplace.com/assets/icons/custom.svg#facebook"/>
                                            </svg>
                                        </app-icons>
                                    </i>
                                </a>
                            </li>
                            <li _ngcontent-serverapp-c2340635486>
                                <a _ngcontent-serverapp-c2340635486 href="https://twitter.com/helperplace" target="_blank" aria-label="CitiMuber twitter">
                                    <i _ngcontent-serverapp-c2340635486 aria-hidden="true" class="icon_social_color center-content">
                                        <app-icons _ngcontent-serverapp-c2340635486 _nghost-serverapp-c4219164779 class="icons" ngh="0">
                                            <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                <use _ngcontent-serverapp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#twitter"/>
                                            </svg>
                                        </app-icons>
                                    </i>
                                </a>
                            </li>
                            <li _ngcontent-serverapp-c2340635486>
                                <a _ngcontent-serverapp-c2340635486 href="https://www.tiktok.com/" target="_blank" aria-label="CitiMuber tiktok">
                                    <i _ngcontent-serverapp-c2340635486 aria-hidden="true" class="icon_social_color center-content">
                                        <app-icons _ngcontent-serverapp-c2340635486 _nghost-serverapp-c4219164779 class="icons" ngh="0">
                                            <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                <use _ngcontent-serverapp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#tiktok"/>
                                            </svg>
                                        </app-icons>
                                    </i>
                                </a>
                            </li>
                            <li _ngcontent-serverapp-c2340635486>
                                <a _ngcontent-serverapp-c2340635486 href="https://www.pinterest.com/helperplace/" target="_blank" aria-label="CitiMuber pinterest">
                                    <i _ngcontent-serverapp-c2340635486 aria-hidden="true" class="icon_social_color center-content">
                                        <app-icons _ngcontent-serverapp-c2340635486 _nghost-serverapp-c4219164779 class="icons" ngh="0">
                                            <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                <use _ngcontent-serverapp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#pinterest"/>
                                            </svg>
                                        </app-icons>
                                    </i>
                                </a>
                            </li>
                            <li _ngcontent-serverapp-c2340635486>
                                <a _ngcontent-serverapp-c2340635486 href="https://www.youtube.com/" target="_blank" aria-label="CitiMuber youtube">
                                    <i _ngcontent-serverapp-c2340635486 aria-hidden="true" class="icon_social_color center-content">
                                        <app-icons _ngcontent-serverapp-c2340635486 _nghost-serverapp-c4219164779 class="icons" ngh="0">
                                            <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                <use _ngcontent-serverapp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#youtube"/>
                                            </svg>
                                        </app-icons>
                                    </i>
                                </a>
                            </li>
                            <li _ngcontent-serverapp-c2340635486>
                                <a _ngcontent-serverapp-c2340635486 href="https://www.linkedin.com/company/helperplace" target="_blank" aria-label="CitiMuber linkedin">
                                    <i _ngcontent-serverapp-c2340635486 aria-hidden="true" class="icon_social_color center-content">
                                        <app-icons _ngcontent-serverapp-c2340635486 _nghost-serverapp-c4219164779 class="icons" ngh="0">
                                            <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                <use _ngcontent-serverapp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#linkedin"/>
                                            </svg>
                                        </app-icons>
                                    </i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div _ngcontent-serverapp-c2340635486 class="row mb-3">
                        <div _ngcontent-serverapp-c2340635486 class="col-12 text-center store-div">
                            <a _ngcontent-serverapp-c2340635486 href="https://play.google.com/" target="_blank">
                                <picture _ngcontent-serverapp-c2340635486>
                                    <source _ngcontent-serverapp-c2340635486 media="(max-width:576px)" width="114px" height="34px" srcset="{{asset('cdn-sub/front-app/assets/images/mobile-google-play-sm.webp')}}">
                                    <source _ngcontent-serverapp-c2340635486 srcset="{{asset('cdn-sub/front-app/assets/images/mobile-google-play-md.webp')}}">
                                    <img _ngcontent-serverapp-c2340635486 loading="lazy" alt="HelperPlace Android app for your android Phone" width="114px" height="34px" onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset = this.src;" class="img-fluid title-img" src="{{asset('cdn-sub/front-app/assets/images/mobile-google-play.png')}}">
                                </picture>
                            </a>
                            <a _ngcontent-serverapp-c2340635486 href="https://itunes.apple.com/" target="_blank">
                                <picture _ngcontent-serverapp-c2340635486>
                                    <source _ngcontent-serverapp-c2340635486 media="(max-width:576px)" width="101px" height="34px" srcset="{{asset('cdn-sub/front-app/assets/images/mobile-app-store-sm.webp')}}">
                                    <source _ngcontent-serverapp-c2340635486 srcset="{{asset('cdn-sub/front-app/assets/images/mobile-app-store-md.webp')}}">
                                    <img _ngcontent-serverapp-c2340635486 loading="lazy" alt="HelperPlace Android app for your android Phone" width="114px" height="34px" onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset = this.src;" class="img-fluid title-img" src="{{asset('cdn-sub/front-app/assets/images/mobile-app-store.png')}}">
                                </picture>
                            </a>
                        </div>
                    </div>
                    <p _ngcontent-serverapp-c2340635486 class="text-justify text-offwhite desc-text"><b _ngcontent-serverapp-c2340635486> We connect Employers, Maid Agencies and Domestic Helpers in Hong Kong, Singapore, Macau, UAE and Saudi Arabia. </b></p>
                </div>
            </div>
            <div _ngcontent-serverapp-c2340635486 class="row copyright-region">
                <div _ngcontent-serverapp-c2340635486 class="col-12 col-lg-4 form-inline region-lang">
                              <span _ngcontent-serverapp-c2340635486 class="mr-1 ng-star-inserted">
                                 <div _ngcontent-serverapp-c2340635486 class="yoga-circle location-icon"><img _ngcontent-serverapp-c2340635486 src="{{asset('cdn-sub/front-app/assets/images/translate.svg')}}" loading="lazy" alt="location_icon" width="16px" height="16px"></div>
                              </span>
                    <!---->
                    <select _ngcontent-serverapp-c2340635486 name="language" style="width: 60px;" class="ng-star-inserted">
                        <option _ngcontent-serverapp-c2340635486 value="en" class="ng-star-inserted"> English </option>
                        <option _ngcontent-serverapp-c2340635486 value="zh_cn" class="ng-star-inserted"> 中文 - 简体 </option>
                        <option _ngcontent-serverapp-c2340635486 value="zh_hk" class="ng-star-inserted"> 中文 - 繁體 </option>
                        <option _ngcontent-serverapp-c2340635486 value="ar" class="ng-star-inserted"> العربية </option>
                        <!---->
                    </select>
                    <!----><span _ngcontent-serverapp-c2340635486 class="ng-star-inserted">/ </span><!---->
                    <span _ngcontent-serverapp-c2340635486>
                                 <div _ngcontent-serverapp-c2340635486 class="yoga-circle location-icon"><img _ngcontent-serverapp-c2340635486 src="{{asset('cdn-sub/front-app/assets/images/globe.svg')}}" loading="lazy" alt="location_icon" width="16px" height="16px"></div>
                              </span>
                    <select _ngcontent-serverapp-c2340635486 name="location">
                        <!---->
                    </select>
                </div>
                <div _ngcontent-serverapp-c2340635486 class="col-12 col-lg-8">
                    <div _ngcontent-serverapp-c2340635486>
                        <p _ngcontent-serverapp-c2340635486 class="copyright-text primary text-left"><span _ngcontent-serverapp-c2340635486 class="mr-3"> © 2024 CitiMuber — All Rights Reserved </span><span _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 class="text-offwhite mr-3" href="/privacy"><b _ngcontent-serverapp-c2340635486> Privacy Policy </b></a></span><span _ngcontent-serverapp-c2340635486><a _ngcontent-serverapp-c2340635486 class="text-offwhite" href="/terms"><b _ngcontent-serverapp-c2340635486> Terms and Conditions </b></a></span></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

