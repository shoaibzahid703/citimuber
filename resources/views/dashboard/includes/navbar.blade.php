<style>
    @media (min-width: 1200px) {
        .container {
            max-width: 1440px;
        }

    }
    .navbar-nav{
        padding-left:15px ;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light  sticky-top" style="background-color: #1E3F66  !important; color:white !important;">
    <div _ngcontent-serverapp-c1428765552 class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <img src="{{asset('cdn-sub/logo.png')}}" alt="Logo">
        </a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="/jobs">Jobs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="/candidates"> Candidates</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="/agency&services"> Agency Services</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-uppercase" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        News & More
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-uppercase" href="/news">Tips & News</a>
                        <a class="dropdown-item text-uppercase" href="/training">Training</a>
                        <a class="dropdown-item text-uppercase" href="/partner">Partner Offer</a>
                        <a class="dropdown-item text-uppercase" href="/event">Event</a>
                        <a class="dropdown-item text-uppercase" href="/about">About Us</a>
                        <a class="dropdown-item text-uppercase" href="/pricing">Pricing</a>
                        <a class="dropdown-item text-uppercase" href="/public_holiday">Public Holiday</a>
                    </div>
                </li>
            </ul>
            <div class="form-inline my-2 my-lg-0">
                <a class="btn login_btn my-2 my-sm-0 text-uppercase" href="#">Login</a>
                <a class="btn register_btn my-2 my-sm-0 text-uppercase" href="#">Register</a>
            </div>
        </div>
    </div>
</nav>
