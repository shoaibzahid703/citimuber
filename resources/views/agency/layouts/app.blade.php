<!DOCTYPE html>
<html data-critters-container>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>@yield('title') - {{env('APP_NAME')}}</title>
    <link rel="canonical" href="https://www.site.com" />

    <base href="." />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="preload" href="{{asset('cdn-sub/web-asset/fonts/proximanova-regular-webfont.woff')}}')}}" as="font" type="font/woff2" crossorigin />
    <style>
        @font-face {
            font-family: "FontAwesome";
            src: url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.eot')}}");
            src: url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.eot')}}") format("embedded-opentype"), url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.woff')}}") format("woff2"),
            url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.woff')}}") format("woff"), url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.ttf')}}") format("truetype"), url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.svg')}}") format("svg");
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }
        @font-face {
            font-family: "proxima_nova_rgregular";
            src: url("https://cdn.helperplace.com/web-asset/fonts/proximanova-regular-webfont.woff") format("woff"), url("{{asset('cdn-sub/web-asset/fonts/proximanova-regular-webfont.ttf')}}") format("truetype");
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }
        @font-face {
            font-family: "proxima_nova_rgbold";
            src: url("{{asset('cdn-sub/web-asset/fonts/proximanova-bold-webfont.woff')}}") format("woff"), url("{{asset('cdn-sub/web-asset/fonts/proximanova-bold-webfont.ttf')}}");
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }
    </style>

    <link rel="icon" type="image/x-icon" href="{{asset('cdn-sub/front-app/favicon.jpg')}}" />
    <!-- <link rel="stylesheet" href="/assets/images/icon/icon.css"> -->
    <link rel="manifest" href="{{asset('cdn-sub/front-app/manifest.json')}}" />
    <link rel="preload" href="https://cdn.helperplace.com/web-asset/fonts/proximanova-regular-webfont.woff" as="font" type="font/woff2" crossorigin />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <style>
        .navbar-brand img {
            height: 45px;
        }
        .navbar {
            padding: 10px 20px;
        }
        .navbar-nav .nav-link {
            margin-right: 20px;
        }
        .form-inline .btn {
            margin-right: 10px;
        }
        @media (max-width: 992px) {
            .navbar-nav {
                text-align: left;
            }
            .navbar-nav .nav-link {
                margin: 0;
            }
            .navbar-nav .dropdown-menu {
                position: static;
                float: none;
                width: auto;
                margin-top: 0;
            }
            .form-inline {
                display: flex;
                justify-content: center;
                margin-top: 10px;
            }
        }
    </style>


    <link rel="stylesheet" href="{{asset('cdn-sub/front-app/styles.1adfe7432471cc54.css')}}" media="print" onload="this.media='all'" />
    <noscript><link rel="stylesheet" href="{{asset('cdn-sub/front-app/styles.1adfe7432471cc54.css')}}" /></noscript>
    <link rel="stylesheet" href="{{asset('admin/vendor/libs/toastr/toastr.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/sweetalert.css')}}">
    @yield('css')

    <link rel="stylesheet" href="{{asset('cdn-sub/front-app/custom.css')}}" />
    <link rel="stylesheet" href="{{asset('cdn-sub/front-app/core.css')}}" />
    <meta name="description" content="HelperPlace connects employers and domestic helpers in Hong Kong, Macau Singapore, United Arab Emirates &amp; Saudi Arabia, in just a few clicks." />
    <meta name="Publisher" content="Helperplace" />
    <meta name="locale" content="en_HK" />
    <meta name="type" content="website" />
    <meta name="image" content="{{asset('cdn-sub/web-asset/img/meta_image.jpg')}}" />
    <meta name="keywords" content="happy,happy employers,employers,helperplace" />
    <meta name="subject" content="HelperPlace is the best way to match your expectations with the right candidates. We offer a free place to allow foreign domestic helpers to meet you with no middlemen." />
    <meta name="copyright" content="HelperPlace" />
    <meta name="MobileOptimized" content="320" />
    <meta name="robots" content="all" />
    <meta name="revised" content="Fri May 24 2024 12:12:20 GMT+0000 (Coordinated Universal Time)" />
    <meta name="date" content="Fri May 24 2024 12:12:20 GMT+0000 (Coordinated Universal Time)" />
    <meta name="twitter:title" content="Domestic Helpers &amp; Maids in Hong Kong &amp; More - HelperPlace" />
    <meta name="twitter:description" content="HelperPlace connects employers and domestic helpers in Hong Kong, Macau Singapore, United Arab Emirates &amp; Saudi Arabia, in just a few clicks." />
    <meta name="twitter:image" content="{{asset('cdn-sub/web-asset/img/meta_image.jpg')}}" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@helperplace" />
    <meta property="og:title" content="Domestic Helpers &amp; Maids in Hong Kong &amp; More - HelperPlace" />
    <meta property="og:description" content="HelperPlace connects employers and domestic helpers in Hong Kong, Macau Singapore, United Arab Emirates &amp; Saudi Arabia, in just a few clicks." />
    <meta property="og:Publisher" content="Helperplace" />
    <meta property="og:locale" content="en_HK" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{asset('cdn-sub/web-asset/img/meta_image.jpg')}}" />
    <meta property="og:site_name" content="https://www.helperplace.com" />
    <meta property="fb:app_id" content="1606320239668688" />
    <meta property="og:url" content="https://www.helperplace.com/" />
    <style>
        .form-group input {
            border: 1px solid #ced4da !important;
            background-color: #fff !important;
        }
    </style>
    @stack('css')
</head>

<body id="main-body" class="cutom-class">
<!--nghm-->


<style>
    .login_btn{
        border: 2px solid #b50000;
        border-radius: 6px;
        background-color: #b50000;
        color: #fff !important;
        letter-spacing: 1px;
        width: 110px;
        text-align: center;
        float: right;
        font-weight: 600;
        font-size: 18px;
    }
    .register_btn{
        border: 2px solid #000;
        border-radius: 6px;
        background-color: #000;
        color: #fff !important;
        letter-spacing: 1px;
        width: 110px;
        text-align: center;
        float: left;
        font-weight: 600;
        font-size: 18px;
    }
    a.nav-link{
        font-weight: 600 !important;
        font-size: 18px !important;
        color: white !important;
    }
    .sticky-top{
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 10000000;
    }
    .dropdown-item{
        padding: 1.25rem 3rem !important;
    }
    .navbar-light .navbar-toggler{
        border: none !important;
    }
</style>

<section class="web_homePage">
        @include('agency.includes.navbar')
        @yield('content')
            @include('agency.includes.footer')

</section>



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script src="{{asset('admin/js/bundle.js')}}"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.12.2/dist/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.12.2/dist/sweetalert2.all.min.js"></script>
@include('candidate.includes.messages')

@stack('js')
</body>
</html>
