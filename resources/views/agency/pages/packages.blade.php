@extends('agency.layouts.app')
@section('title','Agency Packages')
@push('css')
    <style>
        .plan-card {
            cursor: pointer;
            border: 2px solid #d8d8d8;
            transition: border-color 0.3s ease;
        }

        .plan-card:hover {
            border-color: #b50201
        }

        .plan-radio:checked + .card-body {
            border-color: #b50201;
            background-color: rgba(181, 2, 1, 0.31);
        }

        .post-resume {
            margin-top: 70px;
        }

        .hidden {
            visibility: hidden;
        }

    </style>
@endpush
@section('content')
    <section class="agency news-section pb-5 custom-container page-section blog-ar">
        <div class="container">
            <div class="row mt-4" style="margin-top: 140px !important;" >
                <div class="col-12 plr-50">
                    <div class="card">
                        <div class="card-header">
                            <h2>Select a Plan</h2>
                        </div>
                        <div class="card-body">
                            <form method="post" action="" id="package_form" >
                                @csrf
                                <div class="row" id="plansContainer">
                                    @foreach($plans as $plan)

                                        <div class="col-md-4 blog-contain" data-route="{{route('pay_agency_package',$plan)}}">
                                            <div class="card mb-4 plan-card">
                                                <input type="radio" class="plan-radio" name="plan_id" id="plan_{{ $plan->id }}" value="{{ $plan->id }}" hidden >
                                                <label style="margin-bottom: 0px !important;" for="plan_{{ $plan->id }}" class="card-body">
                                                    <h3 class="card-title">{{ $plan->name }}</h3>
                                                    <p class="card-text">Duration: {{ $plan->post_duration }} days</p>
                                                    <h4 class="badge badge-danger text-light" style="font-size: medium;" data-price="{{ $plan->price }}">
                                                        Price: <span class="currency">Hk</span>
                                                        <span class="large">{{ $plan->price }}</span>
                                                    </h4>
                                                    <p class="mt-2"> {!!  $plan->description !!}</p>
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center ">
                                        <button type="submit" class="btn btn-primary bg-primary text-white disabled" id="proceed">Proceed</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')


    <script type="text/javascript">

        $('.blog-contain').click(function(e){
            $('#package_form').attr('action',$(this).attr('data-route'));
            $('#proceed').removeClass('disabled');
            $('.plan-card').each(function() {
                $(this).removeClass('border-danger');
            });
            $(this).find('.plan-card').addClass('border-danger');
        });
    </script>
@endpush
