@extends('agency.layouts.app')
@section('title','Profile')
@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
    <style>
        .hide{
            display: none;
        }
        .valid-msg {
            color: #00c900;
        }
        .error-msg {
            color: red;
        }
        .mobile_label:after{
            content: "";
            position: absolute;
            width: 100px;
            inset-inline-start: 0;
            top: 0.35rem;
            z-index: -1;
            background-color: #fff  !important;
            inset: 0.8555rem 0.5rem;
        }
        .form-control{
            height: 3.0000625rem !important;
            min-height: 3.0000625rem !important;
            line-height: 1.375 !important;
        }
        .iti__country-list {
            z-index: 3 !important;
        }
    </style>
@endpush
@section('content')
    <style>
        .blog-ar{
            margin-top: 97px;
        }
    </style>

    <section _ngcontent-serverapp-c3727738402 class="agency news-section pb-5 custom-container page-section blog-ar">
        <div _ngcontent-serverapp-c3727738402 class="container">
            <div _ngcontent-serverapp-c116635558 class="row mt-4">
                <div _ngcontent-serverapp-c116635558 class="col-12">
                    <div class="container mt-5">
                        <!-- Tabs Navigation -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link {{ session('password_tab') ? '' : 'active' }}" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="{{ session('password_tab') ? 'false' : 'true' }}" style="font-weight: 400 !important; font-size: 18px !important; color: rgba(0, 0, 0, .6) !important; border: 0 !important; border-bottom: 1px solid black !important;">Profile </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ session('password_tab') ? 'active' : '' }}" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="{{ session('password_tab') ? 'true' : 'false' }}" style="font-weight: 400 !important; font-size: 18px !important; color: rgba(0, 0, 0, .6) !important; border: 0 !important; border-bottom: 1px solid black !important;">Change Password</a>
                            </li>
                        </ul>

                        <!-- Tabs Content -->
                        <div class="tab-content" id="myTabContent">
                            <!-- Profile Tab -->
                            <div class="tab-pane fade show {{ session('password_tab') ? '' : 'active' }}" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <form class="mt-3" method="post" action="{{route('agency_profile_update')}}" enctype="multipart/form-data" id="candidateProfileUpdate">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" class="form-control" id="role" name="role" placeholder="Helper" value="{{old('role') ? old('role') : auth()->user()->role}}" readonly />
                                                <label for="role">Role</label>
                                            </div>

                                        </div>
                                        <div class=" col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Name" value="{{old('name') ? old('name') : auth()->user()->name}}">
                                                <label for="name">Name</label>
                                            </div>
                                            @error('name')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class=" col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text" name="company_name"
                                                       class="form-control @error('company_name') is-invalid @enderror"
                                                       id="company_name"
                                                       placeholder="Name"
                                                       value="{{old('company_name') ? old('company_name') : auth()->user()->company_name}}">
                                                <label for="company_name">Company Name</label>
                                            </div>
                                            @error('name')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class=" col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="rvillaroman@widebizz.com" value="{{old('email') ? old('email') : auth()->user()->email}}">
                                                <label for="email">Email</label>
                                            </div>
                                            @error('email')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">
                                                <input type="text"  class="form-control @error('mobile') is-invalid @enderror" id="mobile" name="mobile"  value="{{old('mobile') ? old('mobile') : auth()->user()->mobile}}" >
                                                <label for="mobile" class="mobile_label" style="top: -23px !important;">Mobile No.</label>
                                            </div>
                                            @error('mobile')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                            <span id="mobile-number-valid-msg" class="hide valid-msg">Valid</span>
                                            <span id="mobile-number-error-msg" class="hide error-msg"></span>
                                            <input type="hidden" id="formatted-mobile" name="formatted_mobile" value="{{auth()->user()->mobile}}">
                                        </div>
                                        <div class=" col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">

                                                <select id="location" name="location" class="form-control">
                                                    <option >Select Nationality</option>
                                                    @foreach($country as $countries)
                                                        <option value="{{$countries->id}}" {{ Auth::user()->location == $countries->id ? 'selected' : '' }}>{{$countries->name}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="location">Location</label>
                                            </div>
                                        </div>
                                        <div class=" col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">

                                                <select id="language" name="language" class="form-control">
                                                    <option selected>Select Language</option>
                                                    @foreach($language as $language)
                                                        <option value="{{$language->id}}" {{ Auth::user()->language == $language->id ? 'selected' : '' }}>{{$language->name}}</option>
                                                    @endforeach
                                                    <!-- Add other options as necessary -->
                                                </select>
                                                <label for="language">Language Preference</label>
                                            </div>
                                        </div>


                                        <div class=" col-md-6 mt-2">
                                            <div class="form-floating form-floating-outline">

                                                <input type="text" name="passport"
                                                       class="form-control @error('passport') is-invalid @enderror"
                                                       id="passport" value="{{old('passport') ? old('passport') : auth()->user()->passport}}" placeholder="passport number">
                                                <label for="passport">Passport No</label>
                                            </div>
                                            @error('passport')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

{{--                                        <div class=" col-md-6 mt-2">--}}
{{--                                            <div class="form-floating form-floating-outline">--}}
{{--                                                <input type="file" name="profile_image" class="form-control @error('profile_image') is-invalid @enderror" id="profile_image" placeholder="Name">--}}
{{--                                                <label for="profile_image">Upload Profile</label>--}}
{{--                                            </div>--}}
{{--                                            @if(Auth::user()->profile_image)--}}
{{--                                                <div class="mt-2">--}}
{{--                                                    <img src="{{ asset('storage/profile_image/' . Auth::user()->profile_image) }}" alt="Profile Image" class="img-thumbnail" style="max-width: 150px;">--}}
{{--                                                </div>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
                                        <div class=" col-md-12 mt-3">
                                            <button type="submit" class="btn next_button text-uppercase mt-3 text-white">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- Change Password Tab -->
                            <div class="tab-pane fade {{ session('password_tab') ? 'show active' : '' }}" id="password" role="tabpanel" aria-labelledby="password-tab">
                                <form class="mt-3" method="post" action="{{route('agency_update_password')}}">
                                    @csrf

                                    <div class=" row mt-3">
                                        <div class=" col-md-6">
                                            <div class="form-floating form-floating-outline">

                                                <input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" id="current_password" placeholder="Old Password">
                                                <label for="current_password">Old Password</label>
                                            </div>
                                            @error('current_password')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class=" col-md-6">
                                            <div class="form-floating form-floating-outline">

                                                <input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" id="newPassword" placeholder="New Password">
                                                <label for="newPassword">New Password</label>
                                            </div>
                                            @error('password')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class=" col-md-6">
                                            <div class="form-floating form-floating-outline">

                                                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm Password">
                                                <label for="password_confirmation">Confirm Password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn next_button text-uppercase mt-3 text-white">Change Password</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Check if there is a session variable for password_tab
            if ("{{ session('password_tab') }}") {
                // Activate the tab
                var passwordTab = new bootstrap.Tab(document.querySelector('#password-tab'));
                passwordTab.show();
            }
        });
    </script>

    <script>
        let mobile_number = document.querySelector("#mobile");

        const errorMsg = document.querySelector("#mobile-number-error-msg");
        const validMsg = document.querySelector("#mobile-number-valid-msg");

        const errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        const phone_no_ins = intlTelInput(mobile_number, {
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.js",
        });


        const reset = () => {
            mobile_number.classList.remove("is-invalid");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        mobile_number.addEventListener('blur', () => {
            reset();
            if (mobile_number.value.trim()) {
                if (phone_no_ins.isValidNumber()) {
                    validMsg.classList.remove("hide");
                    var number = phone_no_ins.getNumber(intlTelInputUtils.numberFormat.E164);
                    $("#mobile_number").val(number);
                } else {
                    mobile_number.classList.add("is-invalid");
                    const errorCode = phone_no_ins.getValidationError();
                    if(errorCode === -99){
                        errorMsg.innerHTML = "Invalid Number";
                        errorMsg.classList.remove("hide");
                    }else{
                        errorMsg.innerHTML = errorMap[errorCode];
                        errorMsg.classList.remove("hide");
                    }
                }
            }
        });

        // on keyup / change flag: reset
        mobile_number.addEventListener('change', reset);
        mobile_number.addEventListener('keyup', reset);

        document.querySelector("#candidateProfileUpdate").addEventListener('submit', (event) => {
            if (!phone_no_ins.isValidNumber()) {
                event.preventDefault();
                mobile_number.classList.add("is-invalid");
                errorMsg.innerHTML = "Invalid Number";
                errorMsg.classList.remove("hide");
            } else {
                var number = phone_no_ins.getNumber(intlTelInputUtils.numberFormat.E164);
                document.querySelector("#formatted-mobile").value = number;
            }
        });
    </script>
@endpush
