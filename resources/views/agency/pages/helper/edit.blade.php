@extends('agency.layouts.app')
@section('title','Edit Helper ')
@push('css')
    <link href="https://cdn.jsdelivr.net/npm/smartwizard@6/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />
    <style>
        .post-resume {
            margin-top: 70px;
        }
        .sw-theme-arrows>.nav .nav-link.default {
            color: #b0b0b1 !important;
        }

        .sw-theme-arrows>.nav .nav-link.active {
            color: white !important;
        }
        .sw-theme-arrows>.nav .nav-link.done {
            color: white !important;
        }
        .sw>.nav .nav-link {
            font-size: unset !important;
        }
        .sw>.nav .nav-link>.num {
            font-size: 18px;
        }
        .iti--allow-dropdown {
            width: 100% !important;
        }
        .hide{
            display: none;
        }
        .valid-msg {
            color: #00c900;
        }
        .error-msg {
            color: red;
        }
        input[type="date"]:before {
            top: unset !important;
        }
        .select2-container .select2-search--inline .select2-search__field {
            border: none !important;
        }
        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #ced4da !important;
        }
        .select2-container .select2-selection--multiple {
            min-height: 37px !important;
        }
        .select2-container--default .select2-selection--multiple {
            border: 1px solid #ced4da !important
        }
        .select2-container .select2-search--inline .select2-search__field {
            margin-top: 9px;
            width: unset !important;
        }
        .tab-content {
            height: auto !important;
        }
        .form-control.is-invalid {
            border-color: #dc3545 !important;
        }
        .mobile_number_invalid_feedback{
            display: block;
        }
        .whatsapp_number_invalid_feedback{
            display: block;
        }
        .select_2_invalid_feedback{
            display: block !important;
        }
        .next_button {
            color: white !important;
        }
        .btn-success{
            background-color: #28a745 !important;
            border-color: #28a745 !important;
            color: white !important;
        }

        @media only screen and (max-width: 640px){
            .steps a{
                padding-left:25px !important;
            }

        }
    </style>
@endpush
@section('content')
    <section class="agency news-section pb-5 custom-container page-section blog-ar">
        <div class="container">
            <div class="" style="margin-top: 140px !important;">
                <div class="row">
                    <div class="col-12">
                        <div class="top-banner-wrapper ng-star-inserted">
                            <div class="top-banner-content small-section">
                                <h1>Update Bio Data</h1>
                                <p class="header_2">
                                    A lot of employers are ready to hire you. Choose your region and get full access to all our job offers. With more than 160,000 users and ethical values, we support you to find a better job and connect with
                                    our Employers. HelperPlace is totally free for all our job seekers, no placement fees and no salary deduction!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-12 plr-50">
                    <div id="smartwizard">
                        <ul class="nav nav-progress">
                            <li class="nav-item steps">
                                <a class="nav-link" href="#step-1">
                                    <div class="num">Personal Information</div>
                                </a>
                            </li>
                            <li class="nav-item steps">
                                <a class="nav-link" href="#step-2">
                                    <span class="num">Professional Information</span>
                                </a>
                            </li>
                            <li class="nav-item steps">
                                <a class="nav-link" href="#step-3">
                                    <span class="num">Education & Experience</span>
                                </a>
                            </li>
                            <li class="nav-item steps">
                                <a class="nav-link" href="#step-4">
                                    <span class="num">About You</span>
                                </a>
                            </li>

                        </ul>
                        <form method="post" action="{{route('agency_update_helper',$user)}}" id="register-form" enctype="multipart/form-data">
                            @csrf
                            <div class="tab-content">
                                <div id="step-1" data-url="{{route('validate_add_helper_step_one')}}" class="tab-pane" role="tabpanel" aria-labelledby="step-1" style="padding: 0.8rem;">
                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">Personal Information</h3><br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="profile_image">Profile Image</label>
                                                <input type="file" class="form-control-file" name="profile_image" id="profile_image" accept="image/*" />
                                                <span class="invalid-feedback profile_image_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="first_name">First Name</label>
                                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter Your First Name" value="{{$user->resume_detail ? $user->resume_detail->first_name : '' }}" />
                                                <span class="invalid-feedback first_name_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="middle_name">Middle Name</label>
                                                <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Enter Your Middle Name" value="{{$user->resume_detail ? $user->resume_detail->middle_name : '' }}" />
                                                <span class="invalid-feedback middle_name_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="last_name">Last Name</label>
                                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Your Last Name" value="{{$user->resume_detail ? $user->resume_detail->last_name : '' }}" />
                                                <span class="invalid-feedback last_name_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="age">Age (年齡)</label>
                                                <input class="form-control" type="number" name="age" id="age" placeholder="18+" value="{{$user->resume_detail ? $user->resume_detail->age : '' }}" />
                                                <span class="invalid-feedback age_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="gender">Gender</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <select class="form-control" name="gender" id="gender">
                                                        <option value="male" @if($user->resume_detail ? $user->resume_detail->gender == "male" : '') selected @endif>Male</option>
                                                        <option value="female" @if($user->resume_detail ? $user->resume_detail->gender == "female" : '') selected @endif>Female</option>
                                                    </select>
                                                    <span class="invalid-feedback gender_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="marital_status">Marital Status</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-user-circle"></i></span>
                                                    </div>
                                                    <select class="form-control" name="marital_status" id="marital_status">
                                                        <option value="single" @if($user->resume_detail ? $user->resume_detail->marital_status == "single" : '') selected @endif>Single</option>
                                                        <option value="married" @if($user->resume_detail ? $user->resume_detail->marital_status == "married" : '') selected @endif>Married</option>
                                                        <option value="separated" @if($user->resume_detail ? $user->resume_detail->marital_status == "separated" : '') selected @endif>Separated</option>
                                                    </select>
                                                    <span class="invalid-feedback marital_status_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <label for="kids_detail">Kids Details</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-child"></i></span>
                                                    </div>
                                                    <select class="form-control" name="kids_detail" id="kids_detail">
                                                        <option value="0" @if($user->resume_detail ? $user->resume_detail->kids_detail == "0" : '') selected @endif>0</option>
                                                        <option value="1" @if($user->resume_detail ? $user->resume_detail->kids_detail == "1" : '') selected @endif>1</option>
                                                        <option value="2" @if($user->resume_detail ? $user->resume_detail->kids_detail == "2" : '') selected @endif>2</option>
                                                        <option value="3" @if($user->resume_detail ? $user->resume_detail->kids_detail == "3" : '') selected @endif>3</option>
                                                        <option value="4" @if($user->resume_detail ? $user->resume_detail->kids_detail == "4" : '') selected @endif>4</option>
                                                        <option value="5" @if($user->resume_detail ? $user->resume_detail->kids_detail == "5" : '') selected @endif>5</option>
                                                        <option value="5+" @if($user->resume_detail ? $user->resume_detail->kids_detail == "5+" : '') selected @endif>5+</option>
                                                    </select>
                                                    <span class="invalid-feedback kids_detail_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="nationality">What is your Nationality? 僱主國籍:</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fas fa-passport"></i></span>
                                                    </div>
                                                    <select class="form-control" name="nationality" id="nationality">
                                                        @foreach($nationalities as $nationality)
                                                            <option value="{{$nationality->id}}"  @if($user->resume_detail ? $user->resume_detail->nationality == $nationality->id : '') selected @endif>{{$nationality->nationality}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="invalid-feedback nationality_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="present_country">Where do you live now (Present Country) ?</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fas fa-globe"></i></span>
                                                    </div>
                                                    <select class="form-control" name="present_country" id="present_country">
                                                        @foreach($country as $p_country)
                                                            <option value="{{$p_country->id}}" @if($user->resume_detail ? $user->resume_detail->present_country == $p_country->id : '') selected @endif>{{$p_country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="invalid-feedback present_country_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="religion">Religion</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fas fa-pray"></i></span>
                                                    </div>
                                                    <select class="form-control" name="religion" id="religion">
                                                        <option value="">Select Option</option>
                                                        @foreach($religions as $religion)
                                                            <option value="{{$religion->name}}"
                                                                    @if($user->resume_detail ? $user->resume_detail->religion == $religion->name : '') selected @endif>
                                                                {{$religion->name}}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                    <span class="invalid-feedback religion_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="education_level">Education Level</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-graduation-cap"></i></span>
                                                    </div>
                                                    <select class="form-control" name="education_level" id="education_level">
                                                        <option value="">Select Option</option>
                                                        @foreach($education_levels as $education_level)
                                                            <option value="{{$education_level->name}}"
                                                                    @if($user->resume_detail ? $user->resume_detail->education_level == $education_level->name: '') selected @endif >
                                                                {{$education_level->name}}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                    <span class="invalid-feedback education_level_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">Details</h3><br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email">Email Address</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-envelope"></i></span>
                                                    </div>
                                                    <input type="email" class="form-control" name="email" id="email"  value="{{ \Illuminate\Support\Facades\Auth::user()->email }}" disabled />
                                                    <span class="invalid-feedback email_level_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="mobile_number">Mobile Number</label>
                                                <input type="text" class="form-control" name="mobile_number" id="mobile_number" placeholder="Enter Your Mobile Number" value="{{auth()->user()->mobile }}" />
                                                <span class="invalid-feedback mobile_number_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                </span>
                                                <span id="mobile-number-valid-msg" class="hide valid-msg">Valid</span>
                                                <span id="mobile-number-error-msg" class="hide error-msg"></span>
                                                <input type="hidden" id="formatted-mobile" name="formatted_mobile" value="{{auth()->user()->mobile}}">

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="whatsapp_number">Whatsapp Number</label>
                                                <input type="text" class="form-control" name="whatsapp_number" id="whatsapp_number" placeholder="Enter Your Whatsapp Number" value="{{$user->resume_detail ? $user->resume_detail->whatsapp_number : '' }}" />
                                                <span class="invalid-feedback whatsapp_number_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                  </span>
                                                <span id="whatsapp-number-valid-msg" class="hide valid-msg">Valid</span>
                                                <span id="whatsapp-number-error-msg" class="hide error-msg"></span>
                                                <input type="hidden" id="formatted-whatsapp-number" name="formatted_whatsapp_number" value="{{$user->resume_detail ? $user->resume_detail->whatsapp_number : ''}}">


                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="valid_password">Do you have a valid Passport?</label>&nbsp;&nbsp;&nbsp;
                                                <select class="form-control" name="valid_password" id="valid_password">
                                                    <option value="">Select Option</option>
                                                    <option value="Yes" @if($user->resume_detail ? $user->resume_detail->valid_password == "Yes" : '') selected @endif>Yes</option>
                                                    <option value="No" @if($user->resume_detail ? $user->resume_detail->valid_password == "No" : '') selected @endif>No</option>
                                                </select>
                                                <span class="invalid-feedback valid_password_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="step-2"  data-url="{{route('validate_add_helper_step_two')}}" class="tab-pane" role="tabpane2" aria-labelledby="step-2" style="padding: 0.8rem;">
                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">
                                        Professional Information
                                    </h3>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="position_apply"> For which position, do you want to apply? </label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-level-up"></i></span>
                                                    </div>
                                                    <select class="form-control" name="position_apply" id="position_apply">
                                                        <option value="">Select Option</option>
                                                        <option value="Domestic Helper"  @if($user->resume_detail ? $user->resume_detail->position_apply == "Domestic Helper" : '') selected @endif>Domestic Helper</option>
                                                        <option value="Driver"  @if($user->resume_detail ? $user->resume_detail->position_apply == "Driver" : '') selected @endif>Driver</option>
                                                    </select>
                                                    <span class="invalid-feedback position_apply_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="work_experience">Years of working experience</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-history"></i></span>
                                                    </div>
                                                    <input type="number" class="form-control" name="work_experience" id="work_experience" placeholder="Year of work experience?" value="{{$user->resume_detail ? $user->resume_detail->work_experience : '' }}" />
                                                    <span class="invalid-feedback work_experience_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="job_type"> Job Type</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-tasks"></i></span>
                                                    </div>
                                                    <select class="form-control" name="job_type" id="job_type">
                                                        <option value="">Select Option</option>
                                                        <option value="Full Time" @if($user->resume_detail ? $user->resume_detail->job_type == "Full Time" : '') selected @endif>Full Time</option>
                                                        <option value="Part Time" @if($user->resume_detail ? $user->resume_detail->job_type == "Part Time" : '') selected @endif>Part Time</option>
                                                        <option value="Temporary" @if($user->resume_detail ? $user->resume_detail->job_type == "Temporary" : '') selected @endif>Temporary</option>
                                                    </select>
                                                    <span class="invalid-feedback job_type_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="work_status">What is your current work status? * </label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-bars"></i></span>
                                                    </div>
                                                    <select class="form-control" name="work_status" id="work_status">
                                                        <option value="">Select Option</option>
                                                        <option value="Finished Contract" @if($user->resume_detail ? $user->resume_detail->work_status == "Finished Contract" : '') selected @endif>Finished Contract</option>
                                                        <option value="Terminated (Relocation / Financial)" @if($user->resume_detail ? $user->resume_detail->work_status == "Terminated (Relocation / Financial)" : '') selected @endif>Terminated (Relocation / Financial)</option>
                                                        <option value="Terminated (Other)" @if($user->resume_detail ? $user->resume_detail->work_status == "Terminated (Other)" : '') selected @endif>Terminated (Other)</option>
                                                        <option value="Break Contract" @if($user->resume_detail ? $user->resume_detail->work_status == "Break Contract" : '') selected @endif>Break Contract</option>
                                                        <option value="Transfer" @if($user->resume_detail ? $user->resume_detail->work_status == "Transfer" : '') selected @endif>Transfer</option>
                                                    </select>
                                                    <span class="invalid-feedback work_status_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="job_start_date">What is your job start date ?</label>
                                                <input type="date" class="form-control" name="job_start_date" id="job_start_date" value="{{$user->resume_detail ? $user->resume_detail->job_start_date : '' }}" />
                                                <span class="invalid-feedback job_start_date_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                  </span>
                                            </div>
                                        </div>
                                    </div>

                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">
                                        Your preference for job
                                    </h3>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="preferred_job_location">Preferred Job Location*</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-globe"></i></span>
                                                    </div>
                                                    <select class="form-control" name="preferred_job_location[]" id="preferred_job_location" multiple autocomplete="false">
                                                        @foreach($country as $prefer_country)
                                                            <option value="{{$prefer_country->id}}"
                                                                    @if($user->resume_detail ? in_array($prefer_country->id,explode(',',$user->resume_detail->preferred_job_location)) : '') selected @endif>{{$prefer_country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback preferred_job_location_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label for="monthly_salary">Expected Monthly Salary</label>
                                                <div class="input-group" >
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fas fa-hand-holding-usd"></i></span>
                                                    </div>
                                                    <input class="form-control" name="monthly_salary" id="monthly_salary" type="text" placeholder="Expected Monthly Salary" value="{{$user->resume_detail ? $user->resume_detail->monthly_salary : '' }}">
                                                    <span class="invalid-feedback monthly_salary_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="currency"> Currency </label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-money"></i></span>
                                                    </div>
                                                    <select class="form-control" name="currency" id="currency">
                                                        <option value="">Select Option</option>
                                                        @foreach($currency as $curr)
                                                            <option value="{{$curr->id}}" @if($user->resume_detail ? $user->resume_detail->currency == $curr->id : '') selected @endif>{{$curr->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="invalid-feedback currency_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="day_off_preference">Day off preference * </label>
                                                <div class="input-group" >
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-sign-out"></i></span>
                                                    </div>
                                                    <select class="form-control" name="day_off_preference" id="day_off_preference">
                                                        <option value="">Select Option</option>
                                                        <option value="flexible" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "flexible" : '') selected @endif>flexible</option>
                                                        <option value="to_be_discussed" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "to_be_discussed" : '') selected @endif>To be discussed</option>
                                                        <option value="monday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "monday" : '') selected @endif>monday</option>
                                                        <option value="tuesday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "tuesday" : '') selected @endif>tuesday</option>
                                                        <option value="wednesday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "wednesday" : '') selected @endif>wednesday</option>
                                                        <option value="thursday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "thursday" : '') selected @endif>thursday</option>
                                                        <option value="friday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "friday" : '') selected @endif>friday</option>
                                                        <option value="saturday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "saturday" : '') selected @endif>saturday</option>
                                                        <option value="sunday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "sunday" : '') selected @endif>sunday</option>
                                                        <option value="saturday-sunday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "saturday-sunday" : '') selected @endif>saturday-sunday</option>
                                                        <option value="friday-Saturday" @if($user->resume_detail ? $user->resume_detail->day_off_preference == "friday-Saturday" : '') selected @endif>friday-Saturday</option>
                                                    </select>
                                                    <span class="invalid-feedback day_off_preference_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="accomodation_preference">Accomodation preference </label>
                                                <div class="input-group" >
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-hotel"></i></span>
                                                    </div>
                                                    <select class="form-control" name="accomodation_preference" id="accomodation_preference">
                                                        <option value="">Select Option</option>
                                                        <option value="Live Out" @if($user->resume_detail ? $user->resume_detail->accomodation_preference == "Live Out" : '') selected @endif>Live Out</option>
                                                        <option value="Flexible" @if($user->resume_detail ? $user->resume_detail->accomodation_preference == "Flexible" : '') selected @endif>Flexible</option>
                                                        <option value="To be Discussed" @if($user->resume_detail ? $user->resume_detail->accomodation_preference == "To be Discussed" : '') selected @endif>To be Discussed</option>
                                                        <option value="Live In - Separate room" @if($user->resume_detail ? $user->resume_detail->accomodation_preference == "Live In - Separate room" : '') selected @endif>Live In - Separate room</option>
                                                        <option value="Live In - Share room" @if($user->resume_detail ? $user->resume_detail->accomodation_preference == "Live In - Share room" : '') selected @endif>Live In - Share room</option>
                                                        <option value="Live In" @if($user->resume_detail ? $user->resume_detail->accomodation_preference == "Live In" : '') selected @endif>Live In</option>
                                                    </select>
                                                    <span class="invalid-feedback accomodation_preference_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">
                                        My Skill / Duties
                                    </h3>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6 mb-1 ">
                                            <div class="form-group">
                                                <label for="languages">Language </label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-language"></i></span>
                                                    </div>
                                                    <select class="form-control" name="languages[]" id="languages" multiple autocomplete="false">
                                                        @foreach($language as $lang)
                                                            <option value="{{$lang->id}}" @if($user->resume_detail ? in_array($lang->id,explode(',',$user->resume_detail->languages)) : '') selected @endif>{{$lang->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback languages_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <label for="main_skills">Main Skills *</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-columns"></i></span>
                                                    </div>
                                                    <select class="form-control" name="main_skills[]" id="main_skills" multiple autocomplete="false">
                                                        @foreach($main_skills as $main_skill)
                                                            <option value="{{$main_skill->id}}" @if($user->resume_detail ? in_array($main_skill->id,explode(',',$user->resume_detail->main_skills)) : '') selected @endif>{{$main_skill->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback main_skills_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <label for="cooking_skills">Cooking Skills </label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-home"></i></span>
                                                    </div>
                                                    <select class="form-control" name="cooking_skills[]" id="cooking_skills" multiple autocomplete="false">
                                                        @foreach($cookingSkills as $cookingSkill)
                                                            <option value="{{$cookingSkill->id}}"  @if($user->resume_detail ? in_array($cookingSkill->id,explode(',',$user->resume_detail->cooking_skills)) : '') selected @endif>{{$cookingSkill->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback cooking_skills_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <label for="other_skills">Other Skills *</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-pencil"></i></span>
                                                    </div>
                                                    <select class="form-control" name="other_skills[]" id="other_skills" multiple autocomplete="false">
                                                        @foreach($otherSkills as $otherSkill)
                                                            <option value="{{$otherSkill->id}}" @if($user->resume_detail ? in_array($otherSkill->id,explode(',',$user->resume_detail->other_skills)) : '') selected @endif>{{$otherSkill->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback other_skills_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <label for="personalities">Personality </label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-user-plus"></i></span>
                                                    </div>
                                                    <select class="form-control" name="personalities[]" id="personalities" multiple autocomplete="false">
                                                        @foreach($personality as $pers)
                                                            <option value="{{$pers->id}}" @if($user->resume_detail ? in_array($pers->id,explode(',',$user->resume_detail->personalities)) : '') selected @endif>{{$pers->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback personalities_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div id="step-3" data-url="{{route('validate_add_helper_step_three')}}" class="tab-pane" role="tabpanel" aria-labelledby="step-3" style="padding: 0.8rem;">


                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">
                                        Working Experience
                                    </h3>

                                    @if($user->working_experience->count() != 0)
                                        @foreach($user->working_experience as $key => $experience)
                                            <div class="row mt-3">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="job_position">Job Position * </label>
                                                        <div class="input-group" >
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa-solid fa-layer-group"></i></span>
                                                            </div>
                                                            <select class="form-control" name="job_position[]" id="job_position">
                                                                <option value="">Select Option</option>
                                                                @foreach($job_positions as $job_position)
                                                                    <option value="{{$job_position->name}}" @if($job_position->name == $experience->job_position) selected @endif>
                                                                        {{$job_position->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            <span class="invalid-feedback job_position_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="working_country">Working Country</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fas fa-globe"></i></span>
                                                            </div>
                                                            <select class="form-control" name="working_country[]" id="working_country">
                                                                @foreach($country as $w_country)
                                                                    <option value="{{$w_country->id}}"  @if($experience->working_country  == $w_country->id ) selected @endif>{{$w_country->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="invalid-feedback working_country_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="start_date">Start Date</label>
                                                        <input class="form-control" name="start_date[]" id="start_date" type="date" placeholder="Expected Monthly Salary" value="{{$experience->start_date  }}">

                                                        <span class="invalid-feedback start_date_invalid_feedback" role="alert">
                                                  <strong></strong>
                                                </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="job_end_date">End Date</label>
                                                        <input class="form-control" name="job_end_date[]" id="job_end_date" type="date" placeholder="Expected Monthly Salary" value="{{$experience->job_end_date  }}">

                                                        <span class="invalid-feedback job_end_date_invalid_feedback" role="alert">
                                                  <strong></strong>
                                                </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="employer_type">Employer Type</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa-solid fa-users"></i></span>
                                                            </div>
                                                            <select class="form-control" name="employer_type[]" id="employer_type">
                                                                <option value="">Select Option</option>
                                                                <option value="Family" @if( $experience->employer_type == "Family" ) selected @endif>Family</option>
                                                                <option value="Company" @if( $experience->employer_type == "Company" ) selected @endif>Company</option>
                                                                <option value="Other" @if( $experience->employer_type == "Other" ) selected @endif>Other</option>
                                                            </select>
                                                            <span class="invalid-feedback employer_type_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="family_type">Family type</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa fa-users"></i></span>
                                                            </div>
                                                            <select class="form-control" name="family_type[]" id="family_type">
                                                                <option value="1_adult" @if($experience->family_type == "1_adult") selected @endif>1 Adult</option>
                                                                <option value="1_adult-1_kid" @if($experience->family_type == "1_adult-1_kid") selected @endif>1 Adult + 1 Kid</option>
                                                                <option value="1_adult-2_kid" @if($experience->family_type == "1_adult-2_kid") selected @endif>1 Adult + 2 Kids</option>
                                                                <option value="1_adult-3_kid" @if($experience->family_type == "1_adult-3_kid") selected @endif>1 Adult + 3 Kids</option>
                                                                <option value="1_adult-4_kid" @if($experience->family_type == "1_adult-4_kid") selected @endif>1 Adult + 4 Kids</option>
                                                                <option value="2_adults" @if($experience->family_type == "2_adults") selected @endif>2 Adults</option>
                                                                <option value="2_adults-1_kid" @if($experience->family_type == "2_adults-1_kid") selected @endif>2 Adults + 1 Kid</option>
                                                                <option value="2_adults-2_kid" @if($experience->family_type == "2_adults-2_kid") selected @endif>2 Adults + 2 Kids</option>
                                                                <option value="2_adults-3_kid" @if($experience->family_type == "2_adults-3_kid") selected @endif>2 Adults + 3 Kids</option>
                                                                <option value="2_adults-4_kid" @if($experience->family_type == "2_adults-4_kid") selected @endif>2 Adults + 4 Kids</option>
                                                                <option value="2_adults-5_kid" @if($experience->family_type == "2_adults-5_kid") selected @endif>2 Adults + 5 Kids</option>
                                                                <option value="3_adults" @if($experience->family_type == "3_adults") selected @endif>3 Adults</option>
                                                                <option value="3_adults-1_kid" @if($experience->family_type == "3_adults-1_kid") selected @endif>3 Adults + 1 Kid</option>
                                                                <option value="3_adults-2_kid" @if($experience->family_type == "3_adults-2_kid") selected @endif>3 Adults + 2 Kids</option>
                                                                <option value="3_adults-3_kid" @if($experience->family_type == "3_adults-3_kid") selected @endif>3 Adults + 3 Kids</option>
                                                                <option value="3_adults-4_kid" @if($experience->family_type == "3_adults-4_kid") selected @endif>3 Adults + 4 Kids</option>
                                                                <option value="3_adults-5_kid" @if($experience->family_type == "3_adults-5_kid") selected @endif>3 Adults + 5 Kids</option>
                                                                <option value="4_adults" @if($experience->family_type == "4_adults") selected @endif>4 Adults</option>
                                                                <option value="4_adults-1_kid" @if($experience->family_type == "4_adults-1_kid") selected @endif>4 Adults + 1 Kid</option>
                                                                <option value="4_adults-2_kid" @if($experience->family_type == "4_adults-2_kid") selected @endif>4 Adults + 2 Kids</option>
                                                                <option value="4_adults-3_kid" @if($experience->family_type == "4_adults-3_kid") selected @endif>4 Adults + 3 Kids</option>
                                                                <option value="4_adults-4_kid" @if($experience->family_type == "4_adults-4_kid") selected @endif>4 Adults + 4 Kids</option>
                                                                <option value="4_adults-5_kid" @if($experience->family_type == "4_adults-5_kid") selected @endif>4 Adults + 5 Kids</option>
                                                                <option value="5_adults" @if($experience->family_type == "5_adults") selected @endif>5 Adults</option>
                                                                <option value="other" @if($experience->family_type == "other") selected @endif>Other</option>
                                                            </select>
                                                            <span class="invalid-feedback family_type_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 " >
                                                    <div class="form-group">
                                                        <label for="employer_nationality">
                                                            Employer Nationality
                                                        </label>
                                                        <div class="input-group" >
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fas fa-globe"></i></span>
                                                            </div>
                                                            <select class="form-control employer_nationality" name="employer_nationality[]" id="employer_nationality">
                                                                <option value="">Select employer nationality</option>
                                                                @foreach($nationalities as $e_nation)
                                                                    <option value="{{$e_nation->id}}" @if($experience->employer_nationality  == $e_nation->id) selected @endif>{{$e_nation->nationality}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="invalid-feedback employer_nationality_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="job_duties_{{$key}}">Duties *</label>
                                                        <div class="input-group" style="flex-wrap: nowrap;">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa fa-columns"></i></span>
                                                            </div>
                                                            <select class="form-control job_duties" name="job_duties[{{$key}}][]" id="job_duties_{{$key}}" multiple autocomplete="false">
                                                                @foreach($duties as $duty)
                                                                    <option value="{{$duty->id}}" @if( in_array($duty->id,explode(',',$experience->job_duties))) selected @endif>{{$duty->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <span class="invalid-feedback job_duties_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="reference_letter">Do you have reference Letter?</label>
                                                        <div class="input-group" >
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa fa-columns"></i></span>
                                                            </div>
                                                            <select class="form-control" id="reference_letter" name="reference_letter[]" autocomplete="false">
                                                                <option value="">Select Option</option>
                                                                <option value="Yes" @if( $experience->reference_letter == "Yes" ) selected @endif>Yes</option>
                                                                <option value="No" @if( $experience->reference_letter == "No" ) selected @endif>No</option>
                                                            </select>
                                                            <span class="invalid-feedback reference_letter_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 " style="text-align: end !important;">
                                                    <a href="{{ route('delete_experience',$experience) }}" class="btn next_button text-uppercase mt-4">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="job_position">Job Position * </label>
                                                    <div class="input-group" >
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa-solid fa-layer-group"></i></span>
                                                        </div>
                                                        <select class="form-control" name="job_position[]" id="job_position">
                                                            <option value="">Select Option</option>
                                                            @foreach($job_positions as $job_position)
                                                                <option value="{{$job_position->name}}" @if($job_position->name == $user->job_position) selected @endif>{{$job_position->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="invalid-feedback job_position_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="working_country">Working Country</label>
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fas fa-globe"></i></span>
                                                        </div>
                                                        <select class="form-control" name="working_country[]" id="working_country">
                                                            @foreach($country as $countries)
                                                                <option value="{{$countries->id}}">{{$countries->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="invalid-feedback working_country_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="start_date">Start Date</label>
                                                    <input class="form-control" name="start_date[]" id="start_date" type="date" placeholder="Expected Monthly Salary">

                                                    <span class="invalid-feedback start_date_invalid_feedback" role="alert">
                                                  <strong></strong>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="job_end_date">End Date</label>
                                                    <input class="form-control" name="job_end_date[]" id="job_end_date" type="date" placeholder="Expected Monthly Salary">

                                                    <span class="invalid-feedback job_end_date_invalid_feedback" role="alert">
                                                  <strong></strong>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="employer_type">Employer Type</label>
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa-solid fa-users"></i></span>
                                                        </div>
                                                        <select class="form-control" name="employer_type[]" id="employer_type">
                                                            <option value="">Select Option</option>
                                                            <option value="Family">Family</option>
                                                            <option value="Company">Company</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                        <span class="invalid-feedback employer_type_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="family_type">Family type</label>
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa fa-users"></i></span>
                                                        </div>
                                                        <select class="form-control" name="family_type[]" id="family_type">
                                                            <option value="1_adult">1 Adult</option>
                                                            <option value="1_adult-1_kid">1 Adult + 1 Kid</option>
                                                            <option value="1_adult-2_kid">1 Adult + 2 Kid</option>
                                                            <option value="1_adult-3_kid">1 Adult + 3 Kid</option>
                                                            <option value="1_adult-4_kid">1 Adult + 4 Kid</option>
                                                            <option value="2_adults">2 Adults </option>
                                                            <option value="2_adults-1_kid">2 Adults + 1 Kid</option>
                                                            <option value="2_adults-2_kid">2 Adults + 2 Kids</option>
                                                            <option value="2_adults-3_kid">2 Adults + 3 Kids</option>
                                                            <option value="2_adults-4_kid">2 Adults + 4 Kids</option>
                                                            <option value="2_adults-5_kid">2 Adults + 5 Kids</option>
                                                            <option value="3_adults">3 Adults </option>
                                                            <option value="3_adults-1_kid">3 Adults + 1 Kid</option>
                                                            <option value="3_adults-2_kid">3 Adults + 2 Kids</option>
                                                            <option value="3_adults-3_kid">3 Adults + 3 Kids</option>
                                                            <option value="3_adults-4_kid">3 Adults + 4 Kids</option>
                                                            <option value="3_adults-5_kid">3 Adults + 5 Kids</option>
                                                            <option value="4_adults">4 Adults </option>
                                                            <option value="4_adults-1_kid">4 Adults + 1 Kid</option>
                                                            <option value="4_adults-2_kid">4 Adults + 2 Kids</option>
                                                            <option value="4_adults-3_kid">4 Adults + 3 Kids</option>
                                                            <option value="4_adults-4_kid">4 Adults + 4 Kids</option>
                                                            <option value="4_adults-5_kid">4 Adults + 5 Kids</option>
                                                            <option value="5_adults">5 Adults </option>
                                                            <option value="other">Other</option>
                                                        </select>
                                                        <span class="invalid-feedback family_type_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="form-group">
                                                    <label for="employer_nationality">
                                                        Employer  Nationality
                                                    </label>
                                                    <div class="input-group" >
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fas fa-globe"></i></span>
                                                        </div>
                                                        <select class="form-control" name="employer_nationality[]" id="employer_nationality">
                                                            <option value="">Select employer nationality</option>
                                                            @foreach($nationalities as $e_nation)
                                                                <option value="{{$e_nation->id}}">{{$e_nation->nationality}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="invalid-feedback employer_nationality_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="job_duties_0">Duties *</label>
                                                    <div class="input-group" style="flex-wrap: nowrap;">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa fa-columns"></i></span>
                                                        </div>
                                                        <select class="form-control job_duties" name="job_duties[0][]" id="job_duties_0" multiple autocomplete="false">
                                                            @foreach($duties as $duty)
                                                                <option value="{{$duty->id}}">{{$duty->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <span class="invalid-feedback job_duties_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="reference_letter">Do you have reference Letter?</label>
                                                    <div class="input-group" >
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa fa-columns"></i></span>
                                                        </div>
                                                        <select class="form-control" id="reference_letter" name="reference_letter[]" autocomplete="false">
                                                            <option value="">Select Option</option>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                        </select>
                                                        <span class="invalid-feedback reference_letter_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    @endif
                                    <div id="working_experience_fields"></div>
                                    <div class="row">
                                        <div class="col-sm-12 mt-3 mb-3">
                                            <button class="btn next_button text-uppercase" type="button" onclick="working_experience_fields();" style="float: right; background-color: darkgreen !important; border-color: darkgreen !important;">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">
                                        Education
                                    </h3>

                                    @if($user->education->count() != 0)
                                        @foreach($user->education as $key_edu => $education)
                                            <div class="row mt-3">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="education">Education * </label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa-sharp fa-solid fa-database"></i></span>
                                                            </div>
                                                            <select class="form-control" name="education[]" id="education">
                                                                <option value="">Select Option</option>
                                                                @foreach($educations as $edu)
                                                                    <option value="{{$edu->name}}" @if($edu->name == $education->education) selected @endif>{{$edu->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="invalid-feedback education_invalid_feedback" role="alert">
                                                       <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="course_duration">Course Duration</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa-brands fa-discourse"></i></span>
                                                            </div>
                                                            <select class="form-control" name="course_duration[]" id="course_duration">
                                                                <option value="">Select Option</option>
                                                                <option value="1_month_course" @if($education->course_duration  == "1_month_course" ) selected @endif>1 Month Course</option>
                                                                <option value="2_months_course" @if($education->course_duration  == "2_months_course" ) selected @endif>2 Months Course</option>
                                                                <option value="3_months_course" @if($education->course_duration  == "3_months_course" ) selected @endif>3 Months Course</option>
                                                                <option value="6_months_course" @if($education->course_duration  == "6_months_course" ) selected @endif>6 Months Course</option>
                                                                <option value="1_year_course" @if($education->course_duration  == "1_year_course" ) selected @endif>1 Year Course</option>
                                                                <option value="2_years" @if($education->course_duration  == "2_years" ) selected @endif>2 Years</option>
                                                                <option value="3_years" @if($education->course_duration  == "3_years" ) selected @endif>3 Years</option>
                                                                <option value="4_years" @if($education->course_duration  == "4_years" ) selected @endif>4 Years</option>
                                                                <option value="5_years" @if($education->course_duration  == "5_years" ) selected @endif>5 Years</option>
                                                            </select>
                                                            <span class="invalid-feedback course_duration_invalid_feedback" role="alert">
                                                       <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="completed_this_course">Have you completed this course?</label>&nbsp;&nbsp;&nbsp;
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa-sharp fa-solid fa-school"></i></span>
                                                            </div>
                                                            <select class="form-control" name="completed_this_course[]" id="completed_this_course">
                                                                <option value="">Select Option</option>
                                                                <option value="Yes" @if($education->completed_this_course  == "Yes" ) selected @endif>Yes</option>
                                                                <option value="No" @if($education->completed_this_course  == "No" ) selected @endif>No</option>
                                                            </select>
                                                            <span class="invalid-feedback completed_this_course_invalid_feedback" role="alert">
                                                   <strong></strong>
                                                 </span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="year_completion">Year of Completion</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="fa-sharp fa-solid fa-calendar-days"></i></span>
                                                            </div>
                                                            <select class="form-control" name="year_completion[]" id="year_completion">
                                                                <option value="">Select Option</option>
                                                                @for ($year = 1971; $year <= 2027; $year++)
                                                                    <option value="{{ $year }}" @if($education->year_completion  == $year) selected @endif>{{ $year }}</option>
                                                                @endfor
                                                            </select>
                                                            <span class="invalid-feedback year_completion_invalid_feedback" role="alert">
                                                  <strong></strong>
                                                </span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-12 d-flex justify-content-end">
                                                    <a href="{{ route('delete_education',$education) }}" class="btn next_button text-uppercase mt-4">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="row mt-3">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="education">Education * </label>
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa-sharp fa-solid fa-database"></i></span>
                                                        </div>
                                                        <select class="form-control" name="education[]" id="education">
                                                            <option value="">Select Option</option>
                                                            @foreach($educations as $education)
                                                                <option value="{{$education->name}}" @if($education->name == $user->education) selected @endif>{{$education->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="invalid-feedback education_invalid_feedback" role="alert">
                                                       <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="course_duration">Course Duration</label>
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa-brands fa-discourse"></i></span>
                                                        </div>
                                                        <select class="form-control" name="course_duration[]" id="course_duration">
                                                            <option value="">Select Option</option>
                                                            <option value="1_month_course">1 Month Course</option>
                                                            <option value="2_months_course">2 Months Mourse</option>
                                                            <option value="3_months_course">3 Months Course</option>
                                                            <option value="6_months_course">6 Months Course</option>
                                                            <option value="1_year_course">1 Year Course</option>
                                                            <option value="2_years">2 Years</option>
                                                            <option value="3_years">3 Years</option>
                                                            <option value="4_years">4 Years</option>
                                                            <option value="5_years">5 Years</option>
                                                        </select>
                                                        <span class="invalid-feedback course_duration_invalid_feedback" role="alert">
                                                       <strong></strong>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="completed_this_course">Have you completed this course?</label>&nbsp;&nbsp;&nbsp;
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa-sharp fa-solid fa-school"></i></span>
                                                        </div>
                                                        <select class="form-control" name="completed_this_course[]" id="completed_this_course">
                                                            <option value="">Select Option</option>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                        </select>
                                                        <span class="invalid-feedback completed_this_course_invalid_feedback" role="alert">
                                                   <strong></strong>
                                                 </span>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="year_completion">Year of Completion</label>
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fa-sharp fa-solid fa-calendar-days"></i></span>
                                                        </div>
                                                        <select class="form-control" name="year_completion[]" id="year_completion">
                                                            <option value="">Select Option</option>
                                                            @for ($year = 1971; $year <= 2027; $year++)
                                                                <option value="{{ $year }}">{{ $year }}</option>
                                                            @endfor
                                                        </select>
                                                        <span class="invalid-feedback year_completion_invalid_feedback" role="alert">
                                                  <strong></strong>
                                                </span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endif


                                    <div id="education_fields"></div>
                                    <div class="row">
                                        <div class="col-sm-12 mt-3 mb-3">
                                            <button class="btn next_button text-uppercase" type="button" onclick="education_fields();" style="float: right; background-color: darkgreen !important; border-color: darkgreen !important;">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div id="step-4" data-url="{{route('validate_add_helper_step_four')}}"  class="tab-pane" role="tabpanel" aria-labelledby="step-4" style="padding: 0.8rem;">
                                    <div class="row">
                                        <div class="col-md-8 offset-2">
                                            {{--                                            <div class="form-group">--}}
                                            {{--                                                <label for="resume_description">Resume Description (Describe your working experience and your personality)*</label>--}}
                                            {{--                                                <textarea class="form-control" name="resume_description" id="resume_description"></textarea>--}}

                                            {{--                                            </div>--}}
                                            <div id="editor-container">{!! isset($user->resume_detail) ? $user->resume_detail->resume_description :''  !!}</div>
                                            <span class="invalid-feedback resume_description_invalid_feedback" role="alert">
                                                                                              <strong></strong>
                                            </span>
                                            <input type="hidden" name="resume_description">
                                        </div>

                                        <div class="col-md-8 offset-md-2 mt-5">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="newsletter" class="custom-control-input" id="switch1" value="1" @if($user->resume_detail ? $user->resume_detail->newsletter == "1" : '') checked @endif>
                                                <label class="custom-control-label" for="switch1">Subscribe to Newsletter?</label>
                                            </div>
                                        </div>

                                        <div class="col-md-8 offset-md-2">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="opportunities" class="custom-control-input" id="switch2" value="1" @if($user->resume_detail ? $user->resume_detail->opportunities == "1" : '') checked @endif>
                                                <label class="custom-control-label" for="switch2">Receive more network opportunities?</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <!-- Include optional progressbar HTML -->
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/smartwizard@6/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput.min.js"></script>
    <!-- Script -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>

    <script>
        $(function() {
            // SmartWizard initialize
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'arrows',
                justified: true,
                enableUrlHash: false,
                autoAdjustHeight: true,
                anchor: {
                    enableNavigation: false,
                },
                transition: {
                    animation: 'fade',
                },
                toolbar: {
                    showNextButton: true, // show/hide a Next button
                    showPreviousButton: true, // show/hide a Previous button
                    position: 'bottom', // none/ top/ both bottom
                    extraHtml: `<button class="btn btn-success tn-sm" id="btnFinish" disabled  style="padding: 6px">Finish</button>`
                },
            });
            $("#smartwizard").on("showStep", function(e, anchorObject, stepIndex, stepDirection, stepPosition) {
                if (stepPosition === "last") {
                    $('#btnFinish').prop('disabled', false)
                    $('.sw-btn-next').hide();
                } else {
                    $('#btnFinish').prop('disabled', true)
                    $('.sw-btn-next').show();
                }
            });
            // Leave step event is used for validating the forms
            $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIdx, nextStepIdx, stepDirection) {
                // Validate only on forward movement
                if(currentStepIdx === 0 &&  nextStepIdx === 1) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    const url = $('#step-1').data("url");
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            'first_name': $('#first_name').val(),
                            'middle_name': $('#middle_name').val(),
                            'last_name': $('#last_name').val(),
                            'age': $('#age').val(),
                            'gender': $("#gender").children("option:selected").val(),
                            'marital_status': $("#marital_status").children("option:selected").val(),
                            'kids_detail': $("#kids_detail").children("option:selected").val(),
                            'nationality': $("#nationality").children("option:selected").val(),
                            'present_country': $("#present_country").children("option:selected").val(),
                            'religion': $("#religion").children("option:selected").val(),
                            'education_level': $("#education_level").children("option:selected").val(),
                            'mobile_number': $('#mobile_number').val(),
                            'whatsapp_number': $('#whatsapp_number').val(),
                            'valid_password': $("#valid_password").children("option:selected").val(),
                        },
                        beforeSend: function (xhr) {
                            $('#step-1').find("input, select, textarea").removeClass("is-invalid");
                            $('#step-1').find("invalid-feedback").html("");
                            $('#smartwizard').smartWizard("loader", "show");
                        },
                        success: function (data) {
                            if (data.error) {
                                let messages = data.messages;
                                Object.keys(messages).forEach(function (key) {
                                    $("#" + key).addClass("is-invalid");
                                    $("." + key + "_invalid_feedback").text(messages[key]);
                                });
                                // Hide the loader
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#smartwizard').smartWizard("goToStep", 0, true);
                                $('#smartwizard').smartWizard("setState", [0], 'error');
                            }else {
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#step1').find("input, select, textarea").removeClass("is-invalid");
                                $('#smartwizard').smartWizard("unsetState", [0], 'error');
                                $('#step-1').find("span.invalid-feedback").text("");
                            }
                        }
                    });
                }
                if(currentStepIdx === 1 &&  nextStepIdx === 2) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    const url = $('#step-2').data("url");

                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            'position_apply': $('#position_apply').val(),
                            'work_experience': $('#work_experience').val(),
                            'job_type': $("#job_type").children("option:selected").val(),
                            'work_status': $("#work_status").children("option:selected").val(),
                            'job_start_date': $('#job_start_date').val(),
                            'preferred_job_location': $("#preferred_job_location").children("option:selected").val(),
                            'monthly_salary': $('#monthly_salary').val(),
                            'currency': $("#currency").children("option:selected").val(),
                            'day_off_preference': $("#day_off_preference").children("option:selected").val(),
                            'accomodation_preference': $("#accomodation_preference").children("option:selected").val(),
                            'languages': $("#languages").children("option:selected").val(),
                            'main_skills': $("#main_skills").children("option:selected").val(),
                            'cooking_skills': $("#cooking_skills").children("option:selected").val(),
                            'other_skills': $("#other_skills").children("option:selected").val(),
                            'personalities': $("#personalities").children("option:selected").val(),

                        },
                        beforeSend: function (xhr) {
                            $('#step-2').find("input, select, textarea").removeClass("is-invalid");
                            $('#step-2').find("span.invalid-feedback").text("");
                            $('#smartwizard').smartWizard("loader", "show");
                        },
                        success: function (data) {
                            if (data.error) {
                                let messages = data.messages;
                                Object.keys(messages).forEach(function (key) {
                                    $("#" + key).addClass("is-invalid");
                                    $("." + key + "_invalid_feedback").text(messages[key]);
                                });
                                // Hide the loader
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#smartwizard').smartWizard("goToStep", 1, true);
                                $('#smartwizard').smartWizard("setState", [1], 'error');
                            }else {
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#step2').find("input, select, textarea").removeClass("is-invalid");
                                $('#smartwizard').smartWizard("unsetState", [1], 'error');
                                $('#step2').find("span.invalid-feedback").text("");
                            }
                        }
                    });
                }
                if(currentStepIdx === 2 &&  nextStepIdx === 3) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    const url = $('#step-3').data("url");
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            'job_position': $('#job_position').children("option:selected").val(),
                            'working_country': $('#working_country').children("option:selected").val(),
                            'start_date': $("#start_date").val(),
                            'job_end_date': $('#job_end_date').val(),
                            'employer_type': $("#employer_type").children("option:selected").val(),
                            'family_type': $("#family_type").children("option:selected").val(),
                            'employer_nationality': $("#employer_nationality").children("option:selected").val(),
                            'job_duties': $("#job_duties_0").children("option:selected").val(),
                            'reference_letter': $('#reference_letter').children("option:selected").val(),
                            'education': $("#education").children("option:selected").val(),
                            'course_duration': $("#course_duration").children("option:selected").val(),
                            'completed_this_course': $("#completed_this_course").children("option:selected").val(),
                            'year_completion': $("#year_completion").children("option:selected").val(),
                        },
                        beforeSend: function (xhr) {
                            $('#step-3').find("input, select, textarea").removeClass("is-invalid");
                            $('#smartwizard').smartWizard("loader", "show");
                            $('#step-3').find("span.invalid-feedback").text("");
                        },
                        success: function (data) {
                            if (data.error) {
                                let messages = data.messages;
                                Object.keys(messages).forEach(function (key) {
                                    $("#" + key).addClass("is-invalid");
                                    $("." + key + "_invalid_feedback").text(messages[key]);
                                });
                                // Hide the loader
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#smartwizard').smartWizard("goToStep", 2, true);
                                $('#smartwizard').smartWizard("setState", [2], 'error');
                            }else {
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#step3').find("input, select, textarea").removeClass("is-invalid");
                                $('#smartwizard').smartWizard("unsetState", [2], 'error');
                                $('#step3').find("span.invalid-feedback").text("");
                            }
                        }
                    });
                }
                if(currentStepIdx === 3 &&  nextStepIdx === 4) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    const url = $('#step-4').data("url");
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            'resume_description': $("#resume_description").val(),
                        },
                        beforeSend: function (xhr) {
                            $('#step-4').find("input, select, textarea").removeClass("is-invalid");
                            $('#smartwizard').smartWizard("loader", "show");
                        },
                        success: function (data) {
                            if (data.error) {
                                let messages = data.messages;
                                Object.keys(messages).forEach(function (key) {
                                    $("#" + key).addClass("is-invalid");
                                    $("." + key + "_invalid_feedback").text(messages[key]);
                                });
                                // Hide the loader
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#smartwizard').smartWizard("goToStep", 3, true);
                                $('#smartwizard').smartWizard("setState", [3], 'error');
                            }else {
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#step4').find("input, select, textarea").removeClass("is-invalid");
                                $('#smartwizard').smartWizard("unsetState", [3], 'error');
                            }
                        }
                    })
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#preferred_job_location').select2({
                width: '100%',
                placeholder : "Preferred location ",
                allowClear: true,
                multiple: true,
            });
            $('#languages').select2({
                width: '100%',
                placeholder : "Select Languages",
                allowClear: true,
                multiple: true,
            });
            $('#main_skills').select2({
                width: '100%',
                placeholder : "Select Main Skills",
                allowClear: true,
                multiple: true,
            });
            $('#cooking_skills').select2({
                width: '100%',
                placeholder : "Select Cooking Skills",
                allowClear: true,
                multiple: true,
            });
            $('#other_skills').select2({
                width: '100%',
                placeholder : "Select Other Skills",
                allowClear: true,
                multiple: true,
            });
            $('#personalities').select2({
                width: '100%',
                placeholder : "Select Personalities",
                allowClear: true,
                multiple: true,
            });

        });
    </script>
    <script>
        function selectJobDutiesRefresh() {
            $('.job_duties').select2({
                width: '100%',
                placeholder : "Select Duties *",
                allowClear: true,
                multiple: true,
            });
        }
        selectJobDutiesRefresh();
        let room1 = 1;
        @if($next_experience_iteration > 0)
            room1 = {{$next_experience_iteration}}
            @endif
            function working_experience_fields() {
            var objTo = document.getElementById("working_experience_fields");
            var divtest1 = document.createElement("div");
            divtest1.setAttribute("class", "removeclass" + room1);
            var rdiv = "removeclass" + room1;
            divtest1.innerHTML = `
                                   <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="job_position">Job Position * </label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa-solid fa-layer-group"></i></span>
                                                    </div>
                                                    <select class="form-control" name="job_position[]" id="job_position" required>
                                                        <option value="">Select Option</option>
                                                        @foreach($job_positions as $job_position)
            <option value="{{$job_position->name}}">{{$job_position->name}}</option>
                                                                @endforeach
            </select>
            <span class="invalid-feedback job_position_invalid_feedback" role="alert">
                  <strong></strong>
            </span>
        </div>
    </div>

</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="working_country">Working Country</label>
        <div class="input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" ><i class="fas fa-globe"></i></span>
            </div>
            <select class="form-control" name="working_country[]" id="working_country" required>
@foreach($country as $countries)
            <option value="{{$countries->id}}">{{$countries->name}}</option>
                                                        @endforeach
            </select>
            <span class="invalid-feedback working_country_invalid_feedback" role="alert">
                  <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="start_date">Start Date</label>
        <input class="form-control" name="start_date[]" id="start_date" type="date" placeholder="Expected Monthly Salary" required>

        <span class="invalid-feedback start_date_invalid_feedback" role="alert">
          <strong></strong>
        </span>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="job_end_date">End Date</label>
        <input class="form-control" name="job_end_date[]" id="job_end_date" type="date" placeholder="Expected Monthly Salary">

        <span class="invalid-feedback question_5_invalid_feedback" role="alert">
          <strong></strong>
        </span>
    </div>
</div>


<div class="col-md-6">
    <div class="form-group">
        <label for="employer_type">Employer Type</label>
        <div class="input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" ><i class="fa-solid fa-users"></i></span>
            </div>
              <select class="form-control" name="employer_type[]" id="employer_type" required>
                <option value="">Select Option</option>
                <option value="Family">Family</option>
                <option value="Company">Company</option>
                <option value="Other">Other</option>
              </select>
            <span class="invalid-feedback employer_type_invalid_feedback" role="alert">
                  <strong></strong>
            </span>
        </div>
    </div>
</div>


<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="family_type">Family type</label>
                                                    <div class="input-group ">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fa fa-users"></i></span>
                                                        </div>
                                                        <select class="form-control" name="family_type[]" id="family_type">
                                                            <option value="1_adult">1 Adult</option>
                                                            <option value="1_adult-1_kid">1 Adult + 1 Kid</option>
                                                            <option value="1_adult-2_kid">1 Adult + 2 Kid</option>
                                                            <option value="1_adult-3_kid">1 Adult + 3 Kid</option>
                                                            <option value="1_adult-4_kid">1 Adult + 4 Kid</option>
                                                            <option value="2_adults">2 Adults </option>
                                                            <option value="2_adults-1_kid">2 Adults + 1 Kid</option>
                                                            <option value="2_adults-2_kid">2 Adults + 2 Kids</option>
                                                            <option value="2_adults-3_kid">2 Adults + 3 Kids</option>
                                                            <option value="2_adults-4_kid">2 Adults + 4 Kids</option>
                                                            <option value="2_adults-5_kid">2 Adults + 5 Kids</option>
                                                            <option value="3_adults">3 Adults </option>
                                                            <option value="3_adults-1_kid">3 Adults + 1 Kid</option>
                                                            <option value="3_adults-2_kid">3 Adults + 2 Kids</option>
                                                            <option value="3_adults-3_kid">3 Adults + 3 Kids</option>
                                                            <option value="3_adults-4_kid">3 Adults + 4 Kids</option>
                                                            <option value="3_adults-5_kid">3 Adults + 5 Kids</option>
                                                            <option value="4_adults">4 Adults </option>
                                                            <option value="4_adults-1_kid">4 Adults + 1 Kid</option>
                                                            <option value="4_adults-2_kid">4 Adults + 2 Kids</option>
                                                            <option value="4_adults-3_kid">4 Adults + 3 Kids</option>
                                                            <option value="4_adults-4_kid">4 Adults + 4 Kids</option>
                                                            <option value="4_adults-5_kid">4 Adults + 5 Kids</option>
                                                            <option value="5_adults">5 Adults </option>
                                                            <option value="other">Other</option>
                                                        </select>
                                                        <span class="invalid-feedback family_type_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 " >
                                                <div class="form-group">
                                                    <label for="employer_nationality">
                                                        Employer  Nationality
                                                    </label>
                                                    <div class="input-group" >
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" ><i class="fas fa-globe"></i></span>
                                                        </div>
                                                        <select class="form-control" name="employer_nationality[]" id="employer_nationality">
                                                            <option value="">Select employer nationality</option>
                                                            @foreach($nationalities as $nation)
            <option value="{{$nation->id}}">{{$nation->nationality}}</option>
                                                            @endforeach
            </select>
            <span class="invalid-feedback employer_nationality_invalid_feedback" role="alert">
              <strong></strong>
        </span>
        </div>
    </div>
</div>





<div class="col-md-6">
<div class="form-group">
<label for="job_duties_${room1}">Duties *</label>
        <div class="input-group" style="flex-wrap: nowrap;">
            <div class="input-group-prepend">
                <span class="input-group-text" ><i class="fa fa-columns"></i></span>
            </div>
            <select class="form-control job_duties" name="job_duties[${room1}][]" id="job_duties_${room1}" multiple autocomplete="false" required>
@foreach($duties as $duty)
            <option value="{{$duty->id}}">{{$duty->name}}</option>
                                                        @endforeach
            </select>
            <span class="invalid-feedback job_duties_invalid_feedback" role="alert">
                  <strong></strong>
            </span>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="reference_etter">Do you have reference Letter?</label>
        <div class="input-group" style="flex-wrap: nowrap;">
            <div class="input-group-prepend">
                <span class="input-group-text" ><i class="fa fa-columns"></i></span>
            </div>
            <select class="form-control reference_etter" name="reference_etter[]" autocomplete="false" required>
                 <option value="">Select Option</option>
                 <option value="Yes">Yes</option>
                 <option value="No">No</option>
            </select>
            <span class="invalid-feedback reference_etter_invalid_feedback" role="alert">
                  <strong></strong>
            </span>
        </div>
    </div>
</div>

   <div class="col-md-6" style="text-align: end !important;">
       <div class="contact_10">
           <button class="btn next_button text-uppercase mt-3" type="button" onclick="remove_working_experience_fields(${room1});" style="float: right">
                                                              <i class="fa fa-trash"></i>
                                                           </button>
                                                    </div>
                                                </div>
                                            </div>

                                     <div class="clear"></div>`;
            objTo.appendChild(divtest1);
            selectJobDutiesRefresh();
            room1++;
        }

        function remove_working_experience_fields(rid) {
            $(".removeclass" + rid).remove();
            room1--;
        }
    </script>
    <!-- for education -->
    <script>
        let edu = 1;
        @if($next_education_iteration > 0)
            edu = {{$next_education_iteration}}
            @endif
            function education_fields() {
            var objTo = document.getElementById("education_fields");
            var divtest1 = document.createElement("div");
            divtest1.setAttribute("class", "removeEdu" + edu);
            var rdiv = "removeEdu" + edu;
            divtest1.innerHTML = `<div class="row mt-3">
                  <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="education">Education * </label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa-sharp fa-solid fa-database"></i></span>
                                                    </div>
                                                      <select class="form-control" name="education[]" id="education" required>
                                                        <option value="">Select Option</option>
                                                        @foreach($educations as $education)
            <option value="{{$education->name}}">{{$education->name}}</option>
                                                                @endforeach
            </select>
          <span class="invalid-feedback education_invalid_feedback" role="alert">
             <strong></strong>
          </span>
      </div>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
      <label for="course_duration">Course Duration</label>
      <div class="input-group ">
          <div class="input-group-prepend">
              <span class="input-group-text" ><i class="fa-brands fa-discourse"></i></span>
          </div>
            <select class="form-control" name="course_duration[]" id="course_duration">
              <option value="">Select Option</option>
              <option value="1_month_course">1 Month Course</option>
              <option value="2_months_course">2 Months Mourse</option>
              <option value="3_months_course">3 Months Course</option>
              <option value="6_months_course">6 Months Course</option>
              <option value="1_year_course">1 Year Course</option>
              <option value="2_years">2 Years</option>
              <option value="3_years">3 Years</option>
              <option value="4_years">4 Years</option>
              <option value="5_years">5 Years</option>
            </select>
          <span class="invalid-feedback course_duration_invalid_feedback" role="alert">
             <strong></strong>
          </span>
      </div>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
      <label for="completed_this_course">Have you completed this course?</label>&nbsp;&nbsp;&nbsp;
      <div class="input-group ">
        <div class="input-group-prepend">
            <span class="input-group-text" ><i class="fa-sharp fa-solid fa-school"></i></span>
        </div>
          <select class="form-control" name="completed_this_course[]" id="completed_this_course" required>
             <option value="">Select Option</option>
             <option value="Yes">Yes</option>
             <option value="No">No</option>
         </select>
      </div>
       <span class="invalid-feedback completed_this_course_invalid_feedback" role="alert">
         <strong></strong>
       </span>
  </div>
</div>

<div class="col-md-6">
  <div class="form-group">
      <label for="year_completion">Year of completion</label>
      <div class="input-group ">
        <div class="input-group-prepend">
            <span class="input-group-text" ><i class="fa-sharp fa-solid fa-calendar-days"></i></span>
        </div>
      <select class="form-control" name="year_completion[]" id="year_completion" required>
          <option value="">Select Option</option>
@for ($year = 1971; $year <= 2027; $year++)
            <option value="{{ $year }}">{{ $year }}</option>
                                                    @endfor

            </select>
            </div>
            <span class="invalid-feedback year_completion_invalid_feedback" role="alert">
              <strong></strong>
            </span>
        </div>
    </div>
     <div class="col-sm-12 mb-4">
         <div class="col-sm-12">
             <div class="contact_10">
                 <button class="btn next_button text-uppercase mt-3" type="button" onclick="remove_education_fields(${edu});" style="float: right">
                                                                    <i class="fa fa-trash"></i>
                                                               </button>
                                                    </div>
                                                  </div>
                                              </div>
                                          </div>
                                 <div class="clear"></div>`;
            objTo.appendChild(divtest1);
            edu++;
        }
        function remove_education_fields(rid) {
            $(".removeEdu" + rid).remove();
            edu--;
        }
    </script>
    {{--  phone no with country code --}}
    <script>
        let mobile_number = document.querySelector("#mobile_number");
        let whatsapp_number = document.querySelector("#whatsapp_number");

        const errorMsg = document.querySelector("#mobile-number-error-msg");
        const validMsg = document.querySelector("#mobile-number-valid-msg");
        const whatsapp_number_error_msg = document.querySelector("#whatsapp-number-error-msg");
        const whatsapp_number_valid_msg = document.querySelector("#whatsapp-number-valid-msg");

        const errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        const phone_no_ins = intlTelInput(mobile_number, {
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.js",
        });

        const whatsapp_number_ins = intlTelInput(whatsapp_number, {
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.js",
        });

        const reset = () => {
            mobile_number.classList.remove("is-invalid");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        const whatsapp_reset = () => {
            whatsapp_number.classList.remove("is-invalid");
            whatsapp_number_error_msg.innerHTML = "";
            whatsapp_number_valid_msg.classList.add("hide");
            whatsapp_number_valid_msg.classList.add("hide");
        };

        mobile_number.addEventListener('blur', () => {
            reset();
            if (mobile_number.value.trim()) {
                if (phone_no_ins.isValidNumber()) {
                    validMsg.classList.remove("hide");
                    var number = phone_no_ins.getNumber(intlTelInputUtils.numberFormat.E164);
                    $("#mobile_number").val(number);
                } else {
                    mobile_number.classList.add("is-invalid");
                    const errorCode = phone_no_ins.getValidationError();
                    if(errorCode === -99){
                        errorMsg.innerHTML = "Invalid Number";
                        errorMsg.classList.remove("hide");
                    }else{
                        errorMsg.innerHTML = errorMap[errorCode];
                        errorMsg.classList.remove("hide");
                    }
                }
            }
        });
        whatsapp_number.addEventListener('blur', () => {
            whatsapp_reset();
            if (whatsapp_number.value.trim()) {
                if (whatsapp_number_ins.isValidNumber()) {
                    whatsapp_number_valid_msg.classList.remove("hide");
                    var number = whatsapp_number_ins.getNumber(intlTelInputUtils.numberFormat.E164);
                    $("#whatsapp_number").val(number);
                } else {
                    whatsapp_number.classList.add("is-invalid");
                    const errorCode = whatsapp_number_ins.getValidationError();
                    if(errorCode === -99){
                        whatsapp_number_error_msg.innerHTML = "Invalid Number";
                        whatsapp_number_error_msg.classList.remove("hide");
                    }else{
                        whatsapp_number_error_msg.innerHTML = errorMap[errorCode];
                        whatsapp_number_error_msg.classList.remove("hide");
                    }
                }
            }
        });

        // on keyup / change flag: reset
        mobile_number.addEventListener('change', reset);
        mobile_number.addEventListener('keyup', reset);
        whatsapp_number.addEventListener('change', reset);
        whatsapp_number.addEventListener('keyup', reset);

        var toolbarOptions = [
            ['bold', 'italic', 'underline'],        // toggled buttons
            ['blockquote'],
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        ];

        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: toolbarOptions,
            },
            placeholder: 'Resume Description',
            theme: 'snow'
        });

        $( "#register-form").on( "submit", function( event ) {
            var resume_description = document.querySelector('input[name=resume_description]');
            resume_description.value = quill.root.innerHTML;

            var text = quill.getText().trim();


            // Check if the text is empty
            if (text.length === 0) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 2500,
                    timerProgressBar: true,
                });
                Toast.fire({
                    icon: 'error',
                    title: 'Please Enter resume description'
                })
                return false;
            }

            if (phone_no_ins.isValidNumber()) {
                var number = phone_no_ins.getNumber(intlTelInputUtils.numberFormat.E164);
                document.querySelector("#formatted-mobile").value = number;
            }


            if (whatsapp_number_ins.isValidNumber()) {
                var whatsapp_number = whatsapp_number_ins.getNumber(intlTelInputUtils.numberFormat.E164);
                document.querySelector("#formatted-whatsapp-number").value = whatsapp_number;
            }

        });


    </script>
@endpush
