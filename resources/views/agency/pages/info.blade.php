@extends('agency.layouts.app')
@section('title','Info')
@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />

    <style>
        .hide{
            display: none;
        }
        .valid-msg {
            color: #00c900;
        }
        .error-msg {
            color: red;
        }
        .mobile_label:after{
            content: "";
            position: absolute;
            width: 100px;
            inset-inline-start: 0;
            top: 0.35rem;
            z-index: -1;
            background-color: #fff  !important;
            inset: 0.8555rem 0.5rem;
        }

        .iti__country-list {
            z-index: 3 !important;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #ced4da !important;
        }
        .select2-container .select2-selection--multiple {
            min-height: 37px !important;
        }
        .select2-container--default .select2-selection--multiple {
            border: 1px solid #ced4da !important
        }
        .select2-container--default .select2-search--inline .select2-search__field {
            border: unset !important;
        }
    </style>
@endpush
@section('content')
    <style>
        .blog-ar{
            margin-top: 90px;
        }
    </style>

    <section _ngcontent-serverapp-c3727738402 class="agency news-section pb-5 custom-container page-section blog-ar">
        <div _ngcontent-serverapp-c3727738402 class="container mt-5">
            <form method="post" action="{{route('info_save')}}" enctype="multipart/form-data" id="updateInfo">
                @csrf
                <div class="row mt-5">
                    <div class="col-md-12 text-center">
                        <h3>Agency Info</h3>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="strength">Agency Strength</label>
                            <select class="form-control @error('strength') is-invalid @enderror" name="strength[]" id="strength" multiple style="border: unset">
                                @foreach($strengths as $strength)
                                    <option value="{{ $strength->id }}"   @if( in_array($strength->id,explode(',',$info->strength ?? null)) ) selected @endif >{{$strength->name}}</option>
                                @endforeach
                            </select>
                            @error('strength')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="speak_language">Speak Language</label>
                            <select class="form-control @error('speak_language') is-invalid @enderror" name="speak_language[]" id="speak_language" multiple style="border: unset">
                                @foreach($languages as $lang)
                                    <option value="{{ $lang->id }}" @if( in_array($lang->id,explode(',',$info->speak_language ?? null)) ) selected @endif>{{ $lang->name }}</option>
                                @endforeach
                            </select>
                            @error('speak_language')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_AGENCY &&
                         \Illuminate\Support\Facades\Auth::user()->agency_role == \App\Models\User::AGENCY )

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="help_to_hire">Help to Hire</label>
                                <select class="form-control @error('help_to_hire') is-invalid @enderror" name="help_to_hire[]" id="help_to_hire" multiple style="border: unset">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}"  @if( in_array($country->id,explode(',',$info->help_to_hire ?? null)) ) selected @endif>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                @error('help_to_hire')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="agency_expert">Agency Expert</label>
                                <select class="form-control @error('agency_expert') is-invalid @enderror" name="agency_expert[]" id="agency_expert" multiple style="border: unset">
                                    @foreach($experts as $expert)
                                        <option value="{{ $expert->id }}" @if( in_array($expert->id,explode(',',$info->agency_expert ?? null)) ) selected @endif >{{ $expert->name }}</option>
                                    @endforeach
                                </select>
                                @error('agency_expert')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    @endif


                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="agency_expert">License No</label>
                            <input type="text" class="form-control @error('license_no') is-invalid @enderror" name="license_no"
                                   placeholder="license no"
                                   value="{{ $info->license_no ?? '' }}"
                            >
                            @error('license_no')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="open_time">Open Time</label>
                            <input type="text" class="form-control @error('open_time') is-invalid @enderror" name="open_time"
                                   placeholder="open time"
                                   value="{{ $info->open_time ?? '' }}"
                            >
                            @error('open_time')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="day_off">Day Off</label>
                            <input type="text" class="form-control @error('day_off') is-invalid @enderror" name="day_off"
                                   placeholder="day off"
                                   value="{{ $info->day_off ?? '' }}"
                            >
                            @error('day_off')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    {{--                    <div class="col-md-6">--}}
                    {{--                        <div class="form-group">--}}
                    {{--                            <label for="agency_services">Agency Services</label>--}}
                    {{--                            <select class="form-control @error('agency_services') is-invalid @enderror" name="agency_services[]" id="agency_services" multiple style="border: unset">--}}
                    {{--                                @foreach($services as $service)--}}
                    {{--                                    <option value="{{ $service->id }}" @if( in_array($service->id,explode(',',$info->agency_services ?? null)) ) selected @endif >{{ $expert->name }}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                            @error('agency_services')--}}
                    {{--                            <div class="invalid-feedback">{{ $message }}</div>--}}
                    {{--                            @enderror--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control @error('address') is-invalid @enderror" name="address"
                                   placeholder="address"
                                   value="{{ $info->address ?? '' }}"
                            >
                            @error('address')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="address">Company Image</label>
                            <input type="file" class="form-control-file @error('company_image') is-invalid @enderror" name="company_image"
                                   accept="image/*"
                            >
                            @error('company_image')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="company_banner_image">Company Banner Image</label>
                            <input type="file" class="form-control-file @error('company_banner_image') is-invalid @enderror"
                                   name="company_banner_image"
                                   accept="image/*"
                            >
                            @error('company_banner_image')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label for="company_map_iframe">Company Map Iframe</label>
                            <textarea class="form-control @error('company_map_iframe') is-invalid @enderror"
                                      name="company_map_iframe" id="company_map_iframe" rows="6" placeholder="company map iframe">{{  $info->company_map_iframe ?? null }}</textarea>
                            @error('company_map_iframe')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="company_short_intro">Company Short Introduction</label>
                            <textarea class="form-control @error('company_short_intro') is-invalid @enderror"
                                      name="company_short_intro" id="company_short_intro" rows="6" placeholder="company short introduction">{{  $info->company_short_intro ?? null }}</textarea>
                            @error('company_short_intro')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6 mb-3">
                        <label for="agency_detail">Agency Detail</label>
                        <div id="editor-container">{!! isset($info) ? $info->agency_detail : '' !!}</div>
                        <input type="hidden" name="agency_detail"   value="{{ $info->agency_detail ?? '' }}">
                        @error('agency_detail')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>



                    <div class=" col-md-12 mt-5" style="text-align: end">
                        <button type="submit" class="btn next_button text-uppercase mt-3 text-white">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#strength').select2({
                width: '100%',
                placeholder : "Agency Strength ",
                allowClear: true,
                multiple: true,
            });
            $('#speak_language').select2({
                width: '100%',
                placeholder : "Agency Language ",
                allowClear: true,
                multiple: true,
            });
            $('#help_to_hire').select2({
                width: '100%',
                placeholder : "Help to hire ",
                allowClear: true,
                multiple: true,
            });
            $('#agency_expert').select2({
                width: '100%',
                placeholder : "Agency Expert",
                allowClear: true,
                multiple: true,
            });
            $('#agency_services').select2({
                width: '100%',
                placeholder : "Agency Services",
                allowClear: true,
                multiple: true,
            });

            var toolbarOptions = [
                ['bold', 'italic', 'underline'],        // toggled buttons
                ['blockquote'],
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            ];

            var quill = new Quill('#editor-container', {
                modules: {
                    toolbar: toolbarOptions,
                },
                placeholder: 'Agency Detail',
                theme: 'snow'
            });

            $( "#updateInfo").on( "submit", function( event ) {
                const agency_detail = document.querySelector('input[name=agency_detail]');
                agency_detail.value = quill.root.innerHTML;

            });
        });
    </script>


    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Check if there is a session variable for password_tab
            if ("{{ session('password_tab') }}") {
                // Activate the tab
                var passwordTab = new bootstrap.Tab(document.querySelector('#password-tab'));
                passwordTab.show();
            }
        });
    </script>




@endpush
