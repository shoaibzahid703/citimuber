<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>@yield('title') - {{env('APP_NAME')}}</title>
    <link rel="canonical" href="https://www.site.com" />

    <base href="." />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="preload" href="{{asset('cdn-sub/web-asset/fonts/proximanova-regular-webfont.woff')}}')}}" as="font" type="font/woff2" crossorigin />
    <style>
        @font-face {
            font-family: "FontAwesome";
            src: url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.eot')}}");
            src: url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.eot')}}") format("embedded-opentype"), url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.woff')}}") format("woff2"),
            url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.woff')}}") format("woff"), url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.ttf')}}") format("truetype"), url("{{asset('cdn-sub/web-asset/fonts/fontawesome-webfont.svg')}}") format("svg");
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }
        @font-face {
            font-family: "proxima_nova_rgregular";
            src: url("https://cdn.Citimuber.com/web-asset/fonts/proximanova-regular-webfont.woff") format("woff"), url("{{asset('cdn-sub/web-asset/fonts/proximanova-regular-webfont.ttf')}}") format("truetype");
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }
        @font-face {
            font-family: "proxima_nova_rgbold";
            src: url("{{asset('cdn-sub/web-asset/fonts/proximanova-bold-webfont.woff')}}") format("woff"), url("{{asset('cdn-sub/web-asset/fonts/proximanova-bold-webfont.ttf')}}");
            font-weight: normal;
            font-style: normal;
            font-display: swap;
        }
        @media only screen and (max-width: 991px){
            .set-modal{
                margin-top: 100px !important;
            }
        }

    </style>


    @if(isset($settings->site_fav_icon))
        <link href="{{asset('storage/site_fav_icon/'.$settings->site_fav_icon)}}"  rel="icon" type="image/x-icon" >
    @else
        <link href="{{asset('cdn-sub/front-app/favicon.jpg')}}"  rel="icon" type="image/x-icon" >
    @endif


    <!-- <link rel="stylesheet" href="/assets/images/icon/icon.css"> -->
    <link rel="manifest" href="{{asset('cdn-sub/front-app/manifest.json')}}" />
    <link rel="preload" href="https://cdn.Citimuber.com/web-asset/fonts/proximanova-regular-webfont.woff" as="font" type="font/woff2" crossorigin />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <style>
        .navbar-brand img {
            height: 45px;
        }
        .navbar {
            padding: 10px 20px;
        }
        .navbar-nav .nav-link {
            margin-right: 20px;
        }
        .form-inline .btn {
            margin-right: 10px;
        }
        @media (max-width: 992px) {
            .navbar-nav {
                text-align: left;
            }
            .navbar-nav .nav-link {
                margin: 0;
            }
            .navbar-nav .dropdown-menu {
                position: static;
                float: none;
                width: auto;
                margin-top: 0;
            }
            .form-inline {
                display: flex;
                justify-content: center;
                margin-top: 10px;
            }
        }
    </style>


    <link rel="stylesheet" href="{{asset('cdn-sub/front-app/styles.1adfe7432471cc54.css')}}" media="print" onload="this.media='all'" />
    <noscript><link rel="stylesheet" href="{{asset('cdn-sub/front-app/styles.1adfe7432471cc54.css')}}" /></noscript>
    <link rel="stylesheet" href="{{asset('admin/vendor/libs/toastr/toastr.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/sweetalert.css')}}">
    @yield('css')

    <link rel="stylesheet" href="{{asset('cdn-sub/front-app/custom.css')}}" />
    <link rel="stylesheet" href="{{asset('cdn-sub/front-app/core.css')}}" />
    @if(isset($settings->site_description))
        <meta name="description" content="{{$settings->site_description}}">
    @else
        <meta name="description" content="Citimuber connects employers and domestic helpers in Hong Kong, Macau Singapore, United Arab Emirates &amp; Saudi Arabia, in just a few clicks." />
    @endif
    <meta name="Publisher" content="Citimuber" />

    <meta name="type" content="website" />
    <meta name="image" content="{{asset('cdn-sub/img.png')}}" />
    @if(isset($settings->seo_tags))
        <meta name="keywords" content="{{$settings->seo_tags}}">
    @else
        <meta name="keywords" content="happy,happy employers,employers,Citimuber" />
    @endif

    <meta name="copyright" content="Citimuber" />
    <meta name="MobileOptimized" content="320" />
    <meta name="robots" content="all" />


    <meta property="og:title" content="Domestic Helpers &amp; Maids in Hong Kong &amp; More - Citimuber" />
    @if(isset($settings->site_description))
        <meta property="og:description" content="{{$settings->site_description}}" />
    @else
        <meta property="og:description" content="Citimuber connects employers and domestic helpers in Hong Kong, Macau Singapore, United Arab Emirates &amp; Saudi Arabia, in just a few clicks." />
    @endif
    <meta property="og:Publisher" content="Citimuber" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{asset('cdn-sub/img.png')}}" />
    <meta property="og:site_name" content="https://www.Citimuber.com" />
    @stack('js')

</head>

<body id="main-body" class="cutom-class">
<!--nghm-->


<style>
    .login_btn{
        border: 2px solid #b50000;
        border-radius: 6px;
        background-color: #b50000;
        color: #fff !important;
        letter-spacing: 1px;
        text-align: center;
        float: right;
        font-weight: 600;
        font-size: 18px;
    }
    .register_btn{
        border: 2px solid #000;
        border-radius: 6px;
        background-color: #000;
        color: #fff !important;
        letter-spacing: 1px;
        text-align: center;
        float: left;
        font-weight: 600;
        font-size: 18px;
    }
    a.nav-link{
        font-weight: 600 !important;
        font-size: 18px !important;
        color: black !important;
    }

    a.nav-link:hover{
        font-weight: 600 !important;
        font-size: 18px !important;
        color: white !important;
    }
    .nav-tabs a.active {
        font-weight: 600 !important;
        font-size: 18px !important;
        color: white !important;
    }
    .sticky-top{
        width: 100%;
        position: fixed;
        top: 0;
        z-index: 10000000;
    }
    .dropdown-item{
        padding: 1.25rem 3rem !important;
    }
    .navbar-light .navbar-toggler{
        border: none !important;
    }
</style>

<section class="web_homePage">
    @include('frontend.includes.navbar')
    @yield('content')
    @include('frontend.includes.footer')
    <div id="color"></div>
    <div id="font"></div>

    <!-- login modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content set-modal">
                <div class="modal-header">
                    <img src="{{asset('assets/images/site-logo.jpg')}}" class="w-50">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mt-3">
                        <div class="col-6 offset-3 select-role">
                            <!-- Add a span to hold the dynamic text -->
                            <h4 class="text-center font-weight-bold align-self-center">
                                <span >Login Account</span>
                            </h4>
                        </div>
                    </div>

                    <div class="row mt-3 pt-3 pb-3">
                        <div class="col-12">
                            <div id="loginAlert" class="alert alert-danger d-none"></div>
                            <form class="helper-form mt-3" id="loginForm">
                                @csrf
                                <div class="form-group">
                                    <label for="email" style="color:black !important;">Email address</label>
                                    <input type="email" name="email" class="form-control " id="loginEmail" aria-describedby="emailHelp" autocomplete="off">
                                    <div id="email-error" class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                    <label for="password" style="color:black !important;">Password</label>
                                    <input type="password" name="password" class="form-control" id="loginPassword">
                                    <div id="password-error" class="invalid-feedback"></div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember" style="color:black !important;">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="text-uppercase mt-3 js-btn-next" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn login_btn text-uppercase mt-3" style="border-radius: 30px; float: unset">Login</button>
                                    </div>
                                    <div class="col-md-12 text-center mt-2">
                                        <a data-toggle="modal" data-target="#registerModal" data-dismiss="modal" class="text-center font-weight-bold">Sign Up Here</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- register modal -->

    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="{{asset('assets/images/site-logo.jpg')}}" class="w-50">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mt-3">
                        <div class="col-6 offset-3 select-role">
                            <!-- Add a span to hold the dynamic text -->
                            <h4 class="text-center font-weight-bold align-self-center">
                                <span id="roleText">Select Your Role</span>
                            </h4>
                        </div>
                    </div>

                    <!-- Add id to the row containing form links -->
                    <div class="row mt-3 pt-3 pb-3" id="formLinksRow">
                        <div class="col-6">
                            <a id="helperForm" class="role-link">
                                <div class="card looking-job h-100">
                                    <div class="card-body">
                                        <center><img src="{{asset('assets/images/helper-job.png')}}" class="imag-fluid d-block"></center>
                                        <h3 style=" font-size: 19px !important; " class="font-weight-bold text-white text-center mt-3">I am looking for a job</h3>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-6">
                            <a id="employerForm" class="role-link">
                                <div class="card looking-employer h-100">
                                    <div class="card-body">
                                        <center><img src="{{asset('assets/images/helper-display.png')}}" class="imag-fluid d-block"></center>
                                        <h3 style=" font-size: 19px !important; " class="font-weight-bold text-white mt-3">I am an Employer</h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <!-- Add id to the forms so they can be toggled -->
                    <div class="row mt-3 pt-3 pb-3" id="helperFormContent" style="display: none;">
                        <div class="col-12">
                            <h5 class="font-weight-bold" id="goBackHelper">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> &nbsp;&nbsp;Select Role
                            </h5>
                            <div id="alert-errors" class="alert alert-danger d-none"></div>
                            <div id="success-message" class="success-message alert alert-success d-none"></div>
                            <form class="helper-form mt-3 registerForm" data-type="helper">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" name="role" value="candidate" hidden="">
                                    <label for="helper_name" style="color:black !important;">Name</label>
                                    <input type="text" class="form-control" name="name" id="helper_name" aria-describedby="emailHelp">
                                    <div id="helper-name-error" class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                    <label for="helper_email" style="color:black !important;">Email address</label>
                                    <input type="email" name="email" class="form-control " id="helper_email" aria-describedby="emailHelp" autocomplete="off">
                                    <div id="helper-email-error" class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                    <label for="helper_password" style="color:black !important;">Password</label>
                                    <input type="password" name="password" class="form-control" id="helper_password">
                                    <div id="helper-password-error" class="invalid-feedback"></div>
                                </div>

                                <div class="form-group">
                                    <label for="helper_password_confirmation" style="color:black !important;">Confirm Password</label>
                                    <input type="password" class="form-control" id="helper_password_confirmation" name="password_confirmation">
                                    <div id="helper-password_confirmation-error" class="invalid-feedback"></div>
                                </div>


                                <div class="row">
                                    <div class="col-12">
                                        <span style="color:black;">By clicking Sign Up, you agree to the Citimuber Terms & Conditions, and Privacy Policy.</span>
                                    </div>
                                    <div class="col-12">
                                        <center><button type="submit" class="btn login_btn" style="border-radius: 30px; float: unset">Register</button></center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row mt-3 pt-3 pb-3" id="employerFormContent" style="display: none;">
                        <div class="col-12">
                            <h5 class="font-weight-bold" id="goBackEmployer">
                                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> &nbsp;&nbsp;Select Role
                            </h5>
                            <div id="success-message" class="success-message alert alert-success d-none"></div>
                            <form class="helper-form mt-3 registerForm" data-type="employer">
                                @csrf

                                <div class="form-group">
                                    <input type="text" class="form-control" name="role" value="employer" hidden="">
                                    <label for="employer_name" style="color:black !important;">Name</label>
                                    <input type="text" class="form-control " name="name" id="employer_name" aria-describedby="emailHelp">
                                    <div id="employer-name-error" class="invalid-feedback name-error"></div>
                                </div>

                                <div class="form-group">
                                    <label for="employer_email" style="color:black !important;">Email address</label>
                                    <input type="email" name="email" class="form-control " id="employer_email" aria-describedby="emailHelp" autocomplete="off">
                                    <div id="employer-email-error" class="invalid-feedback email-error"></div>
                                </div>

                                <div class="form-group">
                                    <label for="employer_password" style="color:black !important;">Password</label>
                                    <input type="password" name="password" class="form-control " id="employer_password">
                                    <div id="employer-password-error" class="invalid-feedback password-error"></div>
                                </div>

                                <div class="form-group">
                                    <label for="employer_password_confirmation" style="color:black !important;">Confirm Password</label>
                                    <input type="password" class="form-control password_confirmation" id="employer_password_confirmation" name="password_confirmation">
                                    <div id="employer-password_confirmation-error" class="invalid-feedback password_confirmation-error"></div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <span style="color:black;">By clicking Sign Up, you agree to the CITIMUBER Terms & Conditions, and Privacy Policy.</span>
                                    </div>
                                    <div class="col-12">
                                        <center><button type="submit" class="btn login_btn" style="border-radius: 30px; float: unset">Register</button></center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 text-center">
                            <a data-toggle="modal" data-target="#loginModal" data-dismiss="modal" class="text-center font-weight-bold">Already have an Account? Sign in</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{asset('admin/js/bundle.js')}}"></script>
<script src="{{asset('admin/js/sweetalert.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>



@include('frontend.includes.messages')

<script>
    $(document).ready(function() {
       $('.registerForm').on('submit', function(e) {
            e.preventDefault();
            const type = $(this).attr("data-type");

            let form = $(this);
            let actionUrl = '{{ route("register") }}';

            $('.invalid-feedback').text('');
            $('.form-control').removeClass('is-invalid');

            $.ajax({
                type: 'POST',
                url: actionUrl,
                data: form.serialize(),
                success: function(response) {
                    if (response.success) {
                        // Redirect the user to their dashboard based on their role
                        if (response.role === 'employer') {
                            window.location.href = '{{ route("employer_dashboard") }}';
                        } else if (response.role === 'candidate') {
                            window.location.href = '{{ route("bio-data") }}';
                        }
                    }
                },
                error: function(response) {
                    let errors = response.responseJSON.errors;
                    if (errors) {
                        for (let field in errors) {
                            $('#'+ type +'_'+ field).addClass('is-invalid');
                            $('#'+ type +'-' + field + '-error').text(errors[field][0]);
                        }
                    }
                }
            });
        });
        $('#loginForm').on('submit', function(e) {
            e.preventDefault();

            let form = $(this);
            let actionUrl = '{{ route("login") }}'; // Adjust to your login route

            // Clear previous errors
            $('.invalid-feedback').text('');
            $('.form-control').removeClass('is-invalid');
            $('#loginAlert').addClass('d-none').empty(); // Hide any previous alert

            $.ajax({
                type: 'POST',
                url: actionUrl,
                data: form.serialize(),
                success: function(response) {
                    if (response.redirect) {
                        window.location.href = response.redirect; // Redirect to the specified route
                    }
                },
                error: function(response) {
                    let errors = response.responseJSON.errors;
                    if (errors) {
                        for (let field in errors) {
                            $('#' + field).addClass('is-invalid');
                            $('#' + field + '-error').text(errors[field][0]);
                        }
                    } else {
                        let message = response.responseJSON.message;
                        $('#loginAlert').text(message).removeClass('d-none');
                    }
                }
            });
        });


        // Function to show and hide the appropriate form
        function showForm(selectedFormId, otherFormId, roleText) {
            document.getElementById(selectedFormId).style.display = 'block';
            document.getElementById(otherFormId).style.display = 'none';
            document.getElementById('formLinksRow').style.display = 'none';
            document.getElementById('roleText').textContent = roleText; // Update the role text
            document.getElementById('goBackHelper').style.display = (selectedFormId === 'helperFormContent') ? 'inline-block' : 'none';
            document.getElementById('goBackEmployer').style.display = (selectedFormId === 'employerFormContent') ? 'inline-block' : 'none';
        }

        // Function to go back to role selection
        function goBack() {
            document.getElementById('formLinksRow').style.display = 'flex';
            document.getElementById('helperFormContent').style.display = 'none';
            document.getElementById('employerFormContent').style.display = 'none';
            document.getElementById('roleText').textContent = 'Select Your Role'; // Reset the role text
        }

        document.getElementById('helperForm').addEventListener('click', function () {
            showForm('helperFormContent', 'employerFormContent', 'Register as Helper');
        });

        document.getElementById('employerForm').addEventListener('click', function () {
            showForm('employerFormContent', 'helperFormContent', 'Register as Employer');
        });

        document.getElementById('goBackHelper').addEventListener('click', goBack);
        document.getElementById('goBackEmployer').addEventListener('click', goBack);
    });
</script>

@stack('js')
</body>
</html>
