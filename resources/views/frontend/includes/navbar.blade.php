
<style>
    @media (min-width: 1200px) {
        .container {
            max-width: 1440px;
        }

    }
    .navbar-nav{
        padding-left:15px ;
    }
</style>
<style type="text/css">
    .select-role {
        border-bottom: 3px solid #ebba16 !important;
        padding-bottom: 10px;
    }

    .looking-job {
        background-color: #B50000;
    }

    .looking-job img {
        max-width: 35%;
    }

    .looking-employer {
        background-color: #000000;
    }

    .looking-employer img {
        max-width: 35%;
    }
    .role-link.active {
        border: 2px solid #007bff; /* Example active state styling */
    }
    .go-back-btn {
        display: inline-block;
        margin: 20px 0;
        padding: 10px 20px;
        background-color: #007bff;
        color: white;
        border: none;
        cursor: pointer;
    }
    .go-back-btn:hover {
        background-color: #0056b3;
    }
    .helper-form input {
        background-color: #eaeaea !important;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light  sticky-top" style="background-color: #1E3F66  !important; color:white !important;">
    <div _ngcontent-serverapp-c1428765552 class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="background-color: white !important;">
            <span class="navbar-toggler-icon" ></span>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}">
            @if(isset($settings->site_logo))
                <img src="{{asset('storage/site_logo/'.$settings->site_logo)}}" alt="Logo">
            @else
                <img src="{{asset('cdn-sub/logo.png')}}" alt="Logo">
            @endif
        </a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="{{route('jobs')}}" style="color:white !important;">FIND EMPLOYERS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-uppercase" href="{{route('candidates')}}" style="color:white !important;">FIND HELPERS</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="/agency&services" style="color:white !important;">AGENCY PORTALS</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-uppercase" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white !important;">
                        News & More
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-uppercase" href="/news">Tips & News</a>
                        <a class="dropdown-item text-uppercase" href="/training">Training</a>
                        <a class="dropdown-item text-uppercase" href="/partner">Partner Offer</a>
                        <a class="dropdown-item text-uppercase" href="/event">Event</a>
                        <a class="dropdown-item text-uppercase" href="/about">About Us</a>
                        <a class="dropdown-item text-uppercase" href="/pricing">Pricing</a>
                        <a class="dropdown-item text-uppercase" href="/public_holiday">Public Holiday</a>
                    </div>
                </li>
            </ul>
            @auth
                <div class="form-inline my-2 my-lg-0">
                    @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_ADMIN)
                        <a class="btn login_btn my-2 my-sm-0 text-uppercase" href="{{ route('admin_dashboard') }}">Dashboard</a>
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_CANDIDATE)
                        <a class="btn login_btn my-2 my-sm-0 text-uppercase" href="{{ route('candidate_dashboard') }}">Dashboard</a>
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER)
                        <a class="btn login_btn my-2 my-sm-0 text-uppercase" href="{{ route('employer_dashboard') }}">Dashboard</a>
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_AGENCY)
                        <a class="btn login_btn my-2 my-sm-0 text-uppercase" href="{{ route('agency_dashboard') }}">Dashboard</a>
                    @endif

                </div>
            @endauth
            @guest
                <div class="form-inline my-2 my-lg-0">
                    <a class="btn login_btn my-2 my-sm-0 text-uppercase" data-toggle="modal" data-target="#loginModal">Login</a>
                    <a class="btn register_btn my-2 my-sm-0 text-uppercase" data-toggle="modal" data-target="#registerModal">Register</a>
                </div>

            @endguest
        </div>
    </div>
</nav>



