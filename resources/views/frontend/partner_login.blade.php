@extends('frontend.layouts.app')
@section('title','PARTNER SIGNUP')
@section('content')
    <style ng-app-id="serverApp">[_nghost-serverApp-c1080078657]  .home header {
            position: absolute;
        }

        [_nghost-serverApp-c1080078657]  .home header.sticky {
            position: fixed;
        }

        [_nghost-serverApp-c1080078657]     .description_tag p {
            margin-bottom: 15px !important;
        }
        [_nghost-serverApp-c1080078657]     .description_tag ul {
            list-style-type: disc;
            margin-bottom: 15px !important;
        }</style><style ng-app-id="serverApp">

        [_nghost-serverApp-c1704746407]  header {
            padding: 5px 0;
        }
        @media only screen and (max-width: 575px) {
            [_nghost-serverApp-c1704746407]  header {
                padding: 5px 0;
            }
        }
        [_nghost-serverApp-c1704746407]  header img {
            width: 100%;
            height: auto;
        }
        [_nghost-serverApp-c1704746407]  header img.brand-logo {
            max-width: 180px;
        }
        [_nghost-serverApp-c1704746407]  header nav ul ul {
            z-index: 8;
        }
        [_nghost-serverApp-c1704746407]  header nav ul li a {
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 600;
            line-height: 1;
            line-height: normal;
            padding: 15px 18px;
            color: #262626;
        }
        [_nghost-serverApp-c1704746407]  header nav ul li a:hover {
            color: #054a84;
        }
        @media (max-width: 1200px) {
            [_nghost-serverApp-c1704746407]  header nav ul li a {
                padding: 15px 10px;
            }
        }
        [_nghost-serverApp-c1704746407]  header .d-inline-block {
            align-items: center;
            justify-content: center;
        }
        @media (max-width: 986px) {
            [_nghost-serverApp-c1704746407]  header nav.custom-nav {
                height: 55px;
            }
        }
        @media (min-width: 992px) {
            [_nghost-serverApp-c1704746407]  header nav.custom-nav {
                height: 55px;
            }
        }
        [_nghost-serverApp-c1704746407]  header .custom-app-menu {
            width: 100%;
        }

        header.sticky[_ngcontent-serverApp-c1704746407] {
            background: rgba(255, 255, 255, 0.99);
            box-shadow: 0 3px 12px 0 rgba(0, 0, 0, 0.08);
            position: fixed;
            top: 0;
            height: auto;
            width: 100%;
            transition: all 0.5s;
            padding: 5px 0;
        }
        header.sticky[_ngcontent-serverApp-c1704746407]   img[_ngcontent-serverApp-c1704746407] {
            max-width: 50%;
            min-width: 50%;
            height: auto;
            transition: width 2s;
        }
        @media screen {
            header.sticky[_ngcontent-serverApp-c1704746407]   img[_ngcontent-serverApp-c1704746407] {
                max-width: 80%;
            }
        }
        header.sticky[_ngcontent-serverApp-c1704746407]   img.brand-logo[_ngcontent-serverApp-c1704746407] {
            max-width: 180px;
        }

        .animated[_ngcontent-serverApp-c1704746407] {
            transition: height 1s;
        }

        header[_ngcontent-serverApp-c1704746407]   .responsive-btn[_ngcontent-serverApp-c1704746407]   i[_ngcontent-serverApp-c1704746407] {
            color: #020202 !important;
        }

        .social_left_sticky[_ngcontent-serverApp-c1704746407]   a[_ngcontent-serverApp-c1704746407] {
            text-decoration: none;
            vertical-align: middle;
            text-align: left;
            line-height: 3;
        }
        .social_left_sticky[_ngcontent-serverApp-c1704746407]   p[_ngcontent-serverApp-c1704746407] {
            color: white;
            position: relative;
            left: 0px;
            padding: 0 0 0 10px;
            line-height: 38px;
            font-family: Roboto, "Helvetica Neue", sans-serif;
        }
        .social_left_sticky[_ngcontent-serverApp-c1704746407]   #sidebar[_ngcontent-serverApp-c1704746407] {
            height: 250px;
            width: 10px;
            position: fixed;
            padding: 10px;
            margin-left: 10px;
        }
        .social_left_sticky[_ngcontent-serverApp-c1704746407]   .social[_ngcontent-serverApp-c1704746407] {
            margin-left: 0px;
            width: 230px;
            height: 30px;
            line-height: 30px;
            padding: 0;
            display: inline-table;
            height: 0px;
            background-color: rgba(128, 128, 128, 0.93);
            -moz-transition-property: margin-left;
            -moz-transition-duration: 0.2s;
            -moz-transition-delay: 0.2s;
            -ms-transition-property: margin-left;
            -ms-transition-duration: 0.2s;
            -ms-transition-delay: 0.2s;
            -o-transition-property: margin-left;
            -o-transition-duration: 0.2s;
            -o-transition-delay: 0.2s;
            -webkit-transition-property: margin-left;
            -webkit-transition-duration: 0.2s;
            -webkit-transition-delay: 0.2s;
            box-shadow: 0px 0px 6px 0px #3e3d3d;
            cursor: pointer;
        }
        .social_left_sticky[_ngcontent-serverApp-c1704746407]   .social[_ngcontent-serverApp-c1704746407]:hover {
            margin-left: -30px;
            width: 230px;
            background-color: #054a84;
        }

        .popup-button-main[_ngcontent-serverApp-c1704746407] {
            margin-left: 15px;
        }

        .signup-link[_ngcontent-serverApp-c1704746407]:hover {
            color: #054a84;
        }

        @media (max-width: 767px) {
            .mob_col_11_10[_ngcontent-serverApp-c1704746407] {
                flex: 0 0 65.666667%;
                max-width: 65.666667% !important;
                text-align: end;
            }
        }
        @media (max-width: 991px) {
            .mob_col_11_10[_ngcontent-serverApp-c1704746407] {
                flex: 0 0 65.666667%;
                max-width: 65.666667% !important;
                text-align: end;
            }
        }
        @media (max-width: 1200px) {
            .mob_col_11_10[_ngcontent-serverApp-c1704746407] {
                flex: 0 0 80%;
                max-width: 80% !important;
                text-align: end;
            }
        }
        @media (max-width: 986px) {
            .helper-logo[_ngcontent-serverApp-c1704746407] {
                z-index: 0;
                position: absolute;
                margin-left: auto;
                margin-right: auto;
                left: 0;
                right: 30px;
                text-align: center;
                top: 4px !important;
            }
            .img-fluid[_ngcontent-serverApp-c1704746407] {
                min-width: 0px !important;
            }
        }</style><style ng-app-id="serverApp">.dropdown_menu_color[_ngcontent-serverApp-c719059462] {
            color: #121212;
        }
        .dropdown_menu_color[_ngcontent-serverApp-c719059462]:hover {
            color: #077556 !important;
        }

        .bg-dark[_ngcontent-serverApp-c719059462] {
            background: #25ae88 !important;
            color: #fff !important;
        }

        .popup-button-main[_ngcontent-serverApp-c719059462] {
            margin-left: 10px;
            float: right;
            margin-top: 3px;
        }
        @media screen and (max-width: 991px) {
            .popup-button-main[_ngcontent-serverApp-c719059462] {
                width: 50%;
                display: inline-block;
                margin: 0;
                padding: 0 10px;
            }
        }
        .popup-button-main[_ngcontent-serverApp-c719059462]   a[_ngcontent-serverApp-c719059462] {
            padding: 10px 10px;
        }
        .popup-button-main[_ngcontent-serverApp-c719059462]   a.popup-button-register[_ngcontent-serverApp-c719059462] {
            border: 2px solid #ebba16;
            border-radius: 6px;
            background-color: #ebba16;
            color: #fff;
            letter-spacing: 1px;
            width: 110px;
            text-align: center;
            float: left;
        }
        .popup-button-main[_ngcontent-serverApp-c719059462]   a.popup-button-login[_ngcontent-serverApp-c719059462] {
            border: 2px solid #25ae88;
            border-radius: 6px;
            background-color: #25ae88;
            color: #fff;
            letter-spacing: 1px;
            width: 110px;
            text-align: center;
            float: right;
        }

        .alert-dismissible[_ngcontent-serverApp-c719059462]   .close[_ngcontent-serverApp-c719059462] {
            position: absolute;
            top: 0;
            right: 0;
            padding: 0.75rem 1.25rem;
            color: inherit;
            background: transparent;
        }

        .alert-dismissible[_ngcontent-serverApp-c719059462]   .close[_ngcontent-serverApp-c719059462]:focus {
            outline: none;
        }

        .has-badge[data-count][_ngcontent-serverApp-c719059462]:after {
            position: absolute;
            right: 0%;
            top: -20%;
            width: 16px;
            height: 16px;
            line-height: 17px;
            content: attr(data-count);
            font-size: 40%;
            border-radius: 50%;
            color: white;
            background: rgba(255, 0, 0, 0.85);
            text-align: center;
        }
        @media only screen and (max-width: 991px) {
            .has-badge[data-count][_ngcontent-serverApp-c719059462]:after {
                right: -10%;
                top: -75%;
                font-size: 65%;
            }
        }

        .fa-stack[_ngcontent-serverApp-c719059462] {
            margin: 0 -0.5em 1.7em;
            padding: 1em 1em 1em 1em;
        }
        .fa-stack[_ngcontent-serverApp-c719059462]   i[_ngcontent-serverApp-c719059462] {
            color: #054a84 !important;
        }

        .notification-window[_ngcontent-serverApp-c719059462] {
            position: absolute;
            top: 60px;
            right: 0;
            background: white;
            height: auto;
            width: auto;
            display: table;
            border: 1px solid #f3f3f3;
            border-radius: 7px;
            z-index: 8;
            box-shadow: 0.5px 1.5px 6px 1px #cfcfcf;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .notification-header[_ngcontent-serverApp-c719059462] {
            text-align: left;
            border-bottom: 2px solid #eee;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .notification-header[_ngcontent-serverApp-c719059462]   .notification-title[_ngcontent-serverApp-c719059462] {
            text-align: left;
            display: inline-block;
            padding: 5px 10px;
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
            color: #054a84;
            font-weight: bold;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .notification-header[_ngcontent-serverApp-c719059462]   button[_ngcontent-serverApp-c719059462] {
            color: #aaa;
            font-size: calc(13px + 3 * (100vw - 300px) / 1620) !important;
            line-height: 26px;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462] {
            clear: both;
            height: 300px;
            overflow-y: auto;
            width: 400px;
            margin-bottom: 5px;
        }
        @media screen and (max-width: 500px) {
            .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462] {
                width: 300px;
            }
        }
        @media screen and (max-width: 360px) {
            .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462] {
                width: 275px;
            }
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item[_ngcontent-serverApp-c719059462] {
            text-align: left;
            border: 0;
            border-radius: 0;
            border-bottom: 1px solid #eee;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item[_ngcontent-serverApp-c719059462]   h5[_ngcontent-serverApp-c719059462] {
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
            margin-bottom: 2px;
            color: #262626;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item[_ngcontent-serverApp-c719059462]   p[_ngcontent-serverApp-c719059462] {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            text-transform: none;
            margin: 0;
            line-height: normal;
            color: #999;
            text-align: justify;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
            margin-bottom: 5px;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item[_ngcontent-serverApp-c719059462]   small[_ngcontent-serverApp-c719059462] {
            text-transform: initial;
            font-size: calc(10px + 3 * (100vw - 300px) / 1620) !important;
            color: #999;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item.notification-unread[_ngcontent-serverApp-c719059462] {
            background-color: #f8f9fa;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item.notification-unread[_ngcontent-serverApp-c719059462]   h5[_ngcontent-serverApp-c719059462], .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item.notification-unread[_ngcontent-serverApp-c719059462]   p[_ngcontent-serverApp-c719059462], .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   .list-group-item.notification-unread[_ngcontent-serverApp-c719059462]   small[_ngcontent-serverApp-c719059462] {
            font-weight: bold;
        }
        .notification-window[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462]   button[_ngcontent-serverApp-c719059462] {
            color: #aaa;
            font-size: calc(13px + 3 * (100vw - 300px) / 1620) !important;
            line-height: 26px;
        }

        #notification-icon[_ngcontent-serverApp-c719059462] {
            cursor: pointer;
        }

        .mt_1_c[_ngcontent-serverApp-c719059462] {
            margin-top: 0.75rem;
        }
        @media screen and (max-width: 767px) {
            .mt_1_c[_ngcontent-serverApp-c719059462] {
                margin-top: 0.25rem;
            }
        }

        .navbar[_ngcontent-serverApp-c719059462]   .custom-min-menu[_ngcontent-serverApp-c719059462] {
            width: 100%;
            text-align: left;
        }

        .custom-login-menu[_ngcontent-serverApp-c719059462] {
            float: right;
        }
        @media screen and (max-width: 991px) {
            .custom-login-menu[_ngcontent-serverApp-c719059462] {
                float: none;
            }
        }

        .custom-menu-icon[_ngcontent-serverApp-c719059462] {
            padding: 12px 0;
            color: #054a84;
            font-size: calc(20px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 600;
        }
        .custom-menu-icon[_ngcontent-serverApp-c719059462]   span[_ngcontent-serverApp-c719059462] {
            padding: 0px 5px;
            cursor: pointer;
            position: relative;
        }
        @media screen and (max-width: 991px) {
            .custom-menu-icon[_ngcontent-serverApp-c719059462] {
                display: none;
            }
        }

        .navbar[_ngcontent-serverApp-c719059462]   .custom-mobile-inner-header[_ngcontent-serverApp-c719059462] {
            padding: 10px;
            float: none;
        }
        .navbar[_ngcontent-serverApp-c719059462]   .custom-mobile-inner-header[_ngcontent-serverApp-c719059462]   .custom-mobile-logo[_ngcontent-serverApp-c719059462] {
            max-width: 150px;
        }
        .navbar[_ngcontent-serverApp-c719059462]   .custom-mobile-inner-header[_ngcontent-serverApp-c719059462]   .custom-back-button[_ngcontent-serverApp-c719059462] {
            float: right;
            cursor: pointer;
            padding: 10px;
        }

        .overlay-sidebar-header.overlay-sidebar-header-open[_ngcontent-serverApp-c719059462] {
            z-index: 8;
        }

        .custom-scroll[_ngcontent-serverApp-c719059462]   .list-group[_ngcontent-serverApp-c719059462] {
            scrollbar-color: #aaa #e3e3e3;
            scrollbar-width: thin;
        }
        .custom-scroll[_ngcontent-serverApp-c719059462]   [_ngcontent-serverApp-c719059462]::-webkit-scrollbar {
            width: 8px;
            background-color: #e3e3e3;
            border-radius: 25px;
        }
        .custom-scroll[_ngcontent-serverApp-c719059462]   [_ngcontent-serverApp-c719059462]::-webkit-scrollbar-thumb {
            background-image: linear-gradient(rgba(170, 170, 170, 0.92) 0%, #aaa 100%);
            border-radius: 25px;
        }

        .region[_ngcontent-serverApp-c719059462]   .form-group[_ngcontent-serverApp-c719059462]   input[_ngcontent-serverApp-c719059462] {
            width: auto !important;
        }

        .modal-header[_ngcontent-serverApp-c719059462] {
            padding: 1rem 1rem;
            background: white;
        }

        .nav-pills[_ngcontent-serverApp-c719059462]   .nav-link[_ngcontent-serverApp-c719059462] {
            font-size: 20px !important;
        }

        .welcome-modal[_ngcontent-serverApp-c719059462]   .close[_ngcontent-serverApp-c719059462] {
            position: absolute;
            right: 5px;
            top: 5px;
            background: #f5f5f5;
            opacity: 1 !important;
            color: #666;
            z-index: 2;
            margin: 0;
            padding: 0px 5px;
            border-radius: 50%;
            border: 1px solid #666;
        }
        .welcome-modal[_ngcontent-serverApp-c719059462]   .modal-header[_ngcontent-serverApp-c719059462] {
            padding: 0;
            border: 0;
        }

        .welcome-popup[_ngcontent-serverApp-c719059462] {
            min-height: 400px;
            padding: 0;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462]   .img-container[_ngcontent-serverApp-c719059462] {
            height: 200px;
            overflow: hidden;
            border-top-right-radius: 0.2rem;
            border-top-left-radius: 0.2rem;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462]   .container-fluid[_ngcontent-serverApp-c719059462] {
            height: 200px;
            display: flex;
            flex-direction: column;
            padding: 1rem;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462]   .container-fluid[_ngcontent-serverApp-c719059462]   h3[_ngcontent-serverApp-c719059462] {
            font-weight: bold;
            font-size: calc(20px + 3 * (100vw - 300px) / 1620);
        }
        .welcome-popup[_ngcontent-serverApp-c719059462]   .container-fluid[_ngcontent-serverApp-c719059462]   p[_ngcontent-serverApp-c719059462] {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620);
            margin-top: auto;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462]   .container-fluid[_ngcontent-serverApp-c719059462]   .action-buttons[_ngcontent-serverApp-c719059462] {
            margin-top: auto;
            margin-bottom: auto;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462]   .container-fluid[_ngcontent-serverApp-c719059462]   .action-buttons[_ngcontent-serverApp-c719059462]   .btn-yellow[_ngcontent-serverApp-c719059462] {
            color: #fff;
            background-color: #ebba16;
            border-color: #ebba16;
            width: 155px;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462]   .container-fluid[_ngcontent-serverApp-c719059462]   .action-buttons[_ngcontent-serverApp-c719059462]   .btn-green[_ngcontent-serverApp-c719059462] {
            color: #fff;
            background-color: #25ae88;
            border-color: #25ae88;
            width: 155px;
        }

        .region-country-list[_ngcontent-serverApp-c719059462]   span[_ngcontent-serverApp-c719059462] {
            border: none;
            padding: 0px;
            font-size: 13px;
            font-weight: 300;
            color: #505050;
            display: inline-block;
            margin: 3px auto;
        }
        .region-country-list[_ngcontent-serverApp-c719059462]   span[_ngcontent-serverApp-c719059462]   .flag.id[_ngcontent-serverApp-c719059462] {
            background-position: -16px -528px;
        }
        .region-country-list[_ngcontent-serverApp-c719059462]   span[_ngcontent-serverApp-c719059462]   img.fnone[_ngcontent-serverApp-c719059462] {
            float: none;
            width: 17.5%;
            height: auto;
        }
        .region-country-list[_ngcontent-serverApp-c719059462]   span[_ngcontent-serverApp-c719059462]   img[_ngcontent-serverApp-c719059462] {
            border: 1px solid #ccc;
            border-radius: 50%;
            width: 20px !important;
            vertical-align: bottom;
        }

        .report-modal[_ngcontent-serverApp-c719059462]   .confirm-buttons[_ngcontent-serverApp-c719059462] {
            clear: both;
        }

        .agency-contact-form-modal[_ngcontent-serverApp-c719059462]   .modal-body[_ngcontent-serverApp-c719059462] {
            width: 100%;
            margin: 0 auto;
            padding: 4% 5%;
            border-radius: 5px;
        }
        .agency-contact-form-modal[_ngcontent-serverApp-c719059462]   .modal-body[_ngcontent-serverApp-c719059462]   .agency-contant-mobileNo[_ngcontent-serverApp-c719059462] {
            margin-bottom: 15px !important;
        }
        .agency-contact-form-modal[_ngcontent-serverApp-c719059462]   .modal-body[_ngcontent-serverApp-c719059462]   .example-container[_ngcontent-serverApp-c719059462] {
            padding: 0 10px 0 0;
        }
        @media screen and (max-width: 767px) {
            .agency-contact-form-modal[_ngcontent-serverApp-c719059462]   .modal-body[_ngcontent-serverApp-c719059462] {
                width: 95%;
            }
        }
        .agency-contact-form-modal[_ngcontent-serverApp-c719059462]   .example-full-width[_ngcontent-serverApp-c719059462] {
            width: 100%;
        }

        @media screen and (max-width: 767px) {
            .extra-padding-bottom-mobile[_ngcontent-serverApp-c719059462] {
                margin-bottom: 5rem;
            }
        }

        @media screen and (min-width: 999px) {
            .cstm_length[_ngcontent-serverApp-c719059462] {
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 191px;
            }
        }

        .follow-social[_ngcontent-serverApp-c719059462] {
            display: -webkit-inline-box;
            padding: 20px;
        }

        .socials-lists[_ngcontent-serverApp-c719059462]   ul[_ngcontent-serverApp-c719059462]   li[_ngcontent-serverApp-c719059462] {
            padding: 0px 25px;
        }

        .top-10[_ngcontent-serverApp-c719059462] {
            margin-top: 10px;
            text-align: center;
        }

        .socials-lists[_ngcontent-serverApp-c719059462] {
            margin: auto;
            width: 55%;
        }

        .p-5[_ngcontent-serverApp-c719059462] {
            padding: 10px 0px 10px 0px !important;
        }

        .cstm-menu-ar[_ngcontent-serverApp-c719059462] {
            right: -100px;
        }

        @media only screen and (min-width: 991px) {
            .name-menu[_ngcontent-serverApp-c719059462] {
                float: right;
            }
        }
        .logout[_ngcontent-serverApp-c719059462] {
            position: relative !important;
            right: 57px !important;
        }

        .fa-bars[_ngcontent-serverApp-c719059462]:before, .fa-navicon[_ngcontent-serverApp-c719059462]:before, .fa-reorder[_ngcontent-serverApp-c719059462]:before {
            position: absolute !important;
            top: 2px !important;
            width: 250% !important;
            height: 200% !important;
        }

        .mobile-nav[_ngcontent-serverApp-c719059462] {
            display: flex !important;
            flex-direction: row;
            align-items: center;
            width: 100%;
            justify-content: space-between;
        }

        .custom-mobile-view[_ngcontent-serverApp-c719059462] {
            width: 44px;
        }

        .sign-up[_ngcontent-serverApp-c719059462] {
            font-size: 1.1rem !important;
            font-weight: 600;
        }

        .sign-in[_ngcontent-serverApp-c719059462] {
            color: #25ae88;
            cursor: pointer;
        }

        .sign-up[_ngcontent-serverApp-c719059462] {
            color: #ebba16;
            cursor: pointer;
        }

        .not-mobile-view[_ngcontent-serverApp-c719059462] {
            display: block;
        }

        .sign-up[_ngcontent-serverApp-c719059462]   .popup-button-login[_ngcontent-serverApp-c719059462] {
            font-weight: 600 !important;
            font-size: 15px !important;
        }

        .sign-up[_ngcontent-serverApp-c719059462]   .popup-button-register[_ngcontent-serverApp-c719059462] {
            font-weight: 600 !important;
            font-size: 15px !important;
        }

        @media (max-width: 986px) {
            .view-mobile[_ngcontent-serverApp-c719059462] {
                display: block;
            }
            .not-mobile-view[_ngcontent-serverApp-c719059462] {
                display: none;
            }
        }
        @media (max-width: 986px) {
            .responsive-btn[_ngcontent-serverApp-c719059462] {
                float: left;
            }
            .header[_ngcontent-serverApp-c719059462]   nav[_ngcontent-serverApp-c719059462] {
                justify-content: center !important;
            }
            .notification-icon[_ngcontent-serverApp-c719059462] {
                z-index: 0;
                position: absolute;
                margin-left: auto;
                margin-right: auto;
                left: 0;
                right: 0;
                text-align: end;
            }
            .line[_ngcontent-serverApp-c719059462] {
                border: 1px solid #dddddd;
            }
        }
        @media (min-width: 991px) {
            .shadow-menu[_ngcontent-serverApp-c719059462] {
                box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2) !important;
            }
        }
        @media (max-width: 991px) {
            .notification-icon[_ngcontent-serverApp-c719059462] {
                height: 0px !important;
            }
        }
        .notification-icon[_ngcontent-serverApp-c719059462] {
            z-index: 0;
        }

        [_nghost-serverApp-c719059462]     .icons {
            fill: #054a84 !important;
        }

        @media (max-width: 367px) {
            .btn-position[_ngcontent-serverApp-c719059462] {
                margin-top: 6px !important;
                margin-right: 9px !important;
            }
        }</style><style ng-app-id="serverApp">.button-group-pills[_ngcontent-serverApp-c2828344174]   .btn[_ngcontent-serverApp-c2828344174] {
            font-size: 14px;
        }

        .primary[_ngcontent-serverApp-c2828344174] {
            color: #054a84;
        }

        .signup-div[_ngcontent-serverApp-c2828344174] {
            border-radius: 0.25rem;
            border: 1px solid #ced4da;
            background: #f8f8f8;
        }

        .partner-signup[_ngcontent-serverApp-c2828344174] {
            background-color: #fff;
        }
        .partner-signup[_ngcontent-serverApp-c2828344174]   .form-group[_ngcontent-serverApp-c2828344174]   input[_ngcontent-serverApp-c2828344174] {
            background-color: #fff;
            border: 1px solid #ced4da;
        }

        .dark[_ngcontent-serverApp-c2828344174] {
            color: #25ae88 !important;
        }

        .bg-dark[_ngcontent-serverApp-c2828344174] {
            background-color: #25ae88 !important;
        }

        .my_custom_mat-label[_ngcontent-serverApp-c2828344174], ng-multiselect-dropdown[_ngcontent-serverApp-c2828344174] {
            font-weight: bold;
        }

        .btn-sucesss[_ngcontent-serverApp-c2828344174] {
            background-color: #054a84;
            color: #fff;
        }
        .btn-sucesss[_ngcontent-serverApp-c2828344174]:hover {
            background-color: #25ae88;
        }

        .para2[_ngcontent-serverApp-c2828344174] {
            min-width: 100%;
            max-width: 100%;
            overflow: hidden;
            margin-bottom: 5px;
            resize: none;
            white-space: pre-line;
            border: navajowhite;
            scroll-behavior: inherit;
            text-overflow: ellipsis;
        }

        .custom_errow_only[_ngcontent-serverApp-c2828344174]   input.mat-input-element[_ngcontent-serverApp-c2828344174] {
            margin-top: 0;
        }

        .blog-agency[_ngcontent-serverApp-c2828344174]   .Blog_list_img_size[_ngcontent-serverApp-c2828344174] {
            min-height: 150px !important;
            max-height: 150px !important;
            overflow: hidden;
            background-size: cover !important;
            background-position: center center !important;
        }
        .blog-agency[_ngcontent-serverApp-c2828344174]   .Blog_list_img_size[_ngcontent-serverApp-c2828344174]   .custom_img[_ngcontent-serverApp-c2828344174] {
            width: 100%;
            min-width: 100%;
        }
        @media screen and (max-width: 768px) {
            .blog-agency[_ngcontent-serverApp-c2828344174]   .Blog_list_img_size[_ngcontent-serverApp-c2828344174]   .custom_img[_ngcontent-serverApp-c2828344174] {
                min-height: 130px !important;
                max-height: 130px !important;
            }
        }

        .agency[_ngcontent-serverApp-c2828344174]   h1[_ngcontent-serverApp-c2828344174] {
            font-size: calc(18px + 10 * (100vw - 300px) / 1620);
            line-height: 1.4;
            font-weight: 500;
        }

        .show-password[_ngcontent-serverApp-c2828344174] {
            position: relative;
            bottom: 30px;
            float: right;
            right: 8px;
            cursor: pointer;
        }

        .stronger-password[_ngcontent-serverApp-c2828344174] {
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #dc3545;
        }

        .password-meter[_ngcontent-serverApp-c2828344174] {
            font-size: 20px;
        }

        .invalid-form[_ngcontent-serverApp-c2828344174] {
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #dc3545;
        }</style><style ng-app-id="serverApp">*[_ngcontent-serverApp-c3773246210], *[_ngcontent-serverApp-c3773246210]:before, *[_ngcontent-serverApp-c3773246210]:after{box-sizing:border-box}.psm__progress-bar[_ngcontent-serverApp-c3773246210]{position:relative;height:3px;margin:10px auto;border-radius:3px;background:#fff}.psm__progress-bar-items[_ngcontent-serverApp-c3773246210]{display:flex;justify-content:space-evenly;width:100%;height:100%;background:#ddd}.psm__progress-bar-item[_ngcontent-serverApp-c3773246210]{height:100%;z-index:5;border-right:4px solid #fff}.psm__progress-bar-item[_ngcontent-serverApp-c3773246210]:last-child{border-right:0px}.psm__progress-bar-overlay[_ngcontent-serverApp-c3773246210]{position:absolute;background:red;border-radius:3px;height:3px;top:0;transition:width .5s ease-in-out,background .25s}.psm__feedback[_ngcontent-serverApp-c3773246210], .psm__suggestion[_ngcontent-serverApp-c3773246210]{font-size:70%;font-weight:400;color:#6c757d!important;display:block;margin-top:.25rem}</style><style ng-app-id="serverApp">@charset "UTF-8";
        .dark[_ngcontent-serverApp-c2302031101] {
            color: #25ae88 !important;
        }

        primary[_ngcontent-serverApp-c2302031101] {
            color: #054a84;
        }

        .bg-secondary[_ngcontent-serverApp-c2302031101] {
            background-color: #1f2023 !important;
            color: #fff;
        }

        .light[_ngcontent-serverApp-c2302031101] {
            color: #ebba16 !important;
        }

        .text-offwhite[_ngcontent-serverApp-c2302031101] {
            color: #ddd !important;
        }

        .icon_social_color[_ngcontent-serverApp-c2302031101] {
            color: #1f2023;
            background: #fff;
        }

        .android[_ngcontent-serverApp-c2302031101]::after {
            content: " ";
            display: inline-block;
            width: 100%;
            height: 35px;
            background-position: 0% 0%;
            background-size: 100% 200%;
            background-image: url(../cdn.helperplace.com/web-asset/images/Android-App-Store-logos.png);
            padding: 0;
        }

        div.android[_ngcontent-serverApp-c2302031101]::after {
            max-width: 18px;
            width: 100%;
            height: 0;
            padding: 0 0 100% 0;
        }

        .ios[_ngcontent-serverApp-c2302031101]::after {
            content: " ";
            display: inline-block;
            width: 100%;
            height: 35px;
            background-position: 0% 98%;
            background-size: 100% 200%;
            background-image: url(../cdn.helperplace.com/web-asset/images/Android-App-Store-logos.png);
            padding: 0;
        }

        div.ios[_ngcontent-serverApp-c2302031101]::after {
            max-width: 18px;
            width: 100%;
            height: 0;
            padding: 0 0 100% 0;
        }

        .resume[_ngcontent-serverApp-c2302031101]   p[_ngcontent-serverApp-c2302031101] {
            color: #fff !important;
            font-size: calc(12px + 2 * (100vw - 300px) / 1620);
        }

        .spacer_26[_ngcontent-serverApp-c2302031101]   li[_ngcontent-serverApp-c2302031101] {
            height: 28px;
        }

        .socials-lists[_ngcontent-serverApp-c2302031101]   ul[_ngcontent-serverApp-c2302031101]   li[_ngcontent-serverApp-c2302031101] {
            padding: 0 10px 2% 10px;
        }

        .socials-lists[_ngcontent-serverApp-c2302031101]   ul[_ngcontent-serverApp-c2302031101]   li[_ngcontent-serverApp-c2302031101]   a[_ngcontent-serverApp-c2302031101]   i[_ngcontent-serverApp-c2302031101] {
            height: 35px;
            width: 35px;
        }

        .rounded-top-lr[_ngcontent-serverApp-c2302031101] {
            border-top-left-radius: 0.35rem !important;
            border-top-right-radius: 0.35rem !important;
            border-bottom-left-radius: 0px !important;
            border-bottom-right-radius: 0px !important;
            background: #01ba00 !important;
        }

        .socials-horizontal[_ngcontent-serverApp-c2302031101] {
            align-items: center;
            justify-content: center;
        }

        .store-div[_ngcontent-serverApp-c2302031101]   img[_ngcontent-serverApp-c2302031101] {
            margin: 0 8px;
        }
        @media (max-width: 1200px) {
            .store-div[_ngcontent-serverApp-c2302031101]   img[_ngcontent-serverApp-c2302031101] {
                margin: 0 5px;
            }
        }
        @media (max-width: 992px) {
            .store-div[_ngcontent-serverApp-c2302031101]   img[_ngcontent-serverApp-c2302031101] {
                margin: 0 8px;
            }
        }

        @media (max-width: 576px) {
            .custom-text-align[_ngcontent-serverApp-c2302031101] {
                text-align: center;
            }
        }
        .footer-logo[_ngcontent-serverApp-c2302031101] {
            max-width: 200px;
            width: 100%;
        }

        .copyright-text[_ngcontent-serverApp-c2302031101] {
            font-size: 11px !important;
        }
        .copyright-text[_ngcontent-serverApp-c2302031101]   .text-offwhite[_ngcontent-serverApp-c2302031101] {
            font-size: 11px !important;
        }

        .desc-text[_ngcontent-serverApp-c2302031101] {
            font-size: 14px !important;
        }

        .socials-lists[_ngcontent-serverApp-c2302031101]   ul.socials-horizontal[_ngcontent-serverApp-c2302031101] {
            display: inline-flex !important;
        }

        .region-lang[_ngcontent-serverApp-c2302031101] {
            display: flex;
            align-items: center;
            justify-content: start;
        }
        .region-lang[_ngcontent-serverApp-c2302031101]   select[_ngcontent-serverApp-c2302031101] {
            color: #fff !important;
            background-color: transparent !important;
            border: 0 !important;
            height: 30px !important;
            padding: 2px 7px !important;
            -webkit-appearance: none;
            appearance: none;
        }
        .region-lang[_ngcontent-serverApp-c2302031101]   select[_ngcontent-serverApp-c2302031101]   option[_ngcontent-serverApp-c2302031101] {
            color: #495057 !important;
        }
        @media (max-width: 575px) {
            .region-lang[_ngcontent-serverApp-c2302031101] {
                justify-content: center;
            }
        }

        .copyright-region[_ngcontent-serverApp-c2302031101] {
            align-items: center;
        }
        @media (max-width: 992px) {
            .copyright-region[_ngcontent-serverApp-c2302031101] {
                gap: 10px;
            }
        }

        .lang-section[_ngcontent-serverApp-c2302031101] {
            z-index: 1 !important;
            position: relative !important;
            top: -50px !important;
        }

        .location-icon[_ngcontent-serverApp-c2302031101] {
            margin-top: -3px;
            margin-left: 8px;
        }

        @media screen and (max-width: 992px) {
            .lang-section[_ngcontent-serverApp-c2302031101] {
                top: -42px !important;
            }
        }
    </style>
    <section _ngcontent-serverapp-c2828344174="" class="page-section" style="    margin-top: 115px;">
        <section _ngcontent-serverapp-c2828344174="" class="mt-4 mt-o-phone"></section>
        <section _ngcontent-serverapp-c2828344174="" class="mt-o-phone agency blog blog-sec blog-sidebar pb-5 blog-list sider">
            <div _ngcontent-serverapp-c2828344174="" class="container mt-o-phone">
                <div _ngcontent-serverapp-c2828344174="" class="row partner-signup">
                    <div _ngcontent-serverapp-c2828344174="" class="col-12 col-sm-11 col-md-8 col-lg-8 col-xl-8 text-left mx-auto text-center my-auto ">
                        @if(isset($settings->site_logo))
                            <img src="{{asset('storage/site_logo/'.$settings->site_logo)}}" alt="Logo"  width="40%"
                                 alt="citimuber Logo" class="img-fluid mb-4">
                        @else
                            <img src="{{asset('cdn-sub/logo.png')}}" alt="Logo"  width="40%"
                                 alt="citimuber Logo" class="img-fluid mb-4">
                        @endif
                        <h1 _ngcontent-serverapp-c2828344174="" class="second_data mb-2">Join CitiMuber Network and Reach Thousand of Potential Customers</h1>
                        <p _ngcontent-serverapp-c2828344174="" class="second_data">With few millions of yearly visitors and a trusted community of more than 200,000 users, we can help you to develop your brand awareness.</p>
                        <p _ngcontent-serverapp-c2828344174="" class="second_data">
                            If you are a transparent company and if you are promoting a fair recruitment (domestic helper agency, association, training, special offers), we will be happy to support you.
                        </p>
                    </div>
                    <div _ngcontent-serverapp-c2828344174="" class="col-12 col-sm-11 col-md-4 col-lg-4 col-xl-4 p-4 mx-auto custom_errow_only signup-div">
                        <h4 _ngcontent-serverapp-c2828344174="">Login with CitiMuber Network</h4>
                        <div _ngcontent-serverapp-c2828344174="" aria-labelledby="pills-home-tab" id="pills-home" role="tabpanel" class="tab-pane fade show active">
                            <form method="post" action="{{route('login_partner')}}">
                                @csrf
                                <div _ngcontent-serverapp-c2828344174="" class="ng-untouched ng-pristine ng-invalid mt-3">
                                    <!---->


                                    <div _ngcontent-serverapp-c2828344174="" class="form-group">
                                        <label _ngcontent-serverapp-c2828344174="" for="email" class="my_custom_mat-label">Email Address</label>
                                        <input _ngcontent-serverapp-c2828344174="" type="email"  name="email"
                                               class="form-control @error('email') is-invalid @enderror "
                                               placeholder="email"
                                        />
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                        <!---->
                                    </div>
                                    <div _ngcontent-serverapp-c2828344174="" class="form-group">
                                        <label _ngcontent-serverapp-c2828344174="" for="password" class="my_custom_mat-label">Password</label>
                                        <input _ngcontent-serverapp-c2828344174="" type="password"  name="password"
                                               class="form-control  @error('password') is-invalid @enderror" placeholder="password" />
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                        <!----><!---->
                                    </div>


                                </div>
                                <div _ngcontent-serverapp-c2828344174="" class="form-group text-center mt-2">
                                    <button _ngcontent-serverapp-c2828344174="" type="submit" class="btn mr-2 btn_custom_color btn-default text-uppercase">
                                        <!---->
                                        Sign In
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
