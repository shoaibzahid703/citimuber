@extends('frontend.layouts.app')
@section('title','Home')
@section('css')

@endsection
@section('content')
            <section _ngcontent-serverapp-c3877268260 class="page-section">
                <section _ngcontent-serverapp-c3877268260 id="home" class="app2 header overflow-unset" style=" background: rgba(0, 0, 0, 0.1); z-index:20">
                    <!----><!---->
                    <picture _ngcontent-serverapp-c3877268260 class="ng-star-inserted" >
                        <source _ngcontent-serverapp-c3877268260 media="(max-width:767px)" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: rgba(0, 0, 0, 0.1); z-index:1;" width="273" height="540" srcset="{{asset('storage/home_first_section')}}/{{$home_first_section->bg_image}}" />
                        <source _ngcontent-serverapp-c3877268260 media="(max-width:991px)" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: rgba(0, 0, 0, 0.1); z-index:1;" width="270" height="533" srcset="{{asset('storage/home_first_section')}}/{{$home_first_section->bg_image}}" />
                        <source _ngcontent-serverapp-c3877268260 style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: rgba(0, 0, 0, 0.1); z-index:1;" srcset="{{asset('storage/home_first_section')}}/{{$home_first_section->bg_image}}" />
                        <img
                            _ngcontent-serverapp-c3877268260
                            alt="breadcrumb"
                            onerror="this.onerror = null;
                            this.parentNode.children[0].srcset =
                            this.parentNode.children[1].srcset =this.parentNode.children[2].srcset= this.src;"
                            class="responsive-img"
                            src="{{asset('storage/home_first_section')}}/{{$home_first_section->bg_image}}"
                        />
                    </picture>

                    <!----><!---->
                    <div _ngcontent-serverapp-c3877268260>
                        <div _ngcontent-serverapp-c3877268260 class="container">
                            <div _ngcontent-serverapp-c3877268260 class="row">
                                <div _ngcontent-serverapp-c3877268260 class="col-11 mx-auto">
                                    <div _ngcontent-serverapp-c3877268260 class="center-text">
                                        <div _ngcontent-serverapp-c3877268260 class="mx-auto">
                                            <div _ngcontent-serverapp-c3877268260 class="header-text text-center">
                                                <h1 _ngcontent-serverapp-c3877268260>
                                                    <b _ngcontent-serverapp-c3877268260>
                                                        <span _ngcontent-serverapp-c3877268260 class="ng-star-inserted"> {{$home_first_section->first_heading}} </span>
                                                        <!----><!----><!---->
                                                    </b>
                                                </h1>
                                            </div>
                                            <div _ngcontent-serverapp-c3877268260 class="header-sub-text text-center">
                                                <p _ngcontent-serverapp-c3877268260 class="h2 text-white top-heading-text ng-star-inserted">{{$home_first_section->second_heading}} </p>
                                                <!----><!---->
                                            </div>
                                            <div _ngcontent-serverapp-c3877268260 class="link-horizontal">
                                                <ul _ngcontent-serverapp-c3877268260>
                                                    <li _ngcontent-serverapp-c3877268260 class="mx-auto">
                                                        <a _ngcontent-serverapp-c3877268260 class="btn home-btn btn-helper" href="/jobs"><b _ngcontent-serverapp-c3877268260> {{$home_first_section->first_button}} </b></a>
                                                    </li>
                                                    <li _ngcontent-serverapp-c3877268260 class="mx-auto">
                                                        <a _ngcontent-serverapp-c3877268260 class="btn home-btn btn-employer" href="/candidates"><b _ngcontent-serverapp-c3877268260>{{$home_first_section->sec_button}} </b></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div _ngcontent-serverapp-c3877268260 class="wave"></div>
                    </div>
                </section>


                <section _ngcontent-serverapp-c3877268260 class="hp-achievement">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row">

                            <div _ngcontent-serverapp-c3877268260 class="col-lg-2 col-md-4  res-customer-logo ng-star-inserted">
                                <div _ngcontent-serverapp-c3877268260 class="customer-logo">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_second_section')}}/{{$home_second_section->first_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Trustworthy helper,maid &amp; drivers hiring"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                            src="{{asset('storage/home_second_section')}}/{{$home_second_section->first_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-2 col-md-4  res-customer-logo ng-star-inserted">
                                <div _ngcontent-serverapp-c3877268260 class="customer-logo">
                                    <picture _ngcontent-serverapp-c3877268260 style="padding-left: 30px !important;">
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_second_section')}}/{{$home_second_section->second_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Trustworthy helper,maid &amp; drivers hiring"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                            src="{{asset('storage/home_second_section')}}/{{$home_second_section->second_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-2 col-md-4  res-customer-logo ng-star-inserted">
                                <div _ngcontent-serverapp-c3877268260 class="customer-logo">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_second_section')}}/{{$home_second_section->third_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Trustworthy helper,maid &amp; drivers hiring"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                            src="{{asset('storage/home_second_section')}}/{{$home_second_section->third_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-2 col-md-4  res-customer-logo ng-star-inserted">
                                <div _ngcontent-serverapp-c3877268260 class="customer-logo">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_second_section')}}/{{$home_second_section->forth_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Trustworthy helper,maid &amp; drivers hiring"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                            src="{{asset('storage/home_second_section')}}/{{$home_second_section->forth_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-2 col-md-4  res-customer-logo ng-star-inserted">
                                <div _ngcontent-serverapp-c3877268260 class="customer-logo">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_second_section')}}/{{$home_second_section->five_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Trustworthy helper,maid &amp; drivers hiring"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                            src="{{asset('storage/home_second_section')}}/{{$home_second_section->five_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-2 col-md-4  res-customer-logo ng-star-inserted">
                                <div _ngcontent-serverapp-c3877268260 class="customer-logo">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_second_section')}}/{{$home_second_section->six_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Trustworthy helper,maid &amp; drivers hiring"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                            src="{{asset('storage/home_second_section')}}/{{$home_second_section->six_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                            <!----><!---->
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-12 col-md-12 col-sm-12">
                                <div _ngcontent-serverapp-c3877268260 class="our-achievement-section">
                                    <div _ngcontent-serverapp-c3877268260 class="title2">
                                        <div _ngcontent-serverapp-c3877268260 class="sub-title our-achievement-title">
                                            <div _ngcontent-serverapp-c3877268260 class="hp-logo-candidate">
                                                <picture _ngcontent-serverapp-c3877268260>
                                                    <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_second_section')}}/{{$home_second_section->seven_image}}" />
                                                    <img
                                                        _ngcontent-serverapp-c3877268260
                                                        loading="lazy"
                                                        alt="helper place customer satisfaction"
                                                        height="70px"
                                                        width="70px"
                                                        onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                        class="achieve-winner"
                                                        src="{{asset('storage/home_second_section')}}/{{$home_second_section->seven_image}}"
                                                    />
                                                </picture>
                                            </div>
                                            <div _ngcontent-serverapp-c3877268260 class="achievement-data-wrapper">
                                                <div _ngcontent-serverapp-c3877268260 class="dark h2 hp-achieve-content">
                                                    <div _ngcontent-serverapp-c3877268260 class="hp-achieve-customer"><span _ngcontent-serverapp-c3877268260 class="number"></span> {{$home_second_section->first_heading}}</div>
                                                    <div _ngcontent-serverapp-c3877268260 class="hp-achieve-users"><span _ngcontent-serverapp-c3877268260 class="number"></span> {{$home_second_section->second_heading}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section _ngcontent-serverapp-c3877268260 class="our-community app2 my-5 format">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row">
                            <div _ngcontent-serverapp-c3877268260 class="col-md-5 ">
                                <div _ngcontent-serverapp-c3877268260 class="mobile-sc">
                                    <picture _ngcontent-serverapp-c3877268260 class="ng-star-inserted">
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:767px)" width="273" height="540" srcset="{{asset('storage/home_third_section')}}/{{$home_third_section->side_image}}" />
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:991px)" width="270" height="533" srcset="{{asset('storage/home_third_section')}}/{{$home_third_section->side_image}}" />
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_third_section')}}/{{$home_third_section->side_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Trustworthy helper,maid &amp; drivers hiring"
                                            width="354"
                                            height="700"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset =
                                            this.parentNode.children[1].srcset =this.parentNode.children[2].srcset= this.src;"
                                            src="{{asset('storage/home_third_section')}}/{{$home_third_section->side_image}}"
                                        />
                                    </picture>
                                    <!----><!----><!---->
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-md-7  our-community-content">
                                <div _ngcontent-serverapp-c3877268260>
                                    <div _ngcontent-serverapp-c3877268260 class="format-small-text">
                                        <div _ngcontent-serverapp-c3877268260 class="borders-before ar-borders-before ar text-uppercase pt-1"><span _ngcontent-serverapp-c3877268260> {{$home_third_section->first_text}} </span></div>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="title2 ar">
                                        <div _ngcontent-serverapp-c3877268260 class="sub-title">
                                            <h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize text-left ar">
                                                <span _ngcontent-serverapp-c3877268260 class="ng-star-inserted"> {{$home_third_section->heading}} </span>
                                                <!----><!----><!---->
                                            </h2>
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="format-sub-text ar ng-star-inserted">
                                        <p _ngcontent-serverapp-c3877268260>
                                            {{$home_third_section->paragraph}}
                                        </p>
                                    </div>
                                    <!----><!----><!---->
                                    <div _ngcontent-serverapp-c3877268260 class="community-btn ar">
                                        <a _ngcontent-serverapp-c3877268260 type="text" class="btn btn-primary btn-green" style="max-width: 200px;" href="/candidates"> {{$home_third_section->first_button}} </a>
                                        <a _ngcontent-serverapp-c3877268260 type="text" class="btn btn-secondary btn-yellow" style="max-width: 200px;" href="/jobs"> {{$home_third_section->second_button}} </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section _ngcontent-serverapp-c3877268260 class="find-candidate-wrapper">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row">
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-12 col-md-12 col-sm-12">
                                <div _ngcontent-serverapp-c3877268260 class="find-candidate-header">
                                    <div _ngcontent-serverapp-c3877268260>
                                        <div _ngcontent-serverapp-c3877268260 class="format-small-text">
                                            <div _ngcontent-serverapp-c3877268260 class="borders-before ar-borders-before ar text-uppercase pt-1"><span _ngcontent-serverapp-c3877268260> {{$home_fourth_section->first_text}} </span></div>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="title2">
                                            <div _ngcontent-serverapp-c3877268260 class="sub-title">
                                                <h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize text-left ng-star-inserted">{{$home_fourth_section->first_heading}}</h2>
                                                <!----><!----><!---->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div _ngcontent-serverapp-c3877268260 id="find-helper-carousel" class="ng-star-inserted">
                            <ngb-carousel
                                _ngcontent-serverapp-c3877268260
                                tabindex="0"
                                class="carousel slide team-slider format resume-right-block col-lg-12"
                                aria-activedescendant="slide-ngb-slide-54102"
                                style="display: block;"
                                ngskiphydration
                            >
                                <!--<div role="tablist" class="carousel-indicators">-->
                                <!--    <button type="button" data-bs-target role="tab" class="active ng-star-inserted" aria-labelledby="slide-ngb-slide-54102" aria-controls="slide-ngb-slide-54102" aria-selected="true"></button>-->
                                <!--    <button type="button" data-bs-target role="tab" class="ng-star-inserted" aria-labelledby="slide-ngb-slide-54103" aria-controls="slide-ngb-slide-54103" aria-selected="false"></button>-->
                                    <!---->
                                <!--</div>-->
                                <div class="carousel-inner">

                                    @foreach($candidates->chunk(3) as $key => $chunk)
                                    <div role="tabpanel" class="carousel-item active ng-star-inserted" id="slide-ngb-slide-54102">
                                        <span class="visually-hidden"> Slide {{ $loop->iteration }} of {{ $loop->count }} </span>
                                        <div _ngcontent-serverapp-c3877268260 class="carousel-inner-wrapper ng-star-inserted">
                                            <div _ngcontent-serverapp-c3877268260 class="blog-agency rounded mt-4 mb-5">

                                               @foreach($chunk as $candidate)
                                                <a _ngcontent-serverapp-c3877268260 class="blog-contain ng-star-inserted mt-1" href="{{route('candidate_details', $candidate)}}">
                                                    <div _ngcontent-serverapp-c3877268260 class="img-container">
                                                        <div _ngcontent-serverapp-c3877268260 class="blog-info-resume">
                                                            <div _ngcontent-serverapp-c3877268260 class="find-helper-img">
                                                                <img _ngcontent-serverapp-c3877268260 loading="lazy" alt="Maid jobs" class="related-profile-picture" src="{{ asset('storage/profile_image/' . $candidate->user->profile_image ??'') }}" onerror="this.src='{{asset('assets/images/no-imag-found.png')}}';" />
                                                            </div>
                                                            <div _ngcontent-serverapp-c3877268260 class="text-left">
                                                                <div _ngcontent-serverapp-c3877268260 class="blog-head"><h3 _ngcontent-serverapp-c3877268260 class="align-self-center">{{$candidate->first_name}} {{$candidate->middle_name}} {{$candidate->last_name}}</h3></div>
                                                                <p _ngcontent-serverapp-c3877268260 class="card-text">
                                                                    <app-icons _ngcontent-serverapp-c3877268260 name="map" _nghost-serverapp-c4219164779 class="icons">
                                                                        <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                                            <use _ngcontent-serverapp-c4219164779="" height="11" width="11" href="{{asset('assets/icons/custom.svg#map')}}" />
                                                                        </svg>
                                                                    </app-icons>
                                                                    {{$candidate->user_nationality->nationality ??''}}
                                                                </p>
                                                                <p _ngcontent-serverapp-c3877268260 class="card-text">
                                                                    <app-icons _ngcontent-serverapp-c3877268260 name="certificate" _nghost-serverapp-c4219164779 class="icons">
                                                                        <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                                            <use _ngcontent-serverapp-c4219164779="" height="12" width="12" href="{{asset('assets/icons/custom.svg#certificate')}}" />
                                                                        </svg>
                                                                    </app-icons>
                                                                    {{$candidate->position_apply}} | {{$candidate->job_type}}
                                                                </p>
                                                                <p _ngcontent-serverapp-c3877268260 class="card-text">
                                                                    <app-icons _ngcontent-serverapp-c3877268260 name="calendar" _nghost-serverapp-c4219164779 class="icons">
                                                                        <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                                            <use _ngcontent-serverapp-c4219164779="" height="12" width="12" href="{{asset('assets/icons/custom.svg#calendar')}}" />
                                                                        </svg>
                                                                    </app-icons>
                                                                    From {{ \Carbon\Carbon::parse($candidate->job_start_date ?? '')->format('d F Y') }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>

                                                @endforeach

                                            </div>
                                        </div>
                                        <!---->
                                    </div>

                                    @endforeach



                                </div>
                                <!----><!---->
                            </ngb-carousel>
                        </div>
                        <!---->
                        <div _ngcontent-serverapp-c3877268260>
                            <div _ngcontent-serverapp-c3877268260 class="find-helper-btn"><a _ngcontent-serverapp-c3877268260 type="text" class="btn btn-primary btn-green" href="/candidates"> {{$home_fourth_section->first_button}} </a></div>
                        </div>
                    </div>
                </section>
                <section _ngcontent-serverapp-c3877268260 class="powerful-feature team ng-star-inserted">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row">
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-12 col-md-12 col-sm-12">
                                <div _ngcontent-serverapp-c3877268260 class="title title2 powerful-feature-header">
                                    <img _ngcontent-serverapp-c3877268260 loading="lazy" alt="CITIMUBER Logo" width="45px" height="45px" class="img-fluid title-img" src="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->icon_image}}" />
                                    <div _ngcontent-serverapp-c3877268260 class="borders main-text"><span _ngcontent-serverapp-c3877268260>{{$home_fourth_section->second_text}}</span></div>
                                    <div _ngcontent-serverapp-c3877268260 class="title title2">
                                        <div _ngcontent-serverapp-c3877268260 class="sub-title"><h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize text-center">{{$home_fourth_section->second_heading}}</h2></div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-12 col-sm-11 col-md-4 col-lg-4 col-xl-4 my-2 py-1">
                                <div _ngcontent-serverapp-c3877268260 class="team-slider p-3">
                                    <div _ngcontent-serverapp-c3877268260 class="team-container text-center">
                                        <div _ngcontent-serverapp-c3877268260 class="text-center">
                                            <picture _ngcontent-serverapp-c3877268260>
                                                <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->first_image}}" />
                                                <img
                                                    _ngcontent-serverapp-c3877268260
                                                    loading="lazy"
                                                    alt="CITIMUBER Support"
                                                    width="60px"
                                                    height="60px"
                                                    onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                    class="img-fluid members my-2 mx-auto"
                                                    src="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->first_image}}"
                                                />
                                            </picture>
                                            <h3 _ngcontent-serverapp-c3877268260 style="font-weight: 600; padding: 10px 0;">{{$home_fourth_section->third_heading}}</h3>
                                            <div _ngcontent-serverapp-c3877268260>
                                                <p>{{$home_fourth_section->first_paragraph}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-12 col-sm-11 col-md-4 col-lg-4 col-xl-4 my-2 py-1">
                                <div _ngcontent-serverapp-c3877268260 class="team-slider p-3">
                                    <div _ngcontent-serverapp-c3877268260 class="team-container text-center">
                                        <div _ngcontent-serverapp-c3877268260 class="text-center">
                                            <picture _ngcontent-serverapp-c3877268260>
                                                <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->sec_image}}" />
                                                <img
                                                    _ngcontent-serverapp-c3877268260
                                                    loading="lazy"
                                                    alt="CITIMUBER Feature"
                                                    width="60px"
                                                    height="60px"
                                                    onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                    class="img-fluid members my-2 mx-auto"
                                                    src="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->sec_image}}"
                                                />
                                            </picture>
                                            <h3 _ngcontent-serverapp-c3877268260 style="font-weight: 600; padding: 10px 0;">{{$home_fourth_section->fourth_heading}}</h3>
                                            <div _ngcontent-serverapp-c3877268260>
                                                <p>{{$home_fourth_section->second_paragraph}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-12 col-sm-11 col-md-4 col-lg-4 col-xl-4 my-2 py-1">
                                <div _ngcontent-serverapp-c3877268260 class="team-slider p-3">
                                    <div _ngcontent-serverapp-c3877268260 class="team-container text-center">
                                        <div _ngcontent-serverapp-c3877268260 class="text-center">
                                            <picture _ngcontent-serverapp-c3877268260>
                                                <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->third_image}}" />
                                                <img
                                                    _ngcontent-serverapp-c3877268260
                                                    loading="lazy"
                                                    alt="CITIMUBER job"
                                                    width="60px"
                                                    height="60px"
                                                    onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                    class="img-fluid members my-2 mx-auto"
                                                    src="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->third_image}}"
                                                />
                                            </picture>
                                            <h3 _ngcontent-serverapp-c3877268260 style="font-weight: 600; padding: 10px 0;">{{$home_fourth_section->five_heading}}</h3>
                                            <div _ngcontent-serverapp-c3877268260>
                                                <p>{{$home_fourth_section->third_paragraph}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!---->
                <section _ngcontent-serverapp-c3877268260 class="about-helperplace app2">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row res-about animation-view">
                            <div _ngcontent-serverapp-c3877268260 class="col-xl-6 col-lg-6 col-md-6">
                                <div _ngcontent-serverapp-c3877268260 class="about-content-inner">
                                    <div _ngcontent-serverapp-c3877268260>
                                        <div _ngcontent-serverapp-c3877268260 class="title title2">
                                            <div _ngcontent-serverapp-c3877268260 class="sub-title">
                                                <h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize text-left ng-star-inserted">{{$home_five_section->first_heading}}</h2>
                                                <!----><!----><!---->
                                            </div>
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260>
                                        <div _ngcontent-serverapp-c3877268260 class="ad-sub-contain">
                                            <p _ngcontent-serverapp-c3877268260 class="about-para ng-star-inserted">
                                                        <span _ngcontent-serverapp-c3877268260>
                                                            {{$home_five_section->first_paragraph}}
                                                        </span>
                                            </p>
                                            <!----><!----><!---->
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="row pt-3">
                                        <div _ngcontent-serverapp-c3877268260 class="col-sm-4 col-md-4 col-lg-4 hp-about-data">
                                            <div _ngcontent-serverapp-c3877268260 class="about-data-content">
                                                <div _ngcontent-serverapp-c3877268260 class="card">
                                                    <div _ngcontent-serverapp-c3877268260 class="card-body text-center">
                                                        <span _ngcontent-serverapp-c3877268260 class="card-title">{{$home_five_section->user_count_heading}}</span><span _ngcontent-serverapp-c3877268260 class="card-subtitle mb-2 text-green">{{$home_five_section->user_text}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="col-sm-4 col-md-4 col-lg-4 hp-about-data">
                                            <div _ngcontent-serverapp-c3877268260 class="about-data-content">
                                                <div _ngcontent-serverapp-c3877268260 class="card">
                                                    <div _ngcontent-serverapp-c3877268260 class="card-body text-center">
                                                        <span _ngcontent-serverapp-c3877268260 class="card-title">{{$home_five_section->percentage_heading}}</span><span _ngcontent-serverapp-c3877268260 class="card-subtitle mb-2 text-yellow">{{$home_five_section->percentage_text}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="col-sm-4 col-md-4 col-lg-4 hp-about-data">
                                            <div _ngcontent-serverapp-c3877268260 class="about-data-content">
                                                <div _ngcontent-serverapp-c3877268260 class="card">
                                                    <div _ngcontent-serverapp-c3877268260 class="card-body text-center">
                                                        <span _ngcontent-serverapp-c3877268260 class="card-title">{{$home_five_section->satisfaction_heading}}</span><span _ngcontent-serverapp-c3877268260 class="card-subtitle mb-2 text-green">{{$home_five_section->satisfaction_text}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="pb-3">
                                        <p _ngcontent-serverapp-c3877268260 class="about-para mt-4 ng-star-inserted">
                                            {{$home_five_section->second_paragraph}}
                                        </p>
                                        <!----><!----><!---->
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="col-md-6 col-sm-12 p-0">
                                        <div _ngcontent-serverapp-c3877268260 class="register-btn">
                                            <a _ngcontent-serverapp-c3877268260 data-toggle="modal" data-target="#registerModal"
                                               type="text" class="btn btn-primary btn-green">
                                                {{$home_five_section->register_button}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-xl-6 col-lg-6 col-md-6">
                                <div _ngcontent-serverapp-c3877268260 class="mobile-sc-right">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:767px)" width="273" height="540" srcset="{{asset('storage/home_five_section')}}/{{$home_five_section->side_image}}" />
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:991px)" width="270" height="533" srcset="{{asset('storage/home_five_section')}}/{{$home_five_section->side_image}}" />
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_five_section')}}/{{$home_five_section->side_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="CITIMUBER hiring helper's choice"
                                            width="354"
                                            height="700"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset =
                                this.parentNode.children[2].srcset=this.src;"
                                            src="{{asset('storage/home_five_section')}}/{{$home_five_section->side_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section _ngcontent-serverapp-c3877268260 class="available-jobs-wrapper">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row animation-view">
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-12 col-md-12 col-sm-12">
                                <div _ngcontent-serverapp-c3877268260 class="available-jobs-header">
                                    <div _ngcontent-serverapp-c3877268260>
                                        <div _ngcontent-serverapp-c3877268260 class="format-small-text ar">
                                            <div _ngcontent-serverapp-c3877268260 class="borders-before ar-borders-before text-uppercase pt-1"><span _ngcontent-serverapp-c3877268260>{{$home_six_section->first_text}}</span></div>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="title2">
                                            <div _ngcontent-serverapp-c3877268260 class="sub-title">
                                                <h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize text-left ng-star-inserted">{{$home_six_section->first_heading}}</h2>
                                                <!----><!----><!---->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div _ngcontent-serverapp-c3877268260 id="find-helper-carousel" class="ng-star-inserted">
                            <ngb-carousel
                                _ngcontent-serverapp-c3877268260
                                tabindex="0"
                                class="carousel slide team-slider format resume-right-block col-lg-12"
                                aria-activedescendant="slide-ngb-slide-54104"
                                style="display: block;"
                                ngskiphydration
                            >
                                <div role="tablist" class="carousel-indicators">
                                    <button type="button" data-bs-target role="tab" class="active ng-star-inserted" aria-labelledby="slide-ngb-slide-54104" aria-controls="slide-ngb-slide-54104" aria-selected="true"></button>
                                    <button type="button" data-bs-target role="tab" class="ng-star-inserted" aria-labelledby="slide-ngb-slide-54105" aria-controls="slide-ngb-slide-54105" aria-selected="false"></button>
                                    <!---->
                                </div>
                                <div class="carousel-inner">

                                    @foreach($jobs->chunk(3) as $key => $chunk)

                                    <div role="tabpanel" class="carousel-item active ng-star-inserted" id="slide-ngb-slide-54104">
                                        <span class="visually-hidden"> Slide {{ $loop->iteration }} of {{ $loop->count }} </span>
                                        <div _ngcontent-serverapp-c3877268260 class="carousel-inner-wrapper ng-star-inserted">
                                            <div _ngcontent-serverapp-c3877268260 class="blog-agency rounded mt-4 mb-5">

                                               @foreach($chunk as $job)
                                                <a _ngcontent-serverapp-c3877268260 class="blog-contain ng-star-inserted" href="{{route('job_details', $job)}}">
                                                    <div _ngcontent-serverapp-c3877268260 class="img-container">
                                                        <div _ngcontent-serverapp-c3877268260 class="blog-info-resume">
                                                            <div _ngcontent-serverapp-c3877268260 class="find-helper-img">
                                                                @if($job->job_image)
                                                                    <img _ngcontent-serverapp-c3877268260 loading="lazy" alt="Maid jobs" class="related-profile-picture"
                                                                         src="{{asset('storage/job_picture/'.$job->job_image->img)}}"
                                                                    />
                                                                @endif
                                                            </div>
                                                            <div _ngcontent-serverapp-c3877268260 class="text-left">
                                                                <div _ngcontent-serverapp-c3877268260 class="blog-head">
                                                                    <h3 _ngcontent-serverapp-c3877268260 class="align-self-center">{{$job->job_title}}</h3>
                                                                </div>
                                                                <p _ngcontent-serverapp-c3877268260 class="card-text">{{ucfirst($job->employer_type)}} - {{$job->nationality->nationality ??''}}</p>
                                                                <p _ngcontent-serverapp-c3877268260 class="card-text">
                                                                    <app-icons _ngcontent-serverapp-c3877268260 name="map" _nghost-serverapp-c4219164779 class="icons">
                                                                        <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                                            <use _ngcontent-serverapp-c4219164779="" height="11" width="11" href="{{asset('assets/icons/custom.svg#map')}}" />
                                                                        </svg>
                                                                    </app-icons>
                                                                    {{$job->countryName->name ??''}}
                                                                </p>
                                                                <p _ngcontent-serverapp-c3877268260 class="card-text">
                                                                    <app-icons _ngcontent-serverapp-c3877268260 name="certificate" _nghost-serverapp-c4219164779 class="icons">
                                                                        <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                                            <use _ngcontent-serverapp-c4219164779="" height="12" width="12" href="{{asset('assets/icons/custom.svg#certificate')}}" />
                                                                        </svg>
                                                                    </app-icons>
                                                                    {{$job->position_offered}}  | {{$job->job_type}}
                                                                </p>
                                                                <p _ngcontent-serverapp-c3877268260 class="card-text">
                                                                    <app-icons _ngcontent-serverapp-c3877268260 name="calendar" _nghost-serverapp-c4219164779 class="icons">
                                                                        <svg _ngcontent-serverapp-c4219164779="" class="icons" height="17" width="17">
                                                                            <use _ngcontent-serverapp-c4219164779="" height="12" width="12" href="{{asset('assets/icons/custom.svg#calendar')}}" />
                                                                        </svg>
                                                                    </app-icons>
                                                                    From {{ \Carbon\Carbon::parse($job->resume_detail->job_start_date ?? '')->format('d F Y') }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                @endforeach

                                            </div>
                                        </div>
                                        <!---->
                                    </div>

                                    @endforeach



                                    <!---->
                                </div>
                                <!----><!---->
                            </ngb-carousel>
                        </div>
                        <!---->
                        <div _ngcontent-serverapp-c3877268260>
                            <div _ngcontent-serverapp-c3877268260 class="search-job-btn"><a _ngcontent-serverapp-c3877268260 type="text" class="btn btn-primary btn-green" href="jobs"> {{$home_six_section->first_button}} </a></div>
                        </div>
                    </div>
                </section>
                <section _ngcontent-serverapp-c3877268260 class="direct-hire-wrapper team ng-star-inserted">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row w-100">
                            <div _ngcontent-serverapp-c3877268260 class="col-lg-12 col-md-12 col-sm-12">
                                <div _ngcontent-serverapp-c3877268260 class="title title2 powerful-feature-header">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:576px)" srcset="{{asset('cdn-sub/front-app/assets/images/Just_logo-sm.webp')}}" />
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:768px)" srcset="{{asset('cdn-sub/front-app/assets/images/Just_logo-md.webp')}}" />
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_fourth_section')}}/{{$home_six_section->icon_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            loading="lazy"
                                            alt="CITIMUBER Logo"
                                            width="50"
                                            height="50"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset =
                                this.parentNode.children[2].srcset=this.src;"
                                            class="img-fluid title-img"
                                            src="{{asset('storage/home_six_section')}}/{{$home_six_section->icon_image}}"
                                        />
                                    </picture>
                                    <div _ngcontent-serverapp-c3877268260 class="title title2">
                                        <div _ngcontent-serverapp-c3877268260 class="sub-title"><h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize text-center">{{$home_six_section->second_heading}} </h2></div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-12 col-sm-11 col-md-4 col-lg-4 col-xl-4 my-2 py-1">
                                <div _ngcontent-serverapp-c3877268260 class="team-slider p-3">
                                    <div _ngcontent-serverapp-c3877268260 class="team-container text-center">
                                        <div _ngcontent-serverapp-c3877268260 class="text-center">
                                            <h3 _ngcontent-serverapp-c3877268260 style="font-weight: 600; padding: 10px 0;">{{$home_six_section->third_heading}}</h3>
                                            <p _ngcontent-serverapp-c3877268260 class="my-2">
                                                {{$home_six_section->first_paragraph}}
                                            </p>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="d-flex justify-content-center">
                                            <div _ngcontent-serverapp-c3877268260 width="100%" height="100px" class="feature-img">
                                                <picture _ngcontent-serverapp-c3877268260>
                                                    <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_fourth_section')}}/{{$home_six_section->first_image}}" />
                                                    <img
                                                        _ngcontent-serverapp-c3877268260
                                                        loading="lazy"
                                                        alt="Domestic Helper in Hong Kong"
                                                        height="100px"
                                                        width="200px"
                                                        onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                        class="title-img mx-1 my-3"
                                                        src="{{asset('storage/home_six_section')}}/{{$home_six_section->first_image}}"
                                                    />
                                                </picture>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="text-center pt-3">
                                            <a _ngcontent-serverapp-c3877268260 target="_blank" href="/candidates">
                                                <h3 _ngcontent-serverapp-c3877268260 class="custom-text dark my-2 custom-text-h3">{{$home_six_section->second_text}}</h3>
                                            </a>
                                            <a _ngcontent-serverapp-c3877268260 target="_blank" href="/candidates">
                                                <h3 _ngcontent-serverapp-c3877268260 class="custom-text light my-2 custom-text-h3">{{$home_six_section->third_text}}</h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-12 col-sm-11 col-md-4 col-lg-4 col-xl-4 my-2 py-1">
                                <div _ngcontent-serverapp-c3877268260 class="team-slider p-3">
                                    <div _ngcontent-serverapp-c3877268260 class="team-container text-center">
                                        <div _ngcontent-serverapp-c3877268260 class="text-center">
                                            <h3 _ngcontent-serverapp-c3877268260 style="font-weight: 600; padding: 10px 0;">{{$home_six_section->fourth_heading}}</h3>
                                            <p _ngcontent-serverapp-c3877268260 class="my-2">{{$home_six_section->second_paragraph}}</p>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="d-flex justify-content-center">
                                            <div _ngcontent-serverapp-c3877268260 width="100%" height="100px" class="feature-img">
                                                <picture _ngcontent-serverapp-c3877268260>
                                                    <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_six_section')}}/{{$home_six_section->sec_image}}" />
                                                    <img
                                                        _ngcontent-serverapp-c3877268260
                                                        loading="lazy"
                                                        alt="Domestic Helper in Singapore"
                                                        height="100px"
                                                        width="200px"
                                                        onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                        class="title-img mx-1 my-3"
                                                        src="{{asset('storage/home_six_section')}}/{{$home_six_section->sec_image}}"
                                                    />
                                                </picture>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="text-center pt-3">
                                            <a _ngcontent-serverapp-c3877268260 target="_blank" href="/candidates">
                                                <h3 _ngcontent-serverapp-c3877268260 class="custom-text dark my-2 custom-text-h3">{{$home_six_section->fourth_text}}</h3>
                                            </a>
                                            <a _ngcontent-serverapp-c3877268260 target="_blank" href="/jobs">
                                                <h3 _ngcontent-serverapp-c3877268260 class="custom-text light my-2 custom-text-h3">{{$home_six_section->five_text}}</h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-12 col-sm-11 col-md-4 col-lg-4 col-xl-4 my-2 py-1">
                                <div _ngcontent-serverapp-c3877268260 class="team-slider p-3">
                                    <div _ngcontent-serverapp-c3877268260 class="team-container text-center">
                                        <div _ngcontent-serverapp-c3877268260 class="text-center">
                                            <h3 _ngcontent-serverapp-c3877268260 style="font-weight: 600; padding: 10px 0;">{{$home_six_section->five_heading}}</h3>
                                            <p _ngcontent-serverapp-c3877268260 class="my-2">{{$home_six_section->third_paragraph}}</p>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="d-flex justify-content-center">
                                            <div _ngcontent-serverapp-c3877268260 width="100%" height="100px" class="feature-img">
                                                <picture _ngcontent-serverapp-c3877268260>
                                                    <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_six_section')}}/{{$home_six_section->third_image}}" />
                                                    <img
                                                        _ngcontent-serverapp-c3877268260
                                                        loading="lazy"
                                                        alt="Domestic Helper in Macau"
                                                        height="100px"
                                                        width="200px"
                                                        onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                        class="title-img mx-1 my-3"
                                                        src="{{asset('storage/home_six_section')}}/{{$home_six_section->third_image}}"
                                                    />
                                                </picture>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c3877268260 class="text-center pt-3 custom-text-h3">
                                            <a _ngcontent-serverapp-c3877268260 target="_blank" href="/jobs">
                                                <h3 _ngcontent-serverapp-c3877268260 class="custom-text dark my-2 custom-text-h3">{{$home_six_section->six_text}}</h3>
                                            </a>
                                            <a _ngcontent-serverapp-c3877268260 target="_blank" href="/jobs">
                                                <h3 _ngcontent-serverapp-c3877268260 class="custom-text light my-2 custom-text-h3">{{$home_six_section->seven_text}}</h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!---->
                <section _ngcontent-serverapp-c3877268260 class="our-community app2 my-5 format">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row animation-view">
                            <div _ngcontent-serverapp-c3877268260 class="col-md-5 ">
                                <div _ngcontent-serverapp-c3877268260 class="mobile-sc">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:991px)" width="270" height="533" srcset="{{asset('storage/home_seven_section')}}/{{@$home_seven_section->side_image}}" />
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_seven_section')}}/{{@$home_seven_section->side_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            alt="Helper rules &amp; regulations"
                                            width="354"
                                            height="700"
                                            loading="lazy"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset = this.src;"
                                            src="{{asset('storage/home_seven_section')}}/{{@$home_seven_section->side_image}}"
                                        />
                                    </picture>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-md-7  our-community-content">
                                <div _ngcontent-serverapp-c3877268260>
                                    <div _ngcontent-serverapp-c3877268260 class="format-small-text ar">
                                        <div _ngcontent-serverapp-c3877268260 class="borders-before ar-borders-before text-uppercase pt-1"><span _ngcontent-serverapp-c3877268260>{{$home_seven_section->first_text}}</span></div>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="title2">
                                        <div _ngcontent-serverapp-c3877268260 class="sub-title">
                                            <h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize ar text-left ng-star-inserted">{{$home_seven_section->heading}}</h2>
                                            <!----><!----><!---->
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="format-sub-text ar ng-star-inserted">
                                        <p _ngcontent-serverapp-c3877268260 class="about-para">
                                            {{$home_seven_section->paragraph}}
                                        </p>
                                    </div>
                                    <!----><!----><!---->
                                    <div _ngcontent-serverapp-c3877268260 class="community-btn community-btn-ar">
                                        <a _ngcontent-serverapp-c3877268260 type="text" class="btn btn-primary btn-green" style="max-width: 200px;" href="/training">{{$home_seven_section->first_button}}</a>
                                        <a _ngcontent-serverapp-c3877268260 type="text" class="btn btn-secondary btn-yellow btn-association" style="max-width: 200px;" href="/association"> {{$home_seven_section->second_button}} </a>
                                    </div>
                                    <div _ngcontent-serverapp-c3877268260 class="pt-5 play-store">
                                        <span _ngcontent-serverapp-c3877268260>{{$home_seven_section->second_text}}</span>
                                        <div _ngcontent-serverapp-c3877268260 class="d-flex flex-wrap pt-1">
                                            <div _ngcontent-serverapp-c3877268260 class="play-store mobile-store">
                                                <a _ngcontent-serverapp-c3877268260 href="/" target="_blank">
                                                    <picture _ngcontent-serverapp-c3877268260>
                                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:576px)" srcset="{{asset('cdn-sub/front-app/assets/images/mobile-google-play-sm.webp')}}" />
                                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_seven_section')}}/{{@$home_seven_section->playstore_image}}" />
                                                        <img
                                                            _ngcontent-serverapp-c3877268260
                                                            alt="CITIMUBER Android App"
                                                            loading="lazy"
                                                            height="40px"
                                                            width="134px"
                                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset = this.src;"
                                                            class="img-fluid pr-3"
                                                            src="{{asset('storage/home_seven_section')}}/{{@$home_seven_section->playstore_image}}"
                                                        />
                                                    </picture>
                                                </a>
                                            </div>
                                            <div _ngcontent-serverapp-c3877268260 class="app-store mobile-store">
                                                <a _ngcontent-serverapp-c3877268260 href="/" target="_blank">
                                                    <picture _ngcontent-serverapp-c3877268260>
                                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:576px)" srcset="{{asset('cdn-sub/front-app/assets/images/mobile-app-store-sm.webp')}}" />
                                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_seven_section')}}/{{@$home_seven_section->appstore_image}}" />
                                                        <img
                                                            _ngcontent-serverapp-c3877268260
                                                            alt="CITIMUBER iOS App"
                                                            loading="lazy"
                                                            height="40px"
                                                            width="134px"
                                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset = this.src;"
                                                            class="img-fluid"
                                                            src="{{asset('storage/home_seven_section')}}/{{@$home_seven_section->appstore_image}}"
                                                        />
                                                    </picture>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section _ngcontent-serverapp-c3877268260 class="our-clients app2 team ng-star-inserted">
                    <div _ngcontent-serverapp-c3877268260 class="container">
                        <div _ngcontent-serverapp-c3877268260 class="row animation-view">
                            <div _ngcontent-serverapp-c3877268260 class="col-md-10 offset-md-1">
                                <div _ngcontent-serverapp-c3877268260 class="title title2">
                                    <picture _ngcontent-serverapp-c3877268260>
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:576px)" srcset="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->icon_image}}" />
                                        <source _ngcontent-serverapp-c3877268260 media="(max-width:768px)" srcset="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->icon_image}}" />
                                        <source _ngcontent-serverapp-c3877268260 srcset="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->icon_image}}" />
                                        <img
                                            _ngcontent-serverapp-c3877268260
                                            loading="lazy"
                                            alt="CITIMUBER Logo"
                                            width="50"
                                            height="50"
                                            onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset =
                                this.parentNode.children[2].srcset = this.src;"
                                            class="img-fluid title-img"
                                            src="{{asset('storage/home_fourth_section')}}/{{$home_fourth_section->icon_image}}"
                                        />
                                    </picture>
                                    <div _ngcontent-serverapp-c3877268260 class="borders main-text"><span _ngcontent-serverapp-c3877268260>{{$home_eight_section->first_text}}</span></div>
                                    <div _ngcontent-serverapp-c3877268260 class="title title2">
                                        <div _ngcontent-serverapp-c3877268260 class="sub-title"><h2 _ngcontent-serverapp-c3877268260 class="section-header text-capitalize text-center">{{$home_eight_section->heading}}</h2></div>
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c3877268260 class="col-12">
                                <div _ngcontent-serverapp-c3877268260 id="find-helper-carousel">
                                    <ngb-carousel
                                        _ngcontent-serverapp-c3877268260
                                        tabindex="0"
                                        class="carousel slide team-slider format resume-right-block col-lg-12"
                                        aria-activedescendant="slide-ngb-slide-54106"
                                        style="display: block;"
                                        ngskiphydration
                                    >
                                        <div role="tablist" class="carousel-indicators">
                                            <button type="button" data-bs-target role="tab" class="active ng-star-inserted" aria-labelledby="slide-ngb-slide-54106" aria-controls="slide-ngb-slide-54106" aria-selected="true"></button>
                                            <button type="button" data-bs-target role="tab" class="ng-star-inserted" aria-labelledby="slide-ngb-slide-54107" aria-controls="slide-ngb-slide-54107" aria-selected="false"></button>
                                            <!---->
                                        </div>
                                        <div class="carousel-inner">
                                           @foreach($client_comment->chunk(3) as $key => $chunk)
                                               <div role="tabpanel" class="carousel-item @if($loop->first) active @endif ng-star-inserted" id="slide-ngb-slide-{{ $key }}">
                                                   <span class="visually-hidden"> Slide {{ $loop->iteration }} of {{ $loop->count }} </span>
                                                   <div _ngcontent-serverapp-c3877268260 class="carousel-inner-wrapper ng-star-inserted">
                                                       <div _ngcontent-serverapp-c3877268260 class="blog-agency rounded mt-4 mb-5">
                                                           @foreach($chunk as $client_comment)
                                                               <div _ngcontent-serverapp-c3877268260 class="blog-contain ng-star-inserted">
                                                                   <div _ngcontent-serverapp-c3877268260 class="team-container text-center">
                                                                       <div _ngcontent-serverapp-c3877268260 class="client-img-wrapper">
                                                                           <img
                                                                               _ngcontent-serverapp-c3877268260
                                                                               loading="lazy"
                                                                               alt="Testimonials"
                                                                               width="80px"
                                                                               height="80px"
                                                                               class="img-fluid members"
                                                                               src="{{ asset('storage/client_says/' . $client_comment->image) }}"
                                                                           />
                                                                           <div _ngcontent-serverapp-c3877268260 class="quote-icon">
                                                                               <picture _ngcontent-serverapp-c3877268260>
                                                                                   <source _ngcontent-serverapp-c3877268260 srcset="{{ asset('cdn-sub/front-app/assets/images/quotes-sm.webp') }}" />
                                                                                   <img
                                                                                       _ngcontent-serverapp-c3877268260
                                                                                       alt="Quotes"
                                                                                       width="20px"
                                                                                       loading="lazy"
                                                                                       onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"
                                                                                       src="{{ asset('cdn-sub/front-app/assets/images/quotes.png') }}"
                                                                                   />
                                                                               </picture>
                                                                           </div>
                                                                       </div>
                                                                       <div _ngcontent-serverapp-c3877268260 class="text-center">
                                                                           <p _ngcontent-serverapp-c3877268260 class="team-para">
                                                                               {{ $client_comment->comment }}
                                                                           </p>
                                                                           <h3 _ngcontent-serverapp-c3877268260 class="name custom-text-h3" style="font-weight: 600; font-size: 18px; margin-bottom: 10px; margin-top: 20px;">
                                                                               {{ $client_comment->client_name }}
                                                                           </h3>

                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           @endforeach
                                                       </div>
                                                   </div>

                                               </div>
                                           @endforeach
                                       </div>


                                        <!----><!---->
                                    </ngb-carousel>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!---->
            </section>
@endsection
