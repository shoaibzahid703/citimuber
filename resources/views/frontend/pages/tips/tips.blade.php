@extends('frontend.layouts.app')
@section('title','TIPS')

@section('content')
<style>
  .blog-ar {
    margin-top: 97px;
  }
</style>
<section _ngcontent-serverapp-c3727738402 class="agency news-section pb-5 custom-container page-section blog-ar">
  <div _ngcontent-serverapp-c3727738402 class="container">
    <div _ngcontent-serverapp-c3727738402 class="row">
      <div _ngcontent-serverapp-c3727738402 class="col-12">
        <ul _ngcontent-serverapp-c3727738402 class="nav nav-tabs custom-tab">

          <a href="/news" style="width: 50%;">
            <li style="width: 100%;" class=><i class="fa fa-newspaper-o"></i><span>News</span></li>
          </a>
          <a href="/tips" style="width: 50%;">
            <li style="width: 100%;" class="active"><i class="fa fa-lightbulb-o"></i><span>Tips</span></li>
          </a>
        </ul>
      </div>
    </div>
    <!---->
    <div _ngcontent-serverapp-c3727738402 class="tips-news-section ng-star-inserted">
      <app-tips _ngcontent-serverapp-c3727738402 _nghost-serverapp-c116635558 ngh="3">
        <div _ngcontent-serverapp-c116635558 class="row mt-4">
          <div _ngcontent-serverapp-c116635558 class="col-12">
            <div _ngcontent-serverapp-c116635558 class="top-banner-wrapper">
              <div _ngcontent-serverapp-c116635558 class="top-banner-content small-section">
                <h1 _ngcontent-serverapp-c116635558><span _ngcontent-serverapp-c116635558>{{$tips_page->first_heading}}</span></h1>
                <p _ngcontent-serverapp-c116635558 class="header_2 mt-2"> {{$tips_page->paragraph}}</p>
              </div>
            </div>
          </div>
        </div>
        <div _ngcontent-serverapp-c116635558 class="row mt-5 mb-5 ng-star-inserted">

          @foreach($all_categories as $category)
    <div class="col-12 col-lg-6 custom-tip-block ng-star-inserted mt-3">
        <h2 class="ar-right ng-star-inserted">{{ $category->name }}</h2>
        <div class="list-group ar-right ng-star-inserted" id="tips-list-{{ $category->id }}">
            @foreach($all_tips->where('category_id', $category->id)->take(3) as $tip)
                <a class="list-group-item list-group-item-action ng-star-inserted tip-item" href="{{route('tips_details', $tip)}}">
                    <i aria-hidden="true" class="fa fa-angle-double-right"></i>
                    {{ $tip->title }}
                </a>
            @endforeach
            
            <!-- Additional Tips will be appended here -->
            <div class="additional-tips"></div>
            
            <!-- See More Button -->
            @if($all_tips->where('category_id', $category->id)->count() > 3)
                <a class="list-group-item list-group-item-action text-center see-more ng-star-inserted" 
                   data-category-id="{{ $category->id }}" data-offset="3">
                    See More
                </a>
            @endif
        </div>
    </div>
@endforeach


          <!---->
        </div>
        <!---->
        <!---->
      </app-tips>
    </div>
    <!---->
  </div>
</section>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function () {
        $('.see-more').on('click', function () {
            var categoryId = $(this).data('category-id');
            var offset = $(this).data('offset');

            $.ajax({
                url: '/fetch-more-tips',
                method: 'GET',
                data: {
                    category_id: categoryId,
                    offset: offset
                },
                success: function (response) {
                    // Append new tips to the list
                    $('#tips-list-' + categoryId + ' .additional-tips').append(response.html);
                    
                    // Update offset
                    offset += response.count;
                    if (response.hasMore) {
                        $('.see-more[data-category-id="' + categoryId + '"]').data('offset', offset);
                    } else {
                        // Remove "See More" button if no more tips
                        $('.see-more[data-category-id="' + categoryId + '"]').remove();
                    }
                }
            });
        });
    });
</script>

@endsection