@extends('frontend.layouts.app')
@section('title','PRICING')

@section('content')
<style>
  .top-banner-wrapper {
    margin-top: 97px;
  }
</style>

<section _ngcontent-serverapp-c4210318105 class="page-section">
  <section _ngcontent-serverapp-c4210318105 class="app2 mt-3 pricing">
    <div _ngcontent-serverapp-c4210318105 class="container">
      <!---->
      <!---->
      <div _ngcontent-serverapp-c4210318105 class>
        <div _ngcontent-serverapp-c4210318105 class="row mt-4">
          <div _ngcontent-serverapp-c4210318105 class="col-12">
            <div _ngcontent-serverapp-c4210318105 class="top-banner-wrapper ng-star-inserted">
              <div _ngcontent-serverapp-c4210318105 class="top-banner-content small-section">
                <h1 _ngcontent-serverapp-c4210318105 class="ng-star-inserted"><span _ngcontent-serverapp-c4210318105>Easy solution to connect Employers and Helpers</span></h1>
                <p _ngcontent-serverapp-c4210318105 class="header_2 ng-star-inserted"> Select your plan and access to a large database of domestic helpers looking for a new employer. Easily connect, message and interview the best candidates for your family. </p>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!---->
      <div _ngcontent-serverapp-c4210318105 class="row">
        <div _ngcontent-serverapp-c4210318105 class="col-12 ng-star-inserted">
          <div _ngcontent-serverapp-c4210318105 class="title title2 title-inner">
            <div _ngcontent-serverapp-c4210318105 class="main-title mt-4">
              <div _ngcontent-serverapp-c4210318105 class="row mt-4 text-center currency">

                <form novalidate class="col-12 col-sm-6 col-md-6 col-lg-6 mx-auto">
                   <label class="col-12"><b>Select Currency: </b> &nbsp;&nbsp;</label>
                   <select id="currencySelect" class="col-12 pr-2 pl-1 py-2 mx-auto rounded matNativeControl drop_box">
                       <option disabled selected value>-- Select --</option>
                       @foreach($countries as $country)
                           <option value="{{ $country }}" class="ng-star-inserted">{{ $country }}</option>
                       @endforeach
                   </select>
               </form>
              </div>
            </div>
          </div>
        </div>
        <div _ngcontent-serverapp-c4210318105 class="col-12 ng-star-inserted">
          <div _ngcontent-serverapp-c4210318105 id="find-helper-carousel">
            <ngb-carousel _ngcontent-serverapp-c4210318105 tabindex="0" class="carousel slide pricing-slider price-margin" aria-activedescendant="slide-ngb-slide-54071" style="display: block;" ngskiphydration>
              <div role="tablist" class="carousel-indicators">
                <button type="button" data-bs-target role="tab" class="active ng-star-inserted" aria-labelledby="slide-ngb-slide-54071" aria-controls="slide-ngb-slide-54071" aria-selected="true"></button>
                <!---->
              </div>
              <div class="carousel-inner">
                <div role="tabpanel" class="carousel-item active ng-star-inserted" id="slide-ngb-slide-54071">
                  <span class="visually-hidden"> Slide 1 of 1 </span>



                  <div _ngcontent-serverapp-c4210318105 id="plansContainer" class="blog-agency rounded mt-4 mb-5" >
                    @foreach($plans as $plan)
                     @php
                          $manualPrices = explode(',', $plan->manual_price);
                          $countryList = explode(',', $plan->country);
                          if (count($countryList) === count($manualPrices)) {
                                $countryPriceMap = array_combine($countryList, $manualPrices);
                            } else {
                                $countryPriceMap = [];
                            }
                      @endphp
                    <div _ngcontent-serverapp-c4210318105 class="blog-contain ng-star-inserted" data-country="{{ $plan->country }}" data-prices='@json($countryPriceMap)'>
                      <div _ngcontent-serverapp-c4210318105 class="price-container price-margin shadows text-center">
                        <div _ngcontent-serverapp-c4210318105 class="feature-text border-bottom-helper-green">
                          <h3 _ngcontent-serverapp-c4210318105 class="feature-text-heading text-center"> {{$plan->name}} </h3>
                        </div>
                        <div _ngcontent-serverapp-c4210318105 class="price-feature-container">
                          <div _ngcontent-serverapp-c4210318105 class="price-value">
                            <h6 _ngcontent-serverapp-c4210318105 class="price text-center ng-star-inserted">
                              <span _ngcontent-serverapp-c4210318105 class="currency" >US </span>

                              <span _ngcontent-serverapp-c4210318105 class="large" data-price="{{ $plan->price }}">{{$plan->price}}</span>
                            </h6>
                            <h5 _ngcontent-serverapp-c4210318105 class="price-feature mt-1 text-center ng-star-inserted"> FOR {{$plan->post_duration}} DAYS</h5>
                          </div>
                           @if(Auth::check())
                           @if(Auth::user()->role == \App\Models\User::ROLE_EMPLOYER)

                           <a _ngcontent-serverapp-c4210318105 href="{{route('post_job')}}" class="btn btn-default btn-white"> GET STARTED </a>
                          @endif
                          @else
                           <a _ngcontent-serverapp-c4210318105 type="button" class="btn btn-default btn-white" data-toggle="modal" data-target="#loginModal"> GET STARTED </a>
                          @endif
                          <hr _ngcontent-serverapp-c4210318105 class="set-border">

{{--                          @php--}}
{{--                          $features = explode(',', $plan->features);--}}
{{--                          @endphp--}}
{{--                          @foreach($features as $feature)--}}
                          <div _ngcontent-serverapp-c4210318105 class="price-features ng-star-inserted px-2" style="text-align: left">
{{--                            <h5 _ngcontent-serverapp-c4210318105 class="price-feature text-center">--}}
{{--                              <div _ngcontent-serverapp-c4210318105 class="features-price">--}}
{{--                                <div _ngcontent-serverapp-c4210318105 class="price-label">--}}
{{--                                  <i _ngcontent-serverapp-c4210318105 aria-hidden="true" class="fa fa-check mr-2 ng-star-inserted"></i>--}}
{{--                                  <div _ngcontent-serverapp-c4210318105 style="word-break: break-word;">{{ $feature }}</div>--}}
{{--                                </div>--}}
{{--                              </div>--}}
{{--                            </h5>--}}
                              {!! $plan->features  !!}
                          </div>
{{--                          @endforeach--}}
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>

              </div>
            </ngb-carousel>
          </div>
        </div>
        <div _ngcontent-serverapp-c4210318105 class="col-4 mt-5">
          <!---->
        </div>
        <div _ngcontent-serverapp-c4210318105 class="col-4 mt-5">
          <!---->
        </div>
        <div _ngcontent-serverapp-c4210318105 class="col-4 mt-5">
          <!---->
        </div>
      </div>
    </div>
  </section>
  <div _ngcontent-serverapp-c4210318105 class="break-div"></div>
  <!---->
  <!---->
  <section _ngcontent-serverapp-c4210318105 class="bg bg-dark py-4">
    <div _ngcontent-serverapp-c4210318105 class="container">
      <div _ngcontent-serverapp-c4210318105 class="row text-center">
        <div _ngcontent-serverapp-c4210318105 class="col-12">
          <h4 _ngcontent-serverapp-c4210318105 class="text-light"> Got a question?</h4>
          <h5 _ngcontent-serverapp-c4210318105 class="text-light mb-3"> We are here to help you. Check out our FAQs or send an email to our support team. </h5>
          <a _ngcontent-serverapp-c4210318105 href="mailto:support@CITIMUBER.com" class="btn btn_custom_color text-light text-center my-2 mx-auto btn-lg rounded"> CONTACT US </a>
        </div>
      </div>
    </div>
  </section>
</section>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
    const currencySelect = document.getElementById('currencySelect');
    const plansContainer = document.getElementById('plansContainer');
    const plans = plansContainer.getElementsByClassName('blog-contain');

    currencySelect.addEventListener('change', function () {
        const selectedCountry = this.value;

        for (let i = 0; i < plans.length; i++) {
            const plan = plans[i];
            const planCountries = plan.getAttribute('data-country').split(',');
            const prices = JSON.parse(plan.getAttribute('data-prices') || '{}');
            const currencySpan = plan.querySelector('.currency');
            const priceSpan = plan.querySelector('.large');

            if (planCountries.includes(selectedCountry)) {
                const newPrice = prices[selectedCountry] || priceSpan.dataset.price;
                const countryCode = selectedCountry; // Assuming the country name itself is the code

                priceSpan.textContent = newPrice;
                currencySpan.textContent = `${countryCode} $`;
                plan.style.display = 'block';
            } else {
                plan.style.display = 'none';
            }
        }
    });
});


</script>
<!-- <script type="text/javascript">
  document.addEventListener('DOMContentLoaded', function() {
    const currencySelect = document.getElementById('currencySelect');
    currencySelect.addEventListener('change', function() {
      const selectedCountryCode = currencySelect.value;
      fetchCurrency(selectedCountryCode);
    });

    function fetchCurrency(countryCode) {
      const apiUrl = `https://restcountries.com/v3.1/alpha/${countryCode}`;
      fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
          if (data.length > 0) {
            const countryData = data[0];
            const currencyKey = Object.keys(countryData.currencies)[0];
            const currencyCode = countryData.currencies[currencyKey].code || currencyKey;
            if (currencyCode) {
              fetchConversionRate(currencyCode);
            } else {
              console.error('Currency not found for selected country.');
            }
          } else {
            console.error('Invalid country code or no data returned.');
          }
        })
        .catch(error => console.error('Error fetching currency information:', error));
    }

    function fetchConversionRate(currency) {
      const apiKey = '804207b651131734780b029a';
      const apiUrl = `https://v6.exchangerate-api.com/v6/${apiKey}/latest/USD`;
      fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
          const usdToSelectedCurrencyRate = data.conversion_rates[currency];
          if (usdToSelectedCurrencyRate) {
            updatePrices(usdToSelectedCurrencyRate, currency);
          } else {
            console.error('Conversion rate for selected currency not found.');
          }
        })
        .catch(error => console.error('Error fetching conversion rate:', error));
    }

    function updatePrices(conversionRate, currency) {
      const priceElements = document.querySelectorAll('.price .large');
      priceElements.forEach(priceElement => {
        const originalPrice = parseFloat(priceElement.getAttribute('data-price'));
        const convertedPrice = (originalPrice * conversionRate).toFixed(2);
        priceElement.innerText = convertedPrice;
        priceElement.previousElementSibling.innerText = currency + '$';
      });
    }
  });
</script> -->
@endsection
