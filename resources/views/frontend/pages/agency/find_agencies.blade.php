@extends('frontend.layouts.app')
@section('title','AGENCY SERVICES')

@section('content')
    <style>
        .find-agency-ar{
            margin-top: 97px;
        }
    </style>

    <section _ngcontent-serverapp-c93986215 class="page-section">
        <section _ngcontent-serverapp-c93986215 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom-container find-agency-ar">
            <div _ngcontent-serverapp-c93986215 class="container">
                <div _ngcontent-serverapp-c93986215 class="row">
                    <div _ngcontent-serverapp-c93986215 class="col-12">
                        <ul _ngcontent-serverapp-c93986215 class="nav nav-tabs custom-tab">
                            <a href="{{route('agencies_services')}}" style="width: 25%;">  <li _ngcontent-serverapp-c93986215 style="width:100%;" class="active"><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Agency</span></li></a>
                            <a href="/training" style="width: 25%;">  <li _ngcontent-serverapp-c93986215 style="width:100%;" class><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Training</span></li></a>
                            <a href="/partner" style="width: 25%;"> <li _ngcontent-serverapp-c93986215 style="width:100%;" class><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Partner</span></li></a>
                            <a href="/association" style="width: 25%;"><li _ngcontent-serverapp-c93986215 style="width:100%;" class><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Association</span></li></a>
                        </ul>
                    </div>
                </div>
                <div _ngcontent-serverapp-c93986215 class>
                    <div _ngcontent-serverapp-c93986215 class="row mt-4">
                        <div _ngcontent-serverapp-c93986215 class="col-12">
                            <div _ngcontent-serverapp-c93986215 class="top-banner-wrapper">
                                <div _ngcontent-serverapp-c93986215 class="top-banner-content small-section">
                                    <h1 _ngcontent-serverapp-c93986215 class="ng-star-inserted">
                                        <span _ngcontent-serverapp-c93986215>{{$agencies->first_heading}}</span>
                                    </h1>
                                    <!----><!----><!----><!---->
                                    <p _ngcontent-serverapp-c93986215 class="header_2 ng-star-inserted">{{$agencies->paragraph}} </p>
                                    <!----><!----><!----><!----><!---->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div _ngcontent-serverapp-c93986215 class="row mt-4">
                    <div _ngcontent-serverapp-c93986215 class="col-md-12 col-lg-12">
                        <div _ngcontent-serverapp-c93986215 class="collection-product-wrapper">
                            <div _ngcontent-serverapp-c93986215 class="row ng-star-inserted">
                                @if($agency_infos->count() == 0)
                                    <div class="col-md-12 text-center">
                                        <h4 class="text-info">No Agency Found</h4>
                                    </div>
                                @else
                                    @foreach($agency_infos as $info)
                                        <div _ngcontent-serverapp-c93986215 class="col-12 col-md-6 col-lg-6 pt-3 pb-3 custom-agency-container ng-star-inserted">
                                            <div _ngcontent-serverapp-c93986215 class="agency-box bg bg-light">
                                                <a _ngcontent-serverapp-c93986215 routerlinkactive="router-link-active" href="{{ route('agency_details',$info) }}">
                                                    <div _ngcontent-serverapp-c93986215 class="agency_header w-10"
                                                         style="background-image: url({{asset('storage/company_banner_image').'/'.$info->info->company_banner_image}});"
                                                    >
                                                        <div _ngcontent-serverapp-c93986215 class="agency_header_White_opcity">
                                                            <div _ngcontent-serverapp-c93986215 class="Contacted_tag_right_top position-absolute text-center ng-star-inserted">
                                                                Licence: {{ $info->info->license_no }}
                                                            </div>
                                                            <!---->
                                                        </div>
                                                    </div>
                                                    <div _ngcontent-serverapp-c93986215 class="yoga-circle">
                                                        <picture _ngcontent-serverapp-c93986215>
                                                            <source _ngcontent-serverapp-c93986215 width="98px" height="98px" media="(max-width:450px)"
                                                                    srcset="{{asset('storage/company_image').'/'.$info->info->company_image}}">
                                                            <source _ngcontent-serverapp-c93986215 width="128px" height="128px" media="(max-width:768px)"
                                                                    srcset="{{asset('storage/company_image').'/'.$info->info->company_image}}">
                                                            <source _ngcontent-serverapp-c93986215 width="98px" height="98px" media="(max-width:992px)"
                                                                    srcset="{{asset('storage/company_image').'/'.$info->info->company_image}}">
                                                            <img _ngcontent-serverapp-c93986215 width="128px" height="128px"
                                                                 src="{{asset('storage/company_image').'/'.$info->info->company_image}}"
                                                            >
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c93986215 class="agency-box-detail">
                                                        <div _ngcontent-serverapp-c93986215 class="agency-box-detail-left">
                                                            <div _ngcontent-serverapp-c93986215 class="ratting_div text-center mr-0 ng-star-inserted">
                                                                @for ($i = 1; $i <= 5; $i++)
                                                                    @if ($i <= $info->max_rating)
                                                                        <span class="fa fa-star"></span> <!-- Filled star -->
                                                                    @else
                                                                        <span class="fa fa-star-o"></span> <!-- Empty star -->
                                                                    @endif
                                                                @endfor
                                                            </div>
                                                            @if($info->user_location)
                                                                <span _ngcontent-serverapp-c93986215 class="mt-2 primary custom_font_size_for_location text-center p-0 custom_h4 ng-star-inserted"><i _ngcontent-serverapp-c93986215 class="fa fa-map-marker"></i>
                                                            {{ $info->user_location->name }}
                                                        </span>
                                                            @endif

                                                        </div>
                                                        <div _ngcontent-serverapp-c93986215 class="agency-box-detail-right">
                                                            <h2 _ngcontent-serverapp-c93986215 class="company_title_fix_size text-left"> {{$info->company_name}} </h2>
                                                            <div _ngcontent-serverapp-c93986215 class="listing-about-txt mt-2">
                                                               {{ $info->info->company_short_intro }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div _ngcontent-serverapp-c93986215 class="product-detail mt-2 mb-3 w-100 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c93986215 class="agency_pricing_discount">
                                                        <div _ngcontent-serverapp-c93986215 class="sub_agency_discount">
                                                            <span _ngcontent-serverapp-c93986215 class="ng-star-inserted">Accredited Agency</span><!---->
                                                        </div>
                                                        <div _ngcontent-serverapp-c93986215 class="agency_discount_text"> Additional Services </div>
                                                    </div>
                                                    <div _ngcontent-serverapp-c93986215 class="container agency-listing-detail-bottom">
                                                        <div _ngcontent-serverapp-c93986215>
                                                            <div _ngcontent-serverapp-c93986215 class="row bg-light">
                                                                <div _ngcontent-serverapp-c93986215 class="col-12 year-experience-title text-left">
                                                                    <h3 _ngcontent-serverapp-c93986215 class="text-left mt-2">Our Services</h3>
                                                                    <div _ngcontent-serverapp-c93986215 class="our-service-detail">
                                                                        @foreach(explode(',',$info->info->agency_expert) as $expert_id)
                                                                            @php
                                                                                $expert  = \App\Models\AgencyExperts::where('id',$expert_id)->first();
                                                                            @endphp
                                                                            @if($expert)
                                                                                <span _ngcontent-serverapp-c93986215 class="ng-star-inserted"> {{$expert->name}}</span>
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                    {{--                                                                <div _ngcontent-serverapp-c93986215 class="candidate-working-status my-2">--}}
                                                                    {{--                                                                    <div _ngcontent-serverapp-c93986215 class="pass_right pb-1">--}}
                                                                    {{--                                                                        <span _ngcontent-serverapp-c93986215 class="col-4 col-sm-4 col-md-4 cl-lg-4 col-xl-4 ng-star-inserted">--}}
                                                                    {{--                                                                            <img _ngcontent-serverapp-c93986215 width="18px" height="18px" class="flag id fnone" src="{{asset('cdn-sub/country/philippines.svg')}}" alt="">--}}
                                                                    {{--                                                                            Philippines--}}
                                                                    {{--                                                                        </span><!---->--}}
                                                                    {{--                                                                    </div>--}}
                                                                    {{--                                                                </div>--}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div _ngcontent-serverapp-c93986215 class="row">
                                                            @auth
                                                                <button _ngcontent-serverapp-c93986215 class="col-6 button_bg button_contact send_message"
                                                                        data-toggle="modal"
                                                                        data-target="#messageModal"
                                                                        data-url="{{route('send_helper_message',$info)}}"
                                                                        style="border-right: 1px solid #fff;">
                                                                    Contact Us
                                                                </button>
                                                            @else
                                                                <button _ngcontent-serverapp-c93986215 class="col-6 button_bg button_contact"
                                                                        data-toggle="modal" data-target="#loginModal"
                                                                        style="border-right: 1px solid #fff;">
                                                                    Contact Us
                                                                </button>
                                                            @endauth
                                                            <a _ngcontent-serverapp-c93986215 routerlinkactive="router-link-active" class="col-6 button_bg button_more_details" href="{{ route('agency_details',$info) }}">
                                                                More Details
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!----><!---->
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                                <!---->
                            </div>
                            <!----><!----><!----><!---->
                            <div _ngcontent-serverapp-c93986215 class="container-fluid mt-5">
                                <div _ngcontent-serverapp-c93986215 class="row mt-5">
                                    <div _ngcontent-serverapp-c93986215 class="mt-5 mb-4 mx-auto text-center">
                                        {{ $agency_infos->links()  }}
                                    </div>
                                </div>
                            </div>
                            <div _ngcontent-serverapp-c93986215 class="container-fluid my-4 pt-4 ng-star-inserted">
                                <div _ngcontent-serverapp-c93986215 class="row">
                                    <div _ngcontent-serverapp-c93986215 class="col-12 extra_details_bottom">
                                        <p _ngcontent-serverapp-c93986215>
                                            {!! $agencies->description !!}
                                        </p>

                                    </div>
                                </div>
                            </div>
                            <!----><!----><!----><!---->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="{{ asset('assets/images/site-logo.jpg') }}" class="w-50">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mt-2">
                        <div class="col-12">
                            <form class="helper-form mt-3" method="post" action="" id="send_message_form">
                                @csrf
                                <div class="form-group">
                                    <label for="message" style="color:black !important;">Message </label>
                                    <textarea name="message" required class="form-control" id="message"  autocomplete="off" placeholder="message"></textarea>
                                    <div id="message-error" class="invalid-feedback"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: #000; color:white;">Close</button>
                                    <button type="submit" class="btn btn-primary" style="background-color: #B50000; color:white;">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@push('js')
    <script>
        $(document).on("click", ".send_message", function () {
            const url = $(this).data('url');
            $('#send_message_form').attr('action',url);
        });
    </script>
@endpush
