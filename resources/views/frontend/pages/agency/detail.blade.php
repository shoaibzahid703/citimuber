
<div class="row">
    <div class="col-md-12">
        <h2>{{$service->name}}</h2>
    </div>
    <div class="col-md-6 mt-3">
       <h4> From HK$ {{ number_format($service->price,2,',') }}</h4>
    </div>
    @if(is_null($type))
        <div class="col-md-6 mt-3" style="text-align: end">
            <h4>
                <i _ class="fa fa-calendar light align-text-top mr-1 ng-tns-c147279433-7"></i>  Max Time: {{$service->no_of_weeks}} Weeks
            </h4>
        </div>
    @endif
    <div class="col-md-12 mt-3">
      {!! $service->description !!}
    </div>
</div>
