@extends('frontend.layouts.app')
@section('title','ASSOCIATION DETAILS')

@section('content')
    <style>
        .find-agency-ar{
            margin-top: 97px;
        }
        .star-rating {
            line-height:32px;
            font-size:1.25em;
        }

        .star-rating .fa-star{color: red;}
    </style>

    <section _ngcontent-serverapp-c1214074651 class="page-section agency-view-ar ng-tns-c1214074651-1">
        <section _ngcontent-serverapp-c1214074651 class="agency breadcrumb-section-main breadcrumb-title inner-3 ng-tns-c1214074651-1">
            <div _ngcontent-serverapp-c1214074651 class="container px-2 py-4 ng-tns-c1214074651-1 ng-star-inserted">
                <div _ngcontent-serverapp-c1214074651 class="row px-1 ng-tns-c1214074651-1">
                    <div _ngcontent-serverapp-c1214074651 class="col-12 ng-tns-c1214074651-1">
                        <div _ngcontent-serverapp-c1214074651 class="breadcrumb-contain ng-tns-c1214074651-1">
                            <div _ngcontent-serverapp-c1214074651 class="text-left ng-tns-c1214074651-1">
                                <ul _ngcontent-serverapp-c1214074651 class="custom-breadcrum ng-tns-c1214074651-1">
                                    <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                        <a _ngcontent-serverapp-c1214074651 routerlink="/" class="ng-tns-c1214074651-1" href="/">
                                            <i _ngcontent-serverapp-c1214074651 class="fa fa-home ng-tns-c1214074651-1"></i>
                                        </a
                                    </li>
                                    <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                        <a _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1" href="agency&services">
                                            <i _ngcontent-serverapp-c1214074651 class="fa ng-tns-c1214074651-1 fa-angle-double-right"></i>Find
                                            <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted"> Agency </span><!----><!----><!----><!---->
                                        </a>
                                    </li>
                                    <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                        <a _ngcontent-serverapp-c1214074651 href="#" class="ng-tns-c1214074651-1 ng-star-inserted">
                                            <i _ngcontent-serverapp-c1214074651 class="fa ng-tns-c1214074651-1 fa-angle-double-right"></i>
                                            {{  $user->company_name }}
                                        </a><!---->
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!----><!---->
        </section>
        <!---->
        <section _ngcontent-serverapp-c1214074651 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom_container ng-tns-c1214074651-1 ng-star-inserted">
            <div _ngcontent-serverapp-c1214074651 class="container ng-tns-c1214074651-1">
                <div _ngcontent-serverapp-c1214074651 class="row ng-tns-c1214074651-1">
                    <div _ngcontent-serverapp-c1214074651 class="col-md-12 col-lg-9 ng-tns-c1214074651-1">
                        <div _ngcontent-serverapp-c1214074651 class="product-wrapper-grid list-view ng-tns-c1214074651-1">
                            <div _ngcontent-serverapp-c1214074651 class="row ng-tns-c1214074651-1">
                                <div _ngcontent-serverapp-c1214074651 class="col-12 col-12_padding_Set ng-tns-c1214074651-1">
                                    <div _ngcontent-serverapp-c1214074651 class="agency-box ng-tns-c1214074651-1">
                                        <div _ngcontent-serverapp-c1214074651 class="text-left row company_tabs_header_fix_size font-weight-normal ng-tns-c1214074651-1">
                                            <div _ngcontent-serverapp-c1214074651 class="col-grid-box col-12 ng-tns-c1214074651-1">
                                                <div _ngcontent-serverapp-c1214074651 class="front custom_front_rsume_image ng-tns-c1214074651-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                                        <picture _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">

                                                            <source _ngcontent-serverapp-c1214074651 width="93px" height="93px"
                                                                    media="(max-width:370px)" class="ng-tns-c1214074651-1"
                                                                    srcset="{{asset('storage/company_image').'/'.$user->info->company_image}}">
                                                            <source _ngcontent-serverapp-c1214074651 width="98px" height="98px"
                                                                    media="(max-width:768px)" class="ng-tns-c1214074651-1"
                                                                    srcset="{{asset('storage/company_image').'/'.$user->info->company_image}}">
                                                            <source _ngcontent-serverapp-c1214074651 width="133px" height="133px"
                                                                    media="(max-width:1024px)" class="ng-tns-c1214074651-1"
                                                                    srcset="{{asset('storage/company_image').'/'.$user->info->company_image}}">
                                                            <img _ngcontent-serverapp-c1214074651 width="148px" height="148px"
                                                                 class="ng-tns-c1214074651-1" src="{{asset('storage/company_image').'/'.$user->info->company_image}}">

                                                        </picture>

                                                    </div>
                                                </div>
                                                <!----><!---->
                                                <div _ngcontent-serverapp-c1214074651 class="Phone_center text-left mr-0 float-left rating-wrapper ng-tns-c1214074651-1 ng-star-inserted">
                                                    @for ($i = 1; $i <= 5; $i++)
                                                        @if ($i <= $max_rating)
                                                            <span class="fa fa-star"></span> <!-- Filled star -->
                                                        @else
                                                            <span class="fa fa-star-o"></span> <!-- Empty star -->
                                                        @endif
                                                    @endfor
                                                </div>
                                                <!---->
                                                <div _ngcontent-serverapp-c1214074651 class="agency_header w-10 ng-tns-c1214074651-1 ng-star-inserted"
                                                     style="background-image: url({{asset('storage/company_banner_image').'/'.$user->info->company_banner_image}});" >
                                                    <div _ngcontent-serverapp-c1214074651 class="agency_header_White_opcity ng-tns-c1214074651-1"></div>
                                                </div>
                                                <!----><!---->
                                                <div _ngcontent-serverapp-c1214074651 class="product-box ng-tns-c1214074651-1">
                                                    <div _ngcontent-serverapp-c1214074651 class="product-detail w-100 ng-tns-c1214074651-1 ng-star-inserted">
                                                        <h1 _ngcontent-serverapp-c1214074651 class="mb-0 listing-about-title p-0 ng-tns-c1214074651-1">
                                                            {{  $user->company_name }}
                                                        </h1>
                                                        @if($user->user_location)
                                                            <div _ngcontent-serverapp-c1214074651 class="text-left float-left active_icon_tab ng-tns-c1214074651-1 ng-star-inserted">
                                                                <i _ngcontent-serverapp-c1214074651 class="fa fa-map-marker ng-tns-c1214074651-1"></i>
                                                                <label _ngcontent-serverapp-c1214074651 class="ml-2 ng-tns-c1214074651-1">
                                                                    {{ $user->user_location->name }}
                                                                </label>
                                                            </div>
                                                        @endif
                                                        <!---->
                                                    </div>
                                                    <!----><!---->
                                                </div>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1214074651 class="agency_pricing_discount ng-tns-c1214074651-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1214074651 class="sub_agency_discount ng-tns-c1214074651-1">
                                                <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted">Accredited Agency</span><!---->
                                            </div>
                                            <div _ngcontent-serverapp-c1214074651 class="agency_discount_text ng-tns-c1214074651-1">
                                                <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted">Additional Services</span><!---->
                                            </div>
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1214074651 class="row ng-tns-c1214074651-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto mt-3 jOb_description description_tag ar ng-tns-c1214074651-1">
                                                <div _ngcontent-serverapp-c1214074651 style="word-break: break-word;" class="ng-tns-c1214074651-1">
                                                    {!! $user->info->agency_detail !!}
                                                </div>
                                            </div>
                                        </div>
                                        <!----><!---->
                                        @php
                                            @$agency_services = \App\Models\AgencyService::where('user_id',$user->id)
                                                ->orderBy('id','desc')
                                                ->get();
                                        @endphp
                                        @if($agency_services->count() !== 0)
                                            <div _ngcontent-serverapp-c1214074651 class="row custom-header-box ng-tns-c1214074651-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto ng-tns-c1214074651-1">
                                                    <h2 _ngcontent-serverapp-c1214074651 class="text-light ar ng-tns-c1214074651-1">Services</h2>
                                                </div>
                                            </div>

                                            <section _ngcontent-serverapp-c1214074651 class="app2 team py-4 mt-0 testimonial_bg ng-tns-c1214074651-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1214074651 class="row ml-0 mr-0 ng-tns-c1214074651-1">
                                                    <div _ngcontent-serverapp-c1214074651 class="col-12 ng-tns-c1214074651-1">
                                                        <div _ngcontent-serverapp-c1214074651 id="find-helper-carousel11" class="ng-tns-c1214074651-1 ng-star-inserted">
                                                            <ngb-carousel _ngcontent-serverapp-c1214074651 tabindex="0" class="carousel slide team-slider ng-tns-c1214074651-1" aria-activedescendant="slide-ngb-slide-60627" style="display: block;" ngskiphydration>
                                                                <div class="carousel-inner">
                                                                    <div role="tabpanel" class="carousel-item active ng-star-inserted" id="slide-ngb-slide-60627">



                                                                        <div _ngcontent-serverapp-c1214074651 class="blog-agency rounded mt-4 mb-5 ng-tns-c1214074651-1">
                                                                            @foreach($agency_services as $service)
                                                                                @if($service)
                                                                                    <div _ngcontent-serverapp-c1214074651 class="shivam ng-tns-c1214074651-1 ng-star-inserted">
                                                                                        <div _ngcontent-serverapp-c1214074651 class="team-container ar ng-tns-c1214074651-1">
                                                                                            <h3 _ngcontent-serverapp-c1214074651 class="name ng-tns-c1214074651-1">{{$service->name}}</h3>
                                                                                            <h4 _ngcontent-serverapp-c1214074651 class="post ng-tns-c1214074651-1">\
                                                                                                <i _ngcontent-serverapp-c1214074651 class="bill-with-dollar-sign-and-coins align-text-top mr-1 ng-tns-c1214074651-1"></i>
                                                                                                From HK$ {{ number_format($service->price,2,',') }}
                                                                                            </h4>
                                                                                            {{--                                                                                            <h4 _ngcontent-serverapp-c1214074651 class="post mb-1 ng-tns-c1214074651-1 ng-star-inserted"><i _ngcontent-serverapp-c1214074651 class="fa fa-calendar light align-text-top mr-1 ng-tns-c1214074651-1" style="font-size: 18px;"></i> Max Time: {{$service->no_of_weeks}} Weeks </h4>--}}
                                                                                            <!---->
                                                                                            <a _ngcontent-serverapp-c1214074651
                                                                                               data-url="{{ route('more_detail_service',[$service,'type' => 'partner']) }}"
                                                                                               class="px-2 py-1 float-right btn btn-md bg-dark rounded ng-tns-c1214074651-1 more_detail">
                                                                                                More Details
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                            @endforeach


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ngb-carousel>


                                                        </div>

                                                    </div>
                                                </div>
                                            </section>
                                        @endif

                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1214074651 class="row custom-header-box ng-tns-c1214074651-1">
                                            <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto ng-tns-c1214074651-1">
                                                <h2 _ngcontent-serverapp-c1214074651 class="text-light ar ng-tns-c1214074651-1">How We Can Help You </h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1214074651 class="row yoga mt-3 event ar ng-tns-c1214074651-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1214074651 class="col-11 phone_pedding border-bottom mx-auto ng-tns-c1214074651-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1214074651 class="event-container event-container_footer_margin_custom d-flex ng-tns-c1214074651-1">
                                                    <div _ngcontent-serverapp-c1214074651 class="yoga-circle ng-tns-c1214074651-1"><img _ngcontent-serverapp-c1214074651 src="https://cdn.helperplace.com/skill_cat/result.png" loading="lazy" alt="https://cdn.helperplace.com/skill_cat/result.png" width="100%" height="auto" class="ng-tns-c1214074651-1"></div>
                                                    <div _ngcontent-serverapp-c1214074651 class="event-info ng-tns-c1214074651-1">
                                                        <h3 _ngcontent-serverapp-c1214074651 class="primary ar ng-tns-c1214074651-1"> Our Strength: </h3>
                                                        @foreach(explode(',',$user->info->strength) as $strength_id)
                                                            @php
                                                                $strength  = \App\Models\AgencyStrength::where('id',$strength_id)->first();
                                                            @endphp
                                                            @if($strength)
                                                                <h4 _ngcontent-serverapp-c1214074651 class="float-left color_1 ng-tns-c1214074651-1 ng-star-inserted"> {{$strength->name}} </h4>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c1214074651 class="col-11 phone_pedding border-bottom mx-auto ng-tns-c1214074651-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1214074651 class="event-container event-container_footer_margin_custom d-flex ng-tns-c1214074651-1">
                                                    <div _ngcontent-serverapp-c1214074651 class="yoga-circle ng-tns-c1214074651-1"><img _ngcontent-serverapp-c1214074651 src="https://cdn.helperplace.com/skill_cat/1_1599642484.webp" loading="lazy" alt="https://cdn.helperplace.com/skill_cat/1_1599642484.webp" width="100%" height="auto" class="ng-tns-c1214074651-1"></div>
                                                    <div _ngcontent-serverapp-c1214074651 class="event-info ng-tns-c1214074651-1">
                                                        <h3 _ngcontent-serverapp-c1214074651 class="primary ar ng-tns-c1214074651-1"> We Speak: </h3>
                                                        @foreach(explode(',',$user->info->speak_language) as $lang_id)
                                                            @php
                                                                $lang  = \App\Models\Language::where('id',$lang_id)->first();
                                                            @endphp
                                                            @if($lang)
                                                                <h4 _ngcontent-serverapp-c1214074651 class="float-left color_2 ng-tns-c1214074651-1 ng-star-inserted"> {{$lang->name}}  </h4>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div _ngcontent-serverapp-c1214074651 class="row custom-header-box mt-2 ng-tns-c1214074651-1" id="#contactus">
                                            <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto ng-tns-c1214074651-1">
                                                <h2 _ngcontent-serverapp-c1214074651 class="text-light ar ng-tns-c1214074651-1">Contact Detail</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1214074651 class="row py-3 jOb-requirement ar ng-tns-c1214074651-1">
                                            <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto ng-tns-c1214074651-1">
                                                <div _ngcontent-serverapp-c1214074651 class="row ng-tns-c1214074651-1">
                                                    <div _ngcontent-serverapp-c1214074651 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1214074651-1">
                                                        <p _ngcontent-serverapp-c1214074651 class="address-main ng-tns-c1214074651-1">
                                                            <i _ngcontent-serverapp-c1214074651 class="fa fa-map-marker mt-1 mr-1 address-icon ng-tns-c1214074651-1"></i>
                                                            <span _ngcontent-serverapp-c1214074651 class="primary address-title contact-title ng-tns-c1214074651-1">Address:</span>
                                                            <span _ngcontent-serverapp-c1214074651 class="address-description ng-tns-c1214074651-1">
                                                              {{ $user->info->address }}
                                                            </span>
                                                        </p>
                                                        <p _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                                            <i _ngcontent-serverapp-c1214074651 class="fa fa-phone mt-1 mr-1 ng-tns-c1214074651-1"></i>
                                                            <span _ngcontent-serverapp-c1214074651 class="primary contact-title ng-tns-c1214074651-1">Phone: </span>
                                                            <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted">
                                                                {{ $user->mobile }}
                                                            </span><!----><!---->
                                                        </p>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1214074651 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1214074651-1">
                                                        <p _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                                            <i _ngcontent-serverapp-c1214074651 class="fa fa-clock-o mt-1 mr-1 ng-tns-c1214074651-1"></i>
                                                            <span _ngcontent-serverapp-c1214074651 class="primary contact-title ng-tns-c1214074651-1">Time: </span>
                                                            <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted">
                                                                {{ $user->info->day_off }}
                                                            </span><!----><!---->
                                                        </p>
                                                        <p _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                                            <i _ngcontent-serverapp-c1214074651 aria-hidden="true" class="fa fa-id-card mt-1 mr-1 ng-tns-c1214074651-1"></i>
                                                            <span _ngcontent-serverapp-c1214074651 class="primary contact-title ng-tns-c1214074651-1">Licence: </span>
                                                            <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted">
                                                                    {{ $user->info->license_no }}
                                                            </span><!----><!---->
                                                        </p>
                                                        <p _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                                            <i _ngcontent-serverapp-c1214074651 aria-hidden="true" class="fa fa-calendar-times-o mt-1 mr-1 ng-tns-c1214074651-1"></i>
                                                            <span _ngcontent-serverapp-c1214074651 class="primary contact-title ng-tns-c1214074651-1">Day Off: </span>
                                                            <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted">
                                                                 {{ $user->info->day_off }}
                                                            </span><!----><!---->
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                            @if(auth()->check() )
                                                @if(auth()->user()->role == \App\Models\User::ROLE_CANDIDATE || auth()->user()->role == \App\Models\User::ROLE_EMPLOYER )
                                                    <div _ngcontent-serverapp-c1214074651 class="row custom-header-box mt-2 ng-tns-c1214074651-1">
                                                        <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto ng-tns-c1214074651-1">
                                                            <h2 _ngcontent-serverapp-c1214074651 class="text-light ar ng-tns-c1214074651-1">Submit Your Review </h2>
                                                        </div>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1214074651 class="row py-3 jOb-requirement ar ng-tns-c1214074651-1">
                                                        <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto text-center ng-tns-c1214074651-1">
                                                            <form _ngcontent-serverapp-c1214074651 novalidate name="review-frm" method="post" action="{{ route('submit_agency_review') }}"
                                                                  style="height: 100%;"
                                                                  class="ng-tns-c1214074651-1 ng-untouched ng-pristine ng-valid">
                                                                @csrf
                                                                <input type="hidden" name="agency_id" value="{{$user->id}}">
                                                                <textarea _ngcontent-serverapp-c1214074651 cols="100" rows="3"
                                                                          name="review"
                                                                          class="form-control inblock rounded text_custom_set ng-tns-c1214074651-1 ng-untouched ng-pristine ng-valid"
                                                                          placeholder="Write Your review here" required></textarea>
                                                                <div _ngcontent-serverapp-c1214074651 class="float-left text-left mt-2 ng-tns-c1214074651-1">
                                                                    <div class="star-rating">
                                                                        <span class="fa fa-star-o" data-rating="1"></span>
                                                                        <span class="fa fa-star-o" data-rating="2"></span>
                                                                        <span class="fa fa-star-o" data-rating="3"></span>
                                                                        <span class="fa fa-star-o" data-rating="4"></span>
                                                                        <span class="fa fa-star-o" data-rating="5"></span>
                                                                        <input type="hidden" name="rating" class="rating-value" value="1">
                                                                    </div>
                                                                </div>
                                                                <button _ngcontent-serverapp-c1214074651 value="Submit review" class="bg-light btn custom_review_submit mt-1 rounded text-bold float-right ng-tns-c1214074651-1">
                                                                    <!----> Submit review
                                                                </button>
                                                                <!---->
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif

                                            <div _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1214074651 class="row bg-primary mt-2 ng-tns-c1214074651-1">
                                                    <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto ng-tns-c1214074651-1">
                                                        <h2 _ngcontent-serverapp-c1214074651 class="text-light ar ng-tns-c1214074651-1">User Review </h2>
                                                    </div>
                                                </div>
                                                <div _ngcontent-serverapp-c1214074651 class="clear-fix ng-tns-c1214074651-1">
                                                    @foreach($reviews as $review)
                                                        <div _ngcontent-serverapp-c1214074651 class="row py-3 jOb-requirement ar ng-tns-c1214074651-1 ng-star-inserted">
                                                            <div _ngcontent-serverapp-c1214074651 class="col-11 mx-auto text-left ng-tns-c1214074651-1">
                                                                <h4 _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
                                                                    <span _ngcontent-serverapp-c1214074651 class="review-username ng-tns-c1214074651-1">{{$review->user->name}} </span> -
                                                                    <span _ngcontent-serverapp-c1214074651 class="user_role ng-tns-c1214074651-1">
                                                                        @if($review->user->role == \App\Models\User::ROLE_EMPLOYER)
                                                                            <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted"> Employer </span><!----><!---->
                                                                        @else
                                                                            <span _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1 ng-star-inserted"> Helper </span><!----><!---->
                                                                        @endif
                                                            </span>
                                                                </h4>

                                                                @for ($i = 0; $i < $review->rating ; $i++)
                                                                    <span class="fa fa-star" ></span>
                                                                @endfor

                                                                <span _ngcontent-serverapp-c1214074651 class="ml-10 primary margin-left-reviewDate ng-tns-c1214074651-1">{{ \Illuminate\Support\Carbon::parse($review->created_at)->toDateString()}}</span>
                                                                <p _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1"> {{$review->review}} </p>
                                                            </div>
                                                        </div>

                                                    @endforeach
                                                </div>
                                                <pagination-controls _ngcontent-serverapp-c1214074651 maxsize="6" previouslabel nextlabel class="pagination-agencyView ng-tns-c1214074651-1 mb-3" ngh="3">
                                                    {{  $reviews->links() }}
                                                </pagination-controls>
                                            </div>
                                            <!---->
                                        </div>
                                    </div>
                                    <!----><span _ngcontent-serverapp-c1214074651 class="float-right primary cursor-pointer mr-2 ml-2 ng-tns-c1214074651-1"> Report this Agency </span><!---->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div _ngcontent-serverapp-c1214074651 class="col-md-12 col-lg-3 d-md-block d-lg-block d-xl-block collection-filter-block agency-right-block ng-tns-c1214074651-1">
                        @if($user->info && $user->info->company_map_iframe)
                            <div _ngcontent-serverapp-c1214074651 style="width: 100%;" class="ng-tns-c1214074651-1">
                                <div _ngcontent-serverapp-c1214074651 class="blog-agency ng-tns-c1214074651-1">
                                    <div _ngcontent-serverapp-c1214074651 class="blog-contain register-blog ng-tns-c1214074651-1">
                                        <div _ngcontent-serverapp-c1214074651 class="img-container ng-tns-c1214074651-1">
                                            {!! $user->info->company_map_iframe !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div _ngcontent-serverapp-c1214074651 style="width: 100%;" class="ng-tns-c1214074651-1 ng-star-inserted">
                            <div _ngcontent-serverapp-c1214074651 class="blog-agency ng-tns-c1214074651-1">
                                @if($latest_news)
                                    <div _ngcontent-serverapp-c1214074651 class="blog-contain register-blog ng-tns-c1214074651-1">
                                        <div _ngcontent-serverapp-c1214074651 class="Blog_list_img_size ng-tns-c1214074651-1 ng-star-inserted">
                                            <img _ngcontent-serverapp-c1214074651 loading="lazy" width="250px" height="200px"
                                                 class="img_center ng-tns-c1214074651-1"
                                                 src="https://cdn.helperplace.com/sidebar_adv_banner/24_1604658052.webp"
                                                 alt="What is an Ethical Employment Agency?">
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1214074651 class="img-container ng-tns-c1214074651-1">
                                            <div _ngcontent-serverapp-c1214074651 class="blog-info ng-tns-c1214074651-1 ng-star-inserted">
                                                <h4 _ngcontent-serverapp-c1214074651 class="blog-head ng-tns-c1214074651-1">
                                                    {{$latest_news->title}}
                                                </h4>
                                                <p _ngcontent-serverapp-c1214074651 class="card-text mb-2 ng-tns-c1214074651-1">
                                                    {{$latest_news->description}}
                                                </p>
                                                <a _ngcontent-serverapp-c1214074651 target="_blank"
                                                   class="btn bg-primary btn-rounded mr-2 btn-custom-hover btn-center-position ng-tns-c1214074651-1"
                                                   href="{{  route('news_details',$latest_news) }}">
                                                    More
                                                </a>
                                            </div>
                                            <!----><!---->
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
        </section>
        <!---->
        <section _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">
            <!---->
            <div _ngcontent-serverapp-c1214074651 id="footer" class="footer_custom mobile_sticky permenant-sticky ng-tns-c1214074651-1 ng-star-inserted">
                <div _ngcontent-serverapp-c1214074651 class="container ng-tns-c1214074651-1">
                    <div _ngcontent-serverapp-c1214074651 class="row ng-tns-c1214074651-1">
                        <div _ngcontent-serverapp-c1214074651 class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 res-footer footer-center ng-tns-c1214074651-1">
                            <div _ngcontent-serverapp-c1214074651 class="footer-apply-content ng-tns-c1214074651-1">
                                <div _ngcontent-serverapp-c1214074651 class="left ng-tns-c1214074651-1">
                                    <div _ngcontent-serverapp-c1214074651 class="left-wrapper ng-tns-c1214074651-1">
                                        @php

                                            $get_contact = App\Models\Setting::find(1);

                                        @endphp
                                        <span _ngcontent-serverapp-c1214074651 class="left-content-text ng-tns-c1214074651-1">Contact </span>
                                        <ul _ngcontent-serverapp-c1214074651 class="social-sharing ng-tns-c1214074651-1">
                                            <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1"><a _ngcontent-serverapp-c1214074651 href="mailto:{{ $user->email }}"  class="ng-tns-c1214074651-1"><i _ngcontent-serverapp-c1214074651 class="fa fa-commenting ng-tns-c1214074651-1"></i></a></li>
                                            <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1"><a _ngcontent-serverapp-c1214074651 href="https://wa.me/{{ $user->mobile }}"    class="ng-tns-c1214074651-1"><i _ngcontent-serverapp-c1214074651 class="fa fa-whatsapp ng-tns-c1214074651-1"></i></a></li>
                                            <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1"><a _ngcontent-serverapp-c1214074651 aria-label="agency phone" class="ng-tns-c1214074651-1" href="tel:{{$get_contact->whatsapp_number}}"><i _ngcontent-serverapp-c1214074651 class="fa fa-phone ng-tns-c1214074651-1"></i></a></li>
                                            {{--                                            <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1">--}}
                                            {{--                                                <a _ngcontent-serverapp-c1214074651 href="javascript:" id="#contactus"--}}
                                            {{--                                                   aria-label="Helperplace marker" class="ng-tns-c1214074651-1">--}}
                                            {{--                                                    <i _ngcontent-serverapp-c1214074651 class="fa fa-map-marker ng-tns-c1214074651-1"></i></a>--}}

                                            {{--                                            </li>--}}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div _ngcontent-serverapp-c1214074651 class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 res-footer footer-remove ng-tns-c1214074651-1">
                            <div _ngcontent-serverapp-c1214074651 class="footer-apply-content right-content ng-tns-c1214074651-1">
                                <div _ngcontent-serverapp-c1214074651 class="right ng-tns-c1214074651-1">
                                    <div _ngcontent-serverapp-c1214074651 class="right-wrapper ng-tns-c1214074651-1">
                                        <span _ngcontent-serverapp-c1214074651 class="left-content-text ng-tns-c1214074651-1">Share</span>
                                        <ul _ngcontent-serverapp-c1214074651 class="social-sharing ng-tns-c1214074651-1">
                                            <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1"><a _ngcontent-serverapp-c1214074651 href="javascript:;" class="ng-tns-c1214074651-1"><i _ngcontent-serverapp-c1214074651 aria-hidden="true" class="fa fa-facebook ng-tns-c1214074651-1"></i></a></li>
                                            <li _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1"><a _ngcontent-serverapp-c1214074651 href="javascript:;" class="ng-tns-c1214074651-1"><i _ngcontent-serverapp-c1214074651 aria-hidden="true" class="fa fa-whatsapp ng-tns-c1214074651-1"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </section>
        <app-agency-contact _ngcontent-serverapp-c1214074651 class="ng-tns-c1214074651-1"></app-agency-contact>

        <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered " role="document">
                <div class="modal-content ">
                    <div class="modal-header">
                        <h3 id="agency_name"> {{  $user->company_name }}</h3>

                        <button type="button" class="close close_model" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="service_detail">

                    </div>
                </div>
            </div>
        </div>
        <!----><!----><!----><!---->
    </section>


@endsection
@push('js')
    <script>
        var $star_rating = $('.star-rating .fa');

        var SetRatingStar = function() {
            return $star_rating.each(function() {
                if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
                    return $(this).removeClass('fa-star-o').addClass('fa-star');
                } else {
                    return $(this).removeClass('fa-star').addClass('fa-star-o');
                }
            });
        };

        $star_rating.on('click', function() {
            $star_rating.siblings('input.rating-value').val($(this).data('rating'));
            return SetRatingStar();
        });

        SetRatingStar();

        $(document).on("click", ".more_detail", function () {
            const   url = $(this).data('url');

            $.ajax({
                url: url,
                type: "get",
                success: function (data) {
                    const response_data = data.response_data;
                    $('#service_detail').html('');
                    $('#service_detail').html(response_data);
                    $('#detailModal').modal('show');
                }
            });


        });
        $(document).on("click", ".close_model", function () {
            $('#detailModal').modal('hide');
        });

    </script>
@endpush
