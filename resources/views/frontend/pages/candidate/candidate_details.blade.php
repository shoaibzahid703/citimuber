@extends('frontend.layouts.app')
@section('title','CANDIDATE DETAILS')
@section('css')
    <style>
        .job-view-ar{
            margin-top: 97px;
        }
        .custom-tabs .nav-link {
            background-color: #f8f9fa;
            color: #555;
            border-radius: 0;
            font-weight: bold;
            padding: 15px 20px;
            margin-right: 10px;
            transition: all 0.3s ease;
            border: none;
        }

        .custom-tabs .nav-link.active {
            background-color: #B50000;
            color: white;
            border: none;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        .custom-tabs .nav-link:hover {
            background-color: #0056b3;
            color: white;
        }

        .modal-header img {
            max-width: 150px;
            height: auto;
        }

        .modal-body {
            padding: 20px 30px;
        }

        .tab-content {
            border-top: 2px solid #007bff;
            padding-top: 20px;
        }

        @media only screen and (max-width: 362px)  {
            .product-wrapper-grid.list-view[_ngcontent-serverApp-c1181781737] .product-box[_ngcontent-serverApp-c1181781737] .product-detail[_ngcontent-serverApp-c1181781737] {
                padding: unset !important;
            }
        }
    </style>
    <style>
        /* Lightbox Modal */
        .lightbox {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1000; /* Sit on top */
            top: 0;
            left: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            background-color: rgba(0, 0, 0, 0.8); /* Black background with opacity */
            align-items: center; /* Center vertically */
            justify-content: center; /* Center horizontally */
        }

        /* Lightbox Content */
        .lightbox-content {
            max-height: 60vh; /* Max height of half the viewport */
            width: 450px; /* Maintain aspect ratio */
            display: block;
            margin: auto;
            border-radius: 10px;
        }

        /* Close Button */
        .close {
            position: absolute;
            top: 20px;
            right: 20px;
            color: white;
            font-size: 40px;
            font-weight: bold;
            cursor: pointer;
            z-index: 1001; /* Ensure the button is above other content */
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

    </style>
@endsection
@section('content')


    <section _ngcontent-serverapp-c1181781737="" class="page-section resume-view-ar ng-tns-c1181781737-1">
        <section _ngcontent-serverapp-c1181781737="" class="agency breadcrumb-section-main breadcrumb-title inner-3 ng-tns-c1181781737-1 ng-star-inserted" style="background: #f8f8f8 !important;">
            <div _ngcontent-serverapp-c1181781737="" class="container px-2 py-4 ng-tns-c1181781737-1 ng-star-inserted">
                <div _ngcontent-serverapp-c1181781737="" class="row px-3 ng-tns-c1181781737-1">
                    <div _ngcontent-serverapp-c1181781737="" class="col-12 ng-tns-c1181781737-1">
                        <div _ngcontent-serverapp-c1181781737="" class="breadcrumb-contain ng-tns-c1181781737-1">
                            <div _ngcontent-serverapp-c1181781737="" class="text-left ng-tns-c1181781737-1">
                                <ul _ngcontent-serverapp-c1181781737="" class="text-left header-icon ng-tns-c1181781737-1">
                                    <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                        <a _ngcontent-serverapp-c1181781737="" routerlinkactive="router-link-active" class="ng-tns-c1181781737-1 router-link-active" href="/">
                                            <app-icons _ngcontent-serverapp-c1181781737="" name="home" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                    <use _ngcontent-serverApp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#home"></use>
                                                </svg>
                                            </app-icons>
                                        </a>
                                    </li>
                                    <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                        <a _ngcontent-serverapp-c1181781737="" routerlinkactive="router-link-active" class="ng-tns-c1181781737-1" href="/candidates">
                                            <app-icons _ngcontent-serverapp-c1181781737="" name="double-arrow-right" _nghost-serverapp-c4219164779="" class="icons ng-star-inserted" ngh="0">
                                                <i class="fa fa-home" aria-hidden="true" style="color: #1E3F66"></i>
                                                <i class="fa fa-angle-double-right" aria-hidden="true" style="color: #1E3F66"></i>
                                            </app-icons>
                                            Find Candidate
                                        </a>
                                    </li>
                                    <!-- <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                        <a _ngcontent-serverapp-c1181781737="" href="javascript:;" class="ng-tns-c1181781737-1 ng-star-inserted">
                                            <app-icons _ngcontent-serverapp-c1181781737="" name="double-arrow-right" _nghost-serverapp-c4219164779="" class="icons ng-star-inserted" ngh="0">
                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                    <use _ngcontent-serverApp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#double-arrow-right"></use>
                                                </svg>
                                            </app-icons>
                                             Hong Kong
                                        </a>

                                    </li>
                                    <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                        <a _ngcontent-serverapp-c1181781737="" href="javascript:;" class="ng-tns-c1181781737-1 ng-star-inserted">
                                            <app-icons _ngcontent-serverapp-c1181781737="" name="double-arrow-right" _nghost-serverapp-c4219164779="" class="icons ng-star-inserted" ngh="0">
                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                    <use _ngcontent-serverApp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#double-arrow-right"></use>
                                                </svg>
                                            </app-icons>
                                             Domestic Helper
                                        </a>

                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!----><!---->
        </section>


        <!----><!---->
        <section _ngcontent-serverapp-c1181781737="" class="agency blog blog-sec blog-sidebar pb-5 blog-list sider ng-tns-c1181781737-1 ng-star-inserted">
            <div _ngcontent-serverapp-c1181781737="" class="container ng-tns-c1181781737-1">
                <!---->
                <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1 ng-star-inserted">
                    <div _ngcontent-serverapp-c1181781737="" class="col-md-12 col-lg-9 ng-tns-c1181781737-1">
                        <div _ngcontent-serverapp-c1181781737="" class="product-wrapper-grid list-view ng-tns-c1181781737-1">
                            <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                                <div _ngcontent-serverapp-c1181781737="" class="col-12 col-12_padding_Set ng-tns-c1181781737-1">
                                    <div _ngcontent-serverapp-c1181781737="" class="agency-box ng-tns-c1181781737-1">
                                        <div _ngcontent-serverapp-c1181781737="" class="text-left row company_tabs_header_fix_size font-weight-normal ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-grid-box col-12 ng-tns-c1181781737-1">
                                                <div _ngcontent-serverapp-c1181781737="" class="front custom_front_rsume_image profile-preview ng-tns-c1181781737-1 ng-star-inserted">
                                                    <app-img-preview class="">
                                                        <img id="myImg" style="width: 100%; max-width: 300px;" src="{{ asset('storage/profile_image/' . $candidate->profile_image) }}" onerror="this.src='{{asset('assets/images/no-imag-found.png')}}';" alt="{{$candidate->name}}" class="img-thumbnail">
                                                    </app-img-preview>

                                                    <!-- Custom Lightbox Modal -->
                                                    <div id="customLightbox" class="lightbox">
                                                        <span class="close">&times;</span>
                                                        <img class="lightbox-content" id="lightboxImage">
                                                    </div>



                                                </div>
                                                <!----><span _ngcontent-serverapp-c1181781737="" id="very-active" class="ng-tns-c1181781737-1"></span><!---->
                                                <div _ngcontent-serverapp-c1181781737="" class="listing-sub-title-agency text-left mt-2 mb-2 mr-3 float-left ng-tns-c1181781737-1">
                                                    <label _ngcontent-serverapp-c1181781737="" class="label_blue ng-tns-c1181781737-1 ng-star-inserted">

                                                        @if(is_null($candidate->added_by))
                                                            <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted"> Direct </span>
                                                        @else
                                                            <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted"> Agency </span>
                                                        @endif
                                                    </label>
                                                    <!----><!---->
                                                </div>
                                                <!---->
                                                <h3 _ngcontent-serverapp-c1181781737="" class="top-active text-left ng-tns-c1181781737-1 ng-star-inserted"> Very Active </h3>
                                                <!---->
                                                <div _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                    @if($candidate_page->candidate_detail_page_image)
                                                        <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737="" media="(max-width:576px)" class="ng-tns-c1181781737-1" srcset="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->candidate_detail_page_image}}">
                                                            <source _ngcontent-serverapp-c1181781737="" media="(max-width:768px)" class="ng-tns-c1181781737-1" srcset="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->candidate_detail_page_image}}">
                                                            <source _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1" srcset="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->candidate_detail_page_image}}">

                                                            <img _ngcontent-serverapp-c1181781737="" fetchpriority="high" loading="eager" rel="preload" onerror="this.onerror = null;this.parentNode.children[0].srcset =
                                                         this.parentNode.children[1].srcset =this.parentNode.children[2].srcset=this.src;" class="agency_header w-10 responsive-img ng-tns-c1181781737-1" id="imgDazel Joy" alt="Dazel Joy" src="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->candidate_detail_page_image}}">
                                                        </picture>
                                                    @else
                                                        <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737="" media="(max-width:576px)" class="ng-tns-c1181781737-1" srcset="{{asset('cdn-sub/front-app/assets/images/misc/resume-lg.webp')}}">
                                                            <source _ngcontent-serverapp-c1181781737="" media="(max-width:768px)" class="ng-tns-c1181781737-1" srcset="{{asset('cdn-sub/front-app/assets/images/misc/resume-lg.webp')}}">
                                                            <source _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1" srcset="{{asset('cdn-sub/front-app/assets/images/misc/resume-lg.webp')}}">

                                                            <img _ngcontent-serverapp-c1181781737="" fetchpriority="high" loading="eager" rel="preload" onerror="this.onerror = null;this.parentNode.children[0].srcset =
                                                         this.parentNode.children[1].srcset =this.parentNode.children[2].srcset=this.src;" class="agency_header w-10 responsive-img ng-tns-c1181781737-1" id="imgDazel Joy" alt="Dazel Joy" src="{{asset('cdn-sub/front-app/assets/images/misc/resume-lg.webp')}}">
                                                        </picture>
                                                    @endif

                                                    <div _ngcontent-serverapp-c1181781737="" class="agency_header_White_opcity ng-tns-c1181781737-1"></div>
                                                </div>
                                                <div _ngcontent-serverapp-c1181781737="" class="product-box ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="product-detail w-100 ng-tns-c1181781737-1 ng-star-inserted">
                                                        <div _ngcontent-serverapp-c1181781737="" class="user-detail ng-tns-c1181781737-1">
                                                            <div _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                <h1 _ngcontent-serverapp-c1181781737="" class="mb-0 listing-about-title p-0 ng-tns-c1181781737-1"> {{$candidate->name}} </h1>
                                                            </div>
                                                            <div _ngcontent-serverapp-c1181781737="" class="mt-2 ng-tns-c1181781737-1">
                                                                <h2 _ngcontent-serverapp-c1181781737="" class="resume-age ng-tns-c1181781737-1"> ({{$candidate->resume_detail->age ??''}} Years) </h2>
                                                            </div>
                                                        </div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="hp-candidate-wrapper ng-tns-c1181781737-1">
                                                            <div _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                <p _ngcontent-serverapp-c1181781737="" class="mb-2 p-0 text-left listing-about-sub-title ng-tns-c1181781737-1">
                                                                    <!----><span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted"> {{ucfirst($candidate->resume_detail->gender ??'')}}</span><!----><span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted"> |</span><!----> {{ucfirst($candidate->resume_detail->marital_status ??'')}} | <!----><!----><span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted">{{$candidate->resume_detail->kids_detail ??''}}  Kids</span><!----> | {{$candidate->resume_detail->user_nationality->nationality ??''}} | {{$candidate->resume_detail->religion ??''}}
                                                                </p>
                                                            </div>
                                                            <div _ngcontent-serverapp-c1181781737="" class="apply-btn ng-tns-c1181781737-1">
                                                                <div _ngcontent-serverapp-c1181781737="" role="group" class="btn-group ng-tns-c1181781737-1 ng-star-inserted">
                                                                    @if(Auth::check())
                                                                        @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER || \App\Models\User::ROLE_AGENCY)
                                                                            <button _ngcontent-serverapp-c1181781737="" type="button"
                                                                                    title="Contact Candidate" class="btn hp-apply-btn contact ng-tns-c1181781737-1 message_btn d-none d-sm-none d-md-block d-lg-block"
                                                                                    data-toggle="modal"
                                                                                    data-target="#messageModal"
                                                                                    style="min-width: 91px;">
                                                                                Contact
                                                                            </button>
                                                                        @endif
                                                                    @else
                                                                        <button _ngcontent-serverapp-c1181781737="" type="button"
                                                                                title="Contact Candidate"
                                                                                class="btn hp-apply-btn contact ng-tns-c1181781737-1 d-none d-sm-none d-md-block d-lg-block"
                                                                                data-toggle="modal"
                                                                                data-target="#loginModal" style="min-width: 91px;">
                                                                            Contact
                                                                        </button>
                                                                    @endif
                                                                </div>
                                                                <!----><!----><!---->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!----><!---->
                                                </div>
                                            </div>
                                        </div>


                                        <!-- // contact modal -->

                                        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <img src="{{ asset('assets/images/site-logo.jpg') }}" class="w-50">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @if(Auth::check())
                                                            @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER || \App\Models\User::ROLE_AGENCY)
                                                                @if($candidate->is_contact)
                                                                    <!-- Tab Navigation -->
                                                                    <ul class="nav nav-tabs custom-tabs" id="myTab" role="tablist">
                                                                        <li class="nav-item" style="width: 50%;">
                                                                            <a class="nav-link active" id="message-tab" data-toggle="tab" href="#messageContent" role="tab" aria-controls="messageContent" aria-selected="true" style="border-radius: 20px 0px 0px 20px;"><i class="fa fa-commenting" aria-hidden="true"></i> Message</a>
                                                                        </li>
                                                                        <li class="nav-item" style="width: 50%;">
                                                                            <a class="nav-link" id="call-tab" data-toggle="tab" href="#callContent" role="tab" aria-controls="callContent" aria-selected="false" style="border-radius: 0px 20px 20px 0px;"><i class="fa fa-phone" aria-hidden="true"></i> Call/Whatsapp</a>
                                                                        </li>
                                                                    </ul>
                                                                    <!-- Tab Content -->
                                                                    <div class="tab-content mt-3">
                                                                        <!-- Message Tab Content -->
                                                                        <div class="tab-pane fade show active" id="messageContent" role="tabpanel" aria-labelledby="message-tab">
                                                                            <div class="row mt-3 pt-3 pb-3">
                                                                                <div class="col-12">
                                                                                    <div id="loginAlert" class="alert alert-danger d-none"></div>
                                                                                    <form class="helper-form mt-3" method="post" action="{{ route('send_helper_message',$candidate) }}">
                                                                                        @csrf


                                                                                        <div class="form-group">
                                                                                            <label for="message" style="color:black !important;">Send Message to {{$candidate->name}}</label>
                                                                                            <textarea name="message" required class="form-control" id="message" aria-describedby="emailHelp" autocomplete="off" placeholder="message"></textarea>
                                                                                            <div id="message-error" class="invalid-feedback"></div>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: #000; color:white;">Close</button>
                                                                                            <button type="submit" class="btn btn-primary" style="background-color: #B50000; color:white;">Start Chat</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Call Tab Content -->
                                                                        <div class="tab-pane fade" id="callContent" role="tabpanel" aria-labelledby="call-tab">
                                                                            <div class="row mt-3 pt-3 pb-3">

                                                                                @if(!is_null($candidate->resume_detail->whatsapp_number))
                                                                                    <div class="col-12 d-flex justify-content-center">
                                                                                        <a class="btn login_btn" href="https://wa.me/{{ $candidate->resume_detail->whatsapp_number }}" target="_blank">Chat on WhatsApp</a>
                                                                                    </div>
                                                                                @endif

                                                                                {{--                                                                    @if(is_null($candidate->mobile) &&  is_null($candidate->resume_detail->whatsapp_number))--}}
                                                                                {{--                                                                        <div class="col-12 alert alert-danger">--}}
                                                                                {{--                                                                            You have not any authority to get contact detail.--}}
                                                                                {{--                                                                        </div>--}}
                                                                                {{--                                                                    @endif--}}

                                                                                @if(is_null($candidate->resume_detail->whatsapp_number))
                                                                                    <div class="col-12">
                                                                                        <p>Whatsapp number is not available at the moment.</p>
                                                                                    </div>
                                                                                @endif
                                                                                @if(is_null($candidate->mobile))
                                                                                    <div class="col-12">
                                                                                        <p>Mobile number is not available at the moment.</p>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="row">
                                                                        <div class="col-12 alert alert-danger text-center">
                                                                            Please Contact Admin to contact with <b>{{$candidate->name}}</b> .
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">Professional Info </h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1181781737="" class="row py-3 personal-infomation ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ar ng-tns-c1181781737-1">
                                                <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="user" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#user"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-user" style="color: #1E3F66"></i> &nbsp;
                                                            {{$candidate->resume_detail->position_apply ??''}} | {{$candidate->resume_detail->work_status ??''}}
                                                        </h3>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="map" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#map"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-map-marker" aria-hidden="true" style="color: #1E3F66"></i> &nbsp;
                                                            Present Location: {{$candidate->resume_detail->present_location->name ??''}}
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="certificate" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#certificate"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-cog" aria-hidden="true" style="color: #1E3F66"></i> &nbsp;
                                                            {{$candidate->resume_detail->work_experience ??''}} Years Experience
                                                        </h3>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="calendar" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="15" width="15" href="https://www.helperplace.com/assets/icons/custom.svg#calendar"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-calendar" aria-hidden="true" style="color: #1E3F66"></i>&nbsp;
                                                            Start from {{ \Carbon\Carbon::parse($candidate->resume_detail->job_start_date ?? '')->format('d F Y') }} | {{$candidate->resume_detail->job_type ??''}}
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">Skills / Duties</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1181781737="" class="row yoga mt-3 event ng-tns-c1181781737-1 ng-star-inserted">
                                            @php
                                                $user_detail = App\Models\BioData::where('user_id', $candidate->id)->pluck('languages');
                                                $data_exploade = explode(',', $user_detail[0]);
                                                $languages = App\Models\Language::whereIn('id', $data_exploade)->get();
                                            @endphp
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/1_1599642484.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Language: </h3>
                                                            @foreach($languages as $language)
                                                                <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_1 ng-star-inserted"> {{$language->name}}</h4>
                                                            @endforeach

                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>


                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 ar ng-tns-c2001901274-1 ng-star-inserted">
                                                @php
                                                    $user_detail = App\Models\BioData::where('user_id', $candidate->id)->pluck('main_skills');
                                                    $data_exploade = explode(',', $user_detail[0]);
                                                    $main_skills = App\Models\MainSkill::whereIn('id', $data_exploade)->get();
                                                @endphp
                                                <div _ngcontent-serverapp-c2001901274="" class="col-11 phone_pedding mx-auto ng-tns-c2001901274-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c2001901274="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c2001901274-1" style="margin-bottom: 0px !important;">
                                                        <div _ngcontent-serverapp-c2001901274="" class="yoga-circle ng-tns-c2001901274-1">
                                                            <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <source _ngcontent-serverapp-c2001901274="" width="35px" height="35px" media="(max-width:992px)" class="ng-tns-c2001901274-1" srcset="{{asset('cdn-sub/skill_cat/4_1599643968.webp')}}">
                                                                <img _ngcontent-serverapp-c2001901274="" width="40px" height="40px" class="ng-tns-c2001901274-1" src="{{asset('cdn-sub/skill_cat/4_1599643968.webp')}}" alt="">
                                                            </picture>
                                                        </div>
                                                        <div _ngcontent-serverapp-c2001901274="" class="event-info ng-tns-c2001901274-1">
                                                            <h3 _ngcontent-serverapp-c2001901274="" class="primary ar ng-tns-c2001901274-1"> Main Skills: </h3>
                                                            @foreach($main_skills as $main_skill)
                                                                <h4 _ngcontent-serverapp-c2001901274="" class="float-left float-left-ar ng-tns-c2001901274-1 color_2 ng-star-inserted"> {{$main_skill->name}} </h4>
                                                            @endforeach
                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>


                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                @php
                                                    $user_detail = App\Models\BioData::where('user_id', $candidate->id)->pluck('cooking_skills');
                                                    $data_exploade = explode(',', $user_detail[0]);
                                                    $cooking_skills = App\Models\CookingSkill::whereIn('id', $data_exploade)->get();
                                                @endphp
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/2_1599644151.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Cooking Skills: </h3>
                                                            @foreach($cooking_skills as $cooking_skill)
                                                                <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_3 ng-star-inserted"> {{$cooking_skill->name}} </h4>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                @php
                                                    $user_detail = App\Models\BioData::where('user_id', $candidate->id)->pluck('other_skills');
                                                    $data_exploade = explode(',', $user_detail[0]);
                                                    $other_skills = App\Models\OtherSkill::whereIn('id', $data_exploade)->get();
                                                @endphp
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/3_1599642528.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Other Skills: </h3>
                                                            @foreach($other_skills as $other_skill)
                                                                <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_4 ng-star-inserted"> {{$other_skill->name}} </h4>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                @php
                                                    $user_detail = App\Models\BioData::where('user_id', $candidate->id)->pluck('personalities');
                                                    $data_exploade = explode(',', $user_detail[0]);
                                                    $personality = App\Models\Personality::whereIn('id', $data_exploade)->get();
                                                @endphp
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/5_1599644127.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Personality: </h3>
                                                            @foreach($personality as $personalities)
                                                                <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_5 ng-star-inserted"> {{$personalities->name}} </h4>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <!---->
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">About Me</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto mt-3 jOb_description description_tag ar ng-tns-c1181781737-1">
                                                <div _ngcontent-serverapp-c1181781737="" style="word-break: break-word;" class="ng-tns-c1181781737-1">
                                                    <p>{!! $candidate->resume_detail->resume_description ??'' !!}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">Work Experience</h2>
                                            </div>
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c1181781737 class="row yoga event mt-3 ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737 class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737 class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737 class="yoga-circle ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737 class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737 srcset="{{asset('cdn-sub/web-asset/images/icon/prize-badge-with-star-and-ribbon-black.png')}}" width="35px" height="35px" media="(max-width:768px)" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737 src="{{asset('cdn-sub/web-asset/images/icon/prize-badge-with-star-and-ribbon-black.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737 class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737 class="primary text-left mb-0 ng-tns-c1181781737-1">
                                                            {{ \Carbon\Carbon::parse($candidate->user_work_experience->start_date ?? '')->format('d F Y') }} -
                                                            <span _ngcontent-serverapp-c1181781737 class="ng-tns-c1181781737-1 ng-star-inserted"> {{ \Carbon\Carbon::parse($candidate->user_work_experience->job_end_date ?? '')->format('d F Y') }}</span><!---->
                                                        </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737 class="mb-0 ng-tns-c1181781737-1">
                                                            {{$candidate->resume_detail->position_apply ??''}}
                                                            <span _ngcontent-serverapp-c1181781737 class="location-span ng-tns-c1181781737-1 ng-star-inserted">
                                                            <app-icons _ngcontent-serverapp-c1181781737 name="map" class="icons" _nghost-serverapp-c4219164779 ngh="0">
                                                               <svg _ngcontent-serverApp-c4219164779 class="icons" height="17" width="17">
                                                                  <use _ngcontent-serverApp-c4219164779 height="13" width="13" href="https://www.helperplace.com/assets/icons/custom.svg#map"></use>
                                                               </svg>
                                                            </app-icons>
                                                            <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                <i class="fa fa-map-marker" aria-hidden="true" style="color: #1E3F66"></i> &nbsp;
                                                                {{$candidate->resume_detail->present_location->name ??''}}</span>
                                                         </span>
                                                        </h3>
                                                        <p _ngcontent-serverapp-c1181781737="" class="text-left mb-1 ng-tns-c1181781737-1"> {{$candidate->resume_detail->user_nationality->nationality ??''}} {{$candidate->user_work_experience->employer_type ?? ''}} </p>

                                                        @php

                                                            $duties = App\Models\UserWorkExperience::where('user_id', $candidate->id)->pluck('job_duties');
                                                            $data_exploade = explode(',', $duties[0]);
                                                            $job_duties = App\Models\Duty::whereIn('id', $data_exploade)->get();

                                                        @endphp
                                                        <p _ngcontent-serverapp-c1181781737 class="text-left mb-1 ng-tns-c1181781737-1 ng-star-inserted"> Duties: @foreach($job_duties as $job_duty) {{$job_duty->name}} @if(!$loop->last), @endif @endforeach </p>
                                                        <p _ngcontent-serverapp-c1181781737 class="text-left mb-1 ng-tns-c1181781737-1 ng-star-inserted"> I don't have a reference letter </p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div _ngcontent-serverapp-c1181781737 class="row custom-header-box ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737 class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737 class="text-light ar ng-tns-c1181781737-1">Education</h2>
                                            </div>
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c1181781737 class="row yoga event mt-3 ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737 class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737 class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737 class="yoga-circle mb-0 ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737 class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737 srcset="{{asset('cdn-sub/web-asset/images/icon/diploma.png')}}" width="35px" height="35px" media="(max-width:768px)" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737 src="{{asset('cdn-sub/web-asset/images/icon/diploma.png')}}" loading="lazy" alt="" width="40px" height="40px" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737 class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737 class="primary text-left mb-0 ng-tns-c1181781737-1"> {{$candidate->user_education->year_completion ??''}} - {{$candidate->user_education->education ??''}} </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737 class="mb-0 ar ng-tns-c1181781737-1 ng-star-inserted"> {{ ucfirst(str_replace('_', ' ', $candidate->user_education->course_duration ?? '')) }} </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <!---->
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c1181781737 class="row custom-header-box ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737 class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737 class="text-light ar ng-tns-c1181781737-1">My Expectations</h2>
                                            </div>
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c1181781737 class="row yoga event mt-3 ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737 class="col-11 phone_pedding mx-auto border-bottom ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737 class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737 class="yoga-circle ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737 class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737 srcset="{{asset('cdn-sub/web-asset/images/icon/give-money.png')}}" width="35px" height="35px" media="(max-width:768px)" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737 src="{{asset('cdn-sub/web-asset/images/icon/give-money.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737 class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737 class="primary text-left mb-0 ng-tns-c1181781737-1"> Salary: </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737 class="mb-0 ar ng-tns-c1181781737-1">
                                                            {{$candidate->resume_detail->salary_currency->name ??''}}{{$candidate->resume_detail->salary_currency->sign ??''}} {{$candidate->resume_detail->monthly_salary ??''}} <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted">(≈ US$ 703)</span><!---->
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>

                                            <div _ngcontent-serverapp-c1181781737 class="col-11 phone_pedding mx-auto border-bottom ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737 class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737 class="yoga-circle ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737 class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737 srcset="{{asset('cdn-sub/web-asset/images/icon/bed.png')}}" width="35px" height="35px" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737 src="{{asset('cdn-sub/web-asset/images/icon/bed.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737 class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737 class="primary text-left mb-0 ng-tns-c1181781737-1"> Accommodation: </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737 class="mb-0 ar ng-tns-c1181781737-1"> {{$candidate->resume_detail->accomodation_preference ??''}} </h3>
                                                    </div>
                                                </div>
                                            </div>

                                            <div _ngcontent-serverapp-c1181781737 class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737 class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737 class="yoga-circle ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737 class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737 srcset="{{asset('cdn-sub/web-asset/images/icon/home-rest-day-calendar-page.png')}}" width="35px" height="35px" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737 src="{{asset('cdn-sub/web-asset/images/icon/home-rest-day-calendar-page.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737 class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737 class="primary text-left mb-0 ng-tns-c1181781737-1"> Day Off: </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737 class="mb-0 ar ng-tns-c1181781737-1">  {{ ucfirst(str_replace('_', ' ', $candidate->resume_detail->day_off_preference ?? '')) }}</h3>


                                                    </div>
                                                </div>
                                            </div>
                                            <!---->
                                        </div>
                                    </div>
                                    <span _ngcontent-serverapp-c1181781737 class="float-right float-left-ar primary pointer mr-2 ml-2 ng-tns-c1181781737-1"> Report this Candidate </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div _ngcontent-serverapp-c1181781737 class="col-md-12 col-lg-3 d-md-clock d-lg-block d-xl-block collection-filter-block resume-right-block ng-tns-c1181781737-1">
                        <!---->
                        <div _ngcontent-serverapp-c1181781737 class="ng-tns-c1181781737-1 ng-star-inserted">
                            @foreach($related_candidates as $related_candidate)
                                <div _ngcontent-serverapp-c1181781737 style="width: 100%;" class="ng-tns-c1181781737-1 ng-star-inserted">
                                    <div _ngcontent-serverapp-c1181781737 class="blog-agency ng-tns-c1181781737-1">
                                        <div _ngcontent-serverapp-c1181781737 class="blog-contain ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737 class="img-container ng-tns-c1181781737-1">
                                                <a _ngcontent-serverapp-c1181781737  class="blog-info-resume ng-tns-c1181781737-1" href="{{route('candidate_details',$related_candidate->user)}}">
                                                    <img _ngcontent-serverapp-c1181781737 loading="lazy" width="70px" height="70px" class="related-profile-picture ng-tns-c1181781737-1" src="{{ asset('storage/profile_image/' . $related_candidate->user->profile_image) }}" onerror="this.src='{{asset('assets/images/no-imag-found.png')}}';" alt="{{$related_candidate->middle_name}}">
                                                    <div _ngcontent-serverapp-c1181781737 class="ar ng-tns-c1181781737-1">
                                                        <h4 _ngcontent-serverapp-c1181781737 class="blog-head fixed-1-line ng-tns-c1181781737-1">{{$related_candidate->user->name}}</h4>
                                                        <p _ngcontent-serverapp-c1181781737 class="card-text ng-tns-c1181781737-1"><b _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1"><span _ngcontent-serverapp-c1181781737="" class="related-employer-type ng-tns-c1181781737-1">{{$related_candidate->position_apply}}</span></b></p>
                                                        <p _ngcontent-serverapp-c1181781737 class="card-text ng-tns-c1181781737-1 ng-star-inserted" style="padding-left: 2px;">
                                                            <i class="fa fa-map-marker" aria-hidden="true" style="color: #1E3F66"></i>
                                                            {{$related_candidate->user_nationality->nationality ??''}}
                                                        </p>
                                                        <!---->
                                                        <p _ngcontent-serverapp-c1181781737 class="card-text ng-tns-c1181781737-1 ng-star-inserted">
                                                            <i class="fa fa-cog" aria-hidden="true" style="color: #1E3F66"></i>
                                                            {{$related_candidate->work_experience}}yr Experience
                                                        </p>
                                                        <!---->
                                                        <p _ngcontent-serverapp-c1181781737 class="card-text ng-tns-c1181781737-1">
                                                            <i class="fa fa-calendar" aria-hidden="true" style="color: #1E3F66"></i>
                                                            From {{ \Carbon\Carbon::parse($related_candidate->job_start_date)->format('d F Y') }} | {{$related_candidate->job_type }}
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----><!---->
        <div _ngcontent-serverapp-c1181781737="" id="footer" class="footer_custom mt-5 mobile_sticky permenant-sticky ng-tns-c1181781737-1 ng-star-inserted">

            <div _ngcontent-serverapp-c1181781737="" id="footer2" class="ng-tns-c1181781737-1">
                <div _ngcontent-serverapp-c1181781737="" class="container ng-tns-c1181781737-1">
                    <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                        <div _ngcontent-serverapp-c1181781737="" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 res-footer res-button-footer footer-center ng-tns-c1181781737-1">
                            <div _ngcontent-serverapp-c1181781737="" class="footer-apply-content ng-tns-c1181781737-1">
                                <div _ngcontent-serverapp-c1181781737="" class="left ng-tns-c1181781737-1">
                                    <div _ngcontent-serverapp-c1181781737="" class="left-wrapper ng-tns-c1181781737-1">
                                        <div _ngcontent-serverapp-c1181781737 role="group" class="btn-group ng-tns-c1181781737-1">

                                            @if(Auth::check())
                                                @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER)
                                                    <button _ngcontent-serverapp-c1181781737 type="button"
                                                            title="Contact Candidate"
                                                            data-toggle="modal"
                                                            data-target="#messageModal"
                                                            class="btn btn-primary ng-tns-c1181781737-1 ng-star-inserted">
                                                        Contact
                                                    </button>
                                                @endif
                                            @else
                                                <button _ngcontent-serverapp-c1181781737 type="button"
                                                        title="Contact Candidate"
                                                        data-toggle="modal"
                                                        data-target="#loginModal"
                                                        class="btn btn-primary ng-tns-c1181781737-1 ng-star-inserted">
                                                    Contact
                                                </button>
                                            @endif
                                        </div>
                                        <span _ngcontent-serverapp-c1181781737="" class="left-content-text footer-text ng-tns-c1181781737-1 ng-star-inserted"> Dazel Joy </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div _ngcontent-serverapp-c1181781737="" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 res-footer res-button-footer ng-tns-c1181781737-1">
                            <div _ngcontent-serverapp-c1181781737="" class="footer-apply-content right-content ng-tns-c1181781737-1">
                                <div _ngcontent-serverapp-c1181781737="" class="right ng-tns-c1181781737-1">
                                    <div _ngcontent-serverapp-c1181781737="" class="right-wrapper ng-tns-c1181781737-1">
                                        <span _ngcontent-serverapp-c1181781737="" class="left-content-text ng-tns-c1181781737-1">Share</span>
                                        <ul _ngcontent-serverapp-c1181781737="" class="social-sharing ng-tns-c1181781737-1">
                                            <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                <a _ngcontent-serverapp-c1181781737="" href="javascript:;" class="ng-tns-c1181781737-1">
                                                    <i _ngcontent-serverapp-c1181781737="" aria-hidden="true" class="ng-tns-c1181781737-1">
                                                        <app-icons _ngcontent-serverapp-c1181781737="" name="facebook" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                                        </app-icons>
                                                    </i>
                                                </a>
                                            </li>
                                            <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                <a _ngcontent-serverapp-c1181781737="" href="javascript:;" class="ng-tns-c1181781737-1">
                                                    <i _ngcontent-serverapp-c1181781737="" aria-hidden="true" class="ng-tns-c1181781737-1">
                                                        <app-icons _ngcontent-serverapp-c1181781737="" name="whatsapp" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                                        </app-icons>
                                                    </i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
    </section>

    <script>

        var lightbox = document.getElementById("customLightbox");

        var img = document.getElementById("myImg");
        var lightboxImg = document.getElementById("lightboxImage");

        img.onclick = function(){
            lightbox.style.display = "flex";
            lightboxImg.src = this.src;
        }

        var closeBtn = document.getElementsByClassName("close")[0];

        closeBtn.onclick = function() {
            lightbox.style.display = "none";
        }

        lightbox.onclick = function(event) {
            if (event.target === lightbox) {
                lightbox.style.display = "none";
            }
        }

    </script>

    <!--    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">-->
    <!--    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">-->
    <!--        <div class="modal-content">-->
    <!--            <div class="modal-header">-->
    <!--                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">-->
    <!--                    <span aria-hidden="true">&times;</span>-->
    <!--                </button>-->
    <!--            </div>-->
    <!--            <div class="modal-body">-->
    <!--                <img id="modalImage" src="" class="img-fluid" alt="" style="width:100%; height: 500px;">-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <!--<script type="text/javascript">-->
    <!--    document.getElementById('myImg').addEventListener('click', function() {-->
    <!--    var modalImage = document.getElementById('modalImage');-->
    <!--    modalImage.src = this.src;-->
    <!--    $('#imageModal').modal('show');-->
    <!--});-->

    <!--</script>-->


@endsection
