@extends('frontend.layouts.app')
@section('title','CANDIDATES')
@section('content')
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <style>
        .top-banner-wrapper{
            margin-top: 97px;
        }
    </style>


    <section _ngcontent-serverapp-c3093988918="" class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom-container page-section">
        <div _ngcontent-serverapp-c3093988918="" class="container">
            <div _ngcontent-serverapp-c3093988918="" class="">
                <div _ngcontent-serverapp-c3093988918="" class="row">
                    <div _ngcontent-serverapp-c3093988918="" class="col-12">
                        <div _ngcontent-serverapp-c3093988918="" class="top-banner-wrapper">
                            <div _ngcontent-serverapp-c3093988918="" class="top-banner-content small-section ng-star-inserted">
                                <h1 _ngcontent-serverapp-c3093988918=""><span _ngcontent-serverapp-c3093988918="">{{$candidate_landing_page->first_heading}}</span></h1>
                                <p _ngcontent-serverapp-c3093988918="" class="header_2"> {{$candidate_landing_page->first_paragraph}} </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div _ngcontent-serverapp-c3093988918="" class="row mt-3">
                <div _ngcontent-serverapp-c3093988918="" class="col-12">
                    <div _ngcontent-serverapp-c3093988918="" class="filter-sort-bar c-filter-bar filters-btn">
                        <div _ngcontent-serverapp-c3093988918="" class="filter-main-btn mb-0"><a _ngcontent-serverapp-c3093988918="" class="filter-button btn-sm" data-toggle="modal" data-target="#fiterModal"><i _ngcontent-serverapp-c3093988918="" class="fa fa-filter"></i>Filter </a></div>
                        <div _ngcontent-serverapp-c3093988918="" class="c-res-job-booster" style="gap: 10px;">
                            <div _ngcontent-serverapp-c3093988918="">
                                <app-order-by _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c1558467617="" ngh="3">
                                    <div _ngcontent-serverapp-c1558467617="" class="dropdown-btn">
                                        <div _ngcontent-serverapp-c1558467617="" class="dropdown">
                                            <!-- <button _ngcontent-serverapp-c1558467617="" class="dropbtn">
                                              <span _ngcontent-serverapp-c1558467617="" class="mr-2"><i _ngcontent-serverapp-c1558467617="" aria-hidden="true" class="fa fa-sort-amount-desc"></i></span><span _ngcontent-serverapp-c1558467617="">Last Active</span>
                                            </button> -->
                                            <!---->
                                        </div>
                                    </div>
                                </app-order-by>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div _ngcontent-serverapp-c3093988918="" class="row mt-3" style="clear: both;">
                <div _ngcontent-serverapp-c3093988918="" class="col-md-3 col-lg-3 d-none d-md-clock d-lg-block d-xl-block collection-filter-block ng-star-inserted">
                    <app-filter _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c2564575186="" ngh="10">
                        <div _ngcontent-serverapp-c2564575186="" class="row product-service_1">
                            <div _ngcontent-serverapp-c2564575186="" class="col-md-12 col-lg-12">
                                <div _ngcontent-serverapp-c2564575186="" class="row filter-ar">
                                    <!---->
                                    <div _ngcontent-serverapp-c2564575186="" class="top-banner-content small-section col-12 ng-star-inserted">
                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container mt-3 mb-1 ng-star-inserted">
                                            <h2 _ngcontent-serverapp-c2564575186="" class="float-left custom_h2"><i _ngcontent-serverapp-c2564575186="" class="fa fa-serch"></i> {{$candidate_landing_page->second_heading}}  </h2>
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container mb-0 ng-star-inserted">
                                            <p _ngcontent-serverapp-c2564575186="" class="float-left filter_text"> Filter </p>
                                            <a href="{{route('candidates')}}">
                                                <p _ngcontent-serverapp-c2564575186="" class="text-success float-right reset_button" style="cursor: pointer;"><i _ngcontent-serverapp-c2564575186="" class="fa fa-repeat"></i> Reset </p>
                                            </a>
                                        </div>
                                        <!---->
                                        <form method="GET" action="{{ route('candidates') }}" id="filterForm">
                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Position</h3>

                                                <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494346" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494346-input" name="position_apply" value="Domestic Helper" tabindex="0" {{ $positionApply == 'Domestic Helper' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494346-input">Domestic Helper</label>
                                                        </div>
                                                    </mat-radio-button>
                                                    <!---->
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494347" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494347-input" name="position_apply" value="Driver" tabindex="0" {{ $positionApply == 'Driver' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494347-input">Driver</label>
                                                        </div>
                                                    </mat-radio-button>
                                                    <!---->
                                                    <!---->
                                                </mat-radio-group>

                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Start Date</h3>
                                                <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                    <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="7">

                                                        <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                            <div class="mat-mdc-text-field-wrapper">
                                                                <div class="mat-mdc-form-field-flex">
                                                                    <input class="form-control" type="date" name="job_start_date" placeholder="dd-mm-yyyy" id="mat-input-1442183" value="{{ $jobStartDate }}" onchange="this.form.submit()">
                                                                </div>
                                                            </div>
                                                        </mat-form-field>

                                                    </app-date-picker>
                                                </div>
                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-pos ng-star-inserted mt-1">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">
                                                    <!----><span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Candidate Location</span>
                                                    <!---->
                                                </h3>

                                                <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                    <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                        <!---->
                                                        <div class="mat-mdc-text-field-wrapper">
                                                            <div class="mat-mdc-form-field-flex">
                                                                <select class="form-control" name="present_country" onchange="this.form.submit()">
                                                                    <option value="" disabled selected>Select Location</option>
                                                                    @php
                                                                        $locations = App\Models\Country::all();
                                                                    @endphp
                                                                    @foreach($locations as $loc)
                                                                        <option value="{{$loc->id}}" {{ request()->get('present_country') == $loc->id ? 'selected' : '' }}>{{ $loc->name }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>

                                                    </mat-form-field>
                                                </app-multi-select>

                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio filter-pos ng-star-inserted mt-1">
                                                <div _ngcontent-serverapp-c2564575186="" class="product-page-filter">
                                                    <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Type</h3>

                                                    <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494348" ngh="0">
                                                            <div class="mdc-form-field">
                                                                <div class="mdc-radio">
                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494348-input" name="job_type" value="Full Time" tabindex="0" {{ $jobtypeApply == 'Full Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                    <div class="mdc-radio__background">
                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                    </div>
                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                    </div>
                                                                </div>
                                                                <label class="mdc-label" for="mat-radio-11494348-input">Full Time</label>
                                                            </div>
                                                        </mat-radio-button>
                                                        <!---->
                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494349" ngh="0">
                                                            <div class="mdc-form-field">
                                                                <div class="mdc-radio">
                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494349-input" name="job_type" value="Part Time" tabindex="0" {{ $jobtypeApply == 'Part Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                    <div class="mdc-radio__background">
                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                    </div>
                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                    </div>
                                                                </div>
                                                                <label class="mdc-label" for="mat-radio-11494349-input">Part Time</label>
                                                            </div>
                                                        </mat-radio-button>
                                                        <!---->
                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494350" ngh="0">
                                                            <div class="mdc-form-field">
                                                                <div class="mdc-radio">
                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494350-input" name="job_type" value="Temporary" tabindex="0" {{ $jobtypeApply == 'Temporary' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                    <div class="mdc-radio__background">
                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                    </div>
                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                    </div>
                                                                </div>
                                                                <label class="mdc-label" for="mat-radio-11494350-input">Temporary</label>
                                                            </div>
                                                        </mat-radio-button>
                                                        <!---->
                                                        <!---->
                                                    </mat-radio-group>

                                                </div>
                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Contract Status</h3>

                                                <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                    <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-4 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                        <!---->
                                                        <div class="mat-mdc-text-field-wrapper">
                                                            <div class="mat-mdc-form-field-flex">
                                                                <select class="form-control" name="work_status" onchange="this.form.submit()">
                                                                    <option value="" disabled selected>Select Status</option>

                                                                    <option value="Finished Contract" {{ request()->get('work_status') == 'Finished Contract' ? 'selected' : '' }}>Finished Contract</option>

                                                                    <option value="Terminated (Relocation / Financial)" {{ request()->get('work_status') == 'Terminated (Relocation / Financial)' ? 'selected' : '' }}>Terminated (Relocation / Financial)</option>

                                                                    <option value="Terminated (Other)" {{ request()->get('work_status') == 'Terminated (Other)' ? 'selected' : '' }}>Terminated (Other)</option>

                                                                    <option value="Break Contract" {{ request()->get('work_status') == 'Break Contract' ? 'selected' : '' }}>Break Contract</option>

                                                                    <option value="Transfer" {{ request()->get('work_status') == 'Transfer' ? 'selected' : '' }}>Transfer</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </mat-form-field>
                                                </app-multi-select>

                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio filter-pos ng-star-inserted mt-1">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">
                                                    <!----><span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Resume by</span>
                                                    <!---->
                                                </h3>
                                                <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494341" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494341-input" name="mat-radio-group-11494339" value="Direct" tabindex="0">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494341-input">Direct</label>
                                                        </div>
                                                    </mat-radio-button>
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494342" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494342-input" name="mat-radio-group-11494339" value="Agency" tabindex="0">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494342-input">Agency</label>
                                                        </div>
                                                    </mat-radio-button>
                                                </mat-radio-group>
                                            </div>
                                            <!---->
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">
                                                <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">
                                                    <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Language</h3>

                                                    <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                        <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-9 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                            <!---->
                                                            <div class="mat-mdc-text-field-wrapper">
                                                                <div class="mat-mdc-form-field-flex">
                                                                    <select class="form-control" name="languages" onchange="this.form.submit()">
                                                                        <option value="" disabled selected>Select Language</option>
                                                                        @php
                                                                            $languages = App\Models\Language::all();
                                                                        @endphp
                                                                        @foreach($languages as $language)
                                                                            <option value="{{$language->id}}" {{ request()->get('languages') == $language->id ? 'selected' : '' }}>{{ $language->name }}</option>
                                                                        @endforeach

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </mat-form-field>
                                                    </app-multi-select>

                                                </div>
                                                <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted mt-3">
                                                    <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Main Skills</h3>

                                                    <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                        <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-11 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                            <!---->
                                                            <div class="mat-mdc-text-field-wrapper">
                                                                <div class="mat-mdc-form-field-flex">
                                                                    <select class="form-control" name="main_skills" onchange="this.form.submit()">
                                                                        <option value="" disabled selected>Select Main Skills</option>
                                                                        @php
                                                                            $main_skills = App\Models\MainSkill::all();
                                                                        @endphp
                                                                        @foreach($main_skills as $main_skill)
                                                                            <option value="{{$main_skill->id}}" {{ request()->get('main_skills') == $main_skill->id ? 'selected' : '' }}>{{ $main_skill->name }}</option>
                                                                        @endforeach

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </mat-form-field>
                                                    </app-multi-select>

                                                </div>
                                                <!---->
                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted mt-3">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Nationality</h3>

                                                <mat-form-field _ngcontent-serverapp-c2564575186="" appearance="outline" class="mat-mdc-form-field ng-tns-c1205077789-7 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                    <!---->
                                                    <div class="mat-mdc-text-field-wrapper">
                                                        <div class="mat-mdc-form-field-flex">
                                                            <select class="form-control" name="nationality" onchange="this.form.submit()">
                                                                <option value="" disabled selected>Select Location</option>
                                                                @php
                                                                    $countries = App\Models\Country::all();
                                                                @endphp
                                                                @foreach($countries as $country)
                                                                    <option value="{{$country->id}}" {{ request()->get('nationality') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </mat-form-field>

                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-radio ng-star-inserted mt-3">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Gender</h3>

                                                <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494344" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494344-input" name="gender" value="male" tabindex="0" {{ $genderApply == 'male' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494344-input">Male</label>
                                                        </div>
                                                    </mat-radio-button>
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494345" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494345-input" name="gender" value="female" tabindex="0" {{ $genderApply == 'female' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494345-input">Female</label>
                                                        </div>
                                                    </mat-radio-button>
                                                </mat-radio-group>

                                            </div>

                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Helper Name</h3>
                                                <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                    <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="7">

                                                        <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                            <div class="mat-mdc-text-field-wrapper">
                                                                <div class="mat-mdc-form-field-flex">
                                                                    <input class="form-control" type="text" name="middle_name" placeholder="Search with Helper Name" id="mat-input-1442183" value="{{ $middle_nameApply }}" onchange="this.form.submit()">
                                                                </div>
                                                            </div>
                                                        </mat-form-field>

                                                    </app-date-picker>
                                                </div>
                                            </div>
                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Working Experience</h3>
                                                <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                    <input type="text" class="working_experience" name="working_experience" id="working_experience" onchange="this.form.submit()"
                                                           @if($working_experience)
                                                               @php
                                                                   $experience = explode(';',$working_experience);
                                                                   $from = $experience['0'];
                                                                   $to = $experience['1'];
                                                               @endphp
                                                               data-from="{{$from}}"
                                                           data-to="{{$to}}"
                                                        @endif />
                                                </div>
                                            </div>
                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Age</h3>
                                                <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                    <input type="text" class="age" name="age" id="age" onchange="this.form.submit()"  @if($age_filter)
                                                        @php
                                                            $age = explode(';',$age_filter);
                                                            $from_age = $age['0'];
                                                            $to_age = $age['1'];
                                                        @endphp
                                                        data-from="{{$from_age}}"
                                                           data-to="{{$to_age}}"
                                                        @endif />
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <!---->
                                </div>
                            </div>
                        </div>
                    </app-filter>
                </div>


                @if(Auth::check())
                    @if(Auth::user()->role == \App\Models\User::ROLE_EMPLOYER  )
                        <div _ngcontent-serverapp-c3093988918="" class="col-md-12 col-lg-9">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12" style="padding: 0px;">
                                        <div class="nav nav-tabs justify-content-between" id="nav-tab" role="tablist" style="display: flex; justify-content: space-between; border-bottom: 1px solid #ddd;">
                                            <a class="nav-item nav-link @if(Request::url() == route('candidates')) active @endif" id="nav-home-tab" href="{{ route('candidates') }}" aria-controls="nav-home" aria-selected="true" style="flex: 1; color: black !important; border: 1px solid #ddd !important; border-radius: 4px; padding: 10px 20px; transition: background-color 0.3s, color:black !important; text-decoration: none;">
                                                <i class="fa fa-home"></i> All
                                            </a>
                                            <a class="nav-item nav-link @if(Request::url() == route('recommended')) active @endif" id="nav-profile-tab" href="{{ route('recommended') }}" aria-controls="nav-profile" aria-selected="false" style="flex: 1; color: black !important; border: 1px solid #ddd !important; border-radius: 4px; padding: 10px 20px; transition: background-color 0.3s, color:black !important; text-decoration: none;">
                                                <i class="fa fa-user"></i> Recommended
                                            </a>
                                            <a class="nav-item nav-link @if(Request::url() == route('applicant')) active @endif" id="nav-contact-tab" href="{{ route('applicant') }}" aria-controls="nav-contact" aria-selected="false" style="flex: 1; color: black !important; border: 1px solid #ddd !important; border-radius: 4px; padding: 10px 20px; transition: background-color 0.3s, color:black !important; text-decoration: none;">
                                                <i class="fa fa-cog"></i> Applicant
                                            </a>
                                            <a class="nav-item nav-link @if(Request::url() == route('shortlist')) active @endif" id="nav-shortlisted-tab" href="{{ route('shortlist') }}" aria-controls="nav-shortlisted" aria-selected="false" style="flex: 1; color: black !important; border: 1px solid #ddd !important; border-radius: 4px; padding: 10px 20px; transition: background-color 0.3s, color 0.3s; text-decoration: none;">
                                                <i class="fa fa-star"></i> Shortlisted
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div _ngcontent-serverapp-c3093988918="" class="collection-product-wrapper mt-5">
                                <div _ngcontent-serverapp-c3093988918="" class="product-wrapper-grid list-view">
                                    <!---->
                                    <div _ngcontent-serverapp-c3093988918="" class="ng-star-inserted">

                                        @if($candidates->isEmpty())
                                            <div class="row">
                                                <div class="col-12 mt-5">
                                                    <img class="mx-auto d-block" src="{{asset('cdn-sub/front-app/assets/images/misc/empty-search.jpg')}}">

                                                </div>
                                                <div class="col-10 offset-1">
                                                    <h4 class="text-center">To find available candidates, you just need to adjust your search criteria.</h4>

                                                </div>
                                            </div>
                                        @else

                                            @foreach($candidates as $candidate)
                                                @php
                                                    $check_shortlist = App\Models\ShortList::where('user_id', Auth::user()->id)->where('candidate_id', $candidate->id)->exists();
                                                @endphp
                                                <div _ngcontent-serverapp-c3093988918="" class="col-grid-box w-100 ng-star-inserted">
                                                    <candidate-detail-block _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c400119168="" ngh="11">
                                                        <div _ngcontent-serverapp-c400119168="" class="product-box-container mb-4 ng-star-inserted">
                                                            <div _ngcontent-serverapp-c400119168="" class="product-box">
                                                                <div _ngcontent-serverapp-c400119168="" class="img-wrapper_Custom w-10">
                                                                    <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="{{route('candidate_details', $candidate)}}">
                                                                        <div _ngcontent-serverapp-c400119168="" class="front custom_front_rsume_image">
                                                                            <picture _ngcontent-serverapp-c400119168="">
                                                                                <source _ngcontent-serverapp-c400119168="" onerror="this.src={{ asset('storage/profile_image/' . $candidate->profile_image) }}" width="78px" height="78px" media="(max-width:325px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                                <source _ngcontent-serverapp-c400119168="" onerror="this.src={{ asset('storage/profile_image/' . $candidate->profile_image) }}" width="84px" height="83px" media="(max-width:768px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                                <source _ngcontent-serverapp-c400119168="" onerror="this.src={{ asset('storage/profile_image/' . $candidate->profile_image) }}" width="120px" height="120px" media="(max-width:1024px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                                <img _ngcontent-serverapp-c400119168="" loading="lazy"  width="143px" height="143px" alt="Mery" src="{{ asset('storage/profile_image/' . $candidate->profile_image) }}" onerror="this.src='{{asset('assets/images/no-imag-found.png')}}';" class="">
                                                                            </picture>
                                                                            <span _ngcontent-serverapp-c400119168="" class="d-none">Helper Profile Image</span>
                                                                        </div>
                                                                    </a>
                                                                    <div _ngcontent-serverapp-c400119168="" class="listing-sub-title-agency text-left w-20 mt-2">
                                                                        <label _ngcontent-serverapp-c400119168="" class="label_blue">
                                                                            <span _ngcontent-serverapp-c400119168="" class="ng-star-inserted">Direct</span>

                                                                        </label>
                                                                    </div>

                                                                </div>
                                                                <div _ngcontent-serverapp-c400119168="" class="product-detail w-100 pt-2 align-self-baseline text-left">
                                                                    <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="{{route('candidate_details', $candidate)}}">

                                                                        <h4 _ngcontent-serverapp-c400119168="" class="product-title"> {{$candidate->name}} <span _ngcontent-serverapp-c400119168="">- {{$candidate->resume_detail->age ??''}}<label _ngcontent-serverapp-c400119168="">yr</label></span></h4>
                                                                        <div _ngcontent-serverapp-c400119168="" class="product-header-description">
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="product-sub-title 1 mt-1"><span _ngcontent-serverapp-c400119168="">{{$candidate->resume_detail->position_apply ??''}} - {{$candidate->resume_detail->work_status ??''}}</span> </h5>
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="product-location ng-star-inserted">
                                                                                  <span _ngcontent-serverapp-c400119168="" class="location">
                                                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                                    {{$candidate->resume_detail->present_location->name  ??''}}
                                                                                  </span>
                                                                            </h5>
                                                                        </div>
                                                                        <div _ngcontent-serverapp-c400119168="" class="product-description mt-2" style="word-break: break-word;"> {!! $candidate->resume_detail->resume_description ??'' !!} </div>
                                                                        <div _ngcontent-serverapp-c400119168="" class="product-footer">
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="footer-experience">

                                                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                                                                {{$candidate->resume_detail->work_experience ??''}}yr experience
                                                                            </h5>
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="footer-date ng-star-inserted">

                                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                                From {{ \Carbon\Carbon::parse($candidate->resume_detail->job_start_date ?? '')->format('d F Y') }} | {{$candidate->resume_detail->job_type ??''}}
                                                                            </h5>
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="footer-active ng-star-inserted">

                                                                                <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                Very Active
                                                                            </h5>
                                                                        </div>
                                                                    </a>
                                                                </div>

                                                            </div>

                                                            <div _ngcontent-serverapp-c400119168="" class="row bg-primary py-2 w-100 positions m-0 px-1 inherit-bottom ng-star-inserted">
                                                                <div _ngcontent-serverapp-c400119168="" class="col-12 px-1 text-left">
                                                                    <a class="custom-save-job ng-star-inserted" candidate-id="{{$candidate->id}}" style="color: rgb(255, 255, 255);">
                                                                        <i class="fa fa-bookmark mr-1 ng-star-inserted {{ $check_shortlist ? 'shortlisted' : '' }}" style="color: {{ $check_shortlist ? 'rgb(235, 186, 22)' : 'rgb(255, 255, 255)' }};"></i>Shortlist Candidate
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </candidate-detail-block>
                                                </div>
                                            @endforeach
                                        @endif

                                        <!---->
                                    </div>
                                    <!---->
                                    <!---->
                                    <div _ngcontent-serverapp-c3093988918="" >
                                        <app-pagination>
                                            {{ $candidates->links() }}
                                        </app-pagination>
                                    </div>
                                    <!---->
                                </div>
                            </div>
                        </div>
                    @elseif(Auth::user()->role == \App\Models\User::ROLE_AGENCY)
                        <div _ngcontent-serverapp-c3093988918="" class="col-md-12 col-lg-9">

                            <div _ngcontent-serverapp-c3093988918="" class="collection-product-wrapper">
                                <div _ngcontent-serverapp-c3093988918="" class="product-wrapper-grid list-view">
                                    <!---->
                                    <div _ngcontent-serverapp-c3093988918="" class="ng-star-inserted">
                                        @if($candidates->isEmpty())
                                            <div class="row">
                                                <div class="col-12 mt-5">
                                                    <img class="mx-auto d-block" src="{{asset('cdn-sub/front-app/assets/images/misc/empty-search.jpg')}}">

                                                </div>
                                                <div class="col-10 offset-1">
                                                    <h4 class="text-center">To find available candidates, you just need to adjust your search criteria.</h4>

                                                </div>
                                            </div>
                                        @else
                                            @foreach($candidates as $candidate)
                                                <div _ngcontent-serverapp-c3093988918="" class="col-grid-box w-100 ng-star-inserted">
                                                    <candidate-detail-block _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c400119168="" ngh="11">
                                                        <div _ngcontent-serverapp-c400119168="" class="product-box-container mb-4 ng-star-inserted">
                                                            <div _ngcontent-serverapp-c400119168="" class="product-box">
                                                                <div _ngcontent-serverapp-c400119168="" class="img-wrapper_Custom w-10">
                                                                    <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="{{route('candidate_details', $candidate)}}">
                                                                        <div _ngcontent-serverapp-c400119168="" class="front custom_front_rsume_image">
                                                                            <picture _ngcontent-serverapp-c400119168="">
                                                                                <source _ngcontent-serverapp-c400119168="" onerror="this.src={{asset('assets/images/images.png')}}" width="78px" height="78px" media="(max-width:325px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                                <source _ngcontent-serverapp-c400119168="" onerror="this.src={{asset('assets/images/images.png')}}" width="84px" height="83px" media="(max-width:768px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                                <source _ngcontent-serverapp-c400119168="" onerror="this.src={{asset('assets/images/images.png')}}" width="120px" height="120px" media="(max-width:1024px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                                <img _ngcontent-serverapp-c400119168="" loading="lazy"  width="143px" height="143px" alt="Mery" src="{{ asset('storage/profile_image/' . $candidate->profile_image) }}" onerror="this.src='{{asset('assets/images/no-imag-found.png')}}';" class="">
                                                                            </picture>
                                                                            <span _ngcontent-serverapp-c400119168="" class="d-none">Helper Profile Image</span>
                                                                        </div>
                                                                    </a>
                                                                    <div _ngcontent-serverapp-c400119168="" class="listing-sub-title-agency text-left w-20 mt-2">
                                                                        <label _ngcontent-serverapp-c400119168="" class="label_blue">
                                                                            <span _ngcontent-serverapp-c400119168="" class="ng-star-inserted">Direct</span>

                                                                        </label>
                                                                    </div>

                                                                </div>
                                                                <div _ngcontent-serverapp-c400119168="" class="product-detail w-100 pt-2 align-self-baseline text-left">
                                                                    <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="{{route('candidate_details', $candidate)}}">

                                                                        <h4 _ngcontent-serverapp-c400119168="" class="product-title"> {{$candidate->name}} <span _ngcontent-serverapp-c400119168="">- {{$candidate->resume_detail->age ??''}}<label _ngcontent-serverapp-c400119168="">yr</label></span></h4>
                                                                        <div _ngcontent-serverapp-c400119168="" class="product-header-description">
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="product-sub-title 1 mt-1"><span _ngcontent-serverapp-c400119168="">{{$candidate->resume_detail->position_apply ??''}} - {{$candidate->resume_detail->work_status ??''}}</span> </h5>
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="product-location ng-star-inserted">
                                                                              <span _ngcontent-serverapp-c400119168="" class="location">
                                                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                                          {{$candidate->resume_detail->present_location->name  ??''}}
                                                                              </span>
                                                                            </h5>
                                                                            <!---->
                                                                        </div>
                                                                        <div _ngcontent-serverapp-c400119168="" class="product-description mt-2" style="word-break: break-word;"> {!! $candidate->resume_detail->resume_description ??'' !!} </div>
                                                                        <div _ngcontent-serverapp-c400119168="" class="product-footer">
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="footer-experience">

                                                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                                                                {{$candidate->resume_detail->work_experience ??''}}yr experience
                                                                            </h5>
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="footer-date ng-star-inserted">

                                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                                From {{ \Carbon\Carbon::parse($candidate->resume_detail->job_start_date ?? '')->format('d M y') }} | {{$candidate->resume_detail->job_type ??''}}
                                                                            </h5>
                                                                            <h5 _ngcontent-serverapp-c400119168="" class="footer-active ng-star-inserted">

                                                                                <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                Very Active
                                                                            </h5>
                                                                        </div>
                                                                    </a>
                                                                </div>

                                                            </div>

                                                    </candidate-detail-block>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div _ngcontent-serverapp-c3093988918="" class="center-pagination ng-star-inserted">
                                        @if($candidates->hasPages())
                                            <app-pagination>
                                                <ul class="pagination">

                                                    @if ($candidates->onFirstPage())
                                                        <li class="page-item disabled">
                                                            <a class="prev page-link" href="#" aria-disabled="true">«</a>
                                                        </li>
                                                    @else
                                                        <li class="page-item">
                                                            <a class="prev page-link" href="{{ $candidates->previousPageUrl() }}" rel="prev">«</a>
                                                        </li>
                                                    @endif

                                                    @foreach ($candidates->links()->elements as $element)

                                                        @if (is_string($element))
                                                            <li class="page-item disabled"><a class="page-link" href="#">{{ $element }}</a></li>
                                                        @endif

                                                        @if (is_array($element))
                                                            @foreach ($element as $page => $url)
                                                                @if ($page == $candidates->currentPage())
                                                                    <li class="page-item active"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                                                @else
                                                                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach

                                                    @if ($candidates->hasMorePages())
                                                        <li class="page-item">
                                                            <a class="next page-link" href="{{ $candidates->nextPageUrl() }}" rel="next">»</a>
                                                        </li>
                                                    @else
                                                        <li class="page-item disabled">
                                                            <a class="next page-link" href="#" aria-disabled="true">»</a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </app-pagination>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    <div _ngcontent-serverapp-c3093988918="" class="col-md-12 col-lg-9">

                        <div _ngcontent-serverapp-c3093988918="" class="collection-product-wrapper">
                            <div _ngcontent-serverapp-c3093988918="" class="product-wrapper-grid list-view">
                                <!---->
                                <div _ngcontent-serverapp-c3093988918="" class="ng-star-inserted">


                                    @if($candidates->isEmpty())
                                        <div class="row">
                                            <div class="col-12 mt-5">
                                                <img class="mx-auto d-block" src="{{asset('cdn-sub/front-app/assets/images/misc/empty-search.jpg')}}">

                                            </div>
                                            <div class="col-10 offset-1">
                                                <h4 class="text-center">To find available candidates, you just need to adjust your search criteria.</h4>

                                            </div>
                                        </div>
                                    @else
                                        @foreach($candidates as $candidate)
                                            <div _ngcontent-serverapp-c3093988918="" class="col-grid-box w-100 ng-star-inserted">
                                                <candidate-detail-block _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c400119168="" ngh="11">
                                                    <div _ngcontent-serverapp-c400119168="" class="product-box-container mb-4 ng-star-inserted">
                                                        <div _ngcontent-serverapp-c400119168="" class="product-box">
                                                            <div _ngcontent-serverapp-c400119168="" class="img-wrapper_Custom w-10">
                                                                <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="{{route('candidate_details', $candidate)}}">
                                                                    <div _ngcontent-serverapp-c400119168="" class="front custom_front_rsume_image">
                                                                        <picture _ngcontent-serverapp-c400119168="">
                                                                            <source _ngcontent-serverapp-c400119168="" onerror="this.src={{asset('assets/images/images.png')}}" width="78px" height="78px" media="(max-width:325px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                            <source _ngcontent-serverapp-c400119168="" onerror="this.src={{asset('assets/images/images.png')}}" width="84px" height="83px" media="(max-width:768px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                            <source _ngcontent-serverapp-c400119168="" onerror="this.src={{asset('assets/images/images.png')}}" width="120px" height="120px" media="(max-width:1024px)" srcset="{{ asset('storage/profile_image/' . $candidate->profile_image) }}">
                                                                            <img _ngcontent-serverapp-c400119168="" loading="lazy"  width="143px" height="143px" alt="Mery" src="{{ asset('storage/profile_image/' . $candidate->profile_image) }}" onerror="this.src='{{asset('assets/images/no-imag-found.png')}}';" class="">
                                                                        </picture>
                                                                        <span _ngcontent-serverapp-c400119168="" class="d-none">Helper Profile Image</span>
                                                                    </div>
                                                                </a>
                                                                <div _ngcontent-serverapp-c400119168="" class="listing-sub-title-agency text-left w-20 mt-2">
                                                                    <label _ngcontent-serverapp-c400119168="" class="label_blue">
                                                                        @if(is_null($candidate->added_by))
                                                                            <span _ngcontent-serverapp-c400119168="" class="ng-star-inserted">Direct</span>
                                                                        @else
                                                                            <span _ngcontent-serverapp-c400119168="" class="ng-star-inserted">Agency</span>
                                                                        @endif


                                                                    </label>
                                                                </div>

                                                            </div>
                                                            <div _ngcontent-serverapp-c400119168="" class="product-detail w-100 pt-2 align-self-baseline text-left">
                                                                <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="{{route('candidate_details', $candidate)}}">

                                                                    <h4 _ngcontent-serverapp-c400119168="" class="product-title"> {{$candidate->name}} <span _ngcontent-serverapp-c400119168="">- {{$candidate->resume_detail->age ??''}}<label _ngcontent-serverapp-c400119168="">yr</label></span></h4>
                                                                    <div _ngcontent-serverapp-c400119168="" class="product-header-description">
                                                                        <h5 _ngcontent-serverapp-c400119168="" class="product-sub-title 1 mt-1"><span _ngcontent-serverapp-c400119168="">{{$candidate->resume_detail->position_apply ??''}} - {{$candidate->resume_detail->work_status ??''}}</span> </h5>
                                                                        <h5 _ngcontent-serverapp-c400119168="" class="product-location ng-star-inserted">
                                                                              <span _ngcontent-serverapp-c400119168="" class="location">
                                                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                                  {{$candidate->resume_detail->present_location->name  ??''}}
                                                                              </span>
                                                                        </h5>
                                                                        <!---->
                                                                    </div>
                                                                    <div _ngcontent-serverapp-c400119168="" class="product-description mt-2" style="word-break: break-word;"> {!! $candidate->resume_detail->resume_description ??'' !!} </div>
                                                                    <div _ngcontent-serverapp-c400119168="" class="product-footer">
                                                                        <h5 _ngcontent-serverapp-c400119168="" class="footer-experience">

                                                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                                                            {{$candidate->resume_detail->work_experience ??''}}yr experience
                                                                        </h5>
                                                                        <h5 _ngcontent-serverapp-c400119168="" class="footer-date ng-star-inserted">

                                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                            From {{ \Carbon\Carbon::parse($candidate->resume_detail->job_start_date ?? '')->format('d M y') }} | {{$candidate->resume_detail->job_type ??''}}
                                                                        </h5>
                                                                        <h5 _ngcontent-serverapp-c400119168="" class="footer-active ng-star-inserted">

                                                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                                                            Very Active
                                                                        </h5>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                        </div>

                                                </candidate-detail-block>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>

                                <div _ngcontent-serverapp-c3093988918="" class="center-pagination ng-star-inserted">
                                    @if($candidates->hasPages())
                                        <app-pagination>
                                            <ul class="pagination">

                                                @if ($candidates->onFirstPage())
                                                    <li class="page-item disabled">
                                                        <a class="prev page-link" href="#" aria-disabled="true">«</a>
                                                    </li>
                                                @else
                                                    <li class="page-item">
                                                        <a class="prev page-link" href="{{ $candidates->previousPageUrl() }}" rel="prev">«</a>
                                                    </li>
                                                @endif

                                                @foreach ($candidates->links()->elements as $element)

                                                    @if (is_string($element))
                                                        <li class="page-item disabled"><a class="page-link" href="#">{{ $element }}</a></li>
                                                    @endif

                                                    @if (is_array($element))
                                                        @foreach ($element as $page => $url)
                                                            @if ($page == $candidates->currentPage())
                                                                <li class="page-item active"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                                            @else
                                                                <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach

                                                @if ($candidates->hasMorePages())
                                                    <li class="page-item">
                                                        <a class="next page-link" href="{{ $candidates->nextPageUrl() }}" rel="next">»</a>
                                                    </li>
                                                @else
                                                    <li class="page-item disabled">
                                                        <a class="next page-link" href="#" aria-disabled="true">»</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </app-pagination>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div _ngcontent-serverapp-c3093988918="" class="container-fluid mt-4">
                <div _ngcontent-serverapp-c3093988918="" class="row">
                    <div _ngcontent-serverapp-c3093988918="" class="col-12 mb-5 extra_details_bottom description_tag">
                        <p _ngcontent-serverapp-c3093988918="" style="word-break: break-word;">
                            {!! $candidate_landing_page->description !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="fiterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog mt-5" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="pt-2 font-weight-bold">Filter</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row pb-3">
                        <div class="col-12">
                            <div _ngcontent-serverapp-c3093988918="" class="  d-md-clock d-lg-block d-xl-block collection-filter-block ng-star-inserted">
                                <app-filter _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c2564575186="" ngh="10">
                                    <div _ngcontent-serverapp-c2564575186="" class="row product-service_1">
                                        <div _ngcontent-serverapp-c2564575186="" class="col-md-12 col-lg-12">
                                            <div _ngcontent-serverapp-c2564575186="" class="row filter-ar">
                                                <!---->
                                                <div _ngcontent-serverapp-c2564575186="" class="top-banner-content small-section col-12 ng-star-inserted pt-4">

                                                    <form method="GET" action="{{ route('candidates') }}" id="filterForm">
                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Position</h3>

                                                            <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494346" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494346-input" name="position_apply" value="Domestic Helper" tabindex="0" {{ $positionApply == 'Domestic Helper' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494346-input">Domestic Helper</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                                <!---->
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494347" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494347-input" name="position_apply" value="Driver" tabindex="0" {{ $positionApply == 'Driver' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494347-input">Driver</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                                <!---->
                                                                <!---->
                                                            </mat-radio-group>

                                                        </div>
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Start Date</h3>
                                                            <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                                <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="7">

                                                                    <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                                        <div class="mat-mdc-text-field-wrapper">
                                                                            <div class="mat-mdc-form-field-flex">
                                                                                <input class="form-control" type="date" name="job_start_date" placeholder="dd-mm-yyyy" id="mat-input-1442183" value="{{ $jobStartDate }}" onchange="this.form.submit()">
                                                                            </div>
                                                                        </div>
                                                                    </mat-form-field>

                                                                </app-date-picker>
                                                            </div>
                                                        </div>
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-pos ng-star-inserted mt-1">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">
                                                                <!----><span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Candidate Location</span>
                                                                <!---->
                                                            </h3>

                                                            <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                                <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                    <!---->
                                                                    <div class="mat-mdc-text-field-wrapper">
                                                                        <div class="mat-mdc-form-field-flex">
                                                                            <select class="form-control" name="present_country" onchange="this.form.submit()">
                                                                                <option value="" disabled selected>Select Location</option>
                                                                                @php
                                                                                    $countries = App\Models\Country::all();
                                                                                @endphp
                                                                                @foreach($countries as $country)
                                                                                    <option value="{{$country->id}}" {{ request()->get('present_country') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                                @endforeach

                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </mat-form-field>
                                                            </app-multi-select>

                                                        </div>
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio filter-pos ng-star-inserted mt-1">
                                                            <div _ngcontent-serverapp-c2564575186="" class="product-page-filter">
                                                                <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Type</h3>

                                                                <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494348" ngh="0">
                                                                        <div class="mdc-form-field">
                                                                            <div class="mdc-radio">
                                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494348-input" name="job_type" value="Full Time" tabindex="0" {{ $jobtypeApply == 'Full Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                                <div class="mdc-radio__background">
                                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                                </div>
                                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                </div>
                                                                            </div>
                                                                            <label class="mdc-label" for="mat-radio-11494348-input">Full Time</label>
                                                                        </div>
                                                                    </mat-radio-button>
                                                                    <!---->
                                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494349" ngh="0">
                                                                        <div class="mdc-form-field">
                                                                            <div class="mdc-radio">
                                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494349-input" name="job_type" value="Part Time" tabindex="0" {{ $jobtypeApply == 'Part Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                                <div class="mdc-radio__background">
                                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                                </div>
                                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                </div>
                                                                            </div>
                                                                            <label class="mdc-label" for="mat-radio-11494349-input">Part Time</label>
                                                                        </div>
                                                                    </mat-radio-button>
                                                                    <!---->
                                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494350" ngh="0">
                                                                        <div class="mdc-form-field">
                                                                            <div class="mdc-radio">
                                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494350-input" name="job_type" value="Temporary" tabindex="0" {{ $jobtypeApply == 'Temporary' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                                <div class="mdc-radio__background">
                                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                                </div>
                                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                </div>
                                                                            </div>
                                                                            <label class="mdc-label" for="mat-radio-11494350-input">Temporary</label>
                                                                        </div>
                                                                    </mat-radio-button>
                                                                    <!---->
                                                                    <!---->
                                                                </mat-radio-group>

                                                            </div>
                                                        </div>
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Contract Status</h3>

                                                            <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                                <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-4 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                    <!---->
                                                                    <div class="mat-mdc-text-field-wrapper">
                                                                        <div class="mat-mdc-form-field-flex">
                                                                            <select class="form-control" name="work_status" onchange="this.form.submit()">
                                                                                <option value="" disabled selected>Select Status</option>

                                                                                <option value="Finished Contract" {{ request()->get('work_status') == 'Finished Contract' ? 'selected' : '' }}>Finished Contract</option>

                                                                                <option value="Terminated (Relocation / Financial)" {{ request()->get('work_status') == 'Terminated (Relocation / Financial)' ? 'selected' : '' }}>Terminated (Relocation / Financial)</option>

                                                                                <option value="Terminated (Other)" {{ request()->get('work_status') == 'Terminated (Other)' ? 'selected' : '' }}>Terminated (Other)</option>

                                                                                <option value="Break Contract" {{ request()->get('work_status') == 'Break Contract' ? 'selected' : '' }}>Break Contract</option>

                                                                                <option value="Transfer" {{ request()->get('work_status') == 'Transfer' ? 'selected' : '' }}>Transfer</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </mat-form-field>
                                                            </app-multi-select>

                                                        </div>
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio filter-pos ng-star-inserted mt-1">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">
                                                                <!----><span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Resume by</span>
                                                                <!---->
                                                            </h3>
                                                            <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494341" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494341-input" name="mat-radio-group-11494339" value="Direct" tabindex="0">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494341-input">Direct</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494342" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494342-input" name="mat-radio-group-11494339" value="Agency" tabindex="0">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494342-input">Agency</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                            </mat-radio-group>
                                                        </div>
                                                        <!---->
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">
                                                            <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">
                                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Language</h3>

                                                                <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                                    <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-9 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                        <!---->
                                                                        <div class="mat-mdc-text-field-wrapper">
                                                                            <div class="mat-mdc-form-field-flex">
                                                                                <select class="form-control" name="languages" onchange="this.form.submit()">
                                                                                    <option value="" disabled selected>Select Language</option>
                                                                                    @php
                                                                                        $languages = App\Models\Language::all();
                                                                                    @endphp
                                                                                    @foreach($languages as $language)
                                                                                        <option value="{{$language->id}}" {{ request()->get('languages') == $language->id ? 'selected' : '' }}>{{ $language->name }}</option>
                                                                                    @endforeach

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </mat-form-field>
                                                                </app-multi-select>

                                                            </div>
                                                            <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted mt-3">
                                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Main Skills</h3>

                                                                <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                                    <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-11 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                        <!---->
                                                                        <div class="mat-mdc-text-field-wrapper">
                                                                            <div class="mat-mdc-form-field-flex">
                                                                                <select class="form-control" name="main_skills" onchange="this.form.submit()">
                                                                                    <option value="" disabled selected>Select Main Skills</option>
                                                                                    @php
                                                                                        $main_skills = App\Models\MainSkill::all();
                                                                                    @endphp
                                                                                    @foreach($main_skills as $main_skill)
                                                                                        <option value="{{$main_skill->id}}" {{ request()->get('main_skills') == $main_skill->id ? 'selected' : '' }}>{{ $main_skill->name }}</option>
                                                                                    @endforeach

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </mat-form-field>
                                                                </app-multi-select>

                                                            </div>
                                                            <!---->
                                                        </div>
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted mt-3">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Nationality</h3>

                                                            <mat-form-field _ngcontent-serverapp-c2564575186="" appearance="outline" class="mat-mdc-form-field ng-tns-c1205077789-7 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                <!---->
                                                                <div class="mat-mdc-text-field-wrapper">
                                                                    <div class="mat-mdc-form-field-flex">
                                                                        <select class="form-control" name="nationality" onchange="this.form.submit()">
                                                                            <option value="" disabled selected>Select Location</option>
                                                                            @php
                                                                                $countries = App\Models\Country::all();
                                                                            @endphp
                                                                            @foreach($countries as $country)
                                                                                <option value="{{$country->id}}" {{ request()->get('nationality') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                            @endforeach

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </mat-form-field>

                                                        </div>
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-radio ng-star-inserted mt-3">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Gender</h3>

                                                            <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494344" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494344-input" name="gender" value="male" tabindex="0" {{ $genderApply == 'male' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494344-input">Male</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11494345" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494345-input" name="gender" value="female" tabindex="0" {{ $genderApply == 'female' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494345-input">Female</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                            </mat-radio-group>

                                                        </div>

                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Helper Name</h3>
                                                            <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                                <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="7">

                                                                    <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                                        <div class="mat-mdc-text-field-wrapper">
                                                                            <div class="mat-mdc-form-field-flex">
                                                                                <input class="form-control" type="text" name="middle_name" placeholder="Search with Helper Name" id="mat-input-1442183" value="{{ $middle_nameApply }}" onchange="this.form.submit()">
                                                                            </div>
                                                                        </div>
                                                                    </mat-form-field>

                                                                </app-date-picker>
                                                            </div>
                                                        </div>
                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Working Experience</h3>
                                                            <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                                <input type="text" class="working_experience" name="working_experience" id="working_experience" onchange="this.form.submit()"
                                                                       @if($working_experience)
                                                                           @php
                                                                               $experience = explode(';',$working_experience);
                                                                               $from = $experience['0'];
                                                                               $to = $experience['1'];
                                                                           @endphp
                                                                           data-from="{{$from}}"
                                                                       data-to="{{$to}}"
                                                                    @endif />
                                                            </div>
                                                        </div>
                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Age</h3>
                                                            <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                                <input type="text" class="age" name="age" id="age" onchange="this.form.submit()"  @if($age_filter)
                                                                    @php
                                                                        $age = explode(';',$age_filter);
                                                                        $from_age = $age['0'];
                                                                        $to_age = $age['1'];
                                                                    @endphp
                                                                    data-from="{{$from_age}}"
                                                                       data-to="{{$to_age}}"
                                                                    @endif />
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                                <!---->
                                            </div>
                                        </div>
                                    </div>
                                </app-filter>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
@push('js')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>


    <script>
        document.querySelectorAll('.nav-link').forEach(link => {
            link.addEventListener('mouseover', function() {
                this.style.backgroundColor = '#B50000';
                this.style.color = '#000';
                this.style.borderColor = '#B50000';
            });
            link.addEventListener('mouseout', function() {
                if (!this.classList.contains('active')) {
                    this.style.backgroundColor = '';
                    this.style.color = '#000';
                    this.style.borderColor = '#ddd';
                }
            });
        });

        // Ensure active state styles are applied
        document.querySelectorAll('.nav-link.active').forEach(link => {
            link.style.backgroundColor = '#B50000';
            link.style.color = 'white';
            link.style.borderColor = '#B50000';
        });

        // Apply responsive styles
        function applyResponsiveStyles() {
            if (window.innerWidth <= 768) {
                const navTabs = document.querySelector('.nav-tabs');
                const navItems = document.querySelectorAll('.nav-item');

                if (navTabs) {
                    navTabs.style.flexDirection = 'column';
                }

                navItems.forEach(item => {
                    item.style.marginBottom = '10px';
                    item.style.marginTop = '10px';
                });
            }
        }

        window.addEventListener('resize', applyResponsiveStyles);
        window.addEventListener('load', applyResponsiveStyles);
    </script>

    <script>
        $(document).ready(function() {
            toastr.options = {
                "positionClass": "toast-bottom-right",
                "timeOut": "5000",
                "closeButton": true,
                "progressBar": true
            };

            $('.custom-save-job').on('click', function(e) {
                e.preventDefault();
                var $this = $(this);
                var candidateId = $(this).attr('candidate-id');

                $.ajax({
                    url: '{{ route('shortlist_candidate') }}',
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        candidate_id: candidateId
                    },
                    success: function(response) {
                        if(response.favorited) {
                            $this.find('i.fa-bookmark').css('color', 'rgb(235, 186, 22)').addClass('shortlisted');
                        } else {
                            $this.find('i.fa-bookmark').css('color', 'rgb(255, 255, 255)').removeClass('shortlisted');
                        }

                        if (response.favorited !== undefined) {
                            showSuccessMessage(response.message);
                        } else {
                            showErrorMessage('Something went wrong.');
                        }
                    },
                    error: function(response) {
                        showErrorMessage('Error: ' + response.responseJSON.message);
                    }
                });
            });
            function showSuccessMessage(message) {
                toastr.success(message);
            }

            function showErrorMessage(message) {
                toastr.error(message);
            }
        });
    </script>
    <script>
        $(".working_experience").ionRangeSlider({
            type: "double",
            min: 1,
            max: 40,
        });
        @if($max_age && $max_age != 0)
        $(".age").ionRangeSlider({
            type: "double",
            min: 18,
            max: {{$max_age}},
        });
        @else
        $(".age").ionRangeSlider({
            type: "double",
            min: 18,
            max: 60,
        });
        @endif

    </script>
@endpush
