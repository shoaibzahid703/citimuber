@extends('frontend.layouts.app')
@section('title','PUBLIC HOLIDAY')

@section('content')
<style>
  .blog {
    margin-top: 97px;
  }
</style>

<section _ngcontent-serverapp-c2864336703 class="page-section">
  <section _ngcontent-serverapp-c2864336703 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider">
    <div _ngcontent-serverapp-c2864336703 class="container pb-4">
      <div _ngcontent-serverapp-c2864336703 class="row">
        <div _ngcontent-serverapp-c2864336703 class="col-12">
          <div _ngcontent-serverapp-c2864336703 class="top-banner-wrapper">
            <div _ngcontent-serverapp-c2864336703 class="top-banner-content small-section">
              <h1 _ngcontent-serverapp-c2864336703><span _ngcontent-serverapp-c2864336703>{{$public_holiday_page->first_heading}}</span></h1>
              <p _ngcontent-serverapp-c2864336703 style="height: auto;">{{$public_holiday_page->description}}</p>
            </div>
          </div>
        </div>
      </div>
      <div _ngcontent-serverapp-c2864336703 class="row mt-5">

        <div _ngcontent-serverapp-c2864336703 class="col-12 text-center">
          <form method="GET" action="{{ route('public_holiday') }}">
            <div _ngcontent-serverapp-c2864336703 class="custom-filter">
              <select _ngcontent-serverapp-c2864336703 placeholder="'public_holiday.select_country' | translate" class="selectpicker ng-untouched ng-pristine ng-valid" name="country_id" onchange="this.form.submit()">
                <option _ngcontent-serverapp-c2864336703 disabled selected value>-- Select --</option>
                @foreach($countries as $country)
                <option value="{{ $country->id }}" class="ng-star-inserted" {{ request('country_id') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                @endforeach
              </select>
            </div>
            <div _ngcontent-serverapp-c2864336703 class="custom-filter">
              <select _ngcontent-serverapp-c2864336703 placeholder="'public_holiday.select_year' | translate" class="selectpicker ng-untouched ng-pristine ng-valid" name="holiday_date" onchange="this.form.submit()">
                @php
                $years = $public_holidays->map(function($public_holiday) {
                return \Carbon\Carbon::parse($public_holiday->holiday_date)->format('Y');
                })->unique();
                @endphp
                <option _ngcontent-serverapp-c2864336703 disabled selected value>-- Select --</option>
                @foreach($years as $year)
                <option value="{{ $year }}" class="ng-star-inserted" {{ request('holiday_date') == $year ? 'selected' : '' }}>{{ $year }}</option>
                @endforeach
              </select>
            </div>
          </form>
        </div>

      </div>

    </div>
  </section>
  <section _ngcontent-serverapp-c2864336703 class="yoga event pb-5 hp-public-holiday public-holiday-ar">
    <div _ngcontent-serverapp-c2864336703 class="container">
      <div _ngcontent-serverapp-c2864336703 class="row">

        @foreach($public_holidays as $public_holiday)
        <div _ngcontent-serverapp-c2864336703 class="custom-media col-xl-4 col-lg-4 col-md-6 col-sm-12 mb-3 ng-star-inserted">
          <div _ngcontent-serverapp-c2864336703 class="holiday-wrapper">
            <div _ngcontent-serverapp-c2864336703 class="header-bg expired-holiday">
              <div _ngcontent-serverapp-c2864336703 class="media">
                <div _ngcontent-serverapp-c2864336703 class="mr-3 date-header">
                  <h2 _ngcontent-serverapp-c2864336703 class="date-title">{{ \Carbon\Carbon::parse($public_holiday->holiday_date)->format('d') }}</h2>
                  <div _ngcontent-serverapp-c2864336703 class="date-wrapper"><span _ngcontent-serverapp-c2864336703>{{ \Carbon\Carbon::parse($public_holiday->holiday_date)->format('F') }}</span><span _ngcontent-serverapp-c2864336703>{{ \Carbon\Carbon::parse($public_holiday->holiday_date)->format('Y') }}</span></div>
                </div>
                <div _ngcontent-serverapp-c2864336703 class="media-body">
                  <h3 _ngcontent-serverapp-c2864336703 class="mt-0 ar">{{$public_holiday->title}}</h3>
                </div>
              </div>
            </div>
            <div _ngcontent-serverapp-c2864336703 class="content-bg expired-content">
              <div _ngcontent-serverapp-c2864336703 class="holiday-description ar"><span _ngcontent-serverapp-c2864336703>{{$public_holiday->description}}</span></div>
            </div>
            <div _ngcontent-serverapp-c2864336703 class="hp-holiday-tagline ar expired-tagline">
              <h5 _ngcontent-serverapp-c2864336703><i _ngcontent-serverapp-c2864336703 aria-hidden="true" class="fa fa-flag mr-2"></i><span _ngcontent-serverapp-c2864336703 class="mr-2"> {{$public_holiday->holiday_type}} </span></h5>
            </div>
          </div>
        </div>
        @endforeach

        <!---->
      </div>
      <!---->
    </div>
  </section>
</section>

@endsection