@extends('frontend.layouts.app')
@section('title','JOBS')
@section('css')

@endsection
@section('content')

    <section _ngcontent-serverapp-c1132162511="" class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom-container page-section" style="margin-top: 140px !important;">
        <div _ngcontent-serverapp-c1132162511="" class="container">
            <div _ngcontent-serverapp-c1132162511="" class="">
                <div _ngcontent-serverapp-c1132162511="" class="row">
                    <div _ngcontent-serverapp-c1132162511="" class="col-12">
                        <div _ngcontent-serverapp-c1132162511="" class="top-banner-wrapper ng-star-inserted">
                            <div _ngcontent-serverapp-c1132162511="" class="top-banner-content small-section">
                                <h1 _ngcontent-serverapp-c1132162511=""><span _ngcontent-serverapp-c1132162511="">{{$jobs_page->first_heading}}</span></h1>
                                <p _ngcontent-serverapp-c1132162511="" class="header_2"> {{$jobs_page->paragraph}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div _ngcontent-serverapp-c1132162511="" class="row mt-3">
                <div _ngcontent-serverapp-c1132162511="" class="col-12">
                    <div _ngcontent-serverapp-c1132162511="" class="filter-sort-bar c-filter-bar filters-btn">
                        <div _ngcontent-serverapp-c1132162511="" class="filter-main-btn mb-0"><a _ngcontent-serverapp-c1132162511="" class="filter-button btn-sm"><i _ngcontent-serverapp-c1132162511="" class="fa fa-filter" data-toggle="modal" data-target="#fiterModal"></i>Filter </a></div>
                        <div _ngcontent-serverapp-c1132162511="" class="c-res-job-booster" style="gap: 10px;">
                            <div _ngcontent-serverapp-c1132162511="">
                                <app-order-by _ngcontent-serverapp-c1132162511="" _nghost-serverapp-c1558467617="" ngh="3">
                                    <div _ngcontent-serverapp-c1558467617="" class="dropdown-btn">
                                        <div _ngcontent-serverapp-c1558467617="" class="dropdown">
                                            <!--  <button _ngcontent-serverapp-c1558467617="" class="dropbtn">
                                               <span _ngcontent-serverapp-c1558467617="" class="mr-2"><i _ngcontent-serverapp-c1558467617="" aria-hidden="true" class="fa fa-sort-amount-desc"></i></span><span _ngcontent-serverapp-c1558467617="">Publish Date</span>
                                             </button> -->
                                            <!---->
                                        </div>
                                    </div>
                                </app-order-by>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div _ngcontent-serverapp-c1132162511="" class="row" style="clear: both;">
                <div _ngcontent-serverapp-c1132162511="" class="col-md-3 col-lg-3 d-none d-md-clock d-lg-block d-xl-block collection-filter-block ng-star-inserted">
                    <app-filter _ngcontent-serverapp-c1132162511="" _nghost-serverapp-c2564575186="" ngh="10">
                        <div _ngcontent-serverapp-c2564575186="" class="row product-service_1">
                            <div _ngcontent-serverapp-c2564575186="" class="col-md-12 col-lg-12">
                                <div _ngcontent-serverapp-c2564575186="" class="row filter-ar">
                                    <div _ngcontent-serverapp-c2564575186="" class="top-banner-content small-section col-12 ng-star-inserted">
                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container mt-3 mb-1 ng-star-inserted">
                                            <h2 _ngcontent-serverapp-c2564575186="" class="float-left custom_h2"><i _ngcontent-serverapp-c2564575186="" class="fa fa-serch"></i>{{$jobs_page->second_heading}} </h2>
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container mb-0 ng-star-inserted">
                                            <p _ngcontent-serverapp-c2564575186="" class="float-left filter_text"> Filter </p>
                                            <a href="{{route('jobs')}}">
                                                <p _ngcontent-serverapp-c2564575186="" class="text-success float-right reset_button reset-ar" style="cursor: pointer;"><i _ngcontent-serverapp-c2564575186="" class="fa fa-repeat"></i> Reset </p>
                                            </a>
                                        </div>
                                        <!---->
                                        <!---->
                                        <form method="GET" action="{{route('jobs')}}">
                                            <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">
                                                    <span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Job Location</span>
                                                    <!---->
                                                    <!---->
                                                </h3>
                                                <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                    <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                        <!---->
                                                        <div class="mat-mdc-text-field-wrapper">
                                                            <div class="mat-mdc-form-field-flex">
                                                                <select class="form-control" name="offer_location" onchange="this.form.submit()">
                                                                    <option value="" disabled selected>Select Location</option>
                                                                    @php
                                                                        $countries = App\Models\Country::all();
                                                                    @endphp
                                                                    @foreach($countries as $country)
                                                                        <option value="{{$country->id}}" {{ request()->get('offer_location') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>

                                                    </mat-form-field>
                                                </app-multi-select>
                                            </div>
                                            <!---->

                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted mt-3">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Position</h3>
                                                <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494346" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494346-input" name="position_offered" value="Domestic Helper" tabindex="0" {{ $position_offeredApply == 'Domestic Helper' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494346-input">Domestic Helper</label>
                                                        </div>
                                                    </mat-radio-button>
                                                    <!---->
                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494347" ngh="0">
                                                        <div class="mdc-form-field">
                                                            <div class="mdc-radio">
                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494347-input" name="position_offered" value="Driver" tabindex="0" {{ $position_offeredApply == 'Driver' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                <div class="mdc-radio__background">
                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                </div>
                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                </div>
                                                            </div>
                                                            <label class="mdc-label" for="mat-radio-11494347-input">Driver</label>
                                                        </div>
                                                    </mat-radio-button>
                                                    <!---->

                                                    <div _ngcontent-serverapp-c2564575186="" class="sidebar-container ng-star-inserted">
                                                        <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Start Date</h3>
                                                        <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="9">
                                                            <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                                <div class="mat-mdc-text-field-wrapper">
                                                                    <div class="mat-mdc-form-field-flex">
                                                                        <input class="form-control" type="date" name="job_start_date" placeholder="dd-mm-yyyy" id="mat-input-1442183" value="{{ $job_start_dateApply }}" onchange="this.form.submit()">
                                                                    </div>
                                                                </div>
                                                            </mat-form-field>
                                                        </app-date-picker>
                                                    </div>
                                                    <!---->

                                                    <!---->
                                                    <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted mt-3">
                                                        <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Type</h3>
                                                        <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                            <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494348" ngh="0">
                                                                <div class="mdc-form-field">
                                                                    <div class="mdc-radio">
                                                                        <div class="mat-mdc-radio-touch-target"></div>
                                                                        <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494348-input" name="job_type" value="Full Time" tabindex="0" {{ $jobtypeApply == 'Full Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                        <div class="mdc-radio__background">
                                                                            <div class="mdc-radio__outer-circle"></div>
                                                                            <div class="mdc-radio__inner-circle"></div>
                                                                        </div>
                                                                        <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                            <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                        </div>
                                                                    </div>
                                                                    <label class="mdc-label" for="mat-radio-11494348-input">Full Time</label>
                                                                </div>
                                                            </mat-radio-button>
                                                            <!---->
                                                            <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494349" ngh="0">
                                                                <div class="mdc-form-field">
                                                                    <div class="mdc-radio">
                                                                        <div class="mat-mdc-radio-touch-target"></div>
                                                                        <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494349-input" name="job_type" value="Part Time" tabindex="0" {{ $jobtypeApply == 'Part Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                        <div class="mdc-radio__background">
                                                                            <div class="mdc-radio__outer-circle"></div>
                                                                            <div class="mdc-radio__inner-circle"></div>
                                                                        </div>
                                                                        <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                            <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                        </div>
                                                                    </div>
                                                                    <label class="mdc-label" for="mat-radio-11494349-input">Part Time</label>
                                                                </div>
                                                            </mat-radio-button>
                                                            <!---->
                                                            <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494350" ngh="0">
                                                                <div class="mdc-form-field">
                                                                    <div class="mdc-radio">
                                                                        <div class="mat-mdc-radio-touch-target"></div>
                                                                        <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494350-input" name="job_type" value="Temporary" tabindex="0" {{ $jobtypeApply == 'Temporary' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                        <div class="mdc-radio__background">
                                                                            <div class="mdc-radio__outer-circle"></div>
                                                                            <div class="mdc-radio__inner-circle"></div>
                                                                        </div>
                                                                        <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                            <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                        </div>
                                                                    </div>
                                                                    <label class="mdc-label" for="mat-radio-11494350-input">Temporary</label>
                                                                </div>
                                                            </mat-radio-button>
                                                            <!---->
                                                            <!---->
                                                        </mat-radio-group>
                                                    </div>

                                                    <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-pos ng-star-inserted mt-3">
                                                        <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Contract Status</h3>
                                                        <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="5">
                                                            <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-4 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                <!---->
                                                                <div class="mat-mdc-text-field-wrapper">
                                                                    <div class="mat-mdc-form-field-flex">
                                                                        <select class="form-control" name="contract_status" onchange="this.form.submit()">
                                                                            <option value="" disabled selected>Select Status</option>

                                                                            <option value="">Select Option</option>
                                                                            <option value="any_situation" {{ request()->get('contract_status') == 'any_situation' ? 'selected' : '' }}>Any Situation</option>

                                                                            <option value="finished_contract" {{ request()->get('contract_status') == 'finished_contract' ? 'selected' : '' }}>Finished Contract</option>

                                                                            <option value="terminated_relocation_financial" {{ request()->get('contract_status') == 'terminated_relocation_financial' ? 'selected' : '' }}>Terminated (Relocation / Financial)</option>

                                                                            <option value="terminated_other" {{ request()->get('contract_status') == 'terminated_other' ? 'selected' : '' }}>Terminated (Other)</option>

                                                                            <option value="break_contract" {{ request()->get('contract_status') == 'break_contract' ? 'selected' : '' }}>Break Contract</option>
                                                                            <option value="transfer" {{ request()->get('contract_status') == 'transfer' ? 'selected' : '' }}>Transfer</option>
                                                                            <option value="overseas" {{ request()->get('contract_status') == 'overseas' ? 'selected' : '' }}>Overseas</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </mat-form-field>
                                                        </app-multi-select>
                                                    </div>

                                                    <!---->
                                                    <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted mt-3">
                                                        <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">
                                                            <span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Post by</span>
                                                        </h3>
                                                        <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                            <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11490996" ngh="0">
                                                                <div class="mdc-form-field">
                                                                    <div class="mdc-radio">
                                                                        <div class="mat-mdc-radio-touch-target"></div>
                                                                        <input type="radio" class="mdc-radio__native-control" id="mat-radio-11490996-input" name="mat-radio-group-11490994" value="Direct" tabindex="0">
                                                                        <div class="mdc-radio__background">
                                                                            <div class="mdc-radio__outer-circle"></div>
                                                                            <div class="mdc-radio__inner-circle"></div>
                                                                        </div>
                                                                        <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                            <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                        </div>
                                                                    </div>
                                                                    <label class="mdc-label" for="mat-radio-11490996-input">Direct</label>
                                                                </div>
                                                            </mat-radio-button>
                                                            <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11490997" ngh="0">
                                                                <div class="mdc-form-field">
                                                                    <div class="mdc-radio">
                                                                        <div class="mat-mdc-radio-touch-target"></div>
                                                                        <input type="radio" class="mdc-radio__native-control" id="mat-radio-11490997-input" name="mat-radio-group-11490994" value="Agency" tabindex="0">
                                                                        <div class="mdc-radio__background">
                                                                            <div class="mdc-radio__outer-circle"></div>
                                                                            <div class="mdc-radio__inner-circle"></div>
                                                                        </div>
                                                                        <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                            <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                        </div>
                                                                    </div>
                                                                    <label class="mdc-label" for="mat-radio-11490997-input">Agency</label>
                                                                </div>
                                                            </mat-radio-button>
                                                        </mat-radio-group>
                                                    </div>

                                                    <!---->
                                                    <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted ">
                                                        <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Language</h3>
                                                            <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="5">
                                                                <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-9 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                    <!---->
                                                                    <div class="mat-mdc-text-field-wrapper">
                                                                        <div class="mat-mdc-form-field-flex">
                                                                            <select class="form-control" name="languages" onchange="this.form.submit()">
                                                                                <option value="" disabled selected>Select Language</option>
                                                                                @php
                                                                                    $languages = App\Models\Language::all();
                                                                                @endphp
                                                                                @foreach($languages as $language)
                                                                                    <option value="{{$language->id}}" {{ request()->get('languages') == $language->id ? 'selected' : '' }}>{{ $language->name }}</option>
                                                                                @endforeach

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </mat-form-field>
                                                            </app-multi-select>
                                                        </div>
                                                        <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Main Skills</h3>
                                                            <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="5">
                                                                <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-11 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                    <!---->
                                                                    <div class="mat-mdc-text-field-wrapper">
                                                                        <div class="mat-mdc-form-field-flex">
                                                                            <select class="form-control" name="main_skills" onchange="this.form.submit()">
                                                                                <option value="" disabled selected>Select Main Skills</option>
                                                                                @php
                                                                                    $main_skills = App\Models\MainSkill::all();
                                                                                @endphp
                                                                                @foreach($main_skills as $main_skill)
                                                                                    <option value="{{$main_skill->id}}" {{ request()->get('main_skills') == $main_skill->id ? 'selected' : '' }}>{{ $main_skill->name }}</option>
                                                                                @endforeach

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </mat-form-field>
                                                            </app-multi-select>
                                                        </div>
                                                        <!---->
                                                    </div>
                                                </mat-radio-group>
                                            </div>

                                        </form>

                                    </div>
                                    <!---->
                                    <!---->
                                </div>
                            </div>
                        </div>
                    </app-filter>
                </div>
                <style type="text/css">
                    .nav .nav-link {
                        color: black !important;
                    }
                </style>
                <!---->
                <div _ngcontent-serverapp-c1132162511="" class="col-md-12 col-lg-9">
                    @if(Auth::check())
                        @if(Auth::user()->role == \App\Models\User::ROLE_CANDIDATE)
                            <div class="container">
                                <div class="row">
                                    <div class="col-12" style="padding: 0px;">
                                        <div class="nav nav-tabs justify-content-between" id="nav-tab" role="tablist" style="display: flex; justify-content: space-between; border-bottom: 1px solid #ddd;">
                                            <a class="nav-item nav-link @if(Request::url() == route('jobs')) active @endif" id="nav-home-tab" href="{{ route('jobs') }}" aria-controls="nav-home" aria-selected="true" style="flex: 1; color: black !important; border: 1px solid #ddd !important; border-radius: 4px; padding: 10px 20px; transition: background-color 0.3s, color 0.3s; text-decoration: none;">
                                                <i class="fa fa-home"></i> All
                                            </a>
                                            <a class="nav-item nav-link @if(Request::url() == route('recommended_jobs')) active @endif" id="nav-profile-tab" href="{{route('recommended_jobs')}}" aria-controls="nav-profile" aria-selected="false" style="flex: 1; color: black !important; border: 1px solid #ddd !important; border-radius: 4px; padding: 10px 20px; transition: background-color 0.3s, color 0.3s; text-decoration: none;">
                                                <i class="fa fa-user"></i> Recommended
                                            </a>
                                            <a class="nav-item nav-link @if(Request::url() == route('applicant_jobs')) active @endif" href="{{route('applicant_jobs')}}" aria-controls="nav-contact" aria-selected="false" style="flex: 1; color: black !important; border: 1px solid #ddd !important; border-radius: 4px; padding: 10px 20px; transition: background-color 0.3s, color 0.3s; text-decoration: none;">
                                                <i class="fa fa-cog"></i> Job Application
                                            </a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(Auth::check())
                        @if(Auth::user()->role == \App\Models\User::ROLE_CANDIDATE)
                            <div _ngcontent-serverapp-c1132162511="" class="collection-product-wrapper mt-5">
                                @endif
                                @else
                                    <div _ngcontent-serverapp-c1132162511="" class="collection-product-wrapper">
                                        @endif
                                        <div _ngcontent-serverapp-c1132162511="" class="product-wrapper-grid list-view">
                                            <!---->
                                            <div _ngcontent-serverapp-c1132162511="" class="ng-star-inserted">

                                                @if($jobs->isEmpty())
                                                    <div class="row">
                                                        <div class="col-12 mt-5">
                                                            <img class="mx-auto d-block" src="{{asset('cdn-sub/front-app/assets/images/misc/empty-search.jpg')}}">

                                                        </div>
                                                        <div class="col-10 offset-1">
                                                            <h4 class="text-center">To find available jobs, you just need to adjust your search criteria.</h4>

                                                        </div>
                                                    </div>
                                                @else

                                                    @foreach($jobs as $job)
                                                        <div _ngcontent-serverapp-c1132162511="" class="col-grid-box w-100 mt-3 mb-4 ng-star-inserted">
                                                            <job-detail-block _ngcontent-serverapp-c1132162511="" _nghost-serverapp-c3111442506="" ngh="11">
                                                                <div _ngcontent-serverapp-c3111442506="" class="product-box-container mb-4 ng-star-inserted">
                                                                    <div _ngcontent-serverapp-c3111442506="" class="product-box">
                                                                        <div _ngcontent-serverapp-c3111442506="" class="img-wrapper_Custom w-10">
                                                                            <a _ngcontent-serverapp-c3111442506="" routerlinkactive="router-link-active" href="{{route('job_details', $job)}}" onclick="logClick({{ $job->id }})">
                                                                                @if($job->job_image)
                                                                                    <div _ngcontent-serverapp-c3111442506="" class="front custom_front_rsume_image">
                                                                                        <picture _ngcontent-serverapp-c3111442506="">
                                                                                            <source _ngcontent-serverapp-c3111442506="" srcset="{{asset('storage/job_picture/'.$job->job_image->img)}}">
                                                                                            <img _ngcontent-serverapp-c3111442506="" loading="lazy" width="143px" height="143px" onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"  src="{{asset('assets/images/job/house_keeper.webp')}}">
                                                                                        </picture>
                                                                                    </div>
                                                                                @else
                                                                                    <div _ngcontent-serverapp-c3111442506="" class="front custom_front_rsume_image">
                                                                                        <picture _ngcontent-serverapp-c3111442506="">
                                                                                            <source _ngcontent-serverapp-c3111442506="" srcset="{{asset('assets/images/job/house_keeper.webp')}}">
                                                                                            <img _ngcontent-serverapp-c3111442506="" loading="lazy" width="143px" height="143px"   src="{{asset('assets/images/job/house_keeper.webp')}}">
                                                                                        </picture>
                                                                                    </div>
                                                                                @endif
                                                                            </a>
                                                                            <div _ngcontent-serverapp-c3111442506="" class="listing-sub-title-agency text-left w-20 mt-2">
                                                                                <label _ngcontent-serverapp-c3111442506="" class="label_blue">
                                                                                    @if($job->add_type == 'agency')
                                                                                      <span _ngcontent-serverapp-c3111442506="" class="ng-star-inserted">Agency</span>
                                                                                    @else
                                                                                        <span _ngcontent-serverapp-c3111442506="" class="ng-star-inserted">Direct</span>
                                                                                    @endif
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div _ngcontent-serverapp-c3111442506="" class="product-detail w-100 pt-2 align-self-baseline text-left">
                                                                            <a _ngcontent-serverapp-c3111442506="" routerlinkactive="router-link-active" href="{{route('job_details', $job)}}" onclick="logClick({{ $job->id }})">
                                                                                <!---->
                                                                                <h4 _ngcontent-serverapp-c3111442506="" class="product-title" style="word-break: break-word;"> {{$job->job_title}} </h4>
                                                                                <h5 _ngcontent-serverapp-c3111442506="" class="product-sub-title mt-1 mr-2">
                                                                                    <!----><span _ngcontent-serverapp-c3111442506="" class="ng-star-inserted"> {{ str_replace('_', ' ', $job->family_type) }} | {{$job->nationality->nationality ??''}}</span>
                                                                                    <!---->
                                                                                </h5>
                                                                                <h5 _ngcontent-serverapp-c3111442506="" class="product-sub-title mt-1">

                                                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                                    <span _ngcontent-serverapp-c3111442506="" class="location">{{$job->countryName->name ??''}}</span>
                                                                                </h5>
                                                                                <!---->
                                                                                <div _ngcontent-serverapp-c3111442506="" class="product-description mt-2" style="word-break: break-word;"> {!! $job->job_description ??'' !!}
                                                                                </div>
                                                                                <div _ngcontent-serverapp-c3111442506="" class="product-footer">
                                                                                    <h5 _ngcontent-serverapp-c3111442506="" class="footer-experience">

                                                                                        <i class="fa fa-cog" aria-hidden="true"></i> &nbsp;

                                                                                        {{ ucfirst(str_replace('_', ' ', $job->position_offered)) }}
                                                                                    </h5>
                                                                                    <h5 _ngcontent-serverapp-c3111442506="" class="footer-date">

                                                                                        <i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;
                                                                                        Before {{ \Carbon\Carbon::parse($job->job_start_date ?? '')->format('d F Y') }}
                                                                                    </h5>
                                                                                    <h5 _ngcontent-serverapp-c3111442506="" class="footer-active ng-star-inserted">

                                                                                        <i class="fa fa-circle" aria-hidden="true"></i> &nbsp;
                                                                                        <span _ngcontent-serverapp-c3111442506="" class="very-active">Very Active</span>
                                                                                    </h5>
                                                                                    <!---->
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    @if(Auth::check())
                                                                        @if(Auth::user()->role == \App\Models\User::ROLE_CANDIDATE)

                                                                            <div _ngcontent-serverapp-c3111442506="" class="row bg-primary footer-ar py-2 w-100 positions m-0 px-1 inherit-bottom ng-star-inserted" style="position: relative;">
                                                                                @php
                                                                                    $applied_time = App\Models\JobApply::where('user_id', Auth::user()->id)->where('job_id', $job->id)->exists();
                                                                                @endphp
                                                                                <div _ngcontent-serverapp-c3111442506="" class="col-12 px-1 text-right ng-star-inserted">
                                                                                    @if($applied_time)
                                                                                        <a _ngcontent-serverapp-c3111442506="" class="custom-save-job float-left ng-star-inserted" style="color: rgb(255, 255, 255);"> Applied </a>
                                                                                    @endif

                                                                                    @php
                                                                                        $created_at = \Carbon\Carbon::parse($job->created_at);
                                                                                         if ($created_at->diffInMinutes() < 60) {
                                                                                            $diffForHumans = 'a few minutes ago';
                                                                                            } else {
                                                                                                $diffForHumans = $created_at->diffForHumans();
                                                                                            }
                                                                                    @endphp
                                                                                    <i _ngcontent-serverapp-c3111442506="" class="fa fa-clock-o mr-1 ml-1 text-right"></i> Published {{ $diffForHumans }}
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </job-detail-block>
                                                        </div>
                                                    @endforeach
                                                @endif

                                            </div>
                                            <!---->
                                            <!---->
                                            <div _ngcontent-serverapp-c1132162511="" class="center-pagination ng-star-inserted">
                                                <app-pagination _ngcontent-serverapp-c1132162511="" _nghost-serverapp-c3388104089="" ngh="15">
                                                    {{ $jobs->links() }}
                                                </app-pagination>
                                            </div>
                                            <!---->
                                        </div>
                                    </div>
                            </div>
                </div>
                <div _ngcontent-serverapp-c1132162511="" class="container-fluid mt-4">
                    <div _ngcontent-serverapp-c1132162511="" class="row">
                        <div _ngcontent-serverapp-c1132162511="" class="col-12 mb-5 extra_details_bottom description_tag">
                            <p _ngcontent-serverapp-c1132162511="" style="word-break: break-word;">
                                {!! $jobs_page->description !!}
                            </p>
                        </div>
                    </div>
                </div>
                &gt;
            </div>
    </section>
    <ngx-ui-loader _ngcontent-serverapp-c1132162511="" fgssize="100" bgsopacity="2" overlaycolor="rgba(40,40,40,0.20)" fgscolor="#054a84" _nghost-serverapp-c1591448945="" ngh="16">
        <div _ngcontent-serverapp-c1591448945="" class="ngx-progress-bar ngx-progress-bar-ltr ng-star-inserted" style="height: 3px; color: #00ACC1;"></div>
        <!---->
        <div _ngcontent-serverapp-c1591448945="" class="ngx-overlay" style="background-color: rgba(40,40,40,0.20); border-radius: 0;">
            <!---->
            <div _ngcontent-serverapp-c1591448945="" class="ngx-foreground-spinner center-center" style="color: #054a84; width: 100px; height: 100px; top: 50%;">
                <div _ngcontent-serverapp-c1591448945="" class="sk-ball-spin-clockwise ng-star-inserted">
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                    <!---->
                </div>
                <!---->
                <!---->
            </div>
            <div _ngcontent-serverapp-c1591448945="" class="ngx-loading-text center-center" style="top: 50%; color: #FFFFFF;">
      <span _ngcontent-serverapp-c1591448945="">
        <!--ngetn-->
      </span>
            </div>
        </div>
        <div _ngcontent-serverapp-c1591448945="" class="ngx-background-spinner bottom-right" style="width: 60px; height: 60px; color: #00ACC1; opacity: 2;">
            <div _ngcontent-serverapp-c1591448945="" class="sk-ball-spin-clockwise ng-star-inserted">
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <div _ngcontent-serverapp-c1591448945="" class="ng-star-inserted"></div>
                <!---->
            </div>
            <!---->
            <!---->
        </div>
    </ngx-ui-loader>

    <div class="modal fade" id="fiterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog mt-5" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="pt-2 font-weight-bold">Filter</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row pb-3">
                        <div class="col-12">
                            <div _ngcontent-serverapp-c3093988918="" class="  d-md-clock d-lg-block d-xl-block collection-filter-block ng-star-inserted">
                                <app-filter _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c2564575186="" ngh="10">
                                    <div _ngcontent-serverapp-c2564575186="" class="row product-service_1">
                                        <div _ngcontent-serverapp-c2564575186="" class="col-md-12 col-lg-12">
                                            <div _ngcontent-serverapp-c2564575186="" class="row filter-ar">
                                                <!---->
                                                <div _ngcontent-serverapp-c2564575186="" class="top-banner-content small-section col-12 ng-star-inserted pt-4">

                                                    <form method="GET" action="{{route('jobs')}}">
                                                        <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">
                                                                <span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Job Location</span>
                                                                <!---->
                                                                <!---->
                                                            </h3>
                                                            <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                                <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                    <!---->
                                                                    <div class="mat-mdc-text-field-wrapper">
                                                                        <div class="mat-mdc-form-field-flex">
                                                                            <select class="form-control" name="offer_location" onchange="this.form.submit()">
                                                                                <option value="" disabled selected>Select Location</option>
                                                                                @php
                                                                                    $countries = App\Models\Country::all();
                                                                                @endphp
                                                                                @foreach($countries as $country)
                                                                                    <option value="{{$country->id}}" {{ request()->get('offer_location') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                                @endforeach

                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </mat-form-field>
                                                            </app-multi-select>
                                                        </div>
                                                        <!---->

                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted mt-3">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Position</h3>
                                                            <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494346" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494346-input" name="position_offered" value="Domestic Helper" tabindex="0" {{ $position_offeredApply == 'Domestic Helper' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494346-input">Domestic Helper</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                                <!---->
                                                                <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494347" ngh="0">
                                                                    <div class="mdc-form-field">
                                                                        <div class="mdc-radio">
                                                                            <div class="mat-mdc-radio-touch-target"></div>
                                                                            <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494347-input" name="position_offered" value="Driver" tabindex="0" {{ $position_offeredApply == 'Driver' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                            <div class="mdc-radio__background">
                                                                                <div class="mdc-radio__outer-circle"></div>
                                                                                <div class="mdc-radio__inner-circle"></div>
                                                                            </div>
                                                                            <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                            </div>
                                                                        </div>
                                                                        <label class="mdc-label" for="mat-radio-11494347-input">Driver</label>
                                                                    </div>
                                                                </mat-radio-button>
                                                                <!---->

                                                                <div _ngcontent-serverapp-c2564575186="" class="sidebar-container ng-star-inserted">
                                                                    <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Start Date</h3>
                                                                    <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="9">
                                                                        <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                                            <div class="mat-mdc-text-field-wrapper">
                                                                                <div class="mat-mdc-form-field-flex">
                                                                                    <input class="form-control" type="date" name="job_start_date" placeholder="dd-mm-yyyy" id="mat-input-1442183" value="{{ $job_start_dateApply }}" onchange="this.form.submit()">
                                                                                </div>
                                                                            </div>
                                                                        </mat-form-field>
                                                                    </app-date-picker>
                                                                </div>
                                                                <!---->

                                                                <!---->
                                                                <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted mt-3">
                                                                    <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Type</h3>
                                                                    <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494348" ngh="0">
                                                                            <div class="mdc-form-field">
                                                                                <div class="mdc-radio">
                                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494348-input" name="job_type" value="Full Time" tabindex="0" {{ $jobtypeApply == 'Full Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                                    <div class="mdc-radio__background">
                                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                                    </div>
                                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <label class="mdc-label" for="mat-radio-11494348-input">Full Time</label>
                                                                            </div>
                                                                        </mat-radio-button>
                                                                        <!---->
                                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494349" ngh="0">
                                                                            <div class="mdc-form-field">
                                                                                <div class="mdc-radio">
                                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494349-input" name="job_type" value="Part Time" tabindex="0" {{ $jobtypeApply == 'Part Time' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                                    <div class="mdc-radio__background">
                                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                                    </div>
                                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <label class="mdc-label" for="mat-radio-11494349-input">Part Time</label>
                                                                            </div>
                                                                        </mat-radio-button>
                                                                        <!---->
                                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494350" ngh="0">
                                                                            <div class="mdc-form-field">
                                                                                <div class="mdc-radio">
                                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494350-input" name="job_type" value="Temporary" tabindex="0" {{ $jobtypeApply == 'Temporary' ? 'checked' : '' }} onchange="this.form.submit()">
                                                                                    <div class="mdc-radio__background">
                                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                                    </div>
                                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <label class="mdc-label" for="mat-radio-11494350-input">Temporary</label>
                                                                            </div>
                                                                        </mat-radio-button>
                                                                        <!---->
                                                                        <!---->
                                                                    </mat-radio-group>
                                                                </div>

                                                                <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-pos ng-star-inserted mt-3">
                                                                    <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Contract Status</h3>
                                                                    <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="5">
                                                                        <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-4 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                            <!---->
                                                                            <div class="mat-mdc-text-field-wrapper">
                                                                                <div class="mat-mdc-form-field-flex">
                                                                                    <select class="form-control" name="contract_status" onchange="this.form.submit()">
                                                                                        <option value="" disabled selected>Select Status</option>

                                                                                        <option value="">Select Option</option>
                                                                                        <option value="any_situation" {{ request()->get('contract_status') == 'any_situation' ? 'selected' : '' }}>Any Situation</option>

                                                                                        <option value="finished_contract" {{ request()->get('contract_status') == 'finished_contract' ? 'selected' : '' }}>Finished Contract</option>

                                                                                        <option value="terminated_relocation_financial" {{ request()->get('contract_status') == 'terminated_relocation_financial' ? 'selected' : '' }}>Terminated (Relocation / Financial)</option>

                                                                                        <option value="terminated_other" {{ request()->get('contract_status') == 'terminated_other' ? 'selected' : '' }}>Terminated (Other)</option>

                                                                                        <option value="break_contract" {{ request()->get('contract_status') == 'break_contract' ? 'selected' : '' }}>Break Contract</option>
                                                                                        <option value="transfer" {{ request()->get('contract_status') == 'transfer' ? 'selected' : '' }}>Transfer</option>
                                                                                        <option value="overseas" {{ request()->get('contract_status') == 'overseas' ? 'selected' : '' }}>Overseas</option>

                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </mat-form-field>
                                                                    </app-multi-select>
                                                                </div>

                                                                <!---->
                                                                <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted mt-3">
                                                                    <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">
                                                                        <span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Post by</span>
                                                                    </h3>
                                                                    <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11490996" ngh="0">
                                                                            <div class="mdc-form-field">
                                                                                <div class="mdc-radio">
                                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11490996-input" name="mat-radio-group-11490994" value="Direct" tabindex="0">
                                                                                    <div class="mdc-radio__background">
                                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                                    </div>
                                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <label class="mdc-label" for="mat-radio-11490996-input">Direct</label>
                                                                            </div>
                                                                        </mat-radio-button>
                                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable" id="mat-radio-11490997" ngh="0">
                                                                            <div class="mdc-form-field">
                                                                                <div class="mdc-radio">
                                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11490997-input" name="mat-radio-group-11490994" value="Agency" tabindex="0">
                                                                                    <div class="mdc-radio__background">
                                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                                    </div>
                                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <label class="mdc-label" for="mat-radio-11490997-input">Agency</label>
                                                                            </div>
                                                                        </mat-radio-button>
                                                                    </mat-radio-group>
                                                                </div>

                                                                <!---->
                                                                <div _ngcontent-serverapp-c2564575186="" class="ng-star-inserted ">
                                                                    <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                                        <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Language</h3>
                                                                        <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="5">
                                                                            <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-9 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                                <!---->
                                                                                <div class="mat-mdc-text-field-wrapper">
                                                                                    <div class="mat-mdc-form-field-flex">
                                                                                        <select class="form-control" name="languages" onchange="this.form.submit()">
                                                                                            <option value="" disabled selected>Select Language</option>
                                                                                            @php
                                                                                                $languages = App\Models\Language::all();
                                                                                            @endphp
                                                                                            @foreach($languages as $language)
                                                                                                <option value="{{$language->id}}" {{ request()->get('languages') == $language->id ? 'selected' : '' }}>{{ $language->name }}</option>
                                                                                            @endforeach

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </mat-form-field>
                                                                        </app-multi-select>
                                                                    </div>
                                                                    <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                                        <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Main Skills</h3>
                                                                        <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="5">
                                                                            <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-11 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                                <!---->
                                                                                <div class="mat-mdc-text-field-wrapper">
                                                                                    <div class="mat-mdc-form-field-flex">
                                                                                        <select class="form-control" name="main_skills" onchange="this.form.submit()">
                                                                                            <option value="" disabled selected>Select Main Skills</option>
                                                                                            @php
                                                                                                $main_skills = App\Models\MainSkill::all();
                                                                                            @endphp
                                                                                            @foreach($main_skills as $main_skill)
                                                                                                <option value="{{$main_skill->id}}" {{ request()->get('main_skills') == $main_skill->id ? 'selected' : '' }}>{{ $main_skill->name }}</option>
                                                                                            @endforeach

                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </mat-form-field>
                                                                        </app-multi-select>
                                                                    </div>
                                                                    <!---->
                                                                </div>
                                                            </mat-radio-group>
                                                        </div>

                                                    </form>

                                                </div>
                                                <!---->
                                            </div>
                                        </div>
                                    </div>
                                </app-filter>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        document.querySelectorAll('.nav-link').forEach(link => {
            link.addEventListener('mouseover', function() {
                this.style.backgroundColor = '#B50000';
                this.style.color = 'white';
                this.style.borderColor = '#B50000';
            });
            link.addEventListener('mouseout', function() {
                if (!this.classList.contains('active')) {
                    this.style.backgroundColor = '';
                    this.style.color = 'black';
                    this.style.borderColor = '#ddd';
                }
            });
        });

        // Ensure active state styles are applied
        document.querySelectorAll('.nav-link.active').forEach(link => {
            link.style.backgroundColor = '#B50000';
            link.style.color = 'white';
            link.style.borderColor = '#B50000';
        });

        // Apply responsive styles
        function applyResponsiveStyles() {
            if (window.innerWidth <= 768) {
                const navTabs = document.querySelector('.nav-tabs');
                const navItems = document.querySelectorAll('.nav-item');

                if (navTabs) {
                    navTabs.style.flexDirection = 'column';
                }

                navItems.forEach(item => {
                    item.style.marginBottom = '10px';
                    item.style.marginTop = '10px';
                });
            }
        }

        window.addEventListener('resize', applyResponsiveStyles);
        window.addEventListener('load', applyResponsiveStyles);
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script>
        function logClick(jobId) {
            console.log('job id is: ',jobId);
            $.ajax({
                url: '{{ route('log-click') }}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    job_id: jobId,
                },
                success: function(response) {
                    console.log('Click logged successfully');
                },
                error: function(response) {
                    console.log('Error logging click');
                }
            });
        }
    </script>
@endsection
