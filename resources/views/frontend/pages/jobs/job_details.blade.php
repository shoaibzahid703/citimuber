@extends('frontend.layouts.app')
@section('title','JOB DETAILS - '.$job->job_title)

@section('content')
    <style>
        .job-view-ar{
            margin-top: 97px;
        }


        .modal-header img {
            max-width: 150px;
            height: auto;
        }

        .modal-body {
            padding: 20px 30px;
        }


    </style>

    <section _ngcontent-serverapp-c2001901274="" class="page-section job-view-ar ng-tns-c2001901274-1">
        <section _ngcontent-serverapp-c2001901274="" class="agency breadcrumb-section-main inner-3 breadcrumb-title ng-tns-c2001901274-1 ng-star-inserted" style="background: #f8f8f8 !important;">
            <div _ngcontent-serverapp-c2001901274="" class="container py-4 ng-tns-c2001901274-1 ng-star-inserted">
                <div _ngcontent-serverapp-c2001901274="" class="row px-3 ng-tns-c2001901274-1">
                    <div _ngcontent-serverapp-c2001901274="" class="col-12 ng-tns-c2001901274-1">
                        <div _ngcontent-serverapp-c2001901274="" class="breadcrumb-contain ng-tns-c2001901274-1">
                            <div _ngcontent-serverapp-c2001901274="" class="text-left header-icon ng-tns-c2001901274-1">
                                <ul _ngcontent-serverapp-c2001901274="" class="ar ng-tns-c2001901274-1">
                                    <li _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                        <a _ngcontent-serverapp-c2001901274="" routerlinkactive="router-link-active" class="ng-tns-c2001901274-1 router-link-active" href="/">
                                            <app-icons _ngcontent-serverapp-c2001901274="" name="home" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                    <use _ngcontent-serverApp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#home"></use>
                                                </svg>
                                            </app-icons>
                                        </a>
                                    </li>
                                    <li _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                        <a _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1" href="/jobs">
                                            <app-icons _ngcontent-serverapp-c2001901274="" name="double-arrow-right" _nghost-serverapp-c4219164779="" class="icons ng-star-inserted" ngh="0">
                                                <i class="fa fa-home" aria-hidden="true" style="color: #1E3F66"></i>
                                                <i class="fa fa-angle-double-right" aria-hidden="true" style="color: #1E3F66"></i>
                                            </app-icons>
                                            <!----><!----><!----> Find Job
                                        </a>
                                    </li>
                                    <!-- <li _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                        <a _ngcontent-serverapp-c2001901274="" href="javascript:;" class="ng-tns-c2001901274-1 ng-star-inserted">
                                            <app-icons _ngcontent-serverapp-c2001901274="" name="double-arrow-right" _nghost-serverapp-c4219164779="" class="icons ng-star-inserted" ngh="0">
                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                    <use _ngcontent-serverApp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#double-arrow-right"></use>
                                                </svg>
                                            </app-icons>
                                             Hong Kong
                                        </a>

                                    </li>
                                    <li _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                        <a _ngcontent-serverapp-c2001901274="" href="javascript:;" class="ng-tns-c2001901274-1 ng-star-inserted">
                                            <app-icons _ngcontent-serverapp-c2001901274="" name="double-arrow-right" _nghost-serverapp-c4219164779="" class="icons ng-star-inserted" ngh="0">
                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                    <use _ngcontent-serverApp-c4219164779="" href="https://www.helperplace.com/assets/icons/custom.svg#double-arrow-right"></use>
                                                </svg>
                                            </app-icons>
                                             Domestic Helper
                                        </a>

                                    </li> -->
                                </ul>
                            </div>
                            <div _ngcontent-serverapp-c2001901274="" class="d-none d-md-block d-lg-block d-xl-block d-sm-none ng-tns-c2001901274-1 ng-star-inserted">
                                <!---->
                            </div>
                            <!---->
                        </div>
                    </div>
                </div>
            </div>
            <!----><!---->
        </section>
        <!----><!---->
        <section _ngcontent-serverapp-c2001901274="" class="agency blog blog-sec blog-sidebar pb-5 blog-list sider ng-tns-c2001901274-1 ng-star-inserted">
            <div _ngcontent-serverapp-c2001901274="" class="container ng-tns-c2001901274-1">
                <!----><!---->
                <div _ngcontent-serverapp-c2001901274="" class="row ng-tns-c2001901274-1">
                    <div _ngcontent-serverapp-c2001901274="" class="col-md-12 col-lg-9 ng-tns-c2001901274-1">
                        <div _ngcontent-serverapp-c2001901274="" class="product-wrapper-grid list-view ng-tns-c2001901274-1">
                            <div _ngcontent-serverapp-c2001901274="" class="row ng-tns-c2001901274-1">
                                <div _ngcontent-serverapp-c2001901274="" class="col-12 col-12_padding_Set ng-tns-c2001901274-1">
                                    <div _ngcontent-serverapp-c2001901274="" class="agency-box ng-tns-c2001901274-1">
                                        <div _ngcontent-serverapp-c2001901274="" class="text-left row company_tabs_header_fix_size font-weight-normal ng-tns-c2001901274-1">
                                            <div _ngcontent-serverapp-c2001901274="" class="col-grid-box col-12 ng-tns-c2001901274-1">
                                                <div _ngcontent-serverapp-c2001901274="" class="front custom_front_rsume_image profile-preview ng-tns-c2001901274-1 ng-star-inserted">
                                                    <app-img-preview _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1" _nghost-serverapp-c1281504779="" ngh="3">
                                                        <!---->
                                                        @if($job->job_image)
                                                            <div _ngcontent-serverapp-c3111442506="" class="front custom_front_rsume_image">
                                                                <picture _ngcontent-serverapp-c3111442506="">
                                                                    <source _ngcontent-serverapp-c3111442506="" srcset="{{asset('storage/job_picture/'.$job->job_image->img)}}">
                                                                    <img _ngcontent-serverapp-c3111442506="" loading="lazy" width="143px" height="143px" onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;"  src="{{asset('assets/images/job/house_keeper.webp')}}">
                                                                </picture>
                                                            </div>
                                                        @else
                                                            <div _ngcontent-serverapp-c3111442506="" class="front custom_front_rsume_image">
                                                                <picture _ngcontent-serverapp-c3111442506="">
                                                                    <source _ngcontent-serverapp-c3111442506="" srcset="{{asset('assets/images/job/house_keeper.webp')}}">
                                                                    <img _ngcontent-serverapp-c3111442506="" loading="lazy" width="143px" height="143px"   src="{{asset('assets/images/job/house_keeper.webp')}}">
                                                                </picture>
                                                            </div>
                                                        @endif
                                                    </app-img-preview>
                                                </div>
                                                <!----><!---->
                                                <div _ngcontent-serverapp-c2001901274="" class="listing-sub-title-agency text-left mt-2 mb-2 mr-3 float-left ng-tns-c2001901274-1">
                                                    <label _ngcontent-serverapp-c2001901274="" class="label_blue ng-tns-c2001901274-1 ng-star-inserted">
                                                        @if($job->add_type == 'agency')
                                                            <span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted"> Agency </span>
                                                        @else
                                                            <span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted"> Direct </span>
                                                        @endif
                                                    </label>
                                                    <!---->
                                                    <div _ngcontent-serverapp-c2001901274="" class="mt-1 ng-tns-c2001901274-1">
                                                        <!---->
                                                    </div>
                                                </div>
                                                <!----><span _ngcontent-serverapp-c2001901274="" class="top-active text-left ng-tns-c2001901274-1 ng-star-inserted"> Very Active </span><!---->
                                                <div _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted">
                                                    @if($candidate_page->emp_detail_page_image)
                                                        <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted">
                                                            <source _ngcontent-serverapp-c2001901274="" media="(max-width:576px)" class="ng-tns-c2001901274-1" srcset="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->emp_detail_page_image}}">
                                                            <source _ngcontent-serverapp-c2001901274="" media="(max-width:768px)" class="ng-tns-c2001901274-1" srcset="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->emp_detail_page_image}}">
                                                            <source _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1" srcset="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->emp_detail_page_image}}">
                                                            <img _ngcontent-serverapp-c2001901274="" fetchpriority="high" loading="eager" rel="preload" alt="Trustworthy helper,maid &amp; drivers hiring" onerror="this.onerror = null;this.parentNode.children[0].srcset =
                                                         this.parentNode.children[1].srcset =this.parentNode.children[2].srcset=this.src;" class="agency_header w-10 responsive-img ng-tns-c2001901274-1" id="imgWestern family looking for helper to assist Elderly father." src="{{asset('storage/candidate_detail_page_image')}}/{{@$candidate_page->emp_detail_page_image}}">
                                                        </picture>
                                                    @else
                                                        <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted">
                                                            <source _ngcontent-serverapp-c2001901274="" media="(max-width:576px)" class="ng-tns-c2001901274-1" srcset="{{asset('cdn-sub/front-app/assets/images/misc/1-sm.webp')}}">
                                                            <source _ngcontent-serverapp-c2001901274="" media="(max-width:768px)" class="ng-tns-c2001901274-1" srcset="{{asset('cdn-sub/front-app/assets/images/misc/1-md.webp')}}">
                                                            <source _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1" srcset="{{asset('cdn-sub/front-app/assets/images/misc/1-lg.webp')}}">
                                                            <img _ngcontent-serverapp-c2001901274="" fetchpriority="high" loading="eager" rel="preload" alt="Trustworthy helper,maid &amp; drivers hiring" onerror="this.onerror = null;this.parentNode.children[0].srcset =
                                                         this.parentNode.children[1].srcset =this.parentNode.children[2].srcset=this.src;" class="agency_header w-10 responsive-img ng-tns-c2001901274-1" id="imgWestern family looking for helper to assist Elderly father." src="{{asset('cdn-sub/front-app/assets/images/misc/7-lg.webp')}}">
                                                        </picture>
                                                    @endif
                                                    <!----><!---->
                                                    <div _ngcontent-serverapp-c2001901274="" class="agency_header_White_opcity ng-tns-c2001901274-1"></div>
                                                </div>
                                                <!----><!---->
                                                <div _ngcontent-serverapp-c2001901274="" class="product-box ng-tns-c2001901274-1">
                                                    <div _ngcontent-serverapp-c2001901274="" class="product-detail w-100 ng-tns-c2001901274-1 ng-star-inserted">
                                                        <h1 _ngcontent-serverapp-c2001901274="" class="mb-0 listing-about-title p-0 ng-tns-c2001901274-1" style="word-break: break-word;"> {{$job->job_title}} </h1>
                                                        <div _ngcontent-serverapp-c2001901274="" class="hp-candidate-wrapper ng-tns-c2001901274-1">
                                                            <div _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <p _ngcontent-serverapp-c2001901274="" class="mb-2 p-0 text-left listing-about-sub-title ng-tns-c2001901274-1 ng-star-inserted">
                                                                    {{$job->nationality->nationality ??''}} <span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted">|</span><!----> Family | <span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted"> with {{ str_replace('_', ' ', $job->family_type) }}</span><!----><!----><!----><span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted"></span><!---->
                                                                </p>
                                                                <!----><!---->
                                                                @php
                                                                    $created_at = \Carbon\Carbon::parse($job->created_at);
                                                                        if ($created_at->diffInMinutes() < 60) {
                                                                          $diffForHumans = 'a few minutes ago';
                                                                          } else {
                                                                              $diffForHumans = $created_at->diffForHumans();
                                                                          }
                                                                @endphp
                                                                <p _ngcontent-serverapp-c2001901274="" class="mb-2 p-0 text-left listing-about-sub-title ng-tns-c2001901274-1 ng-star-inserted"> Posted: {{ $diffForHumans }} </p>
                                                                <!---->
                                                            </div>

                                                            @if(Auth::check())
                                                                @if(Auth::user()->role == \App\Models\User::ROLE_CANDIDATE)
                                                                    <div _ngcontent-serverapp-c2001901274="" class="apply-btn ng-tns-c2001901274-1">
                                                                        @php
                                                                            $exist_job = App\Models\JobApply::where('user_id', Auth::user()->id)->where('job_id', $job->id)->exists();

                                                                        @endphp
                                                                        @if($exist_job)
                                                                            <button _ngcontent-serverapp-c2001901274="" type="button" class="btn btn-primary hp-apply-btn ng-tns-c2001901274-1 ng-star-inserted"> Applied </button>
                                                                        @else

                                                                            <button _ngcontent-serverapp-c2001901274="" type="button" class="btn btn-primary hp-apply-btn ng-tns-c2001901274-1 ng-star-inserted"  data-toggle="modal" data-target="#applyModal" data-job-id="{{ $job->id }}"> Apply </button>
                                                                        @endif
                                                                    </div>
                                                                @endif
                                                                @if(Auth::user()->role == \App\Models\User::ROLE_AGENCY)
                                                                    <button _ngcontent-serverapp-c2001901274=""
                                                                            type="button"
                                                                            class="btn btn-primary hp-apply-btn ng-tns-c2001901274-1 ng-star-inserted"
                                                                            data-toggle="modal"
                                                                            data-target="#messageModal"
                                                                    > Contact </button>
                                                                @endif
                                                            @else
                                                                <div _ngcontent-serverapp-c2001901274="" class="apply-btn ng-tns-c2001901274-1">
                                                                    <button _ngcontent-serverapp-c2001901274="" type="button" class="btn btn-primary hp-apply-btn ng-tns-c2001901274-1 ng-star-inserted"  data-toggle="modal" data-target="#loginModal"> Apply </button>

                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c2001901274="" class="row custom-header-box ng-tns-c2001901274-1">
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 mx-auto ng-tns-c2001901274-1">
                                                <h2 _ngcontent-serverapp-c2001901274="" class="text-light ar ng-tns-c2001901274-1">Job Requirement</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c2001901274="" class="row py-3 job-requirement ng-tns-c2001901274-1">
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 mx-auto ng-tns-c2001901274-1">
                                                <div _ngcontent-serverapp-c2001901274="" class="row ar ng-tns-c2001901274-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c2001901274="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c2001901274-1 ng-star-inserted">
                                                        <h3 _ngcontent-serverapp-c2001901274="" class="footer-experience ng-tns-c2001901274-1">
                                                            <app-icons _ngcontent-serverapp-c2001901274="" name="user" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#user"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-cog" aria-hidden="true" style="color: #1E3F66"></i> &nbsp;
                                                            {{$job->position_offered}}  <span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted"> |</span><!----><span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted"> {{$job->job_type}} </span><!---->
                                                        </h3>
                                                    </div>
                                                    <!---->
                                                    <div _ngcontent-serverapp-c2001901274="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c2001901274-1 ng-star-inserted">
                                                        <h3 _ngcontent-serverapp-c2001901274="" class="footer-experience ng-tns-c2001901274-1">
                                                            <app-icons _ngcontent-serverapp-c2001901274="" name="calendar" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#calendar"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-calendar" aria-hidden="true" style="color: #1E3F66"></i>
                                                            Start Before: {{ \Carbon\Carbon::parse($job->resume_detail->job_start_date ?? '')->format('d F Y') }}
                                                        </h3>
                                                    </div>
                                                    <!---->
                                                    <div _ngcontent-serverapp-c2001901274="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c2001901274-1 ng-star-inserted">
                                                        <h3 _ngcontent-serverapp-c2001901274="" class="footer-experience ng-tns-c2001901274-1">
                                                            <app-icons _ngcontent-serverapp-c2001901274="" name="map" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#map"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-map-marker" aria-hidden="true" style="color: #1E3F66"></i>&nbsp;&nbsp;
                                                            {{$job->countryName->name ??''}}
                                                        </h3>
                                                    </div>
                                                    <!---->
                                                    <div _ngcontent-serverapp-c2001901274="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c2001901274-1 ng-star-inserted">
                                                        <h3 _ngcontent-serverapp-c2001901274="" class="footer-experience ng-tns-c2001901274-1">
                                                            <app-icons _ngcontent-serverapp-c2001901274="" name="file" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="16" width="16" href="https://www.helperplace.com/assets/icons/custom.svg#file"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-building" aria-hidden="true" style="color: #1E3F66"></i>&nbsp;
                                                            Contract Status: {{ ucfirst(str_replace('_', ' ', $job->contract_status)) }}
                                                        </h3>
                                                    </div>
                                                    <!----><!---->
                                                    <!-- <div _ngcontent-serverapp-c2001901274="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c2001901274-1 ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2001901274="" class="footer-experience ng-tns-c2001901274-1">
                                                                <app-icons _ngcontent-serverapp-c2001901274="" name="certificate" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                    <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                        <use _ngcontent-serverApp-c4219164779="" height="15" width="15" href="https://www.helperplace.com/assets/icons/custom.svg#certificate"></use>
                                                                    </svg>
                                                                </app-icons>
                                                                Required Experience : <span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted"> </span><span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">  {{ str_replace(';', ' - ', $job->preferred_experience) }} Years </span>
                                                            </h3>
                                                        </div> -->
                                                    <!---->
                                                    <div _ngcontent-serverapp-c2001901274="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c2001901274-1 ng-star-inserted">
                                                        <h3 _ngcontent-serverapp-c2001901274="" class="footer-experience ng-tns-c2001901274-1">
                                                            <app-icons _ngcontent-serverapp-c2001901274="" name="bed-person" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="16" width="16" href="https://www.helperplace.com/assets/icons/custom.svg#bed-person"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-bed" aria-hidden="true" style="color: #1E3F66"></i>&nbsp;&nbsp;
                                                            Accommodation: {{ ucfirst(str_replace('_', ' ', $job->accomodation)) }}
                                                        </h3>
                                                    </div>
                                                    <!---->
                                                    <div _ngcontent-serverapp-c2001901274="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c2001901274-1 ng-star-inserted">
                                                        <h3 _ngcontent-serverapp-c2001901274="" class="footer-experience ng-tns-c2001901274-1">
                                                            <app-icons _ngcontent-serverapp-c2001901274="" name="day_off" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="15" width="15" href="https://www.helperplace.com/assets/icons/custom.svg#day_off"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-calendar" aria-hidden="true" style="color: #1E3F66"></i>
                                                            Day Off: <span _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted">{{ ucfirst(str_replace('_', ' ', $job->day_off)) }}</span><!----><!---->
                                                        </h3>
                                                    </div>
                                                    <!---->
                                                </div>
                                                <!----><!---->
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c2001901274="" class="row custom-header-box ng-tns-c2001901274-1">
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 mx-auto ng-tns-c2001901274-1">
                                                <h2 _ngcontent-serverapp-c2001901274="" class="text-light ar ng-tns-c2001901274-1">Job Description</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c2001901274="" class="row ng-tns-c2001901274-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 mx-auto mt-3 jOb_description description_tag ar ng-tns-c2001901274-1">
                                                <div _ngcontent-serverapp-c2001901274="" style="word-break: break-word;" class="ng-tns-c2001901274-1">
                                                    <p>&nbsp;</p>
                                                    <p class="p1">{!! $job->job_description !!}</p>

                                                </div>
                                            </div>
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c2001901274="" class="row custom-header-box ng-tns-c2001901274-1">
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 mx-auto ng-tns-c2001901274-1">
                                                <h2 _ngcontent-serverapp-c2001901274="" class="text-light ar ng-tns-c2001901274-1">Required Skills / Duties</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c2001901274="" class="row yoga mt-3 event ng-tns-c2001901274-1 ng-star-inserted">
                                            @php
                                                $user_detail = App\Models\Job::pluck('languages');
                                                $data_exploade = explode(',', $user_detail[0]);
                                                $languages = App\Models\Language::whereIn('id', $data_exploade)->get();
                                            @endphp
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 ar ng-tns-c2001901274-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c2001901274="" class="col-11 phone_pedding mx-auto ng-tns-c2001901274-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c2001901274="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c2001901274-1">
                                                        <div _ngcontent-serverapp-c2001901274="" class="yoga-circle ng-tns-c2001901274-1">
                                                            <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <source _ngcontent-serverapp-c2001901274="" width="35px" height="35px" media="(max-width:992px)" class="ng-tns-c2001901274-1" srcset="{{asset('cdn-sub/skill_cat/1_1599642484.webp')}}">
                                                                <img _ngcontent-serverapp-c2001901274="" width="40px" height="40px" class="ng-tns-c2001901274-1" src="{{asset('cdn-sub/skill_cat/1_1599642484.webp')}}" alt="{{asset('cdn-sub/skill_cat/1_1599642484.webp')}}">
                                                            </picture>
                                                        </div>
                                                        <div _ngcontent-serverapp-c2001901274="" class="event-info ng-tns-c2001901274-1">
                                                            <h3 _ngcontent-serverapp-c2001901274="" class="primary ar ng-tns-c2001901274-1"> Language: </h3>
                                                            @foreach($languages as $language)
                                                                <h4 _ngcontent-serverapp-c2001901274="" class="float-left float-left-ar ng-tns-c2001901274-1 color_1 ng-star-inserted"> {{$language->name}} </h4>
                                                            @endforeach

                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 ar ng-tns-c2001901274-1 ng-star-inserted">
                                                @php
                                                    $user_detail = App\Models\Job::pluck('main_skills');
                                                    $data_exploade = explode(',', $user_detail[0]);
                                                    $main_skills = App\Models\MainSkill::whereIn('id', $data_exploade)->get();
                                                @endphp
                                                <div _ngcontent-serverapp-c2001901274="" class="col-11 phone_pedding mx-auto ng-tns-c2001901274-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c2001901274="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c2001901274-1">
                                                        <div _ngcontent-serverapp-c2001901274="" class="yoga-circle ng-tns-c2001901274-1">
                                                            <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <source _ngcontent-serverapp-c2001901274="" width="35px" height="35px" media="(max-width:992px)" class="ng-tns-c2001901274-1" srcset="{{asset('cdn-sub/skill_cat/4_1599643968.webp')}}">
                                                                <img _ngcontent-serverapp-c2001901274="" width="40px" height="40px" class="ng-tns-c2001901274-1" src="{{asset('cdn-sub/skill_cat/4_1599643968.webp')}}" alt="">
                                                            </picture>
                                                        </div>
                                                        <div _ngcontent-serverapp-c2001901274="" class="event-info ng-tns-c2001901274-1">
                                                            <h3 _ngcontent-serverapp-c2001901274="" class="primary ar ng-tns-c2001901274-1"> Main Skills: </h3>
                                                            @foreach($main_skills as $main_skill)
                                                                <h4 _ngcontent-serverapp-c2001901274="" class="float-left float-left-ar ng-tns-c2001901274-1 color_2 ng-star-inserted"> {{$main_skill->name}} </h4>
                                                            @endforeach
                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 ar ng-tns-c2001901274-1 ng-star-inserted">
                                                @php
                                                    $user_detail = App\Models\Job::pluck('cooking_skills');
                                                    $data_exploade = explode(',', $user_detail[0]);
                                                    $cooking_skills = App\Models\CookingSkill::whereIn('id', $data_exploade)->get();
                                                @endphp
                                                <div _ngcontent-serverapp-c2001901274="" class="col-11 phone_pedding mx-auto ng-tns-c2001901274-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c2001901274="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c2001901274-1">
                                                        <div _ngcontent-serverapp-c2001901274="" class="yoga-circle ng-tns-c2001901274-1">
                                                            <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <source _ngcontent-serverapp-c2001901274="" width="35px" height="35px" media="(max-width:992px)" class="ng-tns-c2001901274-1" srcset="https://cdn.helperplace.com/skill_cat/2_1599644151.webp">
                                                                <img _ngcontent-serverapp-c2001901274="" width="40px" height="40px" class="ng-tns-c2001901274-1" src="https://cdn.helperplace.com/skill_cat/2_1599644151.webp" alt="https://cdn.helperplace.com/skill_cat/2_1599644151.webp">
                                                            </picture>
                                                        </div>
                                                        <div _ngcontent-serverapp-c2001901274="" class="event-info ng-tns-c2001901274-1">
                                                            <h3 _ngcontent-serverapp-c2001901274="" class="primary ar ng-tns-c2001901274-1"> Cooking Skills: </h3>
                                                            @foreach($cooking_skills as $cooking_skill)
                                                                <h4 _ngcontent-serverapp-c2001901274="" class="float-left float-left-ar ng-tns-c2001901274-1 color_3 ng-star-inserted"> {{$cooking_skill->name}} </h4>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 ar ng-tns-c2001901274-1 ng-star-inserted">
                                                @php
                                                    $user_detail = App\Models\Job::pluck('other_skills');
                                                    $data_exploade = explode(',', $user_detail[0]);
                                                    $other_skills = App\Models\OtherSkill::whereIn('id', $data_exploade)->get();
                                                @endphp
                                                <div _ngcontent-serverapp-c2001901274="" class="col-11 phone_pedding mx-auto ng-tns-c2001901274-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c2001901274="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c2001901274-1">
                                                        <div _ngcontent-serverapp-c2001901274="" class="yoga-circle ng-tns-c2001901274-1">
                                                            <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <source _ngcontent-serverapp-c2001901274="" width="35px" height="35px" media="(max-width:992px)" class="ng-tns-c2001901274-1" srcset="{{asset('cdn-sub/skill_cat/3_1599642528.webp')}}">
                                                                <img _ngcontent-serverapp-c2001901274="" width="40px" height="40px" class="ng-tns-c2001901274-1" src="{{asset('cdn-sub/skill_cat/3_1599642528.webp')}}" alt="{{asset('cdn-sub/skill_cat/3_1599642528.webp')}}">
                                                            </picture>
                                                        </div>

                                                        <div _ngcontent-serverapp-c2001901274="" class="event-info ng-tns-c2001901274-1">
                                                            <h3 _ngcontent-serverapp-c2001901274="" class="primary ar ng-tns-c2001901274-1"> Other Skills: </h3>
                                                            @foreach($other_skills as $other_skill)
                                                                <h4 _ngcontent-serverapp-c2001901274="" class="float-left float-left-ar ng-tns-c2001901274-1 color_4 ng-star-inserted"> {{$other_skill->name}}  </h4>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c2001901274="" class="col-11 ar ng-tns-c2001901274-1 ng-star-inserted">
                                                <!---->
                                            </div>
                                            <!---->
                                        </div>
                                        <!----><!---->
                                    </div>
                                    <span _ngcontent-serverapp-c2001901274="" class="float-right float-left-ar primary pointer mr-2 ml-2 ng-tns-c2001901274-1"> Report this Job </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div _ngcontent-serverapp-c2001901274="" class="col-md-12 col-lg-3 d-md-block d-lg-block d-xl-block collection-filter-block resume-right-block ng-tns-c2001901274-1">
                        <!---->
                        <div _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1 ng-star-inserted">

                            @php
                                $related_jobs = App\Models\Job::where('position_offered', $job->position_offered ??'')
                                ->where('job_type', $job->job_type)
                                ->orderBy('created_at', 'desc')
                                ->take(10)
                                ->get();
                            @endphp

                            @if($related_jobs->isNotEmpty())
                                @foreach($related_jobs as $related_job)
                                    <div _ngcontent-serverapp-c2001901274="" style="width: 100%;" class="ng-tns-c2001901274-1 ng-star-inserted">
                                        <div _ngcontent-serverapp-c2001901274="" class="blog-agency ng-tns-c2001901274-1">
                                            <div _ngcontent-serverapp-c2001901274="" class="blog-contain ng-tns-c2001901274-1">
                                                <div _ngcontent-serverapp-c2001901274="" class="img-container ng-tns-c2001901274-1">
                                                    <a _ngcontent-serverapp-c2001901274="" class="blog-info-resume ng-tns-c2001901274-1" href="{{route('job_details', $related_job)}}">

                                                        @if($related_job->job_image)
                                                            <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <source _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1" srcset="{{asset('storage/job_picture/'.$related_job->job_image->img)}}">
                                                                <img _ngcontent-serverapp-c2001901274="" width="70px" height="70px" onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;" class="related-profile-picture ng-tns-c2001901274-1" src="{{asset('assets/images/job/house_keeper.webp')}}" >
                                                            </picture>
                                                        @else
                                                            <picture _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                                                <source _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1" srcset="{{asset('assets/images/job/house_keeper.webp')}}">
                                                                <img _ngcontent-serverapp-c2001901274="" width="70px" height="70px" onerror="this.onerror = null;this.parentNode.children[0].srcset = this.src;" class="related-profile-picture ng-tns-c2001901274-1" src="{{asset('assets/images/job/house_keeper.webp')}}" >
                                                            </picture>
                                                        @endif

                                                        <div _ngcontent-serverapp-c2001901274="" class="ar ng-tns-c2001901274-1">
                                                            <h4 _ngcontent-serverapp-c2001901274="" class="blog-head fixed-1-line ng-tns-c2001901274-1">{{ Str::words($related_job->job_title, 15) }}</h4>
                                                            <p _ngcontent-serverapp-c2001901274="" class="card-text ng-tns-c2001901274-1"><span _ngcontent-serverapp-c2001901274="" class="related-employer-type ng-tns-c2001901274-1">{{$related_job->employer_type}}</span> </p>
                                                            <p _ngcontent-serverapp-c2001901274="" class="card-text ng-tns-c2001901274-1 ng-star-inserted">

                                                                <i class="fa fa-map-marker" aria-hidden="true" style="color: #1E3F66"></i>
                                                                {{$related_job->countryName->name ??''}}
                                                            </p>
                                                            <!---->
                                                            <p _ngcontent-serverapp-c2001901274="" class="card-text ng-tns-c2001901274-1">

                                                                <i class="fa fa-cog" aria-hidden="true" style="color: #1E3F66"></i>
                                                                {{$related_job->position_offered}} | {{$related_job->job_type}}
                                                            </p>
                                                            <p _ngcontent-serverapp-c2001901274="" class="card-text ng-tns-c2001901274-1">

                                                                <i class="fa fa-calendar" aria-hidden="true" style="color: #1E3F66"></i>
                                                                From {{ \Carbon\Carbon::parse($related_job->job_start_date ?? '')->format('d F Y') }}
                                                            </p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!----><!---->
        <div _ngcontent-serverapp-c2001901274="" id="footer" class="footer_custom mt-5 pl-4 mobile_sticky permenant-sticky ng-tns-c2001901274-1 ng-star-inserted">
            <div _ngcontent-serverapp-c2001901274="" class="container ng-tns-c2001901274-1">
                <div _ngcontent-serverapp-c2001901274="" class="row ng-tns-c2001901274-1">
                    <div _ngcontent-serverapp-c2001901274="" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 res-footer ng-tns-c2001901274-1">
                        <div _ngcontent-serverapp-c2001901274="" class="footer-apply-content ng-tns-c2001901274-1">
                            <div _ngcontent-serverapp-c2001901274="" class="left ng-tns-c2001901274-1">
                                <div _ngcontent-serverapp-c2001901274="" class="left-wrapper ng-tns-c2001901274-1 ng-star-inserted">


                                    <span _ngcontent-serverapp-c2001901274="" class="left-content-text ng-tns-c2001901274-1">Apply for job</span>

                                    @if(Auth::check())
                                        @if(Auth::user()->role == \App\Models\User::ROLE_CANDIDATE)

                                            @php
                                                $exist_job = App\Models\JobApply::where('user_id', Auth::user()->id)->where('job_id', $job->id)->exists();
                                            @endphp
                                            @if($exist_job)

                                                <button _ngcontent-serverapp-c2001901274="javascript" type="button" class="btn btn-primary ng-tns-c2001901274-1 ng-star-inserted"> Applied </button>
                                            @else

                                                <button _ngcontent-serverapp-c2001901274="" type="button" class="btn btn-primary ng-tns-c2001901274-1 ng-star-inserted" data-toggle="modal" data-target="#applyModal" data-job-id="{{ $job->id }}"> Apply </button>
                                            @endif
                                        @endif
                                    @else
                                        <button _ngcontent-serverapp-c2001901274="" type="button" class="btn btn-primary ng-tns-c2001901274-1 ng-star-inserted" data-toggle="modal" data-target="#loginModal"> Apply </button>


                                    @endif
                                </div>
                                <!----><!---->
                            </div>
                        </div>
                    </div>
                    <div _ngcontent-serverapp-c2001901274="" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 res-footer ng-tns-c2001901274-1">
                        <div _ngcontent-serverapp-c2001901274="" class="footer-apply-content right-content ng-tns-c2001901274-1">
                            <div _ngcontent-serverapp-c2001901274="" class="right ng-tns-c2001901274-1">
                                <div _ngcontent-serverapp-c2001901274="" class="right-wrapper ng-tns-c2001901274-1">
                                    <span _ngcontent-serverapp-c2001901274="" class="left-content-text ng-tns-c2001901274-1">Share</span>
                                    <ul _ngcontent-serverapp-c2001901274="" class="social-sharing ng-tns-c2001901274-1">
                                        <li _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                            <a _ngcontent-serverapp-c2001901274="" href="javascript:;" class="ng-tns-c2001901274-1">
                                                <i _ngcontent-serverapp-c2001901274="" aria-hidden="true" class="ng-tns-c2001901274-1">
                                                    <app-icons _ngcontent-serverapp-c2001901274="" name="facebook" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                        <svg _ngcontent-serverApp-c4219164779="" class="icons" height="15" width="17">
                                                            <use _ngcontent-serverApp-c4219164779="" height="15" width="15" href="https://www.helperplace.com/assets/icons/custom.svg#facebook"></use>
                                                        </svg>
                                                    </app-icons>
                                                </i>
                                            </a>
                                        </li>
                                        <li _ngcontent-serverapp-c2001901274="" class="ng-tns-c2001901274-1">
                                            <a _ngcontent-serverapp-c2001901274="" href="javascript:;" class="ng-tns-c2001901274-1">
                                                <i _ngcontent-serverapp-c2001901274="" aria-hidden="true" class="ng-tns-c2001901274-1">
                                                    <app-icons _ngcontent-serverapp-c2001901274="" name="whatsapp" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                        <svg _ngcontent-serverApp-c4219164779="" class="icons" height="18" width="17">
                                                            <use _ngcontent-serverApp-c4219164779="" height="16" width="16" href="https://www.helperplace.com/assets/icons/custom.svg#whatsapp"></use>
                                                        </svg>
                                                    </app-icons>
                                                </i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->
    </section>




    <div class="modal fade" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="margin-top:70px">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="{{asset('assets/images/site-logo.jpg')}}" class="w-50">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('job_apply')}}">
                        @csrf
                        <div class="row pb-3">
                            <div class="col-12">
                                <form method="post" action="">
                                    @csrf
                                    <input type="hidden" name="job_id" id="job_id">

                                    <p class="p-3" style="font-size: 18px !important;">Ready to apply for this job ? This employer will receive immediately your resume!</p>

                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn login_btn btn-sm text-uppercase" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn register_btn btn-sm my-2 my-sm-0 text-uppercase">Confirm</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="{{ asset('assets/images/site-logo.jpg') }}" class="w-50">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mt-2">
                        <div class="col-12">
                            <form class="helper-form mt-3" method="post" action="{{ route('send_helper_message',$job->userDetail) }}">
                                @csrf


                                <div class="form-group">
                                    <label for="message" style="color:black !important;">Send Message to {{$job->userDetail->name}}</label>
                                    <textarea name="message" required class="form-control" id="message" aria-describedby="emailHelp" autocomplete="off" placeholder="message"></textarea>
                                    <div id="message-error" class="invalid-feedback"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background-color: #000; color:white;">Close</button>
                                    <button type="submit" class="btn btn-primary" style="background-color: #B50000; color:white;">Start Chat</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>--}}
    {{--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>--}}
    {{--    <script>--}}
    {{--        document.addEventListener('DOMContentLoaded', function() {--}}
    {{--            @if (session('success'))--}}
    {{--            toastr.success('{{ session('success') }}', 'Success', {--}}
    {{--                positionClass: 'toast-bottom-right',--}}
    {{--                timeOut: 5000--}}
    {{--            });--}}
    {{--            @endif--}}
    {{--        });--}}
    {{--    </script>--}}
@endsection
@push('js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#applyModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var jobId = button.data('job-id');
                var modal = $(this);
                modal.find('#job_id').val(jobId);
            });
        });
    </script>
@endpush
