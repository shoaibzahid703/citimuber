@extends('frontend.layouts.app')
@section('title','NEWS')

@section('content')
<style>
  .blog-ar {
    margin-top: 97px;
  }
</style>

<section _ngcontent-serverapp-c3727738402 class="agency news-section pb-5 custom-container page-section blog-ar">
  <div _ngcontent-serverapp-c3727738402 class="container">
    <div _ngcontent-serverapp-c3727738402 class="row">
      <div _ngcontent-serverapp-c3727738402 class="col-12">
        <ul _ngcontent-serverapp-c3727738402 class="nav nav-tabs custom-tab">
          <a href="/news" style="width: 50%;">
            <li style="width: 100%;" class="active"><i class="fa fa-newspaper-o"></i><span>News</span></li>
          </a>
          <a href="/tips" style="width: 50%;">
            <li style="width: 100%;" class><i class="fa fa-lightbulb-o"></i><span>Tips</span></li>
          </a>
        </ul>
      </div>
    </div>
    <div _ngcontent-serverapp-c3727738402 class="tips-news-section ng-star-inserted">
      <app-news _ngcontent-serverapp-c3727738402 _nghost-serverapp-c1381573822 ngh="4">
        <section _ngcontent-serverapp-c1381573822 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider mt-4">
          <div _ngcontent-serverapp-c1381573822 class="row">
            <div _ngcontent-serverapp-c1381573822 class="col-12">
              <div _ngcontent-serverapp-c1381573822 class="top-banner-wrapper">
                <div _ngcontent-serverapp-c1381573822 class="top-banner-content small-section">
                  <h1 _ngcontent-serverapp-c1381573822><span _ngcontent-serverapp-c1381573822>{{$news_page->first_heading}}</span></h1>
                  <p _ngcontent-serverapp-c1381573822 class="header_2 mt-2">{{$news_page->paragraph}} </p>
                </div>
              </div>
            </div>
          </div>
          <div _ngcontent-serverapp-c1381573822 class="row mt-3 pl-0 pr-0">
            <div _ngcontent-serverapp-c1381573822 class="col-lg-9">
              <div _ngcontent-serverapp-c1381573822 class="ng-star-inserted">
                <div _ngcontent-serverapp-c1381573822 class="row">

                  @foreach($all_news as $news)
                  <div _ngcontent-serverapp-c1381573822 class="col-md-6 ng-star-inserted">
                    <div _ngcontent-serverapp-c1381573822 class="blog-agency">
                      <div _ngcontent-serverapp-c1381573822 class="blog-contain">
                        <a _ngcontent-serverapp-c1381573822 routerlinkactive="router-link-active" href="{{route('news_details', $news)}}">
                          <div _ngcontent-serverapp-c1381573822 class="Blog_list_img_size">
                            <picture _ngcontent-serverapp-c1381573822>
                              <source _ngcontent-serverapp-c1381573822 width="276px" height="172px" media="(max-width:325px)" class="img-fluid custom_img ng-star-inserted" srcset="{{asset('storage/news_image')}}/{{$news->news_image}}">
                              <!---->
                              <source _ngcontent-serverapp-c1381573822 width="376px" height="235px" media="(max-width:425px)" class="img-fluid custom_img ng-star-inserted" srcset="{{asset('storage/news_image')}}/{{$news->news_image}}">
                              <!---->
                              <source _ngcontent-serverapp-c1381573822 width="486px" height="305px" media="(max-width:535px)" class="img-fluid custom_img ng-star-inserted" srcset="{{asset('storage/news_image')}}/{{$news->news_image}}">
                              <!---->
                              <source _ngcontent-serverapp-c1381573822 width="508px" height="318px" media="(max-width:768px)" class="img-fluid custom_img ng-star-inserted" srcset="{{asset('storage/news_image')}}/{{$news->news_image}}">
                              <!---->
                              <source _ngcontent-serverapp-c1381573822 width="328px" height="205px" media="(max-width:1200px)" class="img-fluid custom_img ng-star-inserted" srcset="{{asset('storage/news_image')}}/{{$news->news_image}}">
                              <!----><img _ngcontent-serverapp-c1381573822 loading="lazy" width="396px" height="247px" class="img-fluid custom_img ng-star-inserted" alt="Hiring Part-Time or Full-Time Helper in Singapore" src="{{asset('storage/news_image')}}/{{$news->news_image}}">
                            </picture>
                            <span _ngcontent-serverapp-c1381573822 class="d-none">{{$news->title}}</span>
                          </div>
                        </a>
                        <div _ngcontent-serverapp-c1381573822 class="img-container">
                          <div _ngcontent-serverapp-c1381573822 class="blog-info">
                            <div _ngcontent-serverapp-c1381573822 class="m-b-20">
                              <div _ngcontent-serverapp-c1381573822 class="center-text">
                                <p _ngcontent-serverapp-c1381573822 class="mr-5 font-blog"><i _ngcontent-serverapp-c1381573822 aria-hidden="true" class="fa fa-calendar"></i>{{ \Carbon\Carbon::parse($news->news_date ?? '')->format('d F Y') }} </p>

                                @php
                                                                                        $created_at = \Carbon\Carbon::parse($news->created_at);
                                                                                         if ($created_at->diffInMinutes() < 60) {
                                                                                            $diffForHumans = 'a few minutes ago';
                                                                                            } else {
                                                                                                $diffForHumans = $created_at->diffForHumans();
                                                                                            }
                                                                                    @endphp


                                 <p _ngcontent-serverapp-c1381573822 class="font-blog font_location"><i _ngcontent-serverapp-c1381573822 aria-hidden="true" class="fa fa-clock-o"></i>{{ $diffForHumans }} </p>
                              </div>
                            </div>
                            <a _ngcontent-serverapp-c1381573822 href="{{route('news_details', $news)}}">
                              <h2 _ngcontent-serverapp-c1381573822 class="blog-head font-600">{{$news->title}}</h2>
                            </a>
                            <textarea _ngcontent-serverapp-c1381573822 rows="5" cols="14" readonly name="seo_metadata" class="para2" style="word-break: break-word;">{{$news->description}}</textarea>
                            <div _ngcontent-serverapp-c1381573822 class="mt-1 text-center"><a _ngcontent-serverapp-c1381573822 class="btn btn-sucesss btn-sm btm text-uppercase" href="{{route('news_details', $news)}}">READ MORE</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  @endforeach

                  <!---->
                </div>
                <div _ngcontent-serverapp-c1381573822 class="center-pagination">
                  <app-pagination>
                    <ul class="pagination">

                      @if ($all_news->onFirstPage())
                      <li class="page-item disabled">
                        <a class="prev page-link" href="#" aria-disabled="true">«</a>
                      </li>
                      @else
                      <li class="page-item">
                        <a class="prev page-link" href="{{ $candidates->previousPageUrl() }}" rel="prev"></a>
                      </li>
                      @endif

                      @foreach ($all_news->links()->elements as $element)

                      @if (is_string($element))
                      <li class="page-item disabled"><a class="page-link" href="#">{{ $element }}</a></li>
                      @endif

                      @if (is_array($element))
                      @foreach ($element as $page => $url)
                      @if ($page == $all_news->currentPage())
                      <li class="page-item active"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                      @else
                      <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                      @endif
                      @endforeach
                      @endif
                      @endforeach

                      @if ($all_news->hasMorePages())
                      <li class="page-item">
                        <a class="next page-link" href="{{ $candidates->nextPageUrl() }}" rel="next">»</a>
                      </li>
                      @else
                      <li class="page-item disabled">
                        <a class="next page-link" href="#" aria-disabled="true">»</a>
                      </li>
                      @endif
                    </ul>
                  </app-pagination>
                </div>
              </div>
              <!---->
              <!---->
              <!---->
            </div>
            <div _ngcontent-serverapp-c1381573822 class="col-lg-3">
              <div _ngcontent-serverapp-c1381573822 class="blog-side custom-right-side">
                <div _ngcontent-serverapp-c1381573822 class="ar-right">
                  <span _ngcontent-serverapp-c1381573822 class="blog-title">categories</span>
                  <div _ngcontent-serverapp-c1381573822 class="sidebar-container borders">
                    <ul _ngcontent-serverapp-c1381573822 class="sidebar-list">

                      @foreach($all_categories as $category)
                      <li _ngcontent-serverapp-c1381573822 class="d-flex ng-star-inserted" tabindex="0"><a _ngcontent-serverapp-c1381573822 href="javascript:;"><i _ngcontent-serverapp-c1381573822 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i>{{$category->name}}</a></li>
                      @endforeach

                    </ul>
                  </div>
                </div>
                <div _ngcontent-serverapp-c1381573822 class="sidebar-container ar-right">
                  <label _ngcontent-serverapp-c1381573822 for="blog_search" class="blog-title">Specific Search</label>
                  <div _ngcontent-serverapp-c1381573822 class="newsletter text-center form">
                    <div _ngcontent-serverapp-c1381573822 class="form-group"><input _ngcontent-serverapp-c1381573822 type="search" name="blog_search" id="blog_search" class="form-control" placeholder="Keywords..."><a _ngcontent-serverapp-c1381573822 href="/news"><i _ngcontent-serverapp-c1381573822 aria-hidden="true" class="fa fa-search"></i><span _ngcontent-serverapp-c1381573822 class="d-none">fa fa-search in helperplace</span></a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </app-news>
    </div>
  </div>
</section>

@endsection