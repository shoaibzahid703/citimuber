@extends('frontend.layouts.app')
@section('title','NEWS DETAILS')

@section('content')
<style>
  .blog-detail-ar {
    margin-top: 97px;
  }
</style>

<section _ngcontent-serverapp-c101221944 class="mt_4_custom agency blog blog-sec blog-sidebar pb-5 blog-list sider page-section ng-star-inserted">
  <div _ngcontent-serverapp-c101221944 class="container">
    <div _ngcontent-serverapp-c101221944 class="row mt-5 pt-4 blog-detail-ar">
      <div _ngcontent-serverapp-c101221944 class="col-lg-9">
        <div _ngcontent-serverapp-c101221944 class="blog-block m-b-20">
          <div _ngcontent-serverapp-c101221944 class="blog-box" style="min-height: 250px;">
            <div _ngcontent-serverapp-c101221944 class="overflow-hidden text-center ng-star-inserted">
              <img _ngcontent-serverapp-c101221944 title="CitiMuber blog lazyload image" class="img-fluid blur-up lazyload" alt="Hiring Part-Time or Full-Time Helper in Singapore" src="{{asset('storage/news_image')}}/{{$news->news_image}}">
            </div>
            <!---->
            <!---->
          </div>
        </div>
        <div _ngcontent-serverapp-c101221944 class="row mb-2">
          <div _ngcontent-serverapp-c101221944 class="blog-text px-2">
            <h1 _ngcontent-serverapp-c101221944 class="blog-head ml-2 mr-2 w-90 ar ng-star-inserted">{{$news->title}}</h1>
            <!---->
            <!---->
            <div _ngcontent-serverapp-c101221944 class="col-3 col-md-1 custom_desktop_profile mt-3 pr-1 col-lg-1 float-left">
              <div _ngcontent-serverapp-c101221944 class="yoga-circle">
                  <img _ngcontent-serverapp-c101221944 loading="lazy" alt="{{asset('storage/news_image')}}/{{$news->location_image}}" src="{{asset('storage/news_image')}}/{{$news->location_image}}">
                  </div>
            </div>
            <div _ngcontent-serverapp-c101221944 class="col-9 py-3 col-md-6 pl-1 col-lg-6 float-left text-left auther_name">
              <p _ngcontent-serverapp-c101221944 class="h5 text-left">CitiMuber</p>

              @php
                                                                                        $created_at = \Carbon\Carbon::parse($news->created_at);
                                                                                         if ($created_at->diffInMinutes() < 60) {
                                                                                            $diffForHumans = 'a few minutes ago';
                                                                                            } else {
                                                                                                $diffForHumans = $created_at->diffForHumans();
                                                                                            }
                                                                                    @endphp
               <p _ngcontent-serverapp-c101221944 class="h6 text-left ng-star-inserted">{{ \Carbon\Carbon::parse($news->news_date ?? '')->format('d F Y') }} | {{ $diffForHumans }} </p>
                <!---->
                <!---->
            </div>
            <div _ngcontent-serverapp-c101221944 class="col-12 col-md-5 col-lg-5 pt_3_custom float-left pt-3">
              <div _ngcontent-serverapp-c101221944 class="typography-box float-left-ar">
                <div _ngcontent-serverapp-c101221944 class="typo-content">
                  <div _ngcontent-serverapp-c101221944 class="socials-lists">
                    <ul _ngcontent-serverapp-c101221944 class="socials-horizontal ng-star-inserted">
                      <li _ngcontent-serverapp-c101221944><a _ngcontent-serverapp-c101221944 href="{{$news->facebook_url}}" title="Share on facebook"><i _ngcontent-serverapp-c101221944 aria-hidden="true" class="fa fa-facebook center-content"></i><span _ngcontent-serverapp-c101221944 class="d-none">facebook</span></a></li>
                      <li _ngcontent-serverapp-c101221944><a _ngcontent-serverapp-c101221944 href="{{$news->twitter_url}}" title="Share on twitter"><i _ngcontent-serverapp-c101221944 aria-hidden="true" class="fa fa-twitter center-content"></i><span _ngcontent-serverapp-c101221944 class="d-none">twitter </span></a></li>
                      <li _ngcontent-serverapp-c101221944><a _ngcontent-serverapp-c101221944 href="{{$news->whatsapp_url}}" title="Share on whatsapp"><i _ngcontent-serverapp-c101221944 aria-hidden="true" class="fa fa-whatsapp center-content"></i><span _ngcontent-serverapp-c101221944 class="d-none">whatsapp </span></a></li>
                      <li _ngcontent-serverapp-c101221944><a _ngcontent-serverapp-c101221944 href="{{$news->linkedIn_url}}" title="Share on linkedin"><i _ngcontent-serverapp-c101221944 aria-hidden="true" class="fa fa-linkedin center-content"></i><span _ngcontent-serverapp-c101221944 class="d-none">linkedin </span></a></li>
                    </ul>
                    <!---->
                    <!---->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div _ngcontent-serverapp-c101221944 class="row mt-3 px-3">
          <div _ngcontent-serverapp-c101221944 class="blog-description description_tag">
            <div _ngcontent-serverapp-c101221944 class="imported_data_custom ar ng-star-inserted" style="word-break: break-word;">
              <p>{!! $news->full_details !!}</p>
            </div>
            <!---->
            <!---->
          </div>
        </div>
        <div _ngcontent-serverapp-c101221944 class="row mt-2">
          <div _ngcontent-serverapp-c101221944 data-toggle="buttons" class="button-group-pills">
            <a _ngcontent-serverapp-c101221944 class="btn btn-default ng-star-inserted" href="/news">
              <input _ngcontent-serverapp-c101221944 type="checkbox" name="options">
              <div _ngcontent-serverapp-c101221944>Domestic Helper</div>
            </a>
            <!---->
          </div>
        </div>
        <div _ngcontent-serverapp-c101221944 class="yoga event bg p-2 mt-5 mb-3 bg-light ng-star-inserted">
          <div _ngcontent-serverapp-c101221944 class="row pt-3 pb-3">
            <div _ngcontent-serverapp-c101221944 class="col-12">
              <h2 _ngcontent-serverapp-c101221944 class="ml-3 ar">Related Post</h2>
            </div>
          </div>
          <div _ngcontent-serverapp-c101221944 class="container my-3">
            <div _ngcontent-serverapp-c101221944 class="row">
              <div _ngcontent-serverapp-c101221944 class="col-md-12 col-12 ng-star-inserted">
                <div _ngcontent-serverapp-c101221944 class="event-container d-flex related-post-container">
                  <a _ngcontent-serverapp-c101221944 class="yoga-circle" style="cursor: pointer;" href="javascript:"><img _ngcontent-serverapp-c101221944 loading="lazy" alt="feature_image for lazyLoad on helperplace blog" src="https://cdn.helperplace.com/blog/transferring-maid-contract.png"><span _ngcontent-serverapp-c101221944 class="d-none">feature image for blog</span></a>
                  <a _ngcontent-serverapp-c101221944 class="related-post ar" href="javascript:">
                    <h3 _ngcontent-serverapp-c101221944 class="text-dark">Transferring Maid Contract in Singapore - How to do?</h3>
                    <p _ngcontent-serverapp-c101221944>Transferring maid contract is an easy process employer. Current employer, new employer and the maid in Singapore must follow some important steps.</p>
                  </a>
                </div>
              </div>
              <div _ngcontent-serverapp-c101221944 class="col-md-12 col-12 ng-star-inserted">
                <div _ngcontent-serverapp-c101221944 class="event-container d-flex related-post-container">
                  <a _ngcontent-serverapp-c101221944 class="yoga-circle" style="cursor: pointer;" href="javascript:"><img _ngcontent-serverapp-c101221944 loading="lazy" alt="feature_image for lazyLoad on helperplace blog" src="https://cdn.helperplace.com/blog/Retention-Singapore.png"><span _ngcontent-serverapp-c101221944 class="d-none">feature image for blog</span></a>
                  <a _ngcontent-serverapp-c101221944 class="related-post ar" href="javascript:">
                    <h3 _ngcontent-serverapp-c101221944 class="text-dark">How to Improve the Retention rate of Helpers in Singapore?</h3>
                    <p _ngcontent-serverapp-c101221944>If you have never hired domestic helpers in Singapore before, it can become quite a daunting task. After all, you have to invite complete strangers into the most intimate areas of your house.</p>
                  </a>
                </div>
              </div>
              <div _ngcontent-serverapp-c101221944 class="col-md-12 col-12 ng-star-inserted">
                <div _ngcontent-serverapp-c101221944 class="event-container d-flex related-post-container">
                  <a _ngcontent-serverapp-c101221944 class="yoga-circle" style="cursor: pointer;" href="javascript:"><img _ngcontent-serverapp-c101221944 loading="lazy" alt="feature_image for lazyLoad on helperplace blog" src="https://cdn.helperplace.com/blog/Smart-phones-usage.jpg"><span _ngcontent-serverapp-c101221944 class="d-none">feature image for blog</span></a>
                  <a _ngcontent-serverapp-c101221944 class="related-post ar" href="javascript:">
                    <h3 _ngcontent-serverapp-c101221944 class="text-dark">7 Tips to Restrict Excessive use of Smart Phones by Helpers</h3>
                    <p _ngcontent-serverapp-c101221944>Smart phones are everywhere and probably in the hand of your helper when she is working! These tips will help you to restrict this usage in your home.</p>
                  </a>
                </div>
              </div>
              <!---->
            </div>
          </div>
        </div>
        <!---->
      </div>
      <div _ngcontent-serverapp-c101221944 class="col-lg-3">
        <div _ngcontent-serverapp-c101221944 class="blog-side custom-right-side">
          <div _ngcontent-serverapp-c101221944 class="ar blog-category-ar">
            <label _ngcontent-serverapp-c101221944 class="blog-title">categories</label>
            <div _ngcontent-serverapp-c101221944 class="sidebar-container borders">
              <ul _ngcontent-serverapp-c101221944 class="sidebar-list ng-star-inserted">

                @foreach($all_categories as $category)
                <li _ngcontent-serverapp-c1381573822 class="d-flex ng-star-inserted" tabindex="0"><a _ngcontent-serverapp-c1381573822 href="javascript:;"><i _ngcontent-serverapp-c1381573822 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i>{{$category->name}}</a></li>
                @endforeach

                <!---->
              </ul>
              <!---->
              <!---->
            </div>
          </div>
          <!-- <div _ngcontent-serverapp-c101221944 class="sidebar-container ar">
            <label _ngcontent-serverapp-c101221944 class="blog-title">Specific Search</label>
            <div _ngcontent-serverapp-c101221944 class="newsletter text-center form">
              <div _ngcontent-serverapp-c101221944 class="form-group"><input _ngcontent-serverapp-c101221944 type="search" class="form-control" placeholder="Keywords..."><a _ngcontent-serverapp-c101221944 href="#"><i _ngcontent-serverapp-c101221944 aria-hidden="true" class="fa fa-search"></i><span _ngcontent-serverapp-c101221944 class="d-none">fa fa-search in CitiMuber</span></a></div>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</section>

@endsection