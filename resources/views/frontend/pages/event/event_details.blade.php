@extends('frontend.layouts.app')
@section('title','EVENT DETAILS')

@section('content')
    <style>
        .find-event-ar{
            margin-top: 97px;
        }
    </style>

        <section _ngcontent-serverapp-c2543660390 class="page-section event-view-ar">
            <section _ngcontent-serverapp-c2543660390 class="agency breadcrumb-section-main breadcrumb-title inner-3 ng-star-inserted" style="background: #f8f8f8 !important;">
                <div _ngcontent-serverapp-c2543660390 class="container px-2 py-4 ng-star-inserted">
                    <div _ngcontent-serverapp-c2543660390 class="row px-3">
                        <div _ngcontent-serverapp-c2543660390 class="col-12">
                            <div _ngcontent-serverapp-c2543660390 class="breadcrumb-contain">
                                <div _ngcontent-serverapp-c2543660390 class="text-left">
                                    <ul _ngcontent-serverapp-c2543660390>
                                        <li _ngcontent-serverapp-c2543660390><a _ngcontent-serverapp-c2543660390 routerlinkactive="router-link-active" href="/" class="router-link-active"><i _ngcontent-serverapp-c2543660390 class="fa fa-home"></i></a></li>
                                        <li _ngcontent-serverapp-c2543660390><a _ngcontent-serverapp-c2543660390 routerlinkactive="router-link-active" href="/event"><i _ngcontent-serverapp-c2543660390 class="fa fa-angle-double-right"></i>Find Event</a></li>
                                        <li _ngcontent-serverapp-c2543660390>
                                            <a _ngcontent-serverapp-c2543660390 class="ng-star-inserted"><i _ngcontent-serverapp-c2543660390 class="fa fa-angle-double-right"></i>First Aid, CPR and Use of AED</a><!---->
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!----><!---->
            </section>
            <!----><!----><!---->
            <section _ngcontent-serverapp-c2543660390 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider ng-star-inserted">
                <div _ngcontent-serverapp-c2543660390 class="container">
                    <div _ngcontent-serverapp-c2543660390 class="row">
                        <div _ngcontent-serverapp-c2543660390 class="col-md-12 col-lg-9 event-more-detail">
                            <div _ngcontent-serverapp-c2543660390 class="product-wrapper-grid list-view">
                                <div _ngcontent-serverapp-c2543660390 class="row">
                                    <div _ngcontent-serverapp-c2543660390 class="col-12 col-12_padding_Set">
                                        <div _ngcontent-serverapp-c2543660390 class="agency-box">
                                            <div _ngcontent-serverapp-c2543660390 class="text-left row company_tabs_header_fix_size font-weight-normal">
                                                <div _ngcontent-serverapp-c2543660390 class="front custom_front_rsume_image ng-star-inserted">
                                                    <!---->
                                                    <div _ngcontent-serverapp-c2543660390 class="inner-custom-div dark ng-star-inserted">
                                                        {{ \Carbon\Carbon::parse($event->event_date)->format('d') }}
                                                        <div _ngcontent-serverapp-c2543660390 class="custom-date">
                                                            <div _ngcontent-serverapp-c2543660390>{{ \Carbon\Carbon::parse($event->event_date)->format('M') }}</div>
                                                            <div _ngcontent-serverapp-c2543660390>{{ \Carbon\Carbon::parse($event->event_date)->format('Y') }}</div>
                                                        </div>
                                                    </div>
                                                    <!---->
                                                </div>
                                                <!----><!---->
                                                <div _ngcontent-serverapp-c2543660390 class="agency_header w-10 ng-star-inserted" style="background-image: url('{{ asset('storage/event_image/' . $event->event_image) }}');" id="imgFirst Aid, CPR and Use of AED">
                                                    <div _ngcontent-serverapp-c2543660390 class="agency_header_White_opcity"></div>
                                                </div>
                                                <!----><!---->
                                                <div _ngcontent-serverapp-c2543660390 class="col-grid-box col-12">
                                                    <div _ngcontent-serverapp-c2543660390 class="product-box ng-star-inserted" style="display: flex; justify-content: space-between;">
                                                        <div _ngcontent-serverapp-c2543660390>
                                                            <div _ngcontent-serverapp-c2543660390 class="product-detail w-100">
                                                                <h1 _ngcontent-serverapp-c2543660390 class="mb-0 listing-about-title p-0"> {{ucfirst($event->title)}} </h1>
                                                            </div>
                                                            <div _ngcontent-serverapp-c2543660390 class="event-time-cal ng-star-inserted">
                                                                <div _ngcontent-serverapp-c2543660390>
                                                                    <div _ngcontent-serverapp-c2543660390 class="location-wrapper">
                                                                        <h2 _ngcontent-serverapp-c2543660390 class="text-left float-left active_icon_tab ng-tns-c135-4 ng-star-inserted loction-content"><span _ngcontent-serverapp-c2543660390><i _ngcontent-serverapp-c2543660390 class="fa fa-map-marker ng-tns-c135-4"></i></span><label _ngcontent-serverapp-c2543660390 class="ml-1 ng-tns-c135-4"> {{$event->countryName->name ??''}} </label></h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!---->
                                                        </div>
                                                        <!---->
                                                    </div>
                                                    <!----><!---->
                                                    <div _ngcontent-serverapp-c2543660390 class="row py-3 job-requirement ng-star-inserted">
                                                        <div _ngcontent-serverapp-c2543660390 class="col-11 mx-auto">
                                                            <div _ngcontent-serverapp-c2543660390 class="row">
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience"><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-sitemap"></i>Organizer Name : {{ucfirst($event->organizer_name)}}  </h2>
                                                                </div>
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience">
                                                                        <i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-calendar" style="margin-right: 7px;"></i><span _ngcontent-serverapp-c2543660390 class="ng-star-inserted"> Date: {{ \Carbon\Carbon::parse($event->event_date)->format('d F Y') }} </span><!----><!---->
                                                                    </h2>
                                                                </div>
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience"><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-users"></i>Audience : {{$event->audience}} </h2>
                                                                </div>
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                                    <!---->
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience ng-star-inserted"><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-clock-o"></i><span _ngcontent-serverapp-c2543660390> Time: From {{ \Carbon\Carbon::parse($event->start_time)->format('h:i A') }} To {{ \Carbon\Carbon::parse($event->end_time)->format('h:i A') }} </span></h2>
                                                                    <!----><!---->
                                                                </div>
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience">
                                                                        <i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-money"></i>Fees Detail : <span _ngcontent-serverapp-c2543660390 class="ng-star-inserted">HK$ {{$event->fees}} </span><!----><!---->
                                                                    </h2>
                                                                </div>
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience call-wrapper">
                                                                        <span _ngcontent-serverapp-c2543660390><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-phone"></i>Phone : {{$event->phone}} </span>
                                                                        <div _ngcontent-serverapp-c2543660390 class="calling-icon ng-star-inserted"><a _ngcontent-serverapp-c2543660390 target="_blank" href="#"><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-whatsapp"></i></a><a _ngcontent-serverapp-c2543660390 href="#"><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-phone"></i></a></div>
                                                                        <!---->
                                                                    </h2>
                                                                </div>
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience"><i _ngcontent-serverapp-c2543660390 class="fa fa-check-square"></i>Requirement : {{$event->requirement}} </h2>
                                                                </div>
                                                                <div _ngcontent-serverapp-c2543660390 class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                                    <h2 _ngcontent-serverapp-c2543660390 class="footer-experience"><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-map-marker"></i>Location : {{$event->location}} </h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!----><!---->
                                                    <div _ngcontent-serverapp-c2543660390 class="row custom-header-box">
                                                        <div _ngcontent-serverapp-c2543660390 class="col-11 mx-auto">
                                                            <h2 _ngcontent-serverapp-c2543660390 class="text-light">Event Detail</h2>
                                                        </div>
                                                    </div>
                                                    <div _ngcontent-serverapp-c2543660390 class="row ng-star-inserted">
                                                        <div _ngcontent-serverapp-c2543660390 class="col-11 mx-auto mt-3 jOb_description description_tag">
                                                            <p _ngcontent-serverapp-c2543660390 style="word-break: break-word;">
                                                                {!! $event->description !!}
                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                    <!----><!---->
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <a _ngcontent-serverapp-c2543660390 class="float-right primary pointer float-left-ar mr-2 ml-2"> Report this Event </a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div _ngcontent-serverapp-c2543660390 class="col-md-12 col-lg-3 d-md-block d-lg-block d-xl-block collection-filter-block resume-right-block">
                            <div _ngcontent-serverapp-c2543660390 class="text-center ng-star-inserted" style="width: 100%;">
                                <div _ngcontent-serverapp-c2543660390 class="blog-agency">
                                    <div _ngcontent-serverapp-c2543660390 class="blog-contain register-blog">
                                        <div _ngcontent-serverapp-c2543660390 class="img-container">
                                            <iframe _ngcontent-serverapp-c2543660390 width="100%" height="270" frameborder="0" style="border: 0;" class="ng-star-inserted"></iframe><!----><!---->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!---->
                        </div>
                    </div>
                </div>
            </section>
            <!---->
            <div _ngcontent-serverapp-c2543660390 id="footer" class="footer_custom mt-5 pl-4 mobile_sticky permenant-sticky ng-star-inserted">
                <div _ngcontent-serverapp-c2543660390 class="container">
                    <div _ngcontent-serverapp-c2543660390 class="row">
                        <div _ngcontent-serverapp-c2543660390 class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 res-footer footer-center">
                            <div _ngcontent-serverapp-c2543660390 class="footer-apply-content ng-star-inserted">
                                <div _ngcontent-serverapp-c2543660390 class="left">
                                    <!---->
                                    <div _ngcontent-serverapp-c2543660390 class="left-wrapper ng-star-inserted"><span _ngcontent-serverapp-c2543660390 class="left-content-text">To attend this event contact to organizer.</span></div>
                                    <!---->
                                </div>
                            </div>
                            <!---->
                        </div>
                        <div _ngcontent-serverapp-c2543660390 class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 res-footer footer-remove">
                            <div _ngcontent-serverapp-c2543660390 class="footer-apply-content right-content">
                                <div _ngcontent-serverapp-c2543660390 class="right">
                                    <div _ngcontent-serverapp-c2543660390 class="right-wrapper">
                                        <span _ngcontent-serverapp-c2543660390 class="left-content-text">Share</span>
                                        <ul _ngcontent-serverapp-c2543660390 class="social-sharing">
                                            <li _ngcontent-serverapp-c2543660390><a _ngcontent-serverapp-c2543660390><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-facebook"></i></a></li>
                                            <li _ngcontent-serverapp-c2543660390><a _ngcontent-serverapp-c2543660390><i _ngcontent-serverapp-c2543660390 aria-hidden="true" class="fa fa-whatsapp"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </section>


@endsection
