@extends('frontend.layouts.app')
@section('title','EVENT')
@section('css')



@endsection
@section('content')
    <style>
        .find-event-ar{
            margin-top: 97px;
        }
    </style>

        <section _ngcontent-serverapp-c3498621427 class="page-section">
            <section _ngcontent-serverapp-c3498621427 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom-container find-event-ar">
                <div _ngcontent-serverapp-c3498621427 class="container">
                    <div _ngcontent-serverapp-c3498621427 class="row">
                        <div _ngcontent-serverapp-c3498621427 class="col-12">
                            <div _ngcontent-serverapp-c3498621427 class="top-banner-wrapper">
                                <div _ngcontent-serverapp-c3498621427 class="top-banner-content small-section">
                                    <h1 _ngcontent-serverapp-c3498621427><span _ngcontent-serverapp-c3498621427>
                                        {{$event_page->first_heading}}</span></h1>
                                    <p _ngcontent-serverapp-c3498621427>{{$event_page->description}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div _ngcontent-serverapp-c3498621427 class="row mt-4">
                        <div _ngcontent-serverapp-c3498621427 class="col-md-12 col-lg-12">
                            <div _ngcontent-serverapp-c3498621427 class="collection-product-wrapper">
                                <div _ngcontent-serverapp-c3498621427 class="row">
                                    

                                   @foreach($all_events as $events)
                                    <div _ngcontent-serverapp-c3498621427 class="col-12 col-md-6 col-lg-6 pt-3 pb-3 custom-agency-container ng-star-inserted">
                                        <div _ngcontent-serverapp-c3498621427 class="agency-box bg bg-light">
                                            <a _ngcontent-serverapp-c3498621427 routerlinkactive="router-link-active" href="{{route('event_details', $events)}}">
                                                <div _ngcontent-serverapp-c3498621427 class="agency_header w-10" style="background-image: url('{{ asset('storage/event_image/' . $events->event_image) }}');"></div>
                                                <div _ngcontent-serverapp-c3498621427 class="yoga-circle">
                                                    <!---->
                                                    <div _ngcontent-serverapp-c3498621427 class="inner-custom-div ng-star-inserted">
                                                        {{ \Carbon\Carbon::parse($events->event_date)->format('d') }}
                                                        <div _ngcontent-serverapp-c3498621427 class="custom-date">
                                                            <div _ngcontent-serverapp-c3498621427>{{ \Carbon\Carbon::parse($events->event_date)->format('M') }}</div>
                                                            <div _ngcontent-serverapp-c3498621427>{{ \Carbon\Carbon::parse($events->event_date)->format('Y') }}</div>
                                                        </div>
                                                    </div>
                                                    <!---->
                                                </div>
                                                <div _ngcontent-serverapp-c3498621427 class="agency-box-detail">
                                                    <div _ngcontent-serverapp-c3498621427 class="agency-box-detail-left">
                                                        <div _ngcontent-serverapp-c3498621427 class="listing-about-txt mt-2 text-center dark"><i _ngcontent-serverapp-c3498621427 class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($events->start_time)->format('h:i A') }}</div>
                                                        <label _ngcontent-serverapp-c3498621427 class="mt-2 primary custom_font_size_for_location custom_h2 text-center p-0 custom_label ng-star-inserted"><i _ngcontent-serverapp-c3498621427 class="fa fa-map-marker"></i> {{$events->countryName->name ??''}} </label><!---->
                                                    </div>
                                                    <div _ngcontent-serverapp-c3498621427 class="agency-box-detail-right">
                                                        <h2 _ngcontent-serverapp-c3498621427 class="company_title_fix_size text-left"> {{ucfirst($events->organizer_name)}}  </h2>
                                                        <div _ngcontent-serverapp-c3498621427 class="listing-about-txt mt-2 text-justify"> {{$events->short_decription}}</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a _ngcontent-serverapp-c3498621427 href="{{route('event_details', $events)}}">
                                                <div _ngcontent-serverapp-c3498621427 class="agency-listing-footer mt-2 pointer">
                                                    <div _ngcontent-serverapp-c3498621427 class="event-name" style="width: 90%; float: left; overflow: hidden; text-overflow: ellipsis;">{{ucfirst($events->title)}}</div>
                                                    <div _ngcontent-serverapp-c3498621427 style="width: 10%; float: left;">
                                                        <span _ngcontent-serverapp-c3498621427 class="ng-star-inserted"><i _ngcontent-serverapp-c3498621427 aria-hidden="true" class="fa fa-money"></i></span><!----><!---->
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>
                                <!---->
                                <div _ngcontent-serverapp-c3498621427 class="container-fluid mt-5 ng-star-inserted">
                                    <div _ngcontent-serverapp-c3498621427 class="row mt-5">
                                        <div _ngcontent-serverapp-c3498621427 class="mt-5 mb-4 mx-auto text-center">
                                            <app-pagination _ngcontent-serverapp-c3498621427 _nghost-serverapp-c3388104089 ngh="3">
                                                <ul _ngcontent-serverapp-c3388104089 class="pagination">
                                                    <li _ngcontent-serverapp-c3388104089 class="page-item disabled"><a _ngcontent-serverapp-c3388104089 queryparamshandling="merge" class="prev page-link" href="/event">« </a></li>
                                                    <li _ngcontent-serverapp-c3388104089 class="page-item ng-star-inserted">
                                                        <a _ngcontent-serverapp-c3388104089 queryparamshandling="merge" class="page-link active ng-star-inserted" href="/event">1</a><!----><!---->
                                                    </li>
                                                    <!---->
                                                    <li _ngcontent-serverapp-c3388104089 class="page-item disabled"><a _ngcontent-serverapp-c3388104089 queryparamshandling="merge" class="next page-link" href="/event"> »</a></li>
                                                </ul>
                                            </app-pagination>
                                        </div>
                                    </div>
                                </div>
                                <!---->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>


@endsection
