@extends('frontend.layouts.app')
@section('title','CONTACT')

@section('content')
    <style>
        .more-info-ar{
            margin-top: 90px;
        }
        .agency h1{
            margin-top: 0 !important;
            margin-bottom: 5px;
        }
    </style>
    <section _ngcontent-serverapp-c777320000 class="page-section">
        <section _ngcontent-serverapp-c777320000 class="mt-4 mt-o-phone"></section>
        <section _ngcontent-serverapp-c777320000 class="mt-o-phone agency blog blog-sec blog-sidebar pb-5 blog-list sider more-info-ar">
            <div _ngcontent-serverapp-c777320000 class="container mt-o-phone">
                <div _ngcontent-serverapp-c777320000 class="row">
                    <app-side-menu _ngcontent-serverapp-c777320000 class="col-12 col-md-3 col-lg-3 col-xl-3" _nghost-serverapp-c1276488213 ngh="3">
                        <div _ngcontent-serverapp-c1276488213 class="collection-filter-block">
                            <div _ngcontent-serverapp-c1276488213 class="top-banner-content small-section app-side-menu-ar">
                                <div _ngcontent-serverapp-c1276488213 class="sidebar-container">
                                    <div _ngcontent-serverapp-c1276488213 class="product-page-filter">
                                        <div _ngcontent-serverapp-c1276488213 class="blog-side">
                                            <div _ngcontent-serverapp-c1276488213>
                                                <p _ngcontent-serverapp-c1276488213 class="blog-title ar">More Information…</p>
                                                <div _ngcontent-serverapp-c1276488213 class="sidebar-container borders">
                                                    <ul _ngcontent-serverapp-c1276488213 class="sidebar-list">
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/about"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> About Us </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_employers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Employers </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_helpers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Helpers </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/faqs"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> FAQ </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/terms"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Terms and Conditions </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/privacy"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Privacy Policy </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/contact"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Contact Us </a></li>
                                                        <!---->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </app-side-menu>
                    <div _ngcontent-serverapp-c777320000 class="col-12 col-md-9 col-lg-9 col-xl-9 ar">
                        <!----><!----><!----><!----><!----><!---->
                        <div _ngcontent-serverapp-c777320000 class="ng-star-inserted">
                            <router-outlet _ngcontent-serverapp-c777320000 name="contact-us"></router-outlet>
                            <app-contact-us _nghost-serverapp-c1373116283 class="ng-star-inserted" ngh="0">
                                <div _ngcontent-serverapp-c1373116283 class="top-banner-wrapper mb-3">
                                    <div _ngcontent-serverapp-c1373116283 class="top-banner-content small-section">
                                        <h1 _ngcontent-serverapp-c1373116283><span _ngcontent-serverapp-c1373116283>{{$contact->first_heading}}</span></h1>
                                        <p _ngcontent-serverapp-c1373116283> {{$contact->first_paragraph}} </p>
                                    </div>
                                </div>
                                <div _ngcontent-serverapp-c1373116283 class="row rounded shadow-lg">
                                    <div _ngcontent-serverapp-c1373116283 class="text-center col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 border-right p-2 mx-auto">
                                        <h2 _ngcontent-serverapp-c1373116283><i _ngcontent-serverapp-c1373116283 aria-hidden="true" class="fa fa-briefcase"></i></h2>

                                        <h3 _ngcontent-serverapp-c1373116283 class="custom_h2"> {{$contact->first_text}} </h3>
                                    </div>
                                    <div _ngcontent-serverapp-c1373116283 class="text-center col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 border-left p-2 mx-auto">
                                        <h2 _ngcontent-serverapp-c1373116283><i _ngcontent-serverapp-c1373116283 aria-hidden="true" class="fa fa-globe"></i></h2>
                                        <h3 _ngcontent-serverapp-c1373116283 class="custom_h2"> {{$contact->second_text}}</h3>
                                    </div>
                                </div>
                                <div _ngcontent-serverapp-c1373116283 class="row my-4">
                                    <div _ngcontent-serverapp-c1373116283 class="custom_h2 mx-auto text-center rounded shadow-lg col-6"><i _ngcontent-serverapp-c1373116283 aria-hidden="true" class="fa fa-phone"></i><input _ngcontent-serverapp-c1373116283 type="button" value="{{$contact->contact_number}}" id="coolbutton" class="border-0 bg-transparent"></div>
                                </div>
                                @if($contact->contact_us_map_src)
                                    <div _ngcontent-serverapp-c1373116283 class="row mt-4">
                                        <div _ngcontent-serverapp-c1373116283 class="col-12 text-center">
                                            <iframe _ngcontent-serverapp-c1373116283
                                                    src="{{$contact->contact_us_map_src}}"
                                                    width="100%" height="450"
                                                    frameborder="0"
                                                    allowfullscreen
                                                    aria-hidden="false"
                                                    tabindex="0"
                                                    style="border: 0;">
                                            </iframe>
                                        </div>
                                    </div>
                                @endif
                            </app-contact-us>
                            <!---->
                        </div>
                        <!---->
                    </div>
                </div>
            </div>
        </section>
        <section _ngcontent-serverapp-c777320000 class="bg bg-dark py-4 ng-star-inserted">
            <div _ngcontent-serverapp-c777320000 class="container">
                <div _ngcontent-serverapp-c777320000 class="row text-center">
                    <div _ngcontent-serverapp-c777320000 class="col-12">
                        <h4 _ngcontent-serverapp-c777320000 class="text-light"> {{$contact->second_heading}}</h4>
                        <h5 _ngcontent-serverapp-c777320000 class="text-light mb-3"> {{$contact->third_text}} </h5>
                        <a _ngcontent-serverapp-c777320000 href="mailto:support@site.com" class="btn btn_custom_color text-light text-center my-2 mx-auto btn-lg rounded"> {{$contact->contact_us_button}} </a>
                    </div>
                </div>
            </div>
        </section>
        <!---->
    </section>
@endsection
