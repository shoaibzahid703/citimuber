@extends('frontend.layouts.app')
@section('title','HAPPY HELPERS')

@section('content')
    <style>
        .mt-o-phone{
            margin-top: 90px;
        }
    </style>
    <section _ngcontent-serverapp-c777320000 class="page-section">
        <section _ngcontent-serverapp-c777320000 class="mt-4 mt-o-phone"></section>
        <section _ngcontent-serverapp-c777320000 class="mt-o-phone agency blog blog-sec blog-sidebar pb-5 blog-list sider more-info-ar">
            <div _ngcontent-serverapp-c777320000 class="container mt-o-phone">
                <div _ngcontent-serverapp-c777320000 class="row">
                    <app-side-menu _ngcontent-serverapp-c777320000 class="col-12 col-md-3 col-lg-3 col-xl-3" _nghost-serverapp-c1276488213 ngh="3">
                        <div _ngcontent-serverapp-c1276488213 class="collection-filter-block">
                            <div _ngcontent-serverapp-c1276488213 class="top-banner-content small-section app-side-menu-ar">
                                <div _ngcontent-serverapp-c1276488213 class="sidebar-container">
                                    <div _ngcontent-serverapp-c1276488213 class="product-page-filter">
                                        <div _ngcontent-serverapp-c1276488213 class="blog-side">
                                            <div _ngcontent-serverapp-c1276488213>
                                                <p _ngcontent-serverapp-c1276488213 class="blog-title ar">More Information…</p>
                                                <div _ngcontent-serverapp-c1276488213 class="sidebar-container borders">
                                                    <ul _ngcontent-serverapp-c1276488213 class="sidebar-list">
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/about"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> About Us </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_employers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Employers </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_helpers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Helpers </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/faqs"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> FAQ </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/terms"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Terms and Conditions </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/privacy"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Privacy Policy </a></li>
                                                        <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/contact"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Contact Us </a></li>
                                                        <!---->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </app-side-menu>
                    <div _ngcontent-serverapp-c777320000 class="col-12 col-md-9 col-lg-9 col-xl-9 ar">
                        <!----><!---->
                        <div _ngcontent-serverapp-c777320000 class="ng-star-inserted">
                            <router-outlet _ngcontent-serverapp-c777320000 name="happy-helpers"></router-outlet>
                            <app-happy-helpers _nghost-serverapp-c2389980517 class="ng-star-inserted" ngh="0">
                                <section _ngcontent-serverapp-c2389980517 class="happy_emp" style="background-image: url('{{asset('storage/happy_clients_page')}}/{{$happy_helpers->bg_image}}');">
                                    <div _ngcontent-serverapp-c2389980517 class="My_dark_overly">
                                        <h1 _ngcontent-serverapp-c2389980517 class="mb-5 text-light"><b _ngcontent-serverapp-c2389980517> {{$happy_helpers->first_heading}}</b></h1>
                                    </div>
                                </section>
                                <section _ngcontent-serverapp-c2389980517>
                                    <div _ngcontent-serverapp-c2389980517 class="container mt-5">
                                        <div _ngcontent-serverapp-c2389980517 class="row">
                                            <div _ngcontent-serverapp-c2389980517 class="col-12 col-md-6 col-lg-6 col-xl-6">
                                                <h2 _ngcontent-serverapp-c2389980517> {{$happy_helpers->second_heading}} </h2>
                                                <p _ngcontent-serverapp-c2389980517><b _ngcontent-serverapp-c2389980517> {{$happy_helpers->first_text}} </b></p>
                                                <p _ngcontent-serverapp-c2389980517>{{$happy_helpers->first_paragraph}}</p>
                                            </div>
                                            <div _ngcontent-serverapp-c2389980517 class="col-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
                                                <div _ngcontent-serverapp-c2389980517 class="faq-img-block">
                                                    <picture _ngcontent-serverapp-c2389980517>
                                                        <source _ngcontent-serverapp-c2389980517 srcset="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->first_image}}" width="247px" height="247px" media="(max-width:325px)" class="img-fluid">
                                                        <source _ngcontent-serverapp-c2389980517 srcset="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->first_image}}" width="347px" height="347px" media="(max-width:425px)" class="img-fluid">
                                                        <source _ngcontent-serverapp-c2389980517 srcset="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->first_image}}" width="458px" height="458px" media="(max-width:535px)" class="img-fluid">
                                                        <source _ngcontent-serverapp-c2389980517 srcset="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->first_image}}" width="480px" height="480px" media="(max-width:768px)" class="img-fluid">
                                                        <source _ngcontent-serverapp-c2389980517 srcset="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->first_image}}" width="225px" height="225px" media="(max-width:992px)" class="img-fluid">
                                                        <source _ngcontent-serverapp-c2389980517 srcset="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->first_image}}" width="315px" height="315px" media="(max-width:1200px)" class="img-fluid">
                                                        <img _ngcontent-serverapp-c2389980517 src="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->first_image}}" loading="lazy" alt="Job Opportunities with HelperPlace" width="383px" height="383px" class="img-fluid">
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c2389980517 class="row my-5 ads_banner ads_banner_padding mt-o-phone rounded">
                                            <div _ngcontent-serverapp-c2389980517 class="col-12 col-md-4 col-lg-4 col-xl-4 text-center align-self-center">
                                                <h3 _ngcontent-serverapp-c2389980517 class="rounded"><a _ngcontent-serverapp-c2389980517 class="btn rounded btn-lg text-light rounded"><b _ngcontent-serverapp-c2389980517 style="color: white;">{{$happy_helpers->register_button}} </b></a></h3>
                                            </div>
                                            <div _ngcontent-serverapp-c2389980517 class="col-12 col-md-8 col-lg-8 col-xl-8 pt-3">
                                                <p _ngcontent-serverapp-c2389980517> {{$happy_helpers->second_paragraph}} </p>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c2389980517 class="row mt-5 px-2">
                                            <div _ngcontent-serverapp-c2389980517 class="col-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
                                                <div _ngcontent-serverapp-c2389980517 class="faq-img-block">
                                                    <img _ngcontent-serverapp-c2389980517 src="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->sec_image}}" loading="lazy" alt="CITIMUBER is now available for your smart Phones" class="img-fluid">
                                                </div>
                                            </div>
                                            <div _ngcontent-serverapp-c2389980517 class="col-12 col-md-6 col-lg-6 col-xl-6 custom_favorit_padding">
                                                <h2 _ngcontent-serverapp-c2389980517> {{$happy_helpers->third_heading}} </h2>
                                                <p _ngcontent-serverapp-c2389980517>{{$happy_helpers->third_paragraph}} </p>
                                                <div _ngcontent-serverapp-c2389980517 class="row mb-3">
                                                    <div _ngcontent-serverapp-c2389980517 class="col-12 text-center store-div"><a _ngcontent-serverapp-c2389980517 href="/"><img _ngcontent-serverapp-c2389980517 alt="CITIMUBER IOS app for your ios Phone" src="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->app_store_image}}" loading="lazy" style="width: 50%;"></a><a _ngcontent-serverapp-c2389980517 href="/"><img _ngcontent-serverapp-c2389980517 alt="CITIMUBER Android app for your android Phone" src="{{asset('storage/happy_clients_page')}}/{{$happy_helpers->google_play_image}}" loading="lazy" style="width: 50%;"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </app-happy-helpers>
                            <!---->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section _ngcontent-serverapp-c777320000 class="bg bg-dark py-4 ng-star-inserted">
            <div _ngcontent-serverapp-c777320000 class="container">
                <div _ngcontent-serverapp-c777320000 class="row text-center">
                    <div _ngcontent-serverapp-c777320000 class="col-12">
                        <h4 _ngcontent-serverapp-c777320000 class="text-light"> {{$happy_helpers->fourth_heading}}</h4>
                        <h5 _ngcontent-serverapp-c777320{{$happy_helpers->second_text}}</h5>
                        <a _ngcontent-serverapp-c777320000 href="mailto:support@site.com" class="btn btn_custom_color text-light text-center my-2 mx-auto btn-lg rounded"> {{$happy_helpers->contact_us_button}} </a>
                    </div>
                </div>
            </div>
        </section>
        <!---->
    </section>
@endsection
