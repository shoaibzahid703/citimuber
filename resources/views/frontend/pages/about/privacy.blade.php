@extends('frontend.layouts.app')
@section('title','PRIVACY POLICY')

@section('content')
<style>
  .more-info-ar {
    margin-top: 90px;
  }

  .agency h1 {
    margin-top: 0 !important;
    margin-bottom: 5px;
  }
</style>
<section _ngcontent-serverapp-c777320000 class="page-section">
  <section _ngcontent-serverapp-c777320000 class="mt-4 mt-o-phone"></section>
  <section _ngcontent-serverapp-c777320000 class="mt-o-phone agency blog blog-sec blog-sidebar pb-5 blog-list sider more-info-ar">
    <div _ngcontent-serverapp-c777320000 class="container mt-o-phone">
      <div _ngcontent-serverapp-c777320000 class="row">
        <app-side-menu _ngcontent-serverapp-c777320000 class="col-12 col-md-3 col-lg-3 col-xl-3" _nghost-serverapp-c1276488213 ngh="3">
          <div _ngcontent-serverapp-c1276488213 class="collection-filter-block">
            <div _ngcontent-serverapp-c1276488213 class="top-banner-content small-section app-side-menu-ar">
              <div _ngcontent-serverapp-c1276488213 class="sidebar-container">
                <div _ngcontent-serverapp-c1276488213 class="product-page-filter">
                  <div _ngcontent-serverapp-c1276488213 class="blog-side">
                    <div _ngcontent-serverapp-c1276488213>
                      <p _ngcontent-serverapp-c1276488213 class="blog-title ar">More Information…</p>
                      <div _ngcontent-serverapp-c1276488213 class="sidebar-container borders">
                        <ul _ngcontent-serverapp-c1276488213 class="sidebar-list">
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/about"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> About Us </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_employers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Employers </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_helpers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Helpers </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/faqs"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> FAQ </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/terms"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Terms and Conditions </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/privacy"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Privacy Policy </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/contact"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Contact Us </a></li>
                          <!---->
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </app-side-menu>
        <div _ngcontent-serverapp-c777320000 class="col-12 col-md-9 col-lg-9 col-xl-9 ar">
          <div _ngcontent-serverapp-c777320000 class="ng-star-inserted">
            <router-outlet _ngcontent-serverapp-c777320000 name="privacy-policy"></router-outlet>
            <app-privacy-policy _nghost-serverapp-c469334495 class="ng-star-inserted" ngh="0">
              {!! $privacy->description !!}
            </app-privacy-policy>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
@endsection