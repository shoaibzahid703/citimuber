@extends('frontend.layouts.app')
@section('title','FAQS')
@section('content')

<style>
  .more-info-ar {
    margin-top: 70px;
  }
</style>
<section _ngcontent-serverapp-c777320000 class="page-section">
  <section _ngcontent-serverapp-c777320000 class="mt-4 mt-o-phone"></section>
  <section _ngcontent-serverapp-c777320000 class="mt-o-phone agency blog blog-sec blog-sidebar pb-5 blog-list sider more-info-ar">
    <div _ngcontent-serverapp-c777320000 class="container mt-o-phone">
      <div _ngcontent-serverapp-c777320000 class="row">
        <app-side-menu _ngcontent-serverapp-c777320000 class="col-12 col-md-3 col-lg-3 col-xl-3" _nghost-serverapp-c1276488213 ngh="3">
          <div _ngcontent-serverapp-c1276488213 class="collection-filter-block">
            <div _ngcontent-serverapp-c1276488213 class="top-banner-content small-section app-side-menu-ar">
              <div _ngcontent-serverapp-c1276488213 class="sidebar-container">
                <div _ngcontent-serverapp-c1276488213 class="product-page-filter">
                  <div _ngcontent-serverapp-c1276488213 class="blog-side">
                    <div _ngcontent-serverapp-c1276488213>
                      <p _ngcontent-serverapp-c1276488213 class="blog-title ar">More Information…</p>
                      <div _ngcontent-serverapp-c1276488213 class="sidebar-container borders">
                        <ul _ngcontent-serverapp-c1276488213 class="sidebar-list">
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/about"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> About Us </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_employers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Employers </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_helpers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Helpers </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/faqs"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> FAQ </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/terms"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Terms and Conditions </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/privacy"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Privacy Policy </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/contact"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Contact Us </a></li>
                          <!---->
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </app-side-menu>
        <div _ngcontent-serverapp-c777320000 class="col-12 col-md-9 col-lg-9 col-xl-9 ar">
          <div _ngcontent-serverapp-c777320000 class="ng-star-inserted">
            <section _ngcontent-serverapp-c3108284789 id="faq" class="mt-o-phone agency blog blog-sec blog-sidebar pt-2 pb-5 blog-list sider faq-ar">
              <div _ngcontent-serverapp-c3108284789 class="container mt-o-phone">
                <div _ngcontent-serverapp-c3108284789 class="row">
                  <div _ngcontent-serverapp-c3108284789 class="col-12 col-md-9 col-lg-9 col-xl-9 saas1 faq testimonial-bg">
                    <div _ngcontent-serverapp-c3108284789 class="row">
                      <div _ngcontent-serverapp-c3108284789 class="col-md-10">
                        <div _ngcontent-serverapp-c3108284789 class="faq-block">
                          <div _ngcontent-serverapp-c3108284789>
                            <h1 _ngcontent-serverapp-c3108284789 class="frequent-text">Frequently Asked Questions</h1>
                            <div class="accordion" id="accordionExample">

                              @foreach($faqs as $index => $row)
                              <div class="card">
                                <div class="card-header" id="headingOne_{{$row->id}}">
                                  <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne_{{$row->id}}" aria-expanded="{{ $index == 0 ? 'true' : 'false' }}" aria-controls="collapseOne_{{$row->id}}">
                                      {{$row->question}}
                                    </button>
                                  </h2>
                                </div>

                                <div id="collapseOne_{{$row->id}}" class="collapse {{ $index == 0 ? 'show' : '' }}" aria-labelledby="headingOne_{{$row->id}}" data-parent="#accordionExample">
                                  <div class="card-body">
                                    {{$row->question_answer}}
                                  </div>
                                </div>
                              </div>
                              @endforeach

                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section _ngcontent-serverapp-c777320000 class="bg bg-dark py-4 ng-star-inserted">
    <div _ngcontent-serverapp-c777320000 class="container">
      <div _ngcontent-serverapp-c777320000 class="row text-center">
        <div _ngcontent-serverapp-c777320000 class="col-12">
          <h4 _ngcontent-serverapp-c777320000 class="text-light"> Got a question?</h4>
          <h5 _ngcontent-serverapp-c777320000 class="text-light mb-3"> We are here to help you. Check out our FAQs or send an email to our support team. </h5>
          <a _ngcontent-serverapp-c777320000 href="mailto:support@site.com" class="btn btn_custom_color text-light text-center my-2 mx-auto btn-lg rounded"> Contact Us </a>
        </div>
      </div>
    </div>
  </section>
  <!---->
</section>

@endsection