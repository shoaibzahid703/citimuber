@extends('frontend.layouts.app')
@section('title','HAPPY EMPLOYERS')

@section('content')
<style>
  .mt-o-phone {
    margin-top: 90px;
  }
</style>
<section _ngcontent-serverapp-c777320000 class="page-section">
  <section _ngcontent-serverapp-c777320000 class="mt-4 mt-o-phone"></section>
  <section _ngcontent-serverapp-c777320000 class="mt-o-phone agency blog blog-sec blog-sidebar pb-5 blog-list sider more-info-ar">
    <div _ngcontent-serverapp-c777320000 class="container mt-o-phone">
      <div _ngcontent-serverapp-c777320000 class="row">
        <app-side-menu _ngcontent-serverapp-c777320000 class="col-12 col-md-3 col-lg-3 col-xl-3" _nghost-serverapp-c1276488213 ngh="3">
          <div _ngcontent-serverapp-c1276488213 class="collection-filter-block">
            <div _ngcontent-serverapp-c1276488213 class="top-banner-content small-section app-side-menu-ar">
              <div _ngcontent-serverapp-c1276488213 class="sidebar-container">
                <div _ngcontent-serverapp-c1276488213 class="product-page-filter">
                  <div _ngcontent-serverapp-c1276488213 class="blog-side">
                    <div _ngcontent-serverapp-c1276488213>
                      <p _ngcontent-serverapp-c1276488213 class="blog-title ar">More Information…</p>
                      <div _ngcontent-serverapp-c1276488213 class="sidebar-container borders">
                        <ul _ngcontent-serverapp-c1276488213 class="sidebar-list">
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/about"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> About Us </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_employers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Employers </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_helpers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Helpers </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/faqs"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> FAQ </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/terms"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Terms and Conditions </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/privacy"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Privacy Policy </a></li>
                          <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/contact"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Contact Us </a></li>
                          <!---->
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </app-side-menu>
        <div _ngcontent-serverapp-c777320000 class="col-12 col-md-9 col-lg-9 col-xl-9 ar">
          <!---->
          <div _ngcontent-serverapp-c777320000 class="ng-star-inserted">
            <router-outlet _ngcontent-serverapp-c777320000 name="happy-employers"></router-outlet>
            <app-happy-employers _nghost-serverapp-c3641503540 class="ng-star-inserted" ngh="4">
              <section _ngcontent-serverapp-c3641503540 class="happy_emp" style="background-image: url('{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->bg_image}}');">
                <div _ngcontent-serverapp-c3641503540 class="My_dark_overly">
                  <h1 _ngcontent-serverapp-c3641503540 class="mb-5 text-light"><b _ngcontent-serverapp-c3641503540>{{$happy_employers_page->first_heading}}</b></h1>
                  <p _ngcontent-serverapp-c3641503540 class="text-light"><b _ngcontent-serverapp-c3641503540> {{$happy_employers_page->first_text}}</b></p>
                </div>
              </section>
              <section _ngcontent-serverapp-c3641503540>
                <div _ngcontent-serverapp-c3641503540 class="container mt-5">
                  <div _ngcontent-serverapp-c3641503540 class="row">
                    <div _ngcontent-serverapp-c3641503540 class="col-12 col-md-6 col-lg-6 col-xl-6">
                      <h2 _ngcontent-serverapp-c3641503540> {{$happy_employers_page->second_heading}} </h2>
                      <p _ngcontent-serverapp-c3641503540></p>
                      <p>{{$happy_employers_page->first_paragraph}}</p>

                    </div>
                    <div _ngcontent-serverapp-c3641503540 class="col-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
                      <div _ngcontent-serverapp-c3641503540 class="faq-img-block">
                        <picture _ngcontent-serverapp-c3641503540>
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->first_image}}" width="247px" height="165px" media="(max-width:325px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->first_image}}" width="347px" height="231px" media="(max-width:425px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->first_image}}" width="458px" height="305px" media="(max-width:535px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->first_image}}" width="480px" height="320px" media="(max-width:768px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->first_image}}" width="225px" height="150px" media="(max-width:992px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->first_image}}" width="315px" height="210px" media="(max-width:1200px)" class="img-fluid">
                          <img _ngcontent-serverapp-c3641503540 src="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->first_image}}" loading="lazy" alt="Job Opportunities with CITIMUBER" width="383px" height="255px" class="img-fluid">
                        </picture>
                      </div>
                    </div>
                  </div>
                  <div _ngcontent-serverapp-c3641503540 class="row my-5 ads_banner mt-o-phone ads_banner_padding rounded">
                    <div _ngcontent-serverapp-c3641503540 class="col-12 col-md-4 col-lg-4 col-xl-4 text-center" style="align-self: center;">
                      <h3 _ngcontent-serverapp-c3641503540 class="rounded"><a _ngcontent-serverapp-c3641503540 class="btn rounded text-light btn-lg rounded"><b _ngcontent-serverapp-c3641503540 style="color: white;">Register Now </b></a></h3>
                    </div>
                    <div _ngcontent-serverapp-c3641503540 class="col-12 col-md-8 col-lg-8 col-xl-8 pt-3">
                      <p _ngcontent-serverapp-c3641503540> CITIMUBER allows you to search, message and offer a job to your future domestic helper. Our solution is totally free for all our candidates, we value direct and transparent recruitment! Are you still looking for the best helpers in town? </p>
                    </div>
                  </div>
                  <div _ngcontent-serverapp-c3641503540 class="row mt-5 px-2">
                    <div _ngcontent-serverapp-c3641503540 class="col-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
                      <div _ngcontent-serverapp-c3641503540 class="faq-img-block">
                        <picture _ngcontent-serverapp-c3641503540>
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->sec_image}}" width="231px" height="221px" media="(max-width:325px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->sec_image}}" width="331px" height="317px" media="(max-width:425px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->sec_image}}" width="442px" height="442px" media="(max-width:535px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->sec_image}}" width="464px" height="444px" media="(max-width:768px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->sec_image}}" width="217px" height="207px" media="(max-width:992px)" class="img-fluid">
                          <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->sec_image}}" width="307px" height="293px" media="(max-width:1200px)" class="img-fluid">
                          <img _ngcontent-serverapp-c3641503540 src="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->sec_image}}" loading="lazy" alt="CITIMUBER is now available for your smart Phones" width="375px" height="358px" class="img-fluid">
                        </picture>
                      </div>
                    </div>
                    <div _ngcontent-serverapp-c3641503540 class="col-12 col-md-6 col-lg-6 col-xl-6 custom_favorit_padding">
                      <h2 _ngcontent-serverapp-c3641503540> {{$happy_employers_page->third_heading}} </h2>
                      <p _ngcontent-serverapp-c3641503540> {{$happy_employers_page->second_paragraph}}</p>
                      <div _ngcontent-serverapp-c3641503540 class="row mb-3">
                        <div _ngcontent-serverapp-c3641503540 class="col-12 text-center store-div">
                          <a _ngcontent-serverapp-c3641503540 href="/">
                            <picture _ngcontent-serverapp-c3641503540>
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->app_store_image}}" width="116px" height="34px" media="(max-width:325px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->app_store_image}}" width="166px" height="49px" media="(max-width:425px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->app_store_image}}" width="221px" height="66px" media="(max-width:535px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->app_store_image}}" width="232px" height="69px" media="(max-width:768px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->app_store_image}}" width="109px" height="32px" media="(max-width:992px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->app_store_image}}" width="154px" height="46px" media="(max-width:1200px)">
                              <img _ngcontent-serverapp-c3641503540 alt="HelperPlace IOS app for your IOS Phone" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->app_store_image}}" loading="lazy" width="187px" height="56px">
                            </picture>
                          </a>
                          <a _ngcontent-serverapp-c3641503540 href="/">
                            <picture _ngcontent-serverapp-c3641503540>
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->google_play_image}}" width="116px" height="34px" media="(max-width:325px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->google_play_image}}" width="166px" height="49px" media="(max-width:425px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->google_play_image}}" width="221px" height="66px" media="(max-width:535px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->google_play_image}}" width="232px" height="69px" media="(max-width:768px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->google_play_image}}" width="109px" height="32px" media="(max-width:992px)">
                              <source _ngcontent-serverapp-c3641503540 srcset="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->google_play_image}}" width="154px" height="46px" media="(max-width:1200px)">
                              <img _ngcontent-serverapp-c3641503540 alt="HelperPlace Android app for your android Phone" src="{{asset('storage/happy_employees_page')}}/{{@$happy_employers_page->google_play_image}}" loading="lazy" width="187px" height="56px">
                            </picture>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section _ngcontent-serverapp-c3641503540 class="app2 team py-4 mt-0 testimonial_bg">
                <div _ngcontent-serverapp-c3641503540 class="container">
                  <div _ngcontent-serverapp-c3641503540 class="row pb-4">
                    <div _ngcontent-serverapp-c3641503540 class="col-12">
                      <div _ngcontent-serverapp-c3641503540 class="title title2 title-inner">
                        <div _ngcontent-serverapp-c3641503540 class="main-title">
                          <br _ngcontent-serverapp-c3641503540>
                          <h2 _ngcontent-serverapp-c3641503540 class="borders text-center mb-4 text-light text-uppercase m-b-0"><span _ngcontent-serverapp-c3641503540> {{$happy_employers_page->fourth_heading}}</span></h2>
                          <br _ngcontent-serverapp-c3641503540><br _ngcontent-serverapp-c3641503540>
                          <p _ngcontent-serverapp-c3641503540 class="text-light mt-4"><b _ngcontent-serverapp-c3641503540>{{$happy_employers_page->second_text}}</b></p>
                        </div>
                      </div>
                    </div>
                    <div _ngcontent-serverapp-c3641503540 class="col-12">
                      <div _ngcontent-serverapp-c3641503540 id="find-helper-carousel" class="ng-star-inserted">
                        <ngb-carousel _ngcontent-serverapp-c3641503540 tabindex="0" class="carousel slide team-slider" aria-activedescendant="slide-ngb-slide-60478" style="display: block;" ngskiphydration>
                          <div role="tablist" class="carousel-indicators">
                            <button type="button" data-bs-target role="tab" class="active ng-star-inserted" aria-labelledby="slide-ngb-slide-60478" aria-controls="slide-ngb-slide-60478" aria-selected="true"></button><button type="button" data-bs-target role="tab" class="ng-star-inserted" aria-labelledby="slide-ngb-slide-60479" aria-controls="slide-ngb-slide-60479" aria-selected="false"></button>
                            <!---->
                          </div>
                          <div class="carousel-inner">
                            @foreach($happy_employers->chunk(3) as $employerChunk)
                            <div role="tabpanel" class="carousel-item @if($loop->first) active @endif ng-star-inserted" id="slide-ngb-slide-60478">
                              <span class="visually-hidden"> Slide {{ $loop->iteration }} of {{ ceil($happy_employers->count() / 3) }} </span>
                              <div class="d-flex justify-content-around">
                                @foreach($employerChunk as $employeer)
                                <a _ngcontent-serverapp-c3641503540 class="carousel-inner-wrapper ng-star-inserted">
                                  <div _ngcontent-serverapp-c3641503540 class="blog-agency rounded mt-4 mb-5">
                                    <div _ngcontent-serverapp-c3641503540 class="blog-contain ng-star-inserted" style="width:100% !important">
                                      <div _ngcontent-serverapp-c3641503540 class="team-container text-center">
                                        <img _ngcontent-serverapp-c3641503540 alt="CITIMUBER Happy Employers testimonial" loading="lazy" onerror="this.src='{{ asset('storage/happy_employee_image/' . $employeer->employee_image) }}'" class="img-fluid members" src="{{ asset('storage/happy_employee_image/' . $employeer->employee_image) }}">
                                        <div _ngcontent-serverapp-c3641503540 class="text-center">
                                          <h5 _ngcontent-serverapp-c3641503540 class="name text-center">{{$employeer->employee_name}}</h5>
                                          <p _ngcontent-serverapp-c3641503540 class="team-para text-center">{{$employeer->description}}</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </a>
                                @endforeach
                              </div>
                            </div>
                            @endforeach
                          </div>

                        </ngb-carousel>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </app-happy-employers>
          </div>

        </div>
      </div>
    </div>
  </section>
  <section _ngcontent-serverapp-c777320000 class="bg bg-dark py-4 ng-star-inserted">
    <div _ngcontent-serverapp-c777320000 class="container">
      <div _ngcontent-serverapp-c777320000 class="row text-center">
        <div _ngcontent-serverapp-c777320000 class="col-12">
          <h4 _ngcontent-serverapp-c777320000 class="text-light"> {{$happy_employers_page->five_heading}}</h4>
          <h5 _ngcontent-serverapp-c777320000 class="text-light mb-3"> {{$happy_employers_page->third_text}}</h5>
          <a _ngcontent-serverapp-c777320000 href="mailto:support@site.com" class="btn btn_custom_color text-light text-center my-2 mx-auto btn-lg rounded"> {{$happy_employers_page->contact_us_button}} </a>
        </div>
      </div>
    </div>
  </section>
  <!---->
</section>
@endsection