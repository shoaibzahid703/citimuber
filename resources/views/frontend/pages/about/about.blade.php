@extends('frontend.layouts.app')
@section('title','ABOUT US')

@section('content')
    <style>
        .mt-o-phone{
            margin-top: 90px;
        }
    </style>
        <section _ngcontent-serverapp-c777320000 class="page-section">
            <section _ngcontent-serverapp-c777320000 class="mt-4 mt-o-phone"></section>
            <section _ngcontent-serverapp-c777320000 class="mt-o-phone agency blog blog-sec blog-sidebar pb-5 blog-list sider more-info-ar">
                <div _ngcontent-serverapp-c777320000 class="container mt-o-phone" style="margin-top:20px">
                    <div _ngcontent-serverapp-c777320000 class="row">
                        <app-side-menu _ngcontent-serverapp-c777320000 class="col-12 col-md-3 col-lg-3 col-xl-3" _nghost-serverapp-c1276488213 ngh="3">
                            <div _ngcontent-serverapp-c1276488213 class="collection-filter-block">
                                <div _ngcontent-serverapp-c1276488213 class="top-banner-content small-section app-side-menu-ar">
                                    <div _ngcontent-serverapp-c1276488213 class="sidebar-container">
                                        <div _ngcontent-serverapp-c1276488213 class="product-page-filter">
                                            <div _ngcontent-serverapp-c1276488213 class="blog-side">
                                                <div _ngcontent-serverapp-c1276488213>
                                                    <p _ngcontent-serverapp-c1276488213 class="blog-title ar">More Information…</p>
                                                    <div _ngcontent-serverapp-c1276488213 class="sidebar-container borders">
                                                        <ul _ngcontent-serverapp-c1276488213 class="sidebar-list">
                                                            <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/about"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> About Us </a></li>
                                                            <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_employers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Employers </a></li>
                                                            <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/happy_helpers"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Happy Helpers </a></li>
                                                            <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/faqs"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> FAQ </a></li>
                                                            <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/terms"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Terms and Conditions </a></li>
                                                            <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/privacy"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Privacy Policy </a></li>
                                                            <li _ngcontent-serverapp-c1276488213 class="d-flex ng-star-inserted"><a _ngcontent-serverapp-c1276488213 href="/contact"><i _ngcontent-serverapp-c1276488213 aria-hidden="true" class="fa m-r-15 fa-angle-right"></i> Contact Us </a></li>
                                                            <!---->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </app-side-menu>
                        <div _ngcontent-serverapp-c777320000 class="col-12 col-md-9 col-lg-9 col-xl-9 ar">
                            <div _ngcontent-serverapp-c777320000 class="ng-star-inserted">
                                <router-outlet _ngcontent-serverapp-c777320000 name="about-us"></router-outlet>
                                <app-about-us _nghost-serverapp-c1331885571 class="ng-star-inserted" ngh="0">
                                    <h1 _ngcontent-serverapp-c1331885571 class="primary text-center"> {{$avout_us_page->first_heading}}</h1>

                                    <div _ngcontent-serverapp-c1331885571 class="content">
                                       <p>
                                        {!! $avout_us_page->description !!}
                                        </p>
                                    </div>
                                    <!-- <h2 _ngcontent-serverapp-c1331885571> Why choose CITIMUBER?</h2>
                                    <h3 _ngcontent-serverapp-c1331885571> Honest and Responsible</h3>
                                    <div _ngcontent-serverapp-c1331885571 class="content">
                                        <p>
                                            Our goal is to revolutionize the recruitment process, making it more efficient, fair, and accessible for everyone. At CITIMUBER, we believe in the power of connection and the value of informed decisions.                                        </p>
                                    </div>

                                    <h3 _ngcontent-serverapp-c1331885571>For Helpers</h3>
                                    <div _ngcontent-serverapp-c1331885571 class="content">
                                        <p>
                                            At CITIMUBER, we offer FREE access to employers in need of domestic helpers. We've created a marketplace where you can directly connect with potential employers at no cost. Our goal is to help you find the right employer and foster long-term, positive working relationships. Simply submit your profile on our platform for free and wait for your perfect match with a family that suits your preferences. Remember, there are no placement fees for helpers at CITIMUBER.                                        </p>
                                    </div>

                                    <div _ngcontent-serverapp-c1331885571 class="text-center col-12 col-md-10 col-lg-10 col-xl-10 mx-auto mb-5">
                                        <picture _ngcontent-serverapp-c1331885571>
                                            <source _ngcontent-serverapp-c1331885571 media="(max-width:576px)" srcset="{{asset('cdn-sub/front-app/assets/images/No-placement-fees-sm.webp')}}">
                                            <source _ngcontent-serverapp-c1331885571 srcset="{{asset('cdn-sub/front-app/assets/images/No-placement-fees-md.webp')}}">
                                            <img _ngcontent-serverapp-c1331885571 alt="No Placement fees" loading="lazy" onerror="this.onerror = null;this.parentNode.children[0].srcset = this.parentNode.children[1].srcset = this.src;" class="img-fluid title-img" src="{{asset('cdn-sub/front-app/assets/images/No-placement-fees.png')}}">
                                        </picture>
                                    </div>
                                    <h3 _ngcontent-serverapp-c1331885571> For Employers</h3>
                                    <div _ngcontent-serverapp-c1331885571 class="content">
                                        <p>
                                            Our online platform is your gateway to a pool of skilled and experienced domestic helpers. Regardless of your location, we are here to assist you in finding the right helper for your needs. All you need to do is select your location, post a job, and start receiving applications. Your journey to finding the perfect helper for your family starts here.                                        </p>
                                    </div>
                                    <h3 _ngcontent-serverapp-c1331885571>Mission Statement</h3>
                                    <div _ngcontent-serverapp-c1331885571 class="content">
                                        Our mission at CITIMUBER is to foster a fair recruitment environment and continually enhance our platform with features that improve your experience. We aim to reach as many candidates and employers as possible, ensuring a safe and efficient job placement process. We take pride in being a promising platform for hiring domestic staff. Are you ready to join our community?                                    </div>
                                    <h3 _ngcontent-serverapp-c1331885571>Contact Us</h3>
                                    <div _ngcontent-serverapp-c1331885571 class="content">
                                        Our headquarters is located in Hong Kong, and we are delighted to support our users in Hong Kong. Feel free to reach out to us at Tel: +852-9739-8915.                                    </div> -->

                                </app-about-us>
                                <!---->
                            </div>
                            <!----><!----><!----><!----><!----><!----><!---->
                        </div>
                    </div>
                </div>
            </section>
            <section _ngcontent-serverapp-c777320000 class="bg bg-dark py-4 ng-star-inserted">
                <div _ngcontent-serverapp-c777320000 class="container">
                    <div _ngcontent-serverapp-c777320000 class="row text-center">
                        <div _ngcontent-serverapp-c777320000 class="col-12">
                            <h4 _ngcontent-serverapp-c777320000 class="text-light"> {{$avout_us_page->second_heading}}</h4>
                            <h5 _ngcontent-serverapp-c777320000 class="text-light mb-3">{{$avout_us_page->text_line}}</h5>
                            <a _ngcontent-serverapp-c777320000 href="mailto:support@site.com" class="btn btn_custom_color text-light text-center my-2 mx-auto btn-lg rounded"> {{$avout_us_page->button_name}} </a>
                        </div>
                    </div>
                </div>
            </section>
            <!---->
        </section>
@endsection
