@extends('frontend.layouts.app')
@section('title','PARTNER')

@section('content')
    <style>
        .find-agency-ar{
            margin-top: 97px;
        }
    </style>

        <section _ngcontent-serverapp-c93986215 class="page-section">
            <section _ngcontent-serverapp-c93986215 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom-container find-agency-ar">
                <div _ngcontent-serverapp-c93986215 class="container">
                    <div _ngcontent-serverapp-c93986215 class="row">
                        <div _ngcontent-serverapp-c93986215 class="col-12">
                            <ul _ngcontent-serverapp-c93986215 class="nav nav-tabs custom-tab">
                                <a href="/agency&services" style="width: 25%;">  <li _ngcontent-serverapp-c93986215 style="width:100%;" class=><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Agency</span></li></a>
                                <a href="/training" style="width: 25%;">  <li _ngcontent-serverapp-c93986215 style="width:100%;" class><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Training</span></li></a>
                                <a href="/partner" style="width: 25%;"> <li _ngcontent-serverapp-c93986215 style="width:100%;" class="active"><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Partner</span></li></a>
                                <a href="/association" style="width: 25%;"><li _ngcontent-serverapp-c93986215 style="width:100%;" class><i _ngcontent-serverapp-c93986215 class="fa fa-user"></i><span _ngcontent-serverapp-c93986215>Association</span></li></a>
                            </ul>
                        </div>
                    </div>
                    <div _ngcontent-serverapp-c93986215 class>
                        <div _ngcontent-serverapp-c93986215 class="row mt-4">
                            <div _ngcontent-serverapp-c93986215 class="col-12">
                                <div _ngcontent-serverapp-c93986215 class="top-banner-wrapper">
                                    <div _ngcontent-serverapp-c93986215 class="top-banner-content small-section">
                                        <!----><!---->
                                        <h1 _ngcontent-serverapp-c93986215 class="ng-star-inserted"><span _ngcontent-serverapp-c93986215>{{$partner_landing_page->first_heading}}</span></h1>
                                        <!----><!----><!----><!---->
                                        <p _ngcontent-serverapp-c93986215 class="header_2 ng-star-inserted"> {{$partner_landing_page->paragraph}}</p>
                                        <!----><!----><!---->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div _ngcontent-serverapp-c93986215 class="row mt-4">
                        <div _ngcontent-serverapp-c93986215 class="col-md-12 col-lg-12">
                            <div _ngcontent-serverapp-c93986215 class="collection-product-wrapper">
                                <div _ngcontent-serverapp-c93986215 class="row ng-star-inserted">


                                    @if($partners->count() == 0)
                                        <div class="col-md-12 text-center">
                                            <h4 class="text-info">No Partners Found</h4>
                                        </div>
                                    @else
                                        @foreach($partners as $partner)
                                            <div _ngcontent-serverapp-c93986215 class="col-12 col-md-6 col-lg-6 pt-3 pb-3 custom-agency-container ng-star-inserted">
                                                <div _ngcontent-serverapp-c93986215 class="agency-box bg bg-light">
                                                    <a _ngcontent-serverapp-c93986215 routerlinkactive="router-link-active" href="{{ route('partner_details',$partner) }}">
                                                        <div _ngcontent-serverapp-c93986215 class="agency_header w-10"
                                                             style="background-image: url({{asset('storage/company_banner_image').'/'.$partner->info->company_banner_image}});"
                                                        >
                                                            <div _ngcontent-serverapp-c93986215 class="agency_header_White_opcity">
                                                                <div _ngcontent-serverapp-c93986215 class="Contacted_tag_right_top position-absolute text-center ng-star-inserted">
                                                                    Licence: {{ $partner->info->license_no }}
                                                                </div>
                                                                <!---->
                                                            </div>
                                                        </div>
                                                        <div _ngcontent-serverapp-c93986215 class="yoga-circle">
                                                            <picture _ngcontent-serverapp-c93986215>
                                                                <source _ngcontent-serverapp-c93986215 width="98px" height="98px" media="(max-width:450px)"
                                                                        srcset="{{asset('storage/company_image').'/'.$partner->info->company_image}}">
                                                                <source _ngcontent-serverapp-c93986215 width="128px" height="128px" media="(max-width:768px)"
                                                                        srcset="{{asset('storage/company_image').'/'.$partner->info->company_image}}">
                                                                <source _ngcontent-serverapp-c93986215 width="98px" height="98px" media="(max-width:992px)"
                                                                        srcset="{{asset('storage/company_image').'/'.$partner->info->company_image}}">
                                                                <img _ngcontent-serverapp-c93986215 width="128px" height="128px"
                                                                     src="{{asset('storage/company_image').'/'.$partner->info->company_image}}"
                                                                >
                                                            </picture>
                                                        </div>
                                                        <div _ngcontent-serverapp-c93986215 class="agency-box-detail">
                                                            <div _ngcontent-serverapp-c93986215 class="agency-box-detail-left">
                                                                <div _ngcontent-serverapp-c93986215 class="ratting_div text-center mr-0 ng-star-inserted">
                                                                    @for ($i = 1; $i <= 5; $i++)
                                                                        @if ($i <= $partner->max_rating)
                                                                            <span class="fa fa-star"></span> <!-- Filled star -->
                                                                        @else
                                                                            <span class="fa fa-star-o"></span> <!-- Empty star -->
                                                                        @endif
                                                                    @endfor
                                                                </div>
                                                                @if($partner->user_location)
                                                                    <span _ngcontent-serverapp-c93986215 class="mt-2 primary custom_font_size_for_location text-center p-0 custom_h4 ng-star-inserted"><i _ngcontent-serverapp-c93986215 class="fa fa-map-marker"></i>
                                                            {{ $partner->user_location->name }}
                                                        </span>
                                                                @endif

                                                            </div>
                                                            <div _ngcontent-serverapp-c93986215 class="agency-box-detail-right">
                                                                <h2 _ngcontent-serverapp-c93986215 class="company_title_fix_size text-left"> {{$partner->company_name}} </h2>
                                                                <div _ngcontent-serverapp-c93986215 class="listing-about-txt mt-2">
                                                                    {{ $partner->info->company_short_intro }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a _ngcontent-serverapp-c93986215 routerlinkactive="router-link-active"
                                                       href="{{ route('partner_details',$partner) }}" class="ng-star-inserted">
                                                        @php
                                                            $partner_service = \App\Models\AgencyService::where('user_id',$partner->id)
                                                               ->orderBy('id','desc')
                                                               ->first();
                                                        @endphp
                                                        @if($partner_service)
                                                            <div _ngcontent-serverapp-c93986215 class="agency-listing-footer mt-2"> {{ $partner_service->name }} </div>
                                                        @else
                                                            <div _ngcontent-serverapp-c93986215 class="agency-listing-footer mt-2"> </div>
                                                        @endif
                                                    </a>
                                                    <!----><!---->
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <!----><!----><!----><!---->
                                <div _ngcontent-serverapp-c93986215 class="container-fluid mt-5">
                                    <div _ngcontent-serverapp-c93986215 class="row mt-5">
                                        <div _ngcontent-serverapp-c93986215 class="mt-5 mb-4 mx-auto text-center">
                                            {{ $partners->links()  }}
                                        </div>
                                    </div>
                                </div>
                                <!----><!---->
                                <div _ngcontent-serverapp-c93986215 class="container-fluid my-4 pt-4 ng-star-inserted">
                                    <div _ngcontent-serverapp-c93986215 class="row">
                                        <div _ngcontent-serverapp-c93986215 class="col-12 extra_details_bottom">
                                            <p _ngcontent-serverapp-c93986215></p>
                                            {!! $partner_landing_page->description !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!----><!---->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>

@endsection
