@extends('frontend.layouts.app')
@section('content')



<style>
    .find-agency-ar{
        margin-top: 97px;
    }
</style>
<section _ngcontent-serverapp-c93986215 class="page-section">
    <section _ngcontent-serverapp-c93986215 class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom-container find-agency-ar">
        <div _ngcontent-serverapp-c93986215 class="container">
            <div _ngcontent-serverapp-c93986215 class>
                <div _ngcontent-serverapp-c93986215 class="row mt-4">
                    <div _ngcontent-serverapp-c93986215 class="col-12">
                        <div _ngcontent-serverapp-c93986215 class="top-banner-wrapper">
                            <div _ngcontent-serverapp-c93986215 class="top-banner-content small-section">
                                <h1 _ngcontent-serverapp-c93986215 class="ng-star-inserted"><span _ngcontent-serverapp-c93986215>
                                        {{ __('Register') }}
                                    </span>
                                </h1>
                                <!----><!----><!----><!---->
                                <p _ngcontent-serverapp-c93986215 class="header_2 ng-star-inserted">{{ __('Register Here') }}</p>
                                <!----><!----><!----><!----><!---->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container mt-5">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf

                                    <div class="row mb-3">
                                        <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                    </div>

                                    <div class="row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
