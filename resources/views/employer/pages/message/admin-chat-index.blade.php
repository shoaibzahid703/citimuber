
@extends('employer.layouts.app')
@section('title','Messages')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/dashboard.css')}}">
    <style>
        .messages_div {
            height: 400px; /* or any other fixed height */
            overflow-y: auto;
        }
        @media (max-width: 768px) {
            .nk-chat-body {
                width: 100%;
                max-width: 100%;
                opacity: 1;
                pointer-events: auto;
            }
            .messages_div {
                height: 500px !important;
            }
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid " >
        <div class="nk-content-inner" >
            <div class="nk-content-body p-0" >
                <div class="nk-chat">
                    <div class="nk-chat-body mt-5">
                        <div class="nk-chat-panel" data-simplebar="init">
                            <div class="simplebar-wrapper" style="margin: -20px -28px;">
                                <div class="simplebar-height-auto-observer-wrapper">
                                    <div class="simplebar-height-auto-observer"></div>
                                </div>
                                <div class="simplebar-mask">
                                    <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                        <div class="simplebar-content-wrapper" tabindex="0" role="region" aria-label="scrollable content" style="height: 100%; overflow: hidden scroll;">
                                            <div class="simplebar-content messages_div" style="padding: 20px 28px;">

                                                @if($messages->count() == 0)
                                                    <div class="no_message_text text-center h4 mt-5">No Messages Found</div>
                                                @else
                                                    @foreach($messages as $key => $message)
                                                        @if($message->from_user == auth()->user()->id)
                                                            <div class="chat is-you message @if($key == 0) mt-2 @endif">
                                                                <div class="chat-avatar">
                                                                    <div class="user-avatar bg-purple">
                                                                        <span>{{ get_name_first_letters($message->from->name) }}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="chat-content">
                                                                    <div class="chat-bubbles">
                                                                        <div class="chat-bubble">
                                                                            <div class="chat-msg">
                                                                                {{$message->message}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <ul class="chat-meta">
                                                                        <li>{{$message->from->name}}</li>
                                                                        <li>{{\Carbon\Carbon::parse($message->created_at)->format('d-m-y h:i A')}} </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="chat is-me message">
                                                                <div class="chat-content">
                                                                    <div class="chat-bubbles">
                                                                        <div class="chat-bubble">
                                                                            <div class="chat-msg"> {{$message->message}}. </div>
                                                                        </div>
                                                                    </div>
                                                                    <ul class="chat-meta">
                                                                        <li>{{$message->from->name}}</li>
                                                                        <li>{{\Carbon\Carbon::parse($message->created_at)->format('d-m-y h:i A')}} </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="simplebar-placeholder" style="width: auto; height: 753px;"></div>
                            </div>
                            <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
                                <div class="simplebar-scrollbar" style="width: 0px; display: none;"></div>
                            </div>
                            <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
                                <div class="simplebar-scrollbar" style="height: 25px; transform: translate3d(0px, 25px, 0px); display: block;"></div>
                            </div>
                        </div>
                        <div class="nk-chat-editor">
                            <div class="nk-chat-editor-form">
                                <form method="post" action="{{route('employer_send_support_msg')}}" class="ajax-form">
                                    @csrf
                                    <div class="form-control-wrap">
                                    <textarea class="form-control form-control-simple no-resize" rows="2"
                                              name="comment"
                                              id="comment"
                                              placeholder="Type your message to {{$admin->name}} "
                                              required></textarea>
                                    </div>
                                    <input type="hidden" name="from_user" value="{{auth()->user()->id}}">
                                    <input type="hidden" name="to_user" value="{{$admin->id}}">
                                </form>
                            </div>
                            <ul class="nk-chat-editor-tools g-2">
                                <li>
                                    <button class="btn login_btn btn-icon post_message" id="post_message">
                                        <em class="icon ni ni-send-alt"></em>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).on("click", ".post_message", function () {
            const comment_len = $('#comment').val().length;
            if (comment_len === 0){
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'bottom-end',
                    showConfirmButton: false,
                    timer: 2500,
                    timerProgressBar: true,
                });

                Toast.fire({
                    icon: 'error',
                    title: 'Please Enter message to send'
                })
            }else {
                $('.ajax-form').submit();
            }
        });
        // ajax forms
        $(".ajax-form").on("submit", function (e) {
            e.preventDefault();
            let current_url = $(this).attr("action");
            let request_type = $(this).attr("method");
            let payload = $(this).serializeArray();
            $.ajax({
                url: current_url,
                type: request_type,
                data: payload,
                beforeSend: () => {
                    $('.post_message').addClass('disabled');
                    $('#comment').addClass('disabled');
                },
                success: (data) => {
                    $('.post_message').removeClass('disabled');
                    $('#comment').removeClass('disabled');
                    $(this).trigger("reset");
                    if ($('.message').length === 0){
                        $('.no_message_text').addClass('d-none');
                    }
                    if(data.status === true){
                        $('.messages_div').append(data.message_html);
                        $('.messages_div').animate({
                            scrollTop: $('.messages_div')[0].scrollHeight
                        }, 500);
                    }
                }
            });
        });

    </script>

    <script>
        setInterval(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route('employer_received_messages')}}',
                data:{
                    'from_user':  {{ $admin->id }},
                    'to_user':  {{   \Illuminate\Support\Facades\Auth::user()->id }},
                },
                type: "POST",
                success: function (data) {
                    if(data.status){
                        $('.messages_div').append(data.message_html);
                        $('.messages_div').animate({
                            scrollTop: $('.messages_div')[0].scrollHeight
                        }, 500);
                    }
                }
            });

        }, 3000);
    </script>

@endpush

