@extends('employer.layouts.app')
@section('title','Upgrade Package')
@push('css')
    <style>
        .plan-card {
            cursor: pointer;
            border: 2px solid #d8d8d8;
            transition: border-color 0.3s ease;
        }

        .plan-card:hover {
            border-color: #b50201
        }

        .plan-radio:checked + .card-body {
            border-color: #b50201;
            background-color: rgba(181, 2, 1, 0.31);
        }

        .post-resume {
            margin-top: 70px;
        }

        .hidden {
            visibility: hidden;
        }

    </style>
@endpush
@section('content')
    <section class="agency news-section pb-5 custom-container page-section blog-ar">
        <div class="container">
            <div class="row mt-4" style="margin-top: 140px !important;" >
                <div class="col-12 plr-50">
                    <div class="card">
                        <div class="card-header">
                            <h2>Select a Plan</h2>
                        </div>
                        <div class="card-body">
                            @php
                                $allCountries = [];
                                foreach ($plans as $plan) {
                                    $countries = explode(',', $plan->country);
                                    $allCountries = array_merge($allCountries, $countries);
                                }
                                $uniqueCountries = array_unique($allCountries);
                                $countries = collect($uniqueCountries);
                            @endphp
                            <form method="get" action="{{route('pay_stripe',$job)}}">
                                <input type="hidden" name="update_package_id"  id="update_package_id">
                                <div class="row">
                                    <div class="col-md-6 offset-md-2 plr-50">
                                        <div class="form-group">
                                            <label for="family_type">Currency Country*</label>
                                            <div class="input-group ">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" ><i class="fa fa-users"></i></span>
                                                </div>

                                                <select class="form-control" id="currencySelect">
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country }}" @if($country == "HK") selected @endif>{{ $country }}</option>
                                                    @endforeach
                                                </select>

                                                <span class="invalid-feedback family_type_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="plansContainer">
                                    @foreach($plans as $plan)
                                        @php
                                            $manualPrices = explode(',', $plan->manual_price);
                                            $countryList = explode(',', $plan->country);
                                            if (count($countryList) === count($manualPrices)) {
                                                $countryPriceMap = array_combine($countryList, $manualPrices);
                                            } else {
                                                $countryPriceMap = [];
                                            }
                                            $features = explode(',', $plan->features);
                                        @endphp
                                        @if((int)$plan->price != 0)
                                            <div class="col-md-4 blog-contain" data-plan-id="{{$plan->id}}" data-country="{{ $plan->country }}" data-prices='@json($countryPriceMap)'>
                                                <div class="card mb-4 plan-card">
                                                    <input type="radio" name="currency" class="currency-input" value="" id="country_currency_{{ $plan->id }}" hidden>
                                                    <input type="radio" class="plan-radio" name="plan_id" id="plan_{{ $plan->id }}" value="{{ $plan->id }}" hidden >
                                                    <input type="radio" name="converted_price" class="converted_price-input" value="" id="currency_{{ $plan->id }}" hidden>

                                                    <label style="margin-bottom: 0px !important;" for="plan_{{ $plan->id }}" class="card-body">
                                                        <h3 class="card-title">{{ $plan->name }}</h3>
                                                        <p class="card-text">Duration: {{ $plan->post_duration }} days</p>
                                                        <h4 class="badge badge-danger text-light" style="font-size: medium;" data-price="{{ $plan->price }}">
                                                            Price: <span class="currency">Hk</span>
                                                            <span class="large">{{ $plan->price }}</span>
                                                        </h4>
                                                        <p class="mt-2"> {!!  $plan->features !!}</p>
                                                    </label>
{{--                                                    <ul class="list-group">--}}
{{--                                                        @foreach($features as $feature)--}}
{{--                                                            <li class="list-group-item">{{$feature}}</li>--}}
{{--                                                        @endforeach--}}
{{--                                                    </ul>--}}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center ">
                                        <button type="submit" class="btn btn-primary bg-primary text-white disabled" id="proceed">Proceed</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')


    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            const currencySelect = document.getElementById('currencySelect');
            const plansContainer = document.getElementById('plansContainer');
            const plans = plansContainer.getElementsByClassName('blog-contain');

            currencySelect.addEventListener('change', function () {
                const selectedCountry = this.value;

                for (let i = 0; i < plans.length; i++) {
                    const plan = plans[i];
                    const planCountries = plan.getAttribute('data-country').split(',');
                    const prices = JSON.parse(plan.getAttribute('data-prices') || '{}');
                    const currencyInput = plan.querySelector('input[name="currency"]');
                    const converted_priceInput = plan.querySelector('input[name="converted_price"]');
                    const currencySpan = plan.querySelector('.currency');
                    const priceSpan = plan.querySelector('.large');

                    if (planCountries.includes(selectedCountry)) {
                        const newPrice = prices[selectedCountry] || priceSpan.dataset.price;
                        const countryCode = selectedCountry; // Assuming the country name itself is the code

                        priceSpan.textContent = newPrice;
                        currencySpan.textContent = `${countryCode}`;
                        currencyInput.value = countryCode; // Set the currency input value
                        converted_priceInput.value = newPrice; // Set the converted price input value
                        plan.style.display = 'block';
                    } else {
                        plan.style.display = 'none';
                    }
                }
            });

            plansContainer.addEventListener('change', function (event) {
                if (event.target.classList.contains('plan-radio')) {
                    const selectedPlanId = event.target.value;
                    const selectedPlan = document.getElementById(`currency_${selectedPlanId}`);
                    const selectedCountry = document.getElementById(`country_currency_${selectedPlanId}`);

                    // Uncheck all converted price inputs
                    const convertedPriceInputs = document.querySelectorAll('input[name="converted_price"]');
                    const convertedCountryInputs = document.querySelectorAll('input[name="currency"]');
                    convertedPriceInputs.forEach(input => input.checked = false);
                    convertedCountryInputs.forEach(input => input.checked = false);

                    // Check the corresponding converted price input
                    selectedPlan.checked = true;
                    selectedCountry.checked = true;
                }
            });
        });


        $('.blog-contain').click(function(e){
            $('#update_package_id').val($(this).attr('data-plan-id'));
            $('#proceed').removeClass('disabled');
            $('.plan-card').each(function() {
                $(this).removeClass('border-danger');
            });
            $(this).find('.plan-card').addClass('border-danger');
        });
    </script>
@endpush
