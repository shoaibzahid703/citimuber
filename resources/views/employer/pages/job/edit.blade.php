@extends('employer.layouts.app')
@section('title','Submit Job Offer')
@push('css')
    <link href="https://cdn.jsdelivr.net/npm/smartwizard@6/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />
    <style>
        .plan-card {
            cursor: pointer;
            border: 2px solid #d8d8d8;
            transition: border-color 0.3s ease;
        }

        .plan-card:hover {
            border-color: #b50201
        }

        .plan-radio:checked + .card-body {
            border-color: #b50201;
            background-color: rgba(181, 2, 1, 0.31);
        }
        .sw>.nav {
            padding-left: 30px;
        }

        .post-resume {
            margin-top: 70px;
        }
        .sw-theme-arrows>.nav .nav-link.default {
            color: #b0b0b1 !important;
        }

        .sw-theme-arrows>.nav .nav-link.active {
            color: white !important;
        }
        .sw-theme-arrows>.nav .nav-link.done {
            color: white !important;
        }
        .sw>.nav .nav-link {
            font-size: unset !important;
        }
        .sw>.nav .nav-link>.num {
            font-size: 18px;
        }
        .iti--allow-dropdown {
            width: 100% !important;
        }
        .hide{
            display: none;
        }
        .valid-msg {
            color: #00c900;
        }
        .error-msg {
            color: red;
        }
        input[type="date"]:before {
            top: unset !important;
        }
        .select2-container .select2-search--inline .select2-search__field {
            border: none !important;
        }
        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #ced4da !important;
        }
        .select2-container--default.select2-container--focus .select2-selection--single {
            border: 1px solid #ced4da !important;
        }
        .select2-container .select2-selection--multiple {
            min-height: 37px !important;
        }
        .select2-container .select2-selection--single {
            min-height: 37px !important;
        }
        .select2-container--default .select2-selection--multiple {
            border: 1px solid #ced4da !important
        }
        .select2-container--default .select2-selection--single {
            border: 1px solid #ced4da !important
        }
        .select2-container .select2-search--inline .select2-search__field {
            margin-top: 9px;
            width: unset !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            margin-top: 5px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 5px;
        }

        .tab-content {
            height: auto !important;
        }
        .was-validated .form-control:invalid, .form-control.is-invalid {
            border-color: #dc3545 !important;
        }

        .irs--flat .irs-handle>i:first-child {
            background-color: #b50201 !important;//Replace With Your color code
        }
        .irs--flat .irs-bar {
            background-color: #b50201 !important;//Replace With Your color code
        }
        .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {
            background-color: #b50201 !important;//Replace With Your color code
        }
        .irs--flat .irs-from:before, .irs--flat .irs-to:before, .irs--flat .irs-single:before {
            border-top-color: #b50201 !important;//Replace With Your color code
        }
        .radio-button-label > input {
            display: none;
        }
        .radio-button-label > img {
            cursor: pointer;
            border: 3px solid #ddd;
            width: 150px;
        }
        .radio-button-label > input:checked + img {
            border: 3px solid #2885bb;
        }
        .select_2_invalid_feedback{
            display: block !important;
        }
        .btn-success{
            background-color: #28a745 !important;
            border-color: #28a745 !important;
            color: white !important;
        }
        .sw-theme-arrows>.nav .nav-link.active {
            color: white !important;
            border-color: #b50201;
            background-color: #b50201;
            cursor: pointer;
        }
        .sw-theme-arrows>.nav .nav-link.active::after {
            border-left-color: #b50201;
        }
        .sw-theme-arrows>.nav .nav-link.done {
            border-color: #064a84;
            background-color: #064a84;
        }
        .sw-theme-arrows>.nav .nav-link.done::after {
            border-left-color: #064a84;
        }
        .sw>.progress>.progress-bar {
            background-color: #b50000;
        }
        .sw .toolbar>.sw-btn {
            background-color: #b50200;
            border: 1px solid #b50200;
        }

        .hidden {
            visibility: hidden;
        }

    </style>
@endpush
@section('content')
    <section class="agency news-section pb-5 custom-container page-section blog-ar">
        <div class="container">

            <div class="row" style="margin-top: 140px !important;">
                <div class="col-md-12 text-center">
                    <div class="alert alert-success" role="alert">
                        Post your job offer and get a full access to all our candidates
                    </div>
                </div>
            </div>


            <div class="row mt-4" >
                <div class="col-12 plr-50">
                    <div id="smartwizard">
                        <ul class="nav nav-progress">
                            <li class="nav-item">
                                <a class="nav-link" href="#step-1">
                                    <div class="num">Job Requirement</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#step-2">
                                    <span class="num">About You</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#step-3">
                                    <span class="num">Education & Experience</span>
                                </a>
                            </li>
                            {{--                            <li class="nav-item">--}}
                            {{--                                <a class="nav-link" href="#step-4">--}}
                            {{--                                    <span class="num">Subscribe & Publish</span>--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}

                        </ul>
                        <form method="post" action="{{ route('job_update', $job) }}" id="register-form" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="residential_type" value="1">
                            <div class="tab-content">
                                <div id="step-1" data-url="{{route('validateJobStepOne')}}" class="tab-pane" role="tabpanel" aria-labelledby="step-1" style="padding: 0.8rem;">
                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;"> Basic Information </h3><br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="position_offered">Position Offered</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-level-up"></i></span>
                                                    </div>
                                                    <select class="form-control" name="position_offered" id="position_offered">
                                                        <option value="Domestic Helper" {{ $job->position_offered == 'Domestic Helper' ? 'selected' : '' }}>Domestic Helper</option>
                                                        <option value="Driver" {{ $job->position_offered == 'Driver' ? 'selected' : '' }}>Driver</option>
                                                    </select>
                                                    <span class="invalid-feedback position_offered_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="job_type"> Job Type</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-tasks"></i></span>
                                                    </div>
                                                    <select class="form-control" name="job_type" id="job_type">
                                                        <option value="">Select Option</option>
                                                        <option value="Full Time" {{ $job->job_type == 'Full Time' ? 'selected' : '' }}>Full Time</option>
                                                        <option value="Part Time" {{ $job->job_type == 'Part Time' ? 'selected' : '' }}>Part Time</option>
                                                        <option value="Temporary" {{ $job->job_type == 'Temporary' ? 'selected' : '' }}>Temporary</option>
                                                    </select>
                                                    <span class="invalid-feedback job_type_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="offer_location">Where is your Job Offer Location</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-globe"></i></span>
                                                    </div>
                                                    <select class="form-control select2" name="offer_location" id="offer_location">
                                                        <option value=""></option>
                                                        @foreach($country as $countries)
                                                            <option value="{{ $countries->id }}" {{ $job->offer_location == $countries->id ? 'selected' : '' }}>
                                                                {{ $countries->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback offer_location_invalid_feedback select_2_invalid_feedback" role="alert">
            <strong></strong>
        </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4" id="state-container" style="display: none;">
                                            <div class="form-group">
                                                <label for="state">Select State</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-map-o"></i></span>
                                                    </div>
                                                    <select class="form-control" name="state" id="state">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback state_invalid_feedback select_2_invalid_feedback" role="alert">
            <strong></strong>
        </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4" id="city-container" style="display: none;">
                                            <div class="form-group">
                                                <label for="city">Select City</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas fa-city"></i></span>
                                                    </div>
                                                    <select class="form-control" name="city" id="city">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                                <span class="invalid-feedback city_invalid_feedback select_2_invalid_feedback" role="alert">
            <strong></strong>
        </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="job_start_date">What is your job start date ?</label>
                                                <input type="date" class="form-control" name="job_start_date" id="job_start_date" value="{{$job->job_start_date}}" />
                                                <span class="invalid-feedback question_1_invalid_feedback" role="alert">
                                                    <strong></strong>
                                                  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="start_date_flexibility">Start Date Flexibility</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-check"></i></span>
                                                    </div>
                                                    <select class="form-control" name="start_date_flexibility" id="start_date_flexibility">
                                                        <option value="">Select Flexibility</option>
                                                        <option value="2_weeks" {{ $job->start_date_flexibility == '2_weeks' ? 'selected' : '' }}>2 Weeks</option>
                                                        <option value="1_month" {{ $job->start_date_flexibility == '1_month' ? 'selected' : '' }}>1 Month</option>
                                                        <option value="2_month" {{ $job->start_date_flexibility == '2_month' ? 'selected' : '' }}>2 Month</option>
                                                        <option value="3_month" {{ $job->start_date_flexibility == '3_month' ? 'selected' : '' }}>3 Month</option>
                                                    </select>

                                                    <span class="invalid-feedback start_date_flexibility_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;"> Required Skills & Duties </h3><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="languages">Language </label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-language"></i></span>
                                                    </div>
                                                    <select class="form-control" name="languages[]" id="languages" multiple autocomplete="false">
                                                        @foreach($language as $lang)
                                                            @php
                                                                $selectedLanguages = explode(',', $job->languages);
                                                            @endphp
                                                            <option value="{{ $lang->id }}" {{ in_array($lang->id, $selectedLanguages) ? 'selected' : '' }}>
                                                                {{ $lang->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <span class="invalid-feedback languages_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="main_skills">Main Skills *</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-columns"></i></span>
                                                    </div>
                                                    <select class="form-control" name="main_skills[]" id="main_skills" multiple autocomplete="false">
                                                        @foreach($main_skills as $main_skill)
                                                            @php
                                                                $selectedMainSkills = explode(',', $job->main_skills);
                                                            @endphp

                                                            <option value="{{ $main_skill->id }}" {{ in_array($main_skill->id, $selectedMainSkills) ? 'selected' : '' }}>
                                                                {{ $main_skill->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <span class="invalid-feedback main_skills_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cooking_skills">Cooking Skills </label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-home"></i></span>
                                                    </div>
                                                    <select class="form-control" name="cooking_skills[]" id="cooking_skills" multiple autocomplete="false">
                                                        @foreach($cookingSkills as $cookingSkill)
                                                            @php
                                                                $selectedCookingSkills = explode(',', $job->cooking_skills); // Convert comma-separated string to array
                                                            @endphp

                                                            <option value="{{ $cookingSkill->id }}" {{ in_array($cookingSkill->id, $selectedCookingSkills) ? 'selected' : '' }}>
                                                                {{ $cookingSkill->name }}
                                                            </option>
                                                        @endforeach

                                                    </select>

                                                </div>
                                                <span class="invalid-feedback cooking_skills_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="other_skills">Other Skills *</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-pencil"></i></span>
                                                    </div>
                                                    <select class="form-control" name="other_skills[]" id="other_skills" multiple autocomplete="false">
                                                        @foreach($otherSkills as $otherSkill)
                                                            @php
                                                                $selectedOtherSkills = explode(',', $job->other_skills); // Convert comma-separated string to array
                                                            @endphp

                                                            <option value="{{ $otherSkill->id }}" {{ in_array($otherSkill->id, $selectedOtherSkills) ? 'selected' : '' }}>
                                                                {{ $otherSkill->name }}
                                                            </option>
                                                        @endforeach

                                                    </select>

                                                </div>
                                                <span class="invalid-feedback other_skills_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;"> Candidate Preferences  </h3><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="prefer_candidate_location">Preferred Candidate location </label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-globe"></i></span>
                                                    </div>
                                                    <select class="form-control" name="prefer_candidate_location" id="prefer_candidate_location">
                                                        <option value="any_location" {{ $job->prefer_candidate_location == 'any_location' ? 'selected' : '' }}> Any Location </option>
                                                        <option value="present_in_my_country" {{ $job->prefer_candidate_location == 'present_in_my_country' ? 'selected' : '' }}> Present in my Country </option>
                                                        <option value="only_overseas" {{ $job->prefer_candidate_location == 'only_overseas' ? 'selected' : '' }}> Only Overseas </option>
                                                    </select>

                                                    <span class="invalid-feedback prefer_candidate_location_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contract_status">Preferred Contract Status</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-bars"></i></span>
                                                    </div>
                                                    <select class="form-control" name="contract_status" id="contract_status">
                                                        <option value="">Select Option</option>
                                                        <option value="any_situation" {{ $job->contract_status == 'any_situation' ? 'selected' : '' }}> Any Situation </option>
                                                        <option value="finished_contract" {{ $job->contract_status == 'finished_contract' ? 'selected' : '' }}> Finished Contract </option>
                                                        <option value="terminated_relocation_financial" {{ $job->contract_status == 'terminated_relocation_financial' ? 'selected' : '' }}> Terminated (Relocation / Financial) </option>
                                                        <option value="terminated_other" {{ $job->contract_status == 'terminated_other' ? 'selected' : '' }}> Terminated (Other) </option>
                                                        <option value="break_contract" {{ $job->contract_status == 'break_contract' ? 'selected' : '' }}> Break Contract </option>
                                                        <option value="transfer" {{ $job->contract_status == 'transfer' ? 'selected' : '' }}> Transfer </option>
                                                        <option value="overseas" {{ $job->contract_status == 'overseas' ? 'selected' : '' }}> Overseas </option>
                                                    </select>

                                                    <span class="invalid-feedback contract_status_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="gender">Gender</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <select class="form-control" name="gender" id="gender">
                                                        <option value="male" {{ $job->gender == 'male' ? 'selected' : '' }}>Male</option>
                                                        <option value="female" {{ $job->gender == 'female' ? 'selected' : '' }}>Female</option>
                                                    </select>

                                                    <span class="invalid-feedback gender_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="preferred_candidate_nationality">Preferred Candidate Nationality</label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-passport"></i></span>
                                                    </div>
                                                    <select class="form-control" name="preferred_candidate_nationality[]" id="preferred_candidate_nationality" multiple autocomplete="false">
                                                        @php
                                                            $selectedNationalities = explode(',', $job->preferred_candidate_nationality); // Convert comma-separated string to array
                                                        @endphp
                                                        @foreach($nationalities as $nationality)
                                                            <option value="{{$nationality->id}}" {{ in_array($nationality->id, $selectedNationalities) ? 'selected' : '' }}>{{$nationality->nationality}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <span class="invalid-feedback preferred_candidate_nationality_invalid_feedback select_2_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="education_level">Minimum Education Level</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-graduation-cap"></i></span>
                                                    </div>
                                                    <select class="form-control" name="education_level" id="education_level">
                                                        <option value="">Select Option</option>
                                                        @foreach($education_levels as $education_level)
                                                        <option value="{{$education_level->name}}" @if($education_level->name == $job->education_level) selected @endif>{{$education_level->name}}</option>
                                                        @endforeach
                                                    </select>

                                                    <span class="invalid-feedback education_level_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="preferred_age">Preferred Age (Year)</label>
                                                <input type="text" class="preferred_age" name="preferred_age" id="preferred_age" value="{{$job->preferred_age}}" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="preferred_experience">Preferred Experience (Year)</label>
                                                <input type="text" class="preferred_experience" name="preferred_experience" id="preferred_experience" value="{{$job->preferred_experience}}" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div id="step-2" data-url="{{route('validateJobStepTwo')}}" class="tab-pane" role="tabpanel" aria-labelledby="step-2" style="padding: 0.8rem;">
                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;"> About You </h3><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="employer_type">Employer type</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-user-circle"></i></span>
                                                    </div>
                                                    <select class="form-control" name="employer_type" id="employer_type">
                                                        <option value="company" {{ $job->employer_type === 'company' ? 'selected' : '' }}>Company</option>
                                                        <option value="family" {{ $job->employer_type === 'family' ? 'selected' : '' }}>Family</option>
                                                        <option value="other" {{ $job->employer_type === 'other' ? 'selected' : '' }}>Other</option>
                                                    </select>

                                                    <span class="invalid-feedback employer_type_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label></label>
                                            <div class="form-check form-switch mt-3">
                                                <input class="form-check-input" type="checkbox" role="switch" name="receive_email" id="receive_email" value="1" {{ $job->receive_email === '1' ? 'checked' : '' }}>
                                                <label class="form-check-label" for="receive_email"> Do you want to receive applicant details by email?</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 @if($job->receive_email != '1') d-none @endif " id="email_div">
                                            <div class="form-group">
                                                <label for="notify_email">Email Address</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-envelope"></i></span>
                                                    </div>
                                                    <input type="email" class="form-control" name="notify_email" id="notify_email" placeholder="Enter Your email" value="{{$job->notify_email}}"  />
                                                    <span class="invalid-feedback notify_email_level_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 @if($job->employer_type != 'family') d-none @endif" id="family_div">
                                            <div class="form-group">
                                                <label for="family_type">Family type</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-users"></i></span>
                                                    </div>
                                                    <select class="form-control" name="family_type" id="family_type">
                                                        <option value="1_adult" {{ $job->family_type === '1_adult' ? 'selected' : '' }}>1 Adult</option>
                                                        <option value="1_adult-1_kid" {{ $job->family_type === '1_adult-1_kid' ? 'selected' : '' }}>1 Adult + 1 Kid</option>
                                                        <option value="1_adult-2_kid" {{ $job->family_type === '1_adult-2_kid' ? 'selected' : '' }}>1 Adult + 2 Kids</option>
                                                        <option value="1_adult-3_kid" {{ $job->family_type === '1_adult-3_kid' ? 'selected' : '' }}>1 Adult + 3 Kids</option>
                                                        <option value="1_adult-4_kid" {{ $job->family_type === '1_adult-4_kid' ? 'selected' : '' }}>1 Adult + 4 Kids</option>
                                                        <option value="2_adults" {{ $job->family_type === '2_adults' ? 'selected' : '' }}>2 Adults</option>
                                                        <option value="2_adults-1_kid" {{ $job->family_type === '2_adults-1_kid' ? 'selected' : '' }}>2 Adults + 1 Kid</option>
                                                        <option value="2_adults-2_kid" {{ $job->family_type === '2_adults-2_kid' ? 'selected' : '' }}>2 Adults + 2 Kids</option>
                                                        <option value="2_adults-3_kid" {{ $job->family_type === '2_adults-3_kid' ? 'selected' : '' }}>2 Adults + 3 Kids</option>
                                                        <option value="2_adults-4_kid" {{ $job->family_type === '2_adults-4_kid' ? 'selected' : '' }}>2 Adults + 4 Kids</option>
                                                        <option value="2_adults-5_kid" {{ $job->family_type === '2_adults-5_kid' ? 'selected' : '' }}>2 Adults + 5 Kids</option>
                                                        <option value="3_adults" {{ $job->family_type === '3_adults' ? 'selected' : '' }}>3 Adults</option>
                                                        <option value="3_adults-1_kid" {{ $job->family_type === '3_adults-1_kid' ? 'selected' : '' }}>3 Adults + 1 Kid</option>
                                                        <option value="3_adults-2_kid" {{ $job->family_type === '3_adults-2_kid' ? 'selected' : '' }}>3 Adults + 2 Kids</option>
                                                        <option value="3_adults-3_kid" {{ $job->family_type === '3_adults-3_kid' ? 'selected' : '' }}>3 Adults + 3 Kids</option>
                                                        <option value="3_adults-4_kid" {{ $job->family_type === '3_adults-4_kid' ? 'selected' : '' }}>3 Adults + 4 Kids</option>
                                                        <option value="3_adults-5_kid" {{ $job->family_type === '3_adults-5_kid' ? 'selected' : '' }}>3 Adults + 5 Kids</option>
                                                        <option value="4_adults" {{ $job->family_type === '4_adults' ? 'selected' : '' }}>4 Adults</option>
                                                        <option value="4_adults-1_kid" {{ $job->family_type === '4_adults-1_kid' ? 'selected' : '' }}>4 Adults + 1 Kid</option>
                                                        <option value="4_adults-2_kid" {{ $job->family_type === '4_adults-2_kid' ? 'selected' : '' }}>4 Adults + 2 Kids</option>
                                                        <option value="4_adults-3_kid" {{ $job->family_type === '4_adults-3_kid' ? 'selected' : '' }}>4 Adults + 3 Kids</option>
                                                        <option value="4_adults-4_kid" {{ $job->family_type === '4_adults-4_kid' ? 'selected' : '' }}>4 Adults + 4 Kids</option>
                                                        <option value="4_adults-5_kid" {{ $job->family_type === '4_adults-5_kid' ? 'selected' : '' }}>4 Adults + 5 Kids</option>
                                                        <option value="5_adults" {{ $job->family_type === '5_adults' ? 'selected' : '' }}>5 Adults</option>
                                                        <option value="other" {{ $job->family_type === 'other' ? 'selected' : '' }}>Other</option>
                                                    </select>

                                                    <span class="invalid-feedback family_type_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 @if($job->employer_type != 'family') d-none @endif" id="pets_div">
                                            <div class="form-group">
                                                <label for="pets">Do you have any Pets</label>
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fas fa-feather"></i></span>
                                                    </div>
                                                    <select class="form-control" name="pets" id="pets">
                                                        <option value="yes" {{ $job->pets === 'yes' ? 'selected' : '' }}>Yes</option>
                                                        <option value="no" {{ $job->pets === 'no' ? 'selected' : '' }}>No</option>
                                                    </select>

                                                    <span class="invalid-feedback pets_invalid_feedback" role="alert">
                                                <strong></strong>
                                              </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 @if($job->employer_type != 'family') d-none @endif" id="employer_nationality_div">
                                            <div class="form-group">
                                                <label for="employer_nationality">
                                                    Nationality
                                                </label>
                                                <div class="input-group" style="flex-wrap: nowrap;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fas fa-globe"></i></span>
                                                    </div>
                                                    <select class="form-control" name="employer_nationality" id="employer_nationality">
                                                        <option value=""></option>
                                                        @foreach($nationalities as $nation)
                                                            <option value="{{$nation->id}}" {{ $job->employer_nationality == $nation->id ? 'selected' : '' }}>{{$nation->nationality}}</option>
                                                        @endforeach
                                                    </select>

                                                    <span class="invalid-feedback employer_nationality_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class=" font-weight-bold py-1 my-1 text-white  text-center" style="background-color: #054a84; !important;">  What do you offer to the right candidates?  </h3><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="day_off">Day off</label>
                                                <div class="input-group" >
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-sign-out"></i></span>
                                                    </div>
                                                    <select class="form-control" name="day_off" id="day_off">
                                                        <option value="">Select Option</option>
                                                        <option value="flexible" {{ $job->day_off == 'flexible' ? 'selected' : '' }}>Flexible</option>
                                                        <option value="to_be_discussed" {{ $job->day_off == 'to_be_discussed' ? 'selected' : '' }}>To be discussed</option>
                                                        <option value="monday" {{ $job->day_off == 'monday' ? 'selected' : '' }}>Monday</option>
                                                        <option value="tuesday" {{ $job->day_off == 'tuesday' ? 'selected' : '' }}>Tuesday</option>
                                                        <option value="wednesday" {{ $job->day_off == 'wednesday' ? 'selected' : '' }}>Wednesday</option>
                                                        <option value="thursday" {{ $job->day_off == 'thursday' ? 'selected' : '' }}>Thursday</option>
                                                        <option value="friday" {{ $job->day_off == 'friday' ? 'selected' : '' }}>Friday</option>
                                                        <option value="saturday" {{ $job->day_off == 'saturday' ? 'selected' : '' }}>Saturday</option>
                                                        <option value="sunday" {{ $job->day_off == 'sunday' ? 'selected' : '' }}>Sunday</option>
                                                        <option value="saturday-sunday" {{ $job->day_off == 'saturday-sunday' ? 'selected' : '' }}>Saturday-Sunday</option>
                                                        <option value="friday-Saturday" {{ $job->day_off == 'friday-Saturday' ? 'selected' : '' }}>Friday-Saturday</option>
                                                    </select>

                                                    <span class="invalid-feedback day_off_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="accomodation">Accomodation</label>
                                                <div class="input-group" >
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" ><i class="fa fa-hotel"></i></span>
                                                    </div>
                                                    <select class="form-control" name="accomodation" id="accomodation">
                                                        <option value="">Select Option</option>
                                                        <option value="live_out" {{ $job->accomodation == 'live_out' ? 'selected' : '' }}>Live Out</option>
                                                        <option value="flexible" {{ $job->accomodation == 'flexible' ? 'selected' : '' }}>Flexible</option>
                                                        <option value="to_be_discussed" {{ $job->accomodation == 'to_be_discussed' ? 'selected' : '' }}>To be Discussed</option>
                                                        <option value="live_in_separate_room" {{ $job->accomodation == 'live_in_separate_room' ? 'selected' : '' }}>Live In - Separate room</option>
                                                        <option value="live_in_share_room" {{ $job->accomodation == 'live_in_share_room' ? 'selected' : '' }}>Live In - Share room</option>
                                                        <option value="live_in" {{ $job->accomodation == 'live_in' ? 'selected' : '' }}>Live In</option>
                                                    </select>

                                                    <span class="invalid-feedback accomodation_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-12">
                                            <label>Monthly Salary Offer</label>
                                            <br>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="salary_offer_dont_mention" name="salary_offer" class="custom-control-input salary_offer" value="dont_mention" {{ $job->salary_offer == 'dont_mention' ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="salary_offer_dont_mention">Don't Mention</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="salary_offer_range" name="salary_offer" class="custom-control-input salary_offer" value="range" {{ $job->salary_offer == 'range' ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="salary_offer_range">Range</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="salary_offer_fix" name="salary_offer" class="custom-control-input salary_offer" value="fix" {{ $job->salary_offer == 'fix' ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="salary_offer_fix">Fix</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="salary_offer_other" name="salary_offer" class="custom-control-input salary_offer" value="other" {{ $job->salary_offer == 'other' ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="salary_offer_other">Other</label>
                                            </div>
                                        </div>

                                        <div class="col-md-12 mt-1 @if($job->salary_offer == 'range') @else  d-none @endif" id="range_div">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="range_min_salary">Range Min Salary</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fas fa-hand-holding-usd"></i></span>
                                                            </div>
                                                            <input type="number" class="form-control" name="range_min_salary" id="range_min_salary" placeholder="min salary" value="{{$job->range_min_salary}}" />
                                                            <span class="invalid-feedback range_min_salary_level_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="range_max_salary">Range Max Salary</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fas fa-hand-holding-usd"></i></span>
                                                            </div>
                                                            <input type="number" class="form-control" name="range_max_salary" id="range_max_salary" value="{{$job->range_max_salary}}" placeholder="max salary" />
                                                            <span class="invalid-feedback range_max_salary_level_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="range_currency">Currency </label>
                                                        <div class="input-group" style="flex-wrap: nowrap;">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa fa-money"></i></span>
                                                            </div>
                                                            <select class="form-control" name="range_currency" id="range_currency">
                                                                <option value="">Select Option</option>
                                                                @foreach($currency as $curr)
                                                                    <option value="{{$curr->id}}" {{ $job->range_currency == $curr->id ? 'selected' : '' }}>{{$curr->name}}</option>
                                                                @endforeach
                                                            </select>

                                                            <span class="invalid-feedback range_currency_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-1 @if($job->salary_offer == 'fix') @else  d-none @endif" id="fix_div">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fix_monthly_salary">Fix Monthly Salary</label>
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fas fa-hand-holding-usd"></i></span>
                                                            </div>
                                                            <input type="number" value="{{$job->fix_monthly_salary}}" class="form-control" name="fix_monthly_salary" id="fix_monthly_salary" placeholder="fix salary" />
                                                            <span class="invalid-feedback fix_monthly_salary_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fix_currency"> Currency </label>
                                                        <div class="input-group" style="flex-wrap: nowrap;">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa fa-money"></i></span>
                                                            </div>
                                                            <select class="form-control" name="fix_currency" id="fix_currency">
                                                                <option value="">Select Option</option>
                                                                @foreach($currency as $curr)
                                                                    <option value="{{$curr->id}}" {{ $job->fix_currency == $curr->id ? 'selected' : '' }}>{{$curr->name}}</option>
                                                                @endforeach
                                                            </select>

                                                            <span class="invalid-feedback fix_currency_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-1  @if($job->salary_offer == 'other') @else  d-none @endif" id="other_div">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="salary_description">Salary Description  </label>
                                                        <div class="input-group" style="flex-wrap: nowrap;">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" ><i class="fa fa-money"></i></span>
                                                            </div>
                                                            <select class="form-control" name="salary_description" id="salary_description">
                                                                <option value="">Select Option</option>
                                                                <option value="based_on_experience" {{ $job->salary_description == 'based_on_experience' ? 'selected' : '' }}>Based on experience</option>
                                                                <option value="based_on_government_law" {{ $job->salary_description == 'based_on_government_law' ? 'selected' : '' }}>Based on government law</option>
                                                                <option value="to_be_discussed" {{ $job->salary_description == 'to_be_discussed' ? 'selected' : '' }}>To be discussed</option>
                                                            </select>

                                                            <span class="invalid-feedback salary_description_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-3" data-url="{{route('validateJobStepThree')}}" class="tab-pane" role="tabpanel" aria-labelledby="step-3" style="padding: 0.8rem;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Select Job Picture</label>
                                            <div class=" d-flex justify-content-between" style="overflow-x: auto ; scrollbar-width: thin;">
                                                @foreach($job_pictures as $key => $job_picture)
                                                    <label class="radio-button-label">
                                                        <input type="radio" name="job_picture" value="{{$job_picture->id}}" @if($job->job_picture == $job_picture->id) checked @endif/>
                                                        <img src="{{asset('storage/job_picture/'.$job_picture->img)}}">
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-md-6 offset-md-3">
                                            <div class="form-group">
                                                <label for="job_title">Job Title</label>
                                                <input type="text" class="form-control" name="job_title" id="job_title" placeholder="job title" value="{{$job->job_title}}">
                                                <span class="invalid-feedback job_title_invalid_feedback" role="alert">
                                                          <strong></strong>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 offset-md-3 mb-3">
                                            <label for="job_description">Job Description</label>
                                            <!-- <div id="editor-container"></div>
                                            <span class="invalid-feedback job_description_invalid_feedback" role="alert">
                                                                                              <strong></strong>
                                            </span>
                                            <input type="hidden" name="job_description" value="{{$job->job_description}}"> -->
                                            <textarea id="letter_format" class="form-control form-control-sm"  name="job_description">{{$job->job_description}}</textarea>
                                        </div>
                                        <div class="col-md-6 offset-md-3" style="margin-top: 65px">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="newsletter" class="custom-control-input" id="switch1" value="1" >
                                                <label class="custom-control-label" for="switch1"> Subscribe to our tips and newsletters? </label>
                                            </div>
                                        </div>

                                        <div class="col-md-6 offset-md-3">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="opportunities" class="custom-control-input" id="switch2" value="1">
                                                <label class="custom-control-label" for="switch2"> Receive privileged and discount offers. ?</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
{{--                                <div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4" style="padding: 0.8rem;">--}}
{{--                                    @php--}}
{{--                                        $get_plans = App\Models\Transaction::where('job_id', $job->id)->get();--}}
{{--                                    @endphp--}}
{{--                                    @if($get_plans->isEmpty())--}}
{{--                                        <div class="container">--}}
{{--                                            <h2>Select a Plan</h2>--}}
{{--                                            @php--}}
{{--                                                $plans = App\Models\Plans::all();--}}
{{--                                                 $allCountries = [];--}}
{{--                                                 foreach ($plans as $plan) {--}}
{{--                                                     $countries = explode(',', $plan->country);--}}
{{--                                                     $allCountries = array_merge($allCountries, $countries);--}}
{{--                                                 }--}}
{{--                                                 $uniqueCountries = array_unique($allCountries);--}}
{{--                                                 $countries = collect($uniqueCountries);--}}
{{--                                            @endphp--}}
{{--                                            <div class="row">--}}
{{--                                                <div class="col-md-6 offset-md-2 plr-50">--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <label for="family_type">Currency *</label>--}}
{{--                                                        <div class="input-group ">--}}
{{--                                                            <div class="input-group-prepend">--}}
{{--                                                                <span class="input-group-text" ><i class="fa fa-credit-card" aria-hidden="true"></i></span>--}}
{{--                                                            </div>--}}

{{--                                                            <select class="form-control" id="currencySelect">--}}
{{--                                                                @foreach($countries as $country)--}}
{{--                                                                    <option value="{{ $country }}" @if($country == "HK") selected @endif>{{ $country }}</option>--}}
{{--                                                                @endforeach--}}
{{--                                                            </select>--}}

{{--                                                            <span class="invalid-feedback family_type_invalid_feedback" role="alert">--}}
{{--                                                <strong></strong>--}}
{{--                                              </span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                            <div class="row" id="plansContainer">--}}
{{--                                                @foreach($plans as $plan)--}}
{{--                                                    @php--}}
{{--                                                        $manualPrices = explode(',', $plan->manual_price);--}}
{{--                                                        $countryList = explode(',', $plan->country);--}}
{{--                                                        $countryPriceMap = array_combine($countryList, $manualPrices);--}}
{{--                                                        $features = explode(',', $plan->features);--}}

{{--                                                    @endphp--}}

{{--                                                    <div class="col-md-4 blog-contain" data-country="{{ $plan->country }}" data-prices='@json($countryPriceMap)'>--}}
{{--                                                        <div class="card mb-4 plan-card">--}}
{{--                                                            <input type="hidden" name="currency" class="currency-input" value="">--}}
{{--                                                            <input type="radio" class="plan-radio" name="plan_id" id="plan_{{ $plan->id }}" value="{{ $plan->id }}" hidden required>--}}
{{--                                                            <input type="radio" name="converted_price" class="converted_price-input" value="" id="currency_{{ $plan->id }}" hidden>--}}
{{--                                                            <label style="margin-bottom: 0px !important;" for="plan_{{ $plan->id }}" class="card-body">--}}
{{--                                                                <h3 class="card-title">{{ $plan->name }}</h3>--}}
{{--                                                                <p class="card-text">Duration: {{ $plan->post_duration }} days</p>--}}
{{--                                                                <h4 class="badge badge-danger text-light" style="font-size: medium;" data-price="{{ $plan->price }}">Price:--}}
{{--                                                                    <span class="currency">Hk</span>--}}
{{--                                                                    <span class="large">{{ $plan->price }}</span>--}}
{{--                                                                </h4>--}}
{{--                                                            </label>--}}
{{--                                                            <ul class="list-group">--}}
{{--                                                                @foreach($features as $feature)--}}
{{--                                                                    <li class="list-group-item">{{$feature}}</li>--}}
{{--                                                                @endforeach--}}
{{--                                                            </ul>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                @endforeach--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
                            </div>
                        </form>

                        <!-- Include optional progressbar HTML -->
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/smartwizard@6/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput.min.js"></script>

    <!-- Script -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>

    <script>
        $(function() {
            // SmartWizard initialize
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'arrows',
                justified: true,
                enableUrlHash: false,
                autoAdjustHeight: true,
                anchor: {
                    enableNavigation: false,
                },
                transition: {
                    animation: 'fade',
                },
                toolbar: {
                    showNextButton: true, // show/hide a Next button
                    showPreviousButton: true, // show/hide a Previous button
                    position: 'bottom', // none/ top/ both bottom
                    extraHtml: `<button class="btn btn-success tn-sm" id="btnFinish" disabled  style="padding: 6px">Finish</button>`
                },
            });
            $("#smartwizard").on("showStep", function(e, anchorObject, stepIndex, stepDirection, stepPosition) {
                if (stepPosition === "last") {
                    $('#btnFinish').prop('disabled', false)
                    $('.sw-btn-next').hide();
                } else {
                    $('#btnFinish').prop('disabled', true)
                    $('.sw-btn-next').show();
                }
            });
            // Leave step event is used for validating the forms
            $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIdx, nextStepIdx, stepDirection) {
                // Validate only on forward movement
                if(currentStepIdx === 0 &&  nextStepIdx === 1) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    const url = $('#step-1').data("url");
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            'position_offered': $("#position_offered").children("option:selected").val(),
                            'job_type': $("#job_type").children("option:selected").val(),
                            'offer_location': $("#offer_location").children("option:selected").val(),
                            'state': $("#state").children("option:selected").val(),
                            'city': $("#city").children("option:selected").val(),
                            'job_start_date': $("#job_start_date").val(),
                            'start_date_flexibility': $("#start_date_flexibility").children("option:selected").val(),
                            'languages': $("#languages").children("option:selected").val(),
                            'main_skills': $("#main_skills").children("option:selected").val(),
                            'cooking_skills': $("#cooking_skills").children("option:selected").val(),
                            'other_skills': $("#other_skills").children("option:selected").val(),
                            'prefer_candidate_location': $("#prefer_candidate_location").children("option:selected").val(),
                            'contract_status': $("#contract_status").children("option:selected").val(),
                            'gender': $("#gender").children("option:selected").val(),
                            'preferred_candidate_nationality': $("#preferred_candidate_nationality").children("option:selected").val(),
                            'education_level': $("#education_level").children("option:selected").val(),
                        },
                        beforeSend: function (xhr) {
                            $('#step-1').find("input, select, textarea").removeClass("is-invalid");
                            $('#step-1').find("span.invalid-feedback").text("");
                            $('#smartwizard').smartWizard("loader", "show");
                        },
                        success: function (data) {
                            if (data.error) {
                                let messages = data.messages;
                                Object.keys(messages).forEach(function (key) {
                                    $("#" + key).addClass("is-invalid");
                                    $("." + key + "_invalid_feedback").text(messages[key]);
                                });
                                // Hide the loader
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#smartwizard').smartWizard("goToStep", 0, true);
                                $('#smartwizard').smartWizard("setState", [0], 'error');
                            }else {
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#step1').find("input, select, textarea").removeClass("is-invalid");
                                $('#smartwizard').smartWizard("unsetState", [0], 'error');
                                $('#step-1').find("span.invalid-feedback").text("");
                            }
                        }
                    });
                }
                if(currentStepIdx === 1 &&  nextStepIdx === 2) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    const url = $('#step-2').data("url");

                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            'employer_type': $("#employer_type").children("option:selected").val(),
                            'day_off': $("#day_off").children("option:selected").val(),
                            'accomodation': $("#accomodation").children("option:selected").val(),
                        },
                        beforeSend: function (xhr) {
                            $('#step-2').find("input, select, textarea").removeClass("is-invalid");
                            $('#step-2').find("span.invalid-feedback").text("");
                            $('#smartwizard').smartWizard("loader", "show");
                        },
                        success: function (data) {
                            if (data.error) {
                                let messages = data.messages;
                                Object.keys(messages).forEach(function (key) {
                                    $("#" + key).addClass("is-invalid");
                                    $("." + key + "_invalid_feedback").text(messages[key]);
                                });
                                // Hide the loader
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#smartwizard').smartWizard("goToStep", 1, true);
                                $('#smartwizard').smartWizard("setState", [1], 'error');
                            }else {
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#step2').find("input, select, textarea").removeClass("is-invalid");
                                $('#smartwizard').smartWizard("unsetState", [1], 'error');
                                $('#step2').find("span.invalid-feedback").text("");
                            }
                        }
                    });
                }
                if(currentStepIdx === 2 &&  nextStepIdx === 3) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    const url = $('#step-3').data("url");
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            'job_title': $('#job_title').val(),
                        },
                        beforeSend: function (xhr) {
                            $('#step-3').find("input, select, textarea").removeClass("is-invalid");
                            $('#smartwizard').smartWizard("loader", "show");
                            $('#step-3').find("span.invalid-feedback").text("");
                        },
                        success: function (data) {
                            if (data.error) {
                                let messages = data.messages;
                                Object.keys(messages).forEach(function (key) {
                                    $("#" + key).addClass("is-invalid");
                                    $("." + key + "_invalid_feedback").text(messages[key]);
                                });
                                // Hide the loader
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#smartwizard').smartWizard("goToStep", 2, true);
                                $('#smartwizard').smartWizard("setState", [2], 'error');
                            }else {
                                $('#smartwizard').smartWizard("loader", "hide");
                                $('#step3').find("input, select, textarea").removeClass("is-invalid");
                                $('#smartwizard').smartWizard("unsetState", [2], 'error');
                                $('#step3').find("span.invalid-feedback").text("");
                            }
                        }
                    });
                }

            });
        });
    </script>

    <!-- for education -->
    {{--  phone no with country code --}}
    <script>
        $(document).ready(function() {
            $('#offer_location').select2({
                width: '100%',
                placeholder : "Offer location ",
                allowClear: true,
            });
            $('#employer_nationality').select2({
                width: '100%',
                placeholder : "Employer Nationality",
                allowClear: true,
            });
            $('#state').select2({
                width: '100%',
                placeholder : "State ",
                allowClear: true,
            });
            $('#city').select2({
                width: '100%',
                placeholder : "City",
                allowClear: true,
            });
            $('#languages').select2({
                width: '100%',
                placeholder : "Select Languages",
                allowClear: true,
                multiple: true,
            });
            $('#main_skills').select2({
                width: '100%',
                placeholder : "Select Main Skills",
                allowClear: true,
                multiple: true,
            });
            $('#cooking_skills').select2({
                width: '100%',
                placeholder : "Select Cooking Skills",
                allowClear: true,
                multiple: true,
            });
            $('#other_skills').select2({
                width: '100%',
                placeholder : "Select Other Skills",
                allowClear: true,
                multiple: true,
            });
            $('#preferred_candidate_nationality').select2({
                width: '100%',
                placeholder : "Preferred  Nationality",
                allowClear: true,
                multiple: true,
            });

            $("#preferred_age").ionRangeSlider({
                type: "double",
                min: 18,
                max: 60,
            });

            $("#preferred_experience").ionRangeSlider({
                type: "double",
                min: 0,
                max: 40,
            });


            $( "#employer_type" ).change(function() {
                const employer_type = $("#employer_type").children("option:selected").val();
                if (employer_type  === "family"){
                    $('#family_div').removeClass('d-none');
                    $('#pets_div').removeClass('d-none');
                    $('#employer_nationality_div').removeClass('d-none');
                }else {
                    $('#family_div').addClass('d-none');
                    $('#pets_div').addClass('d-none');
                    $('#employer_nationality_div').addClass('d-none');
                }
            });

            $("#receive_email").on('change', function(){    // 2nd (A)
                const val  =  $('#receive_email:checked').val();
                if(val === "1") {
                    $('#email_div').removeClass('d-none');
                } else{
                    $('#email_div').addClass('d-none');
                }
            });
            $(".salary_offer").on('change', function(){    // 2nd (A)
                const val  =  $('.salary_offer:checked').val();
                if(val === "range") {
                    $('#range_div').removeClass('d-none');
                    $('#fix_div').addClass('d-none');
                    $('#other_div').addClass('d-none');
                } else if(val === "fix"){
                    $('#fix_div').removeClass('d-none');
                    $('#range_div').addClass('d-none');
                    $('#other_div').addClass('d-none');
                }else if(val === "other"){
                    $('#other_div').removeClass('d-none');
                    $('#range_div').addClass('d-none');
                    $('#fix_div').addClass('d-none');
                }else {
                    $('#range_div').addClass('d-none');
                    $('#fix_div').addClass('d-none');
                    $('#other_div').addClass('d-none');
                }
            });

            var toolbarOptions = [
                ['bold', 'italic', 'underline'],        // toggled buttons
                ['blockquote'],
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            ];

            // var quill = new Quill('#editor-container', {
            //     modules: {
            //         toolbar: toolbarOptions,
            //     },
            //     placeholder: 'Job Description',
            //     theme: 'snow'
            // });

        });
    </script>

    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace( 'letter_format', {
            filebrowserUploadMethod: 'form'
        });

    </script>

    <script>
        $(document).ready(function() {
            // Load states if offer_location is pre-selected
            const selectedCountry = $("#offer_location").val();
            if (selectedCountry) {
                loadStates(selectedCountry, {{ $job->state ?? 'null' }});
            }

            // Load cities if state is pre-selected
            const selectedState = {{ $job->state ?? 'null' }};
            if (selectedState) {
                loadCities(selectedState, {{ $job->city ?? 'null' }});
            }

            // Event listener for offer_location change
            $("#offer_location").change(function() {
                const location = $(this).val();
                $('#state').empty().append('<option value=""></option>');
                $('#city').empty().append('<option value=""></option>');
                if (location) {
                    loadStates(location, null);
                } else {
                    $("#state-container").hide();
                    $("#city-container").hide();
                }
            });

            // Event listener for state change
            $("#state").change(function() {
                const state = $(this).val();
                $('#city').empty().append('<option value=""></option>');
                if (state) {
                    loadCities(state, null);
                } else {
                    $("#city-container").hide();
                }
            });
        });

        function loadStates(countryId, selectedStateId) {
            const path = "{{ route('get_states', '') }}/" + countryId;
            $.ajax({
                url: path,
                type: "GET",
                success: function(data) {
                    const states = data.states;
                    let options = '<option value=""></option>';
                    if (states.length > 0) {
                        $.each(states, function(i, value) {
                            options += `<option value="${value.id}" ${selectedStateId == value.id ? 'selected' : ''}>${value.name}</option>`;
                        });
                        $('#state').append(options);
                        $("#state-container").show();
                    } else {
                        $("#state-container").hide();
                        $("#city-container").hide(); // Hide city container if no states
                    }
                }
            });
        }

        function loadCities(stateId, selectedCityId) {
            const path = "{{ route('get_cities', '') }}/" + stateId;
            $.ajax({
                url: path,
                type: "GET",
                success: function(data) {
                    const cities = data.cities;
                    let options = '<option value=""></option>';
                    if (cities.length > 0) {
                        $.each(cities, function(i, value) {
                            options += `<option value="${value.id}" ${selectedCityId == value.id ? 'selected' : ''}>${value.name}</option>`;
                        });
                        $('#city').append(options);
                        $("#city-container").show();
                    } else {
                        $("#city-container").hide();
                    }
                }
            });
        }
    </script>


    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            const currencySelect = document.getElementById('currencySelect');
            const plansContainer = document.getElementById('plansContainer');
            const plans = plansContainer.getElementsByClassName('blog-contain');

            currencySelect.addEventListener('change', function () {
                const selectedCountry = this.value;

                for (let i = 0; i < plans.length; i++) {
                    const plan = plans[i];
                    const planCountries = plan.getAttribute('data-country').split(',');
                    const prices = JSON.parse(plan.getAttribute('data-prices') || '{}');
                    const currencyInput = plan.querySelector('input[name="currency"]');
                    const converted_priceInput = plan.querySelector('input[name="converted_price"]');
                    const currencySpan = plan.querySelector('.currency');
                    const priceSpan = plan.querySelector('.large');

                    if (planCountries.includes(selectedCountry)) {
                        const newPrice = prices[selectedCountry] || priceSpan.dataset.price;
                        const countryCode = selectedCountry; // Assuming the country name itself is the code

                        priceSpan.textContent = newPrice;
                        currencySpan.textContent = `${countryCode}`;
                        currencyInput.value = countryCode; // Set the currency input value
                        converted_priceInput.value = newPrice; // Set the converted price input value
                        plan.style.display = 'block';
                    } else {
                        plan.style.display = 'none';
                    }
                }
            });

            plansContainer.addEventListener('change', function (event) {
                if (event.target.classList.contains('plan-radio')) {
                    const selectedPlanId = event.target.value;
                    const selectedPlan = document.getElementById(`currency_${selectedPlanId}`);

                    // Uncheck all converted price inputs
                    const convertedPriceInputs = document.querySelectorAll('input[name="converted_price"]');
                    convertedPriceInputs.forEach(input => input.checked = false);

                    // Check the corresponding converted price input
                    selectedPlan.checked = true;
                }
            });
        });
    </script>
@endpush
