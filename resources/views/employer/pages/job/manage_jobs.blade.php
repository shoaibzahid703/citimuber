@extends('employer.layouts.app')
@section('title','Manage Jobs')
@section('content')

    <style>
        .top-banner-wrapper {
            margin-top: 97px;
        }
    </style>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <section class="agency blog blog-sec blog-sidebar pb-5 blog-list sider custom-container page-section">
        <div class="container">
            <div class="">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="top-banner-wrapper" style="min-height: auto;">
                                    <div class="top-banner-content small-section ng-star-inserted">
                                        <h1><span>Create and Manage your Job Offers</span></h1>
                                        <p class="header_2"> Create a new Job Offer and start to receive application. Easily check, follow and communicate with all your applications. </p>

                                        <a href="{{route('post_job')}}" class="btn bg-success" style="padding-top: 8px;color: white;">Post Job Offer</a>
                                        <a href="/candidates" class="btn bg-warning" style="padding-top: 8px;color: white;padding-left: 27px;">Find Candidate</a>

                                    </div>
                                    <!---->
                                    <!---->
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div _ngcontent-serverapp-c1132162511="" class="row mt-3">
                <div _ngcontent-serverapp-c1132162511="" class="col-12">
                    <div _ngcontent-serverapp-c1132162511="" class="filter-sort-bar c-filter-bar filters-btn">
                        <div _ngcontent-serverapp-c1132162511="" class="filter-main-btn mb-0"><a _ngcontent-serverapp-c1132162511="" class="filter-button btn-sm"><i _ngcontent-serverapp-c1132162511="" class="fa fa-filter" data-toggle="modal" data-target="#fiterModal"></i>Filter </a></div>
                        <div _ngcontent-serverapp-c1132162511="" class="c-res-job-booster" style="gap: 10px;">
                            <div _ngcontent-serverapp-c1132162511="">
                                <app-order-by _ngcontent-serverapp-c1132162511="" _nghost-serverapp-c1558467617="" ngh="3">
                                    <div _ngcontent-serverapp-c1558467617="" class="dropdown-btn">
                                        <div _ngcontent-serverapp-c1558467617="" class="dropdown">
                                            <!-- <button _ngcontent-serverapp-c1558467617="" class="dropbtn">
                                              <span _ngcontent-serverapp-c1558467617="" class="mr-2"><i _ngcontent-serverapp-c1558467617="" aria-hidden="true" class="fa fa-sort-amount-desc"></i></span><span _ngcontent-serverapp-c1558467617="">Last Active</span>
                                            </button> -->
                                            <!---->
                                        </div>
                                    </div>
                                </app-order-by>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row mt-3" style="clear: both;">
                <div class="col-md-3 col-lg-3 d-none d-md-clock d-lg-block d-xl-block collection-filter-block ng-star-inserted">
                    <app-filter _nghost-serverapp-c2564575186="" ngh="10">
                        <div _ngcontent-serverapp-c2564575186="" class="row product-service_1">
                            <div _ngcontent-serverapp-c2564575186="" class="col-md-12 col-lg-12">
                                <div _ngcontent-serverapp-c2564575186="" class="row filter-ar">
                                    <!---->
                                    <div _ngcontent-serverapp-c2564575186="" class="top-banner-content small-section col-12 ng-star-inserted">
                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container mt-3 mb-1 ng-star-inserted">
                                            <h2 _ngcontent-serverapp-c2564575186="" class="float-left custom_h2"><i _ngcontent-serverapp-c2564575186="" class="fa fa-serch"></i> I'm Looking For </h2>
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container mb-0 ng-star-inserted">
                                            <p _ngcontent-serverapp-c2564575186="" class="float-left filter_text"> Filter </p>
                                            <a href="{{route('manage_jobs')}}">
                                                <p _ngcontent-serverapp-c2564575186="" class="text-success float-right reset_button" style="cursor: pointer;"><i _ngcontent-serverapp-c2564575186="" class="fa fa-repeat"></i> Reset </p>
                                            </a>
                                        </div>

                                        <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3 ng-star-inserted">Job Title</h3>
                                        <form id="job-search-form">
                                            <mat-form-field _ngcontent-serverapp-c2564575186="" appearance="outline" class="mat-mdc-form-field ng-tns-c1205077789-6 mat-mdc-form-field-type-mat-input mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                <!---->
                                                <div class="mat-mdc-text-field-wrapper mdc-text-field ng-tns-c1205077789-6 mdc-text-field--outlined mdc-text-field--no-label">
                                                    <!---->
                                                    <div class="mat-mdc-form-field-flex ng-tns-c1205077789-6">
                                                        <div matformfieldnotchedoutline="" class="mdc-notched-outline ng-tns-c1205077789-6 mdc-notched-outline--no-label ng-star-inserted" ngh="0">
                                                            <div class="mdc-notched-outline__leading"></div>
                                                            <div class="mdc-notched-outline__notch">
                                                                <!---->
                                                                <!---->
                                                                <!---->
                                                            </div>
                                                            <div class="mdc-notched-outline__trailing"></div>
                                                        </div>
                                                        <!---->
                                                        <!---->
                                                        <!---->
                                                        <div class="mat-mdc-form-field-infix ng-tns-c1205077789-6">
                                                            <!---->
                                                            <input _ngcontent-serverapp-c2564575186="" matinput="" class="mat-mdc-input-element ng-tns-c1205077789-6 ng-untouched ng-pristine ng-valid mat-input-server mat-mdc-form-field-input-control mdc-text-field__input" placeholder="Job Title" name="job_title" id="job_title" aria-invalid="false" aria-required="false">
                                                            <mat-icon _ngcontent-serverapp-c2564575186="" role="img" class="mat-icon notranslate search_icon ng-tns-c1205077789-6 material-icons mat-ligature-font mat-icon-no-color" aria-hidden="true" data-mat-icon-type="font" ngh="0">
                                                                <app-icons _ngcontent-serverapp-c2564575186="" name="search" _nghost-serverapp-c4219164779="" class="icons" ngh="0">
                                                                    <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                        <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#search"></use>
                                                                    </svg>
                                                                </app-icons>
                                                            </mat-icon>
                                                        </div>
                                                        <!---->
                                                        <!---->
                                                    </div>
                                                    <!---->
                                                </div>
                                                <div class="mat-mdc-form-field-subscript-wrapper mat-mdc-form-field-bottom-align ng-tns-c1205077789-6">
                                                    <!---->
                                                    <div class="mat-mdc-form-field-hint-wrapper ng-tns-c1205077789-6 ng-trigger ng-trigger-transitionMessages ng-star-inserted" style="opacity: 1; transform: translateY(0%);">
                                                        <!---->
                                                        <div class="mat-mdc-form-field-hint-spacer ng-tns-c1205077789-6"></div>
                                                    </div>
                                                    <!---->
                                                </div>
                                            </mat-form-field>
                                            <!---->
                                            <!---->
                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-pos ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">
                                                    <!----><span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Candidate Location</span>
                                                    <!---->
                                                </h3>
                                                <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                    <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">

                                                        <div class="mat-mdc-text-field-wrapper">
                                                            <div class="mat-mdc-form-field-flex">
                                                                <select class="form-control" name="offer_location" id="offer_location">
                                                                    <option value="" disabled selected>Select Location</option>
                                                                    @php
                                                                        $countries = App\Models\Country::all();
                                                                    @endphp
                                                                    @foreach($countries as $country)
                                                                        <option value="{{$country->id}}" {{ request()->get('offer_location') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>

                                                    </mat-form-field>
                                                </app-multi-select>
                                            </div>
                                            <!---->

                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio filter-pos ng-star-inserted">
                                                <div _ngcontent-serverapp-c2564575186="" class="product-page-filter">
                                                    <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Position</h3>
                                                    <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494346" ngh="0">
                                                            <div class="mdc-form-field">
                                                                <div class="mdc-radio">
                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494346-input" name="position_offered" value="Domestic Helper" tabindex="0">
                                                                    <div class="mdc-radio__background">
                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                    </div>
                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                    </div>
                                                                </div>
                                                                <label class="mdc-label" for="mat-radio-11494346-input">Domestic Helper</label>
                                                            </div>
                                                        </mat-radio-button>
                                                        <!---->
                                                        <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494347" ngh="0">
                                                            <div class="mdc-form-field">
                                                                <div class="mdc-radio">
                                                                    <div class="mat-mdc-radio-touch-target"></div>
                                                                    <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494347-input" name="position_offered" value="Driver" tabindex="0">
                                                                    <div class="mdc-radio__background">
                                                                        <div class="mdc-radio__outer-circle"></div>
                                                                        <div class="mdc-radio__inner-circle"></div>
                                                                    </div>
                                                                    <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                        <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                    </div>
                                                                </div>
                                                                <label class="mdc-label" for="mat-radio-11494347-input">Driver</label>
                                                            </div>
                                                        </mat-radio-button>
                                                        <!---->
                                                        <!---->
                                                        <!---->
                                                    </mat-radio-group>
                                                </div>
                                            </div>

                                            <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Start Date</h3>
                                                <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                    <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="7">
                                                        <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                            <div class="mat-mdc-text-field-wrapper">
                                                                <div class="mat-mdc-form-field-flex">
                                                                    <input class="form-control" type="date" name="job_start_date" placeholder="dd-mm-yyyy" id="job_start_date">
                                                                </div>
                                                            </div>
                                                        </mat-form-field>
                                                    </app-date-picker>
                                                </div>
                                            </div>

                                            <!---->
                                            <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Status</h3>
                                                <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                    <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                        <!---->
                                                        <div class="mat-mdc-text-field-wrapper">
                                                            <div class="mat-mdc-form-field-flex">
                                                                <select class="form-control" name="status" id="status">
                                                                    <option value="" disabled selected>Select Status</option>

                                                                    <option value="publish">Published</option>
                                                                    <option value="unpublish">Not Publish</option>
                                                                    <option value="draft">Draft</option>

                                                                </select>
                                                            </div>
                                                        </div>

                                                    </mat-form-field>
                                                </app-multi-select>
                                            </div>
                                        </form>

                                    </div>
                                    <!---->
                                </div>
                            </div>
                        </div>
                    </app-filter>
                </div>
                <!---->
                <div class="col-md-12 col-lg-9">
                    <div class="collection-product-wrapper">
                        <div class="product-wrapper-grid list-view">
                            <!---->
                            <div id="empty-state" class="row" style="display: none;">
                                <div class="col-12 mt-5">
                                    <img class="mx-auto d-block" src="{{asset('cdn-sub/front-app/assets/images/misc/empty-search.jpg')}}">
                                </div>
                                <div class="col-10 offset-1">
                                    <h4 class="text-center">To find available jobs, you just need to adjust your search criteria.</h4>
                                </div>
                            </div>
                            <div class="ng-star-inserted" id="job-list">

                            </div>
                            <!---->
                            <!---->

                            <div id="pagination-links" class="center-pagination">

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- filter modal  -->
    <div class="modal fade mt-3" id="fiterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog mt-5" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="pt-2 font-weight-bold">Filter</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row pb-3">
                        <div class="col-12">
                            <div _ngcontent-serverapp-c3093988918="" class="  d-md-clock d-lg-block d-xl-block collection-filter-block ng-star-inserted">
                                <app-filter _ngcontent-serverapp-c3093988918="" _nghost-serverapp-c2564575186="" ngh="10">
                                    <div _ngcontent-serverapp-c2564575186="" class="row product-service_1">
                                        <div _ngcontent-serverapp-c2564575186="" class="col-md-12 col-lg-12">
                                            <div _ngcontent-serverapp-c2564575186="" class="row filter-ar">
                                                <!---->
                                                <div _ngcontent-serverapp-c2564575186="" class="top-banner-content small-section col-12 ng-star-inserted pt-4">

                                                    <form id="job-search-form">
                                                        <mat-form-field _ngcontent-serverapp-c2564575186="" appearance="outline" class="mat-mdc-form-field ng-tns-c1205077789-6 mat-mdc-form-field-type-mat-input mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                            <!---->
                                                            <div class="mat-mdc-text-field-wrapper mdc-text-field ng-tns-c1205077789-6 mdc-text-field--outlined mdc-text-field--no-label">
                                                                <!---->
                                                                <div class="mat-mdc-form-field-flex ng-tns-c1205077789-6">
                                                                    <div matformfieldnotchedoutline="" class="mdc-notched-outline ng-tns-c1205077789-6 mdc-notched-outline--no-label ng-star-inserted" ngh="0">
                                                                        <div class="mdc-notched-outline__leading"></div>
                                                                        <div class="mdc-notched-outline__notch">
                                                                            <!---->
                                                                            <!---->
                                                                            <!---->
                                                                        </div>
                                                                        <div class="mdc-notched-outline__trailing"></div>
                                                                    </div>
                                                                    <!---->
                                                                    <!---->
                                                                    <!---->
                                                                    <div class="mat-mdc-form-field-infix ng-tns-c1205077789-6">
                                                                        <!---->
                                                                        <input _ngcontent-serverapp-c2564575186="" matinput="" class="mat-mdc-input-element ng-tns-c1205077789-6 ng-untouched ng-pristine ng-valid mat-input-server mat-mdc-form-field-input-control mdc-text-field__input" placeholder="Job Title" name="job_title" id="job_title" aria-invalid="false" aria-required="false">
                                                                        <mat-icon _ngcontent-serverapp-c2564575186="" role="img" class="mat-icon notranslate search_icon ng-tns-c1205077789-6 material-icons mat-ligature-font mat-icon-no-color" aria-hidden="true" data-mat-icon-type="font" ngh="0">
                                                                            <app-icons _ngcontent-serverapp-c2564575186="" name="search" _nghost-serverapp-c4219164779="" class="icons" ngh="0">
                                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#search"></use>
                                                                                </svg>
                                                                            </app-icons>
                                                                        </mat-icon>
                                                                    </div>
                                                                    <!---->
                                                                    <!---->
                                                                </div>
                                                                <!---->
                                                            </div>
                                                            <div class="mat-mdc-form-field-subscript-wrapper mat-mdc-form-field-bottom-align ng-tns-c1205077789-6">
                                                                <!---->
                                                                <div class="mat-mdc-form-field-hint-wrapper ng-tns-c1205077789-6 ng-trigger ng-trigger-transitionMessages ng-star-inserted" style="opacity: 1; transform: translateY(0%);">
                                                                    <!---->
                                                                    <div class="mat-mdc-form-field-hint-spacer ng-tns-c1205077789-6"></div>
                                                                </div>
                                                                <!---->
                                                            </div>
                                                        </mat-form-field>
                                                        <!---->
                                                        <!---->
                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container filter-pos ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">
                                                                <!----><span _ngcontent-serverapp-c2564575186="" class="ng-star-inserted">Candidate Location</span>
                                                                <!---->
                                                            </h3>
                                                            <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                                <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">

                                                                    <div class="mat-mdc-text-field-wrapper">
                                                                        <div class="mat-mdc-form-field-flex">
                                                                            <select class="form-control" name="offer_location" id="offer_location">
                                                                                <option value="" disabled selected>Select Location</option>
                                                                                @php
                                                                                    $countries = App\Models\Country::all();
                                                                                @endphp
                                                                                @foreach($countries as $country)
                                                                                    <option value="{{$country->id}}" {{ request()->get('offer_location') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                                                @endforeach

                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </mat-form-field>
                                                            </app-multi-select>
                                                        </div>
                                                        <!---->

                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio filter-pos ng-star-inserted">
                                                            <div _ngcontent-serverapp-c2564575186="" class="product-page-filter">
                                                                <h3 _ngcontent-serverapp-c2564575186="" class="filter_title custom_h3">Job Position</h3>
                                                                <mat-radio-group _ngcontent-serverapp-c2564575186="" role="radiogroup" aria-label="Select an option" class="mat-mdc-radio-group ng-untouched ng-pristine ng-valid">
                                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494346" ngh="0">
                                                                        <div class="mdc-form-field">
                                                                            <div class="mdc-radio">
                                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494346-input" name="position_offered" value="Domestic Helper" tabindex="0">
                                                                                <div class="mdc-radio__background">
                                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                                </div>
                                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                </div>
                                                                            </div>
                                                                            <label class="mdc-label" for="mat-radio-11494346-input">Domestic Helper</label>
                                                                        </div>
                                                                    </mat-radio-button>
                                                                    <!---->
                                                                    <mat-radio-button _ngcontent-serverapp-c2564575186="" class="mat-mdc-radio-button mat-accent _mat-animation-noopable ng-star-inserted" id="mat-radio-11494347" ngh="0">
                                                                        <div class="mdc-form-field">
                                                                            <div class="mdc-radio">
                                                                                <div class="mat-mdc-radio-touch-target"></div>
                                                                                <input type="radio" class="mdc-radio__native-control" id="mat-radio-11494347-input" name="position_offered" value="Driver" tabindex="0">
                                                                                <div class="mdc-radio__background">
                                                                                    <div class="mdc-radio__outer-circle"></div>
                                                                                    <div class="mdc-radio__inner-circle"></div>
                                                                                </div>
                                                                                <div mat-ripple="" class="mat-ripple mat-radio-ripple mat-mdc-focus-indicator">
                                                                                    <div class="mat-ripple-element mat-radio-persistent-ripple"></div>
                                                                                </div>
                                                                            </div>
                                                                            <label class="mdc-label" for="mat-radio-11494347-input">Driver</label>
                                                                        </div>
                                                                    </mat-radio-button>
                                                                    <!---->
                                                                    <!---->
                                                                    <!---->
                                                                </mat-radio-group>
                                                            </div>
                                                        </div>

                                                        <div _ngcontent-serverapp-c2564575186="" class="sidebar-container filter-radio ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Start Date</h3>
                                                            <div _ngcontent-serverapp-c2564575186="" class="newsletter text-center form">
                                                                <app-date-picker _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c3739594431="" ngh="7">
                                                                    <mat-form-field _ngcontent-serverapp-c3739594431="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-1 mat-mdc-form-field-type-mat-input mat-mdc-form-field-has-icon-suffix mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="4">

                                                                        <div class="mat-mdc-text-field-wrapper">
                                                                            <div class="mat-mdc-form-field-flex">
                                                                                <input class="form-control" type="date" name="job_start_date" placeholder="dd-mm-yyyy" id="job_start_date">
                                                                            </div>
                                                                        </div>
                                                                    </mat-form-field>
                                                                </app-date-picker>
                                                            </div>
                                                        </div>

                                                        <!---->
                                                        <div _ngcontent-serverapp-c2564575186="" class="title_items sidebar-container ng-star-inserted">
                                                            <h3 _ngcontent-serverapp-c2564575186="" class="custom_h3">Status</h3>
                                                            <app-multi-select _ngcontent-serverapp-c2564575186="" _nghost-serverapp-c648054139="" class="ng-untouched ng-pristine ng-valid" ngh="9">
                                                                <mat-form-field _ngcontent-serverapp-c648054139="" appearance="outline" class="mat-mdc-form-field multi ng-tns-c1205077789-2 mat-mdc-form-field-type-mat-select mat-form-field-no-animations mat-form-field-appearance-outline mat-primary ng-untouched ng-pristine ng-valid ng-star-inserted" ngh="8">
                                                                    <!---->
                                                                    <div class="mat-mdc-text-field-wrapper">
                                                                        <div class="mat-mdc-form-field-flex">
                                                                            <select class="form-control" name="status" id="status">
                                                                                <option value="" disabled selected>Select Status</option>

                                                                                <option value="publish">Published</option>
                                                                                <option value="unpublish">Not Publish</option>
                                                                                <option value="draft">Draft</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </mat-form-field>
                                                            </app-multi-select>
                                                        </div>
                                                    </form>

                                                </div>
                                                <!---->
                                            </div>
                                        </div>
                                    </div>
                                </app-filter>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script>
        function formatToRange(rangeString) {
            let parts = rangeString.split(';');
            if (parts.length === 2) {
                let start = parts[0].trim();
                let end = parts[1].trim();
                return `${start} - ${end}`;
            } else {
                return rangeString;
            }
        }
        $(document).ready(function() {
            fetchJobs();
            $('#job_title, input[name="position_offered"]').on('input change', function() {
                fetchJobs();
            });

            $('#offer_location').on('input', function() {
                fetchJobs();
            });

            $('#job_start_date').on('input', function() {
                fetchJobs();
            });

            $('#status').on('input', function() {
                fetchJobs();
            });

            function fetchJobs(page = 1) {
                var jobTitle = $('#job_title').val();
                var offer_location = $('#offer_location').val();
                var positionOffered = $('input[name="position_offered"]:checked').val();
                var job_start_date = $('#job_start_date').val();
                var status = $('#status').val();
                $.ajax({
                    url: "{{ route('jobs.fetch') }}",
                    method: 'GET',
                    data: {
                        job_title: jobTitle,
                        offer_location: offer_location,
                        position_offered: positionOffered,
                        job_start_date: job_start_date,
                        status: status,
                        page: page
                    },
                    dataType: 'json',
                    success: function(response) {
                        let jobsHtml = '';
                        if (response.jobs.data.length > 0) {
                            $.each(response.jobs.data, function(index, job) {
                                let position = (job.position_offered == 'domestic_helper') ? 'Domestic Helper' : 'Domestic Helper';
                                let prefer_location = (job.prefer_candidate_location == 'any_location') ? "Any Location" : job.prefer_candidate_location;
                                let job_image = "{{asset('assets/images/job/house_keeper.webp')}}";
                                if(job.job_image) {
                                    job_image = "{{ asset('storage/job_picture') }}" + "/" + job.job_image.img;
                                }
                                jobsHtml += `
              <div class="col-grid-box w-100 ng-star-inserted">
                <candidate-detail-block _nghost-serverapp-c400119168="" ngh="11">
                  <div _ngcontent-serverapp-c400119168="" class="product-box-container mb-4 ng-star-inserted">
                    <div _ngcontent-serverapp-c400119168="" class="product-box">
                      <div _ngcontent-serverapp-c400119168="" class="img-wrapper_Custom w-10">
                        <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="#">
                          <div _ngcontent-serverapp-c400119168="" class="front custom_front_rsume_image">
                            <picture _ngcontent-serverapp-c400119168="">
                              <img _ngcontent-serverapp-c400119168="" loading="lazy" onerror="this.src='${job_image}'" width="143px" height="143px" alt="${job.job_title}" src="${job_image}" >
                            </picture>
                            <span _ngcontent-serverapp-c400119168="" class="d-none">Helper Profile Image</span>
                          </div>
                        </a>
                        <div _ngcontent-serverapp-c400119168="" class="listing-sub-title-agency text-left w-20 mt-2">
                          <label _ngcontent-serverapp-c400119168="" class="label_blue">
                            <!----><span _ngcontent-serverapp-c400119168="" class="ng-star-inserted">${ job.education_level }</span><!---->
                          </label>
                        </div>
                      </div>
                      <div _ngcontent-serverapp-c400119168="" class="product-detail w-100 pt-2 align-self-baseline text-left">
                        <a _ngcontent-serverapp-c400119168="" routerlinkactive="router-link-active" href="/job_details/${job.id}">
                          <!----><!---->
                          <h4 _ngcontent-serverapp-c400119168="" class="product-title"> ${ job.job_title }-${job.job_start_date} </h4>
                          <div _ngcontent-serverapp-c400119168="" class="product-header-description">
                            <h5 _ngcontent-serverapp-c400119168="" class="product-sub-title 1 mt-1"><span _ngcontent-serverapp-c400119168=""> ${ job.position_offered } - ${ job.contract_status.replace('_',' ') } </span></h5>
                            <h5 _ngcontent-serverapp-c400119168="" class="product-location ng-star-inserted">
                              <i _ngcontent-serverapp-c400119168="" aria-hidden="true" class="dark">
                                <app-icons _ngcontent-serverapp-c400119168="" name="map" _nghost-serverapp-c4219164779="" class="icons" ngh="0">
                                  <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#map"></use>
                                  </svg>
                                </app-icons>
                              </i>
                              <span _ngcontent-serverapp-c400119168="" class="location"></span>
                            </h5>
                            <!---->
                          </div>
                          <div _ngcontent-serverapp-c400119168="" class="mt-2" style="word-break: break-word;"><i class="fa fa-map-marker" aria-hidden="true"></i> ${ job.country_name ? job.country_name.name : 'Empty' }</div>

                          <div _ngcontent-serverapp-c400119168="" class="mt-2"><i class="fa fa-calendar" aria-hidden="true"></i> Job Start from ${job.job_start_date} | ${job.job_type}</div>
                          <div _ngcontent-serverapp-c400119168="" class="product-footer">
                            <h5 _ngcontent-serverapp-c400119168="" class="footer-experience">
                              <i _ngcontent-serverapp-c400119168="">
                                <app-icons _ngcontent-serverapp-c400119168="" name="certificate" _nghost-serverapp-c4219164779="" class="icons" ngh="0">
                                  <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#certificate"></use>
                                  </svg>
                                </app-icons>
                              </i>

                            </h5>
                            <h5 _ngcontent-serverapp-c400119168="" class="footer-date ng-star-inserted">
                              <i _ngcontent-serverapp-c400119168="" aria-hidden="true">
                                <app-icons _ngcontent-serverapp-c400119168="" name="calendar" _nghost-serverapp-c4219164779="" class="icons" ngh="0">
                                  <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#calendar"></use>
                                  </svg>
                                </app-icons>
                              </i>

                            </h5>
                            <!---->
                            <h5 _ngcontent-serverapp-c400119168="" class="footer-active ng-star-inserted">
                              <i _ngcontent-serverapp-c400119168="" aria-hidden="true">
                                <app-icons _ngcontent-serverapp-c400119168="" name="circle" _nghost-serverapp-c4219164779="" class="icons" ngh="0">
                                  <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#circle"></use>
                                  </svg>
                                </app-icons>
                              </i>
                            </h5>
                            <!---->
                          </div>
                        </a>
                      </div>
                    </div>

                     <div class="text-right m-3">
                       ${job.status == 'publish' ? `
                         <a href="/employer/job_status/${job.id}" class="btn next_button text-uppercase mt-3 text-white bg-success">Published</a>
                       ` : job.status == 'unpublish' ? `
                         <a href="/employer/job_status/${job.id}" class="btn next_button text-uppercase mt-3 text-white" style="background-color: #1E3F66 !important">Not Publish</a>
                       ` : job.status == 'expire' ? `
                         <a href="javascript:" class="btn next_button text-uppercase mt-3 text-white bg-primary">Expire</a>
                         <a href="/employer/upgrade-package/${job.id}" class="btn next_button text-uppercase mt-3 text-white bg-secondary">Upgrade Package</a>
                       ` : `
                         <a href="javascript:" class="btn next_button text-uppercase mt-3 text-white bg-warning">Draft</a>
<a href="/employer/upgrade-package/${job.id}" class="btn next_button text-uppercase mt-3 text-white bg-secondary">Upgrade Package</a>
                       `}
                       <a href="/employer/manage-jobs/edit/${job.id}" class="btn next_button text-uppercase mt-3 text-white bg-dark">Edit</a>
                       <a href="/employer/manage-jobs/delete/${job.id}" class="btn next_button text-uppercase mt-3 text-white bg-danger">Delete</a>

                     </div>
                    <!---->
                  </div>
                  <!----><!----><!----><!---->
                </candidate-detail-block>
              </div>
            `;
                            });
                            $('#job-list').html(jobsHtml);
                            let paginationHtml = '';
                            if (response.pagination) {
                                paginationHtml = response.pagination.map(link => {
                                    return `<li class="page-item ${link.active ? 'active' : ''}"><a class="page-link" href="#" data-page="${link.url ? link.url.split('page=')[1] : ''}">${link.label}</a></li>`;
                                }).join('');
                            }
                            $('#pagination-links').html(`<ul class="pagination">${paginationHtml}</ul>`);
                            $('.page-link').on('click', function(e) {
                                e.preventDefault();
                                const page = $(this).data('page');
                                fetchJobs(page);
                            });
                        } else {
                            $('#job-list').html(''); // Clear existing jobs
                            $('#empty-state').show(); // Show empty state if no jobs are found
                        }
                    }
                });
            }
        });
    </script>



@endsection
