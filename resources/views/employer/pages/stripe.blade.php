@extends('employer.layouts.app')
@section('title','Job Payment')
@push('css')
    <style>
        #card-element {
            border: 1px solid #ced4da;
            border-radius: .25rem;
            padding: .375rem .75rem;
        }
    </style>
@endpush

@section('content')
    <section class="agency news-section pb-5 custom-container page-section blog-ar">
        <div class="container">
            <div class="row mt-4" style="margin-top: 100px !important;">

                <div class="col-md-8 offset-md-2 plr-50">
                    <div class="card">
                        <div class="card-header">
                            @if( is_null($currency_price) && is_null($currency_select))
                                <div class="card-title"> <b >Payment For {{ ucfirst($job->job_title) }} - HK - {{$job->jobPlan->price}}</b></div>
                            @else
                                <div class="card-title"> <b >Payment For {{ ucfirst($job->job_title) }} - {{$currency_select}} - {{$currency_price}}</b></div>
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <form id="payment-form" method="post" action="{{route('pay_stripe_payment',$job)}}">
                                        @csrf
                                        <div class="form-group">
                                            <input type="hidden" name="currency_price" value="{{$currency_price}}">
                                            <input type="hidden" name="currency_select" value="{{$currency_select}}">
                                            <label for="card-element">Credit or debit card</label>
                                            <div id="card-element">
                                                <!-- A Stripe Element will be inserted here. -->
                                            </div>
                                            <!-- Used to display form errors. -->
                                            <div id="card-errors" role="alert" class="text-danger mt-2"></div>
                                        </div>
                                        <button type="submit" class="btn next_button text-uppercase mt-3 text-white">Confirm Payment</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        // Create a Stripe client.
        var stripe = Stripe('pk_test_51MqLb7ClPdA9BXGRe9xSFojSRvvfCqzeMSnXSLYyBWBrEdoXPHDgvNmjWkW3oXy66WmGm93uPX9M865GwI2CbkAK00yk1wY04N');

        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.on('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        // Submit the token to your server.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server.
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form.
            form.submit();
        }
    </script>
@endpush
