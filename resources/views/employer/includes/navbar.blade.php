<style>
    @media (min-width: 1200px) {
        .container {
            max-width: 1440px;
        }

    }
    .navbar-nav{
        padding-left:15px ;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light  sticky-top" style="background-color: #1E3F66  !important; color:white !important;">
    <div _ngcontent-serverapp-c1428765552 class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" style="background-color: white !important;">
            <span class="navbar-toggler-icon" ></span>
        </button>
        <a class="navbar-brand" href="/">
            <img src="{{asset('cdn-sub/logo.png')}}" alt="Logo">
        </a>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">

                @auth
                    @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER)
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="{{route('manage_jobs')}}">Manage Jobs</a>
                        </li>
                    @endif
                @endauth

                @auth
                    @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER)
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="/candidates">FIND HELPERS</a>
                        </li>
                    @endif
                @endauth
                <li class="nav-item">
                    <a class="nav-link text-uppercase" href="/agency&services"> Agency Portal</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-uppercase" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        News & More
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-uppercase" href="/news">Tips & News</a>
                        <a class="dropdown-item text-uppercase" href="/training">Training</a>
                        <a class="dropdown-item text-uppercase" href="/partner">Partner Offer</a>
                        <a class="dropdown-item text-uppercase" href="/event">Event</a>
                        <a class="dropdown-item text-uppercase" href="/about">About Us</a>
                        <a class="dropdown-item text-uppercase" href="/pricing">Pricing</a>
                        <a class="dropdown-item text-uppercase" href="/public_holiday">Public Holiday</a>
                                                @auth
                            @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER)
                                <a class="dropdown-item text-uppercase" href="{{ route('employer_support') }}">
                                    Support
                                </a>
                            @endif
                        @endauth
                    </div>
                </li>
            </ul>
            @auth
                <div class="form-inline my-2 my-lg-0">
                    @if(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_CANDIDATE)
                        <a class="btn login_btn my-2 my-sm-0 text-uppercase" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        <a class="btn register_btn my-2 my-sm-0 text-uppercase" href="{{route('candidate_dashboard')}}" style="width: auto" >Dashboard</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    @elseif(\Illuminate\Support\Facades\Auth::user()->role == \App\Models\User::ROLE_EMPLOYER)
                        <a class="btn  my-2 my-sm-0 text-white" href="{{ route('employer_messages') }}" >
                            <i class="far fa-comments fa-2x"></i>
                        </a>
                        <a class="btn login_btn my-2 my-sm-0 text-uppercase" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        <a class="btn register_btn my-2 my-sm-0 text-uppercase" href="{{route('employer_dashboard')}}" style="width: auto" >Dashboard</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    @endif
                </div>
            @endauth
        </div>
    </div>
</nav>
