
@extends('candidate.layouts.app')
@section('title','Messages')
@section('css')
    <link rel="stylesheet" href="{{asset('admin/css/dashboard.css')}}">

@endsection
@section('content')
    <div class="nk-content">

        <div class="container-fluid mt-5" >
            <div class="nk-content-inner">
                <div class="nk-content-body p-0">
                    <div class="nk-chat">
                        <div class="nk-chat-aside">
                            <div class="nk-chat-aside-head">
                                <div class="nk-chat-aside-user">
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle " data-bs-toggle="dropdown">
                                            <div class="user-avatar">
                                                {{get_name_first_letters( \Illuminate\Support\Facades\Auth::user()->name)}}
                                            </div>
                                            <div class="title" style="margin-bottom: 0 !important;">Users</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="nk-chat-aside-body" data-simplebar="init">
                                <div class="simplebar-wrapper" style="margin: 0px;">
                                    <div class="simplebar-height-auto-observer-wrapper">
                                        <div class="simplebar-height-auto-observer"></div>
                                    </div>
                                    <div class="simplebar-mask">
                                        <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                                            <div class="simplebar-content-wrapper" tabindex="0" role="region" aria-label="scrollable content" style="height: 100%; overflow: hidden scroll;">
                                                <div class="simplebar-content" style="padding: 0px;">
                                                    <div class="nk-chat-aside-search">
                                                        <div class="form-group">
                                                            <div class="form-control-wrap">
                                                                <div class="form-icon form-icon-left">
                                                                    <em class="icon ni ni-search"></em>
                                                                </div>
                                                                <input type="text" class="form-control form-round" id="search_by_name"
                                                                       placeholder="Search by name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-chat-list">
                                                        <h6 class="title overline-title-alt"></h6>
                                                        <ul class="chat-list"  id="userList">
                                                            @if($users->count() == 0)
                                                                <li class="chat-item text-center">
                                                                    No Chat Found
                                                                </li>
                                                            @else
                                                                @foreach($users as $user)
                                                                    <li class="chat-item">
                                                                        <a class="chat-link chat-open" href="{{ route('candidate_chat_index',$user) }}" >
                                                                            <div class="chat-media user-avatar bg-purple">
                                                                                <span>{{get_name_first_letters($user->name)}}</span>
                                                                            </div>
                                                                            <div class="chat-info">
                                                                                <div class="chat-from">
                                                                                    <div class="name">{{$user->name}}</div>
                                                                                </div>
                                                                                <div class="chat-context" >
                                                                                    <div class="text">
                                                                                        @if($user->role == \App\Models\User::ROLE_EMPLOYER)
                                                                                            <span>
                                                                                                EMPLOYER
                                                                                            </span>
                                                                                        @elseif($user->role == \App\Models\User::ROLE_AGENCY)
                                                                                            <span>
                                                                                                AGENCY
                                                                                              </span>
                                                                                        @else
                                                                                            <span>
                                                                                                HELPER
                                                                                              </span>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="status status delivered" id="user_{{$user->id}}">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                        <div class="chat-actions">
                                                                            <div class="dropdown">
                                                                                <a href="#" class="btn btn-icon btn-sm btn-trigger dropdown-toggle" data-bs-toggle="dropdown">
                                                                                    <em class="icon ni ni-more-h"></em>
                                                                                </a>
                                                                                <div class="dropdown-menu dropdown-menu-end">
                                                                                    <ul class="link-list-opt no-bdr">
                                                                                        <li>
                                                                                            <a href="{{ route('candidate_message_mark_as_read',$user) }}">Mark as read</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nk-chat-body m-auto">
                            <div class="card ">
                                <div class=" card-body text-center">
                                    <img src="{{asset('admin/images/chat.svg')}}"/>
                                    <h5 class="mt-2">
                                        Select Any User for chat <span class="nk-menu-icon"><em class="icon ni ni-chat"></em></span>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')
    <script>
        $(document).ready(function() {
            $('#search_by_name').on('keyup', function() {
                var input = $(this).val().toLowerCase();

                $('.chat-item').filter(function() {
                    var userName = $(this).find('.name').text().toLowerCase();
                    $(this).toggle(userName.indexOf(input) !== -1);
                });
            });
        });


    </script>

    <script>
        setInterval(function () {
            @if($users_ids->count() > 0)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route('candidate_unread_messages_count')}}',
                data:{
                    'users_ids':  '{{implode(',',$users_ids->toArray())}}',
                },
                type: "POST",
                success: function (data) {
                    if(data.status){
                        const response_data = data.response_data;
                        $.map(response_data, function(value, index) {
                            const user_id = value.user_id;
                            const badge_id = '#user_badge_'+user_id;
                            const user = '#user_'+user_id;
                            const count_message = value.count_message;
                            if ($(badge_id).length) {
                                if(count_message > 0){
                                    if ($(badge_id).hasClass('d-none')){
                                        $(badge_id).removeClass('d-none');
                                    }
                                    $(badge_id).text(count_message);
                                }else {
                                    if (value.is_remove === true){
                                        $(user).addClass('d-none');
                                    }
                                }
                            }else {
                                if(count_message > 0){
                                    const span = '<div class="user-avatar-group" > ' +
                                        ' <div class="user-avatar xs bg-blue" >' +
                                        '<span id="user_badge_'+user_id+'">'+count_message+'</span>' +
                                        '</div> ' +
                                        '</div>'
                                    $(user).append(span)
                                }
                            }
                        });
                    }
                }
            });
            @endif
        }, 3000);
    </script>
@endpush

