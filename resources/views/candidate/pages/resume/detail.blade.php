@extends('candidate.layouts.app')
@section('title','Resume Detail')
@section('content')
    <style>
        .job-view-ar{
            margin-top: 97px;
        }
        .back_button {
            background: #054a84 !important;
            color: white !important;
        }
        .no_need_button {
            background: #25ae88 !important;
            color: white !important;
        }
    </style>

    <section _ngcontent-serverapp-c1181781737="" class="page-section resume-view-ar ng-tns-c1181781737-1">

        <!----><!---->
        <section _ngcontent-serverapp-c1181781737="" class="agency blog blog-sec blog-sidebar pb-5 blog-list sider ng-tns-c1181781737-1 ng-star-inserted" style="margin-top: 80px">
            <div _ngcontent-serverapp-c1181781737="" class="container ng-tns-c1181781737-1">
                <!---->
                <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1 ng-star-inserted">
                    <div _ngcontent-serverapp-c1181781737="" class="col-md-12 col-lg-9 ng-tns-c1181781737-1">
                        <div _ngcontent-serverapp-c1181781737="" class="product-wrapper-grid list-view ng-tns-c1181781737-1">
                            <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                                <div _ngcontent-serverapp-c1181781737="" class="col-12 col-12_padding_Set ng-tns-c1181781737-1">
                                    <div _ngcontent-serverapp-c1181781737="" class="agency-box ng-tns-c1181781737-1">
                                        <div _ngcontent-serverapp-c1181781737="" class="text-left row company_tabs_header_fix_size font-weight-normal ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-grid-box col-12 ng-tns-c1181781737-1">
                                                <div _ngcontent-serverapp-c1181781737="" class="front custom_front_rsume_image profile-preview ng-tns-c1181781737-1 ng-star-inserted">
                                                    <app-img-preview _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1" _nghost-serverapp-c1281504779="" ngh="3">
                                                        <img _ngcontent-serverapp-c1281504779="" id="myImg" style="width: 100%; max-width: 300px;"
                                                             src="{{ asset('storage/profile_image/' . Auth::user()->profile_image) }}" onerror="this.src='https://cdn.helperplace.com/cpi/199077_1719311605.jpg';"
                                                             alt="{{$user->first_name}}" class="ng-star-inserted"><!----><!----><!----><!---->
                                                    </app-img-preview>
                                                </div>
                                                <!----><span _ngcontent-serverapp-c1181781737="" id="very-active" class="ng-tns-c1181781737-1"></span><!---->
                                                <div _ngcontent-serverapp-c1181781737="" class="listing-sub-title-agency text-left mt-2 mb-2 mr-3 float-left ng-tns-c1181781737-1">
                                                    <label _ngcontent-serverapp-c1181781737="" class="label_blue ng-tns-c1181781737-1 ng-star-inserted">
                                                        <!----><span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted"> Direct </span><!---->
                                                    </label>
                                                    <!----><!---->
                                                </div>
                                                <!---->
                                                <h3 _ngcontent-serverapp-c1181781737="" class="top-active text-left ng-tns-c1181781737-1 ng-star-inserted"> Very Active </h3>
                                                <!---->
                                                <div _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                    <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                        <source _ngcontent-serverapp-c1181781737="" media="(max-width:576px)" class="ng-tns-c1181781737-1" srcset="{{asset('assets/images/resume_header.jpg')}}">
                                                        <source _ngcontent-serverapp-c1181781737="" media="(max-width:768px)" class="ng-tns-c1181781737-1" srcset="{{asset('assets/images/resume_header.jpg')}}">
                                                        <source _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1" srcset="{{asset('assets/images/resume_header.jpg')}}">
                                                        <img _ngcontent-serverapp-c1181781737="" fetchpriority="high" loading="eager" rel="preload" onerror="this.onerror = null;this.parentNode.children[0].srcset =
                                                         this.parentNode.children[1].srcset =this.parentNode.children[2].srcset=this.src;" class="agency_header w-10 responsive-img ng-tns-c1181781737-1" id="img" alt="{{$user->resume_detail ? $user->resume_detail->first_name : ''}}" src="{{asset('assets/images/resume_header.jpg')}}">
                                                    </picture>
                                                    <div _ngcontent-serverapp-c1181781737="" class="agency_header_White_opcity ng-tns-c1181781737-1"></div>
                                                </div>
                                                <div _ngcontent-serverapp-c1181781737="" class="product-box ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="product-detail w-100 ng-tns-c1181781737-1 ng-star-inserted">
                                                        <div _ngcontent-serverapp-c1181781737="" class="user-detail ng-tns-c1181781737-1">
                                                            <div _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                <h1 _ngcontent-serverapp-c1181781737="" class="mb-0 listing-about-title p-0 ng-tns-c1181781737-1"> {{$user->resume_detail ? $user->resume_detail->first_name : ''}} {{$user->resume_detail ? $user->resume_detail->middle_name : ''}} {{$user->resume_detail ? $user->resume_detail->last_name : ''}} </h1>
                                                            </div>
                                                            <div _ngcontent-serverapp-c1181781737="" class="mt-2 ng-tns-c1181781737-1">
                                                                <h2 _ngcontent-serverapp-c1181781737="" class="resume-age ng-tns-c1181781737-1"> ({{$user->resume_detail ? $user->resume_detail->age : ''}} Years) </h2>
                                                            </div>
                                                        </div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="hp-candidate-wrapper ng-tns-c1181781737-1">
                                                            <div _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                <p _ngcontent-serverapp-c1181781737="" class="mb-2 p-0 text-left listing-about-sub-title ng-tns-c1181781737-1">
                                                                    <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted"> {{$user->resume_detail ? ucfirst($user->resume_detail->gender) : ''}}</span>
                                                                    <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted"> |</span> {{$user->resume_detail ? ucfirst($user->resume_detail->martial_status) : ''}}
                                                                    <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted">{{$user->resume_detail ? ucfirst($user->resume_detail->kids_detail) : ''}}  Kids</span>
                                                                    | @if($user->resume_detail && $user->resume_detail->user_nationality) {{ ucfirst($user->resume_detail->user_nationality->name) }} @endif
                                                                    | {{$user->resume_detail ? ucfirst($user->resume_detail->religion) : ''}}
                                                                </p>
                                                            </div>
                                                            <div _ngcontent-serverapp-c1181781737="" class="apply-btn ng-tns-c1181781737-1">
                                                                <div _ngcontent-serverapp-c1181781737="" role="group" class="btn-group ng-tns-c1181781737-1 ng-star-inserted">
                                                                    <a href="{{route('bio-data')}}" _ngcontent-serverapp-c1181781737="" type="button" title="Edit Info"
                                                                            class="btn hp-apply-btn contact ng-tns-c1181781737-1" style="background-color: #1E3F66; border-color: #1E3F66" >
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    <button _ngcontent-serverapp-c1181781737="" type="button" title="Update Profile Visibility" data-toggle="modal" data-target="#exampleModal"
                                                                            class="btn hp-apply-btn contact ng-tns-c1181781737-1" style="min-width: 91px;">
                                                                        Update Visibility
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!----><!---->
                                                </div>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">Professional Info </h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1181781737="" class="row py-3 personal-infomation ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ar ng-tns-c1181781737-1">
                                                <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="user" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#user"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-user" style="color: #1E3F66"></i> &nbsp;
                                                            {{$user->resume_detail ? ucfirst($user->resume_detail->position_apply) : ''}} |  {{$user->resume_detail ? ucfirst($user->resume_detail->work_status) : ''}}
                                                        </h3>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="map" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#map"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-map-marker" aria-hidden="true" style="color: #1E3F66"></i> &nbsp;
                                                            Present Location: @if($user->resume_detail && $user->resume_detail->present_location ) {{ $user->resume_detail->present_location->name }} @endif
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="certificate" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="14" width="14" href="https://www.helperplace.com/assets/icons/custom.svg#certificate"></use>
                                                                </svg>
                                                            </app-icons>
                                                            <i class="fa fa-cog" aria-hidden="true" style="color: #1E3F66"></i> &nbsp;
                                                            {{$user->resume_detail ? ucfirst($user->resume_detail->work_experience) : ''}} Years Experience
                                                        </h3>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="footer-experience ng-tns-c1181781737-1">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="calendar" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                                <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                    <use _ngcontent-serverApp-c4219164779="" height="15" width="15" href="https://www.helperplace.com/assets/icons/custom.svg#calendar"></use>
                                                                </svg>
                                                            </app-icons>

                                                            <i class="fa fa-calendar" aria-hidden="true" style="color: #1E3F66"></i>&nbsp;

                                                            Start from {{$user->resume_detail ? \Illuminate\Support\Carbon::parse($user->resume_detail->job_start_date)->format('d M Y') : ''}} | {{$user->resume_detail ? ucfirst($user->resume_detail->job_type) : ''}}
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">Skills / Duties</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1181781737="" class="row yoga mt-3 event ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/1_1599642484.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Language: </h3>
                                                            @if($user->resume_detail && $user->resume_detail->languages)
                                                                @foreach($user->resume_detail->language_names as $name )
                                                                    <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_1 ng-star-inserted">{{$name}}</h4>
                                                                @endforeach
                                                            @endif
                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1"
                                                                 src="{{asset('cdn-sub/skill_cat/4_1599643968.webp')}}" alt="skills_image"></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Main Skills: </h3>
                                                            @if($user->resume_detail && $user->resume_detail->main_skills)
                                                                @foreach($user->resume_detail->skill_names as $name )
                                                                    <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_2 ng-star-inserted"> {{$name}} </h4>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/2_1599644151.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Cooking Skills: </h3>
                                                            @if($user->resume_detail && $user->resume_detail->cooking_skills)
                                                                @foreach($user->resume_detail->cooking_skill_names as $name )
                                                                    <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_3 ng-star-inserted"> {{$name}} </h4>
                                                                @endforeach
                                                            @endif
                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/3_1599642528.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Other Skills: </h3>
                                                            @if($user->resume_detail && $user->resume_detail->other_skills)
                                                                @foreach($user->resume_detail->other_skill_names as $name )
                                                                    <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_4 ng-star-inserted"> {{$name}}  </h4>
                                                                @endforeach
                                                            @endif
                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                        <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1"><img _ngcontent-serverapp-c1181781737="" loading="lazy" width="100%" height="auto" class="ng-tns-c1181781737-1" src="{{asset('cdn-sub/skill_cat/5_1599644127.webp')}}" alt=""></div>
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-info ng-tns-c1181781737-1">
                                                            <h3 _ngcontent-serverapp-c1181781737="" class="primary ar ng-tns-c1181781737-1"> Personality: </h3>
                                                            @if($user->resume_detail && $user->resume_detail->personalities)
                                                                @foreach($user->resume_detail->personality_names as $name )
                                                                    <h4 _ngcontent-serverapp-c1181781737="" class="float-left ng-tns-c1181781737-1 color_5 ng-star-inserted"> {{$name}} </h4>
                                                                @endforeach
                                                            @endif
                                                            <!---->
                                                        </div>
                                                    </div>
                                                </div>
                                                <!---->
                                            </div>
                                            <!---->
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">About Me</h2>
                                            </div>
                                        </div>
                                        <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto mt-3 jOb_description description_tag ar ng-tns-c1181781737-1">
                                                <div _ngcontent-serverapp-c1181781737="" style="word-break: break-word;" class="ng-tns-c1181781737-1">
                                                    @if($user->resume_detail && $user->resume_detail->resume_description)
                                                        {!! $user->resume_detail->resume_description  !!}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!----><!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">Work Experience</h2>
                                            </div>
                                        </div>
                                        <!---->
                                        @if($user->working_experience)
                                            @foreach($user->working_experience as $experience)
                                                <div _ngcontent-serverapp-c1181781737="" class="row yoga event mt-3 ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                            <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1">
                                                                <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                    <source _ngcontent-serverapp-c1181781737="" srcset="{{asset('cdn-sub/web-asset/images/icon/prize-badge-with-star-and-ribbon-black.png')}}" width="35px" height="35px" media="(max-width:768px)" class="ng-tns-c1181781737-1">
                                                                    <img _ngcontent-serverapp-c1181781737="" src="{{asset('cdn-sub/web-asset/images/icon/prize-badge-with-star-and-ribbon-black.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                                </picture>
                                                            </div>
                                                            <div _ngcontent-serverapp-c1181781737="" class="event-info text-left ng-tns-c1181781737-1">
                                                                <h3 _ngcontent-serverapp-c1181781737="" class="primary text-left mb-0 ng-tns-c1181781737-1">
                                                                    {{ \Illuminate\Support\Carbon::parse($experience->start_date)->format('d M Y') }} - <!----><span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1 ng-star-inserted">{{ \Illuminate\Support\Carbon::parse($experience->job_end_date)->format('d M Y') }}</span><!---->
                                                                </h3>
                                                                <h3 _ngcontent-serverapp-c1181781737="" class="mb-0 ng-tns-c1181781737-1">
                                                                    {{$experience->job_position}}
                                                                    <span _ngcontent-serverapp-c1181781737="" class="location-span ng-tns-c1181781737-1 ng-star-inserted">
                                                            <app-icons _ngcontent-serverapp-c1181781737="" name="map" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                               <svg _ngcontent-serverApp-c4219164779="" class="icons" height="17" width="17">
                                                                  <use _ngcontent-serverApp-c4219164779="" height="13" width="13" href="https://www.helperplace.com/assets/icons/custom.svg#map"></use>
                                                               </svg>
                                                            </app-icons>
                                                            <span _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                @if($experience->work_country)
                                                                    {{$experience->work_country->name}}
                                                                @endif
                                                            </span>
                                                         </span>
                                                                    <!---->
                                                                </h3>
                                                                <p _ngcontent-serverapp-c1181781737="" class="text-left mb-1 ng-tns-c1181781737-1">
                                                                    {{$experience->employer_type}}
                                                                </p>
                                                                <p _ngcontent-serverapp-c1181781737="" class="text-left mb-1 ng-tns-c1181781737-1 ng-star-inserted">
                                                                    @if($experience->job_duties && !$experience->duty_names->isEmpty())
                                                                        Duties: {{ $experience->duty_names->implode(',') }}
                                                                    @else
                                                                        Duties:
                                                                    @endif
                                                                </p>
                                                                <!----><!---->
                                                                <p _ngcontent-serverapp-c1181781737="" class="text-left mb-1 ng-tns-c1181781737-1 ng-star-inserted">
                                                                    @if($experience->reference_letter && $experience->reference_letter == "Yes")
                                                                        I  have a reference letter
                                                                    @else
                                                                        I don't have a reference letter
                                                                    @endif
                                                                </p>
                                                                <!---->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!---->
                                                </div>
                                            @endforeach
                                        @endif

                                        <!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">Education</h2>
                                            </div>
                                        </div>
                                        <!---->
                                        @if($user->education)
                                            @foreach($user->education as $education)
                                                <div _ngcontent-serverapp-c1181781737="" class="row yoga event mt-3 ng-tns-c1181781737-1 ng-star-inserted">
                                                    <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                        <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                            <div _ngcontent-serverapp-c1181781737="" class="yoga-circle mb-0 ng-tns-c1181781737-1">
                                                                <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                                    <source _ngcontent-serverapp-c1181781737="" srcset="{{asset('cdn-sub/web-asset/images/icon/diploma.png')}}" width="35px" height="35px" media="(max-width:768px)" class="ng-tns-c1181781737-1">
                                                                    <img _ngcontent-serverapp-c1181781737="" src="{{asset('cdn-sub/web-asset/images/icon/diploma.png')}}" loading="lazy" alt="" width="40px" height="40px" class="ng-tns-c1181781737-1">
                                                                </picture>
                                                            </div>
                                                            <div _ngcontent-serverapp-c1181781737="" class="event-info text-left ng-tns-c1181781737-1">
                                                                <h3 _ngcontent-serverapp-c1181781737="" class="primary text-left mb-0 ng-tns-c1181781737-1"> {{$education->year_completion}} - {{$education->education}} </h3>
                                                                <h3 _ngcontent-serverapp-c1181781737="" class="mb-0 ar ng-tns-c1181781737-1 ng-star-inserted"> {{ ucwords(str_replace('_',' ',$education->course_duration)) }} </h3>
                                                                <!---->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!---->
                                                </div>
                                            @endforeach
                                        @endif

                                        <!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row custom-header-box ng-tns-c1181781737-1 ng-star-inserted">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 mx-auto ng-tns-c1181781737-1">
                                                <h2 _ngcontent-serverapp-c1181781737="" class="text-light ar ng-tns-c1181781737-1">My Expectations</h2>
                                            </div>
                                        </div>
                                        <!---->
                                        <div _ngcontent-serverapp-c1181781737="" class="row yoga event mt-3 ng-tns-c1181781737-1">
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto border-bottom ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737="" srcset="{{asset('cdn-sub/web-asset/images/icon/give-money.png')}}" width="35px" height="35px" media="(max-width:768px)" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737="" src="{{asset('cdn-sub/web-asset/images/icon/give-money.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="primary text-left mb-0 ng-tns-c1181781737-1"> Salary: </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="mb-0 ar ng-tns-c1181781737-1">
                                                        @if( $user->resume_detail && $user->resume_detail->salary_currency) {{ ucfirst($user->resume_detail->salary_currency->sign ) }} @endif     {{ $user->resume_detail ? number_format($user->resume_detail->monthly_salary ,2): ''}} <!---->
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto border-bottom ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737="" srcset="{{asset('cdn-sub/web-asset/images/icon/bed.png')}}" width="35px" height="35px" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737="" src="{{asset('cdn-sub/web-asset/images/icon/bed.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="primary text-left mb-0 ng-tns-c1181781737-1"> Accommodation: </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="mb-0 ar ng-tns-c1181781737-1">{{ $user->resume_detail ? ucfirst($user->resume_detail->accomodation_preference) : ''}} </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <!---->
                                            <div _ngcontent-serverapp-c1181781737="" class="col-11 phone_pedding mx-auto ng-tns-c1181781737-1 ng-star-inserted">
                                                <div _ngcontent-serverapp-c1181781737="" class="event-container event-container_footer_margin_custom d-flex ng-tns-c1181781737-1">
                                                    <div _ngcontent-serverapp-c1181781737="" class="yoga-circle ng-tns-c1181781737-1">
                                                        <picture _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                            <source _ngcontent-serverapp-c1181781737="" srcset="{{asset('cdn-sub/web-asset/images/icon/home-rest-day-calendar-page.png')}}" width="35px" height="35px" class="ng-tns-c1181781737-1">
                                                            <img _ngcontent-serverapp-c1181781737="" src="{{asset('cdn-sub/web-asset/images/icon/home-rest-day-calendar-page.png')}}" width="40px" height="40px" loading="lazy" alt="" class="ng-tns-c1181781737-1">
                                                        </picture>
                                                    </div>
                                                    <div _ngcontent-serverapp-c1181781737="" class="event-info text-left ng-tns-c1181781737-1">
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="primary text-left mb-0 ng-tns-c1181781737-1"> Day Off: </h3>
                                                        <h3 _ngcontent-serverapp-c1181781737="" class="mb-0 ar ng-tns-c1181781737-1"> {{ $user->resume_detail ? ucfirst($user->resume_detail->day_off_preference) : ''}} </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <!---->
                                        </div>
                                    </div>
{{--                                    <span _ngcontent-serverapp-c1181781737="" class="float-right float-left-ar primary pointer mr-2 ml-2 ng-tns-c1181781737-1"> Report this Candidate </span>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!---->
            </div>
        </section>
        <!----><!---->
        <div _ngcontent-serverapp-c1181781737="" id="footer" class="footer_custom mt-5 mobile_sticky permenant-sticky ng-tns-c1181781737-1 ng-star-inserted">
            <!---->
            <div _ngcontent-serverapp-c1181781737="" id="footer2" class="ng-tns-c1181781737-1">
                <div _ngcontent-serverapp-c1181781737="" class="container ng-tns-c1181781737-1">
                    <div _ngcontent-serverapp-c1181781737="" class="row ng-tns-c1181781737-1">
                        <div _ngcontent-serverapp-c1181781737="" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 res-footer res-button-footer footer-center ng-tns-c1181781737-1">
                            <div _ngcontent-serverapp-c1181781737="" class="footer-apply-content ng-tns-c1181781737-1">
                                <div _ngcontent-serverapp-c1181781737="" class="left ng-tns-c1181781737-1">
                                    <div _ngcontent-serverapp-c1181781737="" class="left-wrapper ng-tns-c1181781737-1">
                                        <div _ngcontent-serverapp-c1181781737="" role="group" class="btn-group ng-tns-c1181781737-1">
                                            <!----><button _ngcontent-serverapp-c1181781737="" type="button" class="btn btn-primary ng-tns-c1181781737-1 ng-star-inserted"> Contact </button><!---->
                                        </div>
                                        <span _ngcontent-serverapp-c1181781737="" class="left-content-text footer-text ng-tns-c1181781737-1 ng-star-inserted"> Dazel Joy </span><!----><!----><!---->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div _ngcontent-serverapp-c1181781737="" class="col-xl-6 col-lg-6 col-md-6 col-sm-6 res-footer res-button-footer ng-tns-c1181781737-1">
                            <div _ngcontent-serverapp-c1181781737="" class="footer-apply-content right-content ng-tns-c1181781737-1">
                                <div _ngcontent-serverapp-c1181781737="" class="right ng-tns-c1181781737-1">
                                    <div _ngcontent-serverapp-c1181781737="" class="right-wrapper ng-tns-c1181781737-1">
                                        <span _ngcontent-serverapp-c1181781737="" class="left-content-text ng-tns-c1181781737-1">Share</span>
                                        <ul _ngcontent-serverapp-c1181781737="" class="social-sharing ng-tns-c1181781737-1">
                                            <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                <a _ngcontent-serverapp-c1181781737="" href="javascript:;" class="ng-tns-c1181781737-1">
                                                    <i _ngcontent-serverapp-c1181781737="" aria-hidden="true" class="ng-tns-c1181781737-1">
                                                        <app-icons _ngcontent-serverapp-c1181781737="" name="facebook" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                            <svg _ngcontent-serverApp-c4219164779="" class="icons" height="15" width="17">
                                                                <use _ngcontent-serverApp-c4219164779="" height="15" width="15" href="https://www.helperplace.com/assets/icons/custom.svg#facebook"></use>
                                                            </svg>
                                                        </app-icons>
                                                    </i>
                                                </a>
                                            </li>
                                            <li _ngcontent-serverapp-c1181781737="" class="ng-tns-c1181781737-1">
                                                <a _ngcontent-serverapp-c1181781737="" href="javascript:;" class="ng-tns-c1181781737-1">
                                                    <i _ngcontent-serverapp-c1181781737="" aria-hidden="true" class="ng-tns-c1181781737-1">
                                                        <app-icons _ngcontent-serverapp-c1181781737="" name="whatsapp" class="icons" _nghost-serverapp-c4219164779="" ngh="0">
                                                            <svg _ngcontent-serverApp-c4219164779="" class="icons" height="18" width="17">
                                                                <use _ngcontent-serverApp-c4219164779="" height="16" width="16" href="https://www.helperplace.com/assets/icons/custom.svg#whatsapp"></use>
                                                            </svg>
                                                        </app-icons>
                                                    </i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->




        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="{{asset('assets/images/site-logo.jpg')}}" class="w-50">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <!-- Add a span to hold the dynamic text -->
                                <h4 class="text-center font-weight-bold align-self-center">
                                    <span >Update your visibility status</span>
                                </h4>
                            </div>
                            <div class="col-md-12 py-4">
                                <h5 class="font-weight-bold">
                                    @if($user->visibility_status == \App\Models\User::VISIBLE)
                                        Already got a Job? Update your resume visibility as No Need Job to hide your profile.
                                    @else
                                        Dont got a Job? Update your resume visibility as  Need Job profile.
                                    @endif
                                </h5>
                            </div>
                            <div class="col-md-12 py-2 text-center">
                                <a href="#" class="btn back_button " data-dismiss="modal">
                                    Cancel
                                </a>
                                @if($user->visibility_status == \App\Models\User::VISIBLE)
                                    <a href="{{ route('change_visibility')}}" class="btn no_need_button">
                                        No Need Job
                                    </a>
                                @else
                                    <a href="{{ route('change_visibility')}}" class="btn no_need_button">
                                        Need Job
                                    </a>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
