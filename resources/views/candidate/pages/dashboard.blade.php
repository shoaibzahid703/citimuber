@extends('candidate.layouts.app')
@section('title','DASHBOARD')
@section('css')
    <style>
        [_nghost-serverApp-c1080078657] .home header {
            position: absolute;
        }
        [_nghost-serverApp-c1080078657] .home header.sticky {
            position: fixed;
        }
        [_nghost-serverApp-c1080078657] .description_tag p {
            margin-bottom: 15px !important;
        }
        [_nghost-serverApp-c1080078657] .description_tag ul {
            list-style-type: disc;
            margin-bottom: 15px !important;
        }
    </style>
    <style>
        [_nghost-serverApp-c1428765552] header {
            padding: 5px 0;
        }
        @media only screen and (max-width: 575px) {
            [_nghost-serverApp-c1428765552] header {
                padding: 5px 0;
            }
        }
        [_nghost-serverApp-c1428765552] header img {
            width: 100%;
            height: auto;
        }
        [_nghost-serverApp-c1428765552] header img.brand-logo {
            max-width: 180px;
        }
        [_nghost-serverApp-c1428765552] header nav ul ul {
            z-index: 8;
        }
        [_nghost-serverApp-c1428765552] header nav ul li a {
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 600;
            line-height: 1;
            line-height: normal;
            padding: 15px 18px;
            color: #262626;
        }
        [_nghost-serverApp-c1428765552] header nav ul li a:hover {
            color: #054a84;
        }
        @media (max-width: 1200px) {
            [_nghost-serverApp-c1428765552] header nav ul li a {
                padding: 15px 10px;
            }
        }
        [_nghost-serverApp-c1428765552] header .d-inline-block {
            align-items: center;
            justify-content: center;
        }
        @media (max-width: 986px) {
            [_nghost-serverApp-c1428765552] header nav.custom-nav {
                height: 55px;
            }
        }
        @media (min-width: 992px) {
            [_nghost-serverApp-c1428765552] header nav.custom-nav {
                height: 55px;
            }
        }
        [_nghost-serverApp-c1428765552] header .custom-app-menu {
            width: 100%;
        }
        header.sticky[_ngcontent-serverApp-c1428765552] {
            background: rgba(255, 255, 255, 0.99);
            box-shadow: 0 3px 12px #00000014;
            position: fixed;
            top: 0;
            height: auto;
            width: 100%;
            transition: all 0.5s;
            padding: 5px 0;
        }
        header.sticky[_ngcontent-serverApp-c1428765552] img[_ngcontent-serverApp-c1428765552] {
            max-width: 50%;
            min-width: 50%;
            height: auto;
            transition: width 2s;
        }
        @media screen {
            header.sticky[_ngcontent-serverApp-c1428765552] img[_ngcontent-serverApp-c1428765552] {
                max-width: 80%;
            }
        }
        header.sticky[_ngcontent-serverApp-c1428765552] img.brand-logo[_ngcontent-serverApp-c1428765552] {
            max-width: 180px;
        }
        .animated[_ngcontent-serverApp-c1428765552] {
            transition: height 1s;
        }
        header[_ngcontent-serverApp-c1428765552] .responsive-btn[_ngcontent-serverApp-c1428765552] i[_ngcontent-serverApp-c1428765552] {
            color: #020202 !important;
        }
        .social_left_sticky[_ngcontent-serverApp-c1428765552] a[_ngcontent-serverApp-c1428765552] {
            text-decoration: none;
            vertical-align: middle;
            text-align: left;
            line-height: 3;
        }
        .social_left_sticky[_ngcontent-serverApp-c1428765552] p[_ngcontent-serverApp-c1428765552] {
            color: #fff;
            position: relative;
            left: 0;
            padding: 0 0 0 10px;
            line-height: 38px;
            font-family: Roboto, Helvetica Neue, sans-serif;
        }
        .social_left_sticky[_ngcontent-serverApp-c1428765552] #sidebar[_ngcontent-serverApp-c1428765552] {
            height: 250px;
            width: 10px;
            position: fixed;
            padding: 10px;
            margin-left: 10px;
        }
        .social_left_sticky[_ngcontent-serverApp-c1428765552] .social[_ngcontent-serverApp-c1428765552] {
            margin-left: 0;
            width: 230px;
            height: 30px;
            line-height: 30px;
            padding: 0;
            display: inline-table;
            height: 0px;
            background-color: #808080ed;
            -moz-transition-property: margin-left;
            -moz-transition-duration: 0.2s;
            -moz-transition-delay: 0.2s;
            -ms-transition-property: margin-left;
            -ms-transition-duration: 0.2s;
            -ms-transition-delay: 0.2s;
            -o-transition-property: margin-left;
            -o-transition-duration: 0.2s;
            -o-transition-delay: 0.2s;
            -webkit-transition-property: margin-left;
            -webkit-transition-duration: 0.2s;
            -webkit-transition-delay: 0.2s;
            box-shadow: 0 0 6px #3e3d3d;
            cursor: pointer;
        }
        .social_left_sticky[_ngcontent-serverApp-c1428765552] .social[_ngcontent-serverApp-c1428765552]:hover {
            margin-left: -30px;
            width: 230px;
            background-color: #054a84;
        }
        .popup-button-main[_ngcontent-serverApp-c1428765552] {
            margin-left: 15px;
        }
        .signup-link[_ngcontent-serverApp-c1428765552]:hover {
            color: #054a84;
        }
        @media (max-width: 767px) {
            .mob_col_11_10[_ngcontent-serverApp-c1428765552] {
                flex: 0 0 65.666667%;
                max-width: 65.666667% !important;
                text-align: end;
            }
        }
        @media (max-width: 991px) {
            .mob_col_11_10[_ngcontent-serverApp-c1428765552] {
                flex: 0 0 65.666667%;
                max-width: 65.666667% !important;
                text-align: end;
            }
        }
        @media (max-width: 1200px) {
            .mob_col_11_10[_ngcontent-serverApp-c1428765552] {
                flex: 0 0 80%;
                max-width: 80% !important;
                text-align: end;
            }
        }
        @media (max-width: 986px) {
            .helper-logo[_ngcontent-serverApp-c1428765552] {
                z-index: 0;
                position: absolute;
                margin-left: auto;
                margin-right: auto;
                left: 0;
                right: 30px;
                text-align: center;
            }
            .img-fluid[_ngcontent-serverApp-c1428765552] {
                min-width: 0px !important;
            }
        }
    </style>
    <style>
        .dropdown_menu_color[_ngcontent-serverApp-c719059462] {
            color: #121212;
        }
        .dropdown_menu_color[_ngcontent-serverApp-c719059462]:hover {
            color: #077556 !important;
        }
        .bg-dark[_ngcontent-serverApp-c719059462] {
            background: #25ae88 !important;
            color: #fff !important;
        }
        .popup-button-main[_ngcontent-serverApp-c719059462] {
            margin-left: 10px;
            float: right;
            margin-top: 3px;
        }
        @media screen and (max-width: 991px) {
            .popup-button-main[_ngcontent-serverApp-c719059462] {
                width: 50%;
                display: inline-block;
                margin: 0;
                padding: 0 10px;
            }
        }
        .popup-button-main[_ngcontent-serverApp-c719059462] a[_ngcontent-serverApp-c719059462] {
            padding: 10px;
        }
        .popup-button-main[_ngcontent-serverApp-c719059462] a.popup-button-register[_ngcontent-serverApp-c719059462] {
            border: 2px solid #ebba16;
            border-radius: 6px;
            background-color: #ebba16;
            color: #fff;
            letter-spacing: 1px;
            width: 110px;
            text-align: center;
            float: left;
        }
        .popup-button-main[_ngcontent-serverApp-c719059462] a.popup-button-login[_ngcontent-serverApp-c719059462] {
            border: 2px solid #25ae88;
            border-radius: 6px;
            background-color: #25ae88;
            color: #fff;
            letter-spacing: 1px;
            width: 110px;
            text-align: center;
            float: right;
        }
        .alert-dismissible[_ngcontent-serverApp-c719059462] .close[_ngcontent-serverApp-c719059462] {
            position: absolute;
            top: 0;
            right: 0;
            padding: 0.75rem 1.25rem;
            color: inherit;
            background: transparent;
        }
        .alert-dismissible[_ngcontent-serverApp-c719059462] .close[_ngcontent-serverApp-c719059462]:focus {
            outline: none;
        }
        .has-badge[data-count][_ngcontent-serverApp-c719059462]:after {
            position: absolute;
            right: 0%;
            top: -20%;
            width: 16px;
            height: 16px;
            line-height: 17px;
            content: attr(data-count);
            font-size: 40%;
            border-radius: 50%;
            color: #fff;
            background: rgba(255, 0, 0, 0.85);
            text-align: center;
        }
        @media only screen and (max-width: 991px) {
            .has-badge[data-count][_ngcontent-serverApp-c719059462]:after {
                right: -10%;
                top: -75%;
                font-size: 65%;
            }
        }
        .fa-stack[_ngcontent-serverApp-c719059462] {
            margin: 0 -0.5em 1.7em;
            padding: 1em;
        }
        .fa-stack[_ngcontent-serverApp-c719059462] i[_ngcontent-serverApp-c719059462] {
            color: #054a84 !important;
        }
        .notification-window[_ngcontent-serverApp-c719059462] {
            position: absolute;
            top: 60px;
            right: 0;
            background: white;
            height: auto;
            width: auto;
            display: table;
            border: 1px solid #f3f3f3;
            border-radius: 7px;
            z-index: 8;
            box-shadow: 0.5px 1.5px 6px 1px #cfcfcf;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .notification-header[_ngcontent-serverApp-c719059462] {
            text-align: left;
            border-bottom: 2px solid #eee;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .notification-header[_ngcontent-serverApp-c719059462] .notification-title[_ngcontent-serverApp-c719059462] {
            text-align: left;
            display: inline-block;
            padding: 5px 10px;
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
            color: #054a84;
            font-weight: 700;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .notification-header[_ngcontent-serverApp-c719059462] button[_ngcontent-serverApp-c719059462] {
            color: #aaa;
            font-size: calc(13px + 3 * (100vw - 300px) / 1620) !important;
            line-height: 26px;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] {
            clear: both;
            height: 300px;
            overflow-y: auto;
            width: 400px;
            margin-bottom: 5px;
        }
        @media screen and (max-width: 500px) {
            .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] {
                width: 300px;
            }
        }
        @media screen and (max-width: 360px) {
            .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] {
                width: 275px;
            }
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item[_ngcontent-serverApp-c719059462] {
            text-align: left;
            border: 0;
            border-radius: 0;
            border-bottom: 1px solid #eee;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item[_ngcontent-serverApp-c719059462] h5[_ngcontent-serverApp-c719059462] {
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
            margin-bottom: 2px;
            color: #262626;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item[_ngcontent-serverApp-c719059462] p[_ngcontent-serverApp-c719059462] {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            text-transform: none;
            margin: 0 0 5px;
            line-height: normal;
            color: #999;
            text-align: justify;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item[_ngcontent-serverApp-c719059462] small[_ngcontent-serverApp-c719059462] {
            text-transform: initial;
            font-size: calc(10px + 3 * (100vw - 300px) / 1620) !important;
            color: #999;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item.notification-unread[_ngcontent-serverApp-c719059462] {
            background-color: #f8f9fa;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item.notification-unread[_ngcontent-serverApp-c719059462] h5[_ngcontent-serverApp-c719059462],
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item.notification-unread[_ngcontent-serverApp-c719059462] p[_ngcontent-serverApp-c719059462],
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] .list-group-item.notification-unread[_ngcontent-serverApp-c719059462] small[_ngcontent-serverApp-c719059462] {
            font-weight: 700;
        }
        .notification-window[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] button[_ngcontent-serverApp-c719059462] {
            color: #aaa;
            font-size: calc(13px + 3 * (100vw - 300px) / 1620) !important;
            line-height: 26px;
        }
        #notification-icon[_ngcontent-serverApp-c719059462] {
            cursor: pointer;
        }
        .mt_1_c[_ngcontent-serverApp-c719059462] {
            margin-top: 0.75rem;
        }
        @media screen and (max-width: 767px) {
            .mt_1_c[_ngcontent-serverApp-c719059462] {
                margin-top: 0.25rem;
            }
        }
        .navbar[_ngcontent-serverApp-c719059462] .custom-min-menu[_ngcontent-serverApp-c719059462] {
            width: 100%;
            text-align: left;
        }
        .custom-login-menu[_ngcontent-serverApp-c719059462] {
            float: right;
        }
        @media screen and (max-width: 991px) {
            .custom-login-menu[_ngcontent-serverApp-c719059462] {
                float: none;
            }
        }
        .custom-menu-icon[_ngcontent-serverApp-c719059462] {
            padding: 12px 0;
            color: #054a84;
            font-size: calc(20px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 600;
        }
        .custom-menu-icon[_ngcontent-serverApp-c719059462] span[_ngcontent-serverApp-c719059462] {
            padding: 0 5px;
            cursor: pointer;
            position: relative;
        }
        @media screen and (max-width: 991px) {
            .custom-menu-icon[_ngcontent-serverApp-c719059462] {
                display: none;
            }
        }
        .navbar[_ngcontent-serverApp-c719059462] .custom-mobile-inner-header[_ngcontent-serverApp-c719059462] {
            padding: 10px;
            float: none;
        }
        .navbar[_ngcontent-serverApp-c719059462] .custom-mobile-inner-header[_ngcontent-serverApp-c719059462] .custom-mobile-logo[_ngcontent-serverApp-c719059462] {
            max-width: 150px;
        }
        .navbar[_ngcontent-serverApp-c719059462] .custom-mobile-inner-header[_ngcontent-serverApp-c719059462] .custom-back-button[_ngcontent-serverApp-c719059462] {
            float: right;
            cursor: pointer;
            padding: 10px;
        }
        .overlay-sidebar-header.overlay-sidebar-header-open[_ngcontent-serverApp-c719059462] {
            z-index: 8;
        }
        .custom-scroll[_ngcontent-serverApp-c719059462] .list-group[_ngcontent-serverApp-c719059462] {
            scrollbar-color: #aaa #e3e3e3;
            scrollbar-width: thin;
        }
        .custom-scroll[_ngcontent-serverApp-c719059462] [_ngcontent-serverApp-c719059462]::-webkit-scrollbar {
            width: 8px;
            background-color: #e3e3e3;
            border-radius: 25px;
        }
        .custom-scroll[_ngcontent-serverApp-c719059462] [_ngcontent-serverApp-c719059462]::-webkit-scrollbar-thumb {
            background-image: linear-gradient(rgba(170, 170, 170, 0.92) 0%, #aaa 100%);
            border-radius: 25px;
        }
        .region[_ngcontent-serverApp-c719059462] .form-group[_ngcontent-serverApp-c719059462] input[_ngcontent-serverApp-c719059462] {
            width: auto !important;
        }
        .modal-header[_ngcontent-serverApp-c719059462] {
            padding: 1rem;
            background: white;
        }
        .nav-pills[_ngcontent-serverApp-c719059462] .nav-link[_ngcontent-serverApp-c719059462] {
            font-size: 20px !important;
        }
        .welcome-modal[_ngcontent-serverApp-c719059462] .close[_ngcontent-serverApp-c719059462] {
            position: absolute;
            right: 5px;
            top: 5px;
            background: #f5f5f5;
            opacity: 1 !important;
            color: #666;
            z-index: 2;
            margin: 0;
            padding: 0 5px;
            border-radius: 50%;
            border: 1px solid #666;
        }
        .welcome-modal[_ngcontent-serverApp-c719059462] .modal-header[_ngcontent-serverApp-c719059462] {
            padding: 0;
            border: 0;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] {
            min-height: 400px;
            padding: 0;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] .img-container[_ngcontent-serverApp-c719059462] {
            height: 200px;
            overflow: hidden;
            border-top-right-radius: 0.2rem;
            border-top-left-radius: 0.2rem;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] .container-fluid[_ngcontent-serverApp-c719059462] {
            height: 200px;
            display: flex;
            flex-direction: column;
            padding: 1rem;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] .container-fluid[_ngcontent-serverApp-c719059462] h3[_ngcontent-serverApp-c719059462] {
            font-weight: 700;
            font-size: calc(20px + 3 * (100vw - 300px) / 1620);
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] .container-fluid[_ngcontent-serverApp-c719059462] p[_ngcontent-serverApp-c719059462] {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620);
            margin-top: auto;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] .container-fluid[_ngcontent-serverApp-c719059462] .action-buttons[_ngcontent-serverApp-c719059462] {
            margin-top: auto;
            margin-bottom: auto;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] .container-fluid[_ngcontent-serverApp-c719059462] .action-buttons[_ngcontent-serverApp-c719059462] .btn-yellow[_ngcontent-serverApp-c719059462] {
            color: #fff;
            background-color: #ebba16;
            border-color: #ebba16;
            width: 155px;
        }
        .welcome-popup[_ngcontent-serverApp-c719059462] .container-fluid[_ngcontent-serverApp-c719059462] .action-buttons[_ngcontent-serverApp-c719059462] .btn-green[_ngcontent-serverApp-c719059462] {
            color: #fff;
            background-color: #25ae88;
            border-color: #25ae88;
            width: 155px;
        }
        .region-country-list[_ngcontent-serverApp-c719059462] span[_ngcontent-serverApp-c719059462] {
            border: none;
            padding: 0;
            font-size: 13px;
            font-weight: 300;
            color: #505050;
            display: inline-block;
            margin: 3px auto;
        }
        .region-country-list[_ngcontent-serverApp-c719059462] span[_ngcontent-serverApp-c719059462] .flag.id[_ngcontent-serverApp-c719059462] {
            background-position: -16px -528px;
        }
        .region-country-list[_ngcontent-serverApp-c719059462] span[_ngcontent-serverApp-c719059462] img.fnone[_ngcontent-serverApp-c719059462] {
            float: none;
            width: 17.5%;
            height: auto;
        }
        .region-country-list[_ngcontent-serverApp-c719059462] span[_ngcontent-serverApp-c719059462] img[_ngcontent-serverApp-c719059462] {
            border: 1px solid #ccc;
            border-radius: 50%;
            width: 20px !important;
            vertical-align: bottom;
        }
        .report-modal[_ngcontent-serverApp-c719059462] .confirm-buttons[_ngcontent-serverApp-c719059462] {
            clear: both;
        }
        .agency-contact-form-modal[_ngcontent-serverApp-c719059462] .modal-body[_ngcontent-serverApp-c719059462] {
            width: 100%;
            margin: 0 auto;
            padding: 4% 5%;
            border-radius: 5px;
        }
        .agency-contact-form-modal[_ngcontent-serverApp-c719059462] .modal-body[_ngcontent-serverApp-c719059462] .agency-contant-mobileNo[_ngcontent-serverApp-c719059462] {
            margin-bottom: 15px !important;
        }
        .agency-contact-form-modal[_ngcontent-serverApp-c719059462] .modal-body[_ngcontent-serverApp-c719059462] .example-container[_ngcontent-serverApp-c719059462] {
            padding: 0 10px 0 0;
        }
        @media screen and (max-width: 767px) {
            .agency-contact-form-modal[_ngcontent-serverApp-c719059462] .modal-body[_ngcontent-serverApp-c719059462] {
                width: 95%;
            }
        }
        .agency-contact-form-modal[_ngcontent-serverApp-c719059462] .example-full-width[_ngcontent-serverApp-c719059462] {
            width: 100%;
        }
        @media screen and (max-width: 767px) {
            .extra-padding-bottom-mobile[_ngcontent-serverApp-c719059462] {
                margin-bottom: 5rem;
            }
        }
        @media screen and (min-width: 999px) {
            .cstm_length[_ngcontent-serverApp-c719059462] {
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 191px;
            }
        }
        .follow-social[_ngcontent-serverApp-c719059462] {
            display: -webkit-inline-box;
            padding: 20px;
        }
        .socials-lists[_ngcontent-serverApp-c719059462] ul[_ngcontent-serverApp-c719059462] li[_ngcontent-serverApp-c719059462] {
            padding: 0 25px;
        }
        .top-10[_ngcontent-serverApp-c719059462] {
            margin-top: 10px;
            text-align: center;
        }
        .socials-lists[_ngcontent-serverApp-c719059462] {
            margin: auto;
            width: 55%;
        }
        .p-5[_ngcontent-serverApp-c719059462] {
            padding: 10px 0 !important;
        }
        .cstm-menu-ar[_ngcontent-serverApp-c719059462] {
            right: -100px;
        }
        @media only screen and (min-width: 991px) {
            .name-menu[_ngcontent-serverApp-c719059462] {
                float: right;
            }
        }
        .logout[_ngcontent-serverApp-c719059462] {
            position: relative !important;
            right: 57px !important;
        }
        .fa-bars[_ngcontent-serverApp-c719059462]:before,
        .fa-navicon[_ngcontent-serverApp-c719059462]:before,
        .fa-reorder[_ngcontent-serverApp-c719059462]:before {
            position: absolute !important;
            top: 2px !important;
            width: 250% !important;
            height: 200% !important;
        }
        .mobile-nav[_ngcontent-serverApp-c719059462] {
            display: flex !important;
            flex-direction: row;
            align-items: center;
            width: 100%;
            justify-content: space-between;
        }
        .custom-mobile-view[_ngcontent-serverApp-c719059462] {
            width: 44px;
        }
        .sign-up[_ngcontent-serverApp-c719059462] {
            font-size: 1.1rem !important;
            font-weight: 600;
        }
        .sign-in[_ngcontent-serverApp-c719059462] {
            color: #25ae88;
            cursor: pointer;
        }
        .sign-up[_ngcontent-serverApp-c719059462] {
            color: #ebba16;
            cursor: pointer;
        }
        .not-mobile-view[_ngcontent-serverApp-c719059462] {
            display: block;
        }
        .sign-up[_ngcontent-serverApp-c719059462] .popup-button-login[_ngcontent-serverApp-c719059462],
        .sign-up[_ngcontent-serverApp-c719059462] .popup-button-register[_ngcontent-serverApp-c719059462] {
            font-weight: 600 !important;
            font-size: 15px !important;
        }
        @media (max-width: 986px) {
            .view-mobile[_ngcontent-serverApp-c719059462] {
                display: block;
            }
            .not-mobile-view[_ngcontent-serverApp-c719059462] {
                display: none;
            }
        }
        @media (max-width: 986px) {
            .responsive-btn[_ngcontent-serverApp-c719059462] {
                float: left;
            }
            .header[_ngcontent-serverApp-c719059462] nav[_ngcontent-serverApp-c719059462] {
                justify-content: center !important;
            }
            .notification-icon[_ngcontent-serverApp-c719059462] {
                z-index: 0;
                position: absolute;
                margin-left: auto;
                margin-right: auto;
                left: 0;
                right: 0;
                text-align: end;
            }
            .line[_ngcontent-serverApp-c719059462] {
                border: 1px solid #dddddd;
            }
        }
        @media (min-width: 991px) {
            .shadow-menu[_ngcontent-serverApp-c719059462] {
                box-shadow: 0 5px 5px #0003 !important;
            }
        }
        @media (max-width: 991px) {
            .notification-icon[_ngcontent-serverApp-c719059462] {
                height: 0px !important;
            }
        }
        .notification-icon[_ngcontent-serverApp-c719059462] {
            z-index: 0;
        }
        [_nghost-serverApp-c719059462] .icons {
            fill: #054a84 !important;
        }
        @media (max-width: 367px) {
            .btn-position[_ngcontent-serverApp-c719059462] {
                margin-top: 6px !important;
                margin-right: 9px !important;
            }
        }
    </style>
    <style>
        .secondary-light[_ngcontent-serverApp-c1262338835] {
            color: #bfbfc0;
        }
        .bg-white[_ngcontent-serverApp-c1262338835] {
            background: #fff;
        }
        .agency[_ngcontent-serverApp-c1262338835] h1[_ngcontent-serverApp-c1262338835],
        .agency[_ngcontent-serverApp-c1262338835] h2[_ngcontent-serverApp-c1262338835],
        .agency[_ngcontent-serverApp-c1262338835] h3[_ngcontent-serverApp-c1262338835],
        .agency[_ngcontent-serverApp-c1262338835] h4[_ngcontent-serverApp-c1262338835],
        .agency[_ngcontent-serverApp-c1262338835] h5[_ngcontent-serverApp-c1262338835],
        .agency[_ngcontent-serverApp-c1262338835] h6[_ngcontent-serverApp-c1262338835] {
            color: #434345;
        }
        .bg-primary[_ngcontent-serverApp-c1262338835] {
            background: #054a84 !important;
            color: #fff;
        }
        a[_ngcontent-serverApp-c1262338835] {
            text-decoration: none;
            color: #25ae88;
        }
        .p_001[_ngcontent-serverApp-c1262338835] {
            padding: 0.1em;
        }
        @media screen and (max-width: 991px) {
            .p_001[_ngcontent-serverApp-c1262338835] {
                padding: 0.1em 0.1em 0 !important;
            }
        }
        h2[_ngcontent-serverApp-c1262338835] {
            margin: 5px 0 10px !important;
        }
        h3[_ngcontent-serverApp-c1262338835] {
            margin: 10px 0 5px;
        }
        p[_ngcontent-serverApp-c1262338835] {
            margin: 2px 0;
        }
        .custom_errow_only[_ngcontent-serverApp-c1262338835] {
            width: unset !important;
            box-shadow: unset !important;
        }
        .product-detail[_ngcontent-serverApp-c1262338835] {
            margin: 0 auto;
        }
        .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] .listing-about-title[_ngcontent-serverApp-c1262338835] {
            width: 99%;
            line-height: 20px;
            text-align: left;
            margin: 0;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: normal;
        }
        @media screen and (min-width: 1280px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] .listing-about-title[_ngcontent-serverApp-c1262338835] {
                margin-top: 0 !important;
                height: 30px;
            }
        }
        .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h4[_ngcontent-serverApp-c1262338835] {
            font-size: 20px;
            line-height: 20px;
            color: #25ae88;
            font-weight: 400;
        }
        @media screen and (max-width: 767px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h4[_ngcontent-serverApp-c1262338835] {
                font-size: 18px;
                line-height: 16px;
            }
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h4[_ngcontent-serverApp-c1262338835] span[_ngcontent-serverApp-c1262338835] {
                font-size: 13px;
            }
        }
        @media screen and (max-width: 375px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h4[_ngcontent-serverApp-c1262338835] {
                font-size: 14px;
                line-height: 16px;
            }
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h4[_ngcontent-serverApp-c1262338835] span[_ngcontent-serverApp-c1262338835] {
                font-size: 11px;
            }
        }
        .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h5[_ngcontent-serverApp-c1262338835] {
            font-size: 14px;
            line-height: 20px;
            color: #000;
            font-weight: 500;
        }
        .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h5[_ngcontent-serverApp-c1262338835] img[_ngcontent-serverApp-c1262338835] {
            width: 14px;
            height: 20px;
            margin-right: 2px;
            margin-top: -2px;
        }
        @media screen and (max-width: 374px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h5[_ngcontent-serverApp-c1262338835] {
                font-size: 11px;
                line-height: 16px;
            }
        }
        @media screen and (max-width: 767px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h5[_ngcontent-serverApp-c1262338835] {
                font-size: 13px;
                line-height: 16px;
            }
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h5[_ngcontent-serverApp-c1262338835] img[_ngcontent-serverApp-c1262338835] {
                width: 12px;
                height: 17px;
                margin-top: -2px;
            }
        }
        .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h6[_ngcontent-serverApp-c1262338835] {
            font-size: 14px;
            line-height: 20px;
            color: #054a84;
            font-weight: 500;
        }
        .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h6[_ngcontent-serverApp-c1262338835] img[_ngcontent-serverApp-c1262338835] {
            width: 14px;
            height: 20px;
            margin-top: -2px;
        }
        @media screen and (max-width: 374px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h6[_ngcontent-serverApp-c1262338835] {
                font-size: 12px;
                line-height: 16px;
            }
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h6[_ngcontent-serverApp-c1262338835] img[_ngcontent-serverApp-c1262338835] {
                width: 12px;
                height: 17px;
                margin-top: -2px;
            }
        }
        @media screen and (max-width: 767px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h6[_ngcontent-serverApp-c1262338835] {
                font-size: 12px;
                line-height: 16px;
            }
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] h6[_ngcontent-serverApp-c1262338835] img[_ngcontent-serverApp-c1262338835] {
                width: 12px;
                height: 17px;
                margin-top: -2px;
            }
        }
        @media screen and (min-width: 768px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] .desktop_spacer[_ngcontent-serverApp-c1262338835] {
                margin-bottom: 0.75rem !important;
                margin-top: 0.75rem !important;
            }
        }
        .product-box[_ngcontent-serverApp-c1262338835] {
            box-shadow: 0 0 8px #ddd;
            padding: 1em 0;
            border-radius: 7px;
            overflow: hidden;
            margin-top: 0 !important;
            margin-bottom: 20px;
            background-color: #f8f9fa;
        }
        @media screen and (max-width: 767px) {
            .d-sm-block[_ngcontent-serverApp-c1262338835] {
                display: none !important;
            }
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] {
                padding-top: 0;
            }
            .top-banner-wrapper[_ngcontent-serverApp-c1262338835] .top-banner-content[_ngcontent-serverApp-c1262338835] {
                padding: 10px 0;
            }
        }
        @media screen and (min-width: 1280px) {
            .product-detail[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] {
                padding-top: 1px;
            }
        }
        .blog-sidebar[_ngcontent-serverApp-c1262338835] input[_ngcontent-serverApp-c1262338835] {
            padding: 0.25rem;
        }
        .blog-sidebar[_ngcontent-serverApp-c1262338835] .sidebar-container[_ngcontent-serverApp-c1262338835] {
            margin-bottom: 10px;
        }
        .product-page-filter[_ngcontent-serverApp-c1262338835] {
            margin-bottom: 30px;
        }
        .agency[_ngcontent-serverApp-c1262338835] h1[_ngcontent-serverApp-c1262338835] {
            font-weight: 500;
        }
        input[_ngcontent-serverApp-c1262338835]:focus,
        textarea[_ngcontent-serverApp-c1262338835]:focus,
        select[_ngcontent-serverApp-c1262338835]:focus {
            outline: none !important;
        }
        @media screen and (max-width: 374px) {
            .mb-3[_ngcontent-serverApp-c1262338835],
            .my-3[_ngcontent-serverApp-c1262338835] {
                margin-bottom: 0.55rem !important;
            }
            .mt-3[_ngcontent-serverApp-c1262338835],
            .my-3[_ngcontent-serverApp-c1262338835] {
                margin-top: 0.55rem !important;
            }
        }
        @media screen and (min-width: 768px) {
            .date_desktop_spacer[_ngcontent-serverApp-c1262338835] {
                margin-top: 0.7rem !important;
            }
        }
        .multipleJobs[_ngcontent-serverApp-c1262338835] .form-group[_ngcontent-serverApp-c1262338835] input[_ngcontent-serverApp-c1262338835] {
            width: auto !important;
        }
        .owl-carousel.owl-drag[_ngcontent-serverApp-c1262338835] .owl-item[_ngcontent-serverApp-c1262338835] {
            -webkit-user-select: none;
            user-select: none;
            max-width: 530px !important;
        }
        .owl-carousel[_ngcontent-serverApp-c1262338835] .owl-item[_ngcontent-serverApp-c1262338835] {
            min-height: 1px;
            float: left !important;
        }
        .owl-carousel[_ngcontent-serverApp-c1262338835],
        .owl-carousel[_ngcontent-serverApp-c1262338835] .owl-item[_ngcontent-serverApp-c1262338835] {
            -webkit-tap-highlight-color: transparent;
            position: relative;
        }
        .owl-carousel[_ngcontent-serverApp-c1262338835] .owl-stage[_ngcontent-serverApp-c1262338835] {
            position: relative;
        }
        .app2.team[_ngcontent-serverApp-c1262338835] .team-slider[_ngcontent-serverApp-c1262338835] .owl-stage-outer[_ngcontent-serverApp-c1262338835] {
            margin: -5px;
        }
        .owl-carousel[_ngcontent-serverApp-c1262338835] .owl-stage-outer[_ngcontent-serverApp-c1262338835] {
            position: relative;
            overflow: hidden;
            -webkit-transform: translate3d(0, 0, 0);
        }
        .owl-height[_ngcontent-serverApp-c1262338835] {
            transition: height 0.5s ease-in-out;
        }
        .no-js[_ngcontent-serverApp-c1262338835] .owl-carousel[_ngcontent-serverApp-c1262338835],
        .owl-carousel.owl-loaded[_ngcontent-serverApp-c1262338835] {
            display: block;
        }
        .owl-carousel[_ngcontent-serverApp-c1262338835] {
            display: none;
            width: 100%;
            z-index: 1;
        }
        [_nghost-serverApp-c1262338835] .owl-theme .owl-dots .owl-dot span {
            width: 10px;
            height: 10px;
            margin: 5px 7px;
            background: #d6d6d6;
            display: block;
            -webkit-backface-visibility: visible;
            transition: opacity 0.2s ease;
            border-radius: 30px;
        }
        [_nghost-serverApp-c1262338835] .owl-theme .owl-dots .owl-dot.active span,
        [_nghost-serverApp-c1262338835] .owl-theme .owl-dots .owl-dot:hover span {
            background: #25ae88;
        }
        [_nghost-serverApp-c1262338835] .owl-theme .owl-dots .owl-dot {
            display: inline-block;
            zoom: 1;
            cursor: pointer;
        }
        [_nghost-serverApp-c1262338835] .owl-theme .owl-dots,
        [_nghost-serverApp-c1262338835] .owl-theme .owl-nav {
            text-align: center;
            -webkit-tap-highlight-color: transparent;
        }
        [_nghost-serverApp-c1262338835] .owl-theme .owl-nav.disabled + .owl-dots {
            margin-top: 20px;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail h2 {
            font-size: calc(18px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 700;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .owl-carousel.owl-drag .owl-item {
            max-width: 100% !important;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container {
            margin-left: 0;
            margin-right: 0;
            border: 0;
            padding: 0;
            background-color: #f8f9fa;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .name {
            font-size: calc(20px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 700;
            display: -webkit-box;
            -webkit-line-clamp: 1;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
            margin-bottom: 0;
        }
        @media screen and (max-width: 767px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .name {
                -webkit-line-clamp: 3;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .sub-title {
            display: block;
            margin-bottom: 15px;
            margin-top: 0;
            line-height: normal;
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count {
            display: table;
            width: 100%;
            table-layout: fixed;
            border-spacing: 10px 5px;
        }
        @media screen and (min-width: 768px) and (max-width: 991px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count {
                max-width: 475px;
                border-spacing: 10px 5px;
            }
        }
        @media screen and (min-width: 992px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count {
                max-width: 600px;
                border-spacing: 30px 5px;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container {
            background-color: #25ae8826;
            border-bottom: 2px solid #25ae88;
            cursor: pointer;
            border-radius: 5px;
            display: table-cell;
        }
        @media screen and (max-width: 574px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container {
                display: block;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container h3 {
            font-weight: 700;
            font-size: calc(20px + 3 * (100vw - 300px) / 1620) !important;
            margin-bottom: 0;
        }
        @media screen and (max-width: 574px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container h3 {
                padding-top: 10px;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container p {
            display: block;
            font-size: calc(10px + 3 * (100vw - 300px) / 1620) !important;
            margin-top: 0;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container:hover {
            background-color: #25ae88;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container:hover h3,
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-candidate-count .custom-candidate-count-container:hover p {
            color: #fff;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-devicder {
            width: 100%;
            border-bottom: 1px solid #aaa;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action {
            display: table;
            width: 100%;
        }
        @media screen and (min-width: 768px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action {
                position: absolute;
                top: -10px;
                right: 0;
                width: 450px;
                height: 100px;
            }
        }
        @media screen and (min-width: 768px) and (max-width: 1199px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action {
                width: 250px;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-left {
            display: table-cell;
            width: 33%;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-left select {
            min-width: 90px;
            border: 0;
            border-bottom: 2px solid #25ae88;
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            cursor: pointer;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right {
            display: table-cell;
            text-align: right;
            vertical-align: middle;
        }
        @media screen and (max-width: 767px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right {
                text-align: center;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .custom-transperant-button {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            padding: 5px 10px;
            letter-spacing: normal;
            color: #fff;
            box-shadow: 0 0.5px 0.7px #00000009, 0 1.1px 1.8px #0000000c, 0 2.1px 3.4px #0000000f, 0 3.8px 6px #00000012, 0 7.1px 11.3px #00000016, 0 17px 200px #0000001f;
            background: #25ae88;
            border-radius: 10px !important;
            padding: 10px 20px !important;
            border: 0 !important;
            width: 200px !important;
            margin-right: 20px;
            margin-top: 10px;
        }
        @media screen and (min-width: 768px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .custom-transperant-button {
                max-width: 440px;
                border-spacing: 20px 5px;
            }
        }
        @media screen and (min-width: 991px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .custom-transperant-button {
                max-width: 600px;
                border-spacing: 30px 5px;
            }
        }
        @media screen and (min-width: 768px) and (max-width: 991px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .custom-transperant-button {
                width: 150px !important;
            }
        }
        @media screen and (max-width: 991px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .custom-transperant-button {
                margin-right: 0;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .custom-transperant-button:hover {
            background-color: #25ae88;
            color: #fff;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .bg-warning {
            background-color: #ebba16 !important;
            display: inline-block;
            padding: 5px 10px;
            font-size: calc(12px + 3 * (100vw - 300px) / 1620);
            text-transform: none;
            color: #fff;
        }
        @media screen and (max-width: 374px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-action .custom-job-action-right .bg-warning {
                padding: 5px;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .hp-download-btn {
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .team-slider .team-container .custom-job-note {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            color: #25ae88;
            font-weight: 700;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .main-title {
            font-size: calc(20px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 700;
            color: #054a84;
            text-transform: initial;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .sub-title {
            display: block;
            margin-top: -5px;
            line-height: normal;
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .custom-job-action {
            display: flex;
            justify-content: flex-start;
            width: 100%;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .custom-job-action .custom-job-action-detail {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .custom-job-action .custom-job-action-detail .custom-transperant-button {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            letter-spacing: normal;
            color: #fff;
            box-shadow: 0 0.5px 0.7px #00000009, 0 1.1px 1.8px #0000000c, 0 2.1px 3.4px #0000000f, 0 3.8px 6px #00000012, 0 7.1px 11.3px #00000016, 0 17px 200px #0000001f;
            background: #25ae88;
            border-radius: 10px !important;
            padding: 10px 20px !important;
            border: 0 !important;
            width: 205px !important;
            margin-right: 20px;
            margin-top: 10px;
        }
        @media screen and (max-width: 991px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .custom-job-action {
                display: flex;
                justify-content: center;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .hp-download-btn {
            display: flex;
            justify-content: flex-end;
        }
        @media screen and (max-width: 991px) {
            [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .hp-download-btn {
                display: flex;
                justify-content: center;
            }
        }
        [_nghost-serverApp-c1262338835] .Custom-Dashboard .product-box .product-detail .custom-job-action-footer-message {
            display: block;
            font-weight: 700;
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            position: absolute;
            bottom: -30px;
            right: 5px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] {
            box-shadow: 0.5px 1.5px 6px 1px #cfcfcf;
            overflow: hidden;
            border-radius: 7px;
            position: relative;
            background-color: #f8f9fa;
            margin: 15px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-header[_ngcontent-serverApp-c1262338835] {
            background: rgba(5, 74, 132, 0.1490196078);
            padding: 10px 15px;
            border-bottom: 3px solid #054a84;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-header[_ngcontent-serverApp-c1262338835] h2[_ngcontent-serverApp-c1262338835] {
            font-size: calc(18px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 700;
            color: #054a84;
            margin: 5px 0 !important;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-header[_ngcontent-serverApp-c1262338835] h2[_ngcontent-serverApp-c1262338835] i[_ngcontent-serverApp-c1262338835] {
            font-size: calc(16px + 3 * (100vw - 300px) / 1620) !important;
            margin-right: 10px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] {
            padding: 15px;
            min-height: 262px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-item-single[_ngcontent-serverApp-c1262338835] {
            margin-bottom: 90px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-item[_ngcontent-serverApp-c1262338835] {
            display: table;
            width: 100%;
            border-spacing: 10px 0;
            padding: 10px 0;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-item[_ngcontent-serverApp-c1262338835] .widget-item-image[_ngcontent-serverApp-c1262338835] {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
            width: 70px;
            height: 70px;
            max-width: 70px;
            max-height: 70px;
            border-radius: 50%;
            text-overflow: ellipsis;
            white-space: nowrap;
            border: 1px solid #dcdcdc;
            box-shadow: 0 1px 3px #434345;
            overflow: hidden;
            font-size: 22px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-item[_ngcontent-serverApp-c1262338835] .widget-item-image.date[_ngcontent-serverApp-c1262338835] {
            font-size: calc(14px + 3 * (100vw - 300px) / 1620) !important;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-item[_ngcontent-serverApp-c1262338835] .widget-item-image.img[_ngcontent-serverApp-c1262338835] {
            border: 0;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-item[_ngcontent-serverApp-c1262338835] .widget-item-detail[_ngcontent-serverApp-c1262338835] {
            display: table-cell;
            vertical-align: middle;
            height: 70px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835]
        .widget-body[_ngcontent-serverApp-c1262338835]
        .widget-item[_ngcontent-serverApp-c1262338835]
        .widget-item-detail[_ngcontent-serverApp-c1262338835]
        h3[_ngcontent-serverApp-c1262338835] {
            font-size: calc(16px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 700;
            color: #054a84;
            margin: 0;
            display: -webkit-box;
            -webkit-line-clamp: 1;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
            text-transform: initial;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835]
        .widget-body[_ngcontent-serverApp-c1262338835]
        .widget-item[_ngcontent-serverApp-c1262338835]
        .widget-item-detail[_ngcontent-serverApp-c1262338835]
        p[_ngcontent-serverApp-c1262338835] {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            margin-top: 0;
            line-height: 1.5;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
            margin-bottom: 0;
            text-align: justify;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835]
        .widget-body[_ngcontent-serverApp-c1262338835]
        .widget-item[_ngcontent-serverApp-c1262338835]
        .widget-item-detail[_ngcontent-serverApp-c1262338835]
        h5.widget-item-location[_ngcontent-serverApp-c1262338835] {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            font-weight: 700;
            display: inline-block;
            padding: 0;
            color: #666;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835]
        .widget-body[_ngcontent-serverApp-c1262338835]
        .widget-item[_ngcontent-serverApp-c1262338835]
        .widget-item-detail[_ngcontent-serverApp-c1262338835]
        h5.widget-item-location[_ngcontent-serverApp-c1262338835]
        i[_ngcontent-serverApp-c1262338835] {
            margin-right: 5px;
            color: #25ae88;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835]
        .widget-body[_ngcontent-serverApp-c1262338835]
        .widget-item[_ngcontent-serverApp-c1262338835]
        .widget-item-detail.single-line[_ngcontent-serverApp-c1262338835]
        h3[_ngcontent-serverApp-c1262338835] {
            -webkit-line-clamp: 2;
            color: #666;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-item.no-data-message[_ngcontent-serverApp-c1262338835] {
            height: 181px;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .border-bottom[_ngcontent-serverApp-c1262338835] {
            border-bottom: 1px solid #ccc;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-action[_ngcontent-serverApp-c1262338835] {
            text-align: center;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-action[_ngcontent-serverApp-c1262338835] .custom-transperant-button[_ngcontent-serverApp-c1262338835] {
            font-size: calc(12px + 3 * (100vw - 300px) / 1620) !important;
            letter-spacing: normal;
            color: #054a84;
            box-shadow: 0 0.5px 0.7px #00000009, 0 1.1px 1.8px #0000000c, 0 2.1px 3.4px #0000000f, 0 3.8px 6px #00000012, 0 7.1px 11.3px #00000016, 0 17px 200px #0000001f;
            background: rgba(97, 107, 104, 0.2117647059);
            border-radius: 10px !important;
            padding: 10px 20px !important;
            border: 0 !important;
            width: 205px !important;
            margin-right: 20px;
            margin-top: 10px;
            max-width: 100%;
            font-weight: 700;
        }
        .custom-dashboard-widget[_ngcontent-serverApp-c1262338835] .widget-body[_ngcontent-serverApp-c1262338835] .widget-action[_ngcontent-serverApp-c1262338835] .custom-transperant-button[_ngcontent-serverApp-c1262338835]:hover {
            background-color: #25ae88;
            color: #fff;
        }
        .back-btn[_ngcontent-serverApp-c1262338835] {
            color: #1c4c7c;
            cursor: pointer;
            font-weight: 600;
        }
        .login-modal[_ngcontent-serverApp-c1262338835] .modal-header[_ngcontent-serverApp-c1262338835] {
            padding: 1rem;
            background: white;
        }
        .publish-tile-buttons-left[_ngcontent-serverApp-c1262338835] {
            text-align: left !important;
        }
        .publish-tile-buttons-right[_ngcontent-serverApp-c1262338835] {
            text-align: right !important;
        }
        .breadcrumb-section-main[_ngcontent-serverApp-c1262338835] {
            padding: 0;
            background: transparent !important;
        }
        .breadcrumb-section-main[_ngcontent-serverApp-c1262338835] a[_ngcontent-serverApp-c1262338835] {
            color: #434345;
        }
        .eye-icon[_ngcontent-serverApp-c1262338835] {
            background: #054a84 !important;
            border: 1px solid #054a84 !important;
        }
        .edit_icons[_ngcontent-serverApp-c1262338835] {
            width: 35px;
            height: 35px;
            border: 1px solid #25ae88;
            border-radius: 50px;
            text-align: center;
            margin-top: 10px;
            line-height: 35px;
            cursor: pointer;
            background: #25ae88;
            color: #fff;
            margin-left: 5px;
        }
        .product-box[_ngcontent-serverApp-c1262338835] .hp-download-btn[_ngcontent-serverApp-c1262338835] {
            display: flex !important;
            align-items: center;
            justify-content: flex-end;
        }
        .custom_errow_only[_ngcontent-serverApp-c1262338835] form[_ngcontent-serverApp-c1262338835] {
            padding: 0;
        }
        .custom_errow_only[_ngcontent-serverApp-c1262338835] {
            margin-bottom: 0;
        }
        .yellow-border-bottom[_ngcontent-serverApp-c1262338835] {
            border-bottom: 3px solid #ebba16 !important;
        }
        .employer-job-carousel-icon[_ngcontent-serverApp-c1262338835] {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            padding-right: 20px;
        }
        .listing-block[_ngcontent-serverApp-c1262338835] .info-block[_ngcontent-serverApp-c1262338835]:first-child .widget-header[_ngcontent-serverApp-c1262338835] {
            border-bottom: 3px solid #ebba16 !important;
        }
        .agency-publish-btn[_ngcontent-serverApp-c1262338835] {
            display: flex !important;
            align-items: center !important;
        }
        @media screen and (max-width: 425px) {
            .agency-publish-btn[_ngcontent-serverApp-c1262338835] {
                flex-direction: column !important;
            }
        }
        @media screen and (max-width: 320px) {
            .agency-publish-btn[_ngcontent-serverApp-c1262338835] {
                display: table-cell !important;
            }
        }
        @media screen and (max-width: 425px) {
            .custom-transperant-button[_ngcontent-serverApp-c1262338835] {
                margin-right: 0 !important;
            }
        }
        .product-page-filter[_ngcontent-serverApp-c1262338835] .title_items[_ngcontent-serverApp-c1262338835] {
            margin: 30px 0;
        }
        .product-page-filter[_ngcontent-serverApp-c1262338835] .datalist[_ngcontent-serverApp-c1262338835] {
            list-style: none;
            border-radius: 3px;
            margin-top: -30px;
            color: #565656;
            margin-bottom: 5px;
            box-shadow: 0 0 6px #aaa;
            max-height: 140px;
            overflow: hidden;
            overflow-y: scroll;
            text-overflow: ellipsis;
            white-space: normal;
        }
        .product-page-filter[_ngcontent-serverApp-c1262338835] .datalist[_ngcontent-serverApp-c1262338835]::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 50px;
            background-color: #fff;
        }
        .product-page-filter[_ngcontent-serverApp-c1262338835] .datalist[_ngcontent-serverApp-c1262338835]::-webkit-scrollbar {
            width: 6px;
            background-color: #fff;
        }
        .product-page-filter[_ngcontent-serverApp-c1262338835] .datalist[_ngcontent-serverApp-c1262338835]::-webkit-scrollbar-thumb {
            border-radius: 50px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            background-color: #595959;
        }
        .product-page-filter[_ngcontent-serverApp-c1262338835] .datalist[_ngcontent-serverApp-c1262338835] .option[_ngcontent-serverApp-c1262338835]:hover {
            background-color: #979797;
        }
        .modal-header[_ngcontent-serverApp-c1262338835] {
            padding: 1rem;
            background: white;
        }
        .help-heading[_ngcontent-serverApp-c1262338835] {
            font-size: 22px !important;
            font-weight: 700;
        }
        mat-label[_ngcontent-serverApp-c1262338835] {
            font-size: large;
        }
        .my_custom_style[_ngcontent-serverApp-c1262338835] .my_custom_mat-label[_ngcontent-serverApp-c1262338835] {
            top: -0.45625em;
            color: #434345;
            position: relative;
        }
        .my_custom_style[_ngcontent-serverApp-c1262338835] .mat-input-element[_ngcontent-serverApp-c1262338835] {
            float: right;
            padding: 10px 10px 4px;
            z-index: 9;
        }
        .helpagency-title[_ngcontent-serverApp-c1262338835] {
            background-color: #054a84;
            padding: 7px;
            color: #fff;
            font-weight: 200;
            font-size: 20px;
        }
        .mb-20[_ngcontent-serverApp-c1262338835] {
            margin-bottom: 20px;
        }
        .ngx-intel[_ngcontent-serverApp-c1262338835] {
            order: 1px solid #054a84;
            border-left-width: 5px;
            font-size: 12px;
            padding: 13px 0;
            height: inherit;
            background: #fff;
            border-radius: 7px;
            width: 100%;
        }
        .custom_errow_only[_ngcontent-serverApp-c1262338835] select.mat-input-element[_ngcontent-serverApp-c1262338835] {
            margin-top: 2.5em !important;
        }
        .modal-header[_ngcontent-serverApp-c1262338835] .close[_ngcontent-serverApp-c1262338835] {
            margin: 0 !important;
        }
        @media screen and (max-width: 375px) {
            .breadcrumb-skelton[_ngcontent-serverApp-c1262338835] {
                height: 90px;
            }
        }
        .border-radius-card[_ngcontent-serverApp-c1262338835] {
            border-radius: 1.2rem;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .card[_ngcontent-serverApp-c1262338835] {
            border: 0px solid rgba(0, 0, 0, 0.125) !important;
            background-color: #e0dcdc !important;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .custom-btn[_ngcontent-serverApp-c1262338835] {
            font-size: 10px !important;
            font-weight: 600;
            padding: 5px 10px;
            letter-spacing: normal;
            color: #fff;
            border-radius: 10px !important;
            padding: 10px !important;
            border: 0 !important;
            width: 160 !important;
            margin-right: 20px;
            margin-top: 10px;
            width: 130px;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .btn-success[_ngcontent-serverApp-c1262338835] {
            background: #25ae88 !important;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .btn-warning[_ngcontent-serverApp-c1262338835] {
            background: #ebba16 !important;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .title[_ngcontent-serverApp-c1262338835] {
            font-size: 17px !important;
            color: #054a84 !important;
            font-weight: 600;
            line-height: 1.4em !important;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .publish_title[_ngcontent-serverApp-c1262338835] {
            line-height: 1.4em !important;
            text-align: center !important;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .grid-container[_ngcontent-serverApp-c1262338835] {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            justify-content: center;
            align-items: center;
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .grid-2-container[_ngcontent-serverApp-c1262338835] {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            justify-content: center;
            align-items: center;
        }
        @media screen and (max-width: 320px) {
            .dashboard-mobile[_ngcontent-serverApp-c1262338835] .grid-container[_ngcontent-serverApp-c1262338835] {
                display: grid;
                grid-template-columns: repeat(2, 1fr);
                justify-content: center;
                align-items: center;
            }
        }
        @media screen and (max-width: 353px) {
            .dashboard-mobile[_ngcontent-serverApp-c1262338835] .grid-2-container[_ngcontent-serverApp-c1262338835] {
                grid-template-columns: repeat(1, 1fr) !important;
            }
        }
        .dashboard-mobile[_ngcontent-serverApp-c1262338835] .icon-box[_ngcontent-serverApp-c1262338835] {
            display: flex;
            flex-direction: column;
            text-align: center;
            gap: 7px;
            margin-top: 35px;
            font-size: 13px;
            font-weight: 600;
            cursor: pointer;
        }
        .chat-help-need .mat-mdc-form-field:not(.mat-form-field-disabled) .mat-mdc-floating-label.mdc-floating-label {
            font-weight: 600 !important;
        }
    </style>
    <style>
        @charset "UTF-8";
        .dark[_ngcontent-serverApp-c2340635486] {
            color: #25ae88 !important;
        }
        primary[_ngcontent-serverApp-c2340635486] {
            color: #054a84;
        }
        .bg-secondary[_ngcontent-serverApp-c2340635486] {
            background-color: #1f2023 !important;
            color: #fff;
        }
        .light[_ngcontent-serverApp-c2340635486] {
            color: #ebba16 !important;
        }
        .text-offwhite[_ngcontent-serverApp-c2340635486] {
            color: #ddd !important;
        }
        .icon_social_color[_ngcontent-serverApp-c2340635486] {
            color: #1f2023;
            background: #fff;
        }
        .android[_ngcontent-serverApp-c2340635486]:after {
            content: "\a0";
            display: inline-block;
            width: 100%;
            height: 35px;
            background-position: 0% 0%;
            background-size: 100% 200%;
            background-image: url(https://cdn.helperplace.com/web-asset/images/Android-App-Store-logos.png);
            padding: 0;
        }
        div.android[_ngcontent-serverApp-c2340635486]:after {
            max-width: 18px;
            width: 100%;
            height: 0;
            padding: 0 0 100%;
        }
        .ios[_ngcontent-serverApp-c2340635486]:after {
            content: "\a0";
            display: inline-block;
            width: 100%;
            height: 35px;
            background-position: 0% 98%;
            background-size: 100% 200%;
            background-image: url(https://cdn.helperplace.com/web-asset/images/Android-App-Store-logos.png);
            padding: 0;
        }
        div.ios[_ngcontent-serverApp-c2340635486]:after {
            max-width: 18px;
            width: 100%;
            height: 0;
            padding: 0 0 100%;
        }
        .resume[_ngcontent-serverApp-c2340635486] p[_ngcontent-serverApp-c2340635486] {
            color: #fff !important;
            font-size: calc(12px + 2 * (100vw - 300px) / 1620);
        }
        .spacer_26[_ngcontent-serverApp-c2340635486] li[_ngcontent-serverApp-c2340635486] {
            height: 28px;
        }
        .socials-lists[_ngcontent-serverApp-c2340635486] ul[_ngcontent-serverApp-c2340635486] li[_ngcontent-serverApp-c2340635486] {
            padding: 0 10px 2%;
        }
        .socials-lists[_ngcontent-serverApp-c2340635486] ul[_ngcontent-serverApp-c2340635486] li[_ngcontent-serverApp-c2340635486] a[_ngcontent-serverApp-c2340635486] i[_ngcontent-serverApp-c2340635486] {
            height: 35px;
            width: 35px;
        }
        .rounded-top-lr[_ngcontent-serverApp-c2340635486] {
            border-top-left-radius: 0.35rem !important;
            border-top-right-radius: 0.35rem !important;
            border-bottom-left-radius: 0 !important;
            border-bottom-right-radius: 0 !important;
            background: #01ba00 !important;
        }
        .socials-horizontal[_ngcontent-serverApp-c2340635486] {
            align-items: center;
            justify-content: center;
        }
        .store-div[_ngcontent-serverApp-c2340635486] img[_ngcontent-serverApp-c2340635486] {
            margin: 0 8px;
        }
        @media (max-width: 1200px) {
            .store-div[_ngcontent-serverApp-c2340635486] img[_ngcontent-serverApp-c2340635486] {
                margin: 0 5px;
            }
        }
        @media (max-width: 992px) {
            .store-div[_ngcontent-serverApp-c2340635486] img[_ngcontent-serverApp-c2340635486] {
                margin: 0 8px;
            }
        }
        @media (max-width: 576px) {
            .custom-text-align[_ngcontent-serverApp-c2340635486] {
                text-align: center;
            }
        }
        .footer-logo[_ngcontent-serverApp-c2340635486] {
            max-width: 200px;
            width: 100%;
        }
        .copyright-text[_ngcontent-serverApp-c2340635486],
        .copyright-text[_ngcontent-serverApp-c2340635486] .text-offwhite[_ngcontent-serverApp-c2340635486] {
            font-size: 11px !important;
        }
        .desc-text[_ngcontent-serverApp-c2340635486] {
            font-size: 14px !important;
        }
        .socials-lists[_ngcontent-serverApp-c2340635486] ul.socials-horizontal[_ngcontent-serverApp-c2340635486] {
            display: inline-flex !important;
        }
        .region-lang[_ngcontent-serverApp-c2340635486] {
            display: flex;
            align-items: center;
            justify-content: start;
        }
        .region-lang[_ngcontent-serverApp-c2340635486] select[_ngcontent-serverApp-c2340635486] {
            color: #fff !important;
            background-color: transparent !important;
            border: 0 !important;
            height: 30px !important;
            padding: 2px 7px !important;
            -webkit-appearance: none;
            appearance: none;
        }
        .region-lang[_ngcontent-serverApp-c2340635486] select[_ngcontent-serverApp-c2340635486] option[_ngcontent-serverApp-c2340635486] {
            color: #495057 !important;
        }
        @media (max-width: 575px) {
            .region-lang[_ngcontent-serverApp-c2340635486] {
                justify-content: center;
            }
        }
        .copyright-region[_ngcontent-serverApp-c2340635486] {
            align-items: center;
        }
        @media (max-width: 992px) {
            .copyright-region[_ngcontent-serverApp-c2340635486] {
                gap: 10px;
            }
        }
        .lang-section[_ngcontent-serverApp-c2340635486] {
            z-index: 1 !important;
            position: relative !important;
            top: -50px !important;
        }
        .location-icon[_ngcontent-serverApp-c2340635486] {
            margin-top: -3px;
            margin-left: 8px;
        }
        @media screen and (max-width: 992px) {
            .lang-section[_ngcontent-serverApp-c2340635486] {
                top: -42px !important;
            }
        }
    </style>
    <style>
        .skeleton-loader[_ngcontent-serverApp-c3247267950] {
            box-sizing: border-box;
            overflow: hidden;
            position: relative;
            background: rgb(239, 241, 246) no-repeat;
            border-radius: 4px;
            width: 100%;
            height: 20px;
            display: inline-block;
            margin-bottom: 10px;
            will-change: transform;
        }
        .skeleton-loader[_ngcontent-serverApp-c3247267950]:after,
        .skeleton-loader[_ngcontent-serverApp-c3247267950]:before {
            box-sizing: border-box;
        }
        .skeleton-loader.circle[_ngcontent-serverApp-c3247267950] {
            width: 40px;
            height: 40px;
            margin: 5px;
            border-radius: 50%;
        }
        .skeleton-loader.progress[_ngcontent-serverApp-c3247267950],
        .skeleton-loader.progress-dark[_ngcontent-serverApp-c3247267950] {
            transform: translateZ(0);
        }
        .skeleton-loader.progress[_ngcontent-serverApp-c3247267950]:after,
        .skeleton-loader.progress[_ngcontent-serverApp-c3247267950]:before,
        .skeleton-loader.progress-dark[_ngcontent-serverApp-c3247267950]:after,
        .skeleton-loader.progress-dark[_ngcontent-serverApp-c3247267950]:before {
            box-sizing: border-box;
        }
        .skeleton-loader.progress[_ngcontent-serverApp-c3247267950]:before,
        .skeleton-loader.progress-dark[_ngcontent-serverApp-c3247267950]:before {
            animation: _ngcontent-serverApp-c3247267950_progress 2s ease-in-out infinite;
            background-size: 200px 100%;
            position: absolute;
            z-index: 1;
            top: 0;
            left: 0;
            width: 200px;
            height: 100%;
            content: "";
        }
        .skeleton-loader.progress[_ngcontent-serverApp-c3247267950]:before {
            background-image: linear-gradient(90deg, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.6), rgba(255, 255, 255, 0));
        }
        .skeleton-loader.progress-dark[_ngcontent-serverApp-c3247267950]:before {
            background-image: linear-gradient(90deg, transparent, rgba(0, 0, 0, 0.2), transparent);
        }
        .skeleton-loader.pulse[_ngcontent-serverApp-c3247267950] {
            animation: _ngcontent-serverApp-c3247267950_pulse 1.5s cubic-bezier(0.4, 0, 0.2, 1) infinite;
            animation-delay: 0.5s;
        }
        .skeleton-loader.custom-content[_ngcontent-serverApp-c3247267950] {
            height: 100%;
            background: none;
        }
        @media (prefers-reduced-motion: reduce) {
            .skeleton-loader.pulse[_ngcontent-serverApp-c3247267950],
            .skeleton-loader.progress-dark[_ngcontent-serverApp-c3247267950],
            .skeleton-loader.custom-content[_ngcontent-serverApp-c3247267950],
            .skeleton-loader.progress[_ngcontent-serverApp-c3247267950]:before {
                animation: none;
            }
            .skeleton-loader.progress[_ngcontent-serverApp-c3247267950]:before,
            .skeleton-loader.progress-dark[_ngcontent-serverApp-c3247267950],
            .skeleton-loader.custom-content[_ngcontent-serverApp-c3247267950] {
                background-image: none;
            }
        }
        @media screen and (min-device-width: 1200px) {
            .skeleton-loader[_ngcontent-serverApp-c3247267950] {
                -webkit-user-select: none;
                user-select: none;
                cursor: wait;
            }
        }
        @keyframes _ngcontent-serverApp-c3247267950_progress {
            0% {
                transform: translate3d(-200px, 0, 0);
            }
            to {
                transform: translate3d(calc(200px + 100vw), 0, 0);
            }
        }
        @keyframes _ngcontent-serverApp-c3247267950_pulse {
            0% {
                opacity: 1;
            }
            50% {
                opacity: 0.4;
            }
            to {
                opacity: 1;
            }
        }
    </style>
    <style>
        h2[_ngcontent-serverApp-c1758552152] {
            font-size: calc(14px + 10 * (100vw - 300px) / 1620) !important;
        }
        .h1[_ngcontent-serverApp-c1758552152] {
            font-size: calc(18px + 10 * (100vw - 300px) / 1620) !important;
        }
        .stronger-password[_ngcontent-serverApp-c1758552152] {
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #dc3545;
        }
        .light_green[_ngcontent-serverApp-c1758552152] {
            background: #dffff6 !important;
        }
        .light_yellow[_ngcontent-serverApp-c1758552152] {
            background: rgba(235, 186, 22, 0.75);
        }
        .light_blue_color[_ngcontent-serverApp-c1758552152] {
            background: #1c4c7c !important;
        }
        .light_blue[_ngcontent-serverApp-c1758552152] {
            background: #e0eefc !important;
        }
        .bg-primary[_ngcontent-serverApp-c1758552152] {
            background: #054a84 !important;
            color: #fff !important;
        }
        .invalid-feedback[_ngcontent-serverApp-c1758552152] {
            display: block;
        }
        .tp-section[_ngcontent-serverApp-c1758552152] {
            display: flex;
            align-content: center;
            align-items: center;
            height: 60px;
        }
        .tp-margin[_ngcontent-serverApp-c1758552152] {
            margin: 30px;
        }
        .pagination-PlanView[_ngcontent-serverApp-c1758552152] {
            margin: auto;
            display: table;
        }
        .active-group-pills[_ngcontent-serverApp-c1758552152] {
            line-height: 1.2;
            border: 3px solid;
            border-color: #eee;
        }
        .active-group-pills[_ngcontent-serverApp-c1758552152]:hover {
            border-color: #25ae88 !important;
        }
        h6[_ngcontent-serverApp-c1758552152] {
            font-size: calc(13px + 1 * (100vw - 300px) / 1620) !important;
        }
        .custom_footer_bar_plan_status[_ngcontent-serverApp-c1758552152] {
            width: 110%;
            margin: 15px -29px -24px;
        }
        @media screen and (max-width: 767px) {
            .custom_footer_bar_plan_status[_ngcontent-serverApp-c1758552152] {
                width: 130%;
            }
        }
        .icon-position[_ngcontent-serverApp-c1758552152] {
            position: absolute;
            top: 36px;
            left: -15px;
        }
        .Pc_float_left[_ngcontent-serverApp-c1758552152] {
            clear: both;
            padding: 0 !important;
        }
        .autoRenew[_ngcontent-serverApp-c1758552152] {
            position: relative;
            bottom: 15px;
            right: 50px;
        }
        .reminderEdit[_ngcontent-serverApp-c1758552152] .form-group[_ngcontent-serverApp-c1758552152] input[_ngcontent-serverApp-c1758552152] {
            width: auto !important;
        }
        .reminderEdit[_ngcontent-serverApp-c1758552152] .next_button[_ngcontent-serverApp-c1758552152]:hover {
            background: #ebba16 !important;
            color: #fff !important;
        }
        .password-meter[_ngcontent-serverApp-c1758552152] {
            font-size: 20px;
        }
        .switch-group[_ngcontent-serverApp-c1758552152] {
            padding-left: 35px;
        }
        .custom-control-input[_ngcontent-serverApp-c1758552152]:checked ~ .custom-control-label[_ngcontent-serverApp-c1758552152]:before {
            color: #fff;
            border-color: #25ae88;
            background-color: #25ae88;
        }
        .custom-control-input[_ngcontent-serverApp-c1758552152] ~ .custom-control-label[_ngcontent-serverApp-c1758552152]:before {
            box-shadow: none !important;
            border-color: #adb5bd;
        }
        .update-btn[_ngcontent-serverApp-c1758552152]:hover {
            background: #ebba16 !important;
            color: #fff !important;
        }
        .modal-header[_ngcontent-serverApp-c1758552152] {
            padding: 1rem;
            background: white;
        }
        .reminder-back-button[_ngcontent-serverApp-c1758552152] {
            background: #054a84;
            color: #e0eefc;
            border: 0;
            padding: 6px 22px;
            border-radius: 5px;
        }
        .reminder-back-button[_ngcontent-serverApp-c1758552152]:hover {
            background: #ebba16 !important;
            color: #fff !important;
        }
        .page-section[_ngcontent-serverApp-c1758552152] {
            margin-bottom: 12rem;
        }
        .plan-header[_ngcontent-serverApp-c1758552152] {
            border-bottom: 3px solid #ebba16;
        }
        .top-banner-wrapper[_ngcontent-serverApp-c1758552152] a[_ngcontent-serverApp-c1758552152] {
            top: 50% !important;
        }
        .mat-tab-body-content,
        .mat-tab-body[_ngcontent-serverApp-c1758552152],
        .mat-tab-body-wrapper[_ngcontent-serverApp-c1758552152] {
            transition: none !important;
            transform: none !important;
            animation-duration: 0ms !important;
        }
        [_nghost-serverApp-c1758552152] .mat-mdc-tab-group,
        .mat-mdc-tab-nav-bar[_ngcontent-serverApp-c1758552152] {
            --mat-tab-header-active-ripple-color: #fff !important;
            --mat-tab-header-inactive-ripple-color: #fff !important;
        }
        [_nghost-serverApp-c1758552152] .mat-mdc-tab .mdc-tab-indicator__content--underline {
            border-color: #000 !important;
        }
        [_nghost-serverApp-c1758552152] .mat-mdc-tab.mdc-tab--active .mdc-tab__text-label {
            color: #6c757d !important;
        }
        [_nghost-serverApp-c1758552152] .mat-mdc-tab-labels {
            border-bottom: 1px solid #dee2e6 !important;
        }
        [_nghost-serverApp-c1758552152] .mdc-tab-indicator {
            top: 1px !important;
        }
        [_nghost-serverApp-c1758552152] .mat-mdc-tab-group-dynamic-height .mat-mdc-tab-body-content {
            padding-top: 12px !important;
        }
    </style>
    <style>
        .agency_header[_ngcontent-serverApp-c1951524160] {
            background-color: #ededed;
        }
        .breadcrumb-text[_ngcontent-serverApp-c1951524160] {
            font-size: calc(20px + 1 * (100vw - 300px) / 1620);
        }
    </style>
    <style>
        .mdc-tab {
            min-width: 90px;
            padding-right: 24px;
            padding-left: 24px;
            display: flex;
            flex: 1 0 auto;
            justify-content: center;
            box-sizing: border-box;
            margin: 0;
            padding-top: 0;
            padding-bottom: 0;
            border: none;
            outline: none;
            text-align: center;
            white-space: nowrap;
            cursor: pointer;
            -webkit-appearance: none;
            z-index: 1;
        }
        .mdc-tab::-moz-focus-inner {
            padding: 0;
            border: 0;
        }
        .mdc-tab[hidden] {
            display: none;
        }
        .mdc-tab--min-width {
            flex: 0 1 auto;
        }
        .mdc-tab__content {
            display: flex;
            align-items: center;
            justify-content: center;
            height: inherit;
            pointer-events: none;
        }
        .mdc-tab__text-label {
            transition: 150ms color linear;
            display: inline-block;
            line-height: 1;
            z-index: 2;
        }
        .mdc-tab__icon {
            transition: 150ms color linear;
            z-index: 2;
        }
        .mdc-tab--stacked .mdc-tab__content {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
        .mdc-tab--stacked .mdc-tab__text-label {
            padding-top: 6px;
            padding-bottom: 4px;
        }
        .mdc-tab--active .mdc-tab__text-label,
        .mdc-tab--active .mdc-tab__icon {
            transition-delay: 100ms;
        }
        .mdc-tab:not(.mdc-tab--stacked) .mdc-tab__icon + .mdc-tab__text-label {
            padding-left: 8px;
            padding-right: 0;
        }
        [dir="rtl"] .mdc-tab:not(.mdc-tab--stacked) .mdc-tab__icon + .mdc-tab__text-label,
        .mdc-tab:not(.mdc-tab--stacked) .mdc-tab__icon + .mdc-tab__text-label[dir="rtl"] {
            padding-left: 0;
            padding-right: 8px;
        }
        .mdc-tab-indicator {
            display: flex;
            position: absolute;
            top: 0;
            left: 0;
            justify-content: center;
            width: 100%;
            height: 100%;
            pointer-events: none;
            z-index: 1;
        }
        .mdc-tab-indicator__content {
            transform-origin: left;
            opacity: 0;
        }
        .mdc-tab-indicator__content--underline {
            align-self: flex-end;
            box-sizing: border-box;
            width: 100%;
            border-top-style: solid;
        }
        .mdc-tab-indicator__content--icon {
            align-self: center;
            margin: 0 auto;
        }
        .mdc-tab-indicator--active .mdc-tab-indicator__content {
            opacity: 1;
        }
        .mdc-tab-indicator .mdc-tab-indicator__content {
            transition: 250ms transform cubic-bezier(0.4, 0, 0.2, 1);
        }
        .mdc-tab-indicator--no-transition .mdc-tab-indicator__content {
            transition: none;
        }
        .mdc-tab-indicator--fade .mdc-tab-indicator__content {
            transition: 150ms opacity linear;
        }
        .mdc-tab-indicator--active.mdc-tab-indicator--fade .mdc-tab-indicator__content {
            transition-delay: 100ms;
        }
        .mat-mdc-tab-ripple {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            pointer-events: none;
        }
        .mat-mdc-tab {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-decoration: none;
            background: none;
            font-family: var(--mat-tab-header-label-text-font);
            font-size: var(--mat-tab-header-label-text-size);
            letter-spacing: var(--mat-tab-header-label-text-tracking);
            line-height: var(--mat-tab-header-label-text-line-height);
            font-weight: var(--mat-tab-header-label-text-weight);
        }
        .mat-mdc-tab .mdc-tab-indicator__content--underline {
            border-color: var(--mdc-tab-indicator-active-indicator-color);
        }
        .mat-mdc-tab .mdc-tab-indicator__content--underline {
            border-top-width: var(--mdc-tab-indicator-active-indicator-height);
        }
        .mat-mdc-tab .mdc-tab-indicator__content--underline {
            border-radius: var(--mdc-tab-indicator-active-indicator-shape);
        }
        .mat-mdc-tab:not(.mdc-tab--stacked) {
            height: var(--mdc-secondary-navigation-tab-container-height);
        }
        .mat-mdc-tab:not(:disabled).mdc-tab--active .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:not(:disabled):hover.mdc-tab--active .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:not(:disabled):focus.mdc-tab--active .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:not(:disabled):active.mdc-tab--active .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:disabled.mdc-tab--active .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:not(:disabled):not(.mdc-tab--active) .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:not(:disabled):hover:not(.mdc-tab--active) .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:not(:disabled):focus:not(.mdc-tab--active) .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:not(:disabled):active:not(.mdc-tab--active) .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab:disabled:not(.mdc-tab--active) .mdc-tab__icon {
            fill: currentColor;
        }
        .mat-mdc-tab.mdc-tab {
            flex-grow: 0;
        }
        .mat-mdc-tab:hover .mdc-tab__text-label {
            color: var(--mat-tab-header-inactive-hover-label-text-color);
        }
        .mat-mdc-tab:focus .mdc-tab__text-label {
            color: var(--mat-tab-header-inactive-focus-label-text-color);
        }
        .mat-mdc-tab.mdc-tab--active .mdc-tab__text-label {
            color: var(--mat-tab-header-active-label-text-color);
        }
        .mat-mdc-tab.mdc-tab--active .mdc-tab__ripple::before,
        .mat-mdc-tab.mdc-tab--active .mat-ripple-element {
            background-color: var(--mat-tab-header-active-ripple-color);
        }
        .mat-mdc-tab.mdc-tab--active:hover .mdc-tab__text-label {
            color: var(--mat-tab-header-active-hover-label-text-color);
        }
        .mat-mdc-tab.mdc-tab--active:hover .mdc-tab-indicator__content--underline {
            border-color: var(--mat-tab-header-active-hover-indicator-color);
        }
        .mat-mdc-tab.mdc-tab--active:focus .mdc-tab__text-label {
            color: var(--mat-tab-header-active-focus-label-text-color);
        }
        .mat-mdc-tab.mdc-tab--active:focus .mdc-tab-indicator__content--underline {
            border-color: var(--mat-tab-header-active-focus-indicator-color);
        }
        .mat-mdc-tab.mat-mdc-tab-disabled {
            opacity: 0.4;
            pointer-events: none;
        }
        .mat-mdc-tab.mat-mdc-tab-disabled .mdc-tab__content {
            pointer-events: none;
        }
        .mat-mdc-tab.mat-mdc-tab-disabled .mdc-tab__ripple::before,
        .mat-mdc-tab.mat-mdc-tab-disabled .mat-ripple-element {
            background-color: var(--mat-tab-header-disabled-ripple-color);
        }
        .mat-mdc-tab .mdc-tab__ripple::before {
            content: "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            opacity: 0;
            pointer-events: none;
            background-color: var(--mat-tab-header-inactive-ripple-color);
        }
        .mat-mdc-tab .mdc-tab__text-label {
            color: var(--mat-tab-header-inactive-label-text-color);
            display: inline-flex;
            align-items: center;
        }
        .mat-mdc-tab .mdc-tab__content {
            position: relative;
            pointer-events: auto;
        }
        .mat-mdc-tab:hover .mdc-tab__ripple::before {
            opacity: 0.04;
        }
        .mat-mdc-tab.cdk-program-focused .mdc-tab__ripple::before,
        .mat-mdc-tab.cdk-keyboard-focused .mdc-tab__ripple::before {
            opacity: 0.12;
        }
        .mat-mdc-tab .mat-ripple-element {
            opacity: 0.12;
            background-color: var(--mat-tab-header-inactive-ripple-color);
        }
        .mat-mdc-tab-group.mat-mdc-tab-group-stretch-tabs > .mat-mdc-tab-header .mat-mdc-tab {
            flex-grow: 1;
        }
        .mat-mdc-tab-group {
            display: flex;
            flex-direction: column;
            max-width: 100%;
        }
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header-pagination {
            background-color: var(--mat-tab-header-with-background-background-color);
        }
        .mat-mdc-tab-group.mat-tabs-with-background.mat-primary > .mat-mdc-tab-header .mat-mdc-tab .mdc-tab__text-label {
            color: var(--mat-tab-header-with-background-foreground-color);
        }
        .mat-mdc-tab-group.mat-tabs-with-background.mat-primary > .mat-mdc-tab-header .mdc-tab-indicator__content--underline {
            border-color: var(--mat-tab-header-with-background-foreground-color);
        }
        .mat-mdc-tab-group.mat-tabs-with-background:not(.mat-primary) > .mat-mdc-tab-header .mat-mdc-tab:not(.mdc-tab--active) .mdc-tab__text-label {
            color: var(--mat-tab-header-with-background-foreground-color);
        }
        .mat-mdc-tab-group.mat-tabs-with-background:not(.mat-primary) > .mat-mdc-tab-header .mat-mdc-tab:not(.mdc-tab--active) .mdc-tab-indicator__content--underline {
            border-color: var(--mat-tab-header-with-background-foreground-color);
        }
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header .mat-mdc-tab-header-pagination-chevron,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header .mat-mdc-focus-indicator::before,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header-pagination .mat-mdc-tab-header-pagination-chevron,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header-pagination .mat-mdc-focus-indicator::before {
            border-color: var(--mat-tab-header-with-background-foreground-color);
        }
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header .mat-ripple-element,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header .mdc-tab__ripple::before,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header-pagination .mat-ripple-element,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header-pagination .mdc-tab__ripple::before {
            background-color: var(--mat-tab-header-with-background-foreground-color);
        }
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header .mat-mdc-tab-header-pagination-chevron,
        .mat-mdc-tab-group.mat-tabs-with-background > .mat-mdc-tab-header-pagination .mat-mdc-tab-header-pagination-chevron {
            color: var(--mat-tab-header-with-background-foreground-color);
        }
        .mat-mdc-tab-group.mat-mdc-tab-group-inverted-header {
            flex-direction: column-reverse;
        }
        .mat-mdc-tab-group.mat-mdc-tab-group-inverted-header .mdc-tab-indicator__content--underline {
            align-self: flex-start;
        }
        .mat-mdc-tab-body-wrapper {
            position: relative;
            overflow: hidden;
            display: flex;
            transition: height 500ms cubic-bezier(0.35, 0, 0.25, 1);
        }
        .mat-mdc-tab-body-wrapper._mat-animation-noopable {
            transition: none !important;
            animation: none !important;
        }
    </style>
    <style>
        .mat-mdc-tab-header {
            display: flex;
            overflow: hidden;
            position: relative;
            flex-shrink: 0;
            --mdc-tab-indicator-active-indicator-height: 2px;
            --mdc-tab-indicator-active-indicator-shape: 0;
            --mdc-secondary-navigation-tab-container-height: 48px;
        }
        .mdc-tab-indicator .mdc-tab-indicator__content {
            transition-duration: var(--mat-tab-animation-duration, 250ms);
        }
        .mat-mdc-tab-header-pagination {
            -webkit-user-select: none;
            user-select: none;
            position: relative;
            display: none;
            justify-content: center;
            align-items: center;
            min-width: 32px;
            cursor: pointer;
            z-index: 2;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            touch-action: none;
            box-sizing: content-box;
            background: none;
            border: none;
            outline: 0;
            padding: 0;
        }
        .mat-mdc-tab-header-pagination::-moz-focus-inner {
            border: 0;
        }
        .mat-mdc-tab-header-pagination .mat-ripple-element {
            opacity: 0.12;
            background-color: var(--mat-tab-header-inactive-ripple-color);
        }
        .mat-mdc-tab-header-pagination-controls-enabled .mat-mdc-tab-header-pagination {
            display: flex;
        }
        .mat-mdc-tab-header-pagination-before,
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-after {
            padding-left: 4px;
        }
        .mat-mdc-tab-header-pagination-before .mat-mdc-tab-header-pagination-chevron,
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-after .mat-mdc-tab-header-pagination-chevron {
            transform: rotate(-135deg);
        }
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-before,
        .mat-mdc-tab-header-pagination-after {
            padding-right: 4px;
        }
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-before .mat-mdc-tab-header-pagination-chevron,
        .mat-mdc-tab-header-pagination-after .mat-mdc-tab-header-pagination-chevron {
            transform: rotate(45deg);
        }
        .mat-mdc-tab-header-pagination-chevron {
            border-style: solid;
            border-width: 2px 2px 0 0;
            height: 8px;
            width: 8px;
            border-color: var(--mat-tab-header-pagination-icon-color);
        }
        .mat-mdc-tab-header-pagination-disabled {
            box-shadow: none;
            cursor: default;
            pointer-events: none;
        }
        .mat-mdc-tab-header-pagination-disabled .mat-mdc-tab-header-pagination-chevron {
            opacity: 0.4;
        }
        .mat-mdc-tab-list {
            flex-grow: 1;
            position: relative;
            transition: transform 500ms cubic-bezier(0.35, 0, 0.25, 1);
        }
        ._mat-animation-noopable .mat-mdc-tab-list {
            transition: none;
        }
        ._mat-animation-noopable span.mdc-tab-indicator__content,
        ._mat-animation-noopable span.mdc-tab__text-label {
            transition: none;
        }
        .mat-mdc-tab-label-container {
            display: flex;
            flex-grow: 1;
            overflow: hidden;
            z-index: 1;
        }
        .mat-mdc-tab-labels {
            display: flex;
            flex: 1 0 auto;
        }
        [mat-align-tabs="center"] > .mat-mdc-tab-header .mat-mdc-tab-labels {
            justify-content: center;
        }
        [mat-align-tabs="end"] > .mat-mdc-tab-header .mat-mdc-tab-labels {
            justify-content: flex-end;
        }
        .mat-mdc-tab::before {
            margin: 5px;
        }
        .cdk-high-contrast-active .mat-mdc-tab[aria-disabled="true"] {
            color: GrayText;
        }
    </style>
    <style>
        .mat-mdc-tab-header {
            display: flex;
            overflow: hidden;
            position: relative;
            flex-shrink: 0;
            --mdc-tab-indicator-active-indicator-height: 2px;
            --mdc-tab-indicator-active-indicator-shape: 0;
            --mdc-secondary-navigation-tab-container-height: 48px;
        }
        .mdc-tab-indicator .mdc-tab-indicator__content {
            transition-duration: var(--mat-tab-animation-duration, 250ms);
        }
        .mat-mdc-tab-header-pagination {
            -webkit-user-select: none;
            user-select: none;
            position: relative;
            display: none;
            justify-content: center;
            align-items: center;
            min-width: 32px;
            cursor: pointer;
            z-index: 2;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            touch-action: none;
            box-sizing: content-box;
            background: none;
            border: none;
            outline: 0;
            padding: 0;
        }
        .mat-mdc-tab-header-pagination::-moz-focus-inner {
            border: 0;
        }
        .mat-mdc-tab-header-pagination .mat-ripple-element {
            opacity: 0.12;
            background-color: var(--mat-tab-header-inactive-ripple-color);
        }
        .mat-mdc-tab-header-pagination-controls-enabled .mat-mdc-tab-header-pagination {
            display: flex;
        }
        .mat-mdc-tab-header-pagination-before,
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-after {
            padding-left: 4px;
        }
        .mat-mdc-tab-header-pagination-before .mat-mdc-tab-header-pagination-chevron,
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-after .mat-mdc-tab-header-pagination-chevron {
            transform: rotate(-135deg);
        }
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-before,
        .mat-mdc-tab-header-pagination-after {
            padding-right: 4px;
        }
        .mat-mdc-tab-header-rtl .mat-mdc-tab-header-pagination-before .mat-mdc-tab-header-pagination-chevron,
        .mat-mdc-tab-header-pagination-after .mat-mdc-tab-header-pagination-chevron {
            transform: rotate(45deg);
        }
        .mat-mdc-tab-header-pagination-chevron {
            border-style: solid;
            border-width: 2px 2px 0 0;
            height: 8px;
            width: 8px;
            border-color: var(--mat-tab-header-pagination-icon-color);
        }
        .mat-mdc-tab-header-pagination-disabled {
            box-shadow: none;
            cursor: default;
            pointer-events: none;
        }
        .mat-mdc-tab-header-pagination-disabled .mat-mdc-tab-header-pagination-chevron {
            opacity: 0.4;
        }
        .mat-mdc-tab-list {
            flex-grow: 1;
            position: relative;
            transition: transform 500ms cubic-bezier(0.35, 0, 0.25, 1);
        }
        ._mat-animation-noopable .mat-mdc-tab-list {
            transition: none;
        }
        ._mat-animation-noopable span.mdc-tab-indicator__content,
        ._mat-animation-noopable span.mdc-tab__text-label {
            transition: none;
        }
        .mat-mdc-tab-label-container {
            display: flex;
            flex-grow: 1;
            overflow: hidden;
            z-index: 1;
        }
        .mat-mdc-tab-labels {
            display: flex;
            flex: 1 0 auto;
        }
        [mat-align-tabs="center"] > .mat-mdc-tab-header .mat-mdc-tab-labels {
            justify-content: center;
        }
        [mat-align-tabs="end"] > .mat-mdc-tab-header .mat-mdc-tab-labels {
            justify-content: flex-end;
        }
        .mat-mdc-tab::before {
            margin: 5px;
        }
        .cdk-high-contrast-active .mat-mdc-tab[aria-disabled="true"] {
            color: GrayText;
        }
    </style>
    <style>
        .mat-mdc-tab-body {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            display: block;
            overflow: hidden;
            outline: 0;
            flex-basis: 100%;
        }
        .mat-mdc-tab-body.mat-mdc-tab-body-active {
            position: relative;
            overflow-x: hidden;
            overflow-y: auto;
            z-index: 1;
            flex-grow: 1;
        }
        .mat-mdc-tab-group.mat-mdc-tab-group-dynamic-height .mat-mdc-tab-body.mat-mdc-tab-body-active {
            overflow-y: hidden;
        }
        .mat-mdc-tab-body-content {
            height: 100%;
            overflow: auto;
        }
        .mat-mdc-tab-group-dynamic-height .mat-mdc-tab-body-content {
            overflow: hidden;
        }
        .mat-mdc-tab-body-content[style*="visibility: hidden"] {
            display: none;
        }
    </style>
    <style>
        .mdc-text-field {
            border-top-left-radius: 4px;
            border-top-left-radius: var(--mdc-shape-small, 4px);
            border-top-right-radius: 4px;
            border-top-right-radius: var(--mdc-shape-small, 4px);
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            display: inline-flex;
            align-items: baseline;
            padding: 0 16px;
            position: relative;
            box-sizing: border-box;
            overflow: hidden;
            will-change: opacity, transform, color;
        }
        .mdc-text-field .mdc-floating-label {
            top: 50%;
            transform: translateY(-50%);
            pointer-events: none;
        }
        .mdc-text-field__input {
            height: 28px;
            width: 100%;
            min-width: 0;
            border: none;
            border-radius: 0;
            background: none;
            appearance: none;
            padding: 0;
        }
        .mdc-text-field__input::-ms-clear {
            display: none;
        }
        .mdc-text-field__input::-webkit-calendar-picker-indicator {
            display: none;
        }
        .mdc-text-field__input:focus {
            outline: none;
        }
        .mdc-text-field__input:invalid {
            box-shadow: none;
        }
        @media all {
            .mdc-text-field__input::placeholder {
                opacity: 0;
            }
        }
        @media all {
            .mdc-text-field__input:-ms-input-placeholder {
                opacity: 0;
            }
        }
        @media all {
            .mdc-text-field--no-label .mdc-text-field__input::placeholder,
            .mdc-text-field--focused .mdc-text-field__input::placeholder {
                opacity: 1;
            }
        }
        @media all {
            .mdc-text-field--no-label .mdc-text-field__input:-ms-input-placeholder,
            .mdc-text-field--focused .mdc-text-field__input:-ms-input-placeholder {
                opacity: 1;
            }
        }
        .mdc-text-field__affix {
            height: 28px;
            opacity: 0;
            white-space: nowrap;
        }
        .mdc-text-field--label-floating .mdc-text-field__affix,
        .mdc-text-field--no-label .mdc-text-field__affix {
            opacity: 1;
        }
        @supports (-webkit-hyphens: none) {
            .mdc-text-field--outlined .mdc-text-field__affix {
                align-items: center;
                align-self: center;
                display: inline-flex;
                height: 100%;
            }
        }
        .mdc-text-field__affix--prefix {
            padding-left: 0;
            padding-right: 2px;
        }
        [dir="rtl"] .mdc-text-field__affix--prefix,
        .mdc-text-field__affix--prefix[dir="rtl"] {
            padding-left: 2px;
            padding-right: 0;
        }
        .mdc-text-field--end-aligned .mdc-text-field__affix--prefix {
            padding-left: 0;
            padding-right: 12px;
        }
        [dir="rtl"] .mdc-text-field--end-aligned .mdc-text-field__affix--prefix,
        .mdc-text-field--end-aligned .mdc-text-field__affix--prefix[dir="rtl"] {
            padding-left: 12px;
            padding-right: 0;
        }
        .mdc-text-field__affix--suffix {
            padding-left: 12px;
            padding-right: 0;
        }
        [dir="rtl"] .mdc-text-field__affix--suffix,
        .mdc-text-field__affix--suffix[dir="rtl"] {
            padding-left: 0;
            padding-right: 12px;
        }
        .mdc-text-field--end-aligned .mdc-text-field__affix--suffix {
            padding-left: 2px;
            padding-right: 0;
        }
        [dir="rtl"] .mdc-text-field--end-aligned .mdc-text-field__affix--suffix,
        .mdc-text-field--end-aligned .mdc-text-field__affix--suffix[dir="rtl"] {
            padding-left: 0;
            padding-right: 2px;
        }
        .mdc-text-field--filled {
            height: 56px;
        }
        .mdc-text-field--filled::before {
            display: inline-block;
            width: 0;
            height: 40px;
            content: "";
            vertical-align: 0;
        }
        .mdc-text-field--filled .mdc-floating-label {
            left: 16px;
            right: initial;
        }
        [dir="rtl"] .mdc-text-field--filled .mdc-floating-label,
        .mdc-text-field--filled .mdc-floating-label[dir="rtl"] {
            left: initial;
            right: 16px;
        }
        .mdc-text-field--filled .mdc-floating-label--float-above {
            transform: translateY(-106%) scale(0.75);
        }
        .mdc-text-field--filled.mdc-text-field--no-label .mdc-text-field__input {
            height: 100%;
        }
        .mdc-text-field--filled.mdc-text-field--no-label .mdc-floating-label {
            display: none;
        }
        .mdc-text-field--filled.mdc-text-field--no-label::before {
            display: none;
        }
        @supports (-webkit-hyphens: none) {
            .mdc-text-field--filled.mdc-text-field--no-label .mdc-text-field__affix {
                align-items: center;
                align-self: center;
                display: inline-flex;
                height: 100%;
            }
        }
        .mdc-text-field--outlined {
            height: 56px;
            overflow: visible;
        }
        .mdc-text-field--outlined .mdc-floating-label--float-above {
            transform: translateY(-37.25px) scale(1);
        }
        .mdc-text-field--outlined .mdc-floating-label--float-above {
            font-size: 0.75rem;
        }
        .mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        .mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            transform: translateY(-34.75px) scale(0.75);
        }
        .mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        .mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            font-size: 1rem;
        }
        .mdc-text-field--outlined .mdc-text-field__input {
            height: 100%;
        }
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading {
            border-top-left-radius: 4px;
            border-top-left-radius: var(--mdc-shape-small, 4px);
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 4px;
            border-bottom-left-radius: var(--mdc-shape-small, 4px);
        }
        [dir="rtl"] .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading,
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading[dir="rtl"] {
            border-top-left-radius: 0;
            border-top-right-radius: 4px;
            border-top-right-radius: var(--mdc-shape-small, 4px);
            border-bottom-right-radius: 4px;
            border-bottom-right-radius: var(--mdc-shape-small, 4px);
            border-bottom-left-radius: 0;
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading {
                width: max(12px, var(--mdc-shape-small, 4px));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__notch {
                max-width: calc(100% - max(12px, var(--mdc-shape-small, 4px)) * 2);
            }
        }
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__trailing {
            border-top-left-radius: 0;
            border-top-right-radius: 4px;
            border-top-right-radius: var(--mdc-shape-small, 4px);
            border-bottom-right-radius: 4px;
            border-bottom-right-radius: var(--mdc-shape-small, 4px);
            border-bottom-left-radius: 0;
        }
        [dir="rtl"] .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__trailing,
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__trailing[dir="rtl"] {
            border-top-left-radius: 4px;
            border-top-left-radius: var(--mdc-shape-small, 4px);
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 4px;
            border-bottom-left-radius: var(--mdc-shape-small, 4px);
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined {
                padding-left: max(16px, calc(var(--mdc-shape-small, 4px) + 4px));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined {
                padding-right: max(16px, var(--mdc-shape-small, 4px));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined + .mdc-text-field-helper-line {
                padding-left: max(16px, calc(var(--mdc-shape-small, 4px) + 4px));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined + .mdc-text-field-helper-line {
                padding-right: max(16px, var(--mdc-shape-small, 4px));
            }
        }
        .mdc-text-field--outlined.mdc-text-field--with-leading-icon {
            padding-left: 0;
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined.mdc-text-field--with-leading-icon {
                padding-right: max(16px, var(--mdc-shape-small, 4px));
            }
        }
        [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-leading-icon,
        .mdc-text-field--outlined.mdc-text-field--with-leading-icon[dir="rtl"] {
            padding-right: 0;
        }
        @supports (top: max(0%)) {
            [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-leading-icon,
            .mdc-text-field--outlined.mdc-text-field--with-leading-icon[dir="rtl"] {
                padding-left: max(16px, var(--mdc-shape-small, 4px));
            }
        }
        .mdc-text-field--outlined.mdc-text-field--with-trailing-icon {
            padding-right: 0;
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined.mdc-text-field--with-trailing-icon {
                padding-left: max(16px, calc(var(--mdc-shape-small, 4px) + 4px));
            }
        }
        [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-trailing-icon,
        .mdc-text-field--outlined.mdc-text-field--with-trailing-icon[dir="rtl"] {
            padding-left: 0;
        }
        @supports (top: max(0%)) {
            [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-trailing-icon,
            .mdc-text-field--outlined.mdc-text-field--with-trailing-icon[dir="rtl"] {
                padding-right: max(16px, calc(var(--mdc-shape-small, 4px) + 4px));
            }
        }
        .mdc-text-field--outlined.mdc-text-field--with-leading-icon.mdc-text-field--with-trailing-icon {
            padding-left: 0;
            padding-right: 0;
        }
        .mdc-text-field--outlined .mdc-notched-outline--notched .mdc-notched-outline__notch {
            padding-top: 1px;
        }
        .mdc-text-field--outlined .mdc-floating-label {
            left: 4px;
            right: initial;
        }
        [dir="rtl"] .mdc-text-field--outlined .mdc-floating-label,
        .mdc-text-field--outlined .mdc-floating-label[dir="rtl"] {
            left: initial;
            right: 4px;
        }
        .mdc-text-field--outlined .mdc-text-field__input {
            display: flex;
            border: none !important;
            background-color: rgba(0, 0, 0, 0);
        }
        .mdc-text-field--outlined .mdc-notched-outline {
            z-index: 1;
        }
        .mdc-text-field--textarea {
            flex-direction: column;
            align-items: center;
            width: auto;
            height: auto;
            padding: 0;
        }
        .mdc-text-field--textarea .mdc-floating-label {
            top: 19px;
        }
        .mdc-text-field--textarea .mdc-floating-label:not(.mdc-floating-label--float-above) {
            transform: none;
        }
        .mdc-text-field--textarea .mdc-text-field__input {
            flex-grow: 1;
            height: auto;
            min-height: 1.5rem;
            overflow-x: hidden;
            overflow-y: auto;
            box-sizing: border-box;
            resize: none;
            padding: 0 16px;
        }
        .mdc-text-field--textarea.mdc-text-field--filled::before {
            display: none;
        }
        .mdc-text-field--textarea.mdc-text-field--filled .mdc-floating-label--float-above {
            transform: translateY(-10.25px) scale(0.75);
        }
        .mdc-text-field--textarea.mdc-text-field--filled .mdc-text-field__input {
            margin-top: 23px;
            margin-bottom: 9px;
        }
        .mdc-text-field--textarea.mdc-text-field--filled.mdc-text-field--no-label .mdc-text-field__input {
            margin-top: 16px;
            margin-bottom: 16px;
        }
        .mdc-text-field--textarea.mdc-text-field--outlined .mdc-notched-outline--notched .mdc-notched-outline__notch {
            padding-top: 0;
        }
        .mdc-text-field--textarea.mdc-text-field--outlined .mdc-floating-label--float-above {
            transform: translateY(-27.25px) scale(1);
        }
        .mdc-text-field--textarea.mdc-text-field--outlined .mdc-floating-label--float-above {
            font-size: 0.75rem;
        }
        .mdc-text-field--textarea.mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        .mdc-text-field--textarea.mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            transform: translateY(-24.75px) scale(0.75);
        }
        .mdc-text-field--textarea.mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        .mdc-text-field--textarea.mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            font-size: 1rem;
        }
        .mdc-text-field--textarea.mdc-text-field--outlined .mdc-text-field__input {
            margin-top: 16px;
            margin-bottom: 16px;
        }
        .mdc-text-field--textarea.mdc-text-field--outlined .mdc-floating-label {
            top: 18px;
        }
        .mdc-text-field--textarea.mdc-text-field--with-internal-counter .mdc-text-field__input {
            margin-bottom: 2px;
        }
        .mdc-text-field--textarea.mdc-text-field--with-internal-counter .mdc-text-field-character-counter {
            align-self: flex-end;
            padding: 0 16px;
        }
        .mdc-text-field--textarea.mdc-text-field--with-internal-counter .mdc-text-field-character-counter::after {
            display: inline-block;
            width: 0;
            height: 16px;
            content: "";
            vertical-align: -16px;
        }
        .mdc-text-field--textarea.mdc-text-field--with-internal-counter .mdc-text-field-character-counter::before {
            display: none;
        }
        .mdc-text-field__resizer {
            align-self: stretch;
            display: inline-flex;
            flex-direction: column;
            flex-grow: 1;
            max-height: 100%;
            max-width: 100%;
            min-height: 56px;
            min-width: fit-content;
            min-width: -moz-available;
            min-width: -webkit-fill-available;
            overflow: hidden;
            resize: both;
        }
        .mdc-text-field--filled .mdc-text-field__resizer {
            transform: translateY(-1px);
        }
        .mdc-text-field--filled .mdc-text-field__resizer .mdc-text-field__input,
        .mdc-text-field--filled .mdc-text-field__resizer .mdc-text-field-character-counter {
            transform: translateY(1px);
        }
        .mdc-text-field--outlined .mdc-text-field__resizer {
            transform: translateX(-1px) translateY(-1px);
        }
        [dir="rtl"] .mdc-text-field--outlined .mdc-text-field__resizer,
        .mdc-text-field--outlined .mdc-text-field__resizer[dir="rtl"] {
            transform: translateX(1px) translateY(-1px);
        }
        .mdc-text-field--outlined .mdc-text-field__resizer .mdc-text-field__input,
        .mdc-text-field--outlined .mdc-text-field__resizer .mdc-text-field-character-counter {
            transform: translateX(1px) translateY(1px);
        }
        [dir="rtl"] .mdc-text-field--outlined .mdc-text-field__resizer .mdc-text-field__input,
        [dir="rtl"] .mdc-text-field--outlined .mdc-text-field__resizer .mdc-text-field-character-counter,
        .mdc-text-field--outlined .mdc-text-field__resizer .mdc-text-field__input[dir="rtl"],
        .mdc-text-field--outlined .mdc-text-field__resizer .mdc-text-field-character-counter[dir="rtl"] {
            transform: translateX(-1px) translateY(1px);
        }
        .mdc-text-field--with-leading-icon {
            padding-left: 0;
            padding-right: 16px;
        }
        [dir="rtl"] .mdc-text-field--with-leading-icon,
        .mdc-text-field--with-leading-icon[dir="rtl"] {
            padding-left: 16px;
            padding-right: 0;
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--filled .mdc-floating-label {
            max-width: calc(100% - 48px);
            left: 48px;
            right: initial;
        }
        [dir="rtl"] .mdc-text-field--with-leading-icon.mdc-text-field--filled .mdc-floating-label,
        .mdc-text-field--with-leading-icon.mdc-text-field--filled .mdc-floating-label[dir="rtl"] {
            left: initial;
            right: 48px;
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--filled .mdc-floating-label--float-above {
            max-width: calc(100% / 0.75 - 64px / 0.75);
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label {
            left: 36px;
            right: initial;
        }
        [dir="rtl"] .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label,
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label[dir="rtl"] {
            left: initial;
            right: 36px;
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined :not(.mdc-notched-outline--notched) .mdc-notched-outline__notch {
            max-width: calc(100% - 60px);
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label--float-above {
            transform: translateY(-37.25px) translateX(-32px) scale(1);
        }
        [dir="rtl"] .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label--float-above,
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label--float-above[dir="rtl"] {
            transform: translateY(-37.25px) translateX(32px) scale(1);
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label--float-above {
            font-size: 0.75rem;
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            transform: translateY(-34.75px) translateX(-32px) scale(0.75);
        }
        [dir="rtl"] .mdc-text-field--with-leading-icon.mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        [dir="rtl"] .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above[dir="rtl"],
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above[dir="rtl"] {
            transform: translateY(-34.75px) translateX(32px) scale(0.75);
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined.mdc-notched-outline--upgraded .mdc-floating-label--float-above,
        .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            font-size: 1rem;
        }
        .mdc-text-field--with-trailing-icon {
            padding-left: 16px;
            padding-right: 0;
        }
        [dir="rtl"] .mdc-text-field--with-trailing-icon,
        .mdc-text-field--with-trailing-icon[dir="rtl"] {
            padding-left: 0;
            padding-right: 16px;
        }
        .mdc-text-field--with-trailing-icon.mdc-text-field--filled .mdc-floating-label {
            max-width: calc(100% - 64px);
        }
        .mdc-text-field--with-trailing-icon.mdc-text-field--filled .mdc-floating-label--float-above {
            max-width: calc(100% / 0.75 - 64px / 0.75);
        }
        .mdc-text-field--with-trailing-icon.mdc-text-field--outlined :not(.mdc-notched-outline--notched) .mdc-notched-outline__notch {
            max-width: calc(100% - 60px);
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--with-trailing-icon {
            padding-left: 0;
            padding-right: 0;
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--with-trailing-icon.mdc-text-field--filled .mdc-floating-label {
            max-width: calc(100% - 96px);
        }
        .mdc-text-field--with-leading-icon.mdc-text-field--with-trailing-icon.mdc-text-field--filled .mdc-floating-label--float-above {
            max-width: calc(100% / 0.75 - 96px / 0.75);
        }
        .mdc-text-field-helper-line {
            display: flex;
            justify-content: space-between;
            box-sizing: border-box;
        }
        .mdc-text-field + .mdc-text-field-helper-line {
            padding-right: 16px;
            padding-left: 16px;
        }
        .mdc-form-field > .mdc-text-field + label {
            align-self: flex-start;
        }
        .mdc-text-field--focused .mdc-notched-outline__leading,
        .mdc-text-field--focused .mdc-notched-outline__notch,
        .mdc-text-field--focused .mdc-notched-outline__trailing {
            border-width: 2px;
        }
        .mdc-text-field--focused + .mdc-text-field-helper-line .mdc-text-field-helper-text:not(.mdc-text-field-helper-text--validation-msg) {
            opacity: 1;
        }
        .mdc-text-field--focused.mdc-text-field--outlined .mdc-notched-outline--notched .mdc-notched-outline__notch {
            padding-top: 2px;
        }
        .mdc-text-field--focused.mdc-text-field--outlined.mdc-text-field--textarea .mdc-notched-outline--notched .mdc-notched-outline__notch {
            padding-top: 0;
        }
        .mdc-text-field--invalid + .mdc-text-field-helper-line .mdc-text-field-helper-text--validation-msg {
            opacity: 1;
        }
        .mdc-text-field--disabled {
            pointer-events: none;
        }
        @media screen and (forced-colors: active) {
            .mdc-text-field--disabled .mdc-text-field__input {
                background-color: Window;
            }
            .mdc-text-field--disabled .mdc-floating-label {
                z-index: 1;
            }
        }
        .mdc-text-field--disabled .mdc-floating-label {
            cursor: default;
        }
        .mdc-text-field--disabled.mdc-text-field--filled .mdc-text-field__ripple {
            display: none;
        }
        .mdc-text-field--disabled .mdc-text-field__input {
            pointer-events: auto;
        }
        .mdc-text-field--end-aligned .mdc-text-field__input {
            text-align: right;
        }
        [dir="rtl"] .mdc-text-field--end-aligned .mdc-text-field__input,
        .mdc-text-field--end-aligned .mdc-text-field__input[dir="rtl"] {
            text-align: left;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__input,
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__affix,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__input,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__affix {
            direction: ltr;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__affix--prefix,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__affix--prefix {
            padding-left: 0;
            padding-right: 2px;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__affix--suffix,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__affix--suffix {
            padding-left: 12px;
            padding-right: 0;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__icon--leading,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__icon--leading {
            order: 1;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__affix--suffix,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__affix--suffix {
            order: 2;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__input,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__input {
            order: 3;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__affix--prefix,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__affix--prefix {
            order: 4;
        }
        [dir="rtl"] .mdc-text-field--ltr-text .mdc-text-field__icon--trailing,
        .mdc-text-field--ltr-text[dir="rtl"] .mdc-text-field__icon--trailing {
            order: 5;
        }
        [dir="rtl"] .mdc-text-field--ltr-text.mdc-text-field--end-aligned .mdc-text-field__input,
        .mdc-text-field--ltr-text.mdc-text-field--end-aligned[dir="rtl"] .mdc-text-field__input {
            text-align: right;
        }
        [dir="rtl"] .mdc-text-field--ltr-text.mdc-text-field--end-aligned .mdc-text-field__affix--prefix,
        .mdc-text-field--ltr-text.mdc-text-field--end-aligned[dir="rtl"] .mdc-text-field__affix--prefix {
            padding-right: 12px;
        }
        [dir="rtl"] .mdc-text-field--ltr-text.mdc-text-field--end-aligned .mdc-text-field__affix--suffix,
        .mdc-text-field--ltr-text.mdc-text-field--end-aligned[dir="rtl"] .mdc-text-field__affix--suffix {
            padding-left: 2px;
        }
        .mdc-floating-label {
            position: absolute;
            left: 0;
            -webkit-transform-origin: left top;
            transform-origin: left top;
            line-height: 1.15rem;
            text-align: left;
            text-overflow: ellipsis;
            white-space: nowrap;
            cursor: text;
            overflow: hidden;
            will-change: transform;
        }
        [dir="rtl"] .mdc-floating-label,
        .mdc-floating-label[dir="rtl"] {
            right: 0;
            left: auto;
            -webkit-transform-origin: right top;
            transform-origin: right top;
            text-align: right;
        }
        .mdc-floating-label--float-above {
            cursor: auto;
        }
        .mdc-floating-label--required:not(.mdc-floating-label--hide-required-marker)::after {
            margin-left: 1px;
            margin-right: 0px;
            content: "*";
        }
        [dir="rtl"] .mdc-floating-label--required:not(.mdc-floating-label--hide-required-marker)::after,
        .mdc-floating-label--required:not(.mdc-floating-label--hide-required-marker)[dir="rtl"]::after {
            margin-left: 0;
            margin-right: 1px;
        }
        .mdc-notched-outline {
            display: flex;
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            box-sizing: border-box;
            width: 100%;
            max-width: 100%;
            height: 100%;
            text-align: left;
            pointer-events: none;
        }
        [dir="rtl"] .mdc-notched-outline,
        .mdc-notched-outline[dir="rtl"] {
            text-align: right;
        }
        .mdc-notched-outline__leading,
        .mdc-notched-outline__notch,
        .mdc-notched-outline__trailing {
            box-sizing: border-box;
            height: 100%;
            pointer-events: none;
        }
        .mdc-notched-outline__trailing {
            flex-grow: 1;
        }
        .mdc-notched-outline__notch {
            flex: 0 0 auto;
            width: auto;
        }
        .mdc-notched-outline .mdc-floating-label {
            display: inline-block;
            position: relative;
            max-width: 100%;
        }
        .mdc-notched-outline .mdc-floating-label--float-above {
            text-overflow: clip;
        }
        .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            max-width: 133.3333333333%;
        }
        .mdc-notched-outline--notched .mdc-notched-outline__notch {
            padding-left: 0;
            padding-right: 8px;
            border-top: none;
        }
        [dir="rtl"] .mdc-notched-outline--notched .mdc-notched-outline__notch,
        .mdc-notched-outline--notched .mdc-notched-outline__notch[dir="rtl"] {
            padding-left: 8px;
            padding-right: 0;
        }
        .mdc-notched-outline--no-label .mdc-notched-outline__notch {
            display: none;
        }
        .mdc-line-ripple::before,
        .mdc-line-ripple::after {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            border-bottom-style: solid;
            content: "";
        }
        .mdc-line-ripple::before {
            z-index: 1;
        }
        .mdc-line-ripple::after {
            transform: scaleX(0);
            opacity: 0;
            z-index: 2;
        }
        .mdc-line-ripple--active::after {
            transform: scaleX(1);
            opacity: 1;
        }
        .mdc-line-ripple--deactivating::after {
            opacity: 0;
        }
        .mdc-floating-label--float-above {
            transform: translateY(-106%) scale(0.75);
        }
        .mdc-notched-outline__leading,
        .mdc-notched-outline__notch,
        .mdc-notched-outline__trailing {
            border-top: 1px solid;
            border-bottom: 1px solid;
        }
        .mdc-notched-outline__leading {
            border-left: 1px solid;
            border-right: none;
            width: 12px;
        }
        [dir="rtl"] .mdc-notched-outline__leading,
        .mdc-notched-outline__leading[dir="rtl"] {
            border-left: none;
            border-right: 1px solid;
        }
        .mdc-notched-outline__trailing {
            border-left: none;
            border-right: 1px solid;
        }
        [dir="rtl"] .mdc-notched-outline__trailing,
        .mdc-notched-outline__trailing[dir="rtl"] {
            border-left: 1px solid;
            border-right: none;
        }
        .mdc-notched-outline__notch {
            max-width: calc(100% - 12px * 2);
        }
        .mdc-line-ripple::before {
            border-bottom-width: 1px;
        }
        .mdc-line-ripple::after {
            border-bottom-width: 2px;
        }
        .mdc-text-field--filled {
            --mdc-filled-text-field-active-indicator-height: 1px;
            --mdc-filled-text-field-focus-active-indicator-height: 2px;
            --mdc-filled-text-field-container-shape: 4px;
            border-top-left-radius: var(--mdc-filled-text-field-container-shape);
            border-top-right-radius: var(--mdc-filled-text-field-container-shape);
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-text-field__input {
            caret-color: var(--mdc-filled-text-field-caret-color);
        }
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-text-field__input {
            caret-color: var(--mdc-filled-text-field-error-caret-color);
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-text-field__input {
            color: var(--mdc-filled-text-field-input-text-color);
        }
        .mdc-text-field--filled.mdc-text-field--disabled .mdc-text-field__input {
            color: var(--mdc-filled-text-field-disabled-input-text-color);
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-floating-label,
        .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-floating-label--float-above {
            color: var(--mdc-filled-text-field-label-text-color);
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label,
        .mdc-text-field--filled:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label--float-above {
            color: var(--mdc-filled-text-field-focus-label-text-color);
        }
        .mdc-text-field--filled.mdc-text-field--disabled .mdc-floating-label,
        .mdc-text-field--filled.mdc-text-field--disabled .mdc-floating-label--float-above {
            color: var(--mdc-filled-text-field-disabled-label-text-color);
        }
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-floating-label,
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-floating-label--float-above {
            color: var(--mdc-filled-text-field-error-label-text-color);
        }
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label,
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label--float-above {
            color: var(--mdc-filled-text-field-error-focus-label-text-color);
        }
        .mdc-text-field--filled .mdc-floating-label {
            font-family: var(--mdc-filled-text-field-label-text-font);
            font-size: var(--mdc-filled-text-field-label-text-size);
            font-weight: var(--mdc-filled-text-field-label-text-weight);
            letter-spacing: var(--mdc-filled-text-field-label-text-tracking);
        }
        @media all {
            .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-text-field__input::placeholder {
                color: var(--mdc-filled-text-field-input-text-placeholder-color);
            }
        }
        @media all {
            .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-text-field__input:-ms-input-placeholder {
                color: var(--mdc-filled-text-field-input-text-placeholder-color);
            }
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled) {
            background-color: var(--mdc-filled-text-field-container-color);
        }
        .mdc-text-field--filled.mdc-text-field--disabled {
            background-color: var(--mdc-filled-text-field-disabled-container-color);
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-line-ripple::before {
            border-bottom-color: var(--mdc-filled-text-field-active-indicator-color);
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-line-ripple::before {
            border-bottom-color: var(--mdc-filled-text-field-hover-active-indicator-color);
        }
        .mdc-text-field--filled:not(.mdc-text-field--disabled) .mdc-line-ripple::after {
            border-bottom-color: var(--mdc-filled-text-field-focus-active-indicator-color);
        }
        .mdc-text-field--filled.mdc-text-field--disabled .mdc-line-ripple::before {
            border-bottom-color: var(--mdc-filled-text-field-disabled-active-indicator-color);
        }
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-line-ripple::before {
            border-bottom-color: var(--mdc-filled-text-field-error-active-indicator-color);
        }
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-line-ripple::before {
            border-bottom-color: var(--mdc-filled-text-field-error-hover-active-indicator-color);
        }
        .mdc-text-field--filled.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-line-ripple::after {
            border-bottom-color: var(--mdc-filled-text-field-error-focus-active-indicator-color);
        }
        .mdc-text-field--filled .mdc-line-ripple::before {
            border-bottom-width: var(--mdc-filled-text-field-active-indicator-height);
        }
        .mdc-text-field--filled .mdc-line-ripple::after {
            border-bottom-width: var(--mdc-filled-text-field-focus-active-indicator-height);
        }
        .mdc-text-field--outlined {
            --mdc-outlined-text-field-outline-width: 1px;
            --mdc-outlined-text-field-focus-outline-width: 2px;
            --mdc-outlined-text-field-container-shape: 4px;
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-text-field__input {
            caret-color: var(--mdc-outlined-text-field-caret-color);
        }
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-text-field__input {
            caret-color: var(--mdc-outlined-text-field-error-caret-color);
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-text-field__input {
            color: var(--mdc-outlined-text-field-input-text-color);
        }
        .mdc-text-field--outlined.mdc-text-field--disabled .mdc-text-field__input {
            color: var(--mdc-outlined-text-field-disabled-input-text-color);
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-floating-label,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-floating-label--float-above {
            color: var(--mdc-outlined-text-field-label-text-color);
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label--float-above {
            color: var(--mdc-outlined-text-field-focus-label-text-color);
        }
        .mdc-text-field--outlined.mdc-text-field--disabled .mdc-floating-label,
        .mdc-text-field--outlined.mdc-text-field--disabled .mdc-floating-label--float-above {
            color: var(--mdc-outlined-text-field-disabled-label-text-color);
        }
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-floating-label,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-floating-label--float-above {
            color: var(--mdc-outlined-text-field-error-label-text-color);
        }
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-floating-label--float-above {
            color: var(--mdc-outlined-text-field-error-focus-label-text-color);
        }
        .mdc-text-field--outlined .mdc-floating-label {
            font-family: var(--mdc-outlined-text-field-label-text-font);
            font-size: var(--mdc-outlined-text-field-label-text-size);
            font-weight: var(--mdc-outlined-text-field-label-text-weight);
            letter-spacing: var(--mdc-outlined-text-field-label-text-tracking);
        }
        @media all {
            .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-text-field__input::placeholder {
                color: var(--mdc-outlined-text-field-input-text-placeholder-color);
            }
        }
        @media all {
            .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-text-field__input:-ms-input-placeholder {
                color: var(--mdc-outlined-text-field-input-text-placeholder-color);
            }
        }
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading {
            border-top-left-radius: var(--mdc-outlined-text-field-container-shape);
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: var(--mdc-outlined-text-field-container-shape);
        }
        [dir="rtl"] .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading,
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading[dir="rtl"] {
            border-top-left-radius: 0;
            border-top-right-radius: var(--mdc-outlined-text-field-container-shape);
            border-bottom-right-radius: var(--mdc-outlined-text-field-container-shape);
            border-bottom-left-radius: 0;
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__leading {
                width: max(12px, var(--mdc-outlined-text-field-container-shape));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__notch {
                max-width: calc(100% - max(12px, var(--mdc-outlined-text-field-container-shape)) * 2);
            }
        }
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__trailing {
            border-top-left-radius: 0;
            border-top-right-radius: var(--mdc-outlined-text-field-container-shape);
            border-bottom-right-radius: var(--mdc-outlined-text-field-container-shape);
            border-bottom-left-radius: 0;
        }
        [dir="rtl"] .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__trailing,
        .mdc-text-field--outlined .mdc-notched-outline .mdc-notched-outline__trailing[dir="rtl"] {
            border-top-left-radius: var(--mdc-outlined-text-field-container-shape);
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: var(--mdc-outlined-text-field-container-shape);
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined {
                padding-left: max(16px, calc(var(--mdc-outlined-text-field-container-shape) + 4px));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined {
                padding-right: max(16px, var(--mdc-outlined-text-field-container-shape));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined + .mdc-text-field-helper-line {
                padding-left: max(16px, calc(var(--mdc-outlined-text-field-container-shape) + 4px));
            }
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined + .mdc-text-field-helper-line {
                padding-right: max(16px, var(--mdc-outlined-text-field-container-shape));
            }
        }
        .mdc-text-field--outlined.mdc-text-field--with-leading-icon {
            padding-left: 0;
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined.mdc-text-field--with-leading-icon {
                padding-right: max(16px, var(--mdc-outlined-text-field-container-shape));
            }
        }
        [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-leading-icon,
        .mdc-text-field--outlined.mdc-text-field--with-leading-icon[dir="rtl"] {
            padding-right: 0;
        }
        @supports (top: max(0%)) {
            [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-leading-icon,
            .mdc-text-field--outlined.mdc-text-field--with-leading-icon[dir="rtl"] {
                padding-left: max(16px, var(--mdc-outlined-text-field-container-shape));
            }
        }
        .mdc-text-field--outlined.mdc-text-field--with-trailing-icon {
            padding-right: 0;
        }
        @supports (top: max(0%)) {
            .mdc-text-field--outlined.mdc-text-field--with-trailing-icon {
                padding-left: max(16px, calc(var(--mdc-outlined-text-field-container-shape) + 4px));
            }
        }
        [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-trailing-icon,
        .mdc-text-field--outlined.mdc-text-field--with-trailing-icon[dir="rtl"] {
            padding-left: 0;
        }
        @supports (top: max(0%)) {
            [dir="rtl"] .mdc-text-field--outlined.mdc-text-field--with-trailing-icon,
            .mdc-text-field--outlined.mdc-text-field--with-trailing-icon[dir="rtl"] {
                padding-right: max(16px, calc(var(--mdc-outlined-text-field-container-shape) + 4px));
            }
        }
        .mdc-text-field--outlined.mdc-text-field--with-leading-icon.mdc-text-field--with-trailing-icon {
            padding-left: 0;
            padding-right: 0;
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-notched-outline__leading,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-notched-outline__notch,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-notched-outline__trailing {
            border-color: var(--mdc-outlined-text-field-outline-color);
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-notched-outline .mdc-notched-outline__leading,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-notched-outline .mdc-notched-outline__notch,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-notched-outline .mdc-notched-outline__trailing {
            border-color: var(--mdc-outlined-text-field-hover-outline-color);
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline__leading,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline__notch,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline__trailing {
            border-color: var(--mdc-outlined-text-field-focus-outline-color);
        }
        .mdc-text-field--outlined.mdc-text-field--disabled .mdc-notched-outline__leading,
        .mdc-text-field--outlined.mdc-text-field--disabled .mdc-notched-outline__notch,
        .mdc-text-field--outlined.mdc-text-field--disabled .mdc-notched-outline__trailing {
            border-color: var(--mdc-outlined-text-field-disabled-outline-color);
        }
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-notched-outline__leading,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-notched-outline__notch,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled) .mdc-notched-outline__trailing {
            border-color: var(--mdc-outlined-text-field-error-outline-color);
        }
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-notched-outline .mdc-notched-outline__leading,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-notched-outline .mdc-notched-outline__notch,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled):not(.mdc-text-field--focused):hover .mdc-notched-outline .mdc-notched-outline__trailing {
            border-color: var(--mdc-outlined-text-field-error-hover-outline-color);
        }
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline__leading,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline__notch,
        .mdc-text-field--outlined.mdc-text-field--invalid:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline__trailing {
            border-color: var(--mdc-outlined-text-field-error-focus-outline-color);
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-notched-outline .mdc-notched-outline__leading,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-notched-outline .mdc-notched-outline__notch,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled) .mdc-notched-outline .mdc-notched-outline__trailing {
            border-width: var(--mdc-outlined-text-field-outline-width);
        }
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline .mdc-notched-outline__leading,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline .mdc-notched-outline__notch,
        .mdc-text-field--outlined:not(.mdc-text-field--disabled).mdc-text-field--focused .mdc-notched-outline .mdc-notched-outline__trailing {
            border-width: var(--mdc-outlined-text-field-focus-outline-width);
        }
        .mat-mdc-form-field-textarea-control {
            vertical-align: middle;
            resize: vertical;
            box-sizing: border-box;
            height: auto;
            margin: 0;
            padding: 0;
            border: none;
            overflow: auto;
        }
        .mat-mdc-form-field-input-control.mat-mdc-form-field-input-control {
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            font: inherit;
            letter-spacing: inherit;
            text-decoration: inherit;
            text-transform: inherit;
            border: none;
        }
        .mat-mdc-form-field .mat-mdc-floating-label.mdc-floating-label {
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            line-height: normal;
            pointer-events: all;
        }
        .mat-mdc-form-field:not(.mat-form-field-disabled) .mat-mdc-floating-label.mdc-floating-label {
            cursor: inherit;
        }
        .mdc-text-field--no-label:not(.mdc-text-field--textarea) .mat-mdc-form-field-input-control.mdc-text-field__input,
        .mat-mdc-text-field-wrapper .mat-mdc-form-field-input-control {
            height: auto;
        }
        .mat-mdc-text-field-wrapper .mat-mdc-form-field-input-control.mdc-text-field__input[type="color"] {
            height: 23px;
        }
        .mat-mdc-text-field-wrapper {
            height: auto;
            flex: auto;
        }
        .mat-mdc-form-field-has-icon-prefix .mat-mdc-text-field-wrapper {
            padding-left: 0;
            --mat-mdc-form-field-label-offset-x: -16px;
        }
        .mat-mdc-form-field-has-icon-suffix .mat-mdc-text-field-wrapper {
            padding-right: 0;
        }
        [dir="rtl"] .mat-mdc-text-field-wrapper {
            padding-left: 16px;
            padding-right: 16px;
        }
        [dir="rtl"] .mat-mdc-form-field-has-icon-suffix .mat-mdc-text-field-wrapper {
            padding-left: 0;
        }
        [dir="rtl"] .mat-mdc-form-field-has-icon-prefix .mat-mdc-text-field-wrapper {
            padding-right: 0;
        }
        .mat-form-field-disabled .mdc-text-field__input::placeholder {
            color: var(--mat-form-field-disabled-input-text-placeholder-color);
        }
        .mat-form-field-disabled .mdc-text-field__input::-moz-placeholder {
            color: var(--mat-form-field-disabled-input-text-placeholder-color);
        }
        .mat-form-field-disabled .mdc-text-field__input::-webkit-input-placeholder {
            color: var(--mat-form-field-disabled-input-text-placeholder-color);
        }
        .mat-form-field-disabled .mdc-text-field__input:-ms-input-placeholder {
            color: var(--mat-form-field-disabled-input-text-placeholder-color);
        }
        .mat-mdc-form-field-label-always-float .mdc-text-field__input::placeholder {
            transition-delay: 40ms;
            transition-duration: 110ms;
            opacity: 1;
        }
        .mat-mdc-text-field-wrapper .mat-mdc-form-field-infix .mat-mdc-floating-label {
            left: auto;
            right: auto;
        }
        .mat-mdc-text-field-wrapper.mdc-text-field--outlined .mdc-text-field__input {
            display: inline-block;
        }
        .mat-mdc-form-field .mat-mdc-text-field-wrapper.mdc-text-field .mdc-notched-outline__notch {
            padding-top: 0;
        }
        .mat-mdc-text-field-wrapper::before {
            content: none;
        }
        .mat-mdc-form-field-subscript-wrapper {
            box-sizing: border-box;
            width: 100%;
            position: relative;
        }
        .mat-mdc-form-field-hint-wrapper,
        .mat-mdc-form-field-error-wrapper {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            padding: 0 16px;
        }
        .mat-mdc-form-field-subscript-dynamic-size .mat-mdc-form-field-hint-wrapper,
        .mat-mdc-form-field-subscript-dynamic-size .mat-mdc-form-field-error-wrapper {
            position: static;
        }
        .mat-mdc-form-field-bottom-align::before {
            content: "";
            display: inline-block;
            height: 16px;
        }
        .mat-mdc-form-field-bottom-align.mat-mdc-form-field-subscript-dynamic-size::before {
            content: unset;
        }
        .mat-mdc-form-field-hint-end {
            order: 1;
        }
        .mat-mdc-form-field-hint-wrapper {
            display: flex;
        }
        .mat-mdc-form-field-hint-spacer {
            flex: 1 0 1em;
        }
        .mat-mdc-form-field-error {
            display: block;
        }
        .mat-mdc-form-field-focus-overlay {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            opacity: 0;
            pointer-events: none;
        }
        select.mat-mdc-form-field-input-control {
            -moz-appearance: none;
            -webkit-appearance: none;
            background-color: rgba(0, 0, 0, 0);
            display: inline-flex;
            box-sizing: border-box;
        }
        select.mat-mdc-form-field-input-control:not(:disabled) {
            cursor: pointer;
        }
        .mat-mdc-form-field-type-mat-native-select .mat-mdc-form-field-infix::after {
            content: "";
            width: 0;
            height: 0;
            border-left: 5px solid rgba(0, 0, 0, 0);
            border-right: 5px solid rgba(0, 0, 0, 0);
            border-top: 5px solid;
            position: absolute;
            right: 0;
            top: 50%;
            margin-top: -2.5px;
            pointer-events: none;
        }
        [dir="rtl"] .mat-mdc-form-field-type-mat-native-select .mat-mdc-form-field-infix::after {
            right: auto;
            left: 0;
        }
        .mat-mdc-form-field-type-mat-native-select .mat-mdc-form-field-input-control {
            padding-right: 15px;
        }
        [dir="rtl"] .mat-mdc-form-field-type-mat-native-select .mat-mdc-form-field-input-control {
            padding-right: 0;
            padding-left: 15px;
        }
        .cdk-high-contrast-active .mat-form-field-appearance-fill .mat-mdc-text-field-wrapper {
            outline: solid 1px;
        }
        .cdk-high-contrast-active .mat-form-field-appearance-fill.mat-form-field-disabled .mat-mdc-text-field-wrapper {
            outline-color: GrayText;
        }
        .cdk-high-contrast-active .mat-form-field-appearance-fill.mat-focused .mat-mdc-text-field-wrapper {
            outline: dashed 3px;
        }
        .cdk-high-contrast-active .mat-mdc-form-field.mat-focused .mdc-notched-outline {
            border: dashed 3px;
        }
        .mat-mdc-form-field-input-control[type="date"],
        .mat-mdc-form-field-input-control[type="datetime"],
        .mat-mdc-form-field-input-control[type="datetime-local"],
        .mat-mdc-form-field-input-control[type="month"],
        .mat-mdc-form-field-input-control[type="week"],
        .mat-mdc-form-field-input-control[type="time"] {
            line-height: 1;
        }
        .mat-mdc-form-field-input-control::-webkit-datetime-edit {
            line-height: 1;
            padding: 0;
            margin-bottom: -2px;
        }
        .mat-mdc-form-field {
            --mat-mdc-form-field-floating-label-scale: 0.75;
            display: inline-flex;
            flex-direction: column;
            min-width: 0;
            text-align: left;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            font-family: var(--mat-form-field-container-text-font);
            line-height: var(--mat-form-field-container-text-line-height);
            font-size: var(--mat-form-field-container-text-size);
            letter-spacing: var(--mat-form-field-container-text-tracking);
            font-weight: var(--mat-form-field-container-text-weight);
        }
        [dir="rtl"] .mat-mdc-form-field {
            text-align: right;
        }
        .mat-mdc-form-field .mdc-text-field--outlined .mdc-floating-label--float-above {
            font-size: calc(var(--mat-form-field-outlined-label-text-populated-size) * var(--mat-mdc-form-field-floating-label-scale));
        }
        .mat-mdc-form-field .mdc-text-field--outlined .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            font-size: var(--mat-form-field-outlined-label-text-populated-size);
        }
        .mat-mdc-form-field-flex {
            display: inline-flex;
            align-items: baseline;
            box-sizing: border-box;
            width: 100%;
        }
        .mat-mdc-text-field-wrapper {
            width: 100%;
        }
        .mat-mdc-form-field-icon-prefix,
        .mat-mdc-form-field-icon-suffix {
            align-self: center;
            line-height: 0;
            pointer-events: auto;
            position: relative;
            z-index: 1;
        }
        .mat-mdc-form-field-icon-prefix,
        [dir="rtl"] .mat-mdc-form-field-icon-suffix {
            padding: 0 4px 0 0;
        }
        .mat-mdc-form-field-icon-suffix,
        [dir="rtl"] .mat-mdc-form-field-icon-prefix {
            padding: 0 0 0 4px;
        }
        .mat-mdc-form-field-icon-prefix > .mat-icon,
        .mat-mdc-form-field-icon-suffix > .mat-icon {
            padding: 12px;
            box-sizing: content-box;
        }
        .mat-mdc-form-field-subscript-wrapper .mat-icon,
        .mat-mdc-form-field label .mat-icon {
            width: 1em;
            height: 1em;
            font-size: inherit;
        }
        .mat-mdc-form-field-infix {
            flex: auto;
            min-width: 0;
            width: 180px;
            position: relative;
            box-sizing: border-box;
        }
        .mat-mdc-form-field .mdc-notched-outline__notch {
            margin-left: -1px;
            -webkit-clip-path: inset(-9em -999em -9em 1px);
            clip-path: inset(-9em -999em -9em 1px);
        }
        [dir="rtl"] .mat-mdc-form-field .mdc-notched-outline__notch {
            margin-left: 0;
            margin-right: -1px;
            -webkit-clip-path: inset(-9em 1px -9em -999em);
            clip-path: inset(-9em 1px -9em -999em);
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__input {
            transition: opacity 150ms 0ms cubic-bezier(0.4, 0, 0.2, 1);
        }
        @media all {
            .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__input::placeholder {
                transition: opacity 67ms 0ms cubic-bezier(0.4, 0, 0.2, 1);
            }
        }
        @media all {
            .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__input:-ms-input-placeholder {
                transition: opacity 67ms 0ms cubic-bezier(0.4, 0, 0.2, 1);
            }
        }
        @media all {
            .mdc-text-field--no-label .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__input::placeholder,
            .mdc-text-field--focused .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__input::placeholder {
                transition-delay: 40ms;
                transition-duration: 110ms;
            }
        }
        @media all {
            .mdc-text-field--no-label .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__input:-ms-input-placeholder,
            .mdc-text-field--focused .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__input:-ms-input-placeholder {
                transition-delay: 40ms;
                transition-duration: 110ms;
            }
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field__affix {
            transition: opacity 150ms 0ms cubic-bezier(0.4, 0, 0.2, 1);
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--filled.mdc-ripple-upgraded--background-focused .mdc-text-field__ripple::before,
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--filled:not(.mdc-ripple-upgraded):focus .mdc-text-field__ripple::before {
            transition-duration: 75ms;
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--outlined .mdc-floating-label--shake {
            animation: mdc-floating-label-shake-float-above-text-field-outlined 250ms 1;
        }
        @keyframes mdc-floating-label-shake-float-above-text-field-outlined {
            0% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            33% {
                animation-timing-function: cubic-bezier(0.5, 0, 0.701732, 0.495819);
                transform: translateX(calc(4% - 0%)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            66% {
                animation-timing-function: cubic-bezier(0.302435, 0.381352, 0.55, 0.956352);
                transform: translateX(calc(-4% - 0%)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            100% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--textarea {
            transition: none;
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--textarea.mdc-text-field--filled .mdc-floating-label--shake {
            animation: mdc-floating-label-shake-float-above-textarea-filled 250ms 1;
        }
        @keyframes mdc-floating-label-shake-float-above-textarea-filled {
            0% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 10.25px)) scale(0.75);
            }
            33% {
                animation-timing-function: cubic-bezier(0.5, 0, 0.701732, 0.495819);
                transform: translateX(calc(4% - 0%)) translateY(calc(0% - 10.25px)) scale(0.75);
            }
            66% {
                animation-timing-function: cubic-bezier(0.302435, 0.381352, 0.55, 0.956352);
                transform: translateX(calc(-4% - 0%)) translateY(calc(0% - 10.25px)) scale(0.75);
            }
            100% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 10.25px)) scale(0.75);
            }
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--textarea.mdc-text-field--outlined .mdc-floating-label--shake {
            animation: mdc-floating-label-shake-float-above-textarea-outlined 250ms 1;
        }
        @keyframes mdc-floating-label-shake-float-above-textarea-outlined {
            0% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 24.75px)) scale(0.75);
            }
            33% {
                animation-timing-function: cubic-bezier(0.5, 0, 0.701732, 0.495819);
                transform: translateX(calc(4% - 0%)) translateY(calc(0% - 24.75px)) scale(0.75);
            }
            66% {
                animation-timing-function: cubic-bezier(0.302435, 0.381352, 0.55, 0.956352);
                transform: translateX(calc(-4% - 0%)) translateY(calc(0% - 24.75px)) scale(0.75);
            }
            100% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 24.75px)) scale(0.75);
            }
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label--shake {
            animation: mdc-floating-label-shake-float-above-text-field-outlined-leading-icon 250ms 1;
        }
        @keyframes mdc-floating-label-shake-float-above-text-field-outlined-leading-icon {
            0% {
                transform: translateX(calc(0% - 32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            33% {
                animation-timing-function: cubic-bezier(0.5, 0, 0.701732, 0.495819);
                transform: translateX(calc(4% - 32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            66% {
                animation-timing-function: cubic-bezier(0.302435, 0.381352, 0.55, 0.956352);
                transform: translateX(calc(-4% - 32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            100% {
                transform: translateX(calc(0% - 32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
        }
        [dir="rtl"] .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--with-leading-icon.mdc-text-field--outlined .mdc-floating-label--shake,
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-text-field--with-leading-icon.mdc-text-field--outlined[dir="rtl"] .mdc-floating-label--shake {
            animation: mdc-floating-label-shake-float-above-text-field-outlined-leading-icon 250ms 1;
        }
        @keyframes mdc-floating-label-shake-float-above-text-field-outlined-leading-icon-rtl {
            0% {
                transform: translateX(calc(0% - -32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            33% {
                animation-timing-function: cubic-bezier(0.5, 0, 0.701732, 0.495819);
                transform: translateX(calc(4% - -32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            66% {
                animation-timing-function: cubic-bezier(0.302435, 0.381352, 0.55, 0.956352);
                transform: translateX(calc(-4% - -32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
            100% {
                transform: translateX(calc(0% - -32px)) translateY(calc(0% - 34.75px)) scale(0.75);
            }
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-floating-label {
            transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1), color 150ms cubic-bezier(0.4, 0, 0.2, 1);
        }
        .mdc-floating-label--shake {
            animation: mdc-floating-label-shake-float-above-standard 250ms 1;
        }
        @keyframes mdc-floating-label-shake-float-above-standard {
            0% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 106%)) scale(0.75);
            }
            33% {
                animation-timing-function: cubic-bezier(0.5, 0, 0.701732, 0.495819);
                transform: translateX(calc(4% - 0%)) translateY(calc(0% - 106%)) scale(0.75);
            }
            66% {
                animation-timing-function: cubic-bezier(0.302435, 0.381352, 0.55, 0.956352);
                transform: translateX(calc(-4% - 0%)) translateY(calc(0% - 106%)) scale(0.75);
            }
            100% {
                transform: translateX(calc(0% - 0%)) translateY(calc(0% - 106%)) scale(0.75);
            }
        }
        .mat-mdc-form-field:not(.mat-form-field-no-animations) .mdc-line-ripple::after {
            transition: transform 180ms cubic-bezier(0.4, 0, 0.2, 1), opacity 180ms cubic-bezier(0.4, 0, 0.2, 1);
        }
        .mdc-notched-outline .mdc-floating-label {
            max-width: calc(100% + 1px);
        }
        .mdc-notched-outline--upgraded .mdc-floating-label--float-above {
            max-width: calc(133.3333333333% + 1px);
        }
    </style>
    <style>
        .country-selector {
            opacity: 1 !important;
            bottom: 8px !important;
            left: -14px !important;
        }
        input:not(.country-search) {
            bottom: 3px !important;
            left: 10px !important;
        }
        .mat-mdc-menu-panel.mat-mdc-menu-panel {
            max-height: 400px !important;
            height: auto !important;
        }
        .label-wrapper {
            font-size: 14px !important;
        }
    </style>
    <style>
        input[_ngcontent-serverApp-c4150375230]:not(.country-search) {
            border: none;
            background: none;
            outline: none;
            font: inherit;
            width: 100%;
            box-sizing: border-box;
            padding: 0 6px 0 90px;
            position: relative;
            z-index: 0;
            margin-top: 0 !important;
            margin-bottom: 0 !important;
            margin-right: 0;
            margin-left: 0;
        }
        input.country-search[_ngcontent-serverApp-c4150375230] {
            width: 100%;
            height: 34px;
            border: none;
            border-bottom: 1px solid #ddd;
            font-size: 14px;
            padding: 20px 20px 24px;
        }
        .icon-wrapper[_ngcontent-serverApp-c4150375230] {
            padding-right: 24px;
        }
        .flag[_ngcontent-serverApp-c4150375230] {
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAABw4CAMAAAAs8X1iAAADAFBMVEUAAAD////OESYAJH0AAAAAK3/80RbtKTn/ywDRETPQFCvYJx7/AAAAejkAI5UANpV0rt0AZTTvKy3BAQPCJy1Bid3eKg8AnUkAlETgGx3VAQD93QXtHCQAeF8AN6YBhU0AakLeChcAAIzKLzwAKWb91AActTr94wAAo17209YAbcsCrsnHDR/kABLoES0AcsEiRY1LktoAak4DJ4s6ecn38/ABHqERSK8DaacBod3GDDDYFDsAgAASrSsDlsAHiTBpuuYCKKYAmzpKrdYAP4j9zyT1s7YAZgAAV6VmzP8ormbeFh4EUZkkSaX630X0rgG6CC6eLzoPPHytHSX33xYZigABPppKl88BPxw6gj3WZHTe5/DZECNbl7H6uBEAlWcBS6joKDoAAGYaRJHlAjb1xsrZJBEAXbsCf/3gUmEjnkLNDCwANrKdqsrVJyftHSjjPDIvmzAMBwTnbnKMGj3mqK/B0eT8OjKyIjQhXDNAWInzlpX950kNXq8ei0M+bU3////c29r/cgDsho3idQnsAAD9TRQ+XrivDgv2gwFLqFZeteMWFpbgACXuxQmakKswVpmnp6X+xh3PSFB/rY8AUbrgTwf+mjIjXEI7eCgAmQDasjM/mQEiG1ASiAeozbbGvcOLJyqNqdbPf4ejyuxRfMB/msPelZ0NrS00sjBtg7hLaaPxK0IZIG0/PnEwkAngUCWwigqxxNpTSgfCHhp/teHG0rzk7uYOdCjy3roAUPBNL2rSuhy1uAglMXTHujMxLRHFpV59CRBvjTp4YmiUsjy1oyHqkBdguI3CdyFtaZFHlHNUWlWLgyqOh4ZyhnMpd2XseCNnXDOmUzh7awhLiKQzDw2yNFDvQ02RqGZagyxpoKt6tB5PaIWRxZrFQBxDTEB5flJsOE9SCxp6MBimuIf24p3sf0nltl5Ds5PYw4DPrJzAfVf22XudPyMSOTn///8CNwPAGTilUGUCi0b////MECjvKiwAMoze8+gABhCSwecEfcgAOJTnESwetTri0eK2AAABAHRSTlMA/////////////////////////////////////////////////////////////////v////3///7///////////7//f/////+///////////+//////7//////////f/////////////9/vz//////////v3//f///////////vz///7////////////////+//7/////////////////+////v/9/v7///////////7//v///v////7//////f///////v///////v////7//////v/+///+//v//////////v/////+//3//v/////U/9f7pY6kfJrKwsC2SaOqKyxZfwAAjiFJREFUeJzsvQ1cVNe97z0bBgRiG6MkPY3JPecTYsy0NiGtu/Ym57WN0z5pDBGbkyvTcNpL6DnkdIgZyphAm2QufKIxdhyETxIfkjIRHW+GlPLmTPhkVEDlRWEIiESivImgAtLTa3uf89zzfO591tovM/tlrb3XShyc0P1Thz17/7/r//uvtfaW2TOzt8kUcxXkM/bMRChLWpqFW8jck1OQn4iRCW7kEQHI5NdggcQ8EeEAITwPFw+ARBGBADo8SQpYIipOSyuOPotG14Xn9niiQBpGkYjMudkrs35yIOn06a1bR0owlqwIS+dvA+qXFZ1pZ/ILcvbAovmlTGnBdadPv316NCkKCOF5fLfmKZCk2pz5+fnrBbURQAyPDJyAiEBOz/x8T09PjpjCJIZLpgaHRIAz85U9GZW1ESBRW0kuS9AC/rpIgUT/5Etnz740GRkI3QxNI/uczn0jTYRAUu11T93x43We62IR2kB9uL3dxe77lHW1t4frCYCS+dnZPb66Ot+e2dn5EhJL4enp2VnuX0WYqAagURA/PRp5qg8oFI/AUtAX9NgqA3SPrUpA99iamHjvvZTA229jLamOrUl1nqTstyeOJkWyaB9bk0r8Z79ZMnqqxPf2X5UkCYD2sTXz42zPKPhXEjmQ6RxbR0vqTo2+fVtJnfg/hM6x9eipUX/JaP3vR0f7j0ZrwOvomVMlTU32Jqf91IkzRwmAE/MnRnfUeTx1nlMnLl/UB949c+ZEid3+d39n31PfP7/5t7rAxfcmj2b/4O8ef/xL/5h9dHL+I13goxOj9W9/s9vpOv3NV97uPXNRFzh66tSpV9721NVt+viU/9RovS6QXT9aUn80Me/WxLrRU/X12boASksCWApaLtGXCGQAsQcYfb1qANTAqwSSAWYCGUBsgEWRPOcqmf5vmQzg8wHrWuiAlkqgjBZiID0DEC3plYTAzpaMVS0ZoH2YggTISE9fxeZb2VUt6RkkQEtlWcu6Yqu1eFVLWWULSYbKQwWgAlBFwaFKkgzplZU5LZb8DEtLTmVlOkmGgrL0VfmHii2r0styiIpuGU8vKC4rO1SQPt5CBpQB7y3poJZaMmBVektlUSX4CyDSkbZa84uL89Mp5lJxRkb6uvjaH5Y+MGUm1JQArBsmix9eJwAJWxzyLeDp8DD3Q7Z2S4IIJHTINzW3mZubzW3N8rUdCVEgQVKGw2F2hMxDQ+aQAy5HC0iQAtIyGtvMoaHGxqGQua1RWoAMkJYx7B5qTHa7kxuH3NFmQAFyQCzD0TxsHkoOJQOFkpvNw82OSAEKQCxjuNPdyMUDotHdORwtQAlEymh2Jwtyi/3EFaAEQBlAbc19naGNgkKdfc1tcO2WBBSQcPFBjC4moIGEEnR8SQIOePhXqPhfPYwFEl5EAS8m4AFUGRcTtAB1GSUJ2kCaRf6LrSVNB0ioZKXxbGWCHpBQJAWKEvSBhOJofHECCRAtQ14AFoiUoSgAD4hlFKk24AC+DGUBWgAsQ1WAFgDKUBegCSQUqQuIAjGWcb4VffqUChgJ94dnA+BhGmlJ/V5WgGFaW+FUDEiK1novCwBX3rp2hZUA2udbAZA70zojAXTOt063zlxjmLfeYiM16LyXFWausK2tbNZMWFKDlkaZa1lZrTMzjPi2i+5bU0HQPjvDtBK/+TUd4CSWoA9kjx0AGssmBjgiGk9yJjG7qkry1ms8ntyMQ2Bx9FUyLV2gBfzt6Gj56g93drRwz/SA4Z0tHQ5zx1e3mB0dLS0OfWBLx/BwR4ejxTG8c2p4aqc+0DE1bHZ8FfzS3TJsHp6a0gemOsDvr/BXWPijgwDY2RH9hRt4I6oh+ht6B0ENU1PRVx/AHkHRHR3D/G/0juEpEkstHSBsii+go2MLSYYfbulwTJmH4biRZNi585YOR0eHeWqneaplCwEAjcDJYe7YuXMKTirdGr761Vsg4JjacgvoZP0MfJopx5SscT0ApFDHa+5ALcM71Su1ALl5EgClpQXEVt+glAHEBvhPlDKA2ABZlDKA2ACLItTpArQM4PMC2wZyC6mAhtzkrgbCU15QhYUJhbnJDaRAWkNubkNaWm5XISGQ0JAMW78lOZcUKNy7FzaeO0AINIBoCHQpekoAzCp19gnnzENtsvVYoDG5kfvtODRkJgMcoRAk2pJDhIDZMRQKtTlCjW1kwBCIG24MDYUUbyjgAEdjcqixsXOos1H51gcuQxtoPDTU5xgaaiYDIMO5b+tsJgVEd52UgEJLCFhPrJsIHLY5DxygAJz8ifXDh28nA2yRk/3OFQ8RALfL3oE48BAOeGiFsPYh+XscB3DA7ayw6bAccGItHWZsh2ECVg4wt+OA9QdAc05lOMM8hAUQwdoZ1q9AEaxkNJTA7TYEABAscAAZzzC0gA0LODEZVuAAXApsBum8k+gwLYAfBzAvEKacWgBEDhxYcdjJRnKx67WBCCjGr6AEpLNbExAsOR8iBVYccNoOHF4hX7lYB7IVFPoPA4gfwEYhJwRQeyXb2mpBHgYxADszOzvbiiAwANs6W91VPdtKDsy0d2V1XUGkwANZAJghr4HWEixidoaiaK5bUfE8QD1wcTj5DIAEiLX+K43e/cUWcgAEr0vPOUQGCMHwsyX6gCSYCbA6gCSYDczNL1xyaQDSltkACAY6g7MkDQaa46KBAihAEQw1L8SHVUUjgvl4LsVcMBiUAOhgWMA8sLQAEa/LxZq0gzkgyIRh+MIlbwACmsEw3sWyXtBJC5dqeUt6wLXZIBwyAMwxkaI1oNZZ7tMUQWApGAU0oNa91dXgsOnqOeNi5AAOCoIcV6qrq10sAkBC16qvzFZ3Vl9pn8XPVhnEXgHB7eD/vBl0BgTEzly5Uj3HYCyhIZZlSQBETTHYRRUQ/WFmUbSMWAYQV4CVWAKAOyKpZQCxBKgHLg4nnwGQADHW9nzGvvseqPzly/O5hd17zm3nlxAy7SkQEQHgwhkfFgABAsIBQvhuaczu3TLgHhGBACL8nt1Op2SFKT+i0uXLS6PPsMByjLCWCAB50TJLVpQlQZskRduZ/O0Fe2DR+8HSuT27VdH33GNndwiAEC52qxTZ7fdHWJ+NT2ESw6MDxyMcYLNFALFykxgunRoQ4Zb8fnXRuN6QaXd0LEkBZIbdqN7BW9p9T13dPfpIFNjhBNpBAdzjZBgnhaXdTfB43oTypNofRE8si3SkmN7RRf+mTepx0gKwwlkiEz2wKLqdWDLgIUrgoQOUwP5DlMChAgJPEeCh/YcqU2vtK0iBhw4cKkhNfSTHq0dILD1U+8hhCktA9hz9eBmwwksJ3K5bsRIgkQFQAFXEEoBkYgnAeWJ9VoDaEnXRcThw1MAPpAqldKWmpqWk9P8AJQOIJ8AjlQhMelDigRS5eACp0GcEGuTalpqa2oARD6QSq/AzAtSWYt9L1AMXh5PPAEiAGOtvKWUAsQGeRemDstIC5IZnn0UCBWzZI49gCBTwCFvGYUUfEAIFzCMcxpTFCqC2RF20drfeTykDiA2wFPRlXk2M/8t53+pE/3/e+eWoRGAT+NP0ZQyBAr785R0MY9uEJtCAhisMgHeFAfCusADOFR7AuMIDGFdaANKVJoBypQmgXOkAald6gMqVHqBypQ8oXBEAclcEgNwVESB1RQZIXJEBElekQMQVMSC6IgZEV5NSYFEkv4bBRpmQ1zcwgM8KhEKUwNAQOTDUuHdjtcNRvXFv4xAR0Gge2tvncPTtHTI3EgF729o2buT/7SWrodFcDYFqkIAM2OtohECjYy9pLzUPQWCombhbh5pDbW2h5iFiQLgyWiMxUN3Y2dbW2RiiGWnYrVRTgxqgmUvxugPFFbBXJgTwTSr98YsCvN3b+zY58PbmDzhtRjJq4OIHEV0kATZ/INFmfeDiBzKpcyiAtz9QSFWHAtisBFSmFIAy/oMPtIFX1IDSkxzoVQO9NxagtkRdNH23Ug8c9dSgn3z005t+B+IYql1UW/EJCIp+E7btiYgua3xeywAMwAAMwAC0gJXEMoC4B7ZvpwQGBiiB3FwKYHvXwEBy8sBAl8oWLkPDXu6DOw24DF9R6Tl3crL7OfV6LPAVmACxGgtccD/3nPsCDRB5ILWEkQF8kQGSe/xE9Seab6FAGUCsgG4qjZlM/0SlqwYQI+AVKvWbTD+j0hkDiBdAssOyxajbGchUoDwIWMoy6AAgq1YaBFBcQJPBUqa+ip8GwGo2jsqg303qGnS6CdVLFrpe0u4kJWApQtzpQgNgdeNVllgdR6iitbvpcwO6I/d5i7YU6cy9zz1wfBq6qaGzR3zO6U2/A9HuogTd9PkPM3QHMuJDJVumO4eAMqwMdx0f3oluh6aVCdemMI3xC1Yd7/x1HYLdJtN9uwJ8ARq+ODcgZOy+/wOA++7rDmr6Et0Edt0nAPfdp+Ur4gYGioDcV1qR1cJa+CGPurlPBkh9FYjXRcrPkLpRAhFfnIPgCHzCSt2oAdEXjExPmIwsj0lC5EDEFxN8MQJE3CCB++6rEeKmhZ/d8s1qYEwx2R/TA2oUwC4SYJrvrukgCQAsjSSkw9CRhIQAmaXJhJYooFM0360jgiWXXrdGB04q/MBJp4YQqzU1KCcf7fSm3IEod1HagwD1YUbTjdrXZzhUmkx/qdBzet9eMwADiBvglQdwMgADuNmA/FO28v9Cl8lkAAZgAAZgAKRAjIW7zyvuzqbY+7xiAdx9XvEA5j6vWAB3n1csgNvA68XNEb1IBHziCgaDmzsqwON1lCWr0tIn4HWMq6PjOhhzEUjE3Oc1CtSu2rlZCmDu8yoC3trrHTvfm6uIALj7vIpAxcTo6OnR3lMRAHefVxEIT/b1nn3jjf4IoNNLwS2Dg4MTvb0dXkLAtXNwcvBS84mdmwkB74mK/sFLe7wn5smAMyNj/Se83j39u0ZOEAGb9zXvvBQOD65q3reZCEjb/MrEC1v37WsT43WBtCcH973wwr7BLeJzXQDM2MHBF6PPCIC0BOkTEkCmRQCWgn5EKdMvaQF4pd9rT1ABnFpJKdlZ7NaZX26kAQTqGiwreYAU4KmZK2/hgBMVLhSSU8CiOwN+LM8x0V8RlIVbCwoKKotZVGdEPsc33Bv2ilRZBpyXGUWozpDfTaitN1wbZFjWUnzoEPjfiGXYQ4rO+JHq9kMdHW2b9wSY4kPwzQKmrEhmFZRl6huU34BqeN0UvEPqe2cqXGzRoQzV1a+5ydc51OyIEFum4H1hp+C/zZ9kBJEAlLtRoDqmhtMdwJnZMQ7vWyvtDBnAKdQ41AbaXxiHt58dXzCLN7oFneFlUQBUcqinZUtDYWHDli3DZoekOkAFUMCPctMKGxrGx7eMFyYo75dldrQhgOTchoGGhkLw0CjtDEG6u6hbQZHt07AzHDSAQDW3UQGcjtAfZhZFt5DKAOILoL6n5VpSGUBMgSOkEoEvkcoA4gvYQiwDiCugmFgCgPzlGCkDiCXQRCwByCSWAcQVYPouoUw3FXj/fSrgb46sXXvkb8iBC6vhbxarLxACzx0Rfxk58hwJAJpfc7w+O7v++BplEhTwfmjt2s6j/GUfjnauXRt6QRuA7jujV4rolFeiAt53A+NrjkaBo2vACvf7GOC/88WOjI0Famrso9keQBzni//vKOCCm++a0brs7MwSv72mChD1/DohiRSI9mW2YCfTX+PLvFfWw3iAP5mfdN45igWEioHqMzkEVpDoqcFaihY9mQkTcMYyMyc1ipZ0K+igTJAn21Oi2a2KgUvyZGbrDRzQC8LUSEoCfUowNb4rTr577yWcfN+ln97fpd6Bvku/i36X+iCA0SICsdXv//brVDKZzd+kQrgziXdTAmbzf/kOJWA2E5cSPZP4TVqAsBTZucrfE5SiOLnZXP2EjpRnQx19lIDZPNxJCZjNbcdiC1Baoi2aslspB452alBOPsrpTbkD0e6ilAcB6sPMomgjofYO0AG5r71GA+wdeI0KAM3rA09cu8YvXOOa1wOeeGKmdWYGMteuXH5NH/jltZlWNsiyQdeM1Vp2WRd44onWVpYDWLZUMKRjaYYLZmcvWy8TAU+A6NaZa6X5xaWlpUQ1wAzWy69dLi1iL+tleIJ7hDUES8tKrcHiAR0A9ucTG60WFspiCeZrZwDdCeOsELicX/aaRBiA5e4CDhV8beA1XeCJVnbmrctlVms+GIHXXtMHrgEjXLdctrDw8bIeMAOdw6ZLrWzp5TKLHnDNEmSDFt7M5dJ81mrVAK5d++VM0FLKllrzhXYv5zPsZQ0AdKjlchEr6coyaz4+wzULm28deG2gVOYb30vXWkv5breyVmV/ooArV8TVVrZYP8NeyZBaWX1LubL1A5j4CLAXG4EGcvUj5YD0ghmlTMm995YwpajLcfw/JgP4ogOlEeXzQH4pQmVRQHq2kAOQskaBEomy7703uwSpcBS4l0inPgdAbYm6aOpujcOpYQAxAFYQywC+2ICTWAJQQSwBQLxoxyh+Aeqiqbs1DqeGAcQEiLGslZZDtTkZxDJl5BfnHKIBanMslZU0gOWQxVpMA1SWHarMiWmGsgxLDlUNwE9BAVWG/JxiqnGwHDqUw9IAOTmWMqpeshYfsuTUxhKgtkRdNHW3cgM3sotY/NSoogC4yTdGAXDTm8YSl4HGEreL0gDcQYAG4A4zNMCiiODiA4I+K/AosTBAnr/JFmRtTf48MsDHusaCY2NsVZD1EQB5zpqqwIirKhAYCYwFnHl6QB4I3RUMTI+MjDDBqpqALU8HcLKBMWZ6ZHp65PjvahjXCOPUBnxjVS52Ojzyu+npnr7JikCNq8anBeSx7BjDzs8H52o/feH49TATGKlh8zQAf1U/Ozd/5cr0745PjkxfujRf6wrU+DWAJnbaNXe5unrWVTs3Hb506dIc6KsmDcBWNc0wlrm5OVfwypX2uWkXw4CO0gDYALzyChsM1rqutV5j4Uftx6pYLWAMXgyGZb0u17Un2iHABgJagG2sBgaxQfbAgWt8hqoqLUugaPj1B9Z5wOk8EAALTNDFaBXtr6mpCYKO+fTs2bPPfDpdE2TGpgNa3QoGrqrm+MjI8RcuXHjh0wvvTIJh0By4R31ghjKs1+saGwt67SwDnmlODTj5aqpYp6vC6/K6nIFAYExn8j2aZxtzBcZcToZxBsemq1y60xvsQAzYDapqxqYrqoKM/g4E62CDzK5gVYBxEe2ij/IHAZbiIKChz3ogowZirOUS4T73J425YYDFQgmcO0cH5C9fnk8BFBVtX758e1EZMVDKPy0lBFjGwj+1MKw+YLUw2/OFCCZ/O2Ox6gDnwI+7LNvhs+2Wu8DjOR2gdLlCpTpAvhLI1wbYu5TAXax2BouCuMui163sduma7SzBOBRFVxSRjXQkx3bCqbF9+7myoqKyc9tJAW66SiYrEWC1xhoopQXyafdpmWTAUtBfU0oEnvodFfAO/GzhkacwMU9JN3AA/ACf+501a9DEb44c+Y0ceGft2gvvXHjqyNojSOB3R478Tg6AyAt//ZunfuNeiy5EZcntfuedd37z1FPvrH0HU4USWAs9HXlqTQR454jbfURBvyO1BLoolAU4wdIF0MBTf/0bmcEja96JAO/Ajyw2gnihaPCc3/jUhQti//zmgjsKwBRrFtasFbs10l2/W7MmkuWp30QtcQO3ZnVk4NxrLwhLF8QFedFcAxLDUSDSfMTaX6Mmn3oEn4q2gAIiRf8GsRE5vYVufQe1Db0/oAZOE9AQCaDeH7Sl3h90pN4fbrQlueiBRdK/6On29uQBGqBqY9ctCWrgPCb8mWO5DYrfo/kNx5AEdKP6xZvflDyCc4MAzp8/P5K8ETw+jXKDAJ6u5i931450A9SQq7TUDuOr0G4SbunaWKUEngbx1Rg3A8kjt/+LEhhJPrYx+Wm0m2PPgFVKoLrqX24/VoV2w61TALfDRoSxU7lBAbi+4dxoAig3GgDaDRbAucEBWDdoQMMNslu13CAAbTcqQM+NCtBzowL03KgAPTdoQMMNCtB0gwC03agAPTcKIEmpi6mcLqo2yIDeXirgaH1nZ/1RCqA+lJISqqex1JuSEvFEBPT19vZRAdncXzJA9YqkjAfKVBsMgAJ4QalBHhhUbTAACiBFqQEeGFBtMAAK4C/wOin7SsmALqCIv0UPUMbrAap4HUAdrw0g4jUBVLwWgIzXANDxeAATjwVw8TgAG48B8PFoQCMeCWjFowDNeASgHa8GdOJVgF68EtCNVwD68XKAIF4GkMRLAaJ4CUAWHwUI4yMAabwIEMdHrqfEa7mGTCgtMaAoH6iIAuDeYyr9swGKuM+/c+8lWLjFIh0gX/mbZ/4XD6AuOg4HbrEA6h1IpaUHvC6TfO7dL5MBGIABGIABkAIxFu6+A4kYYe87gAVw9x2QyOORAZj7Doiy++tYts4fyYi974Cw3cc4E222RCfjEwHc1c75izzZE5v8SdnZSf4mkCpTF0hiWE+SeK0klklSW7LKLdX5RSPQnL9OLBp53wHQaKY9OykKJGXbM5MggLnvQGJito3xS3vXz9iyAYC77wA04cuUApk+aBB73wFYQl6SFEjKqxNqwMjD2OQrbIxHG7A55ZacNm0g0ZPoUT/VAjJtLulTly1TB/CwNk+k7CSPjdXLkJhUlx0Zal92HT81tABgivF74OTzgFHje0AbSLQ3gUGpqwP7V5OwR+gAcKKxfj9rj3SwLpCYmcn/IwYUogcWRf9JJkZDiwxs2EAJ+P1UwIYNTiefgxBogs+baCz5GMZHVQP8ZiQV4OP+xtfAxRLQOMeFPuVlAAZw04C7ZUqQCXmrTQNYQkBhISUwMEADFBYW5uYW3lJIDKR1wa1daRSWANFFVUMuEA2QBkoeSIuzgTOALwBwB7FuCJCSQgq8+Sb3o7ycFHj9dXH1mwTAm2++uXbtm0KStVkEQBb8KH2W4AqbQmoJAhru1UDK2rVi/5DUADzdcYfg6I61rxMAUr2O7VkkgDeEBjRLRwFv0mbQlAEYwM0GkollAAZws4FbiWUAFEACsQyAAojD23obgAEYgAF8wQHV14o09e9xCdTRiebucVDOuARyqFRL858uVKUBxAigHrjYz6XYA9S7aOyPGrEHYq5fHP0WlUxpaRdpgbSHP6IE0tJeJPclfkLqkxIPmaIfqSpjiWaf5DNYGcWUQFpagYUYyBCQIl1fAlBWIGa5btcWBFIzMiz5GWKSF7VHhQO4assipWjOFt5SvjSeCOA+p0ZsKe2RIlA2RdESEXcr9cB9lqlBOfkopzflDkS7i1IeBKgPM7EX+YkZTv9uAPEK9D5KAWSCv8nHwAMWkgMleydvvXV09Na80DEcocjQ2c//PDlJaElf/y75dYmdkRw32Gvog8h/SIBfbpTEtCa3YoAiOtWaCumUI81wuQj3RJoBW8OMbtEz0hBZl8kA6nGQPsvs7OV+5p2cJJwacC7B2Ro6Rpbh1rw8fraCn2QAFJitGlqMXdRE86uucDz+TICHEsh25mXrhkuATCfD1lFl8NucdJY8XBG6SRS9lKebRQZ4mmyM065dujyDh2FsOl0lB/JYm4vGUlKdJ8lPlYFASwiYJJYA/IhYiwbEYbcuBSDGuvXjl6lk+od/IEGOH5cAJMjWjSLBAQTIMZEQAF1ka3W1AtBAjp8ErZ/cuEsObNBATm7cePKk4EkE/LYdsiy7dkmJXdUbASMDDvKAgJzcGAmIJqk+vkteQ7QWLr76uIzgVm1VAj/nf2wExLGtL2+UFg7WHH9ZmWGH7SD8UQLq2wpjonVsrT65NdKtP98QBfwcUH0SbN66MQJsPSmGc4DTpqwC+j0OTIDQ48dPnjx5PGoNAvv3C+1HgBMQOLnxBGpcojX83LlBRlSfQA4lols5lZREOpkMkEiOEAByJAJsIkQic4n1k2URgU3OHZrxEYSoBilCBUBkcQ5+yBcKSP1bvACtV8SXN0QAOzsQeXFEAIDwy9HXjDiAZcWfs13ls5KXWzhgtmt2hmXZmcvl5eWz0g04oLVcVJf8JSwSmGllr0SAK4w2AMxHgzlZdIDLivjyAVYTkLffNTML/GkDrVLgsqo3EEVLUlxRv95FAOyVAS74CiIcDcBX0rNXpCu0gYoKFwiKdqY3rAO4piER5acrdADQ5HSECFaEK/QsMex0RTjsCoKlQM10xXSA0QNAu2GABNlp8COg2ITdgbzT4XBNBdE4RNIEUWvj5jCjAl79N2LxgJ/sjDcT+dphdmYTJZCdTZgkChAmkQBkSWQASRI5QJBECegmUQF6SRCAdhIUoJkEDWgkwQD4JFgAlwQPHP3FD1HCAh+1IONxAKZ5LIBrHgPgm0cDGs2jAM3mEYB28ypAr3kloNu8HCBoXgaQNC8ByJqPAoTNiwD5LYah/pcBxAvQQSeT6U4q7TSAGAFvUqkFcUVmTb1uAPECxFzcRaa487uWhAQLt8BfqQrK6bPbuAWbz+4Sfj8RrksVBYRrVvlcjM0Pcb+TZbl3e+qabIytzpQkXvmKB8SrYvEnlT3+uuykzNHMpDy/X1hlEi2w+yEQDQcB2T5ggbXX7Rutq6lhmCYAZ/ulF51KSJBcB0usAVjfNRbYV+VzMtzLRuyHHn1OFtYw+mG/c1fVvtOZo9lCDThAqME/um8r0Ol9p0uEGnCW+BrYsapdIH7frgDL1ZDn54oWroMFihYuagVW8jVUVI2N7dq6q8rli9QghovdKiJCDUmZp7fu6t/64WheksfO1SCGRweOX8PX4PO/sjVz9MPR0a2nhfc1TWK4dGpAxAdGC4wDW2P31Pf3s0EX21SXB2tgnH4fP01Yn0/49dbut/O/GoNtYC7NzgVtfA1wLnFvPIIuZrn5lAmsO2HybL/TJs6l2Vlum8fuYl1gLgGf/DTJ9vsFn3U+4f1LsC1vtH5+vv5UZJuJv34PsFdn4zxkJnmc3Hz2wPKB2n/2ZsOqWSEok6/BKcx5v7KGJlDDbHlPT/mssga7zYao4SDcFp6bnZs9wdUAtslrEK3LasiuC++pAwvKGrK5GlxiDazPk83XYK/r7eVegDgPZnI1NOnUwDBzxWBvFceIrwF0sY3bb0ENtia+hiabSxijuTlfkrwG3l5epAa/T3hHtY6rr7cXNOSL1PAqUFNdJqgBLNj8nmyPEyzAGjLt/LaSTD8LFsAYZWf6FuHIB2Q7/PeEEgDQxbQAwxygBRgbCSIFAKJfihwgKEUJMKyOLxWg5wsBaCNIQKuLlYD4Oh1bigJgi/R8KYDiQokvZBdLAbZoW+EPt22T3PQT4UueoeiHPyyUXOmaHfsbleSAFQDR+Jr71PEKoKiULRAzBHYhwpWARfjHMMFuZLgS0DKvBSDN4wEX2jwOwJrHAGMabhBAzfs64XIgqGleBbA65pWArnk5ENA3LwdMpoclggU9jJJJIgMwAAzAkCjOgTjsVgP4ggIx1V+hlJxsNjMMcpMBRNS1LT29oJgYeHtzetp776Wlb36bENicPpGWkZ42kb4ZBagvl9KVlpZWeP1UKvjRhbiYinrVtsEX09JGT32S9uLgNgSgPo+clp6eluq/7T34U70VA6TteeklDKA+hhZASxfffglYKkAcYdWr4Fe6WsY/nQA/EF+LwnVrevpmTLfeiIGjnhpxOb1vLrAYulct/lRKMfLb0Abw5wOoDyXwE0D9YewHmFBARXiu1ksDdO8JV/TTAFVF4apdNMCJM2fOhBHrsQAbZF2o9Ryg7rwE/tSAXd3fmQawxAB4KvdVHGC6DSXUTBJfNxnAZwMuvpj6SCXiO94YYOLF1NQXyw6V5ajuoIcGJuAdvTlL+UoCDYD2J7ga2MiHdTWBi6mpm/miC8Qzi9oAn+C2nIKcR8iKBhW8BH6wj6SmFih/SbwxALUl6qKpu5V+4KinBqzjxYdpJh8nVbABUALriGUAFAD5pzVuBJAbCuXSAF09QF0UQDkEyomBxqyunvH29uosXA4lUB0ab1zwHprbW00GZM33jPctzBXP9cxnkQHtPeNnLvbOzfW0EwLz1T0LC5vHx6sJM6RcAUDPeM+Z6itkNaQcO9ZzZn5+vOdYJyEQcqe4q6vBQ4gQ0FX87kBLAVhNLAP4HEDn4ORIKDQ52BciA9aEQo197ZNDjUcIgdUbQ6vdzUPu1e6NZMBGd+eFvvn20JGQikAD1Z19x6v6+/s/7TxGBqwO9fXvb1pxvv84aQ3uyekKb40vPOnGAD9R6GRJeKRqZCRcclK5BQdkXj999uPT1zOJgdHNox97wAMpMBnyn5mcPJMXmiQDHvecnO4DOnbS8wOyDD/xPH6yvnfyB56fPE4GPP74Tx6vr3/8Jz9QJhCBu4hlAOTAQxQygDgCfk8hDqD5coIBxA7opRAHIF91YmQAcQTE460QDIAEsBJLAFBzQHlKUDozEEArawmyiAv04QD2ykzrzAzi6/t4YBaIAghemQ0GZ6+ov5mOA1qvtAJdUReBBWAJACEGGJhgBnFtVgGoUMvrnfMiVgsA5cHbAOIFiLE6HOSOOFfrdtIR8CzIFC2wjsYWf56FwpZ4YobYVuRMDqmt6KkfQluSc0Ut0nd90MdWeByQnj7KEaMsldjrM8vPRmUIh5bKQ0wZESDYYtfl57TkoK8ZjQHYsnWYFDhLxZZDN75o2m7d8tKXSBQBLhGFR4CWCcJ4ASC0EwFI7QgAuR0eoLDDATR2OGApKHVh8AXSbzNw32h4FogG4gAaKAKQQjKABIoCK5dtf3b787qQCDww8NZbK996660HHtDJJAANDW81bB94dtlbAwM69nhg2cBbD4CHhmXPNry1XbsmHmjY2/DsyrcG9m5/duXevcpukEMi8Oyz2/e+NQBqHnhLDUghHuCiGt4a4Ba3owkB4oG39i579oFlewdA7MDelVjgkeunDoqWloGmG5YtAywmAwy+HUiwtHfgAeAIlL5941sPYIOjACRg48vU8dJgCbAStP7AwAPb9+7drhEsASAyAPp1+wNawTIAVD7QoBOsAPSDVYBesAwgCY4ApMEcQBPMAYuidcQyAAqA+q3azwUYnxPQBYzPCWgAcbgDLQUgxvo7Sn1RgL+8+/ENGx6/+y9JARDN63EygIsvGcUQagDEl2za0D+yYVMJilC/hQ9bHt0wMrKBy6F+I1+5oh+GbeofHe3fBJf6dQHPhv6TIyPH+kfA48n+DR5dALRbctIzsmHDiOckKGITCbDBA5oGiTwbSAAP6FHPSH//SP+IZ7SEwFL/hk2cHc7WJoKiYbeOjkx6PJMjHmS3qi5H9xGw79ng8WwYBYV8pL5anWrNj+th/3tg8xvqf0wA/PDHH8Hegb31kToeBfzwhy9+lLlhQ+ZHLyIvoIdaqaV4BBZF5LeAualASSYFUHLrrZludya3QAL0uXtvvXUS3sfH3UcETIaEe/fkhZQ3hLoBRedN9spuDZTXO5mnDbhD8jZDbm3g1hJFz8if34AaEMMlXaUCet2qO2tNuns1gDwUkKcB6OlGAKOKWzrljWoDme6QfKRDbs1eAlNVUfWkbMJiashTLWgDkyGh53sJpzfs+bxJMEvVo4IG8qAVt1tYIAA4KWetLoDWIgHS8+xNws1e7Lgz9/9mAF9cQHqnTuFqi0mZuFt51pko7xns+XMFqLs1DqeGAZAAr0oUAV7FSLjxQGwFL46azX3FFl6kk1vIhpdMxV4Pj7+eanYU4MJZOxbgAyDCAUJ49r144F4REa/fynL5sADuYqlYAPfpKWH7nojIgDc+Ge/pGR8HD5+8QWTpjdraxqyurKyu8gwRgEUrr98aLfqNyoEGoIGMjDMRIFsM4rtVfCYANQFnMCenBkgEom2KA8evEYGxmjEAnB5zioAYLp0aEBEAZ8ALMrCBoEsEcP0tADlAGRnwMUbA3ze5WGjpiZmmvycDDgdZFhT9WEC8NKMe8PeHXS7YrZFLOeoCf7/CnpNjXxF5qg8AZIXkCQkg05IAloK4C08nQlnS0izcAndh6kSMhGNrZhQQLn2NBRLFq2HzgHilbFw8ABJFBAJ64QCQHMjS0iQHMmG7Z4cgjwikYSRsX/+0c/2DUOtJAafLZd/veyYKyCxZVZbWPxhgWYYNRgHYkcKxFRQtXOg6WuP6B+0uKAkgXhub79Y8BbL+wfX2Z56RWhLDIwMnIBHgm998+6Xb3v5OBBDDJVODQ4Tt+xl2rrh1jmX2S2rQku/pZ660T1955ns+QsDj8/WHq/pXrNhECOR5vcVe11y7N480Q8X6+f7++bN2sd90gGzv/B773Jyvae5MEhFgn6uY9zud/rkz82EioG6+tiJce+LMmeu1p4iAxFMlmXXh8GhmyWgiGaBSPAJxqBWUumHA4cOUwIEDVMBhG7zwM02GwwyD8YQBDjidGE83r1uJgQOKWg8rS1ECTpu0Ow/bbE5twAn6X/ocjIeCUGVg5BmU8Z+/Bl3dNODAAdz8vkGzlX5/WOFU9r8eQL1PY0UPLJKipxUO//Mny5Z98s+HUacc/mQAX3jgcES388DthxH6jyjwzxJxAFIPRYFPJPrpsmU//QSpcBRYRqTrnwOgtkRdNHW3xuHUMIAYAF+WSR4kn68GYAAGYAAGQAqYTPeTyCSRAcQHcJJEUkB98X+EDCDmQDuJpMB6EhlA/AExFXdDN5s/SXoimbtRRloxI12X5HeVpaYV5JseSVMjakAMZxiTq0yNKIFoOGMzwSdKRA5Iw/1JJn6FHJEC8nDhzLoSiQLK8MipeDkiAupwybl7KSIC6nAAMCgJAEqmYpSKIFCE3IR9Pw6neASoi6bu1ujAwZuq5hekPVLmEgFhTWqZSzJw6nDp1FAjJnW4fPIpEZM6XDm95YhJHa7egaSISR2O2kWjiEkdjj4IiIhJHY47zPDIYhz6ZJ/4KSwUlxpUC5z+pxxo2LuX317YlTzArxpI7irEAwm5ucLCwF4hrDC5C5EhXdT4uLiQPq5YAIoA6VuPEfzqcPKVKHD33brIyZfvlgF3370rpB1+txLQQDpf5rargLvvPo5Cjm0VtiIABBIJxwAAccO43K5c+CO0VbJFCmx9WbLhZDIY7wEwxqFd0pZkwK7kk9JNZxIKwZBP3n03FjgOzEo2jUM/XRc1gQa3ZNPDaSA+QQt4OdT3qdRSYW5Drqalu++WVM0VnVCoWbREZN0aDacbOMqpQTn5KKc35Q5Eu4siDgLpDaoFyUEgelzKzVUdl/ZqHpf27hUWupI1j0sRRQ6VhWmFioUIgLpE2vblQNsR/+fy+syAxYIGousVwF13oYHldyEBq3XlXVarGrBa79ourpcB52DQOTUgXS/PULryrtJSNVBaetfKUlQGfA13oWuQ3thDDkTXf95xIAa2q7Sck3q9ACwnVvwC1EXHfhzEp9RTg3LyUU9v+h2IdhelPgh8hsNMVDd64NwvoL4Sj5IArF59gRZYTZgkChAmkQJESWQASRIFoJ9ECegmUQM6SRCAmOTDD4kBPsngIDnAJVlYoABSusoHf/jDwcFBlS1chqxb4Le9W/YRW1qdAgFE2VhgDQQQY4IFuu7Izb2jSz0mWCBLeFAmwQIRKZLoA4okJIAsCREgTUIIRJOQApEk5ICQhALgk1ABMAkdAJIIgOqYW/YAJ/U1MW8i8PwA0PM0GQZ+9KMGKksrf/SjlUsbKFOqgAcKVBsE4IGInhcXOEC58oEHVMDAwEpeDWAchMWBAS3gRwjdWKCB0hJh0dTdGodT46YAtLso/UEg5sclHSCBWAYQV0AXsQQAdX6rsRG1VgNobqYA3KFQyGwGD25CoHOYv5T3sOrMH86SuxnGN6sS4GtwQ0AdjwcazQ6HGdFPWKB5KDl5CNFPAtCpUl/kQS4B+CtiGUBcATGW6pCuoxsEWHMqK3OspAAIrt28ZcsntZWVlUTA3Isvzm3u6PgE/ERcwh4B5N95Zz4E4E8yoKMjf/Pw8CfwJwlgOTQ1VQyAzcVTU4fU1+1XAWxt7cWOjHVbtqzL6LhYW6u6Tr46g3dLy5YtGUBbwIL6XXCEpTu/cWfLnUAtYIHAEgDuvHPnnR0d4OFOQmBqaudOs3nnzqkpMmCVw7yzw2zu2Gl2rCIBard0ADW3wccttfoAm5OT80lL+qVL6S2fgEV1tyKvePfjHy8s/Bhx+T94MbxvoHTnOBByyzfQwDe+gYu/YcCdlMCd442xBn42/jMcEGcHvy8KYLFa87H31FAB7KGMVKgCzJFYCeTz4RyCyqIErKkSZSAIBWBJlQl1rJcDlXIgVX2wlwNWRXzqI+ojgAzIUQKpqq6SAxkqQPWqSQ6o4lNzMICwWd0pxcoW5ID6OHpIG1DPBlU3yAGVY/YRbUDlqUzVCwqgUj5QFnW3KQC5KYt6XFSAdE7nqwpAAamPlPFIfgEiHAUAZRTkFKBaxwJail8gDl/tGgAJ4CWWANiIJQDfItafMxAklgAEiCUA+4hlAHEFyPdb+Qmif5LJAAzAAAzAAEgB4T/ho9mS/5FlT5Lqkf+xhzolMaE+yZPJ5CQUsDokaVP65Fudq+tVQFJffaizXmg2++RRd+TJtyYnJ1cfPVmvAOpXuzv7klcf5UJ6V4eiT77lXj0Z6lw9qbQ02Xuyr35SaLRP+qS3t69zcjJJXcOkGMHzkie9k9mIorM7O5MiIdmdJ5MkdEhdNCyjPhryrV7pk2+JuQXgfxFLAFQ3f3W0mZvNbQ71nWLlgOMPfITD3Nzc3NjcPGRu0wT+8Ic/fP8PMLyteahxtdsdcg+1tTm0AUD84Q/NbY1u4VMsQ0NaGRwcApgj0c+9DA058AAs4vtAR9xRwN3c7NAAuATAv0SNWr3E5fjDH2SAe0grAwAcbUOy274cadbOYAZ96pZZahvWAcxtQ1IgpGMJqHlImmKI62qHFmBulowDmCB/4IUHHG1RonGo2SGNR2dwwLnh5vwPDTv4rnZoAcCUo3Fo6MgQmK58YCQB9pbYjuFmR7NDOVU1ADgTkfdZFoCX3yGVADz4AiXwte/QAl/7mBb4GmEZUYCwjChAWIYEICtDChCVIQO+VkP80lIAbo+eEWydAWpVnwOWA197I0qwriDq/tAK4GsHIkDwua90EwBfs4lAoMYbpAIYl4uhsATsz3kZRApc0aBkl4t16QHRbmW4K+EG9SwVW3UlA65/VV9SYJQgXgr86hZKIJ0kXgKQFCAFiAqQAGQFSACyAqIAYQFR4FecnAzz9K90dLMAn+6JQQWwQj2tFdObGnByAvuXjVvYr3ulBkUz+3WLpgae5gQOMfu5hRVNWC1aLymA9brXz/y8U0Mf4NOBXsJdzldpCV8kpmhqAD9QmgNHos8KEF2DRGN6x6CXqN8diH0vUQMx1o2/70BS4lEZIAbI7zsguaZ+b2+vHIi0Gb3vQKY0InHitzJA574DELh0SYLo3UYgMTPxt20T7yZmZhMCdmfJuxMT7/rsdaSWTm2qH63/1g4xge59BxK//e363t6j3/6ypFvFIL5bFUjJaKa/t9f/7eahegGIBogDJ7vvwGhvIsjwbuIp870CEG0vOjUk9x3o7a9/5dN9k7495nejNWgo+0TbxPl3LpyvGzU7iID6/ole/6cX/Jn1DiIgu/fExAl//6Q/afQEmaWJ3sHB3j3nfSWnTtxLBgA1v/1xTbA2TJbht2BavHvvN7NLMnvJblSQ+N5CMxB4qdzxWzLg3eZGMMT3DjVOJJIBE0NnYNN9lwYJAVAFfDz6rvh8idx34Gm55P+F2OQbHzKAeAdYGsAVYJjJIBMIkgI1rtaZmelgIEAKTLPszExrwDVNCIBAtvXaTA1LCgSnWdfs7Ow0+EloKRDwsi7XNHkNwcm5ADM9MskSj0Ng2hWYnvZSjjQbb3PJAD4nQKl0SpnSPzpKoY8AcJTmVptHv0jAxYuJNMBvrz+94vpvKYDrK57+3veukwMf+Z6+evpp30VS4LfhN7539ezTb4TVptDARyveeOOZZ954Y4U6BRq4+EbY5wuvuP4GOXB4xfn+qnCYFPhoxxu29Z4HH6z7iBBIOvGg52ujZ0cQ/YoBjh47drYqdEydADtwF9dfPXkMMQz4uXQxhIzXmHwfIeO/WDuQBnDxIwpdTDf9D0p9hmNlzJWQoPXJBbUMIKbAli2UwNQUDbBlyxaHYwufhAh4knube/hJCkuAGOaXvqIjAXAA0QBbhtetG95CAayLPJBaisoAbg6wCJqouqpnQ6I/mkwOR3+wpvt+cmDdlHm4nWWcY48RAnBym5urr7UywTF9c3/kD98dDsdQ8sZrrSyrZ04AEqAv+CXxX87omPtj5D8I6Iv7ovsTmub+KPkfhfPFScOcFIj44oQx90f5/1kRX8k4c39U/icX9SWaY2TmVIDcl9qcGlD6UphDASpfUnP/50YAlJYwRXfjiqbsVsqBU0+Nbs2pIZt8jO7ko5zesh1IbUQFUO6ilAcB8TCjZUQGUB/IqA+VJtMdxBIOx0sX2EYF3FLKFFEB+QxzjgYoYvKZUkLgFvhgZUpZUqCoaNu2O/JZxnLOyrL5BfoA/NpqcRlr2cZaWFBJkS5QwFisTEF+UamFYUoZxnKLHnCHBRRs3VZQsG2bBfi6Qx/YxjLA/LmCfEuZauzQ3XpLUWnptqLSIgtTRgbccUepxcJaS0GiW8iAMsbCsqX5llKmgAzIZ0Hr24oZVjU9kMAt8FoObMEd26wMoaVbisqKuMh84qIFqefrF22fvqlAjLU9n7Hvvgcqf/nyfG5h955z2/klhEx7CkREALhwxocFQICAcIAQvlsas3u3DLhHRCCACL9nt9MpWWHKj6h0+fLS6DMsgLv8MtYSASAvWmbJirIkaJOkaDuTv71gDyx6P1g6t2e3Kvqee+zsDgEQwsVulSK7/f4I67PxKUxieHTgeIQDbLYIIFZuEsOlUwMi3JLfry4a1xsy7Y6OJSmAzLAb1Tt4S7vvqau7Rx+JAjvgZ/l2UAD3OBnGSWFpdxN8j6UJ5Um1P4ieWBbpSDG9o4v+TZvU46QFYIWzRCZ6YCnoa5QygNgAjFI27BNOKuDA1yRBtui3A7CA8yHps4ecuoAY+RBmA64GAYinGrCKx6nxZwksBZHfU0O4s8ZNAiqXFxSAB0LgrhW1v/jk9OmS3XvO30UE7Pl4/Ucfffzx4OD3zu4hAlbefvsvLp49e7SksnIZEbDc56sEXu6q9PkQG1EHgbLtVvbcOda6vQh1EECsK1peCm+xUbqcFLBairbn528vsqCuMYYEmHPLLZbl5xhSoOiuUsv27ZbSu2JXA1sELRWxpJYsLF8Dq772FgB2I5RjhZasOahtSGD3dVj0deQmNKChP09gEfRtKh01gCUE/IMPyA/+HSQFGCZY1T8Gfp2jAFzTwSAF4A0HrlXsCaIB9cXcMpg9p+YvXz/lzVdv+59IgG0P9cz3VLeqLrWIA45/OnK87/jI1hFCIHWyf7I/PDnZf4IU0JABfHGBmCuDUgYQGwDxO5SmDCA2wCuUMoDYAIsi+dh/R6YUmQzAAAzAAAyAFEB8ZhAjAzCAmw0wxDIAA7jZQIyl/EytwsSr/00hAzAAAzCAzwm8qpAKWCQxRPrT5wAOEOk/osDPiXQwroEb0K2s+nNF2oBvg5rAAyzr3HHgoOp6rFhg/4YNP+f+7mDJAHZD00GGse3Yr7g8Id7S/g2waf8G4hr2cxU/9HNSIGLJF6uiqbsVim7goGinBkoSIA53IGqA+jBD3UvUQEyFu3oeTgYQ1cPkQA74l54TqIU/5mvnc3SByrmCQy7XbLASwt5KVSq1pVpvbU5lLXsILM6p2kcB4z2wbTaYnpbhKiABOM23t89V5rgySIEcJjDHBmcD6YRABst654uDrjnENiQw13oItJ2QgNqGBmbmEGY0gLSc4CE6IK3SVdtDBaSluxCDpglM01lKq8QkiMs9blH0FrEMIK6AVmIJAOJ/WLvfb0esxgL+JputyU8O2JucdruzSZ0DB/ht0JJNneKGAdSWqIvW69ZpYgnAb4hlAHEFxFjwwtN53JXALAkJFm6BvzA1lNNn51/a2Hx2lzD/+GtV50WBPH6Nz8XY/BD3O1nWng0W6ppsjK3OJLQHEA4QwjP5y495/HXZSZmjmUl5fr+wyiRaYPdDIBoOArLhK0XWXrdvtK6mhmGaAJztl16IOiFBciFqsQZgfddYYF+VD35vB7wUxL5F43OysIbRD/udu6r2nc4czRZqwAFCDf7RfVuBTu87XSLUgLPE18COVe0C8ft2BViuhjw/V7RwbWxQtHAda7CSr6Giamxs19ZdVS5fpAYxXOxWERFqSMo8vXVX/9YPR/OSPHauBjE8OnD8Gr4Gn/+VrZmjH46Obj3tEWoQw6VTAyI+MFpgHNgau6e+v58NutimujxYA+P0+/hpwvp8TeJhzM6/+AbbwFyanQva+BrgXKpL4qcJy82nTGDdCZNn+502cS7NznLbPHYX6wJzCfjkp0m23y/4rPPVJYk15I3Wz8/Xn4psM/HfKwX26mych8wkj5Obzx5YPlD7z95sWDUrBGXyNTiFOe9X1tAEapgt7+kpn1XWYLfZEDUchNvCc7Nzsye4GsA2eQ2idVkN2XXhPXVgQVlDNleDS6yB9Xmy+Rrsdb29fujQeTCTq6FJpwaGmSsGe6s4RnwNoItt3H4LarA18TU02VzCGM3N+ZLkNfD28iI1+H3+bKEYWF9vL2jIF6kBnlRtqssENYAFm9+T7XGCBVhDpp3fVpLpZ8ECGKPsTN/iHPxi/96uAcQE0L/dkuK2S8pfo4LiLZ2CyvtNYYBuEXC6ukkA8N8TO8YxrrHubn0gWMWMna6CQGCMBHCe7u4+XQVdBbsJgJqq02NAp7vHWJDn9Onu0zWaADs2VsMJXkM8GAgEXIGgNhBwuRgNCUAFsQQAdbc7tAwgfoDnKGUAZMAuKj1n2jVWNcapSvJHWAGOGtIV4CgytssUpBTuLLwV9U1O7jiAXJufX1xsyScHLGXWMvAX9e1PNFBaCoHSUmLAai0qKytCl4GuwVoEhC4b00vwm8HoLfTfHSgqLeP+AJVJ/iieRlYUmW45V1BQIP3HPxQUFCFXFJrupJQBEAGfTbdGtIkpKywsYzbdipIBfOGBTRHt4IEdm1CKAtLDAgcgZYsCZRIVApWhFQUKibTtcwDUlqiLpu7WOJwaBhAD4C9kks8fxfcmDcAADMAADABK/gFaBPDAAw8UWJQftH31VVa9yvrTBx74qcn0CCAeKFO+RGJ/qVjBWApgIACEBdmLsGutrddaZ2akDZTBqEfK/gR/bbB+8ADnK7p55omNT2zcKMkK3XAhf+J/zyhT+LI0NGzbVqBw8wE0IQCMwpd1oAv8Ubjhnvwp8puMzJd127aGbSsVbhSAzJeloWtg4JzCjQqQ+gIJ4G9NMjdqQOLrUE9PA6twgwJAk4Kvsp/mK90ggagvi8oNBhC75RGVGywglKpyow/8lBCgtCT2jTgErDYgHSmSblWMlN7AqZtU+5IA6nmDakQyvdXpURvkOxBqpJA7ENoN0hd/EMC5Qfj6k3iYQbtR+/oTd+TTcKP09VPsoRIh4VAZc60Bcl/4LoEuuGEsB5AgfDgAhtwkiBjuHjKZHfpINNxhhuewdRBpuNnMn/TWQOThIoBFlOFRAImow6WACkGFywEZgg5XAhIEHa4GIJIihqeowm8AQGmJsmjKbqUcOMqpQTn5KKc37Q5EvYuShEsR+sPMUtCWYdWU1xR/pxw6gA4R3pEnRyJv4ZMikvf8yRDlTZToABJE9TGEAvQ7UhEhPregjYhAJSkiAIfYIsIsPJBWzJRRAQkZGYV0luiL/qzduvntL+nIRBcuA0jCJQBZeAQgDRcA8nAOoAkHAF04ABZF2nNBNi+WNJBfXIzaLzAAm/Pwk0AP56jeQEYD1icjUr5sQQKSeBWBAlhp/JMPs7pAjgx4MkcXeFgOPKwH5D+pUL4OUKwEim80QG2Jumj6bqUeOOqpAYhIGQ8TTT766Q1FtwPhtYQAkt+rhN99vihAG3JRA5gIg4dh+MtfeILMkmvCPFxZOWyecJFZMvd6HauKilY5vL0YYI9S4c2rDh1atTms2iAA+UoVr1pVDP+pNgjAKqU6Vq0qK+N+KIQDHDtX5eSs2ukgBswdq9LTV3WYSYGd5iloacq8M1YAtSX6ovW6lXrgxpQKn4FT40xYtUEAvq/UK9/fWVS08/uTqg0YoP/732+urGzmFoiAl1QLOgBeSwZ4gEr/e0kDz9MAzz+/7bWBhm0oCAmsXDnQBTWwciURsLKhKzfXnZub1dXVoMqBAJ6HzTcMDDQMuLMGGgiAhoHcroaurNyugW1ZWQ1KQg0A/7nbQPC2hoGuhlxVCjWwDb47uOa1hoaGgbUNuaoq1EDDQFZDCigBCJBd+hmeBwAMXsshoGt1a2jIGlg5MLB2be7AwPOgcl1LcBSeB/7Xrh1YSVQ0163Pb8tdk7VyZZbKEQoAKbq6tj0P+jdXPQzIqdGwTZwaAwOqjejJB8aAm3yq9rHTe9vKlQ2vqWceFuAg9Oov1EHgBgKmfyDXv3NHYwMgAX5NrifxQGUGJVBRSw4UgH8ZAe+Pf/3rRypJgH91gbBKrzf117+u9ZIAOa5Dv/7XCq8XeHK5KvWByoqAF4RDVbhcFet0gQxvwOt1ubwB8A9IP8M6rwtm4MJdrjl9oGDO652bg43Dx7kMPSCjeC4wV1xc7PXyD4f0gENeWIPwANx5c7SBR1yW/IDLYrFwDy7wkK9Xg6aejNcdaCkAsZb63QFtIYDxhvFC7kd5+TgRkDBeDn7tAb8qIcLRQMJ4V5cbAKSW+CTjhegtN6LohIS0woYGXApcDbm5XVnERZd3uTkhy0YAhQNudzKQO7cc4QoBNORy8ZBApNAGyhEAay0rSMvIOZTPWoqLKtMqi4pZtnXmlxs3/nKmNbLKwuYfyslIKyizsiaXcPqLdYrnDW3iKsZlU210mR7Nq/M12f2eRx991HNwf5OvLg8u+ZuauFVw4/6D3Ea/ndto4iNAsI+LePTROr+wCgT767hVHr+Pg+AqeM7OZm+yCZntwlUDbE3CKgasYoVV3LU2TCCnDa72A0NgEwvyguw22IzfAzaCVU5gyg+bsoGNJi6T+iEvD73+UZPYkg205DsL08CW/HV1fi4zaPysD2S2iZlNKq9sxD4ohFXVBizlnfX5xN7YH+mNSO/tF3vP5zsLVnE10IgeWAQdD+2lkcm0detJWmDrp8coga1byX0JALmvCEDqKwoQ+hKBXSMju7ZuHdM9893KA7tcTGAXvMbK1gARUMUwY1tHglyqkaA+sIthAuBxl1CKpi8OcDEs50ssfpeGLwjwCURLOr4gMAIrkFjS9CUBtsqABcR/DVAFaEuD6Zh4DhCKjlr6FNe8CAgpdum4iQKygcO7kQCSqaHZfBSAkw8CgzrhEgDq4/X6OiUBzhLES4DTJOFRgMSNFCByEwUI3YgAsRsRUN8YQK4LycmNDvmbLlrhzx1LDjUr36UhbJ4AkDevDyia1wO45uG7X5F3wByNWgDfvGuizSUCzaFkPCC6rwA7o9h88t4GLCC6bwPHJ5dDaL6rMAEDRDqn1wUPaK4JvvmEBAwg6xxXBXDDN48BFH1fEW0eDSj7vs0caR4FqIZW2jwCUA+ttHkVoNe8EuCaD5vNExOY5uWA0LzLGw46MM3LANH9Hobx4pqXABH3YTCwbC+m+SgQ6ZyJcJhhK7wT6OajgLRzvHuwzUcBad/3t2GbjwKyrsc3jwY0mkcBms0jAO3mVYBe81EgxvoJpeIRIL/ilHDdqecppQBWnju3kgJYZoWXXLEuIwWWWRimqIhhLFqEFIAf3Fm2DDxYyYCV1oKiouefLyoqsGrUIQHOMaX8QilzjhBgLPAnqIQMWBkFyCyBosuet1qfLyMtGnRrEWgdPJB2Kxw42KnEA/c89dQgEf30jv0eF3tgUaT5WlKmzw+4UFGSlUrA63CpPmDHuhxeHMC2hSfCiiSu/on+NhYDnGgLt+1xyHKwjj2glQo04HVUwD/yDHCVd9iLAqChExO93GJxZUZGJf/txN6KE1FTMqBiAhqCv3daUx/mlAp3wGBbBXDqRVli23hD1ocjsvKmJsS6lN1aAQ0FU6NAKsuZwnUrr9qHJSqWb0MCF4XYX1y8+OLDlQTA0Se5+Ivwipy/yCABPorEJyV9QgIkHf3o4lE+Puk6ARBOkiisDXDdymZH47PhOE7gulUcuD1RYA/DDVwbcuAq2sSpsUfIkQ3j8VODbeufCPdOwEXXqaNAYWHyhTGTD0xvb69qeu9xVPS62pDTG+xAE4gdCBiaiOyjql20n24XhQcBRimtgwC8ZiZCkpWf/0BGCMRYf7iN8gsTZvN/oQXMVEn4X+0pkgivBciTRF5tkCaJvjwhTCJ9xUSURPYSi2ByvGoA1ADJV82lr1w7tF9XCq8uI/FT60jiIwBZ81GAsHkRIG5eAMib5wCa5iFA1XzCYr1oN31bosy1Mm1LlcgAPiewMXlj8trVpMDGK9Xt7e3H3Mf2kgGr2yNaQ5aher7yCvzYY3X1aiKgvX3+iWvw05jV7dUkAHA0f6W0gAPa15BkAHGXqy9XV4OfISJLa9qry7u63D3l1e1kNVTzwABpDWurw+2gj5K7usBYEGaIiCzD6ghBOA5r165efQzYWU08lyAS2rgmzvaHPwMgtvo6pUxfv3r1Kh3QffIYZYaRk5QZaIGrI92oDbjSIDCCWN8NgKuoljhA3Vb31WMpuSdRBOwlNXAVxAOdRLhC13D1KoxPyUXUAYAqdQ3dPJCCBlQDBwsOwfhjoO4qfeDqsWOACcH47q+HlK2hLIFA7g+I71b1OapoGAseuq9+feTrqh5EdisIBeFwRLtVVWMGDlIwF7KXkFMDdlX3MeRIXx1R9pxQCXr2fYb9gX4Hot5FaQ8ClFqcgx/179EGQALcSywskH3K5zuVTQ6UcJ8xtJWQAiXiS1sVgQayI7dttildoYFT0VfPdUSA5L7WvtgA1Jaoi6buVvqBA67gZ0MppgZW8bsDLQXgLmIZAAWAOj3XjTzJjgfGvvLYGOJmIljA+dhX7v/KY2oCC3Q/dvX+x7pdpEDw6mNAVx9Tf8wVA7jGHrv/K1+5/7H7uwmBAKjg/se+AgClKTQwdnXsMZAA/FV1FCZDN0gA4ruvPqb0hASCV7sfu/+xx+DnrLqDJHcVC9Z0d9fABM6x7m4niSX4+XuQgPtYb4AEYGDd3fc/pl6NvfRKjQu0HazBAnG4Ay0FIMb6vyh18wCnkxJgGDrggNd7gAqw9fbaqICg2RykAWxhszmMToEGmDazuQ1dNhI4UAHf96hAlo0EbNznSSeQnpCAy2xuhp8sJQVs/ea25OQ2cz8qBQpgHQ53crLbMcySAQcqHCH41bKQA1U2ArBNNPJfRmtElY0AXEPJgoYQZasB20hyRCPqFGqAdUcBt7psFXBgY7JEG1Vlq4Bre/fu3SgILF7Tt6QjemARlESlf/9cwFF8HBI4OkEF1B+9dzL7qG6SCDDZ1zc4ONjX10sIHO3r7OwbBA+ThEB2X9/QECD6+kiBzurO6qGNIA0hkDTZCYLBv3pSIPv3Dqjf68R/zpGOFUD+ogzqFwZgADcRiLmyKPVZgNs4Df43TllZC+UL72HUM96FAHqyejDxC1ldKGBhgQMQ1MLCe0hAaI2L6OlZWADGIzQe6Om6lFUOi2zsWmjsyWpcWNABurre43ulq6tr4dJ7lxobdYCs8vcaG0H7WY2N5V2NjY1depaA7S7Yfhbf8nuNepZgnZc4oKenvBywOhkWuhrfW5CNsJ4lUGS5DNCz1DiU1UWZIRLaRQR0XQL1wnEDT2F8OUG3guHm50R51oJ+Br6v+LiFrJ5yXYCbT0J8F8igNdIL4txc4GfcAif89KbfgTR20Z6uz3QQoATiUJLT4BeVVxpG6TMC/Gn9U0TfEuFCnZnw0ac+oYQ4xXTvvZn+OqbJXwcyFOvrkElo2ZZJU4OTYaCnixkE4gBbXZOPooZ7s7P5fiIGIuNQSSD6gTsVVfgQgejvP626vqmOTCS+ZTVQAyyl6IvOoRR9DdQZqGug7iXqGqiBRdG3iWUAsQT2E0sAEJM42NuLersMD4T3VOyhA070hhGr8UDFiRMViNV4wOuQfOWGAGAnzOY2mnf8TsAzuifIgRP8Z+vVBAaoaOudmGgDD6q6MQAb7p1om+jtDeMu8l+D0NgYaq0A/AWxDCB2QC6dDCBegE+pdNxk+isqbV0CwERcAhNUGjSZ0qnUYADxAiyj0v82gHgB6C6vUUrzZWio/CUA/L9xCRyg0n+YTP+ZSv+fAcQLEHNtppQBxAlQSyn6s3BxCFAXHftxMICYAEtB36DUnydgptTNAhzhihO9DuQmNBCGd3ls63cgGDQAr3/q6GWY2AHUlqiL1tAiAM9R6s8TWBSRn0IwgKUHvPceJTAxQQO8d2lw4qXBS+okOGDzSxODANlMYemlwcGXKCylvwjPY75IAbz30ubNLyH66Ys10gaw+ICJ6FOxJpkMwABuGkDy2t4ADCA+gFhKecHvNQr9q0JfHCCFFmgcaqQA3EMpRwYHwQ9SYGhwaA0AhgYbSYG2HqgzbcTAYMOlS++910OawT042AA/dA4ANxnQNjg5GGoED0NkwJrmif6JvsGjACCsYbCtr7dvsKSPopeGGicnhxrJgaFLjWcmQ5caSYGUNY2DQCFuRpEAQEcGm91009stzldSIP72uBsJLAVVU8pUPd5CoXEAtKyiUMuNA1owq3HAeE/PODnQIgKITShgfBz8BcD4qp4eEqClp6elp7qlpboHLhEA4z3VPavgwXgVWFAWggGqx+eBxqs7CYHO6vET8ydGyQFA9PbMT1Z3dpIVDYnJycm+TsRYILsV9k7f8eN9oI/IgHHQs51AECQCWuCggZ4C1ZMCLfw4IISdS3QAVwhyst7IHUgDoD4I0B5mFkVflkn+6638kz8GYAAGYAAGQArIQ7ZryAAMIL4AzbvYGIAB3GSATrfSqTcK/CNJeE95FNhEEl4uAepIwiNAXl6ef1OefrgIbNq0w+87eBDn6tFIePkVDsjb5Nvf1GT3e5BENPzyDMPwwI79Thtrs/vz1F0VDb9igePKA/4m+C1Jp2+HOlzSOMMD/7ipzu/jgf3+g548VPiV6LcuuQybDtptDMM6/XVSS9mDQuOt0Ynm9PGWDvqbnE7n/oMeVXjXbKRxtskP0gvd6vH7gJ86ZXi0cdt+IbswcJvydvjrPPLwaONOXzS1ODX+8daD/ygLvyI23uSXjU50Lnkk4WLjNvtB5chEgbxIuNC4TT0sMgAoczDSuNOPmVgSIPOM0DjbdBA/cU2S8AHYeKT/tIGjZ7jGpf2nBRw90zPL8gOpKwDUz7e36hmRAPXzswRGJEAFrv9wwFLQHVnc7WSyfnanRG/CVW9K1/xMCLvDdOedAiLbDleoWgDhd95pgk95RJrEvXatW9k8DBcAAVkTTfL62rWvR5tfEwmPAAKSJbEQobMk4RKAR1KEsJ+tXfuzaPPRcBnAI4KTlJSIN2m4AuCQFG57eTn3NEURrgI4BMTe8eabILBcFQ4AbosCeZ0fuNdV4aAdE3dXppSsrNfL33xTHIg7+JsJ3RENK389KysF3uhprUl+wyGAvv76m/y4rn3zzUhYVDrA62iACyt/8w6MpZ9BSyJqijonLJq6W1XhNANHOTUwky8FM/kopzflDkS5i/Lhq7UPAqsjBwHqw8yi6D6ZkmVCfhTGAAzAAAzAAHSBFGItceCJJyiBmRlKgGXpgCcYBuVJAC6oFWCYAGK1ALyvVpBhgojVamAXL+7GI2PCE23AxSjk0gbef39MHj+mYwkmkVz1K7jrfX3g/fcjd00J6BfNSzjbwKo2YIBdYoZdGOC4QtMiMK3cIgA/VcgC3BQVAV8W5RY0cI5h8s+BH/kMc44IKGVKFQs6gLVAXCqwEgEfIBc1AA3FLxBjKb+x8LhCqm80LBrQMEAJZGXRZmigA8azssapgIE1awZogPEGt7thnCZDFxBFhvHx8YEB8EAKDKzpGu8Cf7kyCAFONEBDeUMDXYasrnIqwJ2b5aasIYuuhvGGcrpe4q7jTdVLWVkUvTQ+0AAGG+xD44SATHEALIruJpYByNXZSQG8fPzuu4/Dfy8TAluP8z+PbyW39PH58x+TWoJmTtcwTM1pflkXeBk4h3fXc3bzy0SWrtoYxnaV0NLdV2vOn4a/Sp8+X6NkkABwE3TBDC5AOQkA2f0Hu0mAGvircVMTw91lkKQG4F3Q+dMkNXSfP39+DFR7dQwsEGW4ej4iol6C4yxI6Qg/cLAMVQFaAE5/zkCM9SClkMAzPrvX7rQ/vZ4MOD83Pz9/ZnByvn3Op0ZUwDNNtfOzM62tVRUz7fPtTWf1AJ93vr211Xdw/fqzvmDr/Hzrd7SB9TbQ/DMPesJAv1r/TOtcrWu9JtA6f6YVRHjCFXvCoO31gT5vkxawPthTBVv8Ttjl7Ydm1lfNs+s1gJq5+fPcwijrGuUWqia9+zUA5/wM196vKhgGkt95cH24tkkTaAUdG2S5F9Qs6zr44IOzPa3r8UBTsRMaF+6MyjXtqvVqZKioCPIgjPdxi2zYrgH0h13PyIH1rle0gPOTLq5PWMbrYlxwab93Ugs42z9tAyU+HawIhyvgAPzKFQ48rQE8aJ/LB2U/A2dG+JlnwEwJaI80sDwHTH2nHwJg5JoCc07tuQSIWZfTxwGnfE6AK/cI1f6w3tkesHNAv2t2tulp5Xb1HrfeOceVEO5vD+wn2OOAnj7LA88Q7tNAo7CEXyE3YQ4z33lQuWtGgEVRplpw8iFWywGPvclepwbgaj8C8NT5fT4fqwSE1R5UBtCWzY6yZLc1IS1l+ljRkQyQrFZmcLI2u0cFeJpYW5MHBWR6/HbGp7bkOSiulgN1NsYp9ocEAKtd4mpZL2WyjM1pc9oVvZRpk6yWAn67jbH7fE2MRwbUyVYravDbm8Shk9YgWY2dGh4IeNTrBUB5ihWv+AWoixaj7E0+zP5QhwA+y/4gmfiSDDdyf6hzMsj9IbpatT/UofeHOuz+YEPvD7YmH8oSvz+oMtgkq1X7g1+1P8hXK/eHJqcTtT+A1U06+4POofI2Yi1x4NNPKYEjRwiBfdxjpzslxd35YXSFHnBbX0pKn2wFFhCsd7rdnbIVWEAIa+T+CkuaQOg2hWQrUICiO4/rAX1uWZX73H06wIfu0EvRZy+F3B/qAKBD3RFXn7pTZAnQI30kJSXUt++ll/b1hVJSFOONnhqd0TcdO28jAW77NMSHK3tMY/K99Glf36cvqdd/sfa4JQ/EWAX5jD0zEcqSlmbhFjL35BTkJ2Jkght5RAAy+TVYIDFPRDhACM/DxQMgUUQgoBcOAEtExWlpxdFnWCANI35zUnZEZIDnb4G+Ax++l4SyZFVaAsB3fj/x9u8lAOxIO5NfkLMHFs0vZUYccxlGq0algBCex3drnhyBwPdGX35MAojhkYETEEmGb37cLQHEcMnU4BAJ0N/9T/IaNASBq5Nf+hINcPr0xJc+JAbyBhfGF6Y+/HBhYTKRCEjsX1gYvwSBU4RA5uDCQs+HxxcmI1NDB0j0gBwLZ8J54nNdIDGpLhz2RJ/qAwotCWBRhHx9hNzRo4DFO0cLKO8vpwlYLLVe7yELSwx4vd6KCq/clSYwRwswzCGvsuobWzTDsHOU3Uo9cAZgAAawaEAcfixsKQC/JZYBUADKa8fhZQAUwFeIZQAUQCqxDIAe0LqgQwyAl7vHTr9MAZxmx6gyvMzW0FmqYV6mA1wsZdHUALUl6qKpu5V+4JS6MQCBDIACoLgTDwZwOIanpoYR9x/CAMMdHVNAHR3DZMBUx9QwJ7BAADigGTMId3DGHLoAZ34L/KraFo7QA4Y7phzD4rfbhh1T8joQADDhWCcC6xwOeQpUBthqRCAbKsOURB1Tkni+eyUSgARiGUAsgUvEEoAXiGUA8QMo75+uPJmgmh0GsIgAOzdfGyQHWFd7+5lLgy5yYGYWAJcGSQG2dW5uHl4if44MYBnX3NzgmfDkYJgMmLa4ZmcnQfsVFdNEQCt72pvvDZ844Q22EgGuOfb8+Y/7q84Hpl1kRbcGDhye7a+pCbSSdutca2B6uqY1wBKPtGsuYJ2jGOm4nK1LHIhD/VeM7sQoCrz7rgyoxEgE+lIaByekiLK3RAmA2z0Bf0y8++6EABRhZBLa5+MH+/oGJ0hqSAlxVUzAj14JgAUjDng3pZH70Qg/j9P4EQnQyTnq44AJAktuN/djsDMlpXOQpOjBlD4uEzygvkvUrY0C8W5k+PQGbqIz1CcOAeHUUAhrqR4jK0aqy1rpyVRKKfrbehdSih6gtkRdNHW34gYIO3DUNSyKLtJ+kSv1RdT3KrWA1Id7KYHUVDJbEiB1M4ktKUBkSwaQ2FIAqZtpgdT0/rOakgKFDdyP6xVakgBbBrrAw5PgH/qm0LKzDqCHhocbV783tQWiT6pO4aiAtG1TjvGh5kvj27YVQkR5CkedoaH5yOzE4OwTa7v42jtwtiKWCucnJvb0BxpSSYEnJ/pdFcETQry+pS2O+fbZ2SfaOwiLTn14S5Z7W2HWmidTSbs1rSsNxA40UAwcr8LqX2pKCTSkrNWWAhhYrRMvBwqz9MLlwDa3frwU0LcjA9K6SMKjAJEdCdBAZEcCJOmozpZfKTtBqx2ebWfLFGd0CZtXAtlJk+CfRvNKoLO3c7JPq/nU1P+/vfMBiuO68zzNPyHbiqX4zymxL7IXWHmsTRnduUvK6kJURUxStiXWxFlHsFbFRY02U3XIWiZMLIjBc8OVYks1MILIUGSXCSB0i7QERJgR0vAvAgTMMIPQyOGPQDNKCUn8cZLiKrtXydXde/1n+s/0635P1shI7i8w9PT8Pu/3571uBuh+zyYC2sbD4WFfeDwX3Xy1wS32EA6DN4i+sErzdMN/EoDc8TC8Knx4OIxu3rt5s8jDt33wyvOwT6X5zRKgDeTsGQ8P56KblwKwSmG+SsrNywHft8eTfGrNy4EkrtuQzUcDGs0rAmrNKwDqzUcDGs3LAc3mZYB28xIAp3kxgNW8AGA2LwCYzQsAZvMCgNm8AqDefDSg0bwc0GxeAGKs/06o+w58DYgAKPCNA/kKMIGv+Txbt3qC9q0en4oXAfjaJJxUuD8IHtIvogkB8EF7ewfzEzjdhwYOcWpnf1b39zPfPO2HEIoAo4xhB3gDC78/M6oJdLBAZye3oQn0MHb2YJANrQcTeKa//xlMoEPqQTskNulgZ2cQM+l2D9Nwf3+HRlkjh99MOuOCcZA+gz5Khc1rgOjp7OxRtRcDm2d6guDtclFHj4q9BNj8rcP9QJ3fUrGXAjhai8D90wkN3dKBtQlo/SFqSQ5I/+xUerldvDILkEsVuMVEkY0NdLNht2MDpVyihpgBxCERJw2CuqVZ1gYNzcuB/6Wh/6cDaxOInaRX1Y1QURqRWtw7wEoIWOfJAHp+xE0EuEfgnH74AF1TVuYlAea93jI+JixgBDjw1uAD1jKvQGAB3hFAbBihMQHauwG62FDWgOuhwQvtN3hxAdAJ3jJYJxcu0AAcAHnn3XhAwwiIv8y7wVtjxQKsNSMboAMANOAAbu+rG6AHUKmRGjeOB1fNyKtgswxONYgVkttlddPz/NjDPYDmy2INWBsIAUFy4GHQFpnkOctf1wEd0AEdiDVwH/Qikf6iA2sFaFbWWeXdf0bcDO2m3LRb6QWLMuCm6Hz4iAuA96tLN7spmo5+SRlwU93W7EWawgYoqg9oUekFBGC9OX3z5k0FB+ikhUcsgHbTtFLKDIDoOIT+vCYHnw7gAHFx38MWdzrWgbUBFGOLA5QOdmXpQCyBs9jiAIUJQhHSgTUFxMV9OaJSqiQtrYQq/bKC/j1OBx50QLic28gCRqVLvW0CID4zMICisgWgRKTctLTcEkX5BSANS2OfASAOiThp4rKuwaGhAzEBeP3H/43ep6rcAkKid9cFMie9u3a910uCAGDXrhuP4xMMsIsgLg7Y9WPc5HkA28kFaGuago+/xUr+wjtTU9m0wbTLhJn8hV3ZNnB6KC2dwozrwi4T/B2XLuWTz9UgQA4mI20w7xKSV4+rjbW6IdKFx9Xi6o00G9F/gC9NAGSrHroUMBEQADCXGmyl2ETvLlsxqGs2NtG7y8RcEGHDjar3jJH544QBNw/OA9fTGAQAbAbKUGzGrS6okslkNNl24fYH0w9nhJGkSfTukkuDiAY0iMd7o/U47ri6Z0oilA58YYDjhCIHBGcFGRkFRDkUtrQUEgAFhXCVxkJNHxEgowWe8loyYgcQh0Sc9F2UFU/3AXgYBCf1z2V+TYOTyzMbuXCq/2SEuHUHIGJobzfw5nQhEmANIGK4dMnAm+emoYE0HuHXHaAZf0hAuI/fHwr5hWdIAE6P294OH33JyT7hmSpw6RJ8BFGkCc/uXUgwafm6A6pJ8+a5bD9EniEBoU2mHyL+kABvLh4aEEECqBd0QAdIgYdBD+aaLqdPiwHtNV1OV1eLCI0lWhg1NSUQAUm5uUlEIXGBiZJWWdMlsnXa5MjlAJU1XXJPnzrNRZPUxC3aor6mS2UqZ5aQm8AtV6K+pkvrGL8MipC0fIeWvihAlWiFFhygNS+vlQjIra7OFZ5hAAWtrQVEgFQPy5ouMdZrImH9LxXYHXrNbHvtHQJgyjw1ZT5EALxmMBoNBCHZaCNtyKZt+B5Ks+lsoqS/Umqesn0FHzhkPMQ9YALvwMZf+8o7r2EpTttEBz4j8N8wpAOxAe6LFA/F9Ur6HIB8UuAqEUDTltctkSsRMQDL+6+//vr7FoKQaADQBCFR+a+//Xo+CXAWrj+81jouVsDrSlJsZa0ANClgIgJo2rDLEBnCGIAxFVQz1UgQEr1r/S6aICTKsMu8y0ACZBsoQzYJcD867h4A/yCR1ET6mg7ogA7ogA7gAvIJttHSAQKAwhYSWFhMLV4xYAP0xyu347PBAy6wcvt2afzfLHy8uIIHGD42AyD+tu3WbflNR8rAxx/nlRan/s2KreljeVDKwMpKXtPSUtNSU9OKPCZl4FJOXhMjc87HWMDZxrymVeeSP6+p0IIFWFoqU6tbW1JTW1rzsQCq5WTLydZW8FAofwXV05A42XqyMeoF5NDIP9kSHY8aQCneGxcB1uAB9DAAcXG/I7IGSvzke4RAYtFvCIHERMWw2tvRgFJYB5MvqQAKYV1WB2RhHbp88HLy/v2X1QBJWAfbjzP/p2xXA4oiYT2yP3k/+4/N/QdVAH8krMvCv0IvI4EiN+Xnw2oHYtq/dKkd7SHgDomrBb20H2xXAYoSiyTVaod1PXhJJYeoah28DNJ/RBEo6ufjknTiwYOXuZhkQMhJh9hCUa4iUViPPBrfflDRA0Wx8YeogCQsZNKhUJEk8aixhUhaMpf277SBEKVcLSQgX8dOFJYioDC59+9UgKIihwLBh6UEuPmSKoWlFNKgI6QAcGGpjiWlsIgAGBYZAMKKu186u+drRAI/cxt3kAEL3XQrCUD/dAH85D9C4IH9kY+divC+ATMV8V+YTxIA9AqMzIKRCgMs0N0rNL0AU9GMK45pfqH7p/TCAuOqBccDvQLOuis0VipxcMGuhVcAsHdhASILK+qpKAHG6ni0okMymFTMWUCStO0RVft4WVmLU9XNOYDvONXgpQAD5WmbiwGt4CMAW5xszeB5gDmANEop9QAOUTNeNBxAnSIwBwB28DxwXyT0Xc0P1PUFAA6TAYFyIg+1VgV7FaCWdnBbNYdrazEAK8VbuSgaw0OAcvMOwF4MwEW5eBuvW5Q9EqAEoFzIAA0cpiirQo1UASq619RDUh4lKklTtDj2w1pAAGxbhaAirFrHUZSbL6dQV7WhAZ9aa8rLvTVuIR3Vwac0hFWHdw07P41LPGo1DqByb6Bc2h2f+zH9+QEfEOgPUgfa0gEduKdA9L2JaE3pgA58rkDc5rLrj05R2Y8+Orm5PFNV3Nl7cvPmTwFwcN/mskEs4NFPN28OU8ayzfsOSoCJCW/5BLNVLgVMpmXvCGX1BkwmP29b3pmZOdkz3LEzs3y8o2NSCogOQSdr3mHvmMnMBMb95TsnvBP9oQkJUFxc3NBAuRuWiosrmf1XvRMh4GNior+zvHPnzonJ8plOSQ5Plm32Uq7Nmycf5XKYmOzsDJZPlINvkxPl48HgjMTDp2WbJ6upbJD6JAtMBDs6Orzlk8FgsKM82AGeSXOATcN+AI7YclxMSUm5uDOYY0+xz8CFuuwTmRe9E50RIHyzdLXYmL1aeiowxwDDKSkdE5ACpnCx2OFMiJVHAAdFOdwUDb5Z/8JE1GNPuTgBl/RKGZ6Aj5OZM8Md46KyOq0U7XRTbieVxabQ2VleDi1TxqEfOwy0vFwMgHc6oH3wBYGJzP7yUIiJKOUiXI3Ws3MmfDEo7jgneFPopyk3A0xkTlzs6AiywPh4ir2jA1ZNDDgpGJTbScMcQKdOTIDXvSn2nJ4eEFKwc6Yz2BMsFwEOPiDKDZIunyzP7EnJKR/vBJ3XUd4RnJgABb6YKQNAFn6uSsGOcVChjskgGIKToPfCoLI90sFnBeYumqJdbJW8wR647m7HxcnyMHCSA+rklQClq3mltlVbadOq7S/80OjpAe3aO2Bnd+TkdEqBJxgdOAAeSrjBF+zJ6fEAyJOT0wPGVYpnQgKcqKj48ERFTsWJJ67u3Hl150uZVVVBT2DInhIOD9vDV8Oe8NXynVfBazzw1Q9Hcz78MOfEiScyr76V+dbVq9vAF/gegE/Yr5evZoLvkZAqKk78Gn4+IT3m34IP26JPAiD2r3I5wL1Zu3dnbWO+dXVlHe06Cp7CZ1nSHC6dqLgEcuBayuK+MrMgCqzfknr4asVoTsWHOREg663dXbuBMUNkHQWNM9uikNgcnoA5gEiO7mZbP5rFRHKUb1/WD4ykScOwsrgvMRBjmYy0k/i+LBYR35dFO9G3ZBTwiHBfFu8TdQ8Hh0TueZWZ/z7twgURoH0TVFrtv7UJt02p32blmwGWc3OBudyE3N40baC3rxZYDQb8bQkJ4xfr0qJDypaGlNR7OO1wWvlMXblvKBxOS+OTznDS/H1Z7Baf9O9r6w7XtvkHJ8d7PZ6+tl4W4My5+7IyxEha3eGEJF9o8OLM73N733yTuWcnjjcXOo7dw/qoHRr2+Pz+8Zna2uFeNmmF+7IYhAXaxi9OzPnn5vYdDiUJHYfWxTfHd774Ut/VF1+qra0LaQNJdbW1O1/0zgy9+OJQr+9ir7aH2sN1O18aom9efbFtvG8YI6SkcGhnYMZ1ceil3NraXgzANzxcNXTRc23G+/u62iQMIKEtXF5Xd62u7mLCYW6P1i1KvqS6i9fqZtLYbsYBwmnjw9fK24b4HVpAmy8hbaY2oXccF4jSw3Jf1p74/4kpDjh//sLXCYHz539MCpzf8x4hcP58yd8QAufPj5k1JAfOn29W/901Gjj/lPxyHy3g/PntChfYIICnuO9dmMD2Zs1UpEBzvhDXkWqlSdW+LAK6ujZSVFdX14945MLz6v3wFPuvlO2i7G9odBwsjqRcWgAlBbRCAjE99aP8Lvykz2+PPGCXNSLcjiMfGkwqKuZKAOHwJjyACA9R0pMA4WmG8ERGfKqMsZDrxyMUF/89lSXhFYH4ePSi8wggniQs9ro2grD4C+Gww4pcOYcblnCpHWZYomvzHilWP3oYfSC5mM+seG2kChBfrXBdriqgHdZnBUhDIkz6kWKcf1mIOu7wBhxFgF9imQtACNOeAzDDiQC44fAAdjgsQBAOA5CEwwD3Rd+RqfMxlHTgboHvX+ux23NOYAO/sP/iOx1PP12R8zQecK3vlmnq1i3waVciooCerfUVTz9WceKxiss9HizAXv9YKXU2n/7yT04o5RENXINAi4X6yU9OnPhQG/hFz7XliqaWxrONFZcrcuw4QM/A07aWRkvjT35SoZREVEj271x8zAxCoh+bqsAJCeRwrb0i+2z+rf9xa6sdJ+lr9mvjFY89+9jTt+z2rThl/c61nmvDt6ambuVc2qo0OtBD40PcocEMvh57ToWS+QNzAD2YQBaJHlpgtma+ZhYfqOlj1q9/8yYmwJpD9WEBgj1wggHc3CqWgg858IwE2BqdugyYl9pvHdAC+mRAdBYawDMxBzRDIk6auKzkHUc8NMgHH/nwziI+gNT1sACEepRQOhAboJpQqMudlO+EBFIGaKvTqbh2IQKgrY55a4NV+XZLRcD5qpued+ADbuer3d3zThIP8955F0FIFLOyoHLWiLLSbjeirnGNhIrTuskj6g4LHYgJoAtHsuOguOmNH6tKDjibVokAwykNezmgTcg9aEUU5YEuJvSQp1GkaA9OQg9aKUR5oIwx96CFPAA5EPeD1uEQ5cGtOZbcEhm1j4eHRF+SSFq1dRLpgA7ogA7oAC4gXUdGuvKB9B+zOqADOqADOoALpGLr3gDV1YTAlSskwDInXGB5dAAwqQOjCCIKqL4yemV0dHn0CiKP6JCAORQqDQVgeHQYfGoAF3jdWF290nQFfK6u3rigJA7I5dWWVzy6VFy8NFqc15arpCjAYEhdNhiWUw0GTMBqPXXK7T51ymrFBJacztWmpjyn04kJOK3OvOLiPPANEzBalww0bViyGjGBYvdS3qnivCV3Ma4HAwBOLRcbslWBq7xai08tLZ0CX8XFrVeVxAHCu6O85SvVbnf16HIepagooHh0tNjtho+YAJU3Wmw0Fo+ale2VZjE8daq4+NQphL3ytIcGlevhFAE16YAO3GPgJLY4gPgP9zoQE6ANWxxA8D9OHVhLwDewtXaAbdu2kQDbMu/cuZNJAGTemZ6eZglMYNrj6ZvGB7bdmfbY+6bvbMP2cGekr69v5A5+SNvujIyMMA5wywpv0mbrunZ6Wg6kY0sHCIBabOkAAbAJWzrA6Dnwsem55zaUlZXBjU0bnnvuOVXAOz/f8FyNd8Q73+D21syXNTQ0zG9QASbmG2pcE94a1wiwrJmfL/M2zDeUqQDeebphvmxiYqRmBLTurfHWNDSMTKiG1DBvtbpA0yN0mbUGxDbf4PWqABvK5r1Wb1nNiIt2zdfMj9RMgJDUABBzg2sDjH5+ZAIEVOMCDtSAEQDAuL1eaNgw0bBhYkQ1aYnYDtig3g8aekCAMi+jCUbesg2awIaRGkFWSwNZSGCU3/scvihAjPXzn//8H3/OS9hS0vuBV5KTIdAY2dOlZW4fj/v5yxYqfyOzqzGfan5f3fzwYeBhI5XPBURRL2uYM0DXU83c7uaN2zXMGYBVo5JplHkE2G6hmhXDkZkLHvIpJRdR5gLQvD0aUDBnga6IxUYtcwi83Ew1s8XpstCRfkCYMx4a+X54P9IPSHMW2GjhzCyN2zXMGSAy4v5Rq3VJx2nFrgxomksBDHMxgGUuAJjmPIBtzgIE5hAgMgcAmTkAyMwB8DDo21LN7ZXq8kGZZEDBbYn57UNyezkwLbG/FWUuB6QBRYUTBUgCUggnCpjWCEcOzGmFIwNEASHCkQHTmuEcrC41xCkEhAgHWMO/AkcFpBwOay0GplXCiViLgDlkOGJrAeACigpHZi0A00rhRFtHgLnocBSteYAJSByOorXbNT/iLYvjA7qlYs2acnfKcwFdVrYWm0YAEBAXjtg62jQCTLPhRKxRpjwwB8NhrdVNOaDg9iFgjWPKAf4mXFMOeBgUUlwRGa24c4ffIwTOnfslKXCOJCwGIAmLBQjC4gHssCIAblgCcO5cg+JRL9UHYuDcCM60JWLgXJmVENAO67MCpCGRJt2ANVdLxHwT3rQ8EeAHeLP4RIA/YtpzAGY4EQA3HB7ADocFCMJhAJJwGOB+aKdEzKzEQzsR+osOfJEAj0QMYPcgNAyBFAJ51ihAnPQa7DgdWBtA3DpscWdj/umUiQiw0Qbz1NQZbMBIZa9bd6bUjAvYKANsHB8wULZ15lLATJWqZCIAZyjKtM5WCndm4wPakoW07gyAVDl50qXGM1O0Wtrysk5NrWO+sAC2487gd9w64qGBIw7QfCsZkQ7owOcNxFgmI3W2ipnqOzK5fJXDpLLuAHyRRTigit2DnnK8ikcYoEpoAD1HOWcEAS1zyST/2VGT/CsCqusO3BWQMReACxVghzTw5tbprVvfHJAkfZYymkwOmDS7JUo66c2tW4fCPVvfTIoAVbwRW1YZAoGeHrsIEAz4joN78hw84HA4btbUOBwRQGhPGBoQ4V0Er7QUtrR0dYlzUNWnwYstLbMtjdjAZLBztmVwcgwbKAnO1Nb2d5ZgAwm+uatXM69GnmoDSSev3rkqWsNCEwCDQ7xqxlqc5H8NAg+D8M/13Bl/LQMtL1dVvdxCAFR9d+fO71YRACUQKCEBXm5sfBkD+C6vEpqi6JLvakkEWIBIgIJWi6W1gAD4bsbYWIamvRjA01oEniWUDsQGuE8SVqkopUrS0kqoUqUVLP5dBx54oDQiIwsYSxVkEwDx6ZkBFJUtACUi5aal5ZYoyi8AaVga+wwAcUjESROXdQ0ODR2IAaD5lkd476MDdwGYwU/Jd20m+MMScVmOAvCswawJ8He0tpiefbfUbCi1AcSpds/rDzmdBIDBSBuNpc8+W/xDJcmBqWe//OyzRoC9iw1k0wbaYKCnsAGTecpgM5nXYQJMQY3MIxbwvg1YHhxbhb1RiAP80JnadqPthz+80XbD/DIWcGSM2yhQdsADwkQPlSXM/tcrxxTngeAnghC937JBHwWOKfW3Y+Jd5sqqsVPrNN6/SfZ9mR2t+ICqvsjAX2NLB9YU8H+wxQF3//NBB+4l8GdsccBL2NKBNQVgDo44kR4IYCn68kR1YJEMuBkeD09b8QHH+PT4dHgcH6DHoW5iAzR1E9iHKRoPoJ3+cDA4CT4bfFhAOOAPTk6OBSaDYd+kWxuwBnzh8dnZQEtgfNI3SWsDdDg0OT7mD7SMhSfLQwEEcFTQ1fJQ+XggMDcWCE+GQldFryCAo+WhUDAQCIwFxn2h8iwcoDzkmwQEiKg8dBQDADGFJiengb2v/G0c4OjbocBsIByeDYQk9mjgaEuj01HpcBY2ZmECjWdnZ+fmZgvPbsQEENIBMkB6v8JPJfqKRDqw5oG9ewmBoSFC4J/+iQDYOzQ0tGHL0NC1vbge+r4FN77Vhx/S3rING8r2kuSwZcuGLSRJD5X19ZUNkQCRh9j1tA48AMBT2NKBzwhs304INDcTAvn5ZMB2ilKKSRHYDtVFUV3MBg5gEd55WnCAp57q4u27cHPYng/N86OzQCbNRGWJ3o/0wK4qhu+hC2QLco9KAQnkM93cHN13yJAk3zAApHQglsA+bD1EQA22OIDC1t0CedjigL9T0JNPKu1VAUZHCYH6egLgySefvH78+PUno8NCAO3HObXjhvRkPTSvj84bmcOTEFCoExIYhYBCnZDApUtPPnnpEj7wJNP4KDIH4qER+9FKfACtwZNA7IEYK5fsxuDEuB070kiBHX//z4TAjh2PE9w/vYMVdio8gJ2KAOz4+98TAjt2HCl0aooFqnhkLM+sIRaYm96xYw+L3PiH11TFAc/09YU5J3ve0ASqAs8884xHSKXtvXsNEId0F0kTl5XvOMsfNCUCXv/P38KQAGCZC8DrG/DsOeD1LZjmHIAZDQ+QmAOAIBoWeBg0+U0yxa0nJOLWExIAICMgQEQwAAnBAusn/xVXHLD+Mu698jwgJ+x25W0BkBEdHcrbIkBM2O39/Xy74m0pICI6wQ+wos7obSSQEkxMDCpto0JK6ezv71TaRibdwXxGb6PLitTddhzp0CAdfKTDm/QAIj1ESU8CxKeZ2GsHoXRgjQDKv7DRNI2YvEkZoGk3ilAEaLe12211KxLKQENL90JLQzc24GpdXHG5WvGBbn9NIFBT41Z6TTmkGsfZs46b+B6o7vlAQNkedY8fTcmv1ogAsR8aOhAT4G70fw6q/9oj+gWIFfWHKUKA+sBGCFAUXlgiAC8sMYAVlgTACUsGaIclBuif/pTWDEsEdNcvL9cvgLC+ggfkmdqBTCaNsASg6SNvS8vRjXDpuw/MOIDt7Vmns6Wxia0WMiweoGnzRvg3i41N7Dn+D4c0gIWVvBaH31HZsvrTvRCw/FeEeGDvSt6s0+p2tqwu9AH7FpR9BFhYtPkrW1oq/aaFRYquQtpHALpvpbvYZs7OXuhzI8MRAxS9mPzKyspez+1udDgSAPT0wsrKSrdaODKAUq0OAlAPJwrQCkcOaIYjA7TDkQA44YgBrHBEwJ9x7XkgHls6sKaAbGzJjzhNKQA1chvJDgVgi+ytpHuLGkB7N2zYUCZC3HCiYy+NBCgKGEg9gAZUQyqbL5PE5AY71AAaWEsB8EmrAFriAAe2OADzz/yJkWlJdWBtAEew9RABfdjiAOLV3X6GLRTwy1D/IG04VY0JfC/U21vkhIewMRUH+GXRP5eU9HJX6DdxbSCA60CTRUW/B0BiwOVy0TQ9APctW5uUgOuMfWJoMDA4N1bU74eqpAaujxqLFTxcv87ZJ/aHoIoSQz6f02kFUXEvyYBPS0pKJECRGACvfioDflxScgF+vwFDAioa5EJior/BvSrO4ULJDeb7jSJGiS42aTbbX5aU/DIq6R9HysocXCFaVNaffe/Hqh0H7Iv8NG7H/YwZGiBp7KGhorsd3sQH0Bo8CcQeiLFMRqqQnQItMpFcRmWeyhyD8EUW4YAMdg96erEMHmGADKGBiESz8yUw85FxRhBQME9o9VuSRIAwhV/xI48UK8wx2JLvz28VAZpTBuaePF1S9TgucKTg221h3+Rgb9Lfv44TUsHJrsw2Xyjka2t9aedpIelCOOdfJUya3YokXVBVkFkSGBvbebqrqiqJLytvxJZViuzMfP90qGX2pZdebt3DhSQY8B3H7mHyza369smuuZbZ3CPfTng8iXERJ7QnDI1Cdo7B3vBAW1vukdbWuZKC3La2gXAvlwNKbbmLy4NzP4LXFv5obnB5sbdNC+hdXA4HvjH71luzPxoMYwG3AXDyqeaup94CwG1tIGk57BuzWLq6LJYxX3g5SQvoTVgO954uPNvVdbaw99/CywlaSSckDC77BumrhS1v5w/6lgeZXRrT7fl8vha31eJuBRsJOECCK7/JebbQeSrfmYAHDPqtVofDaln1YQJAla6zlcIzfY5BLN2Hc2uB0rm1QLUfCuTnVrQ5zrlVDtzr+VtzMzjxPyOkIUXP3zr2Cid+alL1cysA9n9cmpJTMZojAJx5AVvWAhkCgeM5oyKAN490XIFwbgU6nbK/dH/OpUs5p3mANxcNjYJCYf7WqpSUlOuf5uTs5+eM1TqAcvem2Gdm7Cl7I1XSABIWU3r+9Zs9KYv8c02gau+1b37z2t7ILLbax/RY37VrfZEJYnFOAlUrK6KJdXHOGkmiNwJrcrLUNQjcF0nnZpNeAPK3EumADuiADugALvAliaR/fFdcVkMHdEAHdEAHNAD5eqDv/RcV3V/ATAjsogkBM1XNbjx5UUUcUJ1nMhmobJMpL3X9+kMpKuKAXUau6sXr8YD16+GVThRtWo8NrIc+Tq3HB3ZRVDZFEwA2unq9iWbq9J7WTX5sDrvAQ6ppjY0lbOA9md7QSlqtioplfRgAteNF8QBSK7tiPzwMwJqcUpYY0LrTUHTPISsKW2sXGMMWB/wztnQglgBxx63BwUcMEB+ia/A0QwzEWF8l1BcTOEgo1NhG3ZaFuIFoL72wQO8lABYiD1jA3m5gDcy7lXwoATD+lRVEHgoAvcBB1IICoQzQMCQaF6DolZWVBXjhPmZIgOhegCK4e21hobsbBdQRKu47hPpiAg+D7v4/fkkqVkpAiyUXacRfbyIFkGvH+jwexZAQavOkp7fhA73j6enpdvEeKdB6WvI0KWyHFxYOywHhn8lnW8Sv+TzslYjjMqCKlrbLKXecv3RRDiSdzlCw99kj1zpGhaSg3mHRxZEqSSs0D9WrASSNp0sVVgd6PfILNtV7WhYOI6WxxCl3ONpc4oIHkgpakxDNS7siAlho0BvybCNKaZMDCVVVCtkKsvfKgYSEcAraHhBtciA8DOQRyS4oJSUlPWWcOfAiwLjHMyzTuEThcFhypVpuL1AbK59Ek2FeEg+4+rxPi/dEne2PEikusX+UCIlLTCzq+JgAYW6v6DyOj7D3Y/R7XsFFuBs4ijpewUQid3x0HsdDhFtEQFg4CAOEImFpIwzgHEwM+bmwtBAGqKSsdCgSljoCgZCVoqyJorDUEAD4mfukfPgAiISyOpwh/JASE939kb7ASjrRT1hW4o67i6FBOvhIhzfhAUR6iJKeBIhPM/dFr0JduXJlcHD1SvhVNUmA0bBv0O/HB8Krk6uD4VUf3L6D5cHhXw1fGWc8NLw6Aj60AJACyIMBRl6df3VkXhNIfeO3qVfGX4XGcIdbEwiDjzfCMIMGuIPWAsIgoCvlkSrNWzSAN8LhK5PjPh6Yp5VKJfHw2zeuXHmDTfrOvKtBsbRiYPRK6m/ZpFUk8fAGeJdwg2RoTN64sjo5SQSsXiECSEMiT/oKCOgKSdLl4RuD4clBbGDyozB467UPH7gyCd+ukYQExsYbhEkv+4lOAsvLq6tNBCeB3zr8/tXlZXygetXvX14mCKm6+rfgsK4mANqrU1OrCQBgDJSKBXwdWzrwWYDXbAaaNthewwWmDBRlgF9TeMAURZmf//rzZ2wUJScUgdcM1Jmvv5tNwSsHDK9hADbK/PXnDRQkDJQNAzBQzwOIMptLSwGCAdDAKJsuhVsUTccCIA6JOGlJWZ/HAIg7jnxokA8+tHSAAIixlP4+/4SKIoDRZjMSAbYnnrDFEADxVD/xRDUflTZg45/aYgUQhxT7Kt1Nx2EPDbUXdUAHSID7ouexpQN3B7xbaqBoo+01XMBMg7ckpWZb8RQeYIZnQGbL9A4O8A4N3ylx288rhSUDisGbsMhr75wxawLAgRD7a2coW7ZJFXgXZPBuxN5sEPu7NwBxSORJE5eVuOPIhwYz+GiSwaclHcAGbJS1n/9ruZ8ypqrPbvenuLh4E+0O8cQgZVAnIBBfTdM+ngjRdLUmEJ9qoAYjf/O3UnmaQPwjRsrPE5+4KJsmEP9IMeXgF2kpclDFj2gB8fE2yhVZ1gUUC0UIQHyeqLzoYokAUF46Ul4fqlhiABZLKK+bNmkC8amiYoHymjWB+EeyRcVyUqc0gfj4U5RTVN7sqGJFAfFmUbEC0cWKBiRj0UcZqjUB+Vg0aQKwvAFUsRQBWKxKngDFsmkCcCw6lcciAoBjUXLgHtQE4vNot9JYRAOwWJHyCgeuCiA9cN3cgasGxB8UH7hWtliqgPTAdTLFUgdgsSLlTayEY1ELiBqLmoD8wDXFxV6ouVreRgg5V4uS8SwEUHO1KJhPD8xCADFXSzRQk5JSAwDUXC0KEfX1QQ+oqVfkzdfUcEljALPwI8XOAai5WkTAtH367emUetYFcq4WkYO+FPvs2zV8P6DmahEnMCDaRs7VolAlFkDN1aJgOzsNAVRL0QIdN60JzDLj5212GNlTtD3AirIboEqzs7NaAKioh3ExzYyjt7U98BWd9fS9jQdA1czWCE9wkp5OEfUcGuBin357diClbxYDmJ5lkoa2oojQAFeXafl+JDBr71PcTzA0OOC+6K+U9JiSPitw7K9awRcBcPZY/rGz+MCRPa0thS2te47gAscsheC3skLLMUzg2JFW5o+CrUeOxQggDok8abashTHtOOKhgQ0ovqYDOrAmgb9W1b/2JScHuXe6OMC15OThTxKxAXHzOICkeW1A1jz4PUUdkDefGExWA6KbH05+RQVQaD55ehsSUGz+zje+gQKUm/8GCkA1jwKQzSsDKs0rAmrNKwDqzUcDGs3LAc3mZYB28xIAp3kxgNW8AGA2LwCYzYsAvOYFALN5AcBsXglQbT4a0Gg+CtBqXgD+EVs6oAOfN6B235BUOqADnzeQiK21C3RiiwOewdbaBRRW6Pj1lepfoxfuiJ7HwEyHlyizwvwGCMBMuUNuSoFAAM9TlNMH/yjyPCYwRVGBEFydZQoByAdAiAIhDabaqBBiaMh3F1EUbe1vMtFFmEBigHJYK42GQNQLyNEKCAcVba82WsPhL95oVV9mJnpUorR2ATK1ResN+bxsYsW1WdvanGNtlZVtc842+MSqBTCGFubTOQfBNqvSlZC84trG2sDn3NzYHLsFPp20iuLaCtvaKoHxWFsJaLwQfuKExMTO5dK2Wq2iuDZgXgg8gKRB8074qZkDDL+kBCQyxn53GlUEQ+Jb5nIh74fVPBVxHthPzoNGDiUgcLb+IPySubmStqViFcXBHi6MhMT0uHoOg9FqUhPBbE1cDqcIFTUnoZbIPRDnoFoSpSoR56DWq4o9TZyD2shUHK3EOdwXbceWDnwG4Oj2jRs3dm3cCDZwgI2WoxshAZCjzRsxgObmjazymzc2N2sDzfkglK6jG0HzFghpA8Ds6NHm5mZLF4jJog1YYET5VHMzlQ/ybrZgAEc3WrqAAwq4wgO6QPtdXfl08/ajOACTaj4IirJ0gRJr5wCrCfpsYzPVzBRKEwD9ACxhz4GAcPqBJRhF2aPGUrPF0gw7wiK3R45W0DjANm6MeuHBOYAeRIBM+vtW/X0rKgf9fau29PetWDk8JO9bCTL+LICVctFW1Oyi0QBNLXYvWgeY9eEbdyAl8jDQ3Xd7YHHRBYGvIRUBrG5rn8czXH+7ewkPoBf7+jx2j8dzG2SRb0EqArgWgQMA1A8surGSpt2AsANC3V4A3IsD9XBmt8XbixR19iRSYg+3+4Zv3+6DU57iJE25XXTfgKvejVtW2NPdi26XFW5ZGpGSjiX1fGVDA1McgA4BERI6SUTSxAC6o5AdR5g0MYAezojhHfsqoQ96lZNAbKtEDMRYmYRigeYs5lvW7sxm8KEN5GcReshnNrughy7woQ3QzOY29mEbBtC8W7QHB8jcvTsLxpJ1FCOkOUKJbjbAE/l7jS8kQFzWGUJpXCMZLR3AAu6ntkokGUAf6IAa0E2TAd19AzQRsLC42I0PuLuXFgcGXIvgrSke0L3kdi0OgHebuIDV6XJ3d3cvDi/ghuS0AmJxcQDXA/i1hW4irFL96JIbH+i+vbhYnO2yWt2YAPg1ymg0GN1uKyYAes2QTRuc2B6gig0xHnzUoew1dwA92MAHEkktX4poOllBQy9EpANrHPBEHvCAoY/g40dD2MALL4DWPS+8gAsAC+DioxdeGMIE4EseD3yMFQBD8ngIQoJJf/QRQdKgrB7I4Jc1GcbzEUHHJTMZkwDJkRLhAkN8iXCBZMEBJjA0RAiI9EAAZCs86sCaAXzY4gCFpS1aMzJa0SteRFlXVVHw83TU4jDKQC5dmFCQlFSQUEjLl9tRAgoKEyKLyGQkFBZoAhniVWpO0xmaQIEkjFxND2fzJQsLJeWf1QBOy6opXZ6HAy5giwNQs01ESwfWFBBjoeZqSUAIOVcLEkDN1YIGEHO1IAHUXC1IAFVy7vWqkqoSRqeT8IDyw+c2HZ7cdPhcba5SSNlRIXnrxs/tC28KzuzjgQTEXC2cJjrrNgWs+zrrIh5Qc7XwQN3MpgZrYEYAUHO18DlMzpTPu8NBAUDN1cID5w7va7BuqjtXy1dJpY8YYFPduNVdG5zBBoJ1m2jaO1mHDdTVbXJT8zN15XhAbiA4A4CG4ExNBg6QUX/8ZvmmffvO7aupr8/AAMbqPZ6BmtrDNTeHe+r9OCFljPV1ePr6gPmYaGioZ+Ef6Bse8Ee6/jOsgbd2gPsiCltrHFhwSy5D6F6RrUdKw79NiQDaY39lrwB12+3p3RFj98LKK3Z7txR4hVlk7ZWVbgbqrrenrzD76e6VvewCbG5pSHQ93Omxp9uTuyGfnr6XbSclJWIvS/o23J2ebk9JpqmF9PT0VwC4Nx3uAaqno6tEc0TK3tbCvoFAoG+xcGwxhbPvllaJvR7UvQDXjEt/xZoWkXNvOozoNjBpFlfJss1CZR2lrK5Fe489vU8ABpLTPfbbVgtF7c4Se6C37c7f1kw11N3shtUSloOGLhctWVEAeNq8jZ7fV1fXAHrAU2wzGfKajNXFpwCwmJW1jXbLgfxtWbsp6mLdrwKgl24v8UBT/aL76LasjVn5coDKAlmAkP6lzgUv92higVMDNEXvzjq6rSsqJGr3NlAE175/qQu4qRXPIgsswfJkZWXBAsmB5uZ88Bio+37dzUVQ9jwjAPJW7PYVEGw+a0ArDO+Gurrvz4Cs6e7FvKaBRQoMl5FtolEcfTzc/GjIU99NL9h7hoc99hWqu8/ehzgeIuN+8SMatpwDh6Jl27Y73RoARVn3ubrZdQUXm7dtE0eEOkRdVmqBsaeBhyxKG4Dqrq8Hx2h+V1YzJiDkRApIdN8A4vW7dIDTpUuEwDPPEAJbt5IBFVu3VhAB9q1b7ZjApXR4voZ/vYdn8vRLmsCj+8V/8t+PE1KFYC/PA5VDOmuejpUDVDsLRK9dhwLsLBBdJxTAlGirQt8hALbXKhRiQgB21rI9OiYEcClqQysHpHQAG/jke+sJ9Cd49zQJAoCO/sTE3/0dAeBJJkEAUBS0EyBMDiTIn9hbxhnkE4D8LSbAIUWJRVrIn4Sb0iESBMhvVBEe6O9nkGS7FsIC/fA/UQzSoYWwPQ2M+jvszCTBn3DIJ7/7jbIgEOwo6uws8nSwufSzCEKfMEAQAIkdHfxOgCR7OpQVhEA/iL+ocxgfSOwcDgaD/LRN6iH9kS9rMNlTJEo68Y8/UNb/5oBOZmREarRpM0o8UFQkdFziD5DmAiAMDVVzEcANPg3zCMAPby1zDuAPoB9s0LSHAIk5A3gIzBmAxJzL4Y/Y5gxAYs4AcXFfgmKnGTWtAzJNrUOIO3vrwFoHzKVQZjlQAMUC5ikgMwecOTMFf8cynjkjA9j7dsxQJkbcy1NGSIAHowxARTwFjbMBRGMDgFgHnMQQIA0J2BtIkj4D7ZXKigKQHYcG5NKBNQXEWEmCcnNFT1BLI4iAXIsllwhIKmwk8VDAemktwARaqSoGoFowgdNWzhGuB1YtVURJAz8ZREAkHEygKj+/ighIoigyD0kZGTgA6gUd0AFS4P5oD54eLKCV+8IBjjSe3NNKn93TQjXuOVJ4RAto3XMk3wKg1j0nAdhIndxz5KQKUGXJP7nnpGBx8vSRI5b8I2hgT0sjv1USvUu1SssDVdE71YDpgYHpEgJgbgBoWe5EBRhgFMAEqvzTLDDgxwHmBkQKVGkDe/wDEkQbGJMAcxg5iF1IskABcwgH6LKW8FUamMaoEiPGOmp4qAAgjbE9Vbgdx6QxF7VPFRgjBaoUBvfaPi8Rn+t1YG0A2djigNj/X1QHcAAHtjgA9ffhaOnAmgLu5+CD01BRBvaDprlLMtWuOTGX2syGdoM522YuNRnNpdnV2beyDWqAzQY+bXQ7baJKjTYDZaZMNqMKYCsttpmNZmBXagZfEDDbiu9l0qSAy+V0UpTV6rQygjdNUk6ny4UCrA6X1WGlrGPHqjL2ZGTsOdYKnjisLrBPEQAtgg8XZW09xqnVze20KgMOq8MJXrOePnZsjPk8DZ44nQ6rQxkArzghAT20Op2V0AOwB3scTiWAZnJ1sB6OnR47xnpwMNnTih4q/QBxuK2V/IRYlW43sHf6KxU9UH6rw++HzS0f4FQNnfr9DmslImmnM1AJQj514DqjA00ghcoATFu5H2iYn9/pqn56dPj69eHRp6tdTtC80xkZ4PKepkHP+h3OpgPXR6+DzwNNLoefokSXEsoAtwPWyM/k8OiBA9dBDn5YJ4cbATjg8WatpJ1LziWjsdi6ZKUBAHY6EAAzAthOcrusbrYzYTWcCIB2gKQracoNkrFa3TRIia6E3Y9OGgwNkEhlZSX78wN8c4NdyKTZRJzA0snIAT8kLyoBNI1+dv+O6S1QXlAS8yENiYEtWxooKpsIwHDCAft4QtMJB+wf3oLphAMu7fdgOuGAoo79mE44IDGxE9NJBEjEdCIACk4oI/J9K/sjNcoJemjwP4WlTrZ4oyUDZE4UJAeAk/0SJ/ccIA2JMGnSskZ3nOVslFSGRv6RLykIPfgKH1ey5wHM5gUAs3kBwGxeADCbFwDM5qMAreZlgHbzAvB9bMUC+NWvxJsYQHC0oyPY0fEL8FURxArpFx0VQB0VoxVBLA8QCcJLeIKYObDRs+YEAJ/5WugH5p0cc8x/CKUDOqADnx2IsRz9vyJSHGUNEQIU7fsOGQB+yejswRY3LemkHXslMvZtCj3YQQLQ1CHKEczBEgQWjt8+kA2q9esDGIJA9/H9Bx6FYf0MDzBkf3x8/9PmbAPl+A0GQB86cODS8eP7Txw4MEVb+z/REvBQemD/8f37j18qhen75H+0kovJYT+jbmxggQXAL4V4IVHZx6F9PYWXNABKP769svLxbeyycjcVEXQcI5KhQT74iIc34QFEeoiSngSITzMx1/8HeoVBt2wcOp8AAAAASUVORK5CYII=);
            background-size: 100% auto;
            filter: drop-shadow(1px 1px 1px rgba(0, 0, 0, 0.54));
            height: 14px;
            width: 24px;
        }
        .icon-wrapper[_ngcontent-serverApp-c4150375230],
        .label-wrapper[_ngcontent-serverApp-c4150375230] {
            display: table-cell;
            vertical-align: middle;
        }
        .country-selector[_ngcontent-serverApp-c4150375230] {
            border-radius: 0;
            color: #000000de;
            flex-shrink: 0;
            height: auto;
            height: initial;
            line-height: inherit;
            width: 90px;
            padding: 1px;
            opacity: 0;
            transition: opacity 0.2s;
            position: absolute;
            z-index: 1;
            top: 0;
            bottom: 0;
            right: auto;
            left: 0;
            font-size: inherit;
            font-weight: inherit;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAQAAAD8x0bcAAAAIUlEQVQoz2NgGAUkgtKG0v9IsIGwsgbCpjUQtrRhZAUzAB5rGRmoZRO5AAAAAElFTkSuQmCC);
            background-position: right center;
            background-repeat: no-repeat;
            background-size: 18px auto;
        }
        .country-selector[_ngcontent-serverApp-c4150375230]:disabled {
            color: #00000061;
        }
        .ngx-floating[_nghost-serverApp-c4150375230] .country-selector[_ngcontent-serverApp-c4150375230] {
            opacity: 1 !important;
        }
        .country-selector-flag[_ngcontent-serverApp-c4150375230] {
            display: inline-block;
            margin-right: 0.5ex;
        }
        .country-list-button[_ngcontent-serverApp-c4150375230] {
            color: #000000de;
            direction: ltr;
            font-size: 16px;
            font-weight: 400;
            height: auto;
            height: initial;
            line-height: normal;
            min-height: 48px;
            padding: 14px 24px;
            text-align: left;
            text-transform: none;
            width: 100%;
        }
        .flag.KY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 0;
        }
        .flag.AC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -14px;
        }
        .flag.AE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -28px;
        }
        .flag.AF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -42px;
        }
        .flag.AG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -56px;
        }
        .flag.AI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -70px;
        }
        .flag.AL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -84px;
        }
        .flag.AM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -98px;
        }
        .flag.AO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -112px;
        }
        .flag.AQ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -126px;
        }
        .flag.AR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -140px;
        }
        .flag.AS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -154px;
        }
        .flag.AT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -168px;
        }
        .flag.AU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -182px;
        }
        .flag.AW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -196px;
        }
        .flag.AX[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -210px;
        }
        .flag.AZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -224px;
        }
        .flag.BA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -238px;
        }
        .flag.BB[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -252px;
        }
        .flag.BD[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -266px;
        }
        .flag.BE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -280px;
        }
        .flag.BF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -294px;
        }
        .flag.BG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -308px;
        }
        .flag.BH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -322px;
        }
        .flag.BI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -336px;
        }
        .flag.BJ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -350px;
        }
        .flag.BL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -364px;
        }
        .flag.BM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -378px;
        }
        .flag.BN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -392px;
        }
        .flag.BO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -406px;
        }
        .flag.BQ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -420px;
        }
        .flag.BR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -434px;
        }
        .flag.BS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -448px;
        }
        .flag.BT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -462px;
        }
        .flag.BV[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -476px;
        }
        .flag.BW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -490px;
        }
        .flag.BY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -504px;
        }
        .flag.BZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -518px;
        }
        .flag.CA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -532px;
        }
        .flag.CC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -546px;
        }
        .flag.CD[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -560px;
        }
        .flag.CF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -574px;
        }
        .flag.CG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -588px;
        }
        .flag.CH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -602px;
        }
        .flag.CI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -616px;
        }
        .flag.CK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -630px;
        }
        .flag.CL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -644px;
        }
        .flag.CM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -658px;
        }
        .flag.CN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -672px;
        }
        .flag.CO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -686px;
        }
        .flag.CP[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -700px;
        }
        .flag.CR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -714px;
        }
        .flag.CU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -728px;
        }
        .flag.CV[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -742px;
        }
        .flag.CW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -756px;
        }
        .flag.CX[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -770px;
        }
        .flag.CY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -784px;
        }
        .flag.CZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -798px;
        }
        .flag.DE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -812px;
        }
        .flag.DG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -826px;
        }
        .flag.DJ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -840px;
        }
        .flag.DK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -854px;
        }
        .flag.DM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -868px;
        }
        .flag.DO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -882px;
        }
        .flag.DZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -896px;
        }
        .flag.EA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -910px;
        }
        .flag.EC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -924px;
        }
        .flag.EE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -938px;
        }
        .flag.EG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -952px;
        }
        .flag.EH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -966px;
        }
        .flag.ER[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -980px;
        }
        .flag.ES[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -994px;
        }
        .flag.ET[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1008px;
        }
        .flag.EU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1022px;
        }
        .flag.FI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1036px;
        }
        .flag.FJ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1050px;
        }
        .flag.FK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1064px;
        }
        .flag.FM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1078px;
        }
        .flag.FO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1092px;
        }
        .flag.FR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1106px;
        }
        .flag.GA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1120px;
        }
        .flag.GB[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1134px;
        }
        .flag.GD[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1148px;
        }
        .flag.GE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1162px;
        }
        .flag.GF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1176px;
        }
        .flag.GG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1190px;
        }
        .flag.GH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1204px;
        }
        .flag.GI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1218px;
        }
        .flag.GL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1232px;
        }
        .flag.GM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1246px;
        }
        .flag.GN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1260px;
        }
        .flag.GP[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1274px;
        }
        .flag.GQ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1288px;
        }
        .flag.GR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1302px;
        }
        .flag.GS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1316px;
        }
        .flag.GT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1330px;
        }
        .flag.GU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1344px;
        }
        .flag.GW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1358px;
        }
        .flag.GY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1372px;
        }
        .flag.HK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1386px;
        }
        .flag.HM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1400px;
        }
        .flag.HN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1414px;
        }
        .flag.HR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1428px;
        }
        .flag.HT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1442px;
        }
        .flag.HU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1456px;
        }
        .flag.IC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1470px;
        }
        .flag.ID[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1484px;
        }
        .flag.IE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1498px;
        }
        .flag.IL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1512px;
        }
        .flag.IM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1526px;
        }
        .flag.IN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1540px;
        }
        .flag.IO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1554px;
        }
        .flag.IQ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1568px;
        }
        .flag.IR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1582px;
        }
        .flag.IS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1596px;
        }
        .flag.IT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1610px;
        }
        .flag.JE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1624px;
        }
        .flag.JM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1638px;
        }
        .flag.JO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1652px;
        }
        .flag.JP[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1666px;
        }
        .flag.KE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1680px;
        }
        .flag.KG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1694px;
        }
        .flag.KH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1708px;
        }
        .flag.KI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1722px;
        }
        .flag.KM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1736px;
        }
        .flag.KN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1750px;
        }
        .flag.KP[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1764px;
        }
        .flag.KR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1778px;
        }
        .flag.KW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1792px;
        }
        .flag.AD[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1806px;
        }
        .flag.KZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1820px;
        }
        .flag.LA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1834px;
        }
        .flag.LB[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1848px;
        }
        .flag.LC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1862px;
        }
        .flag.LI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1876px;
        }
        .flag.LK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1890px;
        }
        .flag.LR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1904px;
        }
        .flag.LS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1918px;
        }
        .flag.LT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1932px;
        }
        .flag.LU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1946px;
        }
        .flag.LV[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1960px;
        }
        .flag.LY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1974px;
        }
        .flag.MA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -1988px;
        }
        .flag.MC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2002px;
        }
        .flag.MD[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2016px;
        }
        .flag.ME[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2030px;
        }
        .flag.MF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2044px;
        }
        .flag.MG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2058px;
        }
        .flag.MH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2072px;
        }
        .flag.MK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2086px;
        }
        .flag.ML[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2100px;
        }
        .flag.MM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2114px;
        }
        .flag.MN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2128px;
        }
        .flag.MO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2142px;
        }
        .flag.MP[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2156px;
        }
        .flag.MQ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2170px;
        }
        .flag.MR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2184px;
        }
        .flag.MS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2198px;
        }
        .flag.MT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2212px;
        }
        .flag.MU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2226px;
        }
        .flag.MV[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2240px;
        }
        .flag.MW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2254px;
        }
        .flag.MX[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2268px;
        }
        .flag.MY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2282px;
        }
        .flag.MZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2296px;
        }
        .flag.NA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2310px;
        }
        .flag.NC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2324px;
        }
        .flag.NE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2338px;
        }
        .flag.NF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2352px;
        }
        .flag.NG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2366px;
        }
        .flag.NI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2380px;
        }
        .flag.NL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2394px;
        }
        .flag.NO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2408px;
        }
        .flag.NP[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2422px;
        }
        .flag.NR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2436px;
        }
        .flag.NU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2450px;
        }
        .flag.NZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2464px;
        }
        .flag.OM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2478px;
        }
        .flag.PA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2492px;
        }
        .flag.PE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2506px;
        }
        .flag.PF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2520px;
        }
        .flag.PG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2534px;
        }
        .flag.PH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2548px;
        }
        .flag.PK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2562px;
        }
        .flag.PL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2576px;
        }
        .flag.PM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2590px;
        }
        .flag.PN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2604px;
        }
        .flag.PR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2618px;
        }
        .flag.PS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2632px;
        }
        .flag.PT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2646px;
        }
        .flag.PW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2660px;
        }
        .flag.PY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2674px;
        }
        .flag.QA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2688px;
        }
        .flag.RE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2702px;
        }
        .flag.RO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2716px;
        }
        .flag.RS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2730px;
        }
        .flag.RU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2744px;
        }
        .flag.RW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2758px;
        }
        .flag.SA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2772px;
        }
        .flag.SB[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2786px;
        }
        .flag.SC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2800px;
        }
        .flag.SD[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2814px;
        }
        .flag.SE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2828px;
        }
        .flag.SG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2842px;
        }
        .flag.SH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2856px;
        }
        .flag.SI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2870px;
        }
        .flag.SJ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2884px;
        }
        .flag.SK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2898px;
        }
        .flag.SL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2912px;
        }
        .flag.SM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2926px;
        }
        .flag.SN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2940px;
        }
        .flag.SO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2954px;
        }
        .flag.SR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2968px;
        }
        .flag.SS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2982px;
        }
        .flag.ST[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -2996px;
        }
        .flag.SV[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3010px;
        }
        .flag.SX[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3024px;
        }
        .flag.SY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3038px;
        }
        .flag.SZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3052px;
        }
        .flag.TA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3066px;
        }
        .flag.TC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3080px;
        }
        .flag.TD[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3094px;
        }
        .flag.TF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3108px;
        }
        .flag.TG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3122px;
        }
        .flag.TH[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3136px;
        }
        .flag.TJ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3150px;
        }
        .flag.TK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3164px;
        }
        .flag.TL[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3178px;
        }
        .flag.TM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3192px;
        }
        .flag.TN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3206px;
        }
        .flag.TO[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3220px;
        }
        .flag.TR[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3234px;
        }
        .flag.TT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3248px;
        }
        .flag.TV[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3262px;
        }
        .flag.TW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3276px;
        }
        .flag.TZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3290px;
        }
        .flag.UA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3304px;
        }
        .flag.UG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3318px;
        }
        .flag.UM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3332px;
        }
        .flag.UN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3346px;
        }
        .flag.US[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3360px;
        }
        .flag.UY[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3374px;
        }
        .flag.UZ[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3388px;
        }
        .flag.VA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3402px;
        }
        .flag.VC[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3416px;
        }
        .flag.VE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3430px;
        }
        .flag.VG[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3444px;
        }
        .flag.VI[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3458px;
        }
        .flag.VN[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3472px;
        }
        .flag.VU[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3486px;
        }
        .flag.WF[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3500px;
        }
        .flag.WS[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3514px;
        }
        .flag.XK[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3528px;
        }
        .flag.YE[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3542px;
        }
        .flag.YT[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3556px;
        }
        .flag.ZA[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3570px;
        }
        .flag.ZM[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3584px;
        }
        .flag.ZW[_ngcontent-serverApp-c4150375230] {
            background-position: 0 -3598px;
        }
    </style>
    <style>
        .mdc-touch-target-wrapper {
            display: inline;
        }
        .mdc-elevation-overlay {
            position: absolute;
            border-radius: inherit;
            pointer-events: none;
            opacity: var(--mdc-elevation-overlay-opacity, 0);
            transition: opacity 280ms cubic-bezier(0.4, 0, 0.2, 1);
        }
        .mdc-button {
            position: relative;
            display: inline-flex;
            align-items: center;
            justify-content: center;
            box-sizing: border-box;
            min-width: 64px;
            border: none;
            outline: none;
            line-height: inherit;
            user-select: none;
            -webkit-appearance: none;
            overflow: visible;
            vertical-align: middle;
            background: rgba(0, 0, 0, 0);
        }
        .mdc-button .mdc-elevation-overlay {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }
        .mdc-button::-moz-focus-inner {
            padding: 0;
            border: 0;
        }
        .mdc-button:active {
            outline: none;
        }
        .mdc-button:hover {
            cursor: pointer;
        }
        .mdc-button:disabled {
            cursor: default;
            pointer-events: none;
        }
        .mdc-button[hidden] {
            display: none;
        }
        .mdc-button .mdc-button__icon {
            margin-left: 0;
            margin-right: 8px;
            display: inline-block;
            position: relative;
            vertical-align: top;
        }
        [dir="rtl"] .mdc-button .mdc-button__icon,
        .mdc-button .mdc-button__icon[dir="rtl"] {
            margin-left: 8px;
            margin-right: 0;
        }
        .mdc-button .mdc-button__progress-indicator {
            font-size: 0;
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            line-height: initial;
        }
        .mdc-button .mdc-button__label {
            position: relative;
        }
        .mdc-button .mdc-button__focus-ring {
            pointer-events: none;
            border: 2px solid rgba(0, 0, 0, 0);
            border-radius: 6px;
            box-sizing: content-box;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            height: calc(100% + 4px);
            width: calc(100% + 4px);
            display: none;
        }
        @media screen and (forced-colors: active) {
            .mdc-button .mdc-button__focus-ring {
                border-color: CanvasText;
            }
        }
        .mdc-button .mdc-button__focus-ring::after {
            content: "";
            border: 2px solid rgba(0, 0, 0, 0);
            border-radius: 8px;
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            height: calc(100% + 4px);
            width: calc(100% + 4px);
        }
        @media screen and (forced-colors: active) {
            .mdc-button .mdc-button__focus-ring::after {
                border-color: CanvasText;
            }
        }
        @media screen and (forced-colors: active) {
            .mdc-button.mdc-ripple-upgraded--background-focused .mdc-button__focus-ring,
            .mdc-button:not(.mdc-ripple-upgraded):focus .mdc-button__focus-ring {
                display: block;
            }
        }
        .mdc-button .mdc-button__touch {
            position: absolute;
            top: 50%;
            height: 48px;
            left: 0;
            right: 0;
            transform: translateY(-50%);
        }
        .mdc-button__label + .mdc-button__icon {
            margin-left: 8px;
            margin-right: 0;
        }
        [dir="rtl"] .mdc-button__label + .mdc-button__icon,
        .mdc-button__label + .mdc-button__icon[dir="rtl"] {
            margin-left: 0;
            margin-right: 8px;
        }
        svg.mdc-button__icon {
            fill: currentColor;
        }
        .mdc-button--touch {
            margin-top: 6px;
            margin-bottom: 6px;
        }
        .mdc-button {
            padding: 0 8px 0 8px;
        }
        .mdc-button--unelevated {
            transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);
            padding: 0 16px 0 16px;
        }
        .mdc-button--unelevated.mdc-button--icon-trailing {
            padding: 0 12px 0 16px;
        }
        .mdc-button--unelevated.mdc-button--icon-leading {
            padding: 0 16px 0 12px;
        }
        .mdc-button--raised {
            transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);
            padding: 0 16px 0 16px;
        }
        .mdc-button--raised.mdc-button--icon-trailing {
            padding: 0 12px 0 16px;
        }
        .mdc-button--raised.mdc-button--icon-leading {
            padding: 0 16px 0 12px;
        }
        .mdc-button--outlined {
            border-style: solid;
            transition: border 280ms cubic-bezier(0.4, 0, 0.2, 1);
        }
        .mdc-button--outlined .mdc-button__ripple {
            border-style: solid;
            border-color: rgba(0, 0, 0, 0);
        }
        .mat-mdc-button {
            height: var(--mdc-text-button-container-height, 36px);
            border-radius: var(--mdc-text-button-container-shape, var(--mdc-shape-small, 4px));
        }
        .mat-mdc-button:not(:disabled) {
            color: var(--mdc-text-button-label-text-color, inherit);
        }
        .mat-mdc-button:disabled {
            color: var(--mdc-text-button-disabled-label-text-color, rgba(0, 0, 0, 0.38));
        }
        .mat-mdc-button .mdc-button__ripple {
            border-radius: var(--mdc-text-button-container-shape, var(--mdc-shape-small, 4px));
        }
        .mat-mdc-unelevated-button {
            height: var(--mdc-filled-button-container-height, 36px);
            border-radius: var(--mdc-filled-button-container-shape, var(--mdc-shape-small, 4px));
        }
        .mat-mdc-unelevated-button:not(:disabled) {
            background-color: var(--mdc-filled-button-container-color, transparent);
        }
        .mat-mdc-unelevated-button:disabled {
            background-color: var(--mdc-filled-button-disabled-container-color, rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-unelevated-button:not(:disabled) {
            color: var(--mdc-filled-button-label-text-color, inherit);
        }
        .mat-mdc-unelevated-button:disabled {
            color: var(--mdc-filled-button-disabled-label-text-color, rgba(0, 0, 0, 0.38));
        }
        .mat-mdc-unelevated-button .mdc-button__ripple {
            border-radius: var(--mdc-filled-button-container-shape, var(--mdc-shape-small, 4px));
        }
        .mat-mdc-raised-button {
            height: var(--mdc-protected-button-container-height, 36px);
            border-radius: var(--mdc-protected-button-container-shape, var(--mdc-shape-small, 4px));
            box-shadow: var(--mdc-protected-button-container-elevation, 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-raised-button:not(:disabled) {
            background-color: var(--mdc-protected-button-container-color, transparent);
        }
        .mat-mdc-raised-button:disabled {
            background-color: var(--mdc-protected-button-disabled-container-color, rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-raised-button:not(:disabled) {
            color: var(--mdc-protected-button-label-text-color, inherit);
        }
        .mat-mdc-raised-button:disabled {
            color: var(--mdc-protected-button-disabled-label-text-color, rgba(0, 0, 0, 0.38));
        }
        .mat-mdc-raised-button .mdc-button__ripple {
            border-radius: var(--mdc-protected-button-container-shape, var(--mdc-shape-small, 4px));
        }
        .mat-mdc-raised-button.mdc-ripple-upgraded--background-focused,
        .mat-mdc-raised-button:not(.mdc-ripple-upgraded):focus {
            box-shadow: var(--mdc-protected-button-focus-container-elevation, 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-raised-button:hover {
            box-shadow: var(--mdc-protected-button-hover-container-elevation, 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-raised-button:not(:disabled):active {
            box-shadow: var(--mdc-protected-button-pressed-container-elevation, 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-raised-button:disabled {
            box-shadow: var(--mdc-protected-button-disabled-container-elevation, 0px 0px 0px 0px rgba(0, 0, 0, 0.2), 0px 0px 0px 0px rgba(0, 0, 0, 0.14), 0px 0px 0px 0px rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-outlined-button {
            height: var(--mdc-outlined-button-container-height, 36px);
            border-radius: var(--mdc-outlined-button-container-shape, var(--mdc-shape-small, 4px));
            padding: 0 15px 0 15px;
            border-width: var(--mdc-outlined-button-outline-width, 1px);
        }
        .mat-mdc-outlined-button:not(:disabled) {
            color: var(--mdc-outlined-button-label-text-color, inherit);
        }
        .mat-mdc-outlined-button:disabled {
            color: var(--mdc-outlined-button-disabled-label-text-color, rgba(0, 0, 0, 0.38));
        }
        .mat-mdc-outlined-button .mdc-button__ripple {
            border-radius: var(--mdc-outlined-button-container-shape, var(--mdc-shape-small, 4px));
        }
        .mat-mdc-outlined-button:not(:disabled) {
            border-color: var(--mdc-outlined-button-outline-color, rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-outlined-button:disabled {
            border-color: var(--mdc-outlined-button-disabled-outline-color, rgba(0, 0, 0, 0.12));
        }
        .mat-mdc-outlined-button.mdc-button--icon-trailing {
            padding: 0 11px 0 15px;
        }
        .mat-mdc-outlined-button.mdc-button--icon-leading {
            padding: 0 15px 0 11px;
        }
        .mat-mdc-outlined-button .mdc-button__ripple {
            top: -1px;
            left: -1px;
            bottom: -1px;
            right: -1px;
            border-width: var(--mdc-outlined-button-outline-width, 1px);
        }
        .mat-mdc-outlined-button .mdc-button__touch {
            left: calc(-1 * var(--mdc-outlined-button-outline-width, 1px));
            width: calc(100% + 2 * var(--mdc-outlined-button-outline-width, 1px));
        }
        .mat-mdc-button,
        .mat-mdc-unelevated-button,
        .mat-mdc-raised-button,
        .mat-mdc-outlined-button {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }
        .mat-mdc-button .mat-mdc-button-ripple,
        .mat-mdc-button .mat-mdc-button-persistent-ripple,
        .mat-mdc-button .mat-mdc-button-persistent-ripple::before,
        .mat-mdc-unelevated-button .mat-mdc-button-ripple,
        .mat-mdc-unelevated-button .mat-mdc-button-persistent-ripple,
        .mat-mdc-unelevated-button .mat-mdc-button-persistent-ripple::before,
        .mat-mdc-raised-button .mat-mdc-button-ripple,
        .mat-mdc-raised-button .mat-mdc-button-persistent-ripple,
        .mat-mdc-raised-button .mat-mdc-button-persistent-ripple::before,
        .mat-mdc-outlined-button .mat-mdc-button-ripple,
        .mat-mdc-outlined-button .mat-mdc-button-persistent-ripple,
        .mat-mdc-outlined-button .mat-mdc-button-persistent-ripple::before {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            pointer-events: none;
            border-radius: inherit;
        }
        .mat-mdc-button .mat-mdc-button-ripple,
        .mat-mdc-unelevated-button .mat-mdc-button-ripple,
        .mat-mdc-raised-button .mat-mdc-button-ripple,
        .mat-mdc-outlined-button .mat-mdc-button-ripple {
            overflow: hidden;
        }
        .mat-mdc-button .mat-mdc-button-persistent-ripple::before,
        .mat-mdc-unelevated-button .mat-mdc-button-persistent-ripple::before,
        .mat-mdc-raised-button .mat-mdc-button-persistent-ripple::before,
        .mat-mdc-outlined-button .mat-mdc-button-persistent-ripple::before {
            content: "";
            opacity: 0;
            background-color: var(--mat-mdc-button-persistent-ripple-color);
        }
        .mat-mdc-button .mat-ripple-element,
        .mat-mdc-unelevated-button .mat-ripple-element,
        .mat-mdc-raised-button .mat-ripple-element,
        .mat-mdc-outlined-button .mat-ripple-element {
            background-color: var(--mat-mdc-button-ripple-color);
        }
        .mat-mdc-button .mdc-button__label,
        .mat-mdc-unelevated-button .mdc-button__label,
        .mat-mdc-raised-button .mdc-button__label,
        .mat-mdc-outlined-button .mdc-button__label {
            z-index: 1;
        }
        .mat-mdc-button .mat-mdc-focus-indicator,
        .mat-mdc-unelevated-button .mat-mdc-focus-indicator,
        .mat-mdc-raised-button .mat-mdc-focus-indicator,
        .mat-mdc-outlined-button .mat-mdc-focus-indicator {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
        }
        .mat-mdc-button:focus .mat-mdc-focus-indicator::before,
        .mat-mdc-unelevated-button:focus .mat-mdc-focus-indicator::before,
        .mat-mdc-raised-button:focus .mat-mdc-focus-indicator::before,
        .mat-mdc-outlined-button:focus .mat-mdc-focus-indicator::before {
            content: "";
        }
        .mat-mdc-button[disabled],
        .mat-mdc-unelevated-button[disabled],
        .mat-mdc-raised-button[disabled],
        .mat-mdc-outlined-button[disabled] {
            cursor: default;
            pointer-events: none;
        }
        .mat-mdc-button .mat-mdc-button-touch-target,
        .mat-mdc-unelevated-button .mat-mdc-button-touch-target,
        .mat-mdc-raised-button .mat-mdc-button-touch-target,
        .mat-mdc-outlined-button .mat-mdc-button-touch-target {
            position: absolute;
            top: 50%;
            height: 48px;
            left: 0;
            right: 0;
            transform: translateY(-50%);
        }
        .mat-mdc-button._mat-animation-noopable,
        .mat-mdc-unelevated-button._mat-animation-noopable,
        .mat-mdc-raised-button._mat-animation-noopable,
        .mat-mdc-outlined-button._mat-animation-noopable {
            transition: none !important;
            animation: none !important;
        }
        .mat-mdc-button > .mat-icon {
            margin-left: 0;
            margin-right: 8px;
            display: inline-block;
            position: relative;
            vertical-align: top;
            font-size: 1.125rem;
            height: 1.125rem;
            width: 1.125rem;
        }
        [dir="rtl"] .mat-mdc-button > .mat-icon,
        .mat-mdc-button > .mat-icon[dir="rtl"] {
            margin-left: 8px;
            margin-right: 0;
        }
        .mat-mdc-button .mdc-button__label + .mat-icon {
            margin-left: 8px;
            margin-right: 0;
        }
        [dir="rtl"] .mat-mdc-button .mdc-button__label + .mat-icon,
        .mat-mdc-button .mdc-button__label + .mat-icon[dir="rtl"] {
            margin-left: 0;
            margin-right: 8px;
        }
        .mat-mdc-unelevated-button > .mat-icon,
        .mat-mdc-raised-button > .mat-icon,
        .mat-mdc-outlined-button > .mat-icon {
            margin-left: 0;
            margin-right: 8px;
            display: inline-block;
            position: relative;
            vertical-align: top;
            font-size: 1.125rem;
            height: 1.125rem;
            width: 1.125rem;
            margin-left: -4px;
            margin-right: 8px;
        }
        [dir="rtl"] .mat-mdc-unelevated-button > .mat-icon,
        [dir="rtl"] .mat-mdc-raised-button > .mat-icon,
        [dir="rtl"] .mat-mdc-outlined-button > .mat-icon,
        .mat-mdc-unelevated-button > .mat-icon[dir="rtl"],
        .mat-mdc-raised-button > .mat-icon[dir="rtl"],
        .mat-mdc-outlined-button > .mat-icon[dir="rtl"] {
            margin-left: 8px;
            margin-right: 0;
        }
        [dir="rtl"] .mat-mdc-unelevated-button > .mat-icon,
        [dir="rtl"] .mat-mdc-raised-button > .mat-icon,
        [dir="rtl"] .mat-mdc-outlined-button > .mat-icon,
        .mat-mdc-unelevated-button > .mat-icon[dir="rtl"],
        .mat-mdc-raised-button > .mat-icon[dir="rtl"],
        .mat-mdc-outlined-button > .mat-icon[dir="rtl"] {
            margin-left: 8px;
            margin-right: -4px;
        }
        .mat-mdc-unelevated-button .mdc-button__label + .mat-icon,
        .mat-mdc-raised-button .mdc-button__label + .mat-icon,
        .mat-mdc-outlined-button .mdc-button__label + .mat-icon {
            margin-left: 8px;
            margin-right: -4px;
        }
        [dir="rtl"] .mat-mdc-unelevated-button .mdc-button__label + .mat-icon,
        [dir="rtl"] .mat-mdc-raised-button .mdc-button__label + .mat-icon,
        [dir="rtl"] .mat-mdc-outlined-button .mdc-button__label + .mat-icon,
        .mat-mdc-unelevated-button .mdc-button__label + .mat-icon[dir="rtl"],
        .mat-mdc-raised-button .mdc-button__label + .mat-icon[dir="rtl"],
        .mat-mdc-outlined-button .mdc-button__label + .mat-icon[dir="rtl"] {
            margin-left: -4px;
            margin-right: 8px;
        }
        .mat-mdc-outlined-button .mat-mdc-button-ripple,
        .mat-mdc-outlined-button .mdc-button__ripple {
            top: -1px;
            left: -1px;
            bottom: -1px;
            right: -1px;
            border-width: -1px;
        }
        .mat-mdc-unelevated-button .mat-mdc-focus-indicator::before,
        .mat-mdc-raised-button .mat-mdc-focus-indicator::before {
            margin: calc(calc(var(--mat-mdc-focus-indicator-border-width, 3px) + 2px) * -1);
        }
        .mat-mdc-outlined-button .mat-mdc-focus-indicator::before {
            margin: calc(calc(var(--mat-mdc-focus-indicator-border-width, 3px) + 3px) * -1);
        }
    </style>
    <style>
        .cdk-high-contrast-active .mat-mdc-button:not(.mdc-button--outlined),
        .cdk-high-contrast-active .mat-mdc-unelevated-button:not(.mdc-button--outlined),
        .cdk-high-contrast-active .mat-mdc-raised-button:not(.mdc-button--outlined),
        .cdk-high-contrast-active .mat-mdc-outlined-button:not(.mdc-button--outlined),
        .cdk-high-contrast-active .mat-mdc-icon-button {
            outline: solid 1px;
        }
    </style>
    <style>
        mat-menu {
            display: none;
        }
        .mat-mdc-menu-content {
            margin: 0;
            padding: 8px 0;
            list-style-type: none;
        }
        .mat-mdc-menu-content:focus {
            outline: none;
        }
        .mat-mdc-menu-content,
        .mat-mdc-menu-content .mat-mdc-menu-item .mat-mdc-menu-item-text {
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            white-space: normal;
            font-family: var(--mat-menu-item-label-text-font);
            line-height: var(--mat-menu-item-label-text-line-height);
            font-size: var(--mat-menu-item-label-text-size);
            letter-spacing: var(--mat-menu-item-label-text-tracking);
            font-weight: var(--mat-menu-item-label-text-weight);
        }
        .mat-mdc-menu-panel {
            --mat-menu-container-shape: 4px;
            min-width: 112px;
            max-width: 280px;
            overflow: auto;
            -webkit-overflow-scrolling: touch;
            box-sizing: border-box;
            outline: 0;
            border-radius: var(--mat-menu-container-shape);
            background-color: var(--mat-menu-container-color);
            will-change: transform, opacity;
        }
        .mat-mdc-menu-panel.ng-animating {
            pointer-events: none;
        }
        .cdk-high-contrast-active .mat-mdc-menu-panel {
            outline: solid 1px;
        }
        .mat-mdc-menu-item {
            display: flex;
            position: relative;
            align-items: center;
            justify-content: flex-start;
            overflow: hidden;
            padding: 0;
            padding-left: 16px;
            padding-right: 16px;
            -webkit-user-select: none;
            user-select: none;
            cursor: pointer;
            outline: none;
            border: none;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            cursor: pointer;
            width: 100%;
            text-align: left;
            box-sizing: border-box;
            color: inherit;
            font-size: inherit;
            background: none;
            text-decoration: none;
            margin: 0;
            align-items: center;
            min-height: 48px;
        }
        .mat-mdc-menu-item:focus {
            outline: none;
        }
        [dir="rtl"] .mat-mdc-menu-item,
        .mat-mdc-menu-item[dir="rtl"] {
            padding-left: 16px;
            padding-right: 16px;
        }
        .mat-mdc-menu-item::-moz-focus-inner {
            border: 0;
        }
        .mat-mdc-menu-item,
        .mat-mdc-menu-item:visited,
        .mat-mdc-menu-item:link {
            color: var(--mat-menu-item-label-text-color);
        }
        .mat-mdc-menu-item .mat-icon-no-color,
        .mat-mdc-menu-item .mat-mdc-menu-submenu-icon {
            color: var(--mat-menu-item-icon-color);
        }
        .mat-mdc-menu-item[disabled] {
            cursor: default;
            opacity: 0.38;
        }
        .mat-mdc-menu-item[disabled]::after {
            display: block;
            position: absolute;
            content: "";
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        .mat-mdc-menu-item .mat-icon {
            margin-right: 16px;
        }
        [dir="rtl"] .mat-mdc-menu-item {
            text-align: right;
        }
        [dir="rtl"] .mat-mdc-menu-item .mat-icon {
            margin-right: 0;
            margin-left: 16px;
        }
        .mat-mdc-menu-item.mat-mdc-menu-item-submenu-trigger {
            padding-right: 32px;
        }
        [dir="rtl"] .mat-mdc-menu-item.mat-mdc-menu-item-submenu-trigger {
            padding-right: 16px;
            padding-left: 32px;
        }
        .mat-mdc-menu-item:not([disabled]):hover {
            background-color: var(--mat-menu-item-hover-state-layer-color);
        }
        .mat-mdc-menu-item:not([disabled]).cdk-program-focused,
        .mat-mdc-menu-item:not([disabled]).cdk-keyboard-focused,
        .mat-mdc-menu-item:not([disabled]).mat-mdc-menu-item-highlighted {
            background-color: var(--mat-menu-item-focus-state-layer-color);
        }
        .cdk-high-contrast-active .mat-mdc-menu-item {
            margin-top: 1px;
        }
        .mat-mdc-menu-submenu-icon {
            position: absolute;
            top: 50%;
            right: 16px;
            transform: translateY(-50%);
            width: 5px;
            height: 10px;
            fill: currentColor;
        }
        [dir="rtl"] .mat-mdc-menu-submenu-icon {
            right: auto;
            left: 16px;
            transform: translateY(-50%) scaleX(-1);
        }
        .cdk-high-contrast-active .mat-mdc-menu-submenu-icon {
            fill: CanvasText;
        }
        .mat-mdc-menu-item .mat-mdc-menu-ripple {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            pointer-events: none;
        }
    </style>
    <style>
        .country-search-dropdown[_ngcontent-serverApp-c146864430] .mdc-text-field__input[_ngcontent-serverApp-c146864430] {
            margin-left: 22px !important;
            margin-top: -24px !important;
            display: flex !important;
        }
        .mat-form-field-infix {
            display: flex !important;
        }
        .icon-position[_ngcontent-serverApp-c146864430] {
            position: relative;
            top: -1px;
            left: -5px;
        }
        .arrow-icon[_ngcontent-serverApp-c146864430] {
            position: absolute;
            top: 17px;
            right: 0;
        }
    </style>
    <style>
        div.mat-mdc-autocomplete-panel {
            box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12);
            width: 100%;
            max-height: 256px;
            visibility: hidden;
            transform-origin: center top;
            overflow: auto;
            padding: 8px 0;
            border-radius: 4px;
            box-sizing: border-box;
            position: static;
            background-color: var(--mat-autocomplete-background-color);
        }
        .cdk-high-contrast-active div.mat-mdc-autocomplete-panel {
            outline: solid 1px;
        }
        .cdk-overlay-pane:not(.mat-mdc-autocomplete-panel-above) div.mat-mdc-autocomplete-panel {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .mat-mdc-autocomplete-panel-above div.mat-mdc-autocomplete-panel {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
            transform-origin: center bottom;
        }
        div.mat-mdc-autocomplete-panel.mat-mdc-autocomplete-visible {
            visibility: visible;
        }
        div.mat-mdc-autocomplete-panel.mat-mdc-autocomplete-hidden {
            visibility: hidden;
        }
        mat-autocomplete {
            display: none;
        }
    </style>
    <style>
        .mat-divider {
            --mat-divider-width: 1px;
            display: block;
            margin: 0;
            border-top-style: solid;
            border-top-color: var(--mat-divider-color);
            border-top-width: var(--mat-divider-width);
        }
        .mat-divider.mat-divider-vertical {
            border-top: 0;
            border-right-style: solid;
            border-right-color: var(--mat-divider-color);
            border-right-width: var(--mat-divider-width);
        }
        .mat-divider.mat-divider-inset {
            margin-left: 80px;
        }
        [dir="rtl"] .mat-divider.mat-divider-inset {
            margin-left: auto;
            margin-right: 80px;
        }
    </style>
    <style>
        .mat-mdc-option {
            display: flex;
            position: relative;
            align-items: center;
            justify-content: flex-start;
            overflow: hidden;
            padding: 0;
            padding-left: 16px;
            padding-right: 16px;
            -webkit-user-select: none;
            user-select: none;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            cursor: pointer;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            color: var(--mat-option-label-text-color);
            font-family: var(--mat-option-label-text-font);
            line-height: var(--mat-option-label-text-line-height);
            font-size: var(--mat-option-label-text-size);
            letter-spacing: var(--mat-option-label-text-tracking);
            font-weight: var(--mat-option-label-text-weight);
            min-height: 48px;
        }
        .mat-mdc-option:focus {
            outline: none;
        }
        [dir="rtl"] .mat-mdc-option,
        .mat-mdc-option[dir="rtl"] {
            padding-left: 16px;
            padding-right: 16px;
        }
        .mat-mdc-option:hover:not(.mdc-list-item--disabled) {
            background-color: var(--mat-option-hover-state-layer-color);
        }
        .mat-mdc-option:focus.mdc-list-item,
        .mat-mdc-option.mat-mdc-option-active.mdc-list-item {
            background-color: var(--mat-option-focus-state-layer-color);
        }
        .mat-mdc-option.mdc-list-item--selected:not(.mdc-list-item--disabled) .mdc-list-item__primary-text {
            color: var(--mat-option-selected-state-label-text-color);
        }
        .mat-mdc-option.mdc-list-item--selected:not(.mdc-list-item--disabled):not(.mat-mdc-option-multiple) {
            background-color: var(--mat-option-selected-state-layer-color);
        }
        .mat-mdc-option.mdc-list-item {
            align-items: center;
        }
        .mat-mdc-option.mdc-list-item--disabled {
            cursor: default;
            pointer-events: none;
        }
        .mat-mdc-option.mdc-list-item--disabled .mat-mdc-option-pseudo-checkbox,
        .mat-mdc-option.mdc-list-item--disabled .mdc-list-item__primary-text,
        .mat-mdc-option.mdc-list-item--disabled > mat-icon {
            opacity: 0.38;
        }
        .mat-mdc-optgroup .mat-mdc-option:not(.mat-mdc-option-multiple) {
            padding-left: 32px;
        }
        [dir="rtl"] .mat-mdc-optgroup .mat-mdc-option:not(.mat-mdc-option-multiple) {
            padding-left: 16px;
            padding-right: 32px;
        }
        .mat-mdc-option .mat-icon,
        .mat-mdc-option .mat-pseudo-checkbox-full {
            margin-right: 16px;
            flex-shrink: 0;
        }
        [dir="rtl"] .mat-mdc-option .mat-icon,
        [dir="rtl"] .mat-mdc-option .mat-pseudo-checkbox-full {
            margin-right: 0;
            margin-left: 16px;
        }
        .mat-mdc-option .mat-pseudo-checkbox-minimal {
            margin-left: 16px;
            flex-shrink: 0;
        }
        [dir="rtl"] .mat-mdc-option .mat-pseudo-checkbox-minimal {
            margin-right: 16px;
            margin-left: 0;
        }
        .mat-mdc-option .mat-mdc-option-ripple {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            pointer-events: none;
        }
        .mat-mdc-option .mdc-list-item__primary-text {
            white-space: normal;
            font-size: inherit;
            font-weight: inherit;
            letter-spacing: inherit;
            line-height: inherit;
            font-family: inherit;
            text-decoration: inherit;
            text-transform: inherit;
            margin-right: auto;
        }
        [dir="rtl"] .mat-mdc-option .mdc-list-item__primary-text {
            margin-right: 0;
            margin-left: auto;
        }
        .cdk-high-contrast-active .mat-mdc-option.mdc-list-item--selected:not(.mat-mdc-option-multiple)::after {
            content: "";
            position: absolute;
            top: 50%;
            right: 16px;
            transform: translateY(-50%);
            width: 10px;
            height: 0;
            border-bottom: solid 10px;
            border-radius: 10px;
        }
        [dir="rtl"] .cdk-high-contrast-active .mat-mdc-option.mdc-list-item--selected:not(.mat-mdc-option-multiple)::after {
            right: auto;
            left: 16px;
        }
        .mat-mdc-option-active .mat-mdc-focus-indicator::before {
            content: "";
        }
    </style>
    <style>
        .mat-mdc-select {
            display: inline-block;
            width: 100%;
            outline: none;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            color: var(--mat-select-enabled-trigger-text-color);
            font-family: var(--mat-select-trigger-text-font);
            line-height: var(--mat-select-trigger-text-line-height);
            font-size: var(--mat-select-trigger-text-size);
            font-weight: var(--mat-select-trigger-text-weight);
            letter-spacing: var(--mat-select-trigger-text-tracking);
        }
        .mat-mdc-select-disabled {
            color: var(--mat-select-disabled-trigger-text-color);
        }
        .mat-mdc-select-trigger {
            display: inline-flex;
            align-items: center;
            cursor: pointer;
            position: relative;
            box-sizing: border-box;
            width: 100%;
        }
        .mat-mdc-select-disabled .mat-mdc-select-trigger {
            -webkit-user-select: none;
            user-select: none;
            cursor: default;
        }
        .mat-mdc-select-value {
            width: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .mat-mdc-select-value-text {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .mat-mdc-select-arrow-wrapper {
            height: 24px;
            flex-shrink: 0;
            display: inline-flex;
            align-items: center;
        }
        .mat-form-field-appearance-fill .mat-mdc-select-arrow-wrapper {
            transform: translateY(-8px);
        }
        .mat-form-field-appearance-fill .mdc-text-field--no-label .mat-mdc-select-arrow-wrapper {
            transform: none;
        }
        .mat-mdc-select-arrow {
            width: 10px;
            height: 5px;
            position: relative;
            color: var(--mat-select-enabled-arrow-color);
        }
        .mat-mdc-form-field.mat-focused .mat-mdc-select-arrow {
            color: var(--mat-select-focused-arrow-color);
        }
        .mat-mdc-form-field .mat-mdc-select.mat-mdc-select-invalid .mat-mdc-select-arrow {
            color: var(--mat-select-invalid-arrow-color);
        }
        .mat-mdc-form-field .mat-mdc-select.mat-mdc-select-disabled .mat-mdc-select-arrow {
            color: var(--mat-select-disabled-arrow-color);
        }
        .mat-mdc-select-arrow svg {
            fill: currentColor;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .cdk-high-contrast-active .mat-mdc-select-arrow svg {
            fill: CanvasText;
        }
        .mat-mdc-select-disabled .cdk-high-contrast-active .mat-mdc-select-arrow svg {
            fill: GrayText;
        }
        div.mat-mdc-select-panel {
            box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12);
            width: 100%;
            max-height: 275px;
            outline: 0;
            overflow: auto;
            padding: 8px 0;
            border-radius: 4px;
            box-sizing: border-box;
            position: static;
            background-color: var(--mat-select-panel-background-color);
        }
        .cdk-high-contrast-active div.mat-mdc-select-panel {
            outline: solid 1px;
        }
        .cdk-overlay-pane:not(.mat-mdc-select-panel-above) div.mat-mdc-select-panel {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            transform-origin: top center;
        }
        .mat-mdc-select-panel-above div.mat-mdc-select-panel {
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
            transform-origin: bottom center;
        }
        .mat-mdc-select-placeholder {
            transition: color 400ms 133.3333333333ms cubic-bezier(0.25, 0.8, 0.25, 1);
            color: var(--mat-select-placeholder-text-color);
        }
        ._mat-animation-noopable .mat-mdc-select-placeholder {
            transition: none;
        }
        .mat-form-field-hide-placeholder .mat-mdc-select-placeholder {
            color: rgba(0, 0, 0, 0);
            -webkit-text-fill-color: rgba(0, 0, 0, 0);
            transition: none;
            display: block;
        }
        .mat-mdc-form-field-type-mat-select:not(.mat-form-field-disabled) .mat-mdc-text-field-wrapper {
            cursor: pointer;
        }
        .mat-mdc-form-field-type-mat-select.mat-form-field-appearance-fill .mat-mdc-floating-label {
            max-width: calc(100% - 18px);
        }
        .mat-mdc-form-field-type-mat-select.mat-form-field-appearance-fill .mdc-floating-label--float-above {
            max-width: calc(100% / 0.75 - 24px);
        }
        .mat-mdc-form-field-type-mat-select.mat-form-field-appearance-outline .mdc-notched-outline__notch {
            max-width: calc(100% - 60px);
        }
        .mat-mdc-form-field-type-mat-select.mat-form-field-appearance-outline .mdc-text-field--label-floating .mdc-notched-outline__notch {
            max-width: calc(100% - 24px);
        }
        .mat-mdc-select-min-line:empty::before {
            content: " ";
            white-space: pre;
            width: 1px;
            display: inline-block;
            visibility: hidden;
        }
    </style>
    <style>
        .mat-pseudo-checkbox {
            border-radius: 2px;
            cursor: pointer;
            display: inline-block;
            vertical-align: middle;
            box-sizing: border-box;
            position: relative;
            flex-shrink: 0;
            transition: border-color 90ms cubic-bezier(0, 0, 0.2, 0.1), background-color 90ms cubic-bezier(0, 0, 0.2, 0.1);
        }
        .mat-pseudo-checkbox::after {
            position: absolute;
            opacity: 0;
            content: "";
            border-bottom: 2px solid currentColor;
            transition: opacity 90ms cubic-bezier(0, 0, 0.2, 0.1);
        }
        .mat-pseudo-checkbox._mat-animation-noopable {
            transition: none !important;
            animation: none !important;
        }
        .mat-pseudo-checkbox._mat-animation-noopable::after {
            transition: none;
        }
        .mat-pseudo-checkbox-disabled {
            cursor: default;
        }
        .mat-pseudo-checkbox-indeterminate::after {
            left: 1px;
            opacity: 1;
            border-radius: 2px;
        }
        .mat-pseudo-checkbox-checked::after {
            left: 1px;
            border-left: 2px solid currentColor;
            transform: rotate(-45deg);
            opacity: 1;
            box-sizing: content-box;
        }
        .mat-pseudo-checkbox-full {
            border: 2px solid;
        }
        .mat-pseudo-checkbox-full.mat-pseudo-checkbox-checked,
        .mat-pseudo-checkbox-full.mat-pseudo-checkbox-indeterminate {
            border-color: rgba(0, 0, 0, 0);
        }
        .mat-pseudo-checkbox {
            width: 18px;
            height: 18px;
        }
        .mat-pseudo-checkbox-minimal.mat-pseudo-checkbox-checked::after {
            width: 14px;
            height: 6px;
            transform-origin: center;
            top: -4.2426406871px;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
        }
        .mat-pseudo-checkbox-minimal.mat-pseudo-checkbox-indeterminate::after {
            top: 8px;
            width: 16px;
        }
        .mat-pseudo-checkbox-full.mat-pseudo-checkbox-checked::after {
            width: 10px;
            height: 4px;
            transform-origin: center;
            top: -2.8284271247px;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
        }
        .mat-pseudo-checkbox-full.mat-pseudo-checkbox-indeterminate::after {
            top: 6px;
            width: 12px;
        }
    </style>
@endsection
@section('content')
    <style>
        .dashboard-ar{
            margin-top: 97px;
        }
        .ff a {
            display: inline-block !important;
        }
        .bg-biodata{
            background-color: #000000 !important;
            color: white;
            padding-top: 8px !important;
        }
        .bg-biodata:hover{
            color: white !important;
        }
        .complete-profile{
            background-color: #B50000 !important;
            padding-top: 8px !important;
            color: white;
        }
        .complete-profile:hover{
            color: white !important;
        }
        .search-job{
            background-color: #FFC107;
        }
    </style>
    <section _ngcontent-serverapp-c1262338835="" class="page-section dashboard-ar desktop-responsive">
        <section _ngcontent-serverapp-c1262338835="" class="agency breadcrumb-section-main inner-3" style="background: #f8f8f8 !important;">
            <div _ngcontent-serverapp-c1262338835="" class="container px-2 py-4">
                <div _ngcontent-serverapp-c1262338835="" class="row px-3">
                    <div _ngcontent-serverapp-c1262338835="" class="col-12">
                        <div _ngcontent-serverapp-c1262338835="" class="breadcrumb-contain">
                            <div _ngcontent-serverapp-c1262338835="" class="text-left">
                                <ul _ngcontent-serverapp-c1262338835="" class="text-left ar">
                                    <li _ngcontent-serverapp-c1262338835="">
                                        <a _ngcontent-serverapp-c1262338835="" routerlink="/" href="/"><i _ngcontent-serverapp-c1262338835="" class="fa fa-home"></i></a>
                                    </li>
                                    <li _ngcontent-serverapp-c1262338835="">
                                        <a _ngcontent-serverapp-c1262338835=""><i _ngcontent-serverapp-c1262338835="" class="fa fa-angle-double-right"></i> Dashboard</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section _ngcontent-serverapp-c1262338835="" class="agency blog blog-sec blog-sidebar mt-3 pb-5 blog-list sider Custom-Dashboard ng-star-inserted">
            <div _ngcontent-serverapp-c1262338835="" class="container">
                <div _ngcontent-serverapp-c1262338835="" class="row">
                    <div _ngcontent-serverapp-c1262338835="" class="col-grid-box w-100 col-12 ar">
                        <!----><!----><!----><!----><!----><!----><!----><!----><!----><!---->
                        @if($transaction)
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{\Illuminate\Support\Facades\Auth::user()->name}} !</strong> Congrates for subcribe <b>{{$transaction->helper_plan->name}}</b>
                            </div>
                        @else
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>{{\Illuminate\Support\Facades\Auth::user()->name}} !</strong> Please subscribe to our <a href="{{route('helper_packages')}}" class="btn-link">packages</a> for list your profile
                            </div>
                        @endif

                        <div _ngcontent-serverapp-c1262338835="" class="product-box ng-star-inserted">
                            <div _ngcontent-serverapp-c1262338835="" class="product-detail pl-0 p-0 w-100">
                                <div _ngcontent-serverapp-c1262338835="" class="row px-2 mb-2 ml-0 mr-0">
                                    <div _ngcontent-serverapp-c1262338835="" class="col-12 mb-3">
                                        <h2 _ngcontent-serverapp-c1262338835="" class="main-title">Complete your profile to freely connect with all our employers.</h2>
                                        <div _ngcontent-serverapp-c1262338835="" class="custom-job-action mt-3">
                                            <style type="text/css">

                                            </style>
                                            <div _ngcontent-serverapp-c1262338835="" class="custom-job-action-detail ff">
                                                <a href="{{route('candidate_profile')}}" class="btn custom-transperant-button complete-profile" >Complete My Profile</a>
                                                <a href="{{route('bio-data')}}" class="btn custom-transperant-button bg-biodata btn-sm">Update Biodata</a>
                                                <a href="{{route('jobs')}}" target="_blank" class="btn custom-transperant-button search-job" style="padding-top: 8px;color: white;">Search Job</a>
                                                
                                                <a href="{{route('helper_packages')}}"  class="btn custom-transperant-button search-job" style="padding-top: 8px;color: white; background-color:#1E3F66">SUBSCRIBE</a>
                                                
                                                <a href="{{route('resume_detail',\Illuminate\Support\Facades\Auth::user())}}" target="_blank"  class="btn btn-primary" style="padding-top: 8px;color: white; background: #054a84;float: right">
                                                    <i class="fa fa-eye"></i>
                                                
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!----><!----><!---->
                    </div>
                    <div _ngcontent-serverapp-c1262338835="" class="row mt-3 listing-block">
                        <!----><!----><!----><!---->
                        <div _ngcontent-serverapp-c1262338835="" class="col-12 col-md-6 col-lg-6 col-xl-4 mb-3 info-block ng-star-inserted">
                            <div _ngcontent-serverapp-c1262338835="" class="custom-dashboard-widget">
                                <div _ngcontent-serverapp-c1262338835="" class="widget-header">
                                    <h2 _ngcontent-serverapp-c1262338835=""><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-calendar"></i>Upcoming Events</h2>
                                </div>
                                <div _ngcontent-serverapp-c1262338835="" class="widget-body">
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer border-bottom ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image date">
                                            Jun<br _ngcontent-serverapp-c1262338835="" />
                                            19
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail">
                                            <h3 _ngcontent-serverapp-c1262338835="">First Aid, CPR and Use of AED</h3>
                                            <!---->
                                            <h5 _ngcontent-serverapp-c1262338835="" class="widget-item-location ng-star-inserted"><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-map-marker dark"></i> Hong Kong</h5>
                                            <!---->
                                        </div>
                                    </div>
                                    <!---->
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-action"><button _ngcontent-serverapp-c1262338835="" class="btn custom-transperant-button">See All</button></div>
                                </div>
                            </div>
                        </div>
                        <!---->
                        <div _ngcontent-serverapp-c1262338835="" class="col-12 col-md-6 col-lg-6 col-xl-4 mb-3 info-block ng-star-inserted">
                            <div _ngcontent-serverapp-c1262338835="" class="custom-dashboard-widget">
                                <div _ngcontent-serverapp-c1262338835="" class="widget-header">
                                    <h2 _ngcontent-serverapp-c1262338835=""><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-calendar-times-o"></i>Upcoming Holiday</h2>
                                </div>
                                <div _ngcontent-serverapp-c1262338835="" class="widget-body">
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item border-bottom ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image date">
                                            Jun<br _ngcontent-serverapp-c1262338835="" />
                                            10
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail single-line"><h3 _ngcontent-serverapp-c1262338835="">Dragon Boat Festival</h3></div>
                                    </div>
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image date">
                                            Jun<br _ngcontent-serverapp-c1262338835="" />
                                            16
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail single-line"><h3 _ngcontent-serverapp-c1262338835="">Father's Day</h3></div>
                                    </div>
                                    <!---->
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-action"><button _ngcontent-serverapp-c1262338835="" class="btn custom-transperant-button">See All</button></div>
                                </div>
                            </div>
                        </div>
                        <!----><!---->
                        <div _ngcontent-serverapp-c1262338835="" class="col-12 col-md-6 col-lg-6 col-xl-4 mb-3 info-block ng-star-inserted">
                            <div _ngcontent-serverapp-c1262338835="" class="custom-dashboard-widget">
                                <div _ngcontent-serverapp-c1262338835="" class="widget-header">
                                    <h2 _ngcontent-serverapp-c1262338835=""><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-newspaper-o"></i>Tips &amp; News</h2>
                                </div>
                                <div _ngcontent-serverapp-c1262338835="" class="widget-body">
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer border-bottom ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail details">
                                            <h3 _ngcontent-serverapp-c1262338835="">Babysitter vs. Helper: Nurturing Your Child's Development</h3>
                                            <p _ngcontent-serverapp-c1262338835="">Discover the essential factors influencing the choice between a full-time helper and a babysitter for childcare. Pick the best one for your child!</p>
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail details">
                                            <h3 _ngcontent-serverapp-c1262338835="">Home Leave for Hong Kong's Helpers: Requirements and Practices</h3>
                                            <p _ngcontent-serverapp-c1262338835="">Is it time for your FDH to return to the Philippines for vacation? Learn about domestic helper home leave entitlement and practices in Hong Kong</p>
                                        </div>
                                    </div>
                                    <!----><!---->
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-action"><button _ngcontent-serverapp-c1262338835="" class="btn custom-transperant-button">See All</button></div>
                                </div>
                            </div>
                        </div>
                        <!---->
                        <div _ngcontent-serverapp-c1262338835="" class="col-12 col-md-6 col-lg-6 col-xl-4 mb-3 info-block ng-star-inserted">
                            <div _ngcontent-serverapp-c1262338835="" class="custom-dashboard-widget">
                                <div _ngcontent-serverapp-c1262338835="" class="widget-header">
                                    <h2 _ngcontent-serverapp-c1262338835=""><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-address-card"></i>Agencies Support</h2>
                                </div>
                                <div _ngcontent-serverapp-c1262338835="" class="widget-body">
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer border-bottom ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image img">
                                            <img _ngcontent-serverapp-c1262338835="" alt="Agency Support by helperplace" title="helperplace agency support" width="100%" height="auto" src="https://cdn.helperplace.com/a_logo/14.jpg" />
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail">
                                            <h3 _ngcontent-serverapp-c1262338835="">HKO ARIGATOO LIMITED</h3>
                                            <h5 _ngcontent-serverapp-c1262338835="" class="widget-item-location"><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-map-marker dark"></i>Hong Kong</h5>
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image img">
                                            <img _ngcontent-serverapp-c1262338835="" alt="Agency Support by helperplace" title="helperplace agency support" width="100%" height="auto" src="https://cdn.helperplace.com/a_logo/21.png" />
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail">
                                            <h3 _ngcontent-serverapp-c1262338835="">Fair Employment Agency Limited</h3>
                                            <h5 _ngcontent-serverapp-c1262338835="" class="widget-item-location"><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-map-marker dark"></i>Hong Kong</h5>
                                        </div>
                                    </div>
                                    <!----><!---->
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-action"><button _ngcontent-serverapp-c1262338835="" class="btn custom-transperant-button">See All</button></div>
                                </div>
                            </div>
                        </div>
                        <!---->
                        <div _ngcontent-serverapp-c1262338835="" class="col-12 col-md-6 col-lg-6 col-xl-4 mb-3 info-block ng-star-inserted">
                            <div _ngcontent-serverapp-c1262338835="" class="custom-dashboard-widget">
                                <div _ngcontent-serverapp-c1262338835="" class="widget-header">
                                    <h2 _ngcontent-serverapp-c1262338835=""><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-book"></i>Training Offer</h2>
                                </div>
                                <div _ngcontent-serverapp-c1262338835="" class="widget-body">
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer border-bottom ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image img">
                                            <img
                                                _ngcontent-serverapp-c1262338835=""
                                                alt="Training Provided by helperplace agency"
                                                title="Training Offer by helperplace agency"
                                                width="100%"
                                                height="auto"
                                                src="https://cdn.helperplace.com/a_logo/12.png"
                                            />
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail">
                                            <h3 _ngcontent-serverapp-c1262338835="">CPR and First Aid course</h3>
                                            <h5 _ngcontent-serverapp-c1262338835="" class="widget-item-location"><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-map-marker dark"></i>Hong Kong</h5>
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image img">
                                            <img
                                                _ngcontent-serverapp-c1262338835=""
                                                alt="Training Provided by helperplace agency"
                                                title="Training Offer by helperplace agency"
                                                width="100%"
                                                height="auto"
                                                src="https://cdn.helperplace.com/a_logo/13_1610681753.JPG"
                                            />
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail">
                                            <h3 _ngcontent-serverapp-c1262338835="">IconMed Elite Caregiver Training</h3>
                                            <h5 _ngcontent-serverapp-c1262338835="" class="widget-item-location"><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-map-marker dark"></i>Hong Kong</h5>
                                        </div>
                                    </div>
                                    <!----><!---->
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-action"><button _ngcontent-serverapp-c1262338835="" class="btn custom-transperant-button">See All</button></div>
                                </div>
                            </div>
                        </div>
                        <!---->
                        <div _ngcontent-serverapp-c1262338835="" class="col-12 col-md-6 col-lg-6 col-xl-4 mb-3 info-block ng-star-inserted">
                            <div _ngcontent-serverapp-c1262338835="" class="custom-dashboard-widget">
                                <div _ngcontent-serverapp-c1262338835="" class="widget-header">
                                    <h2 _ngcontent-serverapp-c1262338835=""><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-handshake-o"></i>Partner Offer</h2>
                                </div>
                                <div _ngcontent-serverapp-c1262338835="" class="widget-body">
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer border-bottom ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image img">
                                            <img
                                                _ngcontent-serverapp-c1262338835=""
                                                alt="Helperplace Training and partner agency"
                                                title="Training Offer by helperplace partner agency"
                                                width="100%"
                                                height="auto"
                                                src="https://cdn.helperplace.com/a_logo/120_1661848390.jpeg"
                                            />
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail">
                                            <h3 _ngcontent-serverapp-c1262338835="">Sky Angel Travel</h3>
                                            <h5 _ngcontent-serverapp-c1262338835="" class="widget-item-location"><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-map-marker dark"></i>Hong Kong</h5>
                                        </div>
                                    </div>
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-item cursor-pointer ng-star-inserted">
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-image img">
                                            <img
                                                _ngcontent-serverapp-c1262338835=""
                                                alt="Helperplace Training and partner agency"
                                                title="Training Offer by helperplace partner agency"
                                                width="100%"
                                                height="auto"
                                                src="https://cdn.helperplace.com/a_logo/109_1649155071.jpeg"
                                            />
                                        </div>
                                        <div _ngcontent-serverapp-c1262338835="" class="widget-item-detail">
                                            <h3 _ngcontent-serverapp-c1262338835="">Panda Remit</h3>
                                            <h5 _ngcontent-serverapp-c1262338835="" class="widget-item-location"><i _ngcontent-serverapp-c1262338835="" aria-hidden="true" class="fa fa-map-marker dark"></i>Hong Kong</h5>
                                        </div>
                                    </div>
                                    <!----><!---->
                                    <div _ngcontent-serverapp-c1262338835="" class="widget-action"><button _ngcontent-serverapp-c1262338835="" class="btn custom-transperant-button">See All</button></div>
                                </div>
                            </div>
                        </div>
                        <!----><!---->
                    </div>
                </div>
            </div>
        </section>
        <!----><!---->
    </section>
    <section _ngcontent-serverapp-c1262338835="" class="page-section dashboard-ar mobile-responsive ng-star-inserted">
        <section _ngcontent-serverapp-c1262338835="" class="agency blog blog-sec blog-sidebar mt-3 pb-5 blog-list sider Custom-Dashboard">
            <div _ngcontent-serverapp-c1262338835="" class="container px-3">
                <div _ngcontent-serverapp-c1262338835="" class="dashboard-mobile">
                    <div _ngcontent-serverapp-c1262338835="" class="card border-radius-card bg-color">
                        <div _ngcontent-serverapp-c1262338835="" class="card-body">
                            <!----><!----><!----><!----><!---->
                            <p _ngcontent-serverapp-c1262338835="" class="title ng-star-inserted">Complete your profile to freely connect with all our employers.</p>
                            <div _ngcontent-serverapp-c1262338835="" class="custom-job-action ng-star-inserted">
                                <div _ngcontent-serverapp-c1262338835="" class="custom-job-action-right px-2 row justify-content-center">
                                    <div _ngcontent-serverapp-c1262338835="">
                                        <a href="{{route('candidate_profile')}}" _ngcontent-serverapp-c1262338835="" class="btn custom-btn btn-success">Complete My Profile</a>
                                        <a href="{{route('bio-data')}}"  _ngcontent-serverapp-c1262338835="" class="btn custom-btn btn-dark" style="background-color:#000">Update Biodata</a>
                                        <a href="{{route('jobs')}}" target="_blank" _ngcontent-serverapp-c1262338835="" class="btn custom-btn btn-warning">Search Job</a>
                                        
                                        <a href="{{route('helper_packages')}}" _ngcontent-serverapp-c1262338835="" class="btn custom-btn btn-primary" style="background-color:#1E3F66">SUBSCRIBE</a>
                                       
                                        </div>
                                    
  
                                </div>
                            </div>
                            <!----><!----><!----><!---->
                        </div>
                    </div>
                    @php
                        $homePage = App\Models\CandidateDashboardPage::find(1);
                    @endphp
                    <div _ngcontent-serverapp-c1262338835="" class="grid-container mt-2">
                        <div _ngcontent-serverapp-c1262338835="" class="ng-star-inserted">
                            <a href="{{route('jobs')}}">
                            <div _ngcontent-serverapp-c1262338835="" class="icon-box">
                                <div _ngcontent-serverapp-c1262338835=""><img _ngcontent-serverapp-c1262338835="" src="{{asset('storage/candidate_dashboard_page')}}/{{@$homePage->first_image}}" width="70" height="70" /></div>
                                <div _ngcontent-serverapp-c1262338835="">
                                    
                                            <span _ngcontent-serverapp-c1262338835="">
                                            <b>Find Job</b>
                                        </span>
                                    </div>
                            </div>
                            </a>
                        </div>
                        <!---->
                        <div _ngcontent-serverapp-c1262338835="">
                             <a href="{{route('news')}}">
                            <div _ngcontent-serverapp-c1262338835="" class="icon-box">
                                <div _ngcontent-serverapp-c1262338835=""><img _ngcontent-serverapp-c1262338835="" src="{{asset('storage/candidate_dashboard_page')}}/{{@$homePage->second_image}}" width="70" height="70" /></div>
                                <div _ngcontent-serverapp-c1262338835="">
                                   
                                          <span _ngcontent-serverapp-c1262338835="">Tips &amp; News</span>
                                      
                                    </div>
                            </div>
                             </a>
                        </div>
                        <div _ngcontent-serverapp-c1262338835="">
                            <a href="{{route('agencies_services')}}">
                            <div _ngcontent-serverapp-c1262338835="" class="icon-box">
                                <div _ngcontent-serverapp-c1262338835=""><img _ngcontent-serverapp-c1262338835="" src="{{asset('storage/candidate_dashboard_page')}}/{{@$homePage->third_image}}" width="70" height="70" /></div>
                                <div _ngcontent-serverapp-c1262338835="">
                                    
                                        <span _ngcontent-serverapp-c1262338835="">Agencies Support</span>
                                    
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div _ngcontent-serverapp-c1262338835="">
                            <a href="{{route('event')}}">
                            <div _ngcontent-serverapp-c1262338835="" class="icon-box">
                                <div _ngcontent-serverapp-c1262338835=""><img _ngcontent-serverapp-c1262338835="" src="{{asset('storage/candidate_dashboard_page')}}/{{@$homePage->fourth_image}}" width="70" height="70" /></div>
                                <div _ngcontent-serverapp-c1262338835="">
                                    
                                            <span _ngcontent-serverapp-c1262338835=""> <b>Event </b>
                                            </span>
                                        
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div _ngcontent-serverapp-c1262338835="">
                            <a href="{{route('training_page')}}">
                            <div _ngcontent-serverapp-c1262338835="" class="icon-box">
                                <div _ngcontent-serverapp-c1262338835=""><img _ngcontent-serverapp-c1262338835="" src="{{asset('storage/candidate_dashboard_page')}}/{{@$homePage->five_image}}" width="70" height="70" /></div>
                                <div _ngcontent-serverapp-c1262338835="">
                                    
                                        <span _ngcontent-serverapp-c1262338835="">Training Offer</span>
                                    
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div _ngcontent-serverapp-c1262338835="">
                            <a href="{{route('public_holiday')}}">
                            <div _ngcontent-serverapp-c1262338835="" class="icon-box">
                                <div _ngcontent-serverapp-c1262338835=""><img _ngcontent-serverapp-c1262338835="" src="{{asset('storage/candidate_dashboard_page')}}/{{@$homePage->six_image}}" width="70" height="70" /></div>
                                <div _ngcontent-serverapp-c1262338835="">
                                    
                                            <span _ngcontent-serverapp-c1262338835=""><b> Public Holidays </b></span>
                                        
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div _ngcontent-serverapp-c1262338835="">
                            <a href="/partner">
                            <div _ngcontent-serverapp-c1262338835="" class="icon-box">
                                <div _ngcontent-serverapp-c1262338835=""><img _ngcontent-serverapp-c1262338835="" src="{{asset('storage/candidate_dashboard_page')}}/{{@$homePage->seven_image}}" width="70" height="70" /></div>
                                <div _ngcontent-serverapp-c1262338835="">
                                    
                                        <span _ngcontent-serverapp-c1262338835=""> Partner Offer </span>
                                    
                                    </div>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>


@endsection
