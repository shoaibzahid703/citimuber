<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\EventsLandingPage;
use App\Models\Country;
use App\Models\Event;
use App\Traits\imageUploadTrait;
use Illuminate\Support\Str;

class EventsController extends Controller
{
    
    use imageUploadTrait;

    public function events(Request $request)
    {
        if ($request->ajax()) {
            $data = Event::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

           
            ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

            ->addColumn('description', function ($data) {
                // Function to get the first 10 words
                $words = explode(' ', $data->description);
                $limitedWords = implode(' ', array_slice($words, 0, 10));

                return '<p>' . $limitedWords . '... <a href="#" class="see-more" data-description="' . htmlspecialchars($data->description, ENT_QUOTES, 'UTF-8') . '">Read More</a></p>';
            })

                ->addColumn('action', function($data) {
                    $editUrl = route('edit-event', $data->id);
                    $deleteUrl = route('delete-event', $data->id);
                    $changeStatus = route('block_event', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';

                        if ($data->status == \App\Models\PublicHoliday::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['description','status','action'])
                ->make(true);

        }
        return view('admin.pages.events.index');
    }


   public function save_events(Request $request)
  {

    $request->validate([
         'first_heading' => 'required',
         'description' => 'required',
        
    ]);

    $about_us_page = EventsLandingPage::firstOrNew(['id' => 1]);

    $about_us_page->first_heading = $request->first_heading;
    $about_us_page->description = $request->description;
    
    $about_us_page->save();

    return redirect()->back()->with('success', 'Event updated successfully.');
  }


  public function add_events()
  {
    $country = Country::all();

    return view('admin.pages.events.create', compact('country'));
  }

   public function store_events(Request $request)
    {


        $request->validate([
        'title' => 'required',
        'country_id' => 'required',
        'organizer_name' => 'required',
        'event_date' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'audience' => 'required',
        'fees' => 'required',
        'phone' => 'required',
        'requirement' => 'required',
        'location' => 'required',
        'short_decription' => 'required',
        'description' => 'required',
         ]);

        if($request->event_image){
            $event_image = self::uploadFile($request, fileName: 'event_image', folderName: 'event_image');
        }

        $slug = Str::slug($request->title);

        $residential = Event::create([
            'user_id'  => \Auth::user()->id,
            'title' => $request->title,
            'slug' => $slug,
            'country_id' => $request->country_id,
            'organizer_name' => $request->organizer_name,
            'event_date' => $request->event_date,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'audience' => $request->audience,
            'fees' => $request->fees,
            'phone' => $request->phone,
            'requirement' => $request->requirement,
            'location' => $request->location,
            'event_image' => @$event_image,
            
            'description' => $request->description,
            
            
        ]);
        return redirect()->route('events')->with('success','Event Added Successfully');
    }


    public function edit_event(Event $event)
    {

        $country = Country::all();

     return view('admin.pages.events.edit', compact('event', 'country'));
    }


    public function update_event(Request $request, Event $event)
    {
        $request->validate([
        'title' => 'required',
        'country_id' => 'required',
        'organizer_name' => 'required',
        'event_date' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'audience' => 'required',
        'fees' => 'required',
        'phone' => 'required',
        'requirement' => 'required',
        'location' => 'required',
        'short_decription' => 'required',
        'description' => 'required',
         ]);
        $slug = Str::slug($request->title);

        $updateEvent = [
             'title' => $request->title,
             'slug' => $slug,
             'country_id' => $request->country_id,
             'organizer_name' => $request->organizer_name,
             'event_date' => $request->event_date,
             'start_time' => $request->start_time,
             'end_time' => $request->end_time,
             'audience' => $request->audience,
             'fees' => $request->fees,
             'phone' => $request->phone,
             'requirement' => $request->requirement,
             'location' => $request->location,
             'short_decription' => $request->short_decription,
             'description' => $request->description,
         ];

         if ($request->hasFile('event_image')) {
             $event_image = self::uploadFile($request, 'event_image', 'event_image');
             $updateEvent['event_image'] = $event_image;
         }

         $event->update($updateEvent);

         return redirect()->route('events')->with('success', 'Event Updated Successfully!');
     }



   public function delete_event(Event $event)
    {
         Event::destroy($event->id);
        return redirect()->back()->with('success','Event Deleted Successfully');

    }


    public function block_event(Event $event){

        if ($event->status == Event::ENABLE){

            $event->update([
                'status' => Event::DISABLE
            ]);
            $message = 'Event disable successfully';

        }else{
            $event->update([
                'status' => Event::ENABLE
            ]);
            $message = 'Event enable successfully';
        }

        return redirect()->back()->with('success',$message);

    }


}
