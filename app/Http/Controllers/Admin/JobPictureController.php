<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Models\JobPicture;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JobPictureController extends Controller
{
    use  imageUploadTrait;
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = JobPicture::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('img_preview', function($job_picture){
                    $img_path = asset('storage/job_picture/'.$job_picture->img);
                    return '<img src="'.$img_path.'" width="40%"">';
                })

                ->addColumn('action', function($job_picture) {
                    $editUrl = route('job_picture_edit', $job_picture);
                    $deleteUrl = route('job_picture_delete',$job_picture);


                    return '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>';
                })
                ->rawColumns(['img_preview','action'])
                ->make(true);

        }
        return view('admin.pages.job_picture.index');
    }

    public function create()
    {
        return view('admin.pages.job_picture.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image'
        ]);

        $image_name = $this->uploadFile($request,'image','job_picture');

        JobPicture::create([
            'img' => $image_name
        ]);

        return redirect()->route('job_pictures')->with('success','Image Added Successfully');
    }

    public function edit(JobPicture $job_picture)
    {
        return view('admin.pages.job_picture.edit',compact('job_picture'));
    }

    public function update(Request $request,JobPicture $job_picture)
    {
        $request->validate([
            'image' => 'required|image'
        ]);

        $image_name = $this->uploadFile($request,'image','job_picture');

        $job_picture->update([
            'img' => $image_name
        ]);

        return redirect()->route('job_pictures')->with('success','Image Updated Successfully');
    }

    public function delete(JobPicture $job_picture)
    {
       JobPicture::destroy($job_picture->id);
        return redirect()->route('job_pictures')->with('success','Picture Deleted Successfully');
    }
}
