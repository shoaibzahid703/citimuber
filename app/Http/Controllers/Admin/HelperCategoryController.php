<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmployerCategory;
use App\Models\HelperCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class HelperCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $helper_categories = HelperCategory::orderBy('id', 'DESC');

            return Datatables::of($helper_categories)
                ->addIndexColumn()
                ->editColumn('employer_apply_status', function($category){
                    if ($category->employer_apply_status == HelperCategory::Yes) {
                        return "<div class='badge bg-success'>Yes</div>";
                    } else {
                        return "<div class='badge bg-secondary'>No</div>";
                    }
                })
                ->addColumn('action', function($category) {
                    $deleteUrl = route('helper_category_delete', $category);
                    $editUrl = route('helper_category_edit', $category);

                    $actionHtml = '';
                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';


                    $actionHtml .= '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    return $actionHtml;
                })
                ->rawColumns([ 'employer_apply_status','action'])
                ->make(true);

        }
        return view('admin.pages.helper_category.index');
    }
    public function create()
    {
        return view('admin.pages.helper_category.create');

    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:helper_categories,name',
        ]);
        HelperCategory::create([
            'name' => $request->name,
            'employer_apply_status' => $request->employer_apply ?? 0,
        ]);

        return redirect()->route('helper_categories')->with('success','Category Added Successfully');
    }
    public function edit(HelperCategory $category)
    {
        return view('admin.pages.helper_category.edit',[
            'category'  => $category
        ]);
    }
    public function update(Request $request,HelperCategory $category)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category->update([
            'name' => $request->name,
            'employer_apply_status' => $request->employer_apply ?? 0,
        ]);
        return redirect()->route('helper_categories')
            ->with('success', 'Category is updated successfully.');
    }
    public function delete(HelperCategory $category)
    {
        $category->delete();
        return redirect()->route('helper_categories')
            ->with('success', 'Category is deleted successfully.');
    }
}
