<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NewLandingPage;
use App\Models\News;
use Yajra\DataTables\DataTables;
use App\Traits\imageUploadTrait;
use Illuminate\Support\Str;
use App\Models\Category;

class NewsController extends Controller
{
     use imageUploadTrait;


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = News::orderBy('id', 'DESC')->get();

            return Datatables::of($data)


            ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

            ->addColumn('news_image', function($data) {

                $imageUrl = asset('storage/news_image/' . $data->news_image);
                return '<img src="' . $imageUrl . '" alt="Employee Image" width="50" height="50">';
            })


            ->addColumn('description', function ($data) {
                $words = explode(' ', $data->description);
                $limitedWords = implode(' ', array_slice($words, 0, 5));

                return '<p>' . $limitedWords . '... <a href="#" class="see-more" data-description="' . htmlspecialchars($data->description, ENT_QUOTES, 'UTF-8') . '">Read More</a></p>';
            })

                ->addColumn('action', function($data) {
                    $editUrl = route('edit-news', $data->id);
                    $deleteUrl = route('delete-news', $data->id);
                    $changeStatus = route('block_news', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';

                        if ($data->status == \App\Models\News::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['news_image','description','status','action'])
                ->make(true);

        }

        return view('admin.pages.news.index');
    }


   public function save_news_page(Request $request)
  {

    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',

    ]);

    $news_page = NewLandingPage::firstOrNew(['id' => 1]);

    $news_page->first_heading = $request->first_heading;
    $news_page->paragraph = $request->paragraph;
    $news_page->description = $request->description;

    $news_page->save();

    return redirect()->back()->with('success', 'News landing page updated successfully.');
  }


  public function add_news()
  {
     $categories = Category::where('status', 1)->get();
    return view('admin.pages.news.create', compact('categories'));
  }

   public function store_news(Request $request)
    {
        
        $request->validate([
        'title' => 'required',
        'news_date' => 'required',
        'category_id' => 'required',
        'description' => 'required',
        'news_image' => 'required',
        'full_details' => 'required',
         ]);


        if($request->news_image){
            $news_image = self::uploadFile($request, fileName: 'news_image', folderName: 'news_image');
        }
        
        
        if($request->location_image){
            $location_image = self::uploadFile($request, fileName: 'location_image', folderName: 'news_image');
        }

       News::create([
            'title' => $request->title,
            'news_date' => $request->news_date,
             'category_id' => $request->category_id,
            'news_image' => @$news_image,
        
            'location_image' => @$location_image,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'whatsapp_url' => $request->whatsapp_url,
            'linkedIn_url' => $request->linkedIn_url,

            'description' => $request->description,
            'full_details' => $request->full_details,

        ]);

        return redirect()->route('news_page')->with('success','News Added Successfully');
    }


    public function edit_news(News $news)
    {
       $categories = Category::where('status', 1)->get();
      return view('admin.pages.news.edit', compact('news', 'categories'));
    }


    public function update_news(Request $request, News $news)
    {
        $request->validate([
        'title' => 'required',
        'news_date' => 'required',
        'category_id' => 'required',
        'description' => 'required',
        'full_details' => 'required',
         ]);

        $newsEvent = [
            'title' => $request->title,
            'news_date' => $request->news_date,
            'category_id' => $request->category_id,
            'description' => $request->description,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'whatsapp_url' => $request->whatsapp_url,
            'linkedIn_url' => $request->linkedIn_url,
            'full_details' => $request->full_details,
            'news_image' => $news->news_image,
            'location_image' => $news->location_image
         ];

         if ($request->hasFile('news_image')) {
             $news_image = self::uploadFile($request, 'news_image', 'news_image');
             $newsEvent['news_image'] = $news_image;
         }
         
         if ($request->hasFile('location_image')) {
             $location_image = self::uploadFile($request, 'location_image', 'news_image');
             $newsEvent['location_image'] = $location_image;
         }

         $news->update($newsEvent);

         return redirect()->route('news_page')->with('success', 'News Updated Successfully!');
     }



   public function delete_news(News $news)
    {
         News::destroy($news->id);
        return redirect()->back()->with('success','News Deleted Successfully');

    }


    public function block_news(News $news){

        if ($news->status == News::ACTIVE){

            $news->update([
                'status' => News::BLOCK
            ]);
            $message = 'News Block Successfully';

        }else{
            $news->update([
                'status' => News::ACTIVE
            ]);
            $message = 'News Un-block Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
