<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobPosition;
use Yajra\DataTables\DataTables;

class JobPositionController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = JobPosition::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('job_position_edit', $data->id);
                    $deleteUrl = route('job_position_delete', $data->id);
                    $changeStatus = route('block_job_position', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\JobPosition::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.JobPosition.index');
    }

      public function create()
    {
        return view('admin.pages.JobPosition.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:job_positions,name',

        ]);
        JobPosition::create([
            'name' => $request->name,
        ]);

        return redirect()->route('job-position')->with('success','JobPosition Added Successfully');
    }


    public function edit(JobPosition $JobPosition)
    {
        return view('admin.pages.JobPosition.edit',compact('JobPosition'));
    }
    public function update(Request $request,JobPosition $JobPosition)
    {
        $request->validate([
            'name' => 'required|unique:job_positions,name,'.$JobPosition->id

        ]);
        $JobPosition->update([
            'name' => $request->name,
        ]);

        return redirect()->route('job-position')->with('success','JobPosition Added Successfully');
    }
    public function delete(JobPosition $JobPosition)
    {
        JobPosition::destroy($JobPosition->id);
        return redirect()->route('job-position')->with('success','JobPosition Deleted Successfully');

    }


    public function block_job_position(JobPosition $JobPosition){

        if ($JobPosition->status == JobPosition::ACTIVE){

            $JobPosition->update([
                'status' => JobPosition::BLOCK
            ]);
            $message = 'JobPosition Blocked Successfully';

        }else{
            $JobPosition->update([
                'status' => JobPosition::ACTIVE
            ]);
            $message = 'JobPosition Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }

}
