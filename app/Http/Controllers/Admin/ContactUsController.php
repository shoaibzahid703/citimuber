<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUsPage;

class ContactUsController extends Controller
{


    public function index(){
        $contact_us =  ContactUsPage::find(1);
     return view('admin.pages.contact_us_page.index',compact('contact_us'));
   }


  public function save_contact_us(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'first_paragraph' => 'required',
        'first_text' => 'required',
        'second_text' => 'required',
        'contact_number' => 'required',
        'second_heading' => 'required',
        'third_text' => 'required',
        'contact_us_button' => 'required',
        'contact_us_map_src' => 'required',
    ]);

    $contact_us = ContactUsPage::firstOrNew(['id' => 1]);

    $contact_us->first_heading = $request->first_heading;
    $contact_us->first_paragraph = $request->first_paragraph;
    $contact_us->first_text = $request->first_text;
    $contact_us->second_text = $request->second_text;
    $contact_us->contact_number = $request->contact_number;
    $contact_us->second_heading = $request->second_heading;
    $contact_us->third_text = $request->third_text;
    $contact_us->contact_us_button = $request->contact_us_button;
    $contact_us->contact_us_map_src = $request->contact_us_map_src;
    $contact_us->save();

    return redirect()->back()->with('success', 'Contact Us Page is updated successfully.');
  }
}
