<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminMessage;
use App\Models\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where('role',User::ROLE_CANDIDATE)
            ->where('status',User::ACTIVE)
            ->orwhere('role',User::ROLE_EMPLOYER)
            ->get();

        $users_ids = User::where('role',User::ROLE_CANDIDATE)
            ->where('status',User::ACTIVE)
            ->orwhere('role',User::ROLE_EMPLOYER)
            ->pluck('id');

        return view('admin.pages.message.index',compact(
            'users','users_ids'
        ));
    }
    public function chatIndex(User $receiver,Request $request)
    {
        $count_admin_un_read_message = AdminMessage::where('from_user',$receiver->id)
            ->where('to_user',auth()->user()->id)
            ->where('is_read',0)
            ->count();


        if ($count_admin_un_read_message > 0){
            AdminMessage::where('from_user',$receiver->id)
                ->where('to_user',auth()->user()->id)
                ->where('is_read',0)
                ->update([
                    'is_read' => 1
                ]);
        }


        $messages = AdminMessage::where(function($query) use ($receiver, $request) {
            $query->where('from_user', auth()->user()->id)
                ->where('to_user', $receiver->id);
        })->orWhere(function ($query) use ($receiver, $request) {
            $query->where('from_user', $receiver->id
            )->where('to_user', auth()->user()->id);
        })->orderBy('created_at', 'asc')
            ->get();

        $users = User::where('role',User::ROLE_CANDIDATE)
            ->where('status',User::ACTIVE)
            ->orwhere('role',User::ROLE_EMPLOYER)
            ->get();

        $users_ids = User::where('role',User::ROLE_CANDIDATE)
            ->where('status',User::ACTIVE)
            ->where('id','!=',$receiver->id)
            ->orwhere('role',User::ROLE_EMPLOYER)
            ->pluck('id');


        return view('admin.pages.message.userChat',compact(
            'receiver',
            'messages',
            'users',
            'users_ids'

        ));
    }

    public function send_msg(Request $request)
    {
        $message =  AdminMessage::create([
            'message' => $request->comment,
            'from_user' => $request->from_user,
            'to_user' => $request->to_user,
            'admin' => 'from_user',
        ]);
        $message_html = view('admin.pages.message.message-line',compact('message'))->render();
        return response()->json([
            'status' => true,
            'message_html' => $message_html,
        ]);
    }

    public function delete_msg(AdminMessage $message)
    {
        $message->delete();
        return redirect()->back()->with('success','Message Deleted Successfully');
    }

    public function get_admin_unread_messages_count(Request $request)
    {
        $data  = [];
        foreach (explode(',',$request->users_ids) as $user){
            $count_un_read_message =   AdminMessage::where('from_user',$user)
                ->where('to_user',auth()->user()->id)
                ->where('is_read',0)
                ->count();
            if ($count_un_read_message > 0){
                $result = [
                    'user_id' => $user,
                    'count_message' => $count_un_read_message ,
                    'is_remove' => false ,
                ];
                array_push($data,$result);
            }else{
                $message = [
                    'user_id' => $user,
                    'count_message' => $count_un_read_message ,
                    'is_remove' => true ,
                ];
                array_push($data,$message);
            }
        }
        if (count($data) > 0){
            return response()->json([
                'status' => true,
                'response_data' => $data,
            ]);
        }else{
            return response()->json([
                'status' => false,
            ]);
        }
    }
    public function get_received_messages(Request $request)
    {
        $from_user = $request->from_user;
        $to_user = $request->to_user;

        $messages = AdminMessage::where(function($query) use ($to_user, $from_user, $request) {
            $query->where('from_user', $from_user)
                ->where('to_user',$to_user)
                ->where('is_read',0);
        })->orderBy('created_at', 'asc')
            ->get();



        if ($messages->count() > 0){
            $received_message = true;

            AdminMessage::where('from_user',$from_user)
                ->where('to_user',$to_user)
                ->where('is_read',0)
                ->update([
                    'is_read' => 1
                ]);

            $message_html = view('admin.pages.message.message-line',compact('messages','received_message'))
                ->render();
            return response()->json([
                'status' => true,
                'message_html' => $message_html,
            ]);
        }else{
            return response()->json([
                'status' => false,
            ]);
        }


    }

    public function mark_as_read(User $receiver)
    {
        AdminMessage::where('from_user',$receiver->id)
            ->where('to_user',auth()->user()->id)
            ->where('is_read',0)
            ->update([
                'is_read' => 1
            ]);

        return redirect()->back()->with('success','Read Messages Successfully');
    }

}
