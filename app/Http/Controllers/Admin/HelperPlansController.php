<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\HelperPlan;
use App\Models\Plans;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class HelperPlansController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = HelperPlan::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

                ->addColumn('status', function($state){

                    if ($state->status == Plans::ENABLE) {
                        return "<div class='badge bg-success'>Enable</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Disable</div>";
                    }
                })


                ->addColumn('action', function($data) {
                    $editUrl = route('helper_plan_edit', $data->id);
                    $deleteUrl = route('helper_plan_delete', $data->id);
                    $changeStatus = route('helper_plan_block', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\HelperPlan::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['status','role','action'])
                ->make(true);

        }
        return view('admin.pages.helper_plan.index');
    }

    public function create()
    {
        $countries = Country::where('status',Country::ACTIVE)->get();
        return view('admin.pages.helper_plan.create', compact('countries'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:helper_plans,name',
            'post_duration'=> 'required',
            'price'=> 'required',
        ]);
        HelperPlan::create([
            'name' => $request->name,
            'post_duration' => $request->post_duration,
            'price' => $request->price,
            'country' =>  implode(',', $request['country']),
            'manual_price' => implode(',', $request['manual_price']),
            'features' =>  $request->feature_description,
            'employer_contact' =>    $request->employer_contact ?? 0,
        ]);

        return redirect()->route('helper_plans')->with('success','Plan Added Successfully');
    }
    public function edit(HelperPlan $plan)
    {
        $countries = Country::where('status',Country::ACTIVE)->get();
        return view('admin.pages.helper_plan.edit',compact('plan', 'countries'));
    }

    public function update(Request $request,HelperPlan $plan)
    {
        $request->validate([
            'name' => 'required|unique:helper_plans,name,'.$plan->id,
            'post_duration'=> 'required|integer|min:1',
            'price'=> 'required|numeric|min:0',
        ]);
        $plan->update([
            'name' => $request->name,
            'post_duration' => $request->post_duration,
            'price' => $request->price,
            'country' =>  implode(',', $request['country']),
            'manual_price' => implode(',', $request['manual_price']),
            'features' =>  $request->feature_description,
            'employer_contact' =>    $request->employer_contact ?? 0,
        ]);

        return redirect()->route('helper_plans')->with('success','Plans Added Successfully');
    }
    public function delete(HelperPlan $plan)
    {
        HelperPlan::destroy($plan->id);
        return redirect()->route('helper_plans')->with('success','Plans Deleted Successfully');

    }
    public function block_plan(HelperPlan $plan){

        if ($plan->status == HelperPlan::ENABLE){
            $plan->update([
                'status' => HelperPlan::DISABLE
            ]);
            $message = 'Plans Disable Successfully';

        }else{
            $plan->update([
                'status' => HelperPlan::ENABLE
            ]);
            $message = 'Plans Enable Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
