<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\CookingSkill;

class CookingskillsController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $cooking_skills = CookingSkill::orderBy('id', 'DESC')->get();

            return Datatables::of($cooking_skills)
                ->addColumn('status', function($skill){
                    if ($skill->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })
                ->addColumn('action', function($skill) {
                    $editUrl = route('cooking_skills_edit', $skill);
                    $deleteUrl = route('cooking_skills_delete', $skill);
                    $changeStatus = route('blockCookingSkill', $skill);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($skill->status == \App\Models\CookingSkill::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary  btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.cooking-skills.index');
    }


    public function create()
    {
        return view('admin.pages.cooking-skills.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:cooking_skills,name',

        ]);
        CookingSkill::create([
            'name' => $request->name,
        ]);

        return redirect()->route('cooking_skills')->with('success','Cooking Skill Added Successfully');
    }


    public function edit(CookingSkill $skills)
    {
        return view('admin.pages.cooking-skills.edit',compact('skills'));
    }
    public function update(Request $request,CookingSkill $skills)
    {
        $request->validate([
            'name' => 'required|unique:cooking_skills,name,'.$skills->id,

        ]);
        $skills->update([
            'name' => $request->name,

        ]);
        return redirect()->route('cooking_skills')->with('success','CookingSkill Added Successfully');
    }
    public function delete(CookingSkill $skills)
    {
        CookingSkill::destroy($skills->id);
        return redirect()->route('cooking_skills')->with('success','Cooking Skill Deleted Successfully');

    }


    public function blockCookingSkill(CookingSkill $skills){

        if ($skills->status == CookingSkill::ACTIVE){

            $skills->update([
                'status' => CookingSkill::BLOCK
            ]);
            $message = 'Cooking Skill Blocked Successfully';

        }else{
            $skills->update([
                'status' => CookingSkill::ACTIVE
            ]);
            $message = 'Cooking Skill Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
