<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AgencyService;
use App\Models\Plans;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AgencyServiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AgencyService::with('agency')->orderBy('id', 'DESC')->get();

            return Datatables::of($data)

                ->addColumn('status', function($state){

                    if ($state->status == Plans::ENABLE) {
                        return "<div class='badge bg-success'>Enable</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Disable</div>";
                    }
                })

                ->addColumn('agency_name', function($state){
                    if ($state->agency){
                        return $state->agency->company_name;
                    }else{
                        return '-';
                    }
                })


                ->addColumn('action', function($data) {
//                    $editUrl = route('agency_service_edit', $data->id);
//                    $deleteUrl = route('agency_service_delete', $data->id);
                    $changeStatus = route('agency_service_block', $data->id);
                    $actionHtml = '';

//                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
//                                       <span class="nk-menu-icon text-white">
//                                           <em class="icon ni ni-edit"></em>
//                                       </span>
//                                    </a>
//
//                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
//                                       <span class="nk-menu-icon text-white">
//                                           <em class="icon ni ni-trash-empty"></em>
//                                       </span>
//                                    </a>
//
//                                    ';
                    if ($data->status == \App\Models\AgencyService::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['status','role','action'])
                ->make(true);

        }
        return view('admin.pages.agency_service.index');
    }
    public function create()
    {
        return view('admin.pages.agency_service.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:agency_services,name',
            'no_of_weeks'=> 'required',
            'price'=> 'required',
        ]);
        AgencyService::create([
            'name' => $request->name,
            'no_of_weeks' => $request->no_of_weeks,
            'price' => $request->price,
            'description' =>  $request->description,
        ]);

        return redirect()->route('agency_services')->with('success','Service Added Successfully');
    }
    public function edit(AgencyService $service)
    {
        return view('admin.pages.agency_service.edit',compact('service'));
    }
    public function update(Request $request,AgencyService $service)
    {
        $request->validate([
            'name' => 'required|unique:agency_services,name,' . $service->id,
            'no_of_weeks'=> 'required',
            'price'=> 'required',
        ]);
        $service->update([
            'name' => $request->name,
            'no_of_weeks' => $request->no_of_weeks,
            'price' => $request->price,
            'description' =>  $request->description,
        ]);

        return redirect()->route('agency_services')->with('success', 'Service Updated Successfully');
    }
    public function delete(AgencyService $service)
    {
        AgencyService::destroy($service->id);
        return redirect()->route('agency_services')->with('success','Service Deleted Successfully');

    }
    public function block_service(AgencyService $service){

        if ($service->status == AgencyService::ENABLE){
            $service->update([
                'status' => AgencyService::DISABLE
            ]);
            $message = 'Service Disable Successfully';

        }else{
            $service->update([
                'status' => AgencyService::ENABLE
            ]);
            $message = 'Service Enable Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
