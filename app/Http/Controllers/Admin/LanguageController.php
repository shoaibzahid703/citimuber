<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class LanguageController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $languages = Language::orderBy('id', 'DESC')->get();

            return Datatables::of($languages)
                ->addColumn('action', function($language) {
                   $languageEdit = route('language_edit', $language);
                   $deleteUrl = route('language_delete', $language);


                    return '<a href="'.$languageEdit.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                     <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    })

                ->rawColumns(['action'])
                ->make(true);

        }
        return view('admin.pages.language.index');
    }


    public function create()
    {
        return view('admin.pages.language.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:languages,name',

        ]);
        Language::create([
            'name' => $request->name,

            'status' => $request->status ?? 0,
        ]);

        return redirect()->route('languages')->with('success','Language Added Successfully');
    }
    public function edit(Language $language)
    {
        return view('admin.pages.language.edit',compact('language'));
    }
    public function update(Request $request,Language $language)
    {
        $request->validate([
            'name' => 'required|unique:languages,name,'.$language->id,
        ]);
        $language->update([
            'name' => $request->name,

            'status' => $request->status ?? 0,

        ]);

        return redirect()->route('languages')->with('success','Language Added Successfully');
    }
    public function delete(Language $language)
    {
         Language::destroy($language->id);
        return redirect()->route('languages')->with('success','Language Deleted Successfully');

    }
}
