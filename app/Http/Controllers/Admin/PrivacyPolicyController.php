<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PrivacyPolicy;

class PrivacyPolicyController extends Controller
{
    
    public function index(){

     return view('admin.pages.privacy_policy_page.index');
   }

   
  public function save_privacy_policy(Request $request)
  {
    $request->validate([
        'description' => 'required',
    ]);

    $terms_and_condition = PrivacyPolicy::firstOrNew(['id' => 1]);

    $terms_and_condition->description = $request->description;
    
    $terms_and_condition->save();

    return redirect()->back()->with('success', 'Privacy Policy is updated successfully.');
  }
}
