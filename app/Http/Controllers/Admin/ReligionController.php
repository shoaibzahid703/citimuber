<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Religion;
use Yajra\DataTables\DataTables;

class ReligionController extends Controller
{
    

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Religion::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('religion_edit', $data->id);
                    $deleteUrl = route('religion_delete', $data->id);
                    $changeStatus = route('block_religion', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Religion::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.Religion.index');
    }

      public function create()
    {
        return view('admin.pages.Religion.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:religions,name',

        ]);
        Religion::create([
            'name' => $request->name,
        ]);

        return redirect()->route('religions')->with('success','Religion Added Successfully');
    }


    public function edit(Religion $religion)
    {
        return view('admin.pages.Religion.edit',compact('religion'));
    }
    public function update(Request $request, Religion $religion)
    {
        
        $request->validate([
            'name' => 'required|unique:religions,name,'.$religion->id

        ]);
        $religion->update([
            'name' => $request->name,
        ]);

        return redirect()->route('religions')->with('success','Religion Added Successfully');
    }
    public function delete(Religion $religion)
    {
        Religion::destroy($religion->id);
        return redirect()->route('religions')->with('success','Religion Deleted Successfully');

    }


    public function block_Religion(Religion $religion){

        if ($religion->status == Religion::ACTIVE){

            $religion->update([
                'status' => Religion::BLOCK
            ]);
            $message = 'Religion Blocked Successfully';

        }else{
            $religion->update([
                'status' => Religion::ACTIVE
            ]);
            $message = 'Religion Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
