<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Currency;

class CurrencyController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Currency::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('currency_edit', $data->id);
                    $deleteUrl = route('currency_delete', $data->id);
                    $changeStatus = route('block_currency', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Currency::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.currency.index');
    }


    public function create()
    {
        return view('admin.pages.currency.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:currencies,name',
            'sign' => 'required',
        ]);
        Currency::create([
            'name' => $request->name,
            'sign' => $request->sign,
        ]);

        return redirect()->route('currency')->with('success','Currency Added Successfully');
    }


    public function edit(currency $currency)
    {
        return view('admin.pages.currency.edit',compact('currency'));
    }
    public function update(Request $request, Currency $currency)
    {
        $request->validate([
            'name' => 'required|unique:currencies,name,'.$currency->id,
            'sign' => 'required',
        ]);
        $currency->update([
            'name' => $request->name,
            'sign' => $request->sign,
        ]);

        return redirect()->route('currency')->with('success','Currency Added Successfully');
    }
    public function delete(Currency $currency)
    {
        Currency::destroy($currency->id);
        return redirect()->route('currency')->with('success','Currency Deleted Successfully');

    }


    public function block_currency(Currency $currency){

        if ($currency->status == Currency::ENABLE){

            $currency->update([
                'status' => Currency::DISABLE
            ]);
            $message = 'Currency Blocked Successfully';

        }else{
            $currency->update([
                'status' => Currency::ENABLE
            ]);
            $message = 'Currency Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
