<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $total_users = User::whereIn('role',[User::ROLE_CANDIDATE,User::ROLE_EMPLOYER,User::ROLE_AGENCY])->count();
        $total_candidates = User::where('role',User::ROLE_CANDIDATE)->count();
        $total_employers = User::where('role',User::ROLE_EMPLOYER)->count();
        $total_agencies = User::where('role',User::ROLE_AGENCY)->count();
        $total_jobs = Job::all()->count();
        $today_employer_subscription = Transaction::where('plan_type',Transaction::PLAN_TYPE_EMPLOYER)
            ->whereDate('created_at', Carbon::today())
            ->sum('amount');
        $today_helper_subscription = Transaction::where('plan_type',Transaction::PLAN_TYPE_CANDIDATE)
            ->whereDate('created_at', Carbon::today())
            ->sum('amount');
        $today_agency_subscription = Transaction::where('plan_type',Transaction::PLAN_TYPE_CANDIDATE)
            ->whereDate('created_at', Carbon::today())
            ->sum('amount');
        $today_sales = Transaction::sum('amount');
        return view('admin.pages.dashboard',compact('total_users','total_jobs',
            'total_candidates','total_employers','total_agencies','total_employers',
            'today_employer_subscription','today_helper_subscription','today_agency_subscription','today_sales'));
    }
}
