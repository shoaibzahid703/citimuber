<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\EmployerCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EmployerCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $employer_categories = EmployerCategory::orderBy('id', 'DESC');

            return Datatables::of($employer_categories)
                ->addIndexColumn()
                ->editColumn('helper_apply_status', function($category){
                    if ($category->helper_apply_status == EmployerCategory::Yes) {
                        return "<div class='badge bg-success'>Yes</div>";
                    } else {
                        return "<div class='badge bg-secondary'>No</div>";
                    }
                })
                ->addColumn('action', function($category) {
                    $deleteUrl = route('employer_category_delete', $category);
                    $editUrl = route('employer_category_edit', $category);

                    $actionHtml = '';
                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';


                    $actionHtml .= '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    return $actionHtml;
                })
                ->rawColumns([ 'helper_apply_status','action'])
                ->make(true);

        }
        return view('admin.pages.employer_category.index');
    }
    public function create()
    {
        return view('admin.pages.employer_category.create');

    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:employer_categories,name',
        ]);
        EmployerCategory::create([
            'name' => $request->name,
            'category' => $request->category,
            'helper_apply_status' => $request->helper_apply ?? 0,
        ]);

        return redirect()->route('employer_categories')->with('success','Category Added Successfully');
    }
    public function edit(EmployerCategory $category)
    {
        return view('admin.pages.employer_category.edit',[
            'category'  => $category
        ]);
    }
    public function update(Request $request,EmployerCategory $category)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $category->update([
            'name' => $request->name,
            'category' => $request->category,
            'helper_apply_status' => $request->helper_apply ?? 0,
        ]);
        return redirect()->route('employer_categories')
            ->with('success', 'Category is updated successfully.');
    }
    public function delete(EmployerCategory $category)
    {
        $category->delete();
        return redirect()->route('employer_categories')
            ->with('success', 'Category is deleted successfully.');
    }
}
