<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EducationLevel;
use Yajra\DataTables\DataTables;

class EducationLevelController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = EducationLevel::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('education_level_edit', $data->id);
                    $deleteUrl = route('education_level_delete', $data->id);
                    $changeStatus = route('block_education_level', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\EducationLevel::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.EducationLevel.index');
    }
    public function create()
    {
        return view('admin.pages.EducationLevel.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:education_levels,name',

        ]);
        EducationLevel::create([
            'name' => $request->name,
        ]);

        return redirect()->route('education_levels')->with('success','EducationLevel Added Successfully');
    }
    public function edit(EducationLevel $education)
    {
        return view('admin.pages.EducationLevel.edit',compact('education'));
    }
    public function update(Request $request, EducationLevel $education)
    {

        $request->validate([
            'name' => 'required|unique:education_levels,name,'.$education->id

        ]);
        $education->update([
            'name' => $request->name,
        ]);

        return redirect()->route('education_levels')->with('success','EducationLevel Added Successfully');
    }
    public function delete(EducationLevel $education)
    {
        EducationLevel::destroy($education->id);
        return redirect()->route('education')->with('success','EducationLevel Deleted Successfully');

    }
    public function block_education_level(EducationLevel $education){

        if ($education->status == EducationLevel::ACTIVE){

            $education->update([
                'status' => EducationLevel::BLOCK
            ]);
            $message = 'EducationLevel Blocked Successfully';

        }else{
            $education->update([
                'status' => EducationLevel::ACTIVE
            ]);
            $message = 'EducationLevel Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
