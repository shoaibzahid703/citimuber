<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\OtherSkill;

class OtherskillsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = OtherSkill::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('other_skills_edit', $data->id);
                    $deleteUrl = route('other_skills_delete', $data->id);
                    $changeStatus = route('blockOtherSkill', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Otherskill::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.other-skills.index');
    }


    public function create()
    {
        return view('admin.pages.other-skills.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:other_skills,name',

        ]);
        OtherSkill::create([
            'name' => $request->name,
        ]);
        return redirect()->route('other_skills')->with('success','Other skill Added Successfully');
    }


    public function edit(OtherSkill $skills)
    {
        return view('admin.pages.other-skills.edit',compact('skills'));
    }
    public function update(Request $request,OtherSkill $skills)
    {
        $request->validate([
            'name' => 'required|unique:other_skills,name,'.$skills->id,
        ]);
        $skills->update([
            'name' => $request->name,
        ]);
        return redirect()->route('other_skills')->with('success','Other skill Added Successfully');
    }
    public function delete(OtherSkill $skills)
    {
        OtherSkill::destroy($skills->id);
        return redirect()->route('other_skills')->with('success','Other skill Deleted Successfully');
    }
    public function blockOtherSkill(OtherSkill $skills){

        if ($skills->status == OtherSkill::ACTIVE){
            $skills->update([
                'status' => OtherSkill::BLOCK
            ]);
            $message = 'Other skill Blocked Successfully';
        }else{
            $skills->update([
                'status' => OtherSkill::ACTIVE
            ]);
            $message = 'Other skill Un Blocked Successfully';
        }
        return redirect()->back()->with('success',$message);
    }
}
