<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Duty;

class JobsController extends Controller
{
    

     public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Duty::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('duties_edit', $data->id);
                    $deleteUrl = route('duties_delete', $data->id);
                    $changeStatus = route('block_duties', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Duty::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.duty.index');
    }


    public function create()
    {
        return view('admin.pages.duty.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:duties,name',

        ]);
        Duty::create([
            'name' => $request->name,
        ]);

        return redirect()->route('duties')->with('success','Duty Added Successfully');
    }


    public function edit(Duty $duties)
    {
        return view('admin.pages.duty.edit',compact('duties'));
    }
    public function update(Request $request, Duty $duties)
    {
        $request->validate([
            'name' => 'required|unique:duties,name,'.$duties->id

        ]);
        $duties->update([
            'name' => $request->name,
        ]);

        return redirect()->route('duties')->with('success','Duty Added Successfully');
    }
    public function delete(Duty $duties)
    {
        Duty::destroy($duties->id);
        return redirect()->route('duties')->with('success','Duty Deleted Successfully');

    }


    public function block_duties(Duty $duties){

        if ($duties->status == Duty::ENABLE){

            $duties->update([
                'status' => Duty::DISABLE
            ]);
            $message = 'Duty Blocked Successfully';

        }else{
            $duties->update([
                'status' => Duty::ENABLE
            ]);
            $message = 'Duty Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
