<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Category;

class CategoryController extends Controller
{
    

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('category_edit', $data->id);
                    $deleteUrl = route('category_delete', $data->id);
                    $changeStatus = route('block_category', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Category::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.categories.index');
    }


    public function create()
    {
        return view('admin.pages.categories.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:categories,name',

        ]);
        Category::create([
            'name' => $request->name,
        ]);

        return redirect()->route('categories')->with('success','Category Added Successfully');
    }


    public function edit(Category $category)
    {
        return view('admin.pages.categories.edit',compact('category'));
    }
    public function update(Request $request,Category $category)
    {
        $request->validate([
            'name' => 'required|unique:categories,name,'.$category->id

        ]);
        $category->update([
            'name' => $request->name,
        ]);

        return redirect()->route('categories')->with('success','Category Added Successfully');
    }
    public function delete(Category $category)
    {
        Category::destroy($category->id);
        return redirect()->route('categories')->with('success','Category Deleted Successfully');

    }


    public function block_category(Category $category){

        if ($category->status == Category::ACTIVE){

            $category->update([
                'status' => Category::BLOCK
            ]);
            $message = 'Category Blocked Successfully';

        }else{
            $category->update([
                'status' => Category::ACTIVE
            ]);
            $message = 'Category Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
