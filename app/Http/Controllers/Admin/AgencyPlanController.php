<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\AgencyPlan;
use App\Models\Plans;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AgencyPlanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AgencyPlan::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

                ->addColumn('status', function($state){

                    if ($state->status == Plans::ENABLE) {
                        return "<div class='badge bg-success'>Enable</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Disable</div>";
                    }
                })


                ->addColumn('action', function($data) {
                    $editUrl = route('agency_plan_edit', $data->id);
                    $deleteUrl = route('agency_plan_delete', $data->id);
                    $changeStatus = route('agency_plan_block', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\AgencyPlan::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['status','role','action'])
                ->make(true);

        }
        return view('admin.pages.agency_plan.index');
    }

    public function create()
    {
        return view('admin.pages.agency_plan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:agency_plans,name',
            'no_of_days'=> 'required',
            'no_of_jobs'=> 'required',
            'no_of_candidates'=> 'required',
            'price'=> 'required',
        ]);
        AgencyPlan::create([
            'name' => $request->name,
            'no_of_days' => $request->no_of_days,
            'no_of_jobs' => $request->no_of_jobs,
            'no_of_candidates' => $request->no_of_candidates,
            'price' => $request->price,
            'description' =>  $request->description,
        ]);

        return redirect()->route('agency_plans')->with('success','Plan Added Successfully');
    }
    public function edit(AgencyPlan $plan)
    {
        return view('admin.pages.agency_plan.edit',compact('plan'));
    }
    public function update(Request $request,AgencyPlan $plan)
    {
        $request->validate([
            'name' => 'required|unique:agency_plans,name,'.$plan->id,
            'no_of_days'=> 'required',
            'no_of_jobs'=> 'required',
            'no_of_candidates'=> 'required',
            'price'=> 'required',
        ]);
        $plan->update([
            'name' => $request->name,
            'no_of_days' => $request->no_of_days,
            'no_of_jobs' => $request->no_of_jobs,
            'no_of_candidates' => $request->no_of_candidates,
            'price' => $request->price,
            'description' =>  $request->description,
        ]);

        return redirect()->route('agency_plans')->with('success','Plan Updated Successfully');
    }
    public function delete(AgencyPlan $plan)
    {
        AgencyPlan::destroy($plan->id);
        return redirect()->route('agency_plans')->with('success','Plan Deleted Successfully');

    }
    public function block_plan(AgencyPlan $plan){

        if ($plan->status == AgencyPlan::ENABLE){
            $plan->update([
                'status' => AgencyPlan::DISABLE
            ]);
            $message = 'Plan Disable Successfully';

        }else{
            $plan->update([
                'status' => AgencyPlan::ENABLE
            ]);
            $message = 'Plan Enable Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
