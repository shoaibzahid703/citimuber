<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TermsAndConditionPage;

class TermsAndConditionController extends Controller
{
    

    public function index(){

     return view('admin.pages.terms_and_condition.index');
   }

   
  public function save_term_and_condition(Request $request)
  {
    $request->validate([
        'description' => 'required',
    ]);

    $terms_and_condition = TermsAndConditionPage::firstOrNew(['id' => 1]);

    $terms_and_condition->description = $request->description;
    
    $terms_and_condition->save();

    return redirect()->back()->with('success', 'Terms & condition is updated successfully.');
  }
}
