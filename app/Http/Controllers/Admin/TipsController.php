<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TipsLandingPage;
use App\Models\Category;
use App\Models\Tips;
use App\Traits\imageUploadTrait;
use Yajra\DataTables\DataTables;

class TipsController extends Controller
{


    use imageUploadTrait;


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Tips::orderBy('id', 'DESC')->get();

            return Datatables::of($data)


            ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

            ->addColumn('tips_image', function($data) {

                $imageUrl = asset('storage/tips_image/' . $data->tips_image);
                return '<img src="' . $imageUrl . '" alt="Tips Image" width="50" height="50">';
            })



                ->addColumn('action', function($data) {
                    $editUrl = route('edit-tips', $data->id);
                    $deleteUrl = route('delete-tips', $data->id);
                    $changeStatus = route('block_tips', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';

                        if ($data->status == \App\Models\Tips::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['tips_image','status','action'])
                ->make(true);

        }

        return view('admin.pages.tips.index');
    }


   public function save_tips_page(Request $request)
   {

    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',

    ]);

    $tips_page = TipsLandingPage::firstOrNew(['id' => 1]);

    $tips_page->first_heading = $request->first_heading;
    $tips_page->paragraph = $request->paragraph;
    $tips_page->description = $request->description;

    $tips_page->save();

    return redirect()->back()->with('success', 'Tips landing page updated successfully.');
   }


   public function save_news_page(Request $request)
  {

    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',

    ]);

    $news_page = NewLandingPage::firstOrNew(['id' => 1]);

    $news_page->first_heading = $request->first_heading;
    $news_page->paragraph = $request->paragraph;
    $news_page->description = $request->description;

    $news_page->save();

    return redirect()->back()->with('success', 'News landing page updated successfully.');
  }


  public function add_tips()
  {
     $categories = Category::where('status', 1)->get();
    return view('admin.pages.tips.create', compact('categories'));
  }

   public function store_tips(Request $request)
    {
        $request->validate([
        'title' => 'required',
        'tips_date' => 'required',
        'category_id' => 'required',
        'tips_image' => 'required',
        'full_details' => 'required',
         ]);


        if($request->tips_image){
            $tips_image = self::uploadFile($request, fileName: 'tips_image', folderName: 'tips_image');
        }
        
        if($request->location_image){
            $location_image = self::uploadFile($request, fileName: 'location_image', folderName: 'tips_image');
        }

       Tips::create([

            'title' => $request->title,
            'tips_date' => $request->tips_date,
            'category_id' => $request->category_id,
            'tips_image' => @$tips_image,
            'location_image' => @$location_image,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'whatsapp_url' => $request->whatsapp_url,
            'linkedIn_url' => $request->linkedIn_url,
            'full_details' => $request->full_details,

        ]);

        return redirect()->route('tips_page')->with('success','Tips Added Successfully');
    }


    public function edit_tips(Tips $tips)
    {
       $categories = Category::where('status', 1)->get();
      return view('admin.pages.tips.edit', compact('tips', 'categories'));
    }


    public function update_tips(Request $request, Tips $tips)
    {
        $request->validate([
        'title' => 'required',
        'tips_date' => 'required',
        'category_id' => 'required',
        'full_details' => 'required',
         ]);

        $TipsEvents = [
            'title' => $request->title,
            'tips_date' => $request->tips_date,
            'category_id' => $request->category_id,
            'full_details' => $request->full_details,
            'facebook_url' => $request->facebook_url,
            'twitter_url' => $request->twitter_url,
            'whatsapp_url' => $request->whatsapp_url,
            'linkedIn_url' => $request->linkedIn_url,
            'tips_image' => $tips->tips_image,
            'location_image' => $tips->location_image
         ];

         if ($request->hasFile('tips_image')) {
             $tips_image = self::uploadFile($request, 'tips_image', 'tips_image');
             $TipsEvents['tips_image'] = $tips_image;
         }
         
         if ($request->hasFile('location_image')) {
             $location_image = self::uploadFile($request, 'location_image', 'tips_image');
             $TipsEvents['location_image'] = $location_image;
         }


         $tips->update($TipsEvents);

         return redirect()->route('tips_page')->with('success', 'Tips Updated Successfully!');
     }



   public function delete_tips(Tips $tips)
    {
         Tips::destroy($tips->id);
        return redirect()->back()->with('success','Tips Deleted Successfully');

    }


    public function block_tips(Tips $tips){

        if ($tips->status == Tips::ACTIVE){

            $tips->update([
                'status' => Tips::BLOCK
            ]);
            $message = 'Tips Block Successfully';

        }else{
            $tips->update([
                'status' => Tips::ACTIVE
            ]);
            $message = 'Tips Un-block Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
