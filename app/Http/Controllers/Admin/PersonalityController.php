<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Personality;

class PersonalityController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Personality::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('personality_edit', $data->id);
                    $deleteUrl = route('personality_delete', $data->id);
                    $changeStatus = route('block_personality', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Personality::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.personality.index');
    }


    public function create()
    {
        return view('admin.pages.personality.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:personalities,name',

        ]);
        Personality::create([
            'name' => $request->name,
        ]);

        return redirect()->route('personality')->with('success','Personality Added Successfully');
    }


    public function edit(Personality $personality)
    {
        return view('admin.pages.personality.edit',compact('personality'));
    }
    public function update(Request $request,Personality $personality)
    {
        $request->validate([
            'name' => 'required|unique:personalities,name,'.$personality->id

        ]);
        $personality->update([
            'name' => $request->name,
        ]);

        return redirect()->route('personality')->with('success','Personality Added Successfully');
    }
    public function delete(Personality $personality)
    {
        Personality::destroy($personality->id);
        return redirect()->route('personality')->with('success','Personality Deleted Successfully');

    }


    public function block_personality(Personality $personality){

        if ($personality->status == Personality::ACTIVE){

            $personality->update([
                'status' => Personality::BLOCK
            ]);
            $message = 'Personality Blocked Successfully';

        }else{
            $personality->update([
                'status' => Personality::ACTIVE
            ]);
            $message = 'Personality Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
