<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Plans;
use App\Models\Country;


class PlansController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Plans::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

            ->addColumn('status', function($state){

                    if ($state->status == Plans::ENABLE) {
                        return "<div class='badge bg-success'>Enable</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Disable</div>";
                    }
                })

              ->addColumn('role', function($state){

                    if ($state->role == 'employer') {
                        return "<div class='badge bg-danger'>Employer</div>";
                    } elseif ($state->role == 'candidate') {
                        return "<div class='badge bg-secondary'>Candidate</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('plans_edit', $data->id);
                    $deleteUrl = route('plans_delete', $data->id);
                    $changeStatus = route('block_plans', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Plans::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['status','role','action'])
                ->make(true);

        }
        return view('admin.pages.plans.index');
    }


    public function create()
    {
        $countries = Country::where('status',Country::ACTIVE)->get();
        return view('admin.pages.plans.create', compact('countries'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:plans,name',
            'post_duration'=> 'required',
            'price'=> 'required',
        ]);
        Plans::create([
            'name' => $request->name,
            'post_duration' => $request->post_duration,
            'price' => $request->price,
            'country' =>  implode(',', $request['country']),
            'manual_price' => implode(',', $request['manual_price']),
            'features' =>    $request->feature_description,
            'reply_messages' =>    $request->reply_messages ?? 0,
        ]);

        return redirect()->route('plans')->with('success','Plan Added Successfully');
    }


    public function edit(Plans $plans)
    {
        $countries = Country::where('status',Country::ACTIVE)->get();
        return view('admin.pages.plans.edit',compact('plans', 'countries'));
    }

    public function update(Request $request,Plans $plans)
    {
        $request->validate([
            'name' => 'required|unique:plans,name,'.$plans->id,
            'post_duration'=> 'required|integer|min:1',
            'price'=> 'required|numeric|min:0',
        ]);
        $plans->update([
            'name' => $request->name,
            'post_duration' => $request->post_duration,
            'price' => $request->price,
            'country' =>  implode(',', $request['country']),
            'manual_price' => implode(',', $request['manual_price']),
            'features' =>    $request->feature_description,
            'reply_messages' =>    $request->reply_messages ?? 0,
        ]);

        return redirect()->route('plans')->with('success','Plans Added Successfully');
    }
    public function delete(Plans $plans)
    {
        Plans::destroy($plans->id);
        return redirect()->route('plans')->with('success','Plans Deleted Successfully');

    }
    public function block_plans(Plans $plan){

        if ($plan->status == Plans::ENABLE){

            $plan->update([
                'status' => Plans::DISABLE
            ]);
            $message = 'Plans Disable Successfully';

        }else{
            $plan->update([
                'status' => Plans::ENABLE
            ]);
            $message = 'Plans Enable Successfully';
        }

        return redirect()->back()->with('success',$message);

    }

}

