<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $transactions = Transaction::with('user','job','employer_plan')->select('transactions.*');

            return Datatables::of($transactions)

                ->addColumn('plan_name', function($transaction){
                    if ($transaction->plan_type == "employer") {
                        return $transaction->employer_plan->name;
                    } else {
                        return "<div class='badge bg-secondary'>-</div>";
                    }
                })

                ->addColumn('user_name', function($transaction){
                    if ($transaction->user) {
                        return $transaction->user->name;
                    } else {
                        return "<div class='badge bg-secondary'>-</div>";
                    }
                })

                ->addColumn('job_name', function($transaction){
                    if ($transaction->job) {
                        return $transaction->job->job_title;
                    } else {
                        return "<div class='badge bg-secondary'>-</div>";
                    }
                })

                ->addColumn('action', function($transaction) {
                    $deleteUrl = route('delete_transaction', $transaction);

                    return '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';
                })

                ->rawColumns(['plan_name','action','job_name','user_name'])
                ->make(true);

        }
        return view('admin.pages.transactions');
    }

    public function delete(Transaction $transaction)
    {
        Transaction::destroy($transaction->id);
        return redirect()->route('all_transactions')->with('success','Transaction Deleted Successfully');

    }
}
