<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactQuery;
use App\Models\Setting;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    use imageUploadTrait;
    public function site_setting(){
        $setting = Setting::first();
        return view('admin.pages.setting',compact('setting'));
    }
    public function site_setting_update(Request $request)
    {
        $request->validate([
            'seo_tags' => 'nullable|max:200',
            'site_description' => 'nullable|max:500',
            'site_logo' => 'nullable|image',
            'site_fav_icon' => 'nullable|image',
            'facebook_profile' => 'nullable|max:200',
            'twitter_profile' => 'nullable|max:200',
            'tiktok_profile' => 'nullable|max:200',
            'pinterest_profile' => 'nullable|max:200',
            'instagram_profile' => 'nullable|max:200',
            'youtube_profile' => 'nullable|max:200',
            'linkedin_profile' => 'nullable|max:200',
        ]);
        $setting = Setting::first();
        if ($setting){
            $site_logo = $setting->site_logo;
            $site_fav_icon = $setting->site_fav_icon;
            if ($request->file('site_logo')){
                $site_logo = self::uploadFile($request, fileName: 'site_logo', folderName: 'site_logo');
            }
            if ($request->file('site_fav_icon')){
                $site_fav_icon = self::uploadFile($request, fileName: 'site_fav_icon', folderName: 'site_fav_icon');
            }
            $setting->update([
                'seo_tags' => $request->seo_tags,
                'site_description' => $request->site_description,
                'site_logo' => $site_logo,
                'site_fav_icon' => $site_fav_icon,
                'facebook_profile' => $request->facebook_profile,
                'twitter_profile' => $request->twitter_profile,
                'tiktok_profile' => $request->tiktok_profile,
                'pinterest_profile' => $request->pinterest_profile,
                'instagram_profile' => $request->instagram_profile,
                'youtube_profile' => $request->youtube_profile,
                'linkedin_profile' => $request->linkedin_profile,
            ]);
        }else{
            $site_logo = null;
            $site_fav_icon = null;

            if ($request->file('site_logo')){
                $site_logo = self::uploadFile($request, fileName: 'site_logo', folderName: 'site_logo');
            }
            if ($request->file('site_fav_icon')){
                $site_fav_icon = self::uploadFile($request, fileName: 'site_fav_icon', folderName: 'site_fav_icon');
            }
            Setting::create([
                'seo_tags' => $request->seo_tags,
                'site_description' => $request->site_description,
                'site_logo' => $site_logo,
                'site_fav_icon' => $site_fav_icon,
                'facebook_profile' => $request->facebook_profile,
                'twitter_profile' => $request->twitter_profile,
                'tiktok_profile' => $request->tiktok_profile,
                'pinterest_profile' => $request->pinterest_profile,
                'instagram_profile' => $request->instagram_profile,
                'youtube_profile' => $request->youtube_profile,
                'linkedin_profile' => $request->linkedin_profile,
            ]);
        }


        return redirect()->back()->with('success','setting updated successfully');

    }


    public function contact_us_settings(){
        $setting = Setting::first();
        return view('admin.pages.contact_us_setting',compact('setting'));
    }

    public function contact_us_setting_update(Request $request)
    {
        $request->validate([
            'contact_us_email' => 'nullable|max:200',
            'contact_us_number' => 'nullable|max:500',
            'contact_us_address' => 'nullable|max:200',
        ]);
        $setting = Setting::first();
        if ($setting){
            $setting->update([
                'contact_us_email' => $request->contact_us_email,
                'contact_us_number' => $request->contact_us_number,
                'contact_us_address' => $request->address,
            ]);
        }else{
            Setting::create([
                'contact_us_email' => $request->contact_us_email,
                'contact_us_number' => $request->contact_us_number,
                'contact_us_address' => $request->address,
            ]);
        }
        return redirect()->back()->with('success','setting updated successfully');
    }
}
