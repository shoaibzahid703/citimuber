<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PaymentMethodController extends Controller
{
    

    public function payment_method(){
        return view('admin.pages.payment_method.index');
    }


    public function update(Request $request)
    {
        $request->validate([
            'STRIPE_KEY' => 'required|string',
            'STRIPE_SECRET' => 'required|string',
        ]);

        $this->updateEnv([
            'STRIPE_KEY' => $request->STRIPE_KEY,
            'STRIPE_SECRET' => $request->STRIPE_SECRET,
        ]);

        return back()->with('success', 'Environment keys updated successfully!');
    }

    protected function updateEnv(array $data)
    {
        $envPath = base_path('.env');

        if (File::exists($envPath)) {
            $env = File::get($envPath);

            foreach ($data as $key => $value) {
                $env = preg_replace('/^' . $key . '=.*/m', $key . '=' . $value, $env);
            }

            File::put($envPath, $env);
        }
    }
}
