<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BioData;
use App\Models\City;
use App\Models\CookingSkill;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Duty;
use App\Models\Education;
use App\Models\EducationLevel;
use App\Models\EmployerCategory;
use App\Models\HelperCategory;
use App\Models\Job;
use App\Models\JobPicture;
use App\Models\JobPosition;
use App\Models\Language;
use App\Models\MainSkill;
use App\Models\OtherSkill;
use App\Models\Personality;
use App\Models\Plans;
use App\Models\Religion;
use App\Models\State;
use App\Models\Transaction;
use App\Models\UserEducation;
use App\Models\UserWorkExperience;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;


class UserController extends Controller
{

    use imageUploadTrait;

    public function delete_user(User $user){
        User::destroy($user->id);
        return redirect()->back()->with('success','Record Deleted Successfully');
    }


    public function blockUser(User $user){

        if ($user->status == User::ACTIVE){

            $user->update([
                'status' => User::BLOCK
            ]);
            $message = 'User Blocked Successfully';

        }else{
            $user->update([
                'status' => User::ACTIVE
            ]);
            $message = 'User Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }


    public function all_candidates(Request $request)
    {
        if ($request->ajax()) {
            $candidates = User::with('helper_category')
                ->where('role', User::ROLE_CANDIDATE)
                ->orderBy('id', 'DESC');
            return Datatables::of($candidates)
                ->addIndexColumn()

                ->addColumn('status', function($candidate){
                    if ($candidate->status == User::ACTIVE) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('helper_category_name', function($employer){
                    if ($employer->helper_category) {
                        return $employer->helper_category->name;
                    }else{
                        return '-';
                    }
                })

                ->addColumn('action', function($candidate) {
                    $changeStatus = route('blockUser', $candidate);
                    $deleteUrl = route('delete_user', $candidate);
                    $editUrl = route('admin_edit_candidate', $candidate);
                    $assign_category_url = route('assign_helper_category', $candidate);
                    $candidate_bio_data_url = route('admin_candidate_bio_data', $candidate);



                    $actionHtml = '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-warning btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a> &nbsp;&nbsp;';

                    $actionHtml .= '<a href="'.$assign_category_url.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-arrow-right"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';



                    if ($candidate->status == \App\Models\User::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    $actionHtml .= ' &nbsp;&nbsp; <a href="'.$candidate_bio_data_url.'" class="edit btn btn-gray btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-copy"></em>
                                       </span>
                                    </a> ';




                    return $actionHtml;
                })

                ->rawColumns(['status','action','helper_category_name'])
                ->make(true);

        }
        return view('admin.pages.users.candidates');
    }

    public function add_candidate()
    {
        return view('admin.pages.users.add_candidate');
    }
    public function save_candidate(Request $request)
    {
        $request->validate([
            'name' =>   'required|string|max:255',
            'email' =>   'required|email|unique:users,email',
            'password' =>   'required',
        ]);


        User::create([
            'role' => User::ROLE_CANDIDATE,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('all_candidates')->with('success','Candidate added successfully');
    }

    public function edit_candidate(User $candidate)
    {
        return view('admin.pages.users.edit-candidate',compact('candidate'));
    }

    public function update_candidate(Request $request,User $candidate)
    {
        $request->validate([
            'name' =>   'required|string|max:255',
            'email' =>   'required|email|unique:users,email,'.$candidate->id,
            'password' =>   'sometimes',
        ]);
        if ($request->password){
            $candidate->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            return redirect()->route('all_candidates')->with('success','Candidate update successfully');
        }else{
            $candidate->update([
                'name' => $request->name,
                'email' => $request->email
            ]);

            return redirect()->route('all_candidates')->with('success','Candidate update successfully');
        }
    }

    public function candidate_bio_data(User $candidate)
    {
        $state = State::where('status',State::ACTIVE)->get();
        $language = Language::all();
        $country = Country::where('status',Country::ACTIVE)->get();
        $city = City::where('status',City::ACTIVE)->get();
        $main_skills = MainSkill::where('status',MainSkill::ACTIVE)->get();
        $cookingSkills = CookingSkill::where('status',CookingSkill::ACTIVE)->get();
        $otherSkills = OtherSkill::where('status',OtherSkill::ACTIVE)->get();
        $personality = Personality::where('status',Personality::ACTIVE)->get();
        $duties = Duty::where('status',Duty::ENABLE)->get();
        $currency = Currency::where('status',Currency::ENABLE)->get();
        $nationalities = Country::whereNotNull('nationality')->where('status',Country::ACTIVE)->get();

        $job_positions = JobPosition::where('status', 1)->get();
        $educations = Education::where('status', 1)->get();

        $religions = Religion::where('status', 1)->get();
        $education_levels = EducationLevel::where('status', 1)->get();

        $candidate->load('resume_detail','working_experience','education');

        $count_experience = $candidate->working_experience->count();
        $count_education = $candidate->education->count();
        $next_experience_iteration = 0;
        $next_education_iteration = 0;
        if ($candidate->working_experience && $count_experience > 0){
            $next_experience_iteration = $count_experience + 1;
            if ($next_experience_iteration != 1){
                $next_experience_iteration = $next_experience_iteration - 1;
            }
        }
        if ($candidate->education && $count_education > 0){
            $next_education_iteration = $count_education + 1;
            if ($next_education_iteration != 1){
                $next_education_iteration = $next_education_iteration - 1;
            }
        }

        return view('admin.pages.users.candidate_bio_data', compact('nationalities','candidate','next_experience_iteration','next_education_iteration','state','country', 'language', 'city', 'main_skills', 'cookingSkills', 'personality', 'otherSkills','duties', 'currency', 'job_positions', 'educations', 'religions', 'education_levels'));


    }

    public function candidate_bio_data_save(Request $request,User $candidate)
    {
        $candidate->load('resume_detail','working_experience','education');
        if($request->profile_image){
            $profile = self::uploadFile($request, fileName: 'profile_image', folderName: 'profile_image');
            $candidate->update([
                'profile_image' =>  $profile,
            ]);
        }
        if ($request->mobile_number){
            $candidate->update([
                'mobile' =>  $request->formatted_mobile
            ]);
        }



        if ($candidate->resume_detail){
            $candidate->resume_detail->update([
                'first_name' =>         $request->first_name,
                'middle_name'=>         $request->middle_name,
                'last_name' =>          $request->last_name,
                'age' =>                $request->age,
                'gender' =>             $request->gender,
                'marital_status' =>     $request->marital_status,
                'kids_detail' =>        $request->kids_detail,
                'nationality' =>        $request->nationality,
                'present_country' =>    $request->present_country,
                'religion' =>           $request->religion,
                'education_level' =>    $request->education_level,
                'whatsapp_number' =>    $request->formatted_whatsapp_number,
                'valid_password' =>     $request->valid_password,
                'position_apply' =>     $request->position_apply,
                'work_experience' =>    $request->work_experience,
                'job_type' =>           $request->job_type,

                'work_status' =>        $request->work_status,
                'job_start_date' =>     $request->job_start_date,

                'job_start_date' =>     Carbon::parse($request->job_start_date)->toDate() ,

                'preferred_job_location' =>    implode(',', $request['preferred_job_location']),
                'monthly_salary' =>      $request->monthly_salary,
                'currency' =>            $request->currency,
                'day_off_preference' =>  $request->day_off_preference,
                'accomodation_preference' =>    $request->accomodation_preference,
                'languages' =>    implode(',', $request['languages']),
                'main_skills' =>    implode(',', $request['main_skills']),
                'cooking_skills' =>    implode(',', $request['cooking_skills']),
                'other_skills' =>    implode(',', $request['other_skills']),
                'personalities' =>    implode(',', $request['personalities']),
                'resume_description' =>    $request->resume_description,
                'newsletter' =>    $request->newsletter,
                'opportunities' =>    $request->opportunities,
            ]);
        }else{
            BioData::create([
                'user_id' =>            $candidate->id,
                'first_name' =>         $request->first_name,
                'middle_name'=>         $request->middle_name,
                'last_name' =>          $request->last_name,
                'age' =>                $request->age,
                'gender' =>             $request->gender,
                'marital_status' =>     $request->marital_status,
                'kids_detail' =>        $request->kids_detail,
                'nationality' =>        $request->nationality,
                'present_country' =>    $request->present_country,
                'religion' =>           $request->religion,
                'education_level' =>    $request->education_level,
                'whatsapp_number' =>    $request->formatted_whatsapp_number,
                'valid_password' =>     $request->valid_password,
                'position_apply' =>     $request->position_apply,
                'work_experience' =>    $request->work_experience,
                'job_type' =>           $request->job_type,

                'work_status' =>        $request->work_status,
                'job_start_date' =>     $request->job_start_date,

                'job_start_date' =>     Carbon::parse($request->job_start_date)->toDate() ,

                'preferred_job_location' =>    implode(',', $request['preferred_job_location']),
                'monthly_salary' =>      $request->monthly_salary,
                'currency' =>            $request->currency,
                'day_off_preference' =>  $request->day_off_preference,
                'accomodation_preference' =>    $request->accomodation_preference,
                'languages' =>    implode(',', $request['languages']),
                'main_skills' =>    implode(',', $request['main_skills']),
                'cooking_skills' =>    implode(',', $request['cooking_skills']),
                'other_skills' =>    implode(',', $request['other_skills']),
                'personalities' =>    implode(',', $request['personalities']),
                'resume_description' =>    $request->resume_description,
                'newsletter' =>    $request->newsletter,
                'opportunities' =>    $request->opportunities,
            ]);
        }

        if ($candidate->working_experience){
            if (isset($request['job_position'])){
                foreach ($request['job_position'] as $key => $val){
                    $exp_data = [
                        'job_position' => $val,
                        'working_country' => isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' => isset($request['start_date'][$key]) ? Carbon::parse($request['start_date'][$key])->toDate() : null,
                        'job_end_date' => isset($request['job_end_date'][$key]) ? Carbon::parse($request['job_end_date'][$key])->toDate() : null,
                        'employer_type' => isset($request['employer_type'][$key]) ? $request['employer_type'][$key] : null,
                        'family_type' =>    isset($request['family_type'][$key]) ? $request['family_type'][$key] : null,
                        'employer_nationality' =>    isset($request['employer_nationality'][$key]) ? $request['employer_nationality'][$key] : null,
                        'job_duties' => isset($request['job_duties'][$key]) ? implode(',', $request['job_duties'][$key]) : null,
                        'reference_letter' => isset($request['reference_letter'][$key]) ? $request['reference_letter'][$key] : null,
                    ];
                    UserWorkExperience::updateOrCreate([
                        'user_id' => $candidate->id,
                        'job_position' => $val,
                        'working_country' => isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' => $request['start_date'][$key],
                        'job_end_date' => $request['job_end_date'][$key],
                    ], $exp_data);
                }
            }
        }else{
            if (isset($request['job_position'])){
                foreach ($request['job_position'] as $key => $val){
                    UserWorkExperience::create([
                        'user_id' =>      $candidate->id,
                        'job_position' =>    $val,
                        'working_country' =>   isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' =>    isset($request['start_date'][$key]) ? Carbon::parse($request['start_date'][$key])->toDate() :null,
                        'job_end_date' =>    isset($request['job_end_date'][$key]) ? Carbon::parse($request['job_end_date'][$key])->toDate() : null,
                        'employer_type' =>    isset($request['employer_type'][$key]) ? $request['employer_type'][$key] : null,
                        'family_type' =>    isset($request['family_type'][$key]) ? $request['family_type'][$key] : null,
                        'employer_nationality' =>    isset($request['employer_nationality'][$key]) ? $request['employer_nationality'][$key] : null,
                        'job_duties' =>   isset($request['job_duties'][$key]) ? implode(',',$request['job_duties'][$key])  :null,
                        'reference_letter' =>    isset($request['reference_letter'][$key]) ? $request['reference_letter'][$key]: null,
                    ]);
                }
            }
        }

        if ($candidate->education){
            if (isset($request['education'])){
                foreach ($request['education'] as $key => $val){
                    $edu_data = [
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'course_duration' =>    isset($request['course_duration'][$key]) ? $request['course_duration'][$key] : null,
                        'completed_this_course' =>   isset($request['completed_this_course'][$key]) ? $request['completed_this_course'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ];

                    UserEducation::updateOrCreate([
                        'user_id' =>      $candidate->id,
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ], $edu_data);
                }
            }
        }else{
            if (isset($request['education'])){
                foreach ($request['education'] as $key => $val){
                    UserEducation::create([
                        'user_id' =>     $candidate->id,
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'course_duration' =>    isset($request['course_duration'][$key]) ? $request['course_duration'][$key] : null,
                        'completed_this_course' =>   isset($request['completed_this_course'][$key]) ? $request['completed_this_course'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ]);
                }
            }
        }

        return redirect()->route('all_candidates')->with('success','Candidate bio data update successfully');

    }

    public function validateBioDataStepOne(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|max:100',
            'middle_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'age' => 'required|integer|max:100',
            'gender' => 'required',
            'marital_status' => 'required',
            'kids_detail' => 'required',
            'nationality' => 'required',
            'present_country' => 'required',
            'religion' => 'required',
            'education_level' => 'required',
            'mobile_number' => 'required',
            'whatsapp_number' => 'required',
            'valid_password' => 'required',
        ];
        $validator = Validator::make( $request->all(), $rules);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function validateBioDataStepTwo(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'position_apply' => 'required|string|max:100',
            'work_experience' => 'required|string|max:100',
            'job_type' => 'required|string|max:100',
            'work_status' => 'required|string|max:100',
            'job_start_date' => 'required|string|max:100',
            'preferred_job_location' => 'required',
            'monthly_salary' => 'required',
            'currency' => 'required',
            'day_off_preference' => 'required',
            'accomodation_preference' => 'required',
            'languages' => 'required',
            'main_skills' => 'required',
            'cooking_skills' => 'required',
            'other_skills' => 'required',
            'personalities' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function validateBioDataStepThree(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'job_position' => 'required',
            'working_country' => 'required',
            'start_date' => 'required|string|max:100',
            'job_end_date' => 'required|string|max:100',
            'employer_type' => 'required',
            'family_type' => 'required',
            'employer_nationality' => 'required',
            'job_duties' => 'required',
            'reference_letter' => 'required',
            'education' => 'required',
            'course_duration' => 'required',
            'completed_this_course' => 'required',
            'year_completion' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function validateBioDataStepFour(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'resume_description' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function delete_experience(UserWorkExperience $experience)
    {
        $experience->delete();

        return redirect()->back()->with('success', 'Experience Deleted Successfully!');
    }

    public function delete_education(UserEducation $education)
    {
        $education->delete();
        return redirect()->back()->with('success', 'Education Deleted Successfully!');
    }

    public function add_employer()
    {
        return view('admin.pages.users.add_employer');
    }
    public function save_employer(Request $request)
    {
        $request->validate([
            'name' =>   'required|string|max:255',
            'email' =>   'required|email|unique:users,email',
            'password' =>   'required',
        ]);


        User::create([
            'role' => User::ROLE_EMPLOYER,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('all_employers')->with('success','Employer added successfully');
    }

    public function edit_employer(User $employer)
    {
        return view('admin.pages.users.edit-employer',compact('employer'));
    }

    public function update_employer(Request $request,User $employer)
    {
        $request->validate([
            'name' =>   'required|string|max:255',
            'email' =>   'required|email|unique:users,email,'.$employer->id,
            'password' =>   'sometimes',
        ]);
        if ($request->password){
            $employer->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            return redirect()->route('all_employers')->with('success','Employer update successfully');
        }else{
            $employer->update([
                'name' => $request->name,
                'email' => $request->email
            ]);

            return redirect()->route('all_employers')->with('success','Employer update successfully');
        }
    }
    public function admin_add_employer_job(Request $request,User $employer)
    {
        $state = State::where('status',State::ACTIVE)->get();
        $language = Language::all();
        $country = Country::where('status',Country::ACTIVE)->get();
        $city = City::where('status',City::ACTIVE)->get();
        $main_skills = MainSkill::where('status',MainSkill::ACTIVE)->get();
        $cookingSkills = CookingSkill::where('status',CookingSkill::ACTIVE)->get();
        $otherSkills = OtherSkill::where('status',OtherSkill::ACTIVE)->get();
        $personality = Personality::where('status',Personality::ACTIVE)->get();
        $duties = Duty::where('status',Duty::ENABLE)->get();
        $currency = Currency::where('status',Currency::ENABLE)->get();
        $nationalities = Country::whereNotNull('nationality')->where('status',Country::ACTIVE)->get();
        $job_pictures = JobPicture::all();
        $education_levels = EducationLevel::where('status', 1)->get();


        return view('admin.pages.employer_jobs.add_job',compact('nationalities','state',
            'country', 'language', 'city', 'main_skills', 'cookingSkills',
            'personality', 'otherSkills','duties', 'currency','job_pictures',
            'education_levels','employer'));

    }

    public function validateJobStepOne(Request $request)
    {

        $rules = [
            'position_offered' => 'required',
            'job_type' => 'required',
            'offer_location' => 'required',
            'job_start_date' => 'required',
            'start_date_flexibility' => 'required',
            'languages' => 'required',
            'main_skills' => 'required',
            'cooking_skills' => 'required',
            'other_skills' => 'required',
            'prefer_candidate_location' => 'required',
            'contract_status' => 'required',
            'gender' => 'required',
            'preferred_candidate_nationality' => 'required',
            'education_level' => 'required',

        ];

        $offerLocation = $request->input('offer_location');
        $stateId = $request->input('state');

        $rules['state'] = 'nullable';
        $rules['city'] = 'nullable';

        if ($offerLocation) {

            $country = Country::find($offerLocation);

            if ($country && $country->states()->count() > 0) {
                $rules['state'] = 'required';

                if ($stateId) {
                    $state = State::find($stateId);

                    if ($state && $state->cities()->count() > 0) {
                        $rules['city'] = 'required';
                    }
                }
            }
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ]);
        } else {
            return response()->json([
                'error'    => false,
            ]);
        }
    }
    public function validateJobStepTwo(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'employer_type' => 'required',
            'day_off' => 'required',
            'accomodation' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }
    public function validateJobStepThree(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'job_title' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function save_job(Request $request,User $employer)
    {
        $data = $request->all();

         Job::create([
            'user_id'  => $employer->id,
            'position_offered' => $data['position_offered'] ?? null,
            'job_type' => $data['job_type'] ?? null,
            'offer_location' => $data['offer_location'] ?? null,
            'state' => $data['state'] ?? null,
            'city' => $data['city'] ?? null,
            'job_start_date' => $data['job_start_date'] ?? null,
            'start_date_flexibility' => $data['start_date_flexibility'] ?? null,
            'languages' =>   implode(",",$data['languages']),
            'main_skills' =>   implode(",",$data['main_skills']) ,
            'cooking_skills' =>   implode(",",$data['cooking_skills']),
            'other_skills' =>  implode(",",$data['other_skills']) ,
            'prefer_candidate_location' => $data['prefer_candidate_location'] ?? null,
            'contract_status' => $data['contract_status'] ?? null,
            'gender' => $data['gender'] ?? null,
            'preferred_candidate_nationality' =>  implode(",",$data['preferred_candidate_nationality']) ,
            'education_level' => $data['education_level'] ?? null,
            'preferred_age' => $data['preferred_age'] ?? null,
            'preferred_experience' => $data['preferred_experience'] ?? null,
            'employer_type' => $data['employer_type'] ?? null,
            'receive_email' => $data['receive_email'] ?? null,
            'notify_email' => $data['notify_email'] ?? null,
            'family_type' => $data['family_type'] ?? null,
            'pets' => $data['pets'] ?? null,
            'employer_nationality' => $data['employer_nationality'] ?? null,
            'day_off' => $data['day_off'] ?? null,
            'accomodation' => $data['accomodation'] ?? null,
            'salary_offer' => $data['salary_offer'] ?? null,
            'range_min_salary' => $data['range_min_salary'] ?? null,
            'range_max_salary' => $data['range_max_salary'] ?? null,
            'range_currency' => $data['range_currency'] ?? null,
            'fix_monthly_salary' => $data['fix_monthly_salary'] ?? null,
            'fix_currency' => $data['fix_currency'] ?? null,
            'salary_description' => $data['salary_description'] ?? null,
            'job_picture' => $data['job_picture'] ?? null,
            'job_title' => $data['job_title'] ?? null,
            'job_description' => $data['job_description'] ?? null,
            'newsletter' => $data['newsletter'] ?? null,
            'opportunities' => $data['opportunities'] ?? null,
        ]);
        return redirect()->route('all_jobs')->with('success','Job successfully saved.');
    }

    public function all_employers(Request $request)
    {
        if ($request->ajax()) {
            $employers = User::with('employer_category')
                ->withCount('jobs')
                ->where('role', User::ROLE_EMPLOYER)
                ->orderBy('id', 'DESC');
            return Datatables::of($employers)
                ->addColumn('status', function($employer){
                    if ($employer->status == User::ACTIVE) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })
                ->addColumn('employer_category_name', function($employer){
                    if ($employer->employer_category) {
                        return $employer->employer_category->name;
                    }else{
                        return '-';
                    }
                })

                ->addColumn('jobs_count', function ($row) {
                    return $row->jobs_count; // Ensure jobs_count is accessible
                })

                ->addColumn('action', function($employer) {
                    $changeStatus = route('blockUser', $employer);
                    $deleteUrl = route('delete_user', $employer);
                    $assign_category_url = route('assign_category', $employer);
                    $editUrl = route('admin_edit_employer', $employer);
                    $add_job_url = route('admin_add_employer_job', $employer);

                    $actionHtml = '<div class="btn-group">';

                    $actionHtml .= '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-warning btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a> &nbsp;&nbsp;';

                    $actionHtml .= '<a href="'.$assign_category_url.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-arrow-right"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    if ($employer->status == \App\Models\User::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    $actionHtml .= '&nbsp; <a href="'.$add_job_url.'" class="edit btn btn-gray btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-layers-fill"></em>
                                       </span>
                                    </a> </div>';
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.users.employers');
    }




    public function assign_category(User $user)
    {
        $categories = EmployerCategory::all();
        return view('admin.pages.users.assign_category',compact('user','categories'));
    }
    public function assign_helper_category(User $user)
    {
        $categories = HelperCategory::all();
        return view('admin.pages.users.assign_helper_category',compact('user','categories'));
    }

    public function save_employer_category(Request $request,User $user)
    {
        $user->update([
            'employer_category_id' => $request->employer_category_id
        ]);
        return redirect()->route('all_employers')->with('success','Category Assign Successfully');

    }
    public function save_helper_category(Request $request,User $user)
    {
        $user->update([
            'helper_category_id' => $request->helper_category_id
        ]);
        return redirect()->route('all_candidates')->with('success','Category Assign Successfully');

    }

    public function all_agencies(Request $request)
    {
        if ($request->ajax()) {
            $employers = User::withCount('jobs')
                ->where('role', User::ROLE_AGENCY)
                ->orderBy('id', 'DESC');
            return Datatables::of($employers)
                ->addColumn('status', function($employer){
                    if ($employer->status == User::ACTIVE) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })


                ->addColumn('action', function($employer) {
                    $changeStatus = route('blockUser', $employer);
                    $deleteUrl = route('delete_user', $employer);
//                    $assign_category_url = route('assign_category', $employer);
//                    $editUrl = route('admin_edit_employer', $employer);
//                    $add_job_url = route('admin_add_employer_job', $employer);

                    $actionHtml = '<div class="btn-group">';

                    $actionHtml .= '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

//                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-warning btn-sm">
//                                       <span class="nk-menu-icon text-white">
//                                           <em class="icon ni ni-edit"></em>
//                                       </span>
//                                    </a> &nbsp;&nbsp;';
//
//                    $actionHtml .= '<a href="'.$assign_category_url.'" class="edit btn btn-primary btn-sm">
//                                       <span class="nk-menu-icon text-white">
//                                           <em class="icon ni ni-arrow-right"></em>
//                                       </span>
//                                    </a>&nbsp;&nbsp;';

                    if ($employer->status == \App\Models\User::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

//                    $actionHtml .= '&nbsp; <a href="'.$add_job_url.'" class="edit btn btn-gray btn-sm">
//                                       <span class="nk-menu-icon text-white">
//                                           <em class="icon ni ni-layers-fill"></em>
//                                       </span>
//                                    </a> </div>';

                    $actionHtml .= '&nbsp;  </div>';
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.users.agencies');
    }
}
