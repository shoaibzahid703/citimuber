<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Job;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class JobController extends Controller
{
    public function all_jobs(Request $request)
    {
        if ($request->ajax()) {
            $query = Job::orderBy('id', 'DESC');

            if ($request->user_id) {
                $query->where('user_id', $request->user_id);
            }

            if ($request->job_title) {
                $query->where('job_title', 'LIKE', "%{$request->job_title}%");
            }

            if ($request->offer_location) {
                $query->where('offer_location', $request->offer_location);
            }

            if ($request->position_offered) {
                $query->where('position_offered', $request->position_offered);
            }

            $data = $query->get();

            return Datatables::of($data)

                ->addColumn('user_id', function($data){

                    return $data->userDetail ? $data->userDetail->name : 'N/A';
                })

                ->addColumn('offer_location', function($data){

                    return $data->countryName ? $data->countryName->name : 'N/A';
                })
                ->addColumn('status', function($data){

                    $jobPlan = $data->jobPlan;
                    $postDuration = $jobPlan ? $jobPlan->post_duration : 0;
                    $seenPost = $jobPlan ? $jobPlan->seen_post : 0;
                    $creationDate = \Carbon\Carbon::parse($data->created_at);
                    $expirationDate = $creationDate->addDays($postDuration);
                    $currentDate = \Carbon\Carbon::now();

                    $clickCount = \DB::table('clicks')->where('job_id', $data->id)->count();

                    $isExpiringSoon = false;
                    if ($expirationDate->diffInDays($currentDate) == 1) {
                        $isExpiringSoon = true;
                    } elseif ($clickCount >= $seenPost - 2) {
                        $isExpiringSoon = true;
                    }

                    if ($data->status == 'publish') {
                        if ($isExpiringSoon) {
                            return "<div class='badge bg-warning'>Expires soon</div>";
                        }
                        return "<div class='badge bg-success'>Publish</div>";
                    } elseif ($data->status == 'expire') {
                        return "<div class='badge bg-danger'>Expired</div>";
                    } elseif ($data->status == 'unpublish') {
                        return "<div class='badge bg-secondary'>Un-publish</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $jobUrl = route('job_details', $data->id);
                    $deleteUrl = route('delete_job', $data->id);
                    $changeStatus = route('block_job', $data->id);


                    $actionHtml = '<a href="'.$jobUrl.'" class="edit btn btn-primary btn-sm" target="_blank">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-eye"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>



                                    ';

                    if ($data->status == 'publish') {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['user_id','offer_location','status','action'])
                ->make(true);

        }
        $users = User::where('role', User::ROLE_EMPLOYER)->get();
        return view('admin.pages.employer_jobs.index',compact('users'));
    }

    public function block_job(Job $job){

        if ($job->status == Job::PUBLISH){

            $job->update([
                'status' => Job::EXPIRED
            ]);
            $message = 'Job Expire successfully';

        }else{
            $job->update([
                'status' => Job::PUBLISH
            ]);
            $message = 'Job publish successfully';
        }

        return redirect()->back()->with('success',$message);

    }

    public function delete_job(Job $job)
    {
        Job::destroy($job->id);
        return redirect()->back()->with('success','Job Deleted Successfully');

    }
}
