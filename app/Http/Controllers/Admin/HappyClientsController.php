<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HappyHelpersPage;
use App\Traits\imageUploadTrait;

class HappyClientsController extends Controller
{

    use imageUploadTrait;
    

    public function index(){

     return view('admin.pages.happy_helpers_page.index');
   }

   
  public function save_happy_helpers(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'second_heading' => 'required',
        'first_text' => 'required',
        'first_paragraph' => 'required',
        'register_button' => 'required',
        'second_paragraph' => 'required',
        'third_heading' => 'required',
        'third_paragraph' => 'required',
        'fourth_heading' => 'required',
        'second_text' => 'required',
        'contact_us_button' => 'required',
        
    ]);

    $happy_clients_page = HappyHelpersPage::firstOrNew(['id' => 1]);

    $happy_clients_page->first_heading = $request->first_heading;
    $happy_clients_page->second_heading = $request->second_heading;
    $happy_clients_page->first_text = $request->first_text;
    $happy_clients_page->first_paragraph = $request->first_paragraph;
    $happy_clients_page->register_button = $request->register_button;
    $happy_clients_page->second_paragraph = $request->second_paragraph;
    $happy_clients_page->third_heading = $request->third_heading;
    $happy_clients_page->third_paragraph = $request->third_paragraph;
    $happy_clients_page->fourth_heading = $request->fourth_heading;
    $happy_clients_page->second_text = $request->second_text;
    $happy_clients_page->contact_us_button = $request->contact_us_button;
    

    if ($request->hasFile('bg_image')) {
        $bg_image = self::uploadFile($request, 'bg_image', 'happy_clients_page');
        $happy_clients_page->bg_image = $bg_image;
    }
                         
    if ($request->hasFile('first_image')) {
        $first_image = self::uploadFile($request, 'first_image', 'happy_clients_page');
        $happy_clients_page->first_image = $first_image;
    }

    if ($request->hasFile('sec_image')) {
        $sec_image = self::uploadFile($request, 'sec_image', 'happy_clients_page');
        $happy_clients_page->sec_image = $sec_image;
    }

    if ($request->hasFile('app_store_image')) {
        $app_store_image = self::uploadFile($request, 'app_store_image', 'happy_clients_page');
        $happy_clients_page->app_store_image = $app_store_image;
    }

    if ($request->hasFile('google_play_image')) {
        $google_play_image = self::uploadFile($request, 'google_play_image', 'happy_clients_page');
        $happy_clients_page->google_play_image = $google_play_image;
    }

    $happy_clients_page->save();

    return redirect()->back()->with('success', 'Happy Clients is updated successfully.');
  }
}
