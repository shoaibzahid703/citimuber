<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\MainSkill;

class SkillsController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $main_skills = MainSkill::orderBy('id', 'DESC')->get();

            return Datatables::of($main_skills)
                ->addColumn('status', function($main_skill){
                    if ($main_skill->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($main_skill) {
                    $editUrl = route('main_skills_edit', $main_skill);
                    $deleteUrl = route('main_skills_delete', $main_skill);
                    $changeStatus = route('blockMainSkill', $main_skill);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($main_skill->status == \App\Models\MainSkill::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.main-skills.index');
    }


    public function create()
    {
        return view('admin.pages.main-skills.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:main_skills,name',
        ]);
        MainSkill::create([
            'name' => $request->name,
            'status' => $request->status ?? 1,
        ]);
        return redirect()->route('main_skills')->with('success','Main Skill Added Successfully');
    }


    public function edit(MainSkill $skills)
    {
        return view('admin.pages.main-skills.edit',compact('skills'));
    }
    public function update(Request $request,MainSkill $skills)
    {
        $request->validate([
            'name' => 'required|unique:main_skills,name,'.$skills->id,

        ]);
        $skills->update([
            'name' => $request->name,
        ]);

        return redirect()->route('main_skills')->with('success','Main Skill Added Successfully');
    }
    public function delete(MainSkill $skills)
    {
        MainSkill::destroy($skills->id);
        return redirect()->route('main_skills')->with('success','Main Skill Deleted Successfully');

    }


    public function blockMainSkill(MainSkill $skills){

        if ($skills->status == MainSkill::ACTIVE){

            $skills->update([
                'status' => MainSkill::BLOCK
            ]);
            $message = 'Main Skill Blocked Successfully';

        }else{
            $skills->update([
                'status' => MainSkill::ACTIVE
            ]);
            $message = 'Main Skill Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
