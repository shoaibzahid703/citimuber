<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreState;
use App\Http\Requests\UpdateState;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CityController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $cities =City::with('state')
                ->has('state')
                ->orderBy('id', 'DESC');

            return Datatables::of($cities)
                ->addColumn('status', function($city){
                    if ($city->status == City::ACTIVE) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })
                ->addColumn('state_name', function($city){
                    return $city->state->name;
                })
                ->addColumn('action', function($city) {
                    $block_url= route('city-block', $city);
                    $un_block_url= route('city-un_block', $city);
                    $deleteUrl = route('city-delete', $city);
                    $editUrl = route('city.view', $city);

                    $actionHtml = '';
                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';


                    $actionHtml .= '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    if ($city->status == City::ACTIVE) {
                        $actionHtml .= '<a href="'.$block_url.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$un_block_url.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })
                ->rawColumns(['role', 'status','action','state_name'])
                ->make(true);

        }
        return view('admin.pages.cities.index');
    }


    public function edit(City $city)
    {
        $states = State::where('status', State::ACTIVE)->get();
        return view('admin.pages.cities.edit',[
            'city'  => $city,
            'states' => $states
        ]);
    }

    public function update(Request $request,City $city)
    {
        $request->validate([
            'name' => 'required|unique:cities,name,'.$city->id,
            'state_id' => 'required',
        ]);

        $city->update([
            'name' => $request->name,
            'state_id' => $request->state_id
        ]);
        return redirect()->route('cities')
            ->with('success', 'City is updated successfully.');
    }

    public function delete(City $city)
    {
        $city->delete();
        return redirect()->route('cities')
            ->with('success', 'City is deleted successfully.');
    }

    public function block(City $city) {
        $city->update([
            'status' => City::BLOCK
        ]);
        return redirect()->back()->with( 'success', 'City is locked successfully.');
    }

    public function un_block(City $city) {
        $city->update([
            'status' => City::ACTIVE
        ]);
        return redirect()->back()->with( 'success', 'City is Un locked successfully.');
    }


    public function create()
    {
        $state = State::where('status', State::ACTIVE)->get();
        return view('admin.pages.cities.create', compact('state'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:cities,name',
            'state_id' => 'required',
        ]);
        City::create([
            'name' => $request->name,
            'state_id' => $request->state_id,
        ]);

        return redirect()->route('cities')->with('success','City Added Successfully');
    }


}
