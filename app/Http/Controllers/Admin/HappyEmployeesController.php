<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\HappyEmployeesPage;
use App\Models\HappyEmplyee;
use App\Traits\imageUploadTrait;
use Illuminate\Support\Str;

class HappyEmployeesController extends Controller
{
    

    use imageUploadTrait;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = HappyEmplyee::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

           
            ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

            ->addColumn('employee_image', function($data) {

                $imageUrl = asset('storage/happy_employee_image/' . $data->employee_image);
                return '<img src="' . $imageUrl . '" alt="Employee Image" width="50" height="50">';
            })


            ->addColumn('description', function ($data) {
                $words = explode(' ', $data->description);
                $limitedWords = implode(' ', array_slice($words, 0, 10));

                return '<p>' . $limitedWords . '... <a href="#" class="see-more" data-description="' . htmlspecialchars($data->description, ENT_QUOTES, 'UTF-8') . '">Read More</a></p>';
            })

                ->addColumn('action', function($data) {
                    $editUrl = route('edit-happy-employee', $data->id);
                    $deleteUrl = route('delete-happy-employee', $data->id);
                    $changeStatus = route('block_happy_employee', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';

                        if ($data->status == \App\Models\HappyEmplyee::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['employee_image','description','status','action'])
                ->make(true);

        }
        return view('admin.pages.happy_employees_page.index');
    }


   public function save_happy_employee(Request $request)
  {

    $request->validate([
         'first_heading' => 'required',
         'first_text' => 'required',
         'second_heading' => 'required',

         'first_paragraph' => 'required',
         'third_heading' => 'required',
         'second_paragraph' => 'required',

         'fourth_heading' => 'required',
         'second_text' => 'required',
         'five_heading' => 'required',
         
         'third_text' => 'required',
         'contact_us_button' => 'required',
        
    ]);

    $HappyEmplyee = HappyEmployeesPage::firstOrNew(['id' => 1]);

    $HappyEmplyee->first_heading = $request->first_heading;
    $HappyEmplyee->first_text = $request->first_text;
    $HappyEmplyee->second_heading = $request->second_heading;
    $HappyEmplyee->first_paragraph = $request->first_paragraph;
    $HappyEmplyee->third_heading = $request->third_heading;
    $HappyEmplyee->second_paragraph = $request->second_paragraph;
    $HappyEmplyee->fourth_heading = $request->fourth_heading;
    $HappyEmplyee->second_text = $request->second_text;
    $HappyEmplyee->five_heading = $request->five_heading;
    $HappyEmplyee->third_text = $request->third_text;
    $HappyEmplyee->contact_us_button = $request->contact_us_button;

    if ($request->hasFile('bg_image')) {
        $bg_image = self::uploadFile($request, 'bg_image', 'happy_employees_page');
        $HappyEmplyee->bg_image = $bg_image;
    }
                         
    if ($request->hasFile('first_image')) {
        $first_image = self::uploadFile($request, 'first_image', 'happy_employees_page');
        $HappyEmplyee->first_image = $first_image;
    }

    if ($request->hasFile('sec_image')) {
        $sec_image = self::uploadFile($request, 'sec_image', 'happy_employees_page');
        $HappyEmplyee->sec_image = $sec_image;
    }

    if ($request->hasFile('app_store_image')) {
        $app_store_image = self::uploadFile($request, 'app_store_image', 'happy_employees_page');
        $HappyEmplyee->app_store_image = $app_store_image;
    }

    if ($request->hasFile('google_play_image')) {
        $google_play_image = self::uploadFile($request, 'google_play_image', 'happy_employees_page');
        $HappyEmplyee->google_play_image = $google_play_image;
    }

    if ($request->hasFile('sec_bg_image')) {
        $sec_bg_image = self::uploadFile($request, 'sec_bg_image', 'happy_employees_page');
        $HappyEmplyee->sec_bg_image = $sec_bg_image;
    }
    
    $HappyEmplyee->save();

    return redirect()->back()->with('success', 'Happy Employees Page is updated successfully.');
  }


  public function add_happy_employee()
  {

    return view('admin.pages.happy_employees_page.create');
  }

   public function store_happy_employee(Request $request)
    {
        $request->validate([
        'employee_name' => 'required',
        'description' => 'required',
         ]);


        if($request->employee_image){
            $employee_image = self::uploadFile($request, fileName: 'employee_image', folderName: 'happy_employee_image');
        }

       $save_employee = HappyEmplyee::create([
            
            'employee_name' => $request->employee_name,
            'employee_image' => @$employee_image,
            'description' => $request->description,
            
        ]);

        return redirect()->route('happy-employees')->with('success','Happy Emplyee Added Successfully');
    }


    public function edit_happy_employee(HappyEmplyee $employee)
    {
      return view('admin.pages.happy_employees_page.edit', compact('employee'));
    }


    public function update_happy_employee(Request $request, HappyEmplyee $employee)
    {
        $request->validate([
        'employee_name' => 'required',
        'description' => 'required',
         ]);
        $slug = Str::slug($request->title);

        $updateEvent = [
             'employee_name' => $request->employee_name,
             'description' => $request->description,
         ];

         if ($request->hasFile('employee_image')) {
             $employee_image = self::uploadFile($request, 'employee_image', 'happy_employee_image');
             $updateEvent['employee_image'] = $employee_image;
         }

         $employee->update($updateEvent);

         return redirect()->route('happy-employees')->with('success', 'Happy Employee Updated Successfully!');
     }



   public function delete_happy_employee(HappyEmplyee $employee)
    {
         HappyEmplyee::destroy($employee->id);
        return redirect()->back()->with('success','HappyEmplyee Deleted Successfully');

    }


    public function block_happy_employee(HappyEmplyee $employee){

        if ($employee->status == HappyEmplyee::ACTIVE){

            $employee->update([
                'status' => HappyEmplyee::BLOCK
            ]);
            $message = 'Happy Emplyee Block Successfully';

        }else{
            $employee->update([
                'status' => HappyEmplyee::ACTIVE
            ]);
            $message = 'Happy Emplyee Un-block Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
