<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Education;
use Yajra\DataTables\DataTables;

class EducationController extends Controller
{
   

   public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Education::orderBy('id', 'DESC')->get();

            return Datatables::of($data)
                ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('education_edit', $data->id);
                    $deleteUrl = route('education_delete', $data->id);
                    $changeStatus = route('block_education', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';
                    if ($data->status == \App\Models\Education::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }
                    return $actionHtml;
                })

                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.Education.index');
    }

      public function create()
    {
        return view('admin.pages.Education.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:job_positions,name',

        ]);
        Education::create([
            'name' => $request->name,
        ]);

        return redirect()->route('education')->with('success','Education Added Successfully');
    }


    public function edit(Education $education)
    {
        return view('admin.pages.Education.edit',compact('education'));
    }
    public function update(Request $request, Education $education)
    {
        
        $request->validate([
            'name' => 'required|unique:education,name,'.$education->id

        ]);
        $education->update([
            'name' => $request->name,
        ]);

        return redirect()->route('education')->with('success','Education Added Successfully');
    }
    public function delete(Education $education)
    {
        Education::destroy($education->id);
        return redirect()->route('education')->with('success','Education Deleted Successfully');

    }


    public function block_education(Education $education){

        if ($education->status == Education::ACTIVE){

            $education->update([
                'status' => Education::BLOCK
            ]);
            $message = 'Education Blocked Successfully';

        }else{
            $education->update([
                'status' => Education::ACTIVE
            ]);
            $message = 'Education Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
