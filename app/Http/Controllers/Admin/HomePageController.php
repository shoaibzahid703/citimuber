<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CandidateDetailPage;
use Illuminate\Http\Request;
use App\Models\HomePageFirstSection;
use App\Models\HomePageSecondSection;
use App\Models\HomePageThirdSection;
use App\Models\HomePageFourthSection;
use App\Models\HomePageFiveSection;
use App\Models\HomePageSixSection;
use App\Models\HomePageSevenSection;
use App\Traits\imageUploadTrait;
use App\Models\HomePageEightSection;
use App\Models\ClientSay;
use Yajra\DataTables\DataTables;
use App\Models\CandidateLandingPage;
use App\Models\PartnerLandingPage;
use App\Models\JobLandingPage;
use App\Models\AgencyLandingPage;
use App\Models\TrainingLandingPage;
use App\Models\AssociationLandingPage;

use App\Models\AboutUsLandingPage;
use App\Models\EmployerDashboardPage;
use App\Models\CandidateDashboardPage;

class HomePageController extends Controller
{
    use imageUploadTrait;

    public function first_section(){

     return view('admin.pages.home_page.first_section');
   }


  public function save_first_section(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'second_heading' => 'required',
        'first_button' => 'required',
        'sec_button' => 'required',
    ]);

    $first_section = HomePageFirstSection::firstOrNew(['id' => 1]);

    $first_section->first_heading = $request->first_heading;
    $first_section->second_heading = $request->second_heading;
    $first_section->first_button = $request->first_button;
    $first_section->sec_button = $request->sec_button;


    if ($request->hasFile('bg_image')) {
        $bg_image = self::uploadFile($request, 'bg_image', 'home_first_section');
        $first_section->bg_image = $bg_image;
    }

    $first_section->save();

    return redirect()->back()->with('success', 'Home First section is updated successfully.');
  }


  public function second_section(){

        return view('admin.pages.home_page.second_section');
    }


  public function save_second_section(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'second_heading' => 'required',
    ]);

    $second_section = HomePageSecondSection::firstOrNew(['id' => 1]);

    $second_section->first_heading = $request->first_heading;
    $second_section->second_heading = $request->second_heading;



    if ($request->hasFile('first_image')) {
        $first_image = self::uploadFile($request, 'first_image', 'home_second_section');
        $second_section->first_image = $first_image;
    }

    if ($request->hasFile('second_image')) {
        $second_image = self::uploadFile($request, 'second_image', 'home_second_section');
        $second_section->second_image = $second_image;
    }

    if ($request->hasFile('third_image')) {
        $third_image = self::uploadFile($request, 'third_image', 'home_second_section');
        $second_section->third_image = $third_image;
    }

    if ($request->hasFile('forth_image')) {
        $forth_image = self::uploadFile($request, 'forth_image', 'home_second_section');
        $second_section->forth_image = $forth_image;
    }

    if ($request->hasFile('five_image')) {
        $five_image = self::uploadFile($request, 'five_image', 'home_second_section');
        $second_section->five_image = $five_image;
    }

    if ($request->hasFile('six_image')) {
        $six_image = self::uploadFile($request, 'six_image', 'home_second_section');
        $second_section->six_image = $six_image;
    }

    if ($request->hasFile('seven_image')) {
        $seven_image = self::uploadFile($request, 'seven_image', 'home_second_section');
        $second_section->seven_image = $seven_image;
    }

    $second_section->save();

    return redirect()->back()->with('success', 'Home Second section is updated successfully.');
  }


  public function third_section(){

     return view('admin.pages.home_page.third_section');
   }


  public function save_third_section(Request $request)
  {
    $request->validate([
        'first_text' => 'required',
        'heading' => 'required',
        'paragraph' => 'required',
        'first_button' => 'required',
        'second_button' => 'required',
    ]);

    $third_section = HomePageThirdSection::firstOrNew(['id' => 1]);

    $third_section->first_text = $request->first_text;
    $third_section->heading = $request->heading;
    $third_section->paragraph = $request->paragraph;
    $third_section->first_button = $request->first_button;
    $third_section->second_button = $request->second_button;


    if ($request->hasFile('side_image')) {
        $side_image = self::uploadFile($request, 'side_image', 'home_third_section');
        $third_section->side_image = $side_image;
    }

    $third_section->save();

    return redirect()->back()->with('success', 'Home Third section is updated successfully.');
  }


  public function fourth_section(){

     return view('admin.pages.home_page.fourth_section');
   }


  public function save_fourth_section(Request $request)
  {
    $request->validate([
        'first_text' => 'required',
        'first_heading' => 'required',
        'second_text' => 'required',
        'second_heading' => 'required',
        'third_heading' => 'required',
        'first_paragraph' => 'required',
        'fourth_heading' => 'required',
        'second_paragraph' => 'required',
        'five_heading' => 'required',
        'third_paragraph' => 'required',
    ]);

    $fourth_section = HomePageFourthSection::firstOrNew(['id' => 1]);

    $fourth_section->first_text = $request->first_text;
    $fourth_section->first_heading = $request->first_heading;
    $fourth_section->first_button = $request->first_button;
    $fourth_section->second_text = $request->second_text;
    $fourth_section->second_heading = $request->second_heading;
    $fourth_section->third_heading = $request->third_heading;
    $fourth_section->first_paragraph = $request->first_paragraph;
    $fourth_section->fourth_heading = $request->fourth_heading;
    $fourth_section->second_paragraph = $request->second_paragraph;
    $fourth_section->five_heading = $request->five_heading;
    $fourth_section->third_paragraph = $request->third_paragraph;


    if ($request->hasFile('icon_image')) {
        $icon_image = self::uploadFile($request, 'icon_image', 'home_fourth_section');
        $fourth_section->icon_image = $icon_image;
    }

    if ($request->hasFile('first_image')) {
        $first_image = self::uploadFile($request, 'first_image', 'home_fourth_section');
        $fourth_section->first_image = $first_image;
    }

    if ($request->hasFile('sec_image')) {
        $sec_image = self::uploadFile($request, 'sec_image', 'home_fourth_section');
        $fourth_section->sec_image = $sec_image;
    }

    if ($request->hasFile('third_image')) {
        $third_image = self::uploadFile($request, 'third_image', 'home_fourth_section');
        $fourth_section->third_image = $third_image;
    }

    $fourth_section->save();

    return redirect()->back()->with('success', 'Home Fourth section is updated successfully.');
  }



  public function five_section(){

     return view('admin.pages.home_page.five_section');
   }


  public function save_five_section(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'first_paragraph' => 'required',
        'user_count_heading' => 'required',
        'user_text' => 'required',
        'percentage_heading' => 'required',
        'percentage_text' => 'required',
        'satisfaction_heading' => 'required',
        'satisfaction_text' => 'required',
        'second_paragraph' => 'required',
        'register_button' => 'required',
    ]);

    $five_section = HomePageFiveSection::firstOrNew(['id' => 1]);

    $five_section->first_heading = $request->first_heading;
    $five_section->first_paragraph = $request->first_paragraph;
    $five_section->user_count_heading = $request->user_count_heading;
    $five_section->user_text = $request->user_text;
    $five_section->percentage_heading = $request->percentage_heading;
    $five_section->percentage_text = $request->percentage_text;
    $five_section->satisfaction_heading = $request->satisfaction_heading;

    $five_section->satisfaction_text = $request->satisfaction_text;
    $five_section->second_paragraph = $request->second_paragraph;
    $five_section->register_button = $request->register_button;


    if ($request->hasFile('side_image')) {
        $side_image = self::uploadFile($request, 'side_image', 'home_five_section');
        $five_section->side_image = $side_image;
    }

    $five_section->save();

    return redirect()->back()->with('success', 'Home Five section is updated successfully.');
  }



  public function six_section(){

     return view('admin.pages.home_page.six_section');
   }


  public function save_six_section(Request $request)
  {
    $request->validate([
        'first_text' => 'required',
        'first_heading' => 'required',
        'first_button' => 'required',
        'second_heading' => 'required',
        'third_heading' => 'required',
        'first_paragraph' => 'required',
        'second_text' => 'required',
        'third_text' => 'required',
        'fourth_heading' => 'required',
        'second_paragraph' => 'required',
        'fourth_text' => 'required',
        'five_text' => 'required',
        'five_heading' => 'required',
        'third_paragraph' => 'required',
        'six_text' => 'required',
        'seven_text' => 'required',
    ]);

    $six_section = HomePageSixSection::firstOrNew(['id' => 1]);

    $six_section->first_text = $request->first_text;
    $six_section->first_heading = $request->first_heading;
    $six_section->first_button = $request->first_button;
    $six_section->second_heading = $request->second_heading;


    $six_section->third_heading = $request->third_heading;
    $six_section->first_paragraph = $request->first_paragraph;
    $six_section->second_text = $request->second_text;
    $six_section->third_text = $request->third_text;

    $six_section->fourth_heading = $request->fourth_heading;
    $six_section->second_paragraph = $request->second_paragraph;
    $six_section->fourth_text = $request->fourth_text;
    $six_section->five_text = $request->five_text;

    $six_section->five_heading = $request->five_heading;
    $six_section->third_paragraph = $request->third_paragraph;
    $six_section->six_text = $request->six_text;
    $six_section->seven_text = $request->seven_text;



    if ($request->hasFile('icon_image')) {
        $icon_image = self::uploadFile($request, 'icon_image', 'home_six_section');
        $six_section->icon_image = $icon_image;
    }

    if ($request->hasFile('first_image')) {
        $first_image = self::uploadFile($request, 'first_image', 'home_six_section');
        $six_section->first_image = $first_image;
    }

    if ($request->hasFile('sec_image')) {
        $sec_image = self::uploadFile($request, 'sec_image', 'home_six_section');
        $six_section->sec_image = $sec_image;
    }

    if ($request->hasFile('third_image')) {
        $third_image = self::uploadFile($request, 'third_image', 'home_six_section');
        $six_section->third_image = $third_image;
    }

    $six_section->save();

    return redirect()->back()->with('success', 'Home Fourth section is updated successfully.');
  }


  public function seven_section(){

     return view('admin.pages.home_page.seven_section');
   }


  public function save_seven_section(Request $request)
  {
    $request->validate([
        'first_text' => 'required',
        'heading' => 'required',
        'paragraph' => 'required',
        'first_button' => 'required',
        'second_button' => 'required',
        'second_text' => 'required',
    ]);

    $seven_section = HomePageSevenSection::firstOrNew(['id' => 1]);

    $seven_section->first_text = $request->first_text;
    $seven_section->heading = $request->heading;
    $seven_section->paragraph = $request->paragraph;
    $seven_section->first_button = $request->first_button;
    $seven_section->second_button = $request->second_button;
    $seven_section->second_text = $request->second_button;


    if ($request->hasFile('side_image')) {
        $side_image = self::uploadFile($request, 'side_image', 'home_seven_section');
        $seven_section->side_image = $side_image;
    }

    if ($request->hasFile('playstore_image')) {
        $playstore_image = self::uploadFile($request, 'playstore_image', 'home_seven_section');
        $seven_section->playstore_image = $playstore_image;
    }

    if ($request->hasFile('appstore_image')) {
        $appstore_image = self::uploadFile($request, 'appstore_image', 'home_seven_section');
        $seven_section->appstore_image = $appstore_image;
    }

    $seven_section->save();

    return redirect()->back()->with('success', 'Home Seven section is updated successfully.');
  }


    public function eight_section(Request $request)
    {
        if ($request->ajax()) {
            $data = ClientSay::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

            ->addColumn('image', function ($data) {
                $url = asset('storage/client_says/' . $data->image);
                return '<img src="'.$url.'" height="50" width="50" />';
              })

            ->addColumn('comment', function ($data) {
                // Function to get the first 10 words
                $words = explode(' ', $data->comment);
                $limitedWords = implode(' ', array_slice($words, 0, 10));

                return '<p>' . $limitedWords . '... <a href="#" class="see-more" data-comment="' . htmlspecialchars($data->comment, ENT_QUOTES, 'UTF-8') . '">Read More</a></p>';
            })

            ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

                ->addColumn('action', function($data) {
                    $editUrl = route('edit-comment', $data->id);
                    $deleteUrl = route('delete-comment', $data->id);
                    $changeStatus = route('block_comment', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';

                        if ($data->status == \App\Models\ClientSay::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['comment','status','image','action'])
                ->make(true);

        }
        return view('admin.pages.home_page.eight_section');
    }




  public function save_eight_section(Request $request)
  {
    $request->validate([
        'first_text' => 'required',
        'heading' => 'required',
    ]);

    $eight_section = HomePageEightSection::firstOrNew(['id' => 1]);

    $eight_section->first_text = $request->first_text;
    $eight_section->heading = $request->heading;

    $eight_section->save();

    return redirect()->back()->with('success', 'Home Seven section is updated successfully.');
  }


  public function add_comment(){

     return view('admin.pages.home_page.testimonial.create');
   }


   public function save_comment(Request $request)
    {
        $request->validate([
            'client_name' => 'required',
            'image' => 'required',
            'comment' => 'required',

        ]);

        if($request->image){
            $image = self::uploadFile($request, fileName: 'image', folderName: 'client_says');
        }

        ClientSay::create([
            'client_name' => $request->client_name,
            'comment' => $request->comment,
            'image' => $image,
        ]);
        return redirect()->route('eight_section')->with('success','Comment Added Successfully');
    }


    public function edit_comment(ClientSay $client)
    {

     return view('admin.pages.home_page.testimonial.edit', compact('client'));
    }


    public function update_comment(Request $request, ClientSay $client){

    if ($request->hasFile('image')) {
        $image = self::uploadFile($request, fileName: 'image', folderName: 'client_says');
        $client->update([
            'client_name' => $request->client_name,
            'image'=> $image,
            'comment' =>       $request->comment,

        ]);
    }else{

        $client->update([
            'client_name' => $request->client_name,
            'comment' =>       $request->comment,

        ]);

    }

    return redirect()->route('eight_section')->with('success', 'Client Comment Updated Successfully!');
   }


    public function delete_comment(ClientSay $client)
    {
         ClientSay::destroy($client->id);
        return redirect()->back()->with('success','Client Comment Deleted Successfully');

    }


    public function block_comment(ClientSay $client){

        if ($client->status == ClientSay::ENABLE){

            $client->update([
                'status' => ClientSay::DISABLE
            ]);
            $message = 'Client Comment Blocked Successfully';

        }else{
            $client->update([
                'status' => ClientSay::ENABLE
            ]);
            $message = 'Client Comment Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }



    public function candidates_page(){

     return view('admin.pages.candidate_page.index');
   }


  public function save_candidates_page(Request $request)
  {
    $request->validate([

        'first_heading' => 'required',
        'second_heading' => 'required',
        'first_paragraph' => 'required',
        'description' => 'required',



    ]);

    $candidate_page = CandidateLandingPage::firstOrNew(['id' => 1]);

    $candidate_page->first_heading = $request->first_heading;
    $candidate_page->first_paragraph = $request->first_paragraph;
    $candidate_page->second_heading = $request->second_heading;
    $candidate_page->description = $request->description;


    // if ($request->hasFile('icon_image')) {
    //     $icon_image = self::uploadFile($request, 'icon_image', 'home_six_section');
    //     $six_section->icon_image = $icon_image;
    // }

    // if ($request->hasFile('first_image')) {
    //     $first_image = self::uploadFile($request, 'first_image', 'home_six_section');
    //     $six_section->first_image = $first_image;
    // }

    // if ($request->hasFile('sec_image')) {
    //     $sec_image = self::uploadFile($request, 'sec_image', 'home_six_section');
    //     $six_section->sec_image = $sec_image;
    // }

    // if ($request->hasFile('third_image')) {
    //     $third_image = self::uploadFile($request, 'third_image', 'home_six_section');
    //     $six_section->third_image = $third_image;
    // }

    $candidate_page->save();

    return redirect()->back()->with('success', 'Candidate Page updated successfully.');
  }


  public function partner_page(){

     return view('admin.pages.partner_page.index');
   }


   public function save_partner_page(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',
        'description' => 'required',
    ]);

    $partner_page = PartnerLandingPage::firstOrNew(['id' => 1]);

    $partner_page->first_heading = $request->first_heading;
    $partner_page->paragraph = $request->paragraph;
    $partner_page->description = $request->description;



    $partner_page->save();

    return redirect()->back()->with('success', 'Partner landing page updated successfully.');
  }


  public function jobs_page(){

     return view('admin.pages.jobs_page.index');
   }

   public function save_jobs_page(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',
        'second_heading' => 'required',
        'description' => 'required',
    ]);

    $jobs_page = JobLandingPage::firstOrNew(['id' => 1]);

    $jobs_page->first_heading = $request->first_heading;
    $jobs_page->paragraph = $request->paragraph;
    $jobs_page->second_heading = $request->second_heading;
    $jobs_page->description = $request->description;



    $jobs_page->save();

    return redirect()->back()->with('success', 'Jobs landing page updated successfully.');
  }

  public function agencies_page(){

     return view('admin.pages.agencies_page.index');
   }

   public function save_agencies_page(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',
        'description' => 'required',
    ]);

    $agency_page = AgencyLandingPage::firstOrNew(['id' => 1]);

    $agency_page->first_heading = $request->first_heading;
    $agency_page->paragraph = $request->paragraph;
    $agency_page->description = $request->description;



    $agency_page->save();

    return redirect()->back()->with('success', 'Agencies landing page updated successfully.');
  }


  public function training_page(){

     return view('admin.pages.training_page.index');
   }

   public function save_training_page(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',
        'description' => 'required',
    ]);

    $training_page = TrainingLandingPage::firstOrNew(['id' => 1]);

    $training_page->first_heading = $request->first_heading;
    $training_page->paragraph = $request->paragraph;
    $training_page->description = $request->description;



    $training_page->save();

    return redirect()->back()->with('success', 'Training landing page updated successfully.');
  }


  public function association_page(){

     return view('admin.pages.association_page.index');
   }


   public function save_association_page(Request $request)
  {
    $request->validate([
        'first_heading' => 'required',
        'paragraph' => 'required',
        'description' => 'required',
    ]);

    $association_page = AssociationLandingPage::firstOrNew(['id' => 1]);

    $association_page->first_heading = $request->first_heading;
    $association_page->paragraph = $request->paragraph;
    $association_page->description = $request->description;



    $association_page->save();

    return redirect()->back()->with('success', 'Association landing page updated successfully.');
  }


  public function about_us_page(){

     return view('admin.pages.about_us_page.index');
   }


   public function save_about_us_page(Request $request)
  {

    $request->validate([
        'first_heading' => 'required',
         'description' => 'required',

    ]);

    $about_us_page = AboutUsLandingPage::firstOrNew(['id' => 1]);

    $about_us_page->first_heading = $request->first_heading;
    $about_us_page->description = $request->description;
    $about_us_page->second_heading = $request->second_heading;
    $about_us_page->text_line = $request->text_line;
    $about_us_page->button_name = $request->button_name;

    $about_us_page->save();

    return redirect()->back()->with('success', 'About us page updated successfully.');
  }



  public function employer_dashboard_page(){

     return view('admin.pages.employer_dashboard_page.index');
   }


  public function save_page(Request $request)
  {
    $request->validate([


    ]);

    $employer_dashboard = EmployerDashboardPage::firstOrNew(['id' => 1]);


    if ($request->hasFile('first_image')) {
        $first_image = self::uploadFile($request, 'first_image', 'employer_dashboard_page');
        $employer_dashboard->first_image = $first_image;
    }

    if ($request->hasFile('second_image')) {
        $second_image = self::uploadFile($request, 'second_image', 'employer_dashboard_page');
        $employer_dashboard->second_image = $second_image;
    }

    if ($request->hasFile('third_image')) {
        $third_image = self::uploadFile($request, 'third_image', 'employer_dashboard_page');
        $employer_dashboard->third_image = $third_image;
    }

    if ($request->hasFile('fourth_image')) {
        $fourth_image = self::uploadFile($request, 'fourth_image', 'employer_dashboard_page');
        $employer_dashboard->fourth_image = $fourth_image;
    }

    if ($request->hasFile('five_image')) {
        $five_image = self::uploadFile($request, 'five_image', 'employer_dashboard_page');
        $employer_dashboard->five_image = $five_image;
    }

    if ($request->hasFile('six_image')) {
        $six_image = self::uploadFile($request, 'six_image', 'employer_dashboard_page');
        $employer_dashboard->six_image = $six_image;
    }

    if ($request->hasFile('seven_image')) {
        $seven_image = self::uploadFile($request, 'seven_image', 'employer_dashboard_page');
        $employer_dashboard->seven_image = $seven_image;
    }

    $employer_dashboard->save();

    return redirect()->back()->with('success', 'Employer Dashboard Images Updated Successfully.');
  }



  public function candidate_dashboard_page(){

     return view('admin.pages.candidate_dashboard_page.index');
   }


  public function candidate_save_page(Request $request)
  {
    $request->validate([


    ]);

    $candidate_dashboard = CandidateDashboardPage::firstOrNew(['id' => 1]);


    if ($request->hasFile('first_image')) {
        $first_image = self::uploadFile($request, 'first_image', 'candidate_dashboard_page');
        $candidate_dashboard->first_image = $first_image;
    }

    if ($request->hasFile('second_image')) {
        $second_image = self::uploadFile($request, 'second_image', 'candidate_dashboard_page');
        $candidate_dashboard->second_image = $second_image;
    }

    if ($request->hasFile('third_image')) {
        $third_image = self::uploadFile($request, 'third_image', 'candidate_dashboard_page');
        $candidate_dashboard->third_image = $third_image;
    }

    if ($request->hasFile('fourth_image')) {
        $fourth_image = self::uploadFile($request, 'fourth_image', 'candidate_dashboard_page');
        $candidate_dashboard->fourth_image = $fourth_image;
    }

    if ($request->hasFile('five_image')) {
        $five_image = self::uploadFile($request, 'five_image', 'candidate_dashboard_page');
        $candidate_dashboard->five_image = $five_image;
    }

    if ($request->hasFile('six_image')) {
        $six_image = self::uploadFile($request, 'six_image', 'candidate_dashboard_page');
        $candidate_dashboard->six_image = $six_image;
    }

    if ($request->hasFile('seven_image')) {
        $seven_image = self::uploadFile($request, 'seven_image', 'candidate_dashboard_page');
        $candidate_dashboard->seven_image = $seven_image;
    }

    $candidate_dashboard->save();

    return redirect()->back()->with('success', 'Candidate Dashboard Images Updated Successfully.');
  }

    public function candidate_detail_page()
    {
        $detailPage = CandidateDetailPage::find(1);
        return view('admin.pages.candidate-detail-page.index', compact('detailPage'));
    }

    public function save_candidate_detail_page(Request $request)
    {
        $candidate_page = CandidateDetailPage::firstOrNew(['id' => 1]);

        if ($request->hasFile('candidate_detail_page_image')) {
            $candidate_detail_page_image = self::uploadFile($request, 'candidate_detail_page_image', 'candidate_detail_page_image');
            $candidate_page->candidate_detail_page_image = $candidate_detail_page_image;
        }


        if ($request->hasFile('emp_detail_page_image')) {
            $emp_detail_page_image = self::uploadFile($request, 'emp_detail_page_image', 'candidate_detail_page_image');
            $candidate_page->emp_detail_page_image = $emp_detail_page_image;
        }

        $candidate_page->save();

        return redirect()->back()->with('success', 'Background image is updated successfully.');
    }


}
