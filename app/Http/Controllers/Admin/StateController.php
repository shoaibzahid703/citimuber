<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreState;
use App\Http\Requests\UpdateState;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StateController extends Controller
{

    public function index(Request $request)
    {

        if ($request->ajax()) {
            $states =State::with('country')
                ->has('country')
                ->orderBy('id', 'DESC');

            return Datatables::of($states)
                ->addColumn('status', function($state){

                    if ($state->status == State::ACTIVE) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })
                ->addColumn('country_name', function($state){
                    return $state->country->name;
                })
                ->addColumn('action', function($state) {
                    $block_url= route('state-block', $state);
                    $un_block_url= route('state-un_block', $state);
                    $deleteUrl = route('state-delete', $state);
                    $editUrl = route('state.view', $state);

                    $actionHtml = '';
                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';


                    $actionHtml .= '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    if ($state->status == Country::ACTIVE) {
                        $actionHtml .= '<a href="'.$block_url.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$un_block_url.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })
                ->rawColumns(['role', 'status','action','country_name'])
                ->make(true);

        }

        return view('admin.pages.states.index');
    }


    public function edit(State $state)
    {
        $countries = Country::where('status', Country::ACTIVE)->get();
        return view('admin.pages.states.edit',[
            'state'  => $state,
            'countries' => $countries
        ]);
    }

    public function update(Request $request,State $state)
    {
        $request->validate([
           'name' => 'required',
           'country_id' => 'required',
        ]);
        $state->update([
            'name' => $request->name,
            'country_id' => $request->country_id
        ]);
        return redirect()->route('states')
            ->with('success', 'State is updated successfully.');
    }

    public function delete(State $state)
    {
        $state->delete();
        return redirect()->route('states')
            ->with('success', 'State is deleted successfully.');
    }

    public function block(State $state ) {
        $state->update([
            'status' => State::BLOCK
        ]);
        return redirect()->back()->with( 'success', 'State is locked successfully.' );
    }

    public function un_block( State $state ) {
        $state->update([
            'status' => State::ACTIVE
        ]);
        return redirect()->back()->with( 'success', 'State is Un locked successfully.' );
    }

    public function create()
    {
        $country = Country::where('status', Country::ACTIVE)->get();
        return view('admin.pages.states.create', compact('country'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:states,name',
            'country_id' => 'required',
        ]);
        State::create([
            'name' => $request->name,
            'country_id' => $request->country_id,
        ]);

        return redirect()->route('states')->with('success','State Added Successfully');
    }
}
