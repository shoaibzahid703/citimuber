<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;
use Yajra\DataTables\DataTables;

class FaqsController extends Controller
{
    

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Faq::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

           
            ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

            ->addColumn('question_answer', function ($data) {
                // Function to get the first 10 words
                $words = explode(' ', $data->question_answer);
                $limitedWords = implode(' ', array_slice($words, 0, 10));

                return '<p>' . $limitedWords . '... <a href="#" class="see-more" data-question_answer="' . htmlspecialchars($data->question_answer, ENT_QUOTES, 'UTF-8') . '">Read More</a></p>';
            })

                ->addColumn('action', function($data) {
                    $editUrl = route('edit-faqs', $data->id);
                    $deleteUrl = route('delete-faqs', $data->id);
                    $changeStatus = route('block_faqs', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';

                        if ($data->status == \App\Models\Faq::ACTIVE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['question_answer','status','action'])
                ->make(true);

        }
        return view('admin.pages.faqs_page.index');
    }



  public function add_faqs()
  {

     return view('admin.pages.faqs_page.create');
   }


   public function save_faqs(Request $request)
   {
        $request->validate([
        'question' => 'required',
        'question_answer' => 'required',
         ]);

        Faq::create([
            'question' => $request->question,
            'question_answer' => $request->question_answer,
        ]);
        return redirect()->route('faqs-page')->with('success','Faqs Added Successfully');
    }


    public function edit_faqs(Faq $faqs)
    {

     return view('admin.pages.faqs_page.edit', compact('faqs'));
    }


    public function update_faqs(Request $request, Faq $faqs){

        $faqs->update([
            'question' => $request->question,
            'question_answer' => $request->question_answer,
            
        ]);
    

    return redirect()->route('faqs-page')->with('success', 'Faqs Updated Successfully!');
   }


    public function delete_faqs(Faq $faqs)
    {
         Faq::destroy($faqs->id);
        return redirect()->back()->with('success','Faqs Deleted Successfully');

    }


    public function block_faqs(Faq $faqs){

        if ($faqs->status == Faq::ACTIVE){

            $faqs->update([
                'status' => Faq::BLOCK
            ]);
            $message = 'Faqs Blocked Successfully';

        }else{
            $faqs->update([
                'status' => Faq::ACTIVE
            ]);
            $message = 'Faqs Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }
}
