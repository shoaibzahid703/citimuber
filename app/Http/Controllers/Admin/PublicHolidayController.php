<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PublicHolidayLandingPage;
use App\Models\PublicHoliday;
use Yajra\DataTables\DataTables;
use App\Models\Country;

class PublicHolidayController extends Controller
{


   public function holiday_page(Request $request)
    {
        if ($request->ajax()) {
            $data = PublicHoliday::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

           
            ->addColumn('status', function($data){
                    if ($data->status == 1) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })

            ->addColumn('description', function ($data) {
                // Function to get the first 10 words
                $words = explode(' ', $data->description);
                $limitedWords = implode(' ', array_slice($words, 0, 10));

                return '<p>' . $limitedWords . '... <a href="#" class="see-more" data-description="' . htmlspecialchars($data->description, ENT_QUOTES, 'UTF-8') . '">Read More</a></p>';
            })

                ->addColumn('action', function($data) {
                    $editUrl = route('edit-holiday', $data->id);
                    $deleteUrl = route('delete-holiday', $data->id);
                    $changeStatus = route('block_holiday', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>

                                    ';

                        if ($data->status == \App\Models\PublicHoliday::ENABLE) {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$changeStatus.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })

                ->rawColumns(['description','status','action'])
                ->make(true);

        }
        return view('admin.pages.public_holiday_page.index');
    }







   public function save_holiday_page(Request $request)
  {

    $request->validate([
         'first_heading' => 'required',
         'description' => 'required',
        
    ]);

    $about_us_page = PublicHolidayLandingPage::firstOrNew(['id' => 1]);

    $about_us_page->first_heading = $request->first_heading;
    $about_us_page->description = $request->description;
    
    $about_us_page->save();

    return redirect()->back()->with('success', 'Public holiday updated successfully.');
  }


  public function add_holiday(){

    $country = Country::all();

     return view('admin.pages.public_holiday_page.create', compact('country'));
   }


   public function save_holiday(Request $request)
    {

        $request->validate([
        'title' => 'required',
        'holiday_type' => 'required',
        'holiday_date' => 'required',
        'country_id' => 'required',
        'description' => 'required',

         ]);

       $residential = PublicHoliday::create([
            'title' => $request->title,
            'holiday_type' => $request->holiday_type,
            'holiday_date' => $request->holiday_date,
            'country_id' => $request->country_id,
            'description' => $request->description,
            
            
        ]);
        return redirect()->route('holiday_page')->with('success','Holiday Added Successfully');
    }


    public function edit_holiday(PublicHoliday $holiday)
    {

        $countries = Country::all();

     return view('admin.pages.public_holiday_page.edit', compact('holiday', 'countries'));
    }


    public function update_holiday(Request $request, PublicHoliday $holiday){

        $holiday->update([
            'title' => $request->title,
            'holiday_type' => $request->holiday_type,
            'holiday_date' => $request->holiday_date,
            'country_id' => $request->country_id,
            'description' => $request->description,
            
        ]);
    

    return redirect()->route('holiday_page')->with('success', 'Holiday Updated Successfully!');
   }


    public function delete_holiday(PublicHoliday $holiday)
    {
         PublicHoliday::destroy($holiday->id);
        return redirect()->back()->with('success','Holiday Deleted Successfully');

    }


    public function block_holiday(PublicHoliday $holiday){

        if ($holiday->status == PublicHoliday::ENABLE){

            $holiday->update([
                'status' => PublicHoliday::DISABLE
            ]);
            $message = 'Holiday Blocked Successfully';

        }else{
            $holiday->update([
                'status' => PublicHoliday::ENABLE
            ]);
            $message = 'Holiday Un Blocked Successfully';
        }

        return redirect()->back()->with('success',$message);

    }




}


