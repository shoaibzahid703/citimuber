<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CountryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $countries = Country::orderBy('id', 'DESC');

            return Datatables::of($countries)
                ->addIndexColumn()
                ->addColumn('status', function($country){

                    if ($country->status == Country::ACTIVE) {
                        return "<div class='badge bg-success'>Active</div>";
                    } else {
                        return "<div class='badge bg-secondary'>Block</div>";
                    }
                })
                ->addColumn('action', function($country) {
                    $block_url= route('country-block', $country);
                    $un_block_url= route('country-un_block', $country);
                    $deleteUrl = route('country-delete', $country);
                    $editUrl = route('country.view', $country);

                    $actionHtml = '';
                    $actionHtml .= '<a href="'.$editUrl.'" class="edit btn btn-primary btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';


                    $actionHtml .= '<a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>&nbsp;&nbsp;';

                    if ($country->status == Country::ACTIVE) {
                        $actionHtml .= '<a href="'.$block_url.'" class="btn btn-secondary btn-sm">
                                               <span class="nk-menu-icon text-white">
                                                   <em class="icon ni ni-unlock"></em>
                                               </span>
                                             </a>';
                    } else {
                        $actionHtml .= '<a href="'.$un_block_url.'" class="btn btn-success btn-sm">
                                               <span class="nk-menu-icon text-white">

                                                   <em class="icon ni ni-lock"></em>
                                               </span>
                                             </a>';
                    }

                    return $actionHtml;
                })
                ->rawColumns(['role', 'status','action'])
                ->make(true);

        }
        return view('admin.pages.country.index');
    }

    public function create()
    {
        return view('admin.pages.country.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:countries,name',
            'code' => 'required|unique:countries,code',
            'nationality' => 'required|unique:countries,nationality',
        ]);
        Country::create([
            'name' => $request->name,
            'code' => $request->code,
            'nationality' => $request->nationality,
        ]);

        return redirect()->route('countries')->with('success','Country Added Successfully');
    }


    public function edit(Country $country)
    {
        return view('admin.pages.country.edit',[
            'country'  => $country
        ]);
    }

    public function update(Request $request,Country $country)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required',
            'nationality' => 'required'
        ]);
        $country->update([
            'name' => $request->name,
            'code' => $request->code,
            'nationality' => $request->nationality,
        ]);
        return redirect()->route('countries')
            ->with('success', 'Country is updated successfully.');
    }

    public function delete(Country $country)
    {
        $country->delete();
        return redirect()->route('countries')
            ->with('success', 'Country is deleted successfully.');
    }

    public function block( Country $country ) {
        $country->update([
            'status' => Country::BLOCK
        ]);
        return redirect()->back()->with( 'success', 'Country is locked successfully.' );
    }

    public function un_block( Country $country ) {
        $country->update([
            'status' => Country::ACTIVE
        ]);
        return redirect()->back()->with( 'success', 'Country is Un locked successfully.' );
    }
}
