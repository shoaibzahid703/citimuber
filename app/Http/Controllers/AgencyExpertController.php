<?php

namespace App\Http\Controllers;

use App\Models\AgencyExperts;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AgencyExpertController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AgencyExperts::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

                ->addColumn('action', function($data) {
                    $editUrl = route('agency_expert_edit', $data->id);
                    $deleteUrl = route('agency_expert_delete', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>';
                    return $actionHtml;
                })
                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.agency_expert.index');
    }
    public function create()
    {
        return view('admin.pages.agency_expert.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:agency_experts,name',
        ]);
        AgencyExperts::create([
            'name' => $request->name,
        ]);
        return redirect()->route('agency_experts')->with('success','Agency Expert Added Successfully');
    }
    public function edit(AgencyExperts $expert)
    {
        return view('admin.pages.agency_expert.edit',compact('expert'));
    }
    public function update(Request $request, AgencyExperts $expert)
    {
        $request->validate([
            'name' => 'required|unique:agency_experts,name,'.$expert->id
        ]);
        $expert->update([
            'name' => $request->name,
        ]);
        return redirect()->route('agency_experts')->with('success','Agency Expert Updated Successfully');
    }
    public function delete(AgencyExperts $expert)
    {
        AgencyExperts::destroy($expert->id);
        return redirect()->route('agency_experts')->with('success','Agency Expert Deleted Successfully');

    }
}
