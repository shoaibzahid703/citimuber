<?php
namespace App\Http\Controllers;

use App\Models\BioData;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\EmploymentBiodata;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class EmploymentBioDataController extends Controller
{
    public function view_bio_data(Request $request){

        $boidata = BioData::orderBy('id', 'DESC')->latest()->get();
        return view('admin.pages.admin_employer_bio_data', compact('boidata'));
    }

    public function create()
    {
        return view('dashboard.pages.employment_biodata');
    }
    public function pdf_bio_data($id)
    {

        $biodata = BioData::where('id', $id)->first();

        return view('admin.pages.view_empolyer_biodata', compact('biodata'));
    }

    public function store(Request $request)
    {
        $profileImage = null;
        if ($request->hasFile('profile_image')) {
            $file = $request->file('profile_image');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads'), $filename);
            $profileImage = $filename;
        }

        if(isset($request->location) && is_array($request->location)){
            @$location = implode(',',$request->location);
        }else{
            @$location = ' ';
        }

        if(isset($request->dateFrom) && is_array($request->dateFrom)){
            @$dateFrom = implode(',', $request->dateFrom);
        }else{
            @$dateFrom = ' ';
        }
        if(isset($request->dateTo) && is_array($request->dateTo)){
            @$dateTo = implode(',',$request->dateTo);
        }else{
            @$dateTo = '';
        }
        if(isset($request->salary) && is_array($request->salary)){
            @$salary = implode(',',$request->salary);
        }else{
            @$salary = '';
        }
        if(isset($request->district) && is_array($request->district)){
            @$district = implode(',',$request->district);
        }else{
            @$district = '';
        }
        if(isset($request->language) && is_array($request->language)){
            @$language = implode(',',$request->language);
        }else{
            @$language = '';
        }
        if(isset($request->size) && is_array($request->size)){
            @$size = implode(',',$request->size);
        }else{
            @$size = '';
        }

        if(isset($request->employer_nationality) && is_array($request->employer_nationality)){
            @$employer_nationality = implode(',',$request->employer_nationality);
        }else{
            @$employer_nationality = '';
        }
        if(isset($request->family_members) && is_array($request->family_members)){
            @$family_members = implode(',',$request->family_members);
        }else{
            @$family_members = '';
        }
        if(isset($request->num_adults) && is_array($request->num_adults)){
            @$num_adults = implode(',',$request->num_adults);
        }else{
            @$num_adults = '';
        }
        if(isset($request->num_babies) && is_array($request->num_babies)){
            @$num_babies = implode(',',$request->num_babies);
        }else{
            @$num_babies = '';
        }
        if(isset($request->num_children) && is_array($request->num_children)){
            @$num_children = implode(',',$request->num_children);
        }else{
            @$num_children = '';
        }
        if(isset($request->num_elderly) && is_array($request->num_elderly)){
            @$num_elderly = implode(',',$request->num_elderly);
        }else{
            @$num_elderly = '';
        }
        if(isset($request->main_duties) && is_array($request->main_duties)){
            @$main_duties = implode(',',$request->main_duties);
        }else{
            @$main_duties = '';
        }
        if(isset($request->reason) && is_array($request->reason)){
            @$reason = implode(',',$request->reason);
        }else{
            @$reason = '';
        }
        if(isset($request->years_experience) && is_array($request->years_experience)){
            @$years_experience = implode(',',$request->years_experience);
        }else{
            @$years_experience = '';
        }

        if(isset($request->months_experience) && is_array($request->months_experience)){
            @$months_experience = implode(',',$request->months_experience);
        }else{
            @$months_experience = '';
        }
        // For 'babyCare'
        if (isset($request->employer_babyCare) && is_array($request->employer_babyCare)) {
            $employer_babyCare = implode(',', $request->employer_babyCare);
        } else {
            $employer_babyCare = '';
        }

// For 'childCare'
        if (isset($request->employer_childCare) && is_array($request->employer_childCare)) {
            $employer_childCare = implode(',', $request->employer_childCare);
        } else {
            $employer_childCare = '';
        }

// For 'elderlyCare'
        if (isset($request->employer_elderlyCare) && is_array($request->employer_elderlyCare)) {
            $employer_elderlyCare = implode(',', $request->employer_elderlyCare);
        } else {
            $employer_elderlyCare = '';
        }

// For 'housework'
        if (isset($request->employer_housework) && is_array($request->employer_housework)) {
            $employer_housework = implode(',', $request->employer_housework);
        } else {
            $employer_housework = '';
        }

// For 'cooking'
        if (isset($request->employer_cooking) && is_array($request->employer_cooking)) {
            $employer_cooking = implode(',', $request->employer_cooking);
        } else {
            $employer_cooking = '';
        }

// For 'washing'
        if (isset($request->employer_washing) && is_array($request->employer_washing)) {
            $employer_washing = implode(',', $request->employer_washing);
        } else {
            $employer_washing = '';
        }

// For 'disabledCare'
        if (isset($request->employer_disabledCare) && is_array($request->employer_disabledCare)) {
            $employer_disabledCare = implode(',', $request->employer_disabledCare);
        } else {
            $employer_disabledCare = '';
        }

// For 'driving'
        if (isset($request->employer_driving) && is_array($request->employer_driving)) {
            $employer_driving = implode(',', $request->employer_driving);
        } else {
            $employer_driving = '';
        }

// For 'bedriddenCare'
        if (isset($request->employer_bedriddenCare) && is_array($request->employer_bedriddenCare)) {
            $employer_bedriddenCare = implode(',', $request->employer_bedriddenCare);
        } else {
            $employer_bedriddenCare = '';
        }

// For 'gardening'
        if (isset($request->employer_gardening) && is_array($request->employer_gardening)) {
            $employer_gardening = implode(',', $request->employer_gardening);
        } else {
            $employer_gardening = '';
        }

// For 'petsCare'
        if (isset($request->employer_petsCare) && is_array($request->employer_petsCare)) {
            $employer_petsCare = implode(',', $request->employer_petsCare);
        } else {
            $employer_petsCare = '';
        }

// For 'specialneeds'
        if (isset($request->employer_specialneeds)) {
            $employer_specialneeds = implode(',', $request->employer_specialneeds);
        } else {
            $employer_specialneeds = '';
        }
        $request->validate([
            'email' => 'required|email|unique:employment_biodatas,email',
        ]);
        EmploymentBiodata::create([
            'cnahk' => $request->cnahk,
            'profile_image' => $profileImage,
            'phone_no' => $request->phone_no,
            'email' => $request->email,
            'release_date' => $request->release_date,
            'expiry_date' => $request->expiry_date,
            'apply_date' => $request->apply_date,
            'ref_number' => $request->ref_number,
            'address' => $request->address,
            'state' => $request->state,
            'name' => $request->name,
            'age' => $request->age,
            'dob' => $request->dob,
            'education' => $request->education,
            'personal_nationality' => $request->personal_nationality,
            'id_number' => $request->id_number,
            'religion' => $request->religion,
            'birthplace' => $request->birthplace,
            'height' => $request->height,
            'weight' => $request->weight,
            'no_children' => $request->no_children,
            'marital_status' => $request->marital_status,
            'nursing' => $request->nursing,
            'midwifery' => $request->midwifery,
            'education_course' => $request->education_course,
            'caregiver_course' => $request->caregiver_course,
            'baby_care' => $request->baby_care,
            'child_care' => $request->child_care,
            'elderly_care' => $request->elderly_care,
            'housework' => $request->housework,
            'cooking' => $request->cooking,
            'washing' => $request->washing,
            'disabled_care' => $request->disabled_care,
            'driving' => $request->driving,
            'bedridden_care' => $request->bedridden_care,
            'gardening' => $request->gardening,
            'pets_care' => $request->pets_care,
            'special_needs' => $request->special_needs,
            'english_level' => $request->english_level,
            'cantonese_level' => $request->cantonese_level,
            'mandarin_level' => $request->mandarin_level,
            'newborn_care' => $request->newborn_care,
            'elderly_care_willing' => $request->elderly_care_willing,
            'disabled_care_willing' => $request->disabled_care_willing,
            'share_room' => $request->share_room,
            'accept_day_off' => $request->accept_day_off,
            'work_experience_years' => $request->work_experience_years,
            'work_experience_months' => $request->work_experience_months,
            'location' => $location,
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'salary' => $salary,
            'district' => $district,
            'language' => $language,
            'size' => $size,
            'employer_nationality' => $employer_nationality,
            'family_members' => $family_members,
            'num_adults' => $num_adults,
            'num_babies' => $num_babies,
            'num_children' => $num_children,
            'num_elderly' => $num_elderly,
            'employer_babyCare' => $employer_babyCare,
            'employer_childCare' => $employer_childCare,
            'employer_elderlyCare' => $employer_elderlyCare,
            'employer_housework' => $employer_housework,
            'employer_cooking' => $employer_cooking,
            'employer_washing' => $employer_washing,
            'employer_disabledCare' => $employer_disabledCare,
            'employer_driving' => $employer_driving,
            'employer_bedriddenCare' => $employer_bedriddenCare,
            'employer_gardening' => $employer_gardening,
            'employer_petsCare' => $employer_petsCare,
            'employer_specialneeds' => $employer_specialneeds,
            'reason_for_leaving' => $reason,
            'years_experience' => $years_experience,
            'months_experience' => $months_experience,
        ]);

        return redirect()->route('employment_biodata.create')->with('success', 'Data saved successfully.');
    }
    public function employer_biodata_changestatus(Request $request){
        $employer_biodata_id = $request->id;
        $employer_biodata_status = $request->currStatus;
        DB::update('update employment_biodatas set status = ? where id = ?', [$employer_biodata_status, $employer_biodata_id]);
        return response()->json(['success'=>'200']);
    }
    public function employer_biodata_destroy(Request $request) {
        $employer_biodata_id = EmploymentBiodata::find($request->employer_biodata_id);
        $res=EmploymentBiodata::where('id',$request->employer_biodata_id)->delete();
        return response()->json(['success'=>'200']);
    }
}
