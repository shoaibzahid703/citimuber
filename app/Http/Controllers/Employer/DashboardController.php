<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\JobApply;

class DashboardController extends Controller
{
    
    public function index()
    {
        $publish_job = Job::where('user_id', \Auth::user()->id)->where('status', 'publish')->count();
        $get_jobs = Job::where('user_id', \Auth::user()->id)->pluck('id');
        $job_applied = JobApply::whereIn('job_id', $get_jobs)->count();
        
        return view('employer.pages.dashboard', compact('publish_job', 'job_applied'));
    }
}
