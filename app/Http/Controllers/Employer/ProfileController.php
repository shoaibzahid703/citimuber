<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;
use App\Models\Language;
use App\Traits\imageUploadTrait;

class ProfileController extends Controller
{

    use imageUploadTrait;
    
    public function index()
    {
        $country = Country::all();
        $language = Language::all();
        return view('employer.pages.profile', compact('country', 'language'));
    }


    public function update(Request $request){

        $user = Auth::user();
        $data = [
            
            'name' =>     $request->name,
            'email' =>    $request->email,
            'mobile' =>   $request->mobile,
            'passport' => $request->passport,
            'language' => $request->language,
            'location' => $request->location,
        ];

        if ($request->hasFile('profile_image')) {
          $user['profile_image'] = self::uploadFile($request, fileName: 'profile_image', folderName: 'profile_image');
        }

      $user->update($data);
        return redirect()->route('employer_profile',['password_tab'=> false])->with('success','profile updated successfully');
    }


    public function account_update_password(Request $request){
    
        $rules = [
            'current_password' => 'required',
            'password' => 'required|string|confirmed',
        ];
        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('employer_profile', ['password_tab' => true])
                ->withErrors($validator)
                ->withInput();
        }
    
        $user = Auth::user();
        $hashedPassword = $user->password;

        if (Hash::check($request->current_password, $hashedPassword)) {
            if (!Hash::check($request->password, $hashedPassword)) {
                $user->password = Hash::make($request->password);
                $user->save();

                return redirect()->back()->with('success','Password updated successfully');
            } else {
                return redirect()->back()
                    ->with('danger', 'New password cannot be the same as the old password.')
                    ->with('password_tab', true);
            }
        } else {
            return redirect()->back()
                ->with('danger', 'Old password does not match.')
                ->with('password_tab', true);
        }
   }

}
