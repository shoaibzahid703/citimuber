<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Job;
use App\Models\Plans;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe;

class PaymentController extends Controller
{

    public function pay_stripe(Request $request,Job $job)
    {
        if ($request->update_package_id){
            $job->update([
               'plan_id' =>  $request->update_package_id
            ]);
        }
        $currency_select = $request->currency;
        $currency_price = $request->converted_price;
        return view('employer.pages.stripe',compact('job','currency_price','currency_select'));
    }

    public function pay_stripe_payment(Job $job,Request $request)
    {
        $selected_price = $request->currency_price;
        $selected_currency = $request->currency_select;
        if (!is_null($selected_currency) &&  !is_null($selected_price)) {
            $currency_code_country = Country::where('code',$selected_currency)->first();
            $selected_currency_code = 'hkd';
            if ($currency_code_country && !is_null($currency_code_country->currency_code)){
                $selected_currency_code = strtolower($currency_code_country->currency_code);
            }

            $stripe_secret = env('STRIPE_SECRET');
            Stripe\Stripe::setApiKey($stripe_secret);
            $plan = Plans::find($job->plan_id);

            if ($plan){
                $charge =   Stripe\Charge::create ([
                    "amount" => $selected_price * 100,
                    "currency" => $selected_currency_code,
                    "source" => $request->stripeToken,
                    // "description" => "Job Payment"
                    "description" => 1
                ]);

                if ($charge->status == "succeeded"){
                    $today = Carbon::now();
                    $expire_date = $today->addDays($plan->post_duration);

                    $transaction = new Transaction();
                    $transaction->user_id = Auth::id();
                    $transaction->plan_id = $plan->id;
                    $transaction->job_id = $job->id;
                    $transaction->payment_id = $charge->id;
                    $transaction->amount = $selected_price;
                    $transaction->currency = $charge->currency;
                    $transaction->expires_at = $expire_date;


                    $transaction->save();
                    $job->update([
                        'status' => 'publish'
                    ]);

                    if ($plan->reply_messages == 0){
                        $job->update([
                            'send_messages' => 0
                        ]);
                    }
                    return redirect()->route('employer_dashboard')->with('success','Payment Done Successfully');
                }else{
                    return redirect()->back()->with('danger','Invalid Details');
                }
            }else{
                return redirect()->back()->with('danger','No Plan Found');
            }
        }
        else{
            $stripe_secret = env('STRIPE_SECRET');
            Stripe\Stripe::setApiKey($stripe_secret);
            $plan = Plans::find($job->plan_id);

            if ($plan){
                $charge =   Stripe\Charge::create ([
                    "amount" => $plan->price * 100,
                    "currency" => "hkd",
                    "source" => $request->stripeToken,
                    // "description" => "Job Payment"
                    "description" => 1
                ]);


                if ($charge->status == "succeeded"){

                    $today = Carbon::now();
                    $expire_date = $today->addDays($plan->post_duration);

                    $transaction = new Transaction();
                    $transaction->user_id = Auth::id();
                    $transaction->plan_id = $plan->id;
                    $transaction->job_id = $job->id;
                    $transaction->payment_id = $charge->id;
                    $transaction->amount = $plan->price;
                    $transaction->currency = $charge->currency;
                    $transaction->expires_at = $expire_date;


                    $transaction->save();
                    $job->update([
                        'status' => 'publish'
                    ]);

                    if ($plan->reply_messages == 0){
                        $job->update([
                            'send_messages' => 0
                        ]);
                    }
                    return redirect()->route('employer_dashboard')->with('success','Payment Done Successfully');
                }else{
                    return redirect()->back()->with('danger','Invalid Details');
                }
            }else{
                return redirect()->back()->with('danger','No Plan Found');
            }
        }
    }
}
