<?php
namespace App\Http\Controllers;

use App\Models\AgencyReview;
use App\Models\City;
use App\Models\PartnerLandingPage;
use App\Models\State;
use App\Models\Transaction;
use App\Models\AgencyService;
use App\Models\User;
use App\Models\UserMessage;
use Illuminate\Http\Request;
use App\Models\EmploymentBiodata;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;
use App\Models\Job;
use App\Models\ShortList;
use App\Models\EmployerAction;
use App\Models\HomePageFirstSection;
use App\Models\HomePageSecondSection;
use App\Models\HomePageThirdSection;
use App\Models\HomePageFourthSection;
use App\Models\HomePageFiveSection;
use App\Models\HomePageSixSection;
use App\Models\HomePageEightSection;
use App\Models\HomePageSevenSection;
use App\Models\ClientSay;
use App\Models\BioData;
use App\Models\CandidateLandingPage;
use App\Models\JobLandingPage;
use App\Models\AgencyLandingPage;
use App\Models\TrainingLandingPage;
use App\Models\AssociationLandingPage;
use App\Models\TipsLandingPage;
use App\Models\NewLandingPage;
use App\Models\AboutUsLandingPage;
use App\Models\PublicHolidayLandingPage;
use App\Models\PublicHoliday;
use App\Models\Country;
use App\Models\Plans;
use Illuminate\Support\Facades\Log;
use App\Models\Event;
use App\Models\EventsLandingPage;
use App\Models\Click;
use Illuminate\Support\Facades\Auth;
use App\Models\HappyHelpersPage;
use App\Models\Faq;
use App\Models\PrivacyPolicy;
use App\Models\TermsAndConditionPage;
use App\Models\ContactUsPage;
use App\Models\HappyEmployeesPage;
use App\Models\HappyEmplyee;
use App\Models\News;
use App\Models\Category;
use App\Models\Tips;
use App\Models\CandidateDetailPage;

class FrontendController extends Controller
{

    public function index()
    {
        $home_first_section = HomePageFirstSection::find(1);
        $home_second_section = HomePageSecondSection::find(1);
        $home_third_section = HomePageThirdSection::find(1);
        $home_fourth_section = HomePageFourthSection::find(1);
        $home_five_section = HomePageFiveSection::find(1);
        $home_six_section = HomePageSixSection::find(1);
        $home_eight_section = HomePageEightSection::find(1);
        $home_seven_section = HomePageSevenSection::find(1);
        $client_comment = ClientSay::where('status', 1)->get();
        $jobs = Job::where('status', 'publish')->latest()->take(3)->get();

        $user = User::where('role', 'candidate')->pluck('id');

        $candidates = BioData::whereIn('user_id', $user)->inRandomOrder()->take(3)->get();

        return view('frontend/welcome', compact('home_first_section', 'home_second_section','home_third_section', 'home_fourth_section', 'home_five_section', 'home_six_section','home_eight_section', 'home_seven_section', 'client_comment', 'jobs', 'candidates'));
    }


    public function jobs(Request $request)
    {

        $publishedJobs = Job::where('status', Job::PUBLISH)
            ->select('id', 'created_at', 'plan_id','add_type')
            ->get();

        foreach ($publishedJobs as $job) {
            if ($job->add_type == 'employer'){
                $transaction = Transaction::where('job_id',$job->id)
                    ->where('plan_type',Transaction::PLAN_TYPE_EMPLOYER)
                    ->whereNotNull('expires_at')
                    ->where('expires_at', '>', now())
                    ->latest()
                    ->first();

                if (is_null($transaction)){
                    $job->status = 'expire';
                    $job->save();
                }
            }
        }

        $query = Job::query()
            ->where(function ($q) {
                $q->where(function ($q) {
                    $q->where('add_type', 'employer')
                        ->whereHas('job_package');
                })
                    ->orWhere(function ($q) {
                        $q->where('add_type', 'agency')
                            ->whereHas('userDetail', function ($query) {
                                $query->whereHas('transactions', function ($query) {
                                    $query->where('plan_type', Transaction::PLAN_TYPE_AGENCY)
                                        ->latest()
                                        ->whereNotNull('expires_at')
                                        ->where('expires_at', '>', now());
                                });
                            });
                    });
            });


        if ($request->agency_id){
            $query->where('jobs.user_id',$request->agency_id);
        }

        if ($request->has('offer_location') && $request->input('offer_location') !== null) {
            $query->where('jobs.offer_location', $request->input('offer_location'));
        }

        if ($request->has('position_offered') && $request->input('position_offered') !== null) {
            $query->where('jobs.position_offered', $request->input('position_offered'));
        }

        if ($request->has('job_start_date') && $request->input('job_start_date') !== null) {
            $query->where('jobs.job_start_date', $request->input('job_start_date'));
        }

        if ($request->has('job_type') && $request->input('job_type') !== null) {
            $query->where('jobs.job_type', $request->input('job_type'));
        }

        if ($request->has('contract_status') && $request->input('contract_status') !== null) {
            $query->where('jobs.contract_status', $request->input('contract_status'));
        }

        if ($request->has('languages') && $request->input('languages') !== null) {
            $query->where('jobs.languages', $request->input('languages'));
        }

        if ($request->has('main_skills') && $request->input('main_skills') !== null) {
            $query->where('jobs.main_skills', $request->input('main_skills'));
        }

        $jobs = $query->orderBy('created_at', 'DESC')
            ->select('jobs.*')
            ->paginate(100)
            ->withQueryString();

        $offer_locationApply = $request->input('offer_location');
        $position_offeredApply = $request->input('position_offered');
        $job_start_dateApply = $request->input('job_start_date');
        $jobtypeApply = $request->input('job_type');
        $contract_statusApply = $request->input('contract_status');
        $languagesApply = $request->input('languages');
        $main_skillsApply = $request->input('main_skills');

        $jobs_page = JobLandingPage::find(1);


        return view('frontend.pages.jobs.jobs', compact('jobs', 'offer_locationApply', 'position_offeredApply', 'job_start_dateApply', 'jobtypeApply', 'contract_statusApply', 'languagesApply', 'main_skillsApply', 'jobs_page'));
    }


    public function recommended_jobs(Request $request)
    {

        $query = Job::query()->where('status', 'publish');

        if ($request->has('offer_location') && $request->input('offer_location') !== null) {
            $query->where('jobs.offer_location', $request->input('offer_location'));
        }

        if ($request->has('position_offered') && $request->input('position_offered') !== null) {
            $query->where('jobs.position_offered', $request->input('position_offered'));
        }

        if ($request->has('job_start_date') && $request->input('job_start_date') !== null) {
            $query->where('jobs.job_start_date', $request->input('job_start_date'));
        }

        if ($request->has('job_type') && $request->input('job_type') !== null) {
            $query->where('jobs.job_type', $request->input('job_type'));
        }

        if ($request->has('contract_status') && $request->input('contract_status') !== null) {
            $query->where('jobs.contract_status', $request->input('contract_status'));
        }

        if ($request->has('languages') && $request->input('languages') !== null) {
            $query->where('jobs.languages', $request->input('languages'));
        }

        if ($request->has('main_skills') && $request->input('main_skills') !== null) {
            $query->where('jobs.main_skills', $request->input('main_skills'));
        }

        $jobs = $query->select('jobs.*')->paginate(100);


        $offer_locationApply = $request->input('offer_location');
        $position_offeredApply = $request->input('position_offered');
        $job_start_dateApply = $request->input('job_start_date');
        $jobtypeApply = $request->input('job_type');
        $contract_statusApply = $request->input('contract_status');
        $languagesApply = $request->input('languages');
        $main_skillsApply = $request->input('main_skills');

        return view('frontend.pages.jobs.recommended_jobs', compact('jobs', 'offer_locationApply', 'position_offeredApply', 'job_start_dateApply', 'jobtypeApply', 'contract_statusApply', 'languagesApply', 'main_skillsApply'));
    }


    public function applicant_jobs(Request $request)
    {

        $query = Job::query()->where('jobs.status', 'publish')
            ->join('job_applies', 'jobs.id', '=', 'job_applies.job_id');

        if ($request->has('offer_location') && $request->input('offer_location') !== null) {
            $query->where('jobs.offer_location', $request->input('offer_location'));
        }

        if ($request->has('position_offered') && $request->input('position_offered') !== null) {
            $query->where('jobs.position_offered', $request->input('position_offered'));
        }

        if ($request->has('job_start_date') && $request->input('job_start_date') !== null) {
            $query->where('jobs.job_start_date', $request->input('job_start_date'));
        }

        if ($request->has('job_type') && $request->input('job_type') !== null) {
            $query->where('jobs.job_type', $request->input('job_type'));
        }

        if ($request->has('contract_status') && $request->input('contract_status') !== null) {
            $query->where('jobs.contract_status', $request->input('contract_status'));
        }

        if ($request->has('languages') && $request->input('languages') !== null) {
            $query->where('jobs.languages', 'like', '%' . $request->input('languages') . '%');
        }

        if ($request->has('main_skills') && $request->input('main_skills') !== null) {
            $query->where('jobs.main_skills', 'like', '%' . $request->input('main_skills') . '%');
        }

        $jobs = $query->select('jobs.*')->paginate(100);

        $offer_locationApply = $request->input('offer_location');
        $position_offeredApply = $request->input('position_offered');
        $job_start_dateApply = $request->input('job_start_date');
        $jobtypeApply = $request->input('job_type');
        $contract_statusApply = $request->input('contract_status');
        $languagesApply = $request->input('languages');
        $main_skillsApply = $request->input('main_skills');

        return view('frontend.pages.jobs.applied-jobs', compact('jobs', 'offer_locationApply', 'position_offeredApply', 'job_start_dateApply', 'jobtypeApply', 'contract_statusApply', 'languagesApply', 'main_skillsApply'));
    }



    public function job_details(Job $job)
    {

        $candidate_page = CandidateDetailPage::firstOrNew(['id' => 1]);
        return view('frontend.pages.jobs.job_details', compact('job','candidate_page'));
    }

    public function get_states($country_id)
    {
        $states = State::where([
            'country_id' => $country_id,
            // 'status' => 1
        ])
            ->select('id','name')
            ->get();

        if (!is_null($states)) {
            return response()->json([
                'status' => 'true',
                'states' => $states
            ]);
        } else {
            return response()->json([
                'status' => 'false',
                'error' => 'No states found.'
            ]);
        }
    }

    public function get_cities($state_id)
    {
        $cities = City::where([
            'state_id' => $state_id,
            // 'status' => 1
        ])
            ->select('id','name')
            ->get();
        if (!is_null($cities)) {
            return response()->json([
                'status' => 'true',
                'cities' => $cities
            ]);
        } else {
            return response()->json([
                'status' => 'false',
                'error' => 'No cities found.'
            ]);
        }
    }

    public function candidate_details(User $candidate)
    {
        $related_candidates = BioData::where('position_apply', $candidate->resume_detail->position_apply ?? '')
            ->whereHas('user', function($q){
                $q->where('role', User::ROLE_CANDIDATE);
            })
            ->where('user_id', '!=', $candidate->id)
            ->inRandomOrder()
            ->take(6)
            ->get();

        $candidate_page = CandidateDetailPage::firstOrNew(['id' => 1]);

        return view('frontend.pages.candidate.candidate_details', compact('candidate','related_candidates', 'candidate_page'));
    }

    public function send_helper_message(Request $request,User $candidate)
    {
        UserMessage::create([
            'message' => $request->message,
            'from_user' => Auth::user()->id,
            'to_user' => $candidate->id,
        ]);

        return redirect()->back()->with('success','Message Send Successfully');
    }


    public function candidates(Request $request)
    {
        $query = User::where('role', User::ROLE_CANDIDATE)
            ->where('visibility_status',User::VISIBLE)
            ->where('status',User::ACTIVE)
            ->orderBy('id','desc')
            ->join('bio_data', 'users.id', '=', 'bio_data.user_id')
            ->select(
                'users.*',
                'bio_data.user_id',
                'bio_data.position_apply',
                'bio_data.job_start_date',
                'bio_data.job_type',
                'bio_data.gender',
                'bio_data.present_country',
                'bio_data.work_status',
                'bio_data.languages',
                'bio_data.main_skills',
                'bio_data.nationality',
                'bio_data.first_name',
                'bio_data.middle_name',
                'bio_data.last_name',
                'bio_data.work_experience',
                'bio_data.age',
            );

        if ($request->agency_id){
            $query =  $query->where('added_by',$request->agency_id);
        }

        if ($request->has('position_apply')) {
            $query =    $query->where('position_apply', $request->input('position_apply'));
        }

        if ($request->filled('job_start_date')) {
            $query =   $query->whereDate('job_start_date', $request->input('job_start_date'));
        }

        if ($request->has('job_type')) {
            $query =   $query->where('job_type', $request->input('job_type'));
        }

        if ($request->has('gender')) {
            $query =  $query->where('gender', $request->input('gender'));
        }

        if ($request->has('present_country')) {
            $query =   $query->where('present_country', $request->input('present_country'));
        }

        if ($request->has('work_status')) {
            $query =  $query->where('work_status', $request->input('work_status'));
        }

        if ($request->has('languages')) {
            $languages = $request->input('languages');
            $query =  $query->whereRaw("FIND_IN_SET(?, languages)", [$languages]);
        }

        if ($request->has('main_skills')) {
            $main_skills = $request->input('main_skills');
            $query =  $query->whereRaw("FIND_IN_SET(?, main_skills)", [$main_skills]);
        }

        if ($request->has('nationality')) {
            $query =  $query->where('nationality', $request->input('nationality'));
        }

        if ($request->has('middle_name')) {
            $query =  $query->where('name', 'like', '%' . $request->input('middle_name') . '%');
        }

        if ($request->has('working_experience')) {
            $experience = explode(';',$request->working_experience);
            $from = $experience['0'];
            $to = $experience['1'];
            $query = $query->whereBetween(DB::raw('CAST(work_experience AS UNSIGNED)'), [$from, $to]);
        }

        if ($request->has('age')) {
            $age = explode(';',$request->age);
            $from_age = $age['0'];
            $to_age = $age['1'];
            $query = $query->whereBetween(DB::raw('CAST(age AS UNSIGNED)'), [$from_age, $to_age]);

        }


        $query = $query->where(function ($q) {
            $q->where(function ($q) {
                $q->whereNull('added_by')
                    ->whereHas('helper_package');
            })->orWhere(function ($q) {
                $q->whereNotNull('added_by')
                    ->whereHas('parent_user', function ($query) {
                        $query->whereHas('transactions', function ($query) {
                            $query->where('plan_type', Transaction::PLAN_TYPE_AGENCY)
                                ->whereNotNull('expires_at')
                                ->where('expires_at', '>', now())
                                ->latest();
                        });
                    });
            });
        });


        $candidates = $query->paginate(10)->withQueryString();




        $positionApply = $request->input('position_apply');
        $jobStartDate = $request->input('job_start_date');
        $jobtypeApply = $request->input('job_type');
        $genderApply = $request->input('gender');
        $present_countryApply = $request->input('present_country');
        $work_statusApply = $request->input('work_status');
        $languagesApply = $request->input('languages');
        $main_skillsApply = $request->input('main_skills');
        $nationalityApply = $request->input('nationality');
        $middle_nameApply = $request->input('middle_name');
        $working_experience = $request->input('working_experience');
        $max_age = BioData::max('age');
        $age_filter = $request->input('age');

        $candidate_landing_page = CandidateLandingPage::find(1);

        return view('frontend.pages.candidate.find_candidates', compact('candidates', 'positionApply',
            'jobStartDate', 'jobtypeApply', 'genderApply', 'present_countryApply', 'work_statusApply',
            'languagesApply', 'main_skillsApply', 'nationalityApply', 'middle_nameApply',
            'candidate_landing_page','working_experience','max_age','age_filter'));
    }



    public function recommended(Request $request)
    {


        $query = User::where('role', 'candidate')
            ->join('bio_data', 'users.id', '=', 'bio_data.user_id')
            ->join('jobs', function ($join) {
                $join->on('jobs.job_type', '=', 'bio_data.job_type')
                    ->on('jobs.position_offered', '=', 'bio_data.position_apply');
            });

        if ($request->has('position_apply')) {
            $query->where('bio_data.position_apply', $request->input('position_apply'));
        }

        if ($request->has('job_start_date')) {
            $query->whereDate('bio_data.job_start_date', $request->input('job_start_date'));
        }

        if ($request->has('job_type')) {
            $query->where('bio_data.job_type', $request->input('job_type'));
        }

        if ($request->has('gender')) {
            $query->where('bio_data.gender', $request->input('gender'));
        }

        if ($request->has('present_country')) {
            $query->where('bio_data.present_country', $request->input('present_country'));
        }

        if ($request->has('work_status')) {
            $query->where('bio_data.work_status', $request->input('work_status'));
        }

        if ($request->has('languages')) {
            $languages = $request->input('languages');
            $query->whereRaw("FIND_IN_SET(?, bio_data.languages)", [$languages]);
        }

        if ($request->has('main_skills')) {
            $main_skills = $request->input('main_skills');
            $query->whereRaw("FIND_IN_SET(?, bio_data.main_skills)", [$main_skills]);
        }

        if ($request->has('nationality')) {
            $query->where('bio_data.nationality', $request->input('nationality'));
        }

        if ($request->has('middle_name')) {
            $query->where('bio_data.middle_name', $request->input('middle_name'));
        }

        $candidates = $query->select('users.*', 'bio_data.position_apply', 'bio_data.job_start_date', 'bio_data.job_type', 'bio_data.gender', 'bio_data.present_country', 'bio_data.work_status', 'bio_data.languages', 'bio_data.main_skills', 'bio_data.nationality', 'bio_data.middle_name')->paginate(10);

        $positionApply = $request->input('position_apply');
        $jobStartDate = $request->input('job_start_date');
        $jobtypeApply = $request->input('job_type');
        $genderApply = $request->input('gender');
        $present_countryApply = $request->input('present_country');
        $work_statusApply = $request->input('work_status');
        $languagesApply = $request->input('languages');
        $main_skillsApply = $request->input('main_skills');
        $nationalityApply = $request->input('nationality');
        $middle_nameApply = $request->input('middle_name');

        return view('frontend.pages.candidate.recommended', compact('candidates', 'positionApply', 'jobStartDate', 'jobtypeApply', 'genderApply', 'present_countryApply', 'work_statusApply', 'languagesApply', 'main_skillsApply', 'nationalityApply', 'middle_nameApply'));
    }



    public function applicant(Request $request)
    {
        $query = User::where('role', 'candidate')
            ->join('bio_data', 'users.id', '=', 'bio_data.user_id');


        if ($request->has('position_apply')) {
            $query->where('bio_data.position_apply', $request->input('position_apply'));
        }

        if ($request->has('job_start_date')) {
            $query->whereDate('bio_data.job_start_date', $request->input('job_start_date'));
        }

        if ($request->has('job_type')) {
            $query->where('bio_data.job_type', $request->input('job_type'));
        }

        if ($request->has('gender')) {
            $query->where('bio_data.gender', $request->input('gender'));
        }

        if ($request->has('present_country')) {
            $query->where('bio_data.present_country', $request->input('present_country'));
        }

        if ($request->has('work_status')) {
            $query->where('bio_data.work_status', $request->input('work_status'));
        }

        if ($request->has('languages')) {
            $languages = $request->input('languages');
            $query->whereRaw("FIND_IN_SET(?, bio_data.languages)", [$languages]);
        }

        if ($request->has('main_skills')) {
            $main_skills = $request->input('main_skills');
            $query->whereRaw("FIND_IN_SET(?, bio_data.main_skills)", [$main_skills]);
        }

        if ($request->has('nationality')) {
            $query->where('bio_data.nationality', $request->input('nationality'));
        }

        if ($request->has('middle_name')) {
            $query->where('bio_data.middle_name', $request->input('middle_name'));
        }

        $candidates = $query->select('users.*', 'bio_data.position_apply', 'bio_data.job_start_date', 'bio_data.job_type', 'bio_data.gender', 'bio_data.present_country', 'bio_data.work_status', 'bio_data.languages', 'bio_data.main_skills', 'bio_data.nationality', 'bio_data.middle_name')->paginate(10);

        $positionApply = $request->input('position_apply');
        $jobStartDate = $request->input('job_start_date');
        $jobtypeApply = $request->input('job_type');
        $genderApply = $request->input('gender');
        $present_countryApply = $request->input('present_country');
        $work_statusApply = $request->input('work_status');
        $languagesApply = $request->input('languages');
        $main_skillsApply = $request->input('main_skills');
        $nationalityApply = $request->input('nationality');
        $middle_nameApply = $request->input('middle_name');

        return view('frontend.pages.candidate.applicant', compact('candidates', 'positionApply', 'jobStartDate', 'jobtypeApply', 'genderApply', 'present_countryApply', 'work_statusApply', 'languagesApply', 'main_skillsApply', 'nationalityApply', 'middle_nameApply'));
    }



    public function shortlist(Request $request)
    {
        $query = User::where('role', 'candidate')
            ->join('bio_data', 'users.id', '=', 'bio_data.user_id');

        if ($request->has('position_apply')) {
            $query->where('bio_data.position_apply', $request->input('position_apply'));
        }

        if ($request->filled('job_start_date')) {
            $query->whereDate('bio_data.job_start_date', $request->input('job_start_date'));
        }

        if ($request->has('job_type')) {
            $query->where('bio_data.job_type', $request->input('job_type'));
        }

        if ($request->has('gender')) {
            $query->where('bio_data.gender', $request->input('gender'));
        }

        if ($request->has('present_country')) {
            $query->where('bio_data.present_country', $request->input('present_country'));
        }

        if ($request->has('work_status')) {
            $query->where('bio_data.work_status', $request->input('work_status'));
        }

        if ($request->has('languages')) {
            $languages = $request->input('languages');
            $query->whereRaw("FIND_IN_SET(?, bio_data.languages)", [$languages]);
        }

        if ($request->has('main_skills')) {
            $main_skills = $request->input('main_skills');
            $query->whereRaw("FIND_IN_SET(?, bio_data.main_skills)", [$main_skills]);
        }

        if ($request->has('nationality')) {
            $query->where('bio_data.nationality', $request->input('nationality'));
        }

        if ($request->has('middle_name')) {
            $query->where('bio_data.middle_name', 'like', '%' . $request->input('middle_name') . '%');
        }

        $candidates = $query->select('users.*', 'bio_data.position_apply', 'bio_data.job_start_date', 'bio_data.job_type', 'bio_data.gender', 'bio_data.present_country', 'bio_data.work_status', 'bio_data.languages', 'bio_data.main_skills', 'bio_data.nationality', 'bio_data.middle_name')->paginate(20);

        $positionApply = $request->input('position_apply');
        $jobStartDate = $request->input('job_start_date');
        $jobtypeApply = $request->input('job_type');
        $genderApply = $request->input('gender');
        $present_countryApply = $request->input('present_country');
        $work_statusApply = $request->input('work_status');
        $languagesApply = $request->input('languages');
        $main_skillsApply = $request->input('main_skills');
        $nationalityApply = $request->input('nationality');
        $middle_nameApply = $request->input('middle_name');

        return view('frontend.pages.candidate.shortlist', compact('candidates', 'positionApply', 'jobStartDate', 'jobtypeApply', 'genderApply', 'present_countryApply', 'work_statusApply', 'languagesApply', 'main_skillsApply', 'nationalityApply', 'middle_nameApply'));
    }



    public function shortlist_candidate(Request $request){

        $candidateId = $request->input('candidate_id');

        $isFavorited = ShortList::where('candidate_id', $candidateId)->where('user_id', auth()->id())->exists();

        if ($isFavorited) {
            ShortList::where('candidate_id', $candidateId)
                ->where('user_id', auth()->id())
                ->delete();

            EmployerAction::where('candidate_id', $candidateId)
                ->where('user_id', auth()->id())
                ->delete();

            $favorited = false;
            $message = 'Candidate removed from your shortlist.';
        } else {

            ShortList::create([
                'candidate_id' => $candidateId,
                'short_list' => 'yes',
                'user_id' => auth()->id()
            ]);

            EmployerAction::create([
                'candidate_id' => $candidateId,
                'action' => 'New',
                'user_id' => auth()->id()
            ]);

            $favorited = true;
            $message = 'Candidate added to your shortlist.';
        }

        return response()->json(['favorited' => $favorited, 'message' => $message]);
    }


    public function action(Request $request)
    {

        $validated = $request->validate([
            'candidate_id' => 'required',
            'employer_action' => 'required|string'
        ]);

        $candidateId = $validated['candidate_id'];
        $employerActionValue = $validated['employer_action'];

        $employerAction = EmployerAction::where('candidate_id', $candidateId)->where('user_id', auth()->id())->first();

        if ($employerAction) {

            $employerAction->update([
                'action' => $employerActionValue,
            ]);
        }
        $message = 'Shortlist candidate status updated successfully';

        return response()->json(['success' => true, 'message' => $message]);
    }


    public function agencies_services(){

        $agencies = AgencyLandingPage::find(1);
        $agency_infos = User::where('role',User::ROLE_AGENCY)
            ->where('agency_role',User::AGENCY)
            ->addSelect([
                'max_rating' => AgencyReview::select(DB::raw('MAX(rating)'))
                    ->whereColumn('agency_reviews.agency_id', 'users.id')
            ])
            ->has('info')
            ->paginate('6');


        return view('frontend.pages.agency.find_agencies', compact('agencies','agency_infos'));
    }


    public function training_page(){

        $training_page = TrainingLandingPage::find(1);

        $trainings = User::where('role',User::ROLE_AGENCY)
            ->where('agency_role',User::TRAINING)
            ->addSelect([
                'max_rating' => AgencyReview::select(DB::raw('MAX(rating)'))
                    ->whereColumn('agency_reviews.agency_id', 'users.id')
            ])
            ->has('info')
            ->paginate('6');

        return view('frontend.pages.training.find_training', compact('training_page','trainings'));
    }


    public function training_details(User $user)
    {
        $user->load('info');
        $max_rating = AgencyReview::where('agency_id',$user->id)->max('rating');
        $reviews = AgencyReview::where('agency_id',$user->id)->has('user')->paginate(10);

        $latest_news = News::where('status', 1)->orderBy('id','desc')->limit(1)->first();


        return view('frontend.pages.training.training_details',compact('user',
            'reviews','max_rating','latest_news'));

    }

    public function partner_page(){

        $partner_landing_page = PartnerLandingPage::find(1);

        $partners = User::where('role',User::ROLE_AGENCY)
            ->where('agency_role',User::PARTNER)
            ->addSelect([
                'max_rating' => AgencyReview::select(DB::raw('MAX(rating)'))
                    ->whereColumn('agency_reviews.agency_id', 'users.id')
            ])
            ->has('info')
            ->paginate('6');

        return view('frontend.pages.partner.find_partner', compact('partners','partner_landing_page'));
    }

    public function partner_details(User $user)
    {
        $user->load('info');
        $max_rating = AgencyReview::where('agency_id',$user->id)->max('rating');
        $reviews = AgencyReview::where('agency_id',$user->id)->has('user')->paginate(10);

        $latest_news = News::where('status', 1)->orderBy('id','desc')->limit(1)->first();


        return view('frontend.pages.partner.partner_details',compact('user',
            'reviews','max_rating','latest_news'));

    }


    public function association_page(){

        $association_page = AssociationLandingPage::find(1);

        $associations = User::where('role',User::ROLE_AGENCY)
            ->where('agency_role',User::ASSOCIATION)
            ->addSelect([
                'max_rating' => AgencyReview::select(DB::raw('MAX(rating)'))
                    ->whereColumn('agency_reviews.agency_id', 'users.id')
            ])
            ->has('info')
            ->paginate('6');

        return view('frontend.pages.association.find_association', compact('association_page','associations'));
    }

    public function association_details(User $user)
    {
        $user->load('info');
        $max_rating = AgencyReview::where('agency_id',$user->id)->max('rating');
        $reviews = AgencyReview::where('agency_id',$user->id)->has('user')->paginate(10);

        $latest_news = News::where('status', 1)->orderBy('id','desc')->limit(1)->first();


        return view('frontend.pages.association.association_details',compact('user',
            'reviews','max_rating','latest_news'));

    }



    public function tips()
    {

        $tips_page = TipsLandingPage::find(1);

        $categoryIdsWithTips = Tips::where('status', 1)
            ->pluck('category_id')
            ->unique();

        $all_categories = Category::whereIn('id', $categoryIdsWithTips)->get();


        $all_tips = Tips::where('status', 1)->get();

        return view('frontend.pages.tips.tips', compact('tips_page', 'all_categories', 'all_tips'));
    }

    public function fetchMoreTips(Request $request)
    {
        $categoryId = $request->input('category_id');
        $offset = $request->input('offset');

        $tips = Tips::where('category_id', $categoryId)
            ->skip($offset)
            ->take(3)
            ->get();

        $html = '';
        foreach ($tips as $tip) {
            $url = route('tips_details', $tip);

            $html .= '<a class="list-group-item list-group-item-action ng-star-inserted tip-item" href="' . $url . '">
                    <i aria-hidden="true" class="fa fa-angle-double-right"></i>
                    ' . $tip->title . '
                  </a>';
        }

        $hasMore = Tips::where('category_id', $categoryId)->count() > $offset + 3;
        return response()->json([
            'html' => $html,
            'count' => $tips->count(),
            'hasMore' => $hasMore
        ]);
    }



    public function tips_details(Tips $tips)
    {
        $all_categories = Category::where('status', 1)->get();
        return view('frontend.pages.tips.tips_details', compact('tips', 'all_categories'));
    }




    public function news(){

        $news_page = NewLandingPage::find(1);
        $all_news = News::where('status', 1)->paginate(10);
        $all_categories = Category::where('status', 1)->get();

        return view('frontend.pages.news.news', compact('news_page', 'all_news', 'all_categories'));
    }

    public function news_details(News $news)
    {
        $all_categories = Category::where('status', 1)->get();

        return view('frontend.pages.news.news_details', compact('news', 'all_categories'));
    }

    public function agency_details(User $user)
    {
        $user->load('info');
        $max_rating = AgencyReview::where('agency_id',$user->id)->max('rating');
        $reviews = AgencyReview::where('agency_id',$user->id)->has('user')->paginate(10);
        $count_jobs = Job::where('user_id',$user->id)->count();
        $count_candidates = User::where('role',User::ROLE_CANDIDATE)
            ->where('added_by',$user->id)
            ->count();

        $latest_news = News::where('status', 1)->orderBy('id','desc')->limit(1)->first();


        return view('frontend/pages/agency/agency_details',compact('user',
            'reviews','max_rating','count_jobs',
            'count_candidates','latest_news'));
    }
    public function more_detail_service(Request $request,AgencyService $service)
    {
        $type = $request->type ?? null;
        $html = '';
        $html =  view('frontend/pages/agency/detail',compact('service','type'))->render();
        return response()->json([
            'response_data' => $html
        ]);
    }

    public function submit_agency_review(Request $request)
    {
        AgencyReview::create([
            'user_id' => Auth::user()->id,
            'agency_id' => $request->agency_id,
            'review' => $request->review,
            'rating' => $request->rating,
        ]);

        return redirect()->back()->with('success','Review Added Successfully');
    }


    public function about(){

        $avout_us_page = AboutUsLandingPage::find(1);

        return view('frontend.pages.about.about', compact('avout_us_page'));
    }


    public function public_holiday(Request $request) {

        $public_holiday_page = PublicHolidayLandingPage::find(1);

        $countries = Country::whereIn('id', PublicHoliday::pluck('country_id'))->get();

        $country_id = $request->input('country_id', '');
        $holiday_date = $request->input('holiday_date', '');

        $query = PublicHoliday::where('status', 1);

        if ($request->has('country_id')) {
            $query->where('country_id', $country_id);
        }

        if ($request->filled('holiday_date')) {
            $query->whereYear('holiday_date', $holiday_date);
        }

        $public_holidays = $query->get();

        return view('frontend.pages.public_holiday', compact('public_holiday_page', 'public_holidays', 'countries', 'country_id', 'holiday_date'));
    }



    public function pricing() {
        $plans = Plans::all();

        $allCountries = [];

        foreach ($plans as $plan) {
            $countries = explode(',', $plan->country);
            $allCountries = array_merge($allCountries, $countries);
        }

        $uniqueCountries = array_unique($allCountries);
        $countries = collect($uniqueCountries);

        return view('frontend.pages.pricing', compact('plans', 'countries'));
    }


    public function logClick(Request $request)
    {
        if (Auth::check() && Auth::user()->role == 'candidate') {

            $userId = Auth::user()->id;
            $jobId = $request->input('job_id');

            $existingClick = Click::where('user_id', $userId)->where('job_id', $jobId)->first();

            if ($existingClick) {

                return response()->json(['success' => false, 'message' => 'Click already exists'], 409);

            }

            $click = new Click();
            $click->user_id = $userId;;
            $click->job_id = $jobId;
            $click->ip_address = $request->ip();
            $click->click = 1;
            $click->save();

            return response()->json(['success' => true]);

        } else {
            return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
        }

    }

    public function event()
    {

        $event_page = EventsLandingPage::find(1);

        $all_events = Event::where('status', 1)->get();

        return view('frontend.pages.event.find_event', compact('event_page', 'all_events'));
    }

    public function event_details(Event $event)
    {

        return view('frontend.pages.event.event_details', compact('event'));
    }

    public function happy_helpers()
    {

        $happy_helpers = HappyHelpersPage::find(1);

        return view('frontend.pages.about.happy_helpers', compact('happy_helpers'));
    }


    public function faqs()
    {

        $faqs = Faq::where('status', 1)->get();

        return view('frontend.pages.about.faqs', compact('faqs'));
    }

    public function terms()
    {

        $terms = TermsAndConditionPage::find(1);

        return view('frontend.pages.about.terms', compact('terms'));
    }

    public function privacy()
    {

        $privacy = PrivacyPolicy::find(1);

        return view('frontend.pages.about.privacy', compact('privacy'));
    }

    public function contact()
    {
        $contact = ContactUsPage::find(1);
        return view('frontend.pages.about.contact', compact('contact'));
    }


    public function happy_employers()
    {

        $happy_employers_page = HappyEmployeesPage::find(1);
        $happy_employers      = HappyEmplyee::where('status', 1)->get();

        return view('frontend.pages.about.happy_employers', compact('happy_employers_page', 'happy_employers'));
    }

    public function partner_signup()
    {
        return view('frontend.partner_signup');
    }

    public function partner_login()
    {
        return view('frontend.partner_login');
    }

    public function login_partner(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed
            $user = Auth::User();
            if ($user->status == User::BLOCK){
                Auth::logout();
                return redirect()->back()->with('danger','Waiting for approval from admin side');
            }else{
                return redirect()->route('agency_dashboard');
            }
        }else{
            return redirect()->back()->with('danger','Invalid Credentials');
        }
    }

    public function save_agency(Request $request)
    {

        $request->validate([
            'company_name' => 'required',
            'contact_person' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
            'agency_type' => 'required',
        ]);

        User::create([
            'role' => User::ROLE_AGENCY,
            'company_name' => $request->company_name,
            'name' => $request->contact_person,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'agency_role' => $request->agency_type,
            'status' => User::BLOCK
        ]);

        return redirect()->back()->with('danger','Your profile created successfully,wait for admin approval');
    }


}
