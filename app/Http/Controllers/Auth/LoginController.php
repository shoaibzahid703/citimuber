<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('auth')->only('logout');
    }


    protected function authenticated(Request $request,$user){
        if($user->role === User::ROLE_ADMIN){
            return redirect()->route('admin_dashboard')->with('success','Admin Login successfully');
        }else{
            return redirect('/');
        }
    }



    public function login(Request $request){

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed
            $user = Auth::User();

            if ($user->role === 'admin') {
                return response()->json(['redirect' => route('admin_dashboard')]);
            }elseif ($user->role === 'candidate') {
                if ($user->status == User::BLOCK){
                    Auth::logout();
                    return response()->json(['message' => 'Block From System'], 422);
                }else{
                    return response()->json(['redirect' => route('candidate_dashboard')]);
                }
            }elseif ($user->role === 'employer') {
                if ($user->status == User::BLOCK){
                    Auth::logout();
                    return response()->json(['message' => 'Block From System'], 422);
                }else{
                    return response()->json(['redirect' => route('employer_dashboard')]);
                }
            }elseif ($user->role === 'agency'){
                if ($user->status == User::BLOCK){
                    Auth::logout();
                    return response()->json(['message' => 'Waiting for approval from admin side'], 422);
                }else{
                    return response()->json(['redirect' => route('agency_dashboard')]);
                }
            }

            return response()->json(['redirect' => route('/')]);
        }
        return response()->json(['message' => 'Invalid credentials.'], 422);
    }

    public function showLoginForm()
    {
        return redirect()->route('welcome');
    }
}
