<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{


    public function index()
    {
        $transaction = Transaction::with('helper_plan')
            ->where('user_id',Auth::id())
            ->where('plan_type',Transaction::PLAN_TYPE_CANDIDATE)
            ->whereNotNull('expires_at')
            ->where('expires_at', '>', now())
            ->latest()
            ->first();



        return view('candidate.pages.dashboard',compact('transaction'));
    }
}
