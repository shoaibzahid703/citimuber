<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;
use App\Models\Language;
use App\Traits\imageUploadTrait;

class ProfileController extends Controller
{
    use imageUploadTrait;

    public function index()
    {
        $country = Country::where('status',Country::ACTIVE)->get();
        $language = Language::all();
        return view('candidate.pages.profile', compact('country', 'language'));
    }

    public function update(Request $request){

        $user = Auth::user();
        $data = [
            'name' =>     $request->name,
            'email' =>    $request->email,
            'mobile' =>   $request->mobile,
            'passport' => $request->passport,
            'language' => $request->language,
            'location' => $request->location,
        ];

        if ($request->hasFile('profile_image')) {
            $user['profile_image'] = self::uploadFile($request, fileName: 'profile_image', folderName: 'profile_image');
        }

        $data['mobile'] = $request->formatted_mobile;

        $user->update($data);
        return redirect()->route('candidate_profile',['password_tab'=> false])->with('success','profile updated successfully');
    }


    public function candidate_update_password(Request $request){

        $rules = [
            'current_password' => 'required',
            'password' => 'required|string|confirmed',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('candidate_profile', ['password_tab' => true])
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();
        $hashedPassword = $user->password;

        if (Hash::check($request->current_password, $hashedPassword)) {
            if (!Hash::check($request->password, $hashedPassword)) {
                $user->password = Hash::make($request->password);
                $user->save();

                return redirect()->back()->with('success','Password updated successfully');
            } else {
                return redirect()->route('candidate_profile', ['password_tab' => true])
                    ->with('danger', 'New password cannot be the same as the old password.')
                    ->with('password_tab', true);
            }
        } else {
            return redirect()->route('candidate_profile', ['password_tab' => true])
                ->with('danger', 'Old password does not match.')
                ->with('password_tab', true);
        }
    }
    public function change_visibility(Request $request){
        $user = Auth::user();
        if ($user->visibility_status == User::VISIBLE){
            $user->update([
                'visibility_status' => 0
            ]);
            return redirect()->back()->with('success','Thanks, Your resume status will be changed to Hidden after sometime.');
        }else{
            $user->update([
                'visibility_status' => 1
            ]);
            return redirect()->back()->with('success','Thanks, Your resume status will be changed to Show after sometime.');
        }
    }
}
