<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use App\Models\BioData;
use App\Models\User;
use App\Models\UserEducation;
use App\Models\UserWorkExperience;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use App\Models\State;
use App\Models\Country;
use App\Models\City;
use App\Models\Language;
use App\Models\MainSkill;
use App\Models\CookingSkill;
use App\Models\OtherSkill;
use App\Models\Personality;
use App\Models\Duty;
use App\Models\Currency;
use App\Models\JobApply;
use App\Traits\imageUploadTrait;
use App\Models\JobPosition;
use App\Models\Education;
use App\Models\Religion;
use App\Models\EducationLevel;


class BiodataController extends Controller
{
    use imageUploadTrait;

    public function create()
    {
        $state = State::where('status',State::ACTIVE)->get();
        $language = Language::all();
        $country = Country::where('status',Country::ACTIVE)->get();
        $city = City::where('status',City::ACTIVE)->get();
        $main_skills = MainSkill::where('status',MainSkill::ACTIVE)->get();
        $cookingSkills = CookingSkill::where('status',CookingSkill::ACTIVE)->get();
        $otherSkills = OtherSkill::where('status',OtherSkill::ACTIVE)->get();
        $personality = Personality::where('status',Personality::ACTIVE)->get();
        $duties = Duty::where('status',Duty::ENABLE)->get();
        $currency = Currency::where('status',Currency::ENABLE)->get();
        $nationalities = Country::whereNotNull('nationality')->where('status',Country::ACTIVE)->get();

        $job_positions = JobPosition::where('status', 1)->get();
        $educations = Education::where('status', 1)->get();

        $religions = Religion::where('status', 1)->get();
        $education_levels = EducationLevel::where('status', 1)->get();


        $user =  Auth::user() ;

        $user->load('resume_detail','working_experience','education');
        $count_experience = $user->working_experience->count();
        $count_education = $user->education->count();
        $next_experience_iteration = 0;
        $next_education_iteration = 0;
        if ($user->working_experience && $count_experience > 0){
            $next_experience_iteration = $count_experience + 1;
            if ($next_experience_iteration != 1){
                $next_experience_iteration = $next_experience_iteration - 1;
            }
        }
        if ($user->education && $count_education > 0){
            $next_education_iteration = $count_education + 1;
            if ($next_education_iteration != 1){
                $next_education_iteration = $next_education_iteration - 1;
            }
        }

        return view('candidate.pages.bio_data', compact('nationalities','user','next_experience_iteration','next_education_iteration','state','country', 'language', 'city', 'main_skills', 'cookingSkills', 'personality', 'otherSkills','duties', 'currency', 'job_positions', 'educations', 'religions', 'education_levels'));
    }

    public function validateBioDataStepOne(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|max:100',
            'middle_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'age' => 'required|integer|max:100',
            'gender' => 'required',
            'marital_status' => 'required',
            'kids_detail' => 'required',
            'nationality' => 'required',
            'present_country' => 'required',
            'religion' => 'required',
            'education_level' => 'required',
            'mobile_number' => 'required',
            'whatsapp_number' => 'required',
            'valid_password' => 'required',
        ];
        $user =  Auth::user() ;
        if (is_null($user->profile_image)){
            $rules['profile_image'] = 'required';
        }
        $validator = Validator::make( $request->all(), $rules);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function validateBioDataStepTwo(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'position_apply' => 'required|string|max:100',
            'work_experience' => 'required|string|max:100',
            'job_type' => 'required|string|max:100',
            'work_status' => 'required|string|max:100',
            'job_start_date' => 'required|string|max:100',
            'preferred_job_location' => 'required',
            'monthly_salary' => 'required',
            'currency' => 'required',
            'day_off_preference' => 'required',
            'accomodation_preference' => 'required',
            'languages' => 'required',
            'main_skills' => 'required',
            'cooking_skills' => 'required',
            'other_skills' => 'required',
            'personalities' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function validateBioDataStepThree(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'job_position' => 'required',
            'working_country' => 'required',
            'start_date' => 'required|string|max:100',
            'job_end_date' => 'required|string|max:100',
            'employer_type' => 'required',
            'family_type' => 'required',
            'employer_nationality' => 'required',
            'job_duties' => 'required',
            'reference_letter' => 'required',
            'education' => 'required',
            'course_duration' => 'required',
            'completed_this_course' => 'required',
            'year_completion' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function validateBioDataStepFour(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'resume_description' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function store(Request $request)
    {

        if(isset($request->location) && is_array($request->location)){
            @$location = implode(',',$request->location);
        }else{
            @$location = ' ';
        }

        if(isset($request->dateFrom) && is_array($request->dateFrom)){
            @$dateFrom = implode(',', $request->dateFrom);
        }else{
            @$dateFrom = ' ';
        }
        if(isset($request->dateTo) && is_array($request->dateTo)){
            @$dateTo = implode(',',$request->dateTo);
        }else{
            @$dateTo = '';
        }
        if(isset($request->salary) && is_array($request->salary)){
            @$salary = implode(',',$request->salary);
        }else{
            @$salary = '';
        }
        if(isset($request->district) && is_array($request->district)){
            @$district = implode(',',$request->district);
        }else{
            @$district = '';
        }
        if(isset($request->language) && is_array($request->language)){
            @$language = implode(',',$request->language);
        }else{
            @$language = '';
        }
        if(isset($request->size) && is_array($request->size)){
            @$size = implode(',',$request->size);
        }else{
            @$size = '';
        }

        if(isset($request->employer_nationality) && is_array($request->employer_nationality)){
            @$employer_nationality = implode(',',$request->employer_nationality);
        }else{
            @$employer_nationality = '';
        }
        if(isset($request->family_members) && is_array($request->family_members)){
            @$family_members = implode(',',$request->family_members);
        }else{
            @$family_members = '';
        }
        if(isset($request->num_adults) && is_array($request->num_adults)){
            @$num_adults = implode(',',$request->num_adults);
        }else{
            @$num_adults = '';
        }
        if(isset($request->num_babies) && is_array($request->num_babies)){
            @$num_babies = implode(',',$request->num_babies);
        }else{
            @$num_babies = '';
        }
        if(isset($request->num_children) && is_array($request->num_children)){
            @$num_children = implode(',',$request->num_children);
        }else{
            @$num_children = '';
        }
        if(isset($request->num_elderly) && is_array($request->num_elderly)){
            @$num_elderly = implode(',',$request->num_elderly);
        }else{
            @$num_elderly = '';
        }
        if(isset($request->main_duties) && is_array($request->main_duties)){
            @$main_duties = implode(',',$request->main_duties);
        }else{
            @$main_duties = '';
        }
        if(isset($request->reason) && is_array($request->reason)){
            @$reason = implode(',',$request->reason);
        }else{
            @$reason = '';
        }
        if(isset($request->years_experience) && is_array($request->years_experience)){
            @$years_experience = implode(',',$request->years_experience);
        }else{
            @$years_experience = '';
        }

        if(isset($request->months_experience) && is_array($request->months_experience)){
            @$months_experience = implode(',',$request->months_experience);
        }else{
            @$months_experience = '';
        }
        // For 'babyCare'
        if (isset($request->employer_babyCare) && is_array($request->employer_babyCare)) {
            $employer_babyCare = implode(',', $request->employer_babyCare);
        } else {
            $employer_babyCare = '';
        }

// For 'childCare'
        if (isset($request->employer_childCare) && is_array($request->employer_childCare)) {
            $employer_childCare = implode(',', $request->employer_childCare);
        } else {
            $employer_childCare = '';
        }

// For 'elderlyCare'
        if (isset($request->employer_elderlyCare) && is_array($request->employer_elderlyCare)) {
            $employer_elderlyCare = implode(',', $request->employer_elderlyCare);
        } else {
            $employer_elderlyCare = '';
        }

// For 'housework'
        if (isset($request->employer_housework) && is_array($request->employer_housework)) {
            $employer_housework = implode(',', $request->employer_housework);
        } else {
            $employer_housework = '';
        }

// For 'cooking'
        if (isset($request->employer_cooking) && is_array($request->employer_cooking)) {
            $employer_cooking = implode(',', $request->employer_cooking);
        } else {
            $employer_cooking = '';
        }

// For 'washing'
        if (isset($request->employer_washing) && is_array($request->employer_washing)) {
            $employer_washing = implode(',', $request->employer_washing);
        } else {
            $employer_washing = '';
        }

// For 'disabledCare'
        if (isset($request->employer_disabledCare) && is_array($request->employer_disabledCare)) {
            $employer_disabledCare = implode(',', $request->employer_disabledCare);
        } else {
            $employer_disabledCare = '';
        }

// For 'driving'
        if (isset($request->employer_driving) && is_array($request->employer_driving)) {
            $employer_driving = implode(',', $request->employer_driving);
        } else {
            $employer_driving = '';
        }

// For 'bedriddenCare'
        if (isset($request->employer_bedriddenCare) && is_array($request->employer_bedriddenCare)) {
            $employer_bedriddenCare = implode(',', $request->employer_bedriddenCare);
        } else {
            $employer_bedriddenCare = '';
        }

// For 'gardening'
        if (isset($request->employer_gardening) && is_array($request->employer_gardening)) {
            $employer_gardening = implode(',', $request->employer_gardening);
        } else {
            $employer_gardening = '';
        }

// For 'petsCare'
        if (isset($request->employer_petsCare) && is_array($request->employer_petsCare)) {
            $employer_petsCare = implode(',', $request->employer_petsCare);
        } else {
            $employer_petsCare = '';
        }

// For 'specialneeds'
        if (isset($request->employer_specialneeds)) {
            $employer_specialneeds = implode(',', $request->employer_specialneeds);
        } else {
            $employer_specialneeds = '';
        }

        BioData::create([
            'cnahk' => $request->cnahk,
            'phone_no' => $request->phone_no,
            'release_date' => $request->release_date,
            'expiry_date' => $request->expiry_date,
            'apply_date' => $request->apply_date,
            'ref_number' => $request->ref_number,
            'address' => $request->address,
            'state' => $request->state,
            'name' => $request->name,
            'age' => $request->age,
            'dob' => $request->dob,
            'education' => $request->education,
            'personal_nationality' => $request->personal_nationality,
            'id_number' => $request->id_number,
            'religion' => $request->religion,
            'birthplace' => $request->birthplace,
            'height' => $request->height,
            'weight' => $request->weight,
            'no_children' => $request->no_children,
            'marital_status' => $request->marital_status,
            'nursing' => $request->nursing,
            'midwifery' => $request->midwifery,
            'education_course' => $request->education_course,
            'caregiver_course' => $request->caregiver_course,
            'baby_care' => $request->baby_care,
            'child_care' => $request->child_care,
            'elderly_care' => $request->elderly_care,
            'housework' => $request->housework,
            'cooking' => $request->cooking,
            'washing' => $request->washing,
            'disabled_care' => $request->disabled_care,
            'driving' => $request->driving,
            'bedridden_care' => $request->bedridden_care,
            'gardening' => $request->gardening,
            'pets_care' => $request->pets_care,
            'special_needs' => $request->special_needs,
            'english_level' => $request->english_level,
            'cantonese_level' => $request->cantonese_level,
            'mandarin_level' => $request->mandarin_level,
            'newborn_care' => $request->newborn_care,
            'elderly_care_willing' => $request->elderly_care_willing,
            'disabled_care_willing' => $request->disabled_care_willing,
            'share_room' => $request->share_room,
            'accept_day_off' => $request->accept_day_off,
            'work_experience_years' => $request->work_experience_years,
            'work_experience_months' => $request->work_experience_months,
            'location' => $location,
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'salary' => $salary,
            'district' => $district,
            'language' => $language,
            'size' => $size,
            'employer_nationality' => $employer_nationality,
            'family_members' => $family_members,
            'num_adults' => $num_adults,
            'num_babies' => $num_babies,
            'num_children' => $num_children,
            'num_elderly' => $num_elderly,
            'employer_babyCare' => $employer_babyCare,
            'employer_childCare' => $employer_childCare,
            'employer_elderlyCare' => $employer_elderlyCare,
            'employer_housework' => $employer_housework,
            'employer_cooking' => $employer_cooking,
            'employer_washing' => $employer_washing,
            'employer_disabledCare' => $employer_disabledCare,
            'employer_driving' => $employer_driving,
            'employer_bedriddenCare' => $employer_bedriddenCare,
            'employer_gardening' => $employer_gardening,
            'employer_petsCare' => $employer_petsCare,
            'employer_specialneeds' => $employer_specialneeds,
            'reason_for_leaving' => $reason,
            'years_experience' => $years_experience,
            'months_experience' => $months_experience,
        ]);

        return redirect()->route('employment_biodata.create')->with('success', 'Data saved successfully.');
    }

    public function view_bio_data(Request $request){

        $boidata = BioData::orderBy('id', 'DESC')->latest()->get();
        return view('candidate.pages.admin_employer_bio_data', compact('boidata'));
    }

    public function save(Request $request)
    {
        if($request->profile_image){
            $profile = self::uploadFile($request, fileName: 'profile_image', folderName: 'profile_image');
            Auth::user()->update([
                'profile_image' =>  $profile,
            ]);
        }
        if ($request->mobile_number){
            Auth::user()->update([
                'mobile' =>  $request->formatted_mobile
            ]);
        }

        $user =  Auth::user() ;
        $user->load('resume_detail','working_experience','education');

        if ($user->resume_detail){
            $user->resume_detail->update([
                'first_name' =>         $request->first_name,
                'middle_name'=>         $request->middle_name,
                'last_name' =>          $request->last_name,
                'age' =>                $request->age,
                'gender' =>             $request->gender,
                'marital_status' =>     $request->marital_status,
                'kids_detail' =>        $request->kids_detail,
                'nationality' =>        $request->nationality,
                'present_country' =>    $request->present_country,
                'religion' =>           $request->religion,
                'education_level' =>    $request->education_level,
                'whatsapp_number' =>    $request->formatted_whatsapp_number,
                'valid_password' =>     $request->valid_password,
                'position_apply' =>     $request->position_apply,
                'work_experience' =>    $request->work_experience,
                'job_type' =>           $request->job_type,

                'work_status' =>        $request->work_status,
                'job_start_date' =>     $request->job_start_date,

                'job_start_date' =>     Carbon::parse($request->job_start_date)->toDate() ,

                'preferred_job_location' =>    implode(',', $request['preferred_job_location']),
                'monthly_salary' =>      $request->monthly_salary,
                'currency' =>            $request->currency,
                'day_off_preference' =>  $request->day_off_preference,
                'accomodation_preference' =>    $request->accomodation_preference,
                'languages' =>    implode(',', $request['languages']),
                'main_skills' =>    implode(',', $request['main_skills']),
                'cooking_skills' =>    implode(',', $request['cooking_skills']),
                'other_skills' =>    implode(',', $request['other_skills']),
                'personalities' =>    implode(',', $request['personalities']),
                'resume_description' =>    $request->resume_description,
                'newsletter' =>    $request->newsletter,
                'opportunities' =>    $request->opportunities,
            ]);
        }else{
            BioData::create([
                'user_id' =>      \Auth::user()->id,
                'first_name' =>         $request->first_name,
                'middle_name'=>         $request->middle_name,
                'last_name' =>          $request->last_name,
                'age' =>                $request->age,
                'gender' =>             $request->gender,
                'marital_status' =>     $request->marital_status,
                'kids_detail' =>        $request->kids_detail,
                'nationality' =>        $request->nationality,
                'present_country' =>    $request->present_country,
                'religion' =>           $request->religion,
                'education_level' =>    $request->education_level,
                'whatsapp_number' =>    $request->formatted_whatsapp_number,
                'valid_password' =>     $request->valid_password,
                'position_apply' =>     $request->position_apply,
                'work_experience' =>    $request->work_experience,
                'job_type' =>           $request->job_type,

                'work_status' =>        $request->work_status,
                'job_start_date' =>     $request->job_start_date,

                'job_start_date' =>     Carbon::parse($request->job_start_date)->toDate() ,

                'preferred_job_location' =>    implode(',', $request['preferred_job_location']),
                'monthly_salary' =>      $request->monthly_salary,
                'currency' =>            $request->currency,
                'day_off_preference' =>  $request->day_off_preference,
                'accomodation_preference' =>    $request->accomodation_preference,
                'languages' =>    implode(',', $request['languages']),
                'main_skills' =>    implode(',', $request['main_skills']),
                'cooking_skills' =>    implode(',', $request['cooking_skills']),
                'other_skills' =>    implode(',', $request['other_skills']),
                'personalities' =>    implode(',', $request['personalities']),
                'resume_description' =>    $request->resume_description,
                'newsletter' =>    $request->newsletter,
                'opportunities' =>    $request->opportunities,
            ]);
        }

        if ($user->working_experience){
            if (isset($request['job_position'])){
                foreach ($request['job_position'] as $key => $val){
                    $exp_data = [
                        'job_position' => $val,
                        'working_country' => isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' => isset($request['start_date'][$key]) ? Carbon::parse($request['start_date'][$key])->toDate() : null,
                        'job_end_date' => isset($request['job_end_date'][$key]) ? Carbon::parse($request['job_end_date'][$key])->toDate() : null,
                        'employer_type' => isset($request['employer_type'][$key]) ? $request['employer_type'][$key] : null,
                        'family_type' =>    isset($request['family_type'][$key]) ? $request['family_type'][$key] : null,
                        'employer_nationality' =>    isset($request['employer_nationality'][$key]) ? $request['employer_nationality'][$key] : null,
                        'job_duties' => isset($request['job_duties'][$key]) ? implode(',', $request['job_duties'][$key]) : null,
                        'reference_letter' => isset($request['reference_letter'][$key]) ? $request['reference_letter'][$key] : null,
                    ];
                    UserWorkExperience::updateOrCreate([
                        'user_id' => \Auth::user()->id,
                        'job_position' => $val,
                        'working_country' => isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' => $request['start_date'][$key],
                        'job_end_date' => $request['job_end_date'][$key],
                    ], $exp_data);
                }
            }
        }else{
            if (isset($request['job_position'])){
                foreach ($request['job_position'] as $key => $val){
                    UserWorkExperience::create([
                        'user_id' =>      \Auth::user()->id,
                        'job_position' =>    $val,
                        'working_country' =>   isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' =>    isset($request['start_date'][$key]) ? Carbon::parse($request['start_date'][$key])->toDate() :null,
                        'job_end_date' =>    isset($request['job_end_date'][$key]) ? Carbon::parse($request['job_end_date'][$key])->toDate() : null,
                        'employer_type' =>    isset($request['employer_type'][$key]) ? $request['employer_type'][$key] : null,
                        'family_type' =>    isset($request['family_type'][$key]) ? $request['family_type'][$key] : null,
                        'employer_nationality' =>    isset($request['employer_nationality'][$key]) ? $request['employer_nationality'][$key] : null,
                        'job_duties' =>   isset($request['job_duties'][$key]) ? implode(',',$request['job_duties'][$key])  :null,
                        'reference_letter' =>    isset($request['reference_letter'][$key]) ? $request['reference_letter'][$key]: null,
                    ]);
                }
            }
        }

        if ($user->education){
            if (isset($request['education'])){
                foreach ($request['education'] as $key => $val){
                    $edu_data = [
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'course_duration' =>    isset($request['course_duration'][$key]) ? $request['course_duration'][$key] : null,
                        'completed_this_course' =>   isset($request['completed_this_course'][$key]) ? $request['completed_this_course'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ];

                    UserEducation::updateOrCreate([
                        'user_id' =>      \Auth::user()->id,
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ], $edu_data);
                }
            }
        }else{
            if (isset($request['education'])){
                foreach ($request['education'] as $key => $val){
                    UserEducation::create([
                        'user_id' =>      \Auth::user()->id,
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'course_duration' =>    isset($request['course_duration'][$key]) ? $request['course_duration'][$key] : null,
                        'completed_this_course' =>   isset($request['completed_this_course'][$key]) ? $request['completed_this_course'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ]);
                }
            }
        }

        return redirect()->route('helper_packages')->with('success', 'Resume Update Successfully!');
    }

    public function resume_detail(User $user)
    {
        $user->load('resume_detail.user_nationality','resume_detail.present_location','resume_detail.salary_currency','working_experience.work_country','education');

        return view('candidate.pages.resume.detail',compact('user'));
    }

    public function delete_experience(UserWorkExperience $experience)
    {
        $experience->delete();

        return redirect()->back()->with('success', 'Experience Deleted Successfully!');
    }

    public function delete_education(UserEducation $education)
    {
        $education->delete();
        return redirect()->back()->with('success', 'Education Deleted Successfully!');
    }

    public function job_apply(Request $request)
    {
        JobApply::create([
            'user_id' => Auth::user()->id,
            'job_id' => $request->job_id,

        ]);
        $message = 'Job Apply Successfully!';
        return redirect()->back()->with('success', $message);

    }
}
