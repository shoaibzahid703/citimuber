<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use App\Models\AdminMessage;
use App\Models\Job;
use App\Models\JobApply;
use App\Models\User;
use App\Models\UserMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function support(Request $request)
    {
        $admin = User::where('role',User::ROLE_ADMIN)
            ->first();

        $count_admin_un_read_message = AdminMessage::where('from_user',$admin->id)
            ->where('to_user',auth()->user()->id)
            ->where('is_read',0)
            ->count();


        if ($count_admin_un_read_message > 0){
            AdminMessage::where('from_user',$admin->id)
                ->where('to_user',auth()->user()->id)
                ->where('is_read',0)
                ->update([
                    'is_read' => 1
                ]);
        }


        $messages = AdminMessage::where(function($query) use ($admin, $request) {
            $query->where('from_user', auth()->user()->id)
                ->where('to_user', $admin->id);
        })->orWhere(function ($query) use ($admin, $request) {
            $query->where('from_user', $admin->id
            )->where('to_user', auth()->user()->id);
        })->orderBy('created_at', 'asc')
            ->get();


        return view('candidate.pages.message.admin-chat-index',compact(
            'admin',
            'messages'
        ));
    }

    public function send_support_msg(Request $request)
    {
        $message =  AdminMessage::create([
            'message' => $request->comment,
            'from_user' => $request->from_user,
            'to_user' => $request->to_user,
            'admin' => 'to_user',
        ]);
        $message_html = view('candidate.pages.message.admin-message-line',compact('message'))->render();
        return response()->json([
            'status' => true,
            'message_html' => $message_html,
        ]);
    }

    public function get_received_messages(Request $request)
    {
        $from_user = $request->from_user;
        $to_user = $request->to_user;

        $messages = AdminMessage::where(function($query) use ($to_user, $from_user, $request) {
            $query->where('from_user', $from_user)
                ->where('to_user',$to_user)
                ->where('is_read',0);
        })->orderBy('created_at', 'asc')
            ->get();


        if ($messages->count() > 0){
            $received_message = true;

            AdminMessage::where('from_user',$from_user)
                ->where('to_user',$to_user)
                ->where('is_read',0)
                ->update([
                    'is_read' => 1
                ]);

            $message_html = view('candidate.pages.message.admin-message-line',compact('messages','received_message'))
                ->render();
            return response()->json([
                'status' => true,
                'message_html' => $message_html,
            ]);
        }else{
            return response()->json([
                'status' => false,
            ]);
        }
    }

    public function messages(Request $request)
    {
        $send_messages_users_ids =   UserMessage::where('to_user',Auth::user()->id)
            ->pluck('from_user');
        $jobs_apply_job_ids = JobApply::where('user_id',Auth::user()->id)->pluck('job_id');
        $employer_ids = Job::whereIn('id',$jobs_apply_job_ids)->pluck('user_id');
        $combine_users_ids = $send_messages_users_ids->merge($employer_ids);
        $combine_users_ids = $combine_users_ids->unique();

        $users = User::whereIn('id', $combine_users_ids)
            ->get();
        $users_ids = $combine_users_ids;

        return view('candidate.pages.message.index',compact(
            'users','users_ids'
        ));
    }

    public function chat_index(User $receiver,Request $request)
    {

        $count_admin_un_read_message = UserMessage::where('from_user',$receiver->id)
            ->where('to_user',auth()->user()->id)
            ->where('is_read',0)
            ->count();

        if ($count_admin_un_read_message > 0){
            UserMessage::where('from_user',$receiver->id)
                ->where('to_user',auth()->user()->id)
                ->where('is_read',0)
                ->update([
                    'is_read' => 1
                ]);
        }


        $messages = UserMessage::where(function($query) use ($receiver, $request) {
            $query->where('from_user', auth()->user()->id)
                ->where('to_user', $receiver->id);
        })->orWhere(function ($query) use ($receiver, $request) {
            $query->where('from_user', $receiver->id
            )->where('to_user', auth()->user()->id);
        })->orderBy('created_at', 'asc')
            ->get();

        $send_messages_users_ids =   UserMessage::where('to_user',Auth::user()->id)
            ->pluck('from_user');
        $jobs_apply_job_ids = JobApply::where('user_id',Auth::user()->id)->pluck('job_id');
        $employer_ids = Job::whereIn('id',$jobs_apply_job_ids)->pluck('user_id');
        $combine_users_ids = $send_messages_users_ids->merge($employer_ids);
        $combine_users_ids = $combine_users_ids->unique();

        $users = User::whereIn('id', $combine_users_ids)
            ->get();

        $users_ids = $combine_users_ids;

        return view('candidate.pages.message.chat_index',compact(
            'receiver',
            'messages',
            'users','users_ids'
        ));
    }

    public function send_message(Request $request)
    {
        $message =  UserMessage::create([
            'message' => $request->comment,
            'from_user' => $request->from_user,
            'to_user' => $request->to_user,
        ]);
        $message_html = view('candidate.pages.message.admin-message-line',compact('message'))->render();

        return response()->json([
            'status' => true,
            'message_html' => $message_html,
        ]);
    }

    public function unread_messages_count(Request $request)
    {
        $data  = [];
        foreach (explode(',',$request->users_ids) as $user){
            $count_un_read_message =   UserMessage::where('from_user',$user)
                ->where('to_user',auth()->user()->id)
                ->where('is_read',0)
                ->count();
            if ($count_un_read_message > 0){
                $result = [
                    'user_id' => $user,
                    'count_message' => $count_un_read_message ,
                    'is_remove' => false ,
                ];
                array_push($data,$result);
            }else{
                $result = [
                    'user_id' => $user,
                    'count_message' => $count_un_read_message ,
                    'is_remove' => true ,
                ];
                array_push($data,$result);
            }
        }
        if (count($data) > 0){
            return response()->json([
                'status' => true,
                'response_data' => $data,
            ]);
        }else{
            return response()->json([
                'status' => false,
            ]);
        }
    }

    public function received_messages(Request $request)
    {
        $from_user = $request->from_user;
        $to_user = $request->to_user;

        $messages = UserMessage::where(function($query) use ($to_user, $from_user, $request) {
            $query->where('from_user', $from_user)
                ->where('to_user',$to_user)
                ->where('is_read',0);
        })->orderBy('created_at', 'asc')
            ->get();

        if ($messages->count() > 0){
            $received_message = true;

            UserMessage::where('from_user',$from_user)
                ->where('to_user',$to_user)
                ->where('is_read',0)
                ->update([
                    'is_read' => 1
                ]);

            $message_html = view('candidate.pages.message.admin-message-line',compact('messages','received_message'))
                ->render();
            return response()->json([
                'status' => true,
                'message_html' => $message_html,
            ]);
        }else{
            return response()->json([
                'status' => false,
            ]);
        }
    }

    public function mark_as_read(User $receiver)
    {
        UserMessage::where('from_user',$receiver->id)
            ->where('to_user',auth()->user()->id)
            ->where('is_read',0)
            ->update([
                'is_read' => 1
            ]);

        return redirect()->back()->with('success','Read Messages Successfully');
    }

}
