<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\HelperPlan;
use App\Models\Plans;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Stripe;

class HelperPackageController extends Controller
{
    public function helper_packages()
    {
        $transaction = Transaction::where('user_id',Auth::id())
            ->where('plan_type',Transaction::PLAN_TYPE_CANDIDATE)
            ->whereNotNull('expires_at')
            ->first();


        $plans = HelperPlan::all();

        if ($transaction){
            $plans = HelperPlan::where('price','!=',0.0)->get();
        }

        return view('candidate.pages.packages',compact('plans'));
    }

    public function pay_helper_package(Request $request,HelperPlan $package)
    {
        $currency_select = $request->currency;
        $currency_price = $request->converted_price;
        if (is_null($currency_select) && is_null($currency_select)){
            if ($package->price == 0){

                $today = Carbon::now();
                $expire_date = $today->addDays($package->post_duration);

                Transaction::create([
                    'user_id' => Auth::id(),
                    'plan_id' => $package->id,
                    'payment_id' => Str::random(27),
                    'amount' => 0,
                    'currency' => 'hkd',
                    'plan_type' => 'candidate',
                    'expires_at' => $expire_date
                ]);

                if ($package->employer_contact == 0){
                    Auth::user()->update([
                        'is_contact' => 0
                    ]);
                }

                 return redirect()->route('bio-data')->with('success','Package Subscribed Successfully');
            }else{
                return view('candidate.pages.stripe_pay_card',compact('currency_price','currency_select','package'));
            }
        }else{
            return view('candidate.pages.stripe_pay_card',compact('currency_price','currency_select','package'));
        }
    }

    public function pay_helper_package_payment(Request $request,HelperPlan $package)
    {
        $selected_price = $request->currency_price;
        $selected_currency = $request->currency_select;
        if (!is_null($selected_currency) &&  !is_null($selected_price)) {
            $currency_code_country = Country::where('code',$selected_currency)->first();
            $selected_currency_code = 'hkd';
            if ($currency_code_country && !is_null($currency_code_country->currency_code)){
                $selected_currency_code = strtolower($currency_code_country->currency_code);
            }

            $stripe_secret = env('STRIPE_SECRET');
            Stripe\Stripe::setApiKey($stripe_secret);

            $charge =   Stripe\Charge::create ([
                "amount" => $selected_price * 100,
                "currency" => $selected_currency_code,
                "source" => $request->stripeToken,
                // "description" => "Job Payment"
                "description" => 1
            ]);
            if ($charge->status == "succeeded"){

                $today = Carbon::now();
                $expire_date = $today->addDays($package->post_duration);

                $transaction = new Transaction();
                $transaction->user_id = Auth::id();
                $transaction->plan_id = $package->id;
                $transaction->payment_id = $charge->id;
                $transaction->amount = $selected_price;
                $transaction->currency = $charge->currency;
                $transaction->plan_type = 'candidate';
                $transaction->expires_at = $expire_date;
                $transaction->save();

                if ($package->employer_contact == 0){
                    Auth::user()->update([
                      'is_contact' => 0
                    ]);
                }

                return redirect()->route('bio-data')->with('success','Payment Done Successfully');
            }else{
                return redirect()->back()->with('danger','Invalid Details');
            }
        }
        else{
            $stripe_secret = env('STRIPE_SECRET');
            Stripe\Stripe::setApiKey($stripe_secret);

            $charge =   Stripe\Charge::create ([
                "amount" => $package->price * 100,
                "currency" => "hkd",
                "source" => $request->stripeToken,
                // "description" => "Job Payment"
                "description" => 1
            ]);
            if ($charge->status == "succeeded"){

                $today = Carbon::now();
                $expire_date = $today->addDays($package->post_duration);

                $transaction = new Transaction();
                $transaction->user_id = Auth::id();
                $transaction->plan_id = $package->id;
                $transaction->payment_id = $charge->id;
                $transaction->amount = $package->price;
                $transaction->currency = $charge->currency;
                $transaction->plan_type = 'candidate';
                $transaction->expires_at = $expire_date;

                $transaction->save();

                if ($package->employer_contact == 0){
                    Auth::user()->update([
                        'is_contact' => 0
                    ]);
                }
                 return redirect()->route('bio-data')->with('success','Payment Done Successfully');
            }else{
                return redirect()->back()->with('danger','Invalid Details');
            }
        }
    }
}
