<?php

namespace App\Http\Controllers;

use App\Models\AgencyStrength;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AgencyStrengthController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AgencyStrength::orderBy('id', 'DESC')->get();

            return Datatables::of($data)

                ->addColumn('action', function($data) {
                    $editUrl = route('agency_strength_edit', $data->id);
                    $deleteUrl = route('agency_strength_delete', $data->id);


                    $actionHtml = '<a href="'.$editUrl.'" class="edit btn btn-primary  btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-edit"></em>
                                       </span>
                                    </a>

                                    <a href="'.$deleteUrl.'" class="edit btn btn-danger btn-sm">
                                       <span class="nk-menu-icon text-white">
                                           <em class="icon ni ni-trash-empty"></em>
                                       </span>
                                    </a>';
                    return $actionHtml;
                })
                ->rawColumns(['status','action'])
                ->make(true);

        }
        return view('admin.pages.agency_strength.index');
    }
    public function create()
    {
        return view('admin.pages.agency_strength.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:agency_strengths,name',
        ]);
        AgencyStrength::create([
            'name' => $request->name,
        ]);
        return redirect()->route('agency_strength')->with('success','Agency Strength Added Successfully');
    }
    public function edit(AgencyStrength $strength)
    {
        return view('admin.pages.agency_strength.edit',compact('strength'));
    }
    public function update(Request $request, AgencyStrength $strength)
    {
        $request->validate([
            'name' => 'required|unique:agency_strengths,name,'.$strength->id

        ]);
        $strength->update([
            'name' => $request->name,
        ]);
        return redirect()->route('agency_strength')->with('success','Agency Strength Updated Successfully');
    }
    public function delete(AgencyStrength $strength)
    {
        AgencyStrength::destroy($strength->id);
        return redirect()->route('agency_strength')->with('success','Agency Strength Deleted Successfully');

    }
}
