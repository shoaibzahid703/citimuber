<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use App\Models\AgencyPlan;
use App\Models\BioData;
use App\Models\City;
use App\Models\CookingSkill;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Duty;
use App\Models\Education;
use App\Models\EducationLevel;
use App\Models\JobPosition;
use App\Models\Language;
use App\Models\MainSkill;
use App\Models\OtherSkill;
use App\Models\Personality;
use App\Models\Religion;
use App\Models\State;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserEducation;
use App\Models\UserWorkExperience;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HelperController extends Controller
{
    use imageUploadTrait;

    public function index()
    {
        $candidates = User::where('role',User::ROLE_CANDIDATE)
            ->where('added_by',Auth::user()->id)
            ->get();
        return view('agency.pages.helper.all_helpers',compact('candidates'));
    }

    public function edit_helper(User  $user)
    {
        $state = State::where('status',State::ACTIVE)->get();
        $language = Language::all();
        $country = Country::where('status',Country::ACTIVE)->get();
        $city = City::where('status',City::ACTIVE)->get();
        $main_skills = MainSkill::where('status',MainSkill::ACTIVE)->get();
        $cookingSkills = CookingSkill::where('status',CookingSkill::ACTIVE)->get();
        $otherSkills = OtherSkill::where('status',OtherSkill::ACTIVE)->get();
        $personality = Personality::where('status',Personality::ACTIVE)->get();
        $duties = Duty::where('status',Duty::ENABLE)->get();
        $currency = Currency::where('status',Currency::ENABLE)->get();
        $nationalities = Country::whereNotNull('nationality')->where('status',Country::ACTIVE)->get();

        $job_positions = JobPosition::where('status', 1)->get();
        $educations = Education::where('status', 1)->get();

        $religions = Religion::where('status', 1)->get();
        $education_levels = EducationLevel::where('status', 1)->get();

        $user->load('resume_detail','working_experience','education');
        $count_experience = $user->working_experience->count();
        $count_education = $user->education->count();
        $next_experience_iteration = 0;
        $next_education_iteration = 0;
        if ($user->working_experience && $count_experience > 0){
            $next_experience_iteration = $count_experience + 1;
            if ($next_experience_iteration != 1){
                $next_experience_iteration = $next_experience_iteration - 1;
            }
        }
        if ($user->education && $count_education > 0){
            $next_education_iteration = $count_education + 1;
            if ($next_education_iteration != 1){
                $next_education_iteration = $next_education_iteration - 1;
            }
        }

        return view('agency.pages.helper.edit', compact('nationalities','user',
            'next_experience_iteration','next_education_iteration','state','country',
            'language', 'city', 'main_skills', 'cookingSkills', 'personality',
            'otherSkills','duties', 'currency', 'job_positions', 'educations',
            'religions', 'education_levels'));
    }


    public function create()
    {
        $state = State::where('status',State::ACTIVE)->get();
        $language = Language::all();
        $country = Country::where('status',Country::ACTIVE)->get();
        $city = City::where('status',City::ACTIVE)->get();
        $main_skills = MainSkill::where('status',MainSkill::ACTIVE)->get();
        $cookingSkills = CookingSkill::where('status',CookingSkill::ACTIVE)->get();
        $otherSkills = OtherSkill::where('status',OtherSkill::ACTIVE)->get();
        $personality = Personality::where('status',Personality::ACTIVE)->get();
        $duties = Duty::where('status',Duty::ENABLE)->get();
        $currency = Currency::where('status',Currency::ENABLE)->get();
        $nationalities = Country::whereNotNull('nationality')->where('status',Country::ACTIVE)->get();

        $job_positions = JobPosition::where('status', 1)->get();
        $educations = Education::where('status', 1)->get();

        $religions = Religion::where('status', 1)->get();
        $education_levels = EducationLevel::where('status', 1)->get();



        $transaction = Transaction::with('agency_plan')
            ->where('user_id',Auth::id())
            ->where('plan_type',Transaction::PLAN_TYPE_AGENCY)
            ->whereNotNull('expires_at')
            ->where('expires_at', '>', now())
            ->latest()
            ->first();

        if ($transaction){
            $add_helpers = User::where('added_by',Auth::user()->id)->count();
            $package = AgencyPlan::where('id',$transaction->plan_id)->first();

            if ($add_helpers > $package->no_of_candidates){
                return redirect()->route('agency_packages')->with('warning',"Your helper limit complete now so please update package for further add helper");
            }else{
                return view('agency.pages.helper.add', compact('nationalities',
                    'state','country',
                    'language', 'city', 'main_skills', 'cookingSkills', 'personality',
                    'otherSkills','duties', 'currency', 'job_positions', 'educations',
                    'religions', 'education_levels'));
            }
        }else{
            return redirect()->route('agency_packages');
        }
    }

    public function validate_add_helper_step_one(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|max:100',
            'middle_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'age' => 'required|integer|max:100',
            'gender' => 'required',
            'marital_status' => 'required',
            'kids_detail' => 'required',
            'nationality' => 'required',
            'present_country' => 'required',
            'religion' => 'required',
            'education_level' => 'required',
            'mobile_number' => 'required',
            'whatsapp_number' => 'required',
            'valid_password' => 'required',
        ];
        $validator = Validator::make( $request->all(), $rules);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }
    public function validate_add_helper_step_two(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'position_apply' => 'required|string|max:100',
            'work_experience' => 'required|string|max:100',
            'job_type' => 'required|string|max:100',
            'work_status' => 'required|string|max:100',
            'job_start_date' => 'required|string|max:100',
            'preferred_job_location' => 'required',
            'monthly_salary' => 'required',
            'currency' => 'required',
            'day_off_preference' => 'required',
            'accomodation_preference' => 'required',
            'languages' => 'required',
            'main_skills' => 'required',
            'cooking_skills' => 'required',
            'other_skills' => 'required',
            'personalities' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }
    public function validate_add_helper_step_three(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'job_position' => 'required',
            'working_country' => 'required',
            'start_date' => 'required|string|max:100',
            'job_end_date' => 'required|string|max:100',
            'employer_type' => 'required',
            'family_type' => 'required',
            'employer_nationality' => 'required',
            'job_duties' => 'required',
            'reference_letter' => 'required',
            'education' => 'required',
            'course_duration' => 'required',
            'completed_this_course' => 'required',
            'year_completion' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }
    public function validate_add_helper_step_four(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'resume_description' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function save(Request $request)
    {

        $user =  User::create([
            'name' => $request->first_name.' '.$request->middle_name.' '.$request->last_name,
            'email' => $request->email,
            'password' => Hash::make('12345678'),
            'role' => User::ROLE_CANDIDATE,
            'visibility_status' => User::UN_VISIBLE,
            'added_by' => Auth::user()->id,
            'visibility_status' => User::VISIBLE
        ]);

        if($request->file('profile_image')){
            $profile = self::uploadFile($request, fileName: 'profile_image', folderName: 'profile_image');
            $user->update([
                'profile_image' =>  $profile,
            ]);
        }
        if ($request->mobile_number){
            $user->update([
                'mobile' =>  $request->formatted_mobile
            ]);
        }


        BioData::create([
            'user_id' =>            $user->id,
            'first_name' =>         $request->first_name,
            'middle_name'=>         $request->middle_name,
            'last_name' =>          $request->last_name,
            'age' =>                $request->age,
            'gender' =>             $request->gender,
            'marital_status' =>     $request->marital_status,
            'kids_detail' =>        $request->kids_detail,
            'nationality' =>        $request->nationality,
            'present_country' =>    $request->present_country,
            'religion' =>           $request->religion,
            'education_level' =>    $request->education_level,
            'whatsapp_number' =>    $request->formatted_whatsapp_number,
            'valid_password' =>     $request->valid_password,
            'position_apply' =>     $request->position_apply,
            'work_experience' =>    $request->work_experience,
            'job_type' =>           $request->job_type,

            'work_status' =>        $request->work_status,
            'job_start_date' =>     $request->job_start_date,

            'job_start_date' =>     Carbon::parse($request->job_start_date)->toDate() ,

            'preferred_job_location' =>    implode(',', $request['preferred_job_location']),
            'monthly_salary' =>      $request->monthly_salary,
            'currency' =>            $request->currency,
            'day_off_preference' =>  $request->day_off_preference,
            'accomodation_preference' =>    $request->accomodation_preference,
            'languages' =>    implode(',', $request['languages']),
            'main_skills' =>    implode(',', $request['main_skills']),
            'cooking_skills' =>    implode(',', $request['cooking_skills']),
            'other_skills' =>    implode(',', $request['other_skills']),
            'personalities' =>    implode(',', $request['personalities']),
            'resume_description' =>    $request->resume_description,
            'newsletter' =>    $request->newsletter,
            'opportunities' =>    $request->opportunities,
        ]);

        if (isset($request['job_position'])){
            foreach ($request['job_position'] as $key => $val){
                UserWorkExperience::create([
                    'user_id' =>      $user->id,
                    'job_position' =>    $val,
                    'working_country' =>   isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                    'start_date' =>    isset($request['start_date'][$key]) ? Carbon::parse($request['start_date'][$key])->toDate() :null,
                    'job_end_date' =>    isset($request['job_end_date'][$key]) ? Carbon::parse($request['job_end_date'][$key])->toDate() : null,
                    'employer_type' =>    isset($request['employer_type'][$key]) ? $request['employer_type'][$key] : null,
                    'family_type' =>    isset($request['family_type'][$key]) ? $request['family_type'][$key] : null,
                    'employer_nationality' =>    isset($request['employer_nationality'][$key]) ? $request['employer_nationality'][$key] : null,
                    'job_duties' =>   isset($request['job_duties'][$key]) ? implode(',',$request['job_duties'][$key])  :null,
                    'reference_letter' =>    isset($request['reference_letter'][$key]) ? $request['reference_letter'][$key]: null,
                ]);
            }
        }

        if (isset($request['education'])){
            foreach ($request['education'] as $key => $val){
                UserEducation::create([
                    'user_id' =>      $user->id,
                    'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                    'course_duration' =>    isset($request['course_duration'][$key]) ? $request['course_duration'][$key] : null,
                    'completed_this_course' =>   isset($request['completed_this_course'][$key]) ? $request['completed_this_course'][$key] : null,
                    'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                ]);
            }
        }


        return redirect()->route('agency_dashboard')->with('success', 'Helper Added Successfully!');
    }

    public function update_helper(Request $request,User  $user)
    {
        if($request->profile_image){
            $profile = self::uploadFile($request, fileName: 'profile_image', folderName: 'profile_image');
            $user->update([
                'profile_image' =>  $profile,
            ]);
        }
        if ($request->mobile_number){
            $user->update([
                'mobile' =>  $request->formatted_mobile
            ]);
        }

        $user->load('resume_detail','working_experience','education');

        if ($user->resume_detail){
            $user->resume_detail->update([
                'first_name' =>         $request->first_name,
                'middle_name'=>         $request->middle_name,
                'last_name' =>          $request->last_name,
                'age' =>                $request->age,
                'gender' =>             $request->gender,
                'marital_status' =>     $request->marital_status,
                'kids_detail' =>        $request->kids_detail,
                'nationality' =>        $request->nationality,
                'present_country' =>    $request->present_country,
                'religion' =>           $request->religion,
                'education_level' =>    $request->education_level,
                'whatsapp_number' =>    $request->formatted_whatsapp_number,
                'valid_password' =>     $request->valid_password,
                'position_apply' =>     $request->position_apply,
                'work_experience' =>    $request->work_experience,
                'job_type' =>           $request->job_type,

                'work_status' =>        $request->work_status,
                'job_start_date' =>     $request->job_start_date,

                'job_start_date' =>     Carbon::parse($request->job_start_date)->toDate() ,

                'preferred_job_location' =>    implode(',', $request['preferred_job_location']),
                'monthly_salary' =>      $request->monthly_salary,
                'currency' =>            $request->currency,
                'day_off_preference' =>  $request->day_off_preference,
                'accomodation_preference' =>    $request->accomodation_preference,
                'languages' =>    implode(',', $request['languages']),
                'main_skills' =>    implode(',', $request['main_skills']),
                'cooking_skills' =>    implode(',', $request['cooking_skills']),
                'other_skills' =>    implode(',', $request['other_skills']),
                'personalities' =>    implode(',', $request['personalities']),
                'resume_description' =>    $request->resume_description,
                'newsletter' =>    $request->newsletter,
                'opportunities' =>    $request->opportunities,
            ]);
        }else{
            BioData::create([
                'user_id' =>      \Auth::user()->id,
                'first_name' =>         $request->first_name,
                'middle_name'=>         $request->middle_name,
                'last_name' =>          $request->last_name,
                'age' =>                $request->age,
                'gender' =>             $request->gender,
                'marital_status' =>     $request->marital_status,
                'kids_detail' =>        $request->kids_detail,
                'nationality' =>        $request->nationality,
                'present_country' =>    $request->present_country,
                'religion' =>           $request->religion,
                'education_level' =>    $request->education_level,
                'whatsapp_number' =>    $request->formatted_whatsapp_number,
                'valid_password' =>     $request->valid_password,
                'position_apply' =>     $request->position_apply,
                'work_experience' =>    $request->work_experience,
                'job_type' =>           $request->job_type,

                'work_status' =>        $request->work_status,
                'job_start_date' =>     $request->job_start_date,

                'job_start_date' =>     Carbon::parse($request->job_start_date)->toDate() ,

                'preferred_job_location' =>    implode(',', $request['preferred_job_location']),
                'monthly_salary' =>      $request->monthly_salary,
                'currency' =>            $request->currency,
                'day_off_preference' =>  $request->day_off_preference,
                'accomodation_preference' =>    $request->accomodation_preference,
                'languages' =>    implode(',', $request['languages']),
                'main_skills' =>    implode(',', $request['main_skills']),
                'cooking_skills' =>    implode(',', $request['cooking_skills']),
                'other_skills' =>    implode(',', $request['other_skills']),
                'personalities' =>    implode(',', $request['personalities']),
                'resume_description' =>    $request->resume_description,
                'newsletter' =>    $request->newsletter,
                'opportunities' =>    $request->opportunities,
            ]);
        }

        if ($user->working_experience){
            if (isset($request['job_position'])){
                foreach ($request['job_position'] as $key => $val){
                    $exp_data = [
                        'job_position' => $val,
                        'working_country' => isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' => isset($request['start_date'][$key]) ? Carbon::parse($request['start_date'][$key])->toDate() : null,
                        'job_end_date' => isset($request['job_end_date'][$key]) ? Carbon::parse($request['job_end_date'][$key])->toDate() : null,
                        'employer_type' => isset($request['employer_type'][$key]) ? $request['employer_type'][$key] : null,
                        'family_type' =>    isset($request['family_type'][$key]) ? $request['family_type'][$key] : null,
                        'employer_nationality' =>    isset($request['employer_nationality'][$key]) ? $request['employer_nationality'][$key] : null,
                        'job_duties' => isset($request['job_duties'][$key]) ? implode(',', $request['job_duties'][$key]) : null,
                        'reference_letter' => isset($request['reference_letter'][$key]) ? $request['reference_letter'][$key] : null,
                    ];
                    UserWorkExperience::updateOrCreate([
                        'user_id' => \Auth::user()->id,
                        'job_position' => $val,
                        'working_country' => isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' => $request['start_date'][$key],
                        'job_end_date' => $request['job_end_date'][$key],
                    ], $exp_data);
                }
            }
        }else{
            if (isset($request['job_position'])){
                foreach ($request['job_position'] as $key => $val){
                    UserWorkExperience::create([
                        'user_id' =>      \Auth::user()->id,
                        'job_position' =>    $val,
                        'working_country' =>   isset($request['working_country'][$key]) ? $request['working_country'][$key] : null,
                        'start_date' =>    isset($request['start_date'][$key]) ? Carbon::parse($request['start_date'][$key])->toDate() :null,
                        'job_end_date' =>    isset($request['job_end_date'][$key]) ? Carbon::parse($request['job_end_date'][$key])->toDate() : null,
                        'employer_type' =>    isset($request['employer_type'][$key]) ? $request['employer_type'][$key] : null,
                        'family_type' =>    isset($request['family_type'][$key]) ? $request['family_type'][$key] : null,
                        'employer_nationality' =>    isset($request['employer_nationality'][$key]) ? $request['employer_nationality'][$key] : null,
                        'job_duties' =>   isset($request['job_duties'][$key]) ? implode(',',$request['job_duties'][$key])  :null,
                        'reference_letter' =>    isset($request['reference_letter'][$key]) ? $request['reference_letter'][$key]: null,
                    ]);
                }
            }
        }

        if ($user->education){
            if (isset($request['education'])){
                foreach ($request['education'] as $key => $val){
                    $edu_data = [
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'course_duration' =>    isset($request['course_duration'][$key]) ? $request['course_duration'][$key] : null,
                        'completed_this_course' =>   isset($request['completed_this_course'][$key]) ? $request['completed_this_course'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ];

                    UserEducation::updateOrCreate([
                        'user_id' =>      \Auth::user()->id,
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ], $edu_data);
                }
            }
        }else{
            if (isset($request['education'])){
                foreach ($request['education'] as $key => $val){
                    UserEducation::create([
                        'user_id' =>      \Auth::user()->id,
                        'education' =>    isset($request['education'][$key]) ? $request['education'][$key] : null,
                        'course_duration' =>    isset($request['course_duration'][$key]) ? $request['course_duration'][$key] : null,
                        'completed_this_course' =>   isset($request['completed_this_course'][$key]) ? $request['completed_this_course'][$key] : null,
                        'year_completion' =>    isset($request['year_completion'][$key]) ? $request['year_completion'][$key] : null,
                    ]);
                }
            }
        }

        return redirect()->route('all_helpers')->with('success', 'Helper Updated Successfully!');
    }

    public function agency_delete_experience(UserWorkExperience $experience)
    {
        $experience->delete();

        return redirect()->back()->with('success', 'Experience Deleted Successfully!');
    }

    public function agency_delete_education(UserEducation $education)
    {
        $education->delete();
        return redirect()->back()->with('success', 'Education Deleted Successfully!');
    }
}
