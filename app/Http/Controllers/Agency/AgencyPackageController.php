<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use App\Models\AgencyPlan;
use App\Models\Country;
use App\Models\HelperPlan;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Stripe;

class AgencyPackageController extends Controller
{
    public function agency_packages()
    {
        $transaction = Transaction::where('user_id',Auth::id())
            ->where('plan_type',Transaction::PLAN_TYPE_AGENCY)
            ->whereNotNull('expires_at')
            ->first();


        $plans = AgencyPlan::all();

        if ($transaction){
            $plans = AgencyPlan::where('price','!=',0.0)->get();
        }


        return view('agency.pages.packages',compact('plans'));
    }

    public function pay_agency_package(AgencyPlan $package)
    {
        return view('agency.pages.stripe_pay_card',compact('package'));
    }

    public function pay_agency_package_payment(Request $request,AgencyPlan $package)
    {

        $stripe_secret = env('STRIPE_SECRET');
        Stripe\Stripe::setApiKey($stripe_secret);

        $charge =   Stripe\Charge::create ([
            "amount" => $package->price * 100,
            "currency" => "hkd",
            "source" => $request->stripeToken,
            "description" => 1
        ]);
        if ($charge->status == "succeeded"){

            $today = Carbon::now();
            $expire_date = $today->addDays($package->no_of_days);

            $transaction = new Transaction();
            $transaction->user_id = Auth::id();
            $transaction->plan_id = $package->id;
            $transaction->payment_id = $charge->id;
            $transaction->amount = $package->price;
            $transaction->currency = $charge->currency;
            $transaction->plan_type = 'agency';
            $transaction->expires_at = $expire_date;

            $transaction->save();

            return redirect()->route('agency_dashboard')->with('success','Payment Done Successfully');

        }
    }
}
