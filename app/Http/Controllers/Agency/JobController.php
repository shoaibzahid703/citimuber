<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use App\Models\AgencyPlan;
use App\Models\City;
use App\Models\CookingSkill;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Duty;
use App\Models\EducationLevel;
use App\Models\Job;
use App\Models\JobPicture;
use App\Models\Language;
use App\Models\MainSkill;
use App\Models\OtherSkill;
use App\Models\Personality;
use App\Models\Plans;
use App\Models\State;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class JobController extends Controller
{
    public function create()
    {
        $state = State::where('status',State::ACTIVE)->get();
        $language = Language::all();
        $country = Country::where('status',Country::ACTIVE)->get();
        $city = City::where('status',City::ACTIVE)->get();
        $main_skills = MainSkill::where('status',MainSkill::ACTIVE)->get();
        $cookingSkills = CookingSkill::where('status',CookingSkill::ACTIVE)->get();
        $otherSkills = OtherSkill::where('status',OtherSkill::ACTIVE)->get();
        $personality = Personality::where('status',Personality::ACTIVE)->get();
        $duties = Duty::where('status',Duty::ENABLE)->get();
        $currency = Currency::where('status',Currency::ENABLE)->get();
        $nationalities = Country::whereNotNull('nationality')->where('status',Country::ACTIVE)->get();
        $job_pictures = JobPicture::all();
        $education_levels = EducationLevel::where('status', 1)->get();

        $transaction = Transaction::with('agency_plan')
            ->where('user_id',Auth::id())
            ->where('plan_type',Transaction::PLAN_TYPE_AGENCY)
            ->whereNotNull('expires_at')
            ->where('expires_at', '>', now())
            ->latest()
            ->first();

        if ($transaction){
            $add_jobs = Job::where('user_id',Auth::user()->id)->count();
            $package = AgencyPlan::where('id',$transaction->plan_id)->first();
            if ($add_jobs > $package->no_of_jobs){
                return redirect()->route('agency_packages')->with('warning',"Your Job limit complete now so please update package for further add jobs");
            }else{
                return view('agency.pages.job.create',compact('nationalities',
                    'state','country', 'language', 'city', 'main_skills', 'cookingSkills',
                    'personality', 'otherSkills','duties', 'currency','job_pictures', 'education_levels'));
            }
        }else{
            return redirect()->route('agency_packages');
        }



    }
    public function store(Request $request)
    {

        $data = $request->all();

        // Create a new Job instance with the request data
         Job::create([
            'user_id'  => \Auth::user()->id,
            'position_offered' => $data['position_offered'] ?? null,
            'job_type' => $data['job_type'] ?? null,
            'offer_location' => $data['offer_location'] ?? null,
            'state' => $data['state'] ?? null,
            'city' => $data['city'] ?? null,
            'job_start_date' => $data['job_start_date'] ?? null,
            'start_date_flexibility' => $data['start_date_flexibility'] ?? null,
            'languages' => implode(',',$data['languages']) ?? null,
            'main_skills' => implode($data['main_skills']) ?? null,
            'cooking_skills' => implode(',',$data['cooking_skills']) ?? null,
            'other_skills' => implode(',',$data['other_skills']) ?? null,
            'prefer_candidate_location' => $data['prefer_candidate_location'] ?? null,
            'contract_status' => $data['contract_status'] ?? null,
            'gender' => $data['gender'] ?? null,
            'preferred_candidate_nationality' => implode(',',$data['preferred_candidate_nationality'])?? null,
            'education_level' => $data['education_level'] ?? null,
            'preferred_age' => $data['preferred_age'] ?? null,
            'preferred_experience' => $data['preferred_experience'] ?? null,
            'employer_type' => $data['employer_type'] ?? null,
            'receive_email' => $data['receive_email'] ?? null,
            'notify_email' => $data['notify_email'] ?? null,
            'family_type' => $data['family_type'] ?? null,
            'pets' => $data['pets'] ?? null,
            'employer_nationality' => $data['employer_nationality'] ?? null,
            'day_off' => $data['day_off'] ?? null,
            'accomodation' => $data['accomodation'] ?? null,
            'salary_offer' => $data['salary_offer'] ?? null,
            'range_min_salary' => $data['range_min_salary'] ?? null,
            'range_max_salary' => $data['range_max_salary'] ?? null,
            'range_currency' => $data['range_currency'] ?? null,
            'fix_monthly_salary' => $data['fix_monthly_salary'] ?? null,
            'fix_currency' => $data['fix_currency'] ?? null,
            'salary_description' => $data['salary_description'] ?? null,
            'job_picture' => $data['job_picture'] ?? null,
            'job_title' => $data['job_title'] ?? null,
            'job_description' => $data['job_description'] ?? null,
            'newsletter' => $data['newsletter'] ?? null,
            'opportunities' => $data['opportunities'] ?? null,
             'status' => Job::PUBLISH,
             'add_type' => 'agency'
        ]);

        return  redirect()->route('agency_dashboard')->with('success','Job successfully saved.');

    }
    public function validateJobStepOne(Request $request)
    {

        $rules = [
            'position_offered' => 'required',
            'job_type' => 'required',
            'offer_location' => 'required',
            'job_start_date' => 'required',
            'start_date_flexibility' => 'required',
            'languages' => 'required',
            'main_skills' => 'required',
            'cooking_skills' => 'required',
            'other_skills' => 'required',
            'prefer_candidate_location' => 'required',
            'contract_status' => 'required',
            'gender' => 'required',
            'preferred_candidate_nationality' => 'required',
            'education_level' => 'required',

        ];

        $offerLocation = $request->input('offer_location');
        $stateId = $request->input('state');

        $rules['state'] = 'nullable';
        $rules['city'] = 'nullable';

        if ($offerLocation) {

            $country = Country::find($offerLocation);

            if ($country && $country->states()->count() > 0) {
                $rules['state'] = 'required';

                if ($stateId) {
                    $state = State::find($stateId);

                    if ($state && $state->cities()->count() > 0) {
                        $rules['city'] = 'required';
                    }
                }
            }
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ]);
        } else {
            return response()->json([
                'error'    => false,
            ]);
        }
    }
    public function validateJobStepTwo(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'employer_type' => 'required',
            'day_off' => 'required',
            'accomodation' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }
    public function validateJobStepThree(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'job_title' => 'required',
        ]);
        if ( $validator->fails() ) {
            return response()->json( [
                'error'    => true,
                'messages' => $validator->errors(),
            ] );
        }else{
            return response()->json( [
                'error'    => false,
            ] );
        }
    }

    public function manage_jobs(){
        return view('agency.pages.job.manage_jobs');
    }
    public function fetchJobs(Request $request){

        $jobTitle = $request->input('job_title', '');

        $offer_location = $request->input('offer_location', '');

        $position_offered = $request->input('position_offered', '');
        $job_start_date = $request->input('job_start_date', '');
        $status = $request->input('status', '');

        $query = Job::query()->where('user_id', \Auth::user()->id)->with('countryName','job_image');

        if (!empty($jobTitle)) {
            $query->where('job_title', 'LIKE', '%' . $jobTitle . '%');
        }

        if (!empty($offer_location)) {
            $query->where('offer_location', 'LIKE', '%' . $offer_location . '%');
        }

        if (!empty($position_offered)) {
            $query->where('position_offered', 'LIKE', '%' . $position_offered . '%');
        }

        if (!empty($job_start_date)) {
            $query->where('job_start_date', 'LIKE', '%' . $job_start_date . '%');
        }

        if (!empty($status)) {
            $query->where('status', 'LIKE', '%' . $status . '%');
        }


        $jobs = $query->paginate(10);

        return response()->json([
            'jobs' => $jobs,
            'pagination' => (string) $jobs->links()
        ]);
    }

    public function delete(Job $job)
    {;
        Job::destroy($job->id);
        return redirect()->route('agency_manage_jobs')->with('success','Job Deleted Successfully');
    }

    public function edit(Job $job){

        $state = State::where('status',State::ACTIVE)->get();
        $language = Language::all();
        $country = Country::where('status',Country::ACTIVE)->get();
        $city = City::where('status',City::ACTIVE)->get();
        $main_skills = MainSkill::where('status',MainSkill::ACTIVE)->get();
        $cookingSkills = CookingSkill::where('status',CookingSkill::ACTIVE)->get();
        $otherSkills = OtherSkill::where('status',OtherSkill::ACTIVE)->get();
        $personality = Personality::where('status',Personality::ACTIVE)->get();
        $duties =Duty::where('status',Duty::ENABLE)->get();
        $currency = Currency::where('status',Currency::ENABLE)->get();
        $plans= Plans::all();
        $job_pictures = JobPicture::all();
        $nationalities = Country::whereNotNull('nationality')->where('status',Country::ACTIVE)->get();
        $education_levels = EducationLevel::where('status', 1)->get();
        return view('agency.pages.job.edit', compact('nationalities','job',
            'state','country', 'language', 'city', 'main_skills', 'cookingSkills', 'personality',
            'otherSkills','duties', 'currency','plans','job_pictures','education_levels'));
    }

    public function update(Request $request, Job $job)
    {
        $data = $request->all();

        $job->update([
            'position_offered' => $data['position_offered'] ?? null,
            'job_type' => $data['job_type'] ?? null,
            'offer_location' => $data['offer_location'] ?? null,
            'state' => $data['state'] ?? null,
            'city' => $data['city'] ?? null,
            'job_start_date' => $data['job_start_date'] ?? null,
            'start_date_flexibility' => $data['start_date_flexibility'] ?? null,
            'languages' => implode(',',$data['languages']) ?? null,
            'main_skills' => implode($data['main_skills']) ?? null,
            'cooking_skills' => implode(',',$data['cooking_skills']) ?? null,
            'other_skills' => implode(',',$data['other_skills']) ?? null,
            'prefer_candidate_location' => $data['prefer_candidate_location'] ?? null,
            'contract_status' => $data['contract_status'] ?? null,
            'gender' => $data['gender'] ?? null,
            'preferred_candidate_nationality' => implode(',',$data['preferred_candidate_nationality']) ?? null,
            'education_level' => $data['education_level'] ?? null,
            'preferred_age' => $data['preferred_age'] ?? null,
            'preferred_experience' => $data['preferred_experience'] ?? null,
            'employer_type' => $data['employer_type'] ?? null,
            'receive_email' => $data['receive_email'] ?? null,
            'notify_email' => $data['notify_email'] ?? null,
            'family_type' => $data['family_type'] ?? null,
            'pets' => $data['pets'] ?? null,
            'employer_nationality' => $data['employer_nationality'] ?? null,
            'day_off' => $data['day_off'] ?? null,
            'accomodation' => $data['accomodation'] ?? null,
            'salary_offer' => $data['salary_offer'] ?? null,
            'range_min_salary' => $data['range_min_salary'] ?? null,
            'range_max_salary' => $data['range_max_salary'] ?? null,
            'range_currency' => $data['range_currency'] ?? null,
            'fix_monthly_salary' => $data['fix_monthly_salary'] ?? null,
            'fix_currency' => $data['fix_currency'] ?? null,
            'salary_description' => $data['salary_description'] ?? null,
            'job_picture' => $data['job_picture'] ?? null,
            'job_title' => $data['job_title'] ?? null,
            'job_description' => $data['job_description'] ?? null,
            'newsletter' => $data['newsletter'] ?? null,
            'opportunities' => $data['opportunities'] ?? null,
        ]);
        return  redirect()->route('agency_manage_jobs')->with('success','Job successfully updated.');

    }
}
