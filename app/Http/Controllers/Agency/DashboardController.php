<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Models\JobApply;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $transaction = Transaction::with('agency_plan')
            ->where('user_id',Auth::id())
            ->where('plan_type',Transaction::PLAN_TYPE_AGENCY)
            ->whereNotNull('expires_at')
            ->where('expires_at', '>', now())
            ->latest()
            ->first();


        return view('agency.pages.dashboard',compact('transaction'));
    }
}
