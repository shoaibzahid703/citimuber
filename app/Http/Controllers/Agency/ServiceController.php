<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use App\Models\AgencyService;
use App\Models\Plans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $services = AgencyService::where('user_id',Auth::user()->id)
            ->orderBy('id','desc')
            ->get();
        return view('agency.pages.service.index',compact('services'));
    }
    public function create()
    {
        return view('agency.pages.service.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:agency_services,name',
            'no_of_weeks'=> 'required',
            'price'=> 'sometimes',
        ]);
        AgencyService::create([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'no_of_weeks' => $request->no_of_weeks,
            'price' => $request->price ?? null,
            'description' =>  $request->description,
        ]);

        return redirect()->route('services')->with('success','Service Added Successfully');
    }
    public function edit(AgencyService $service)
    {
        return view('agency.pages.service.edit',compact('service'));
    }
    public function update(Request $request,AgencyService $service)
    {
        $request->validate([
            'name' => 'required|unique:agency_services,name,' . $service->id,
            'no_of_weeks'=> 'required',
            'price'=> 'sometimes',
        ]);
        $service->update([
            'name' => $request->name,
            'no_of_weeks' => $request->no_of_weeks,
            'price' => $request->price ?? null,
            'description' =>  $request->description,
        ]);

        return redirect()->route('services')->with('success', 'Service Updated Successfully');
    }
    public function delete(AgencyService $service)
    {
        AgencyService::destroy($service->id);
        return redirect()->route('services')->with('success','Service Deleted Successfully');

    }
}
