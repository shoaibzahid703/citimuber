<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use App\Models\AgencyExperts;
use App\Models\AgencyInfo;
use App\Models\AgencyStrength;
use App\Models\Country;
use App\Models\Language;
use App\Models\AgencyService;
use App\Traits\imageUploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use function Symfony\Component\String\s;

class ProfileController extends Controller
{
    use imageUploadTrait;
    public function index()
    {
        $country = Country::where('status',Country::ACTIVE)->get();
        $language = Language::all();
        return view('agency.pages.profile', compact('country', 'language'));
    }

    public function update(Request $request){

        $user = Auth::user();
        $data = [
            'name' =>     $request->name,
            'company_name' =>     $request->company_name,
            'email' =>    $request->email,
            'mobile' =>   $request->mobile,
            'passport' => $request->passport,
            'language' => $request->language,
            'location' => $request->location,
        ];


        $data['mobile'] = $request->formatted_mobile;

        $user->update($data);
        return redirect()->route('agency_profile',['password_tab'=> false])->with('success','profile updated successfully');
    }


    public function agency_update_password(Request $request){

        $rules = [
            'current_password' => 'required',
            'password' => 'required|string|confirmed',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('candidate_profile', ['password_tab' => true])
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();
        $hashedPassword = $user->password;

        if (Hash::check($request->current_password, $hashedPassword)) {
            if (!Hash::check($request->password, $hashedPassword)) {
                $user->password = Hash::make($request->password);
                $user->save();

                return redirect()->back()->with('success','Password updated successfully');
            } else {
                return redirect()->route('agency_profile', ['password_tab' => true])
                    ->with('danger', 'New password cannot be the same as the old password.')
                    ->with('password_tab', true);
            }
        } else {
            return redirect()->route('agency_profile', ['password_tab' => true])
                ->with('danger', 'Old password does not match.')
                ->with('password_tab', true);
        }
    }

    public function update_info()
    {
        $countries = Country::where('status',Country::ACTIVE)->get();
        $languages = Language::all();
        $strengths = AgencyStrength::all();
        $experts = AgencyExperts::all();
        $info = AgencyInfo::where('user_id',Auth::user()->id)->first();
        $services = AgencyService::all();
        return view('agency.pages.info',compact('strengths','countries',
            'languages','experts','info','services'));
    }

    public function info_save(Request $request)
    {
        $request->validate([
            'strength' => 'required',
            'speak_language' => 'required',
            'help_to_hire' => 'sometimes',
            'agency_expert' => 'sometimes',
            'license_no' => 'required',
            'open_time' => 'required',
            'day_off' => 'required',
            'address' => 'required|max:200',
            'agency_detail' => 'required',
            'company_image' => 'sometimes|image',
            'company_banner_image' => 'sometimes|image',
            'company_short_intro' => 'required|max:135',
        ]);
        $info = AgencyInfo::where('user_id',Auth::user()->id)->first();

        if ($info){
            $company_image = $info->company_image ;
            $company_banner_image = $info->company_banner_image  ;
            if ($request->hasFile('company_image')) {
                $company_image = self::uploadFile($request, fileName: 'company_image', folderName: 'company_image');
            }
            if ($request->hasFile('company_banner_image')) {
                $company_banner_image = self::uploadFile($request, fileName: 'company_banner_image', folderName: 'company_banner_image');
            }
            $info->update([
                'user_id' => Auth::user()->id,
                'strength' => implode(',',$request->strength),
                'speak_language' => implode(',',$request->speak_language),
                'help_to_hire' =>  $request->help_to_hire ? implode(',',$request->help_to_hire) : null,
                'agency_expert' => $request->agency_expert ? implode(',',$request->agency_expert) : null,
                'license_no' => $request->license_no,
                'open_time' => $request->open_time,
                'day_off' => $request->day_off,
                'address' => $request->address,
                'agency_detail' => $request->agency_detail,
                'company_image' => $company_image,
                'company_banner_image' => $company_banner_image,
                'company_short_intro' => $request->company_short_intro,
                'company_map_iframe' => $request->company_map_iframe,
            ]);
            return redirect()->back()->with('success','Info updated successfully');
        }else{

            $company_image = null;
            $company_banner_image = null;
            if ($request->hasFile('company_image')) {
                $company_image = self::uploadFile($request, fileName: 'company_image', folderName: 'company_image');
            }
            if ($request->hasFile('company_banner_image')) {
                $company_banner_image = self::uploadFile($request, fileName: 'company_banner_image', folderName: 'company_banner_image');
            }
            AgencyInfo::create([
                'user_id' => Auth::user()->id,
                'strength' => implode(',',$request->strength),
                'speak_language' => implode(',',$request->speak_language),
                'help_to_hire' =>  $request->help_to_hire ? implode(',',$request->help_to_hire) : null,
                'agency_expert' => $request->agency_expert ? implode(',',$request->agency_expert) : null,
                'license_no' => $request->license_no,
                'open_time' => $request->open_time,
                'day_off' => $request->day_off,
                'address' => $request->address,
                'agency_detail' => $request->agency_detail,
                'company_image' => $company_image,
                'company_banner_image' => $company_banner_image,
                'company_short_intro' => $request->company_short_intro,
            ]);
            return redirect()->back()->with('success','Info updated successfully');
        }
    }
}
