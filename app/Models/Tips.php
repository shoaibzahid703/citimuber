<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tips extends Model
{
    use HasFactory;

    CONST ACTIVE = 1;
    CONST BLOCK = 0;

    protected $guarded  = [];
}
