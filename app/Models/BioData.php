<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BioData extends Model
{
    use HasFactory;

    protected $table = 'bio_data';

    protected $guarded = [];

    protected $appends = [
        'language_names',
        'skill_names',
        'cooking_skill_names',
        'other_skill_names',
        'personality_names',
    ];

    public function user_nationality()
    {
        return $this->belongsTo(Country::class,'nationality');
    }

    public function salary_currency()
    {
        return $this->belongsTo(Currency::class,'currency');
    }

    public function present_location()
    {
        return $this->belongsTo(Country::class,'present_country');
    }

    public function getLanguageNamesAttribute() {
        if (is_null($this->languages)){
            return null;
        }else{
            $items = Language::whereIn( 'id', explode( ',', $this->languages))->get();
            return $items->pluck( 'name' );
        }
    }

    public function getSkillNamesAttribute() {
        if (is_null($this->main_skills)){
            return null;
        }else{
            $items = MainSkill::whereIn( 'id', explode( ',', $this->main_skills))->get();
            return $items->pluck( 'name' );
        }
    }

    public function getCookingSkillNamesAttribute() {
        if (is_null($this->cooking_skills)){
            return null;
        }else{
            $items = CookingSkill::whereIn( 'id', explode( ',', $this->cooking_skills))->get();
            return $items->pluck( 'name' );
        }
    }

    public function getOtherSkillNamesAttribute() {
        if (is_null($this->other_skills)){
            return null;
        }else{
            $items = OtherSkill::whereIn( 'id', explode( ',', $this->other_skills))->get();
            return $items->pluck( 'name' );
        }
    }

    public function getPersonalityNamesAttribute() {
        if (is_null($this->personalities)){
            return null;
        }else{
            $items = Personality::whereIn( 'id', explode( ',', $this->personalities))->get();
            return $items->pluck( 'name' );
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
