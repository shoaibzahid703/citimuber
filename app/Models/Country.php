<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    CONST ACTIVE = 1;
    CONST BLOCK = 0;

    protected $guarded  = [];

    public function properties()
    {
        return $this->hasMany(Property::class,'country_id');
    }

    public function states()
{
    return $this->hasMany(State::class);
}
}
