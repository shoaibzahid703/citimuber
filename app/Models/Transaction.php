<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    Const PLAN_TYPE_EMPLOYER = 'employer';
    Const PLAN_TYPE_CANDIDATE = 'candidate';
    Const PLAN_TYPE_AGENCY = 'agency';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function job()
    {
        return $this->belongsTo(Job::class,'job_id');
    }
    public function employer_plan()
    {
        return $this->belongsTo(Plans::class,'plan_id');
    }
    public function helper_plan()
    {
        return $this->belongsTo(HelperPlan::class,'plan_id');
    }
    public function agency_plan()
    {
        return $this->belongsTo(AgencyPlan::class,'plan_id');
    }
}
