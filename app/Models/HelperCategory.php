<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HelperCategory extends Model
{
    CONST Yes = 1;
    CONST NO = 0;
    use HasFactory;

    protected $guarded = [];
}
