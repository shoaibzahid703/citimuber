<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandidateDetailPage extends Model
{
    use HasFactory;

    protected $guarded = [];
}
