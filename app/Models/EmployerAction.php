<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployerAction extends Model
{
    use HasFactory;
    protected $guarded = [];
}
