<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    const ROLE_ADMIN = 'admin';
    const ROLE_CANDIDATE = 'candidate';
    const ROLE_EMPLOYER = 'employer';
    const ROLE_AGENCY = 'agency';

    const AGENCY = 'agency';
    const TRAINING = 'training';
    const PARTNER = 'partner';
    const ASSOCIATION = 'association';

    const BLOCK = 'block';
    const ACTIVE = 'active';

    const VISIBLE = 1;
    const UN_VISIBLE = 0;
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'role',
        'name',
        'email',
        'password',
        'status',
        'passport',
        'mobile',
        'language',
        'location',
        'profile_image',
        'employer_category_id',
        'helper_category_id',
        'visibility_status',
        'is_read',
        'agency_role',
        'company_name',
        'added_by'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function jobs()
    {
        return $this->hasMany(Job::class,'user_id');
    }

    public function resume_detail()
    {
        return $this->hasOne(BioData::class,'user_id');
    }

    public function working_experience()
    {
        return $this->hasMany(UserWorkExperience::class,'user_id');
    }

    public function education()
    {
        return $this->hasMany(UserEducation::class,'user_id');
    }

    public function user_work_experience()
    {
        return $this->hasOne(UserWorkExperience::class,'user_id');
    }

    public function user_education()
    {
        return $this->hasOne(UserEducation::class,'user_id');
    }

    public function user_location()
    {
        return $this->belongsTo(Country::class,'location');
    }

    public function employer_category()
    {
        return $this->belongsTo(EmployerCategory::class,'employer_category_id');
    }

    public function helper_category()
    {
        return $this->belongsTo(HelperCategory::class,'helper_category_id');
    }

    public function info()
    {
        return $this->hasOne(AgencyInfo::class,'user_id');
    }

    public function helper_package()
    {
        return $this->hasMany(Transaction::class,'user_id')
            ->where('plan_type',Transaction::PLAN_TYPE_CANDIDATE)
            ->whereNotNull('expires_at')
            ->where('expires_at', '>', now())
            ->latest();
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id');
    }

    public function parent_user()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

}
