<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWorkExperience extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = [
        'duty_names',
    ];

    public function work_country()
    {
        return $this->belongsTo(Country::class,'working_country');
    }

    public function getDutyNamesAttribute() {
        if (is_null($this->job_duties)){
            return null;
        }else{
            $items = Duty::whereIn( 'id', explode( ',', $this->job_duties))->get();
            return $items->pluck( 'name' );
        }
    }
}
