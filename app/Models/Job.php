<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;

class Job extends Model
{
    use HasFactory;

    protected $guarded = [];


    const PUBLISH = 'publish';
    const UN_PUBLISH = 'unpublish';


    public function countryName() {
        return $this->belongsTo(Country::class, 'offer_location');
    }

    public function nationality() {
        return $this->belongsTo(Country::class, 'employer_nationality');
    }

    public function userDetail()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function jobPlan()
    {
        return $this->belongsTo(Plans::class, 'plan_id');
    }

    public function job_applies()
    {
        return $this->hasMany(JobApply::class, 'job_id');
    }

    public function job_image()
    {
        return $this->belongsTo(JobPicture::class, 'job_picture');
    }

    public function job_package()
    {
        return $this->hasMany(Transaction::class,'job_id')
            ->where('plan_type',Transaction::PLAN_TYPE_EMPLOYER)
            ->whereNotNull('expires_at')
            ->where('expires_at', '>', now())
            ->latest();
    }
}
