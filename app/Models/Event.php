<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    CONST ENABLE = 1;
    CONST DISABLE = 0;

    public function countryName() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    protected $guarded = ['id'];
}
