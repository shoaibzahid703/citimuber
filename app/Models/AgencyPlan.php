<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgencyPlan extends Model
{
    use HasFactory;

    CONST ENABLE = 1;
    CONST DISABLE = 0;

    protected $guarded = [];
}
