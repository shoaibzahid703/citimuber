<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPicture extends Model
{
    use HasFactory;

    protected $guarded = [];
}
