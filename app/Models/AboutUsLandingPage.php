<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutUsLandingPage extends Model
{
    use HasFactory;
    protected $guarded = [];
}
