<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandidateDashboardPage extends Model
{
    use HasFactory;
    protected $guarded = [];
}
