<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherSkill extends Model
{
    use HasFactory;
    protected $guarded = [];

    const BLOCK = '0';
    const ACTIVE = '1';
}
