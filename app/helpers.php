<?php

use App\Models\Admin;
use App\Models\User;
use Laravolt\Avatar\Avatar;

function get_name_first_letters($letters){
    $words = explode(" ", $letters);
    $acronym = "";
    foreach ($words as $w) {
        $acronym .= ucwords(mb_substr($w, 0, 1));
    }
    return $acronym;
}


