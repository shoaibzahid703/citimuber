<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@citimuber.com';
        $admin->password = Hash::make( '12345678' );
        $admin->role = 'admin';
        $admin->save();

        $candidate = new User();
        $candidate->name = 'Candidate';
        $candidate->email = 'candidate@citimuber.com';
        $candidate->password = Hash::make( '12345678' );
        $candidate->role = 'candidate';
        $candidate->save();

        $employer = new User();
        $employer->name = 'Candidate';
        $employer->email = 'employer@citimuber.com';
        $employer->password = Hash::make( '12345678' );
        $employer->role = 'employer';
        $employer->save();
    }
}
