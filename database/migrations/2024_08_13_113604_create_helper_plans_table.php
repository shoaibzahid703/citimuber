<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helper_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name', 200);
            $table->integer('post_duration');
            $table->boolean('employer_contact')->default(1);
            $table->double('price');
            $table->text('country')->nullable();
            $table->text('manual_price')->nullable();
            $table->text('features')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helper_plans');
    }
};
