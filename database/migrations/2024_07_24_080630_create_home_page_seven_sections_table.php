<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_seven_sections', function (Blueprint $table) {
            $table->id();
            $table->string('first_text')->nullable();
            $table->string('heading')->nullable();
            $table->text('paragraph')->nullable();
            $table->string('first_button')->nullable();
            $table->string('second_button')->nullable();
            $table->string('second_text')->nullable();
            $table->string('side_image')->nullable();
            $table->string('playstore_image')->nullable();
            $table->string('appstore_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_seven_sections');
    }
};
