<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_plans', function (Blueprint $table) {
            $table->id();
            $table->string('name', 200);
            $table->integer('no_of_days');
            $table->double('price');
            $table->integer('no_of_jobs');
            $table->double('no_of_candidates');
            $table->text('description')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_plans');
    }
};
