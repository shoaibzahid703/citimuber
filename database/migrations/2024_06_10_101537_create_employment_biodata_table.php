<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employment_biodata', function (Blueprint $table) {
            $table->id();
            $table->string('profile_image')->nullable();
            $table->date('release_date')->nullable();
            $table->date('expiry_date')->nullable();
            $table->date('apply_date')->nullable();
            $table->string('ref_number')->nullable();
            $table->string('name')->nullable();
            $table->integer('age')->nullable();
            $table->date('dob')->nullable();
            $table->string('education')->nullable();
            $table->string('nationality')->nullable();
            $table->string('id_number')->nullable();
            $table->string('religion')->nullable();
            $table->string('birthplace')->nullable();
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            // Skills and Work Preferences
            $table->boolean('baby_care')->default(false);
            $table->boolean('child_care')->default(false);
            $table->boolean('elderly_care')->default(false);
            $table->boolean('housework')->default(false);
            $table->boolean('cooking')->default(false);
            $table->boolean('washing')->default(false);
            $table->boolean('disabled_care')->default(false);
            $table->boolean('driving')->default(false);
            $table->boolean('bedridden_care')->default(false);
            $table->boolean('gardening')->default(false);
            $table->boolean('pets_care')->default(false);
            $table->boolean('special_needs')->default(false);
            // Languages
            $table->string('english_level')->nullable();
            $table->string('cantonese_level')->nullable();
            $table->string('mandarin_level')->nullable();
            // Other Questions
            $table->boolean('newborn_care')->default(false);
            $table->boolean('elderly_care_willing')->default(false);
            $table->boolean('disabled_care_willing')->default(false);
            $table->boolean('share_room')->default(false);
            $table->boolean('accept_day_off')->default(false);
            $table->integer('work_experience_years')->nullable();
            $table->integer('work_experience_months')->nullable();
            // Domestic Helper Working Experience
            $table->text('working_experience')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employment_biodata');
    }
};
