<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_second_sections', function (Blueprint $table) {
            $table->id();
            $table->string('first_image')->nullable();
            $table->string('second_image')->nullable();
            $table->string('third_image')->nullable();
            $table->string('forth_image')->nullable();
            $table->string('five_image')->nullable();
            $table->string('six_image')->nullable();
            $table->string('seven_image')->nullable();
            $table->string('first_heading')->nullable();
            $table->string('second_heading')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_second_sections');
    }
};
