<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_infos', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->string('strength');
            $table->string('speak_language');
            $table->string('help_to_hire');
            $table->string('agency_expert');
            $table->string('license_no');
            $table->string('open_time');
            $table->string('day_off');
            $table->string('address');
            $table->longText('agency_detail');
            $table->longText('company_map_iframe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_infos');
    }
};
