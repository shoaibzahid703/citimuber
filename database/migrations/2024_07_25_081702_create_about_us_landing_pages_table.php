<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us_landing_pages', function (Blueprint $table) {
            $table->id();
            $table->string('first_heading')->nullable();
            $table->text('description')->nullable();

            $table->string('second_heading')->nullable();
            $table->string('text_line')->nullable();
            $table->string('button_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us_landing_pages');
    }
};
