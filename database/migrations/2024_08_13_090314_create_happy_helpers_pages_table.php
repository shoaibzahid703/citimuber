<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('happy_helpers_pages', function (Blueprint $table) {
            $table->id();
            $table->string('first_heading')->nullable();
            $table->string('second_heading')->nullable();
            $table->string('first_text')->nullable();
            $table->text('first_paragraph')->nullable();
            $table->string('register_button')->nullable();
            $table->text('second_paragraph')->nullable();
            $table->string('third_heading')->nullable();
            $table->text('third_paragraph')->nullable();
            $table->string('fourth_heading')->nullable();
            $table->string('second_text')->nullable();
            $table->string('contact_us_button')->nullable();
            $table->string('bg_image')->nullable();
            $table->string('first_image')->nullable();
            $table->string('sec_image')->nullable();
            $table->string('app_store_image')->nullable();
            $table->string('google_play_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('happy_helpers_pages');
    }
};
