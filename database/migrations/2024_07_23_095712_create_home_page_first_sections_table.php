<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_first_sections', function (Blueprint $table) {
            $table->id();
            $table->string('first_heading')->nullable();
            $table->string('second_heading')->nullable();
            $table->string('first_button')->nullable();
            $table->string('sec_button')->nullable();
            $table->string('bg_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_first_sections');
    }
};
