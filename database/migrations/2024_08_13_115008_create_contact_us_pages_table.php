<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_us_pages', function (Blueprint $table) {
            $table->id();
            $table->string('first_heading')->nullable();
            $table->text('first_paragraph')->nullable();
            $table->string('first_text')->nullable();
            $table->string('second_text')->nullable();
            $table->string('contact_number')->nullable();

            $table->string('second_heading')->nullable();
            $table->string('third_text')->nullable();
            $table->string('contact_us_button')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_us_pages');
    }
};
