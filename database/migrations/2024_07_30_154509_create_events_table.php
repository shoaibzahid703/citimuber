<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('title')->nullable();
            $table->string('country_id')->nullable();
            $table->string('organizer_name')->nullable();
            $table->string('event_date')->nullable();
            $table->string('start_time')->nullable();

            $table->string('end_time')->nullable();
            $table->string('audience')->nullable();
            $table->string('fees')->nullable();
            $table->string('phone')->nullable();
            $table->string('requirement')->nullable();
            $table->text('location')->nullable();
            $table->text('short_decription')->nullable();
            $table->text('description')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
