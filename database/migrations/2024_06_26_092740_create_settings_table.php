<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string( 'seo_tags')->nullable();
            $table->string( 'site_description')->nullable();
            $table->string( 'site_logo')->nullable();
            $table->string( 'site_fav_icon')->nullable();
            $table->string( 'facebook_profile')->nullable();
            $table->string( 'twitter_profile')->nullable();
            $table->string( 'tiktok_profile')->nullable();
            $table->string( 'pinterest_profile')->nullable();
            $table->string( 'instagram_profile')->nullable();
            $table->string( 'youtube_profile')->nullable();
            $table->string( 'linkedin_profile')->nullable();
            $table->string( 'contact_us_email')->nullable();
            $table->string( 'contact_us_number')->nullable();
            $table->string( 'contact_us_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
};
