<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio_data', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();

            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('kids_detail')->nullable();
            $table->string('nationality')->nullable();
            $table->string('religion')->nullable();
            $table->string('education_level')->nullable();
            // Skills and Work Preferences
            $table->string('whatsapp_number')->nullable();
            $table->string('valid_password')->nullable();
            $table->string('position_apply')->nullable();
            $table->string('work_experience')->nullable();
            $table->string('job_type')->nullable();
            $table->string('present_country')->nullable();
            $table->string('work_status')->nullable();
            $table->date('job_start_date')->nullable();
            $table->text('preferred_job_location')->nullable();
            $table->string('monthly_salary')->nullable();
            $table->string('currency')->nullable();
            $table->string('day_off_preference')->nullable();
            $table->string('accomodation_preference')->nullable();

            $table->text('languages')->nullable();
            $table->text('main_skills')->nullable();
            $table->text('cooking_skills')->nullable();

            $table->text('other_skills')->nullable();
            $table->text('personalities')->nullable();

            $table->text('resume_description')->nullable();
            $table->string('newsletter')->nullable();
            $table->string('opportunities')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio_data');
    }
};
