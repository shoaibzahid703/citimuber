<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_work_experiences', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();

            $table->string('job_position')->nullable();
            $table->unsignedInteger('working_country')->nullable();
            $table->date('start_date')->nullable();
            $table->date('job_end_date')->nullable();
            $table->string('employer_type')->nullable();
            $table->string('family_type')->nullable();
            $table->unsignedInteger('employer_nationality')->nullable();
            $table->string('job_duties')->nullable();
            $table->string('reference_letter')->nullable();

            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_work_experiences');
    }
};
