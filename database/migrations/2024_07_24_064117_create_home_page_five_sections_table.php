<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_five_sections', function (Blueprint $table) {
            $table->id();
            $table->string('first_heading')->nullable();
            $table->text('first_paragraph')->nullable();
            $table->string('user_count_heading')->nullable();
            $table->string('user_text')->nullable();
            $table->string('percentage_heading')->nullable();
            $table->text('percentage_text')->nullable();
            $table->string('satisfaction_heading')->nullable();
            $table->text('satisfaction_text')->nullable();
            $table->text('second_paragraph')->nullable();
            $table->text('register_button')->nullable();
            $table->string('side_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_five_sections');
    }
};
