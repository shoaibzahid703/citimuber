<?php

use App\Http\Controllers\Admin\AgencyPlanController;
use App\Http\Controllers\Admin\AgencyServiceController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\EmployerCategoryController;
use App\Http\Controllers\Admin\HelperCategoryController;
use App\Http\Controllers\Admin\HelperPlansController;
use App\Http\Controllers\Admin\JobPictureController;
use App\Http\Controllers\Admin\MessageController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\StateController;
use App\Http\Controllers\Admin\TipsController;
use App\Http\Controllers\AgencyExpertController;
use App\Http\Controllers\AgencyStrengthController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\EmploymentBiodataController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\LanguageController;
use App\Http\Controllers\Admin\SkillsController;
use App\Http\Controllers\Admin\CookingskillsController;
use App\Http\Controllers\Admin\OtherskillsController;
use App\Http\Controllers\Admin\PersonalityController;
use App\Http\Controllers\Admin\PlansController;

use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\Admin\JobsController;
use App\Http\Controllers\Admin\PaymentMethodController;
use App\Http\Controllers\Admin\HomePageController;
use App\Http\Controllers\Admin\PublicHolidayController;
use App\Http\Controllers\Admin\EventsController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\HappyClientsController;
use App\Http\Controllers\Admin\FaqsController;
use App\Http\Controllers\Admin\TermsAndConditionController;
use App\Http\Controllers\Admin\PrivacyPolicyController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\HappyEmployeesController;
use App\Http\Controllers\Admin\JobPositionController;
use App\Http\Controllers\Admin\EducationController;
use App\Http\Controllers\Admin\ReligionController;
use App\Http\Controllers\Admin\EducationLevelController;


Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin_dashboard');


Route::get( 'profile', [ProfileController::class, 'index'] )->name( 'adminProfile' );
Route::post( 'profile/update', [ProfileController::class, 'update'] )->name( 'adminProfileUpdate' );

Route::get('/employer_bio_data', [EmploymentBioDataController::class, 'view_bio_data'])->name('admin_employer_bio_data');
Route::get('/pdf/employment-bio_data/{id}', [EmploymentBioDataController::class, 'pdf_bio_data'])->name('employment_bio_data_pdf');

Route::get( 'site-settings', [SettingController::class, 'site_setting'] )->name( 'site_settings');
Route::get( 'contact-us-settings', [SettingController::class, 'contact_us_settings'] )->name( 'contact_us_settings');
Route::post( 'site-settings-update', [SettingController::class, 'site_setting_update'] )->name( 'site_settings.update');
Route::post( 'contact-us-setting-update', [SettingController::class, 'contact_us_setting_update'] )->name( 'contact_us_setting_update');


Route::get( 'all-candidates', [UserController::class, 'all_candidates'] )->name( 'all_candidates' );
Route::get( 'add-candidate', [UserController::class, 'add_candidate'] )->name( 'add_candidate' );
Route::post( 'save-candidate', [UserController::class, 'save_candidate'] )->name( 'save_candidate' );
Route::get( 'edit-candidate/{candidate}', [UserController::class, 'edit_candidate'] )->name( 'admin_edit_candidate' );
Route::post( 'update-candidate/{candidate}', [UserController::class, 'update_candidate'] )->name( 'admin_update_candidate' );
Route::get( 'candidate-bio-data/{candidate}', [UserController::class, 'candidate_bio_data'] )->name( 'admin_candidate_bio_data' );
Route::post('bio-data-save/{candidate}', [UserController::class, 'candidate_bio_data_save'])->name('admin_candidate_bio_data_save');

Route::post('validate-bio-data-step-1', [UserController::class, 'validateBioDataStepOne'])->name('adminValidateBioDataStepOne');
Route::post('validate-bio-data-step-2', [UserController::class, 'validateBioDataStepTwo'])->name('adminValidateBioDataStepTwo');
Route::post('validate-bio-data-step-3', [UserController::class, 'validateBioDataStepThree'])->name('adminValidateBioDataStepThree');
Route::post('validate-bio-data-step-4', [UserController::class, 'validateBioDataStepFour'])->name('adminValidateBioDataStepFour');
Route::get('delete_experience/{experience}', [UserController::class, 'delete_experience'])->name('admin_delete_experience');
Route::get('delete_education/{education}', [UserController::class, 'delete_education'])->name('admin_delete_education');



Route::get( 'all-employers', [UserController::class, 'all_employers'] )->name( 'all_employers' );
Route::get( 'add-employer', [UserController::class, 'add_employer'] )->name( 'add_employer' );
Route::post( 'save-employer', [UserController::class, 'save_employer'] )->name( 'save_employer' );
Route::get( 'edit-employer/{employer}', [UserController::class, 'edit_employer'] )->name( 'admin_edit_employer' );
Route::post( 'update-employer/{employer}', [UserController::class, 'update_employer'] )->name( 'admin_update_employer' );

Route::get( 'all-agencies', [UserController::class, 'all_agencies'] )->name( 'all_agencies' );

Route::get( 'add-employer-job/{employer}', [UserController::class, 'admin_add_employer_job'] )->name( 'admin_add_employer_job' );
Route::post('validate-job-step-1', [UserController::class, 'validateJobStepOne'])->name('adminValidateJobStepOne');
Route::post('validate-job-step-2', [UserController::class, 'validateJobStepTwo'])->name('adminValidateJobStepTwo');
Route::post('validate-job-step-3', [UserController::class, 'validateJobStepThree'])->name('adminValidateJobStepThree');
Route::post('save-job/{employer}', [UserController::class,'save_job'])->name('admin_save_job');

Route::get('blockUser/{user}', [UserController::class, 'blockUser'])->name('blockUser');

Route::get('delete_user/{user}', [UserController::class, 'delete_user'])->name('delete_user');
Route::get('assign_category/{user}', [UserController::class, 'assign_category'])->name('assign_category');
Route::get('assign_helper_category/{user}', [UserController::class, 'assign_helper_category'])->name('assign_helper_category');
Route::post('save_employer_category/{user}', [UserController::class, 'save_employer_category'])->name('save_employer_category');
Route::post('save_helper_category/{user}', [UserController::class, 'save_helper_category'])->name('save_helper_category');


// countries
Route::get( '/countries', [CountryController::class, 'index'] )->name( 'countries' );
Route::get( '/country/{country}', [CountryController::class, 'edit'] )->name( 'country.view' );
Route::post( '/country/update/{country}', [CountryController::class, 'update'] )->name( 'country.update' );
Route::get( '/country/delete/{country}', [CountryController::class, 'delete'] )->name( 'country-delete' );
Route::get( '/country/lock/{country}', [CountryController::class, 'block'] )->name( 'country-block' );
Route::get( '/country/un-lock/{country}', [CountryController::class, 'un_block'] )->name( 'country-un_block' );

Route::get( '/add-country', [CountryController::class, 'create'] )->name( 'add-country' );
Route::post( '/save-country', [CountryController::class, 'store'] )->name( 'save-country' );
// states
Route::get( '/states', [StateController::class, 'index'] )->name( 'states' );
Route::get( '/state/{state}', [StateController::class, 'edit'] )->name( 'state.view' );
Route::post( '/state/update/{state}', [StateController::class, 'update'] )->name( 'state.update' );
Route::get( '/state/delete/{state}', [StateController::class, 'delete'] )->name( 'state-delete' );
Route::get( '/state/lock/{state}', [StateController::class, 'block'] )->name( 'state-block' );
Route::get( '/state/un-lock/{state}', [StateController::class, 'un_block'] )->name( 'state-un_block' );
Route::get( '/add-states', [StateController::class, 'create'] )->name( 'add-states' );
Route::post( '/save-states', [StateController::class, 'store'] )->name( 'save-states' );

// states
Route::get( '/cities', [CityController::class, 'index'] )->name( 'cities' );
Route::get( '/city/{city}', [CityController::class, 'edit'] )->name( 'city.view' );
Route::post( '/city/update/{city}', [CityController::class, 'update'] )->name( 'city.update' );
Route::get( '/city/delete/{city}', [CityController::class, 'delete'] )->name( 'city-delete' );
Route::get( '/city/lock/{city}', [CityController::class, 'block'] )->name( 'city-block');
Route::get( '/city/un-lock/{city}', [CityController::class, 'un_block'] )->name( 'city-un_block');
Route::get( '/add-city', [CityController::class, 'create'] )->name( 'add-city' );
Route::post( '/save-city', [CityController::class, 'store'] )->name( 'save-city' );



Route::get( '/languages', [LanguageController::class, 'index'] )->name( 'languages' );
Route::get( '/language/add', [LanguageController::class, 'create'] )->name( 'language_create' );
Route::post( '/language/store', [LanguageController::class, 'store'] )->name( 'language_store' );
Route::get( '/language/{language}', [LanguageController::class, 'edit'] )->name( 'language_edit' );
Route::post( '/language/update/{language}', [LanguageController::class, 'update'] )->name( 'language_update' );
Route::get( '/language/delete/{language}', [LanguageController::class, 'delete'] )->name( 'language_delete' );


Route::get( '/main-skills', [SkillsController::class, 'index'] )->name( 'main_skills' );
Route::get( '/main-skills/add', [SkillsController::class, 'create'] )->name( 'main_skills_create' );
Route::post( '/main-skills/store', [SkillsController::class, 'store'] )->name( 'main_skills_store' );
Route::get( '/main-skills/{skills}', [SkillsController::class, 'edit'] )->name( 'main_skills_edit' );
Route::post( '/main-skills/update/{skills}', [SkillsController::class, 'update'] )->name( 'main_skills_update' );
Route::get( '/main-skills/delete/{skills}', [SkillsController::class, 'delete'] )->name( 'main_skills_delete' );
Route::get('blockMainSkill/{skills}', [SkillsController::class, 'blockMainSkill'])->name('blockMainSkill');


Route::get( '/cooking-skills', [CookingskillsController::class, 'index'] )->name( 'cooking_skills' );
Route::get( '/cooking-skills/add', [CookingskillsController::class, 'create'] )->name( 'cooking_skills_create' );
Route::post( '/cooking-skills/store', [CookingskillsController::class, 'store'] )->name( 'cooking_skills_store' );
Route::get( '/cooking-skills/{skills}', [CookingskillsController::class, 'edit'] )->name( 'cooking_skills_edit' );
Route::post( '/cooking-skills/update/{skills}', [CookingskillsController::class, 'update'] )->name( 'cooking_skills_update' );
Route::get( '/cooking-skills/delete/{skills}', [CookingskillsController::class, 'delete'] )->name( 'cooking_skills_delete' );
Route::get('blockCookingSkill/{skills}', [CookingskillsController::class, 'blockCookingSkill'])->name('blockCookingSkill');



Route::get( '/other-skills', [OtherskillsController::class, 'index'] )->name( 'other_skills' );
Route::get( '/other-skills/add', [OtherskillsController::class, 'create'] )->name( 'other_skills_create' );
Route::post( '/other-skills/store', [OtherskillsController::class, 'store'] )->name( 'other_skills_store' );
Route::get( '/other-skills/{skills}', [OtherskillsController::class, 'edit'] )->name( 'other_skills_edit' );
Route::post( '/other-skills/update/{skills}', [OtherskillsController::class, 'update'] )->name( 'other_skills_update' );
Route::get( '/other-skills/delete/{skills}', [OtherskillsController::class, 'delete'] )->name( 'other_skills_delete' );
Route::get('blockOtherSkill/{skills}', [OtherskillsController::class, 'blockOtherSkill'])->name('blockOtherSkill');


Route::get( '/personality', [PersonalityController::class, 'index'] )->name( 'personality' );
Route::get( '/personality/add', [PersonalityController::class, 'create'] )->name( 'personality_create' );
Route::post( '/personality/store', [PersonalityController::class, 'store'] )->name( 'personality_store' );
Route::get( '/personality/{personality}', [PersonalityController::class, 'edit'] )->name( 'personality_edit' );
Route::post( '/personality/update/{personality}', [PersonalityController::class, 'update'] )->name( 'personality_update' );
Route::get( '/personality/delete/{personality}', [PersonalityController::class, 'delete'] )->name( 'personality_delete' );
Route::get('block/personality/{personality}', [PersonalityController::class, 'block_personality'])->name('block_personality');


Route::get( 'employer/plans', [PlansController::class, 'index'] )->name( 'plans' );
Route::get( 'employer/plan/add', [PlansController::class, 'create'] )->name( 'plans_create' );
Route::post( 'employer/plan/store', [PlansController::class, 'store'] )->name( 'plans_store' );
Route::get( 'employer/plan/{plans}', [PlansController::class, 'edit'] )->name( 'plans_edit' );
Route::post( 'employer/plan/update/{plans}', [PlansController::class, 'update'] )->name( 'plans_update' );
Route::get( 'employer/plan/delete/{plans}', [PlansController::class, 'delete'] )->name( 'plans_delete' );

Route::get('block/plan/{plan}', [PlansController::class, 'block_plans'])->name('block_plans');

Route::get( 'helper/plans', [HelperPlansController::class, 'index'] )->name( 'helper_plans' );
Route::get( 'helper/plan/add', [HelperPlansController::class, 'create'] )->name( 'helper_plan_create' );
Route::post( 'helper/plan/store', [HelperPlansController::class, 'store'] )->name( 'helper_plan_store' );
Route::get( 'helper/plan/{plan}', [HelperPlansController::class, 'edit'] )->name( 'helper_plan_edit' );
Route::post( 'helper/plan/update/{plan}', [HelperPlansController::class, 'update'] )->name( 'helper_plan_update' );
Route::get( 'helper/plan/delete/{plan}', [HelperPlansController::class, 'delete'] )->name( 'helper_plan_delete' );
Route::get('helper/plan/block/{plan}', [HelperPlansController::class, 'block_plan'])->name('helper_plan_block');

Route::get( 'agency/plans', [AgencyPlanController::class, 'index'] )->name( 'agency_plans' );
Route::get( 'agency/plan/add', [AgencyPlanController::class, 'create'] )->name( 'agency_plan_create' );
Route::post( 'agency/plan/store', [AgencyPlanController::class, 'store'] )->name( 'agency_plan_store' );
Route::get( 'agency/plan/{plan}', [AgencyPlanController::class, 'edit'] )->name( 'agency_plan_edit' );
Route::post( 'agency/plan/update/{plan}', [AgencyPlanController::class, 'update'] )->name( 'agency_plan_update' );
Route::get( 'agency/plan/delete/{plan}', [AgencyPlanController::class, 'delete'] )->name( 'agency_plan_delete' );
Route::get('agency/plan/block/{plan}', [AgencyPlanController::class, 'block_plan'])->name('agency_plan_block');

Route::get( 'agency/services', [AgencyServiceController::class, 'index'] )->name( 'agency_services');
Route::get( 'agency/service/add', [AgencyServiceController::class, 'create'] )->name( 'agency_service_create' );
Route::post( 'agency/service/store', [AgencyServiceController::class, 'store'] )->name( 'agency_service_store' );
Route::get( 'agency/service/{service}', [AgencyServiceController::class, 'edit'] )->name( 'agency_service_edit' );
Route::post( 'agency/service/update/{service}', [AgencyServiceController::class, 'update'] )->name( 'agency_service_update' );
Route::get( 'agency/service/delete/{service}', [AgencyServiceController::class, 'delete'] )->name( 'agency_service_delete' );
Route::get('agency/service/block/{service}', [AgencyServiceController::class, 'block_service'])->name('agency_service_block');



Route::get( '/currency', [CurrencyController::class, 'index'] )->name( 'currency' );
Route::get( '/currency/add', [CurrencyController::class, 'create'] )->name( 'currency_create' );
Route::post( '/currency/store', [CurrencyController::class, 'store'] )->name( 'currency_store' );
Route::get( '/currency/{currency}', [CurrencyController::class, 'edit'] )->name( 'currency_edit' );
Route::post( '/currency/update/{currency}', [CurrencyController::class, 'update'] )->name( 'currency_update' );
Route::get( '/currency/delete/{currency}', [CurrencyController::class, 'delete'] )->name( 'currency_delete' );
Route::get('block/currency/{currency}', [CurrencyController::class, 'block_currency'])->name('block_currency');



Route::get( '/duties', [JobsController::class, 'index'] )->name( 'duties' );
Route::get( '/duties/add', [JobsController::class, 'create'] )->name( 'duties_create' );
Route::post( '/duties/store', [JobsController::class, 'store'] )->name( 'duties_store' );
Route::get( '/duties/{duties}', [JobsController::class, 'edit'] )->name( 'duties_edit' );
Route::post( '/duties/update/{duties}', [JobsController::class, 'update'] )->name( 'duties_update' );
Route::get( '/duties/delete/{duties}', [JobsController::class, 'delete'] )->name( 'duties_delete' );
Route::get('block/duties/{duties}', [JobsController::class, 'block_duties'])->name('block_duties');


Route::get( '/payment-method', [PaymentMethodController::class, 'payment_method'] )->name( 'payment-method' );
Route::post('/update-env', [PaymentMethodController::class, 'update'])->name('update.env');


// home page dynamic

Route::get( '/first-section', [HomePageController::class, 'first_section'] )->name( 'first-section' );
Route::post( '/save-first-section', [HomePageController::class, 'save_first_section'] )->name( 'save-first-section' );

Route::get( '/second-section', [HomePageController::class, 'second_section'] )->name( 'second-section' );
Route::post( '/save-second-section', [HomePageController::class, 'save_second_section'] )->name( 'save-second-section' );

Route::get( '/third-section', [HomePageController::class, 'third_section'] )->name( 'third-section' );
Route::post( '/save-third-section', [HomePageController::class, 'save_third_section'] )->name( 'save-third-section' );

Route::get( '/fourth-section', [HomePageController::class, 'fourth_section'] )->name( 'fourth-section' );
Route::post( '/save-fourth-section', [HomePageController::class, 'save_fourth_section'] )->name( 'save-fourth-section' );

Route::get( '/five-section', [HomePageController::class, 'five_section'] )->name( 'five-section' );
Route::post( '/save-five-section', [HomePageController::class, 'save_five_section'] )->name( 'save-five-section' );

Route::get( '/six-section', [HomePageController::class, 'six_section'] )->name( 'six-section' );
Route::post( '/save-six-section', [HomePageController::class, 'save_six_section'] )->name( 'save-six-section' );

Route::get( '/seven-section', [HomePageController::class, 'seven_section'] )->name( 'seven-section' );
Route::post( '/save-seven-section', [HomePageController::class, 'save_seven_section'] )->name( 'save-seven-section' );

Route::get( '/eight-section', [HomePageController::class, 'eight_section'] )->name( 'eight_section' );
Route::post( '/save-eight-section', [HomePageController::class, 'save_eight_section'] )->name( 'save-eight-section' );

Route::get( '/add-comment', [HomePageController::class, 'add_comment'] )->name( 'add-comment' );
Route::post( '/save-comment', [HomePageController::class, 'save_comment'] )->name( 'save-comment' );
Route::get( '/edit-comment/{client}', [HomePageController::class, 'edit_comment'] )->name( 'edit-comment' );
Route::post( '/update-comment/{client}', [HomePageController::class, 'update_comment'] )->name( 'update-comment' );
Route::get( '/delete-comment/{client}', [HomePageController::class, 'delete_comment'] )->name( 'delete-comment' );
Route::get('block/comment/{client}', [HomePageController::class, 'block_comment'])->name('block_comment');

Route::get( '/candidates-landing-page', [HomePageController::class, 'candidates_page'] )->name( 'candidates-page' );
Route::post( '/save-candidates-page', [HomePageController::class, 'save_candidates_page'] )->name( 'save-candidates-page' );


Route::get( '/partner-landing-page', [HomePageController::class, 'partner_page'] )->name( 'partner-page' );
Route::post( '/save-partner-page', [HomePageController::class, 'save_partner_page'] )->name( 'save-partner-page' );

Route::get( '/jobs-landing-page', [HomePageController::class, 'jobs_page'] )->name( 'jobs-page' );
Route::post( '/save-jobs-page', [HomePageController::class, 'save_jobs_page'] )->name( 'save-jobs-page' );

Route::get( '/agencies-landing-page', [HomePageController::class, 'agencies_page'] )->name( 'agencies-page' );
Route::post( '/save-agencies-page', [HomePageController::class, 'save_agencies_page'] )->name( 'save-agencies-page' );

Route::get( '/training-landing-page', [HomePageController::class, 'training_page'] )->name( 'training-page' );
Route::post( '/save-training-page', [HomePageController::class, 'save_training_page'] )->name( 'save-training-page' );

Route::get( '/association-landing-page', [HomePageController::class, 'association_page'] )->name( 'association-page' );
Route::post( '/save-association-page', [HomePageController::class, 'save_association_page'] )->name( 'save-association-page' );

Route::get( '/news-landing-page', [HomePageController::class, 'news_page'] )->name( 'news-page' );
Route::post( '/save-news-page', [HomePageController::class, 'save_news_page'] )->name( 'save-news-page' );

Route::get( '/tips-landing-page', [HomePageController::class, 'tips_page'] )->name( 'tips-page' );
Route::post( '/save-tips-page', [HomePageController::class, 'save_tips_page'] )->name( 'save-tips-page' );

Route::get( '/about-us-landing-page', [HomePageController::class, 'about_us_page'] )->name( 'about_us_page' );
Route::post( '/save-about-us-page', [HomePageController::class, 'save_about_us_page'] )->name( 'save-about-us-page' );


Route::get( '/public-holiday-landing-page', [PublicHolidayController::class, 'holiday_page'] )->name( 'holiday_page' );
Route::post( '/save-holiday-page', [PublicHolidayController::class, 'save_holiday_page'] )->name( 'save-holiday-page' );


Route::get( '/add-holiday', [PublicHolidayController::class, 'add_holiday'] )->name( 'add-holiday' );
Route::post( '/save-holiday', [PublicHolidayController::class, 'save_holiday'] )->name( 'save-holiday' );
Route::get( '/edit-holiday/{holiday}', [PublicHolidayController::class, 'edit_holiday'] )->name( 'edit-holiday' );
Route::post( '/update-holiday/{holiday}', [PublicHolidayController::class, 'update_holiday'] )->name( 'update-holiday' );
Route::get( '/delete-holiday/{holiday}', [PublicHolidayController::class, 'delete_holiday'] )->name( 'delete-holiday' );
Route::get('block/holiday/{holiday}', [PublicHolidayController::class, 'block_holiday'])->name('block_holiday');


Route::get( '/employer-dashboard-page', [HomePageController::class, 'employer_dashboard_page'] )->name( 'employer-dashboard-page' );
Route::post( '/save-employer-dashboard-page', [HomePageController::class, 'save_page'] )->name( 'save-employer-dashboard-page' );


Route::get( '/candidate-dashboard-page', [HomePageController::class, 'candidate_dashboard_page'] )->name( 'candidate-dashboard-page' );
Route::post( '/save-candidate-dashboard-page', [HomePageController::class, 'candidate_save_page'] )->name( 'save-candidate-dashboard-page' );

Route::get( '/events', [EventsController::class, 'events'] )->name( 'events' );
Route::post( '/save-events', [EventsController::class, 'save_events'] )->name( 'save-events' );

Route::get( '/add-events', [EventsController::class, 'add_events'] )->name( 'add-events' );
Route::post( '/store-events', [EventsController::class, 'store_events'] )->name( 'store-events' );

Route::get( '/edit-event/{event}', [EventsController::class, 'edit_event'] )->name( 'edit-event' );
Route::post( '/update-event/{event}', [EventsController::class, 'update_event'] )->name( 'update-event' );
Route::get( '/delete-event/{event}', [EventsController::class, 'delete_event'] )->name( 'delete-event' );
Route::get('block/event/{event}', [EventsController::class, 'block_event'])->name('block_event');

Route::get( '/all-jobs', [JobController::class, 'all_jobs'] )->name( 'all_jobs' );
Route::get('block/job/{job}', [JobController::class, 'block_job'])->name('block_job');
Route::get( '/delete-job/{job}', [JobController::class, 'delete_job'] )->name( 'delete_job' );


Route::get( 'job-pictures', [JobPictureController::class, 'index'] )->name( 'job_pictures' );
Route::get( 'job-picture/add', [JobPictureController::class, 'create'] )->name( 'job_picture_create' );
Route::post( 'job-picture/store', [JobPictureController::class, 'store'] )->name( 'job_picture_store' );
Route::get( 'job-picture/{job_picture}', [JobPictureController::class, 'edit'] )->name( 'job_picture_edit' );
Route::post( 'job-picture/update/{job_picture}', [JobPictureController::class, 'update'] )->name( 'job_picture_update' );
Route::get( 'job-picture/delete/{job_picture}', [JobPictureController::class, 'delete'] )->name( 'job_picture_delete' );


Route::get( 'transactions', [PaymentController::class, 'index'] )->name( 'all_transactions' );
Route::get( 'transaction/delete/{transaction}', [PaymentController::class, 'delete'] )->name( 'delete_transaction' );


//happy helpers
Route::get( '/happy-helpers', [HappyClientsController::class, 'index'] )->name( 'happy-helpers' );
Route::post( '/save-happy-helpers', [HappyClientsController::class, 'save_happy_helpers'] )->name( 'save-happy-helpers' );

//faq's page
Route::get( '/faqs-page', [FaqsController::class, 'index'] )->name( 'faqs-page' );
Route::get( '/add-faqs', [FaqsController::class, 'add_faqs'] )->name( 'add-faqs' );
Route::post( '/save-faqs', [FaqsController::class, 'save_faqs'] )->name( 'save-faqs' );
Route::get( '/edit-faqs/{faqs}', [FaqsController::class, 'edit_faqs'] )->name( 'edit-faqs' );
Route::post( '/update-faqs/{faqs}', [FaqsController::class, 'update_faqs'] )->name( 'update-faqs' );
Route::get( '/delete-faqs/{faqs}', [FaqsController::class, 'delete_faqs'] )->name( 'delete-faqs' );
Route::get('block/faqs/{faqs}', [FaqsController::class, 'block_faqs'])->name('block_faqs');

Route::get( '/term-and-condition', [TermsAndConditionController::class, 'index'] )->name( 'term-and-condition' );
Route::post( '/save-term-and-condition', [TermsAndConditionController::class, 'save_term_and_condition'] )->name( 'save-term-and-condition' );

Route::get( 'privacy-policy', [PrivacyPolicyController::class, 'index'] )->name( 'privacy-policy' );
Route::post( '/save-privacy-policy', [PrivacyPolicyController::class, 'save_privacy_policy'] )->name( 'save-privacy-policy' );

Route::get( 'contact-us', [ContactUsController::class, 'index'] )->name( 'contact-us' );
Route::post( '/save-contact-us', [ContactUsController::class, 'save_contact_us'] )->name( 'save-contact-us' );

// happy employees
Route::get( '/happy-employees', [HappyEmployeesController::class, 'index'] )->name( 'happy-employees' );
Route::post( '/save-happy-employee', [HappyEmployeesController::class, 'save_happy_employee'] )->name( 'save-happy-employees' );

Route::get( '/add-happy-employee', [HappyEmployeesController::class, 'add_happy_employee'] )->name( 'add-happy-employee' );
Route::post( '/store-happy-employee', [HappyEmployeesController::class, 'store_happy_employee'] )->name( 'store-happy-employee' );

Route::get( '/edit-happy-employee/{employee}', [HappyEmployeesController::class, 'edit_happy_employee'] )->name( 'edit-happy-employee' );
Route::post( '/update-happy-employee/{employee}', [HappyEmployeesController::class, 'update_happy_employee'] )->name( 'update-happy-employee' );
Route::get( '/delete-happy-employee/{employee}', [HappyEmployeesController::class, 'delete_happy_employee'] )->name( 'delete-happy-employee' );
Route::get('block/employee/{employee}', [HappyEmployeesController::class, 'block_happy_employee'])->name('block_happy_employee');


Route::get( 'employer/category', [EmployerCategoryController::class, 'index'] )->name( 'employer_categories' );
Route::get( 'employer/category/add', [EmployerCategoryController::class, 'create'] )->name( 'employer_category_create' );
Route::post( 'employer/category/store', [EmployerCategoryController::class, 'store'] )->name( 'employer_category_store' );
Route::get( 'employer/category/{category}', [EmployerCategoryController::class, 'edit'] )->name( 'employer_category_edit' );
Route::post( 'employer/category/update/{category}', [EmployerCategoryController::class, 'update'] )->name( 'employer_category_update' );
Route::get( 'employer/category/delete/{category}', [EmployerCategoryController::class, 'delete'] )->name( 'employer_category_delete' );

Route::get( '/categories', [CategoryController::class, 'index'] )->name( 'categories' );
Route::get( '/category/add', [CategoryController::class, 'create'] )->name( 'category_create' );
Route::post( '/category/store', [CategoryController::class, 'store'] )->name( 'category_store' );
Route::get( '/category/{category}', [CategoryController::class, 'edit'] )->name( 'category_edit' );
Route::post( '/category/update/{category}', [CategoryController::class, 'update'] )->name( 'category_update' );
Route::get( '/category/delete/{category}', [CategoryController::class, 'delete'] )->name( 'category_delete' );
Route::get('block/category/{category}', [CategoryController::class, 'block_category'])->name('block_category');

// news page
Route::get( '/news-landing-page', [NewsController::class, 'index'] )->name( 'news_page' );
Route::post( '/save-news-page', [NewsController::class, 'save_news_page'] )->name( 'save-news-page' );


Route::get( '/add-news', [NewsController::class, 'add_news'] )->name( 'add-news' );
Route::post( '/store-news', [NewsController::class, 'store_news'] )->name( 'store-news' );
Route::get( '/edit-news/{news}', [NewsController::class, 'edit_news'] )->name( 'edit-news' );
Route::post( '/update-news/{news}', [NewsController::class, 'update_news'] )->name( 'update-news' );
Route::get( '/delete-news/{news}', [NewsController::class, 'delete_news'] )->name( 'delete-news' );
Route::get('block/news/{news}', [NewsController::class, 'block_news'])->name('block_news');


Route::get( '/tips-landing-page', [TipsController::class, 'index'] )->name( 'tips_page' );
Route::post( '/save-tips-page', [TipsController::class, 'save_tips_page'] )->name( 'save-tips-page' );

Route::get( '/add-tips', [TipsController::class, 'add_tips'] )->name( 'add-tips' );
Route::post( '/store-tips', [TipsController::class, 'store_tips'] )->name( 'store-tips' );
Route::get( '/edit-tips/{tips}', [TipsController::class, 'edit_tips'] )->name( 'edit-tips' );
Route::post( '/update-tips/{tips}', [TipsController::class, 'update_tips'] )->name( 'update-tips' );
Route::get( '/delete-tips/{tips}', [TipsController::class, 'delete_tips'] )->name( 'delete-tips' );
Route::get('block/tips/{tips}', [TipsController::class, 'block_tips'])->name('block_tips');


Route::get( 'helper/category', [HelperCategoryController::class, 'index'] )->name( 'helper_categories' );
Route::get( 'helper/category/add', [HelperCategoryController::class, 'create'] )->name( 'helper_category_create' );
Route::post( 'helper/category/store', [HelperCategoryController::class, 'store'] )->name( 'helper_category_store' );
Route::get( 'helper/category/{category}', [HelperCategoryController::class, 'edit'] )->name( 'helper_category_edit' );
Route::post( 'helper/category/update/{category}', [HelperCategoryController::class, 'update'] )->name( 'helper_category_update' );
Route::get( 'helper/category/delete/{category}', [HelperCategoryController::class, 'delete'] )->name( 'helper_category_delete' );


Route::get( '/candidate-detail-page', [HomePageController::class, 'candidate_detail_page'] )->name( 'candidate-detail-page' );
Route::post( '/save-candidate-detail-page', [HomePageController::class, 'save_candidate_detail_page'] )->name( 'save-candidate-detail-page');

 // job position
Route::get( '/job-position', [JobPositionController::class, 'index'] )->name( 'job-position' );
Route::get( '/job-position/add', [JobPositionController::class, 'create'] )->name( 'job_position_create' );
Route::post( '/job-position/store', [JobPositionController::class, 'store'] )->name( 'job_position_store' );
Route::get( '/job-position/{JobPosition}', [JobPositionController::class, 'edit'] )->name( 'job_position_edit' );
Route::post( '/job-position/update/{JobPosition}', [JobPositionController::class, 'update'] )->name( 'job_position_update' );
Route::get( '/job-position/delete/{JobPosition}', [JobPositionController::class, 'delete'] )->name( 'job_position_delete' );
Route::get('block/job-position/{JobPosition}', [JobPositionController::class, 'block_job_position'])->name('block_job_position');


 // education
Route::get( '/education', [EducationController::class, 'index'] )->name( 'education' );
Route::get( '/education/add', [EducationController::class, 'create'] )->name( 'education_create' );
Route::post( '/education/store', [EducationController::class, 'store'] )->name( 'education_store' );
Route::get( '/education/{education}', [EducationController::class, 'edit'] )->name( 'education_edit' );
Route::post( '/education/update/{education}', [EducationController::class, 'update'] )->name( 'education_update' );
Route::get( '/education/delete/{education}', [EducationController::class, 'delete'] )->name( 'education_delete' );
Route::get('block/education/{education}', [EducationController::class, 'block_education'])->name('block_education');

 // religins
Route::get( '/religions', [ReligionController::class, 'index'] )->name( 'religions' );
Route::get( '/religion/add', [ReligionController::class, 'create'] )->name( 'religion_create' );
Route::post( '/religion/store', [ReligionController::class, 'store'] )->name( 'religion_store' );
Route::get( '/religion/{religion}', [ReligionController::class, 'edit'] )->name( 'religion_edit' );
Route::post( '/religion/update/{religion}', [ReligionController::class, 'update'] )->name( 'religion_update' );
Route::get( '/religion/delete/{religion}', [ReligionController::class, 'delete'] )->name( 'religion_delete' );
Route::get('block/religion/{religion}', [ReligionController::class, 'block_religion'])->name('block_religion');


 // education levels
Route::get( '/education-levels', [EducationLevelController::class, 'index'] )->name( 'education_levels' );
Route::get( '/education-levels/add', [EducationLevelController::class, 'create'] )->name( 'education_level_create' );
Route::post( '/education-levels/store', [EducationLevelController::class, 'store'] )->name( 'education_level_store' );
Route::get( '/education-levels/{education}', [EducationLevelController::class, 'edit'] )->name( 'education_level_edit' );
Route::post( '/education-levels/update/{education}', [EducationLevelController::class, 'update'] )->name( 'education_level_update' );
Route::get( '/education-levels/delete/{education}', [EducationLevelController::class, 'delete'] )->name( 'education_level_delete' );
Route::get('block/education-levels/{education}', [EducationLevelController::class, 'block_education_level'])->name('block_education_level');

Route::get('messages', [MessageController::class, 'index'])->name('admin_messages');
Route::get('msg/{receiver}', [MessageController::class, 'chatIndex'])->name('adminChatIndex');
Route::post('send_msg', [MessageController::class, 'send_msg'])->name('admin_send_msg');
Route::get('delete_msg/{message}', [MessageController::class, 'delete_msg'])->name('admin_delete_msg');
Route::post('get_admin_unread_messages_count', [MessageController::class, 'get_admin_unread_messages_count'])->name('get_admin_unread_messages_count');
Route::post('get_received_messages', [MessageController::class, 'get_received_messages'])->name('get_received_messages');
Route::get('mark-as-read/{receiver}', [MessageController::class, 'mark_as_read'])->name('mark_as_read');


Route::get( 'agency-strengths', [AgencyStrengthController::class, 'index'] )->name( 'agency_strength');
Route::get( 'agency-strength/add', [AgencyStrengthController::class, 'create'] )->name( 'agency_strength_create');
Route::post( 'agency-strength/store', [AgencyStrengthController::class, 'store'] )->name( 'agency_strength_store');
Route::get( 'agency-strength/{strength}', [AgencyStrengthController::class, 'edit'] )->name( 'agency_strength_edit');
Route::post( 'agency-strength/update/{strength}', [AgencyStrengthController::class, 'update'] )->name( 'agency_strength_update');
Route::get( 'agency-strength/delete/{strength}', [AgencyStrengthController::class, 'delete'] )->name( 'agency_strength_delete');


Route::get( 'agency-experts', [AgencyExpertController::class, 'index'] )->name( 'agency_experts');
Route::get( 'agency-expert/add', [AgencyExpertController::class, 'create'] )->name( 'agency_expert_create');
Route::post( 'agency-expert/store', [AgencyExpertController::class, 'store'] )->name( 'agency_expert_store');
Route::get( 'agency-expert/{expert}', [AgencyExpertController::class, 'edit'] )->name( 'agency_expert_edit');
Route::post( 'agency-expert/update/{expert}', [AgencyExpertController::class, 'update'] )->name( 'agency_expert_update');
Route::get( 'agency-expert/delete/{expert}', [AgencyExpertController::class, 'delete'] )->name( 'agency_expert_delete');


