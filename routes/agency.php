<?php


use App\Http\Controllers\Agency\AgencyPackageController;
use App\Http\Controllers\Agency\DashboardController;
use App\Http\Controllers\Agency\HelperController;
use App\Http\Controllers\Agency\JobController;
use App\Http\Controllers\Agency\MessageController;
use App\Http\Controllers\Agency\ProfileController;
use App\Http\Controllers\Agency\ServiceController;

Route::get('/dashboard', [DashboardController::class, 'index'])->name('agency_dashboard');
Route::get('/profile', [ProfileController::class, 'index'])->name('agency_profile');
Route::get('/info', [ProfileController::class, 'update_info'])->name('update_info');
Route::post('/info-save', [ProfileController::class, 'info_save'])->name('info_save');
Route::post( 'profile/update', [ProfileController::class, 'update'] )->name( 'agency_profile_update' );

Route::post('agency_update_password', [ProfileController::class, 'agency_update_password'])->name('agency_update_password');


Route::get('agency-post-job', [JobController::class, 'create'])->name('agency_post_job');
Route::post('agency-save-job', [JobController::class,'store'])->name('agency_save_job');

Route::post('agency-validate-job-step-1', [JobController::class, 'validateJobStepOne'])->name('agency_validate_job_step_one');
Route::post('agency-validate-job-step-2', [JobController::class, 'validateJobStepTwo'])->name('agency_validate_job_step_two');
Route::post('agency-validate-job-step-3', [JobController::class, 'validateJobStepThree'])->name('agency_validate_job_step_three');

Route::get('manage-jobs', [JobController::class, 'manage_jobs'])->name('agency_manage_jobs');
Route::get('jobs/fetch', [JobController::class, 'fetchJobs'])->name('agency_jobs_fetch');
Route::get( 'manage-jobs/edit/{job}', [JobController::class, 'edit'] )->name( 'agency_job_edit' );
Route::post( 'manage-jobs/update/{job}', [JobController::class, 'update'] )->name( 'agency_job_update' );
Route::get( 'manage-jobs/delete/{job}', [JobController::class, 'delete'] )->name( 'agency_job_delete' );

Route::get('add-helper', [HelperController::class, 'create'])->name('agency_add_helper');
Route::post('helper-save', [HelperController::class, 'save'])->name('agency_save_helper');
Route::get('all-helpers', [HelperController::class, 'index'])->name('all_helpers');
Route::get( 'edit-helper/{user}', [HelperController::class, 'edit_helper'] )->name( 'agency_edit_helper' );
Route::post( 'update-helper/{user}', [HelperController::class, 'update_helper'] )->name( 'agency_update_helper' );

Route::get('agency-packages', [AgencyPackageController::class, 'agency_packages'])->name('agency_packages');
Route::post('pay/package/{package}', [AgencyPackageController::class, 'pay_agency_package'])->name('pay_agency_package');
Route::post('pay/package/payment/{package}', [AgencyPackageController::class, 'pay_agency_package_payment'])->name('pay_agency_package_payment');


Route::get('all-helpers', [HelperController::class, 'index'])->name('all_helpers');
Route::get( 'edit-helper/{user}', [HelperController::class, 'edit_helper'] )->name( 'agency_edit_helper' );
Route::post( 'update-helper/{user}', [HelperController::class, 'update_helper'] )->name( 'agency_update_helper' );


Route::get( 'services', [ServiceController::class, 'index'] )->name( 'services');
Route::get( 'service/add', [ServiceController::class, 'create'] )->name( 'service_create');
Route::post( 'service/store', [ServiceController::class, 'store'] )->name( 'service_store');
Route::get( 'service/{service}', [ServiceController::class, 'edit'] )->name( 'service_edit');
Route::post( 'service/update/{service}', [ServiceController::class, 'update'] )->name( 'service_update');
Route::get( 'service/delete/{service}', [ServiceController::class, 'delete'] )->name( 'service_delete');



Route::post('validate-add-helper-step-1', [HelperController::class, 'validate_add_helper_step_one'])->name('validate_add_helper_step_one');
Route::post('validate-add-helper-step-2', [HelperController::class, 'validate_add_helper_step_two'])->name('validate_add_helper_step_two');
Route::post('validate-add-helper-step-3', [HelperController::class, 'validate_add_helper_step_three'])->name('validate_add_helper_step_three');
Route::post('validate-add-helper-step-4', [HelperController::class, 'validate_add_helper_step_four'])->name('validate_add_helper_step_four');
Route::get('delete_experience/{experience}', [HelperController::class, 'agency_delete_experience'])->name('agency_delete_experience');
Route::get('delete_education/{education}', [HelperController::class, 'agency_delete_education'])->name('agency_delete_education');


Route::get('messages', [MessageController::class, 'messages'])->name('agency_messages');
Route::get('message/{receiver}', [MessageController::class, 'chat_index'])->name('agency_chat_index');
Route::post('send/message', [MessageController::class, 'send_message'])->name('agency_send_message');
Route::post('unread_messages_count', [MessageController::class, 'unread_messages_count'])->name('agency_unread_messages_count');
Route::post('received_messages', [MessageController::class, 'received_messages'])->name('agency_received_messages');
Route::get('mark-as-read/{receiver}', [MessageController::class, 'mark_as_read'])->name('agency_message_mark_as_read');



