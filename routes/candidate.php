<?php

use App\Http\Controllers\Candidate\DashboardController;
use App\Http\Controllers\Candidate\BiodataController;
use App\Http\Controllers\Candidate\HelperPackageController;
use App\Http\Controllers\Candidate\MessageController;
use App\Http\Controllers\Candidate\ProfileController;


Route::get('/dashboard', [DashboardController::class, 'index'])->name('candidate_dashboard');

Route::get('/profile', [ProfileController::class, 'index'])->name('candidate_profile');
Route::post( 'profile/update', [ProfileController::class, 'update'] )->name( 'candidateProfileUpdate' );
Route::post('candidate_update_password', [ProfileController::class, 'candidate_update_password'])->name('candidate_update_password');
Route::get('change_visibility', [ProfileController::class, 'change_visibility'])->name('change_visibility');
Route::get('delete_experience/{experience}', [ProfileController::class, 'change_visibility'])->name('change_visibility');




Route::get('/bio-data', [BiodataController::class, 'create'])->name('bio-data');
Route::post('bio-data-save', [BiodataController::class, 'save'])->name('bio-data-save');
Route::post('validate-bio-data-step-1', [BiodataController::class, 'validateBioDataStepOne'])->name('validateBioDataStepOne');
Route::post('validate-bio-data-step-2', [BiodataController::class, 'validateBioDataStepTwo'])->name('validateBioDataStepTwo');
Route::post('validate-bio-data-step-3', [BiodataController::class, 'validateBioDataStepThree'])->name('validateBioDataStepThree');
Route::post('validate-bio-data-step-4', [BiodataController::class, 'validateBioDataStepFour'])->name('validateBioDataStepFour');
Route::get('delete_experience/{experience}', [BiodataController::class, 'delete_experience'])->name('delete_experience');
Route::get('delete_education/{education}', [BiodataController::class, 'delete_education'])->name('delete_education');


Route::get('resume-detail/{user}', [BiodataController::class, 'resume_detail'])->name('resume_detail');


Route::post('/biodata', [BiodataController::class, 'store'])->name('biodata-store');
Route::get('/view/biodata', [BiodataController::class, 'view_biodata'])->name('biodata-view');


Route::post('/job_apply', [BiodataController::class, 'job_apply'])->name('job_apply');

Route::get('helper-packages', [HelperPackageController::class, 'helper_packages'])->name('helper_packages');
Route::post('pay/helper/package/{package}', [HelperPackageController::class, 'pay_helper_package'])->name('pay_helper_package');
Route::post('pay/package/payment/{package}', [HelperPackageController::class, 'pay_helper_package_payment'])->name('pay_helper_package_payment');


Route::get('support', [MessageController::class, 'support'])->name('candidate_support');
Route::post('send_support_msg', [MessageController::class, 'send_support_msg'])->name('candidate_send_support_msg');
Route::post('get_received_messages', [MessageController::class, 'get_received_messages'])->name('candidate_received_messages');

Route::get('messages', [MessageController::class, 'messages'])->name('candidate_messages');
Route::get('message/{receiver}', [MessageController::class, 'chat_index'])->name('candidate_chat_index');
Route::post('send/message', [MessageController::class, 'send_message'])->name('candidate_send_message');
Route::post('unread_messages_count', [MessageController::class, 'unread_messages_count'])->name('candidate_unread_messages_count');
Route::post('received_messages', [MessageController::class, 'received_messages'])->name('candidate_received_messages');
Route::get('mark-as-read/{receiver}', [MessageController::class, 'mark_as_read'])->name('candidate_message_mark_as_read');






