<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\EmploymentBiodataController;

use Illuminate\Support\Facades\Artisan;

Route::get('/create-storage-link', function () {
    Artisan::call('storage:link');
    return 'Storage link created successfully';
});

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('optimize:clear');
    return 'Cache cleared successfully.';
});

Route::post('/log-click', [FrontendController::class, 'logClick'])->name('log-click');
Route::get('/', [App\Http\Controllers\FrontendController::class, 'index'])->name('welcome');

Route::get('partner-signup', [App\Http\Controllers\FrontendController::class, 'partner_signup'])
    ->name('partner_signup');

Route::get('partner-login', [App\Http\Controllers\FrontendController::class, 'partner_login'])
    ->name('partner_login');

Route::post('login-partner', [App\Http\Controllers\FrontendController::class, 'login_partner'])
    ->name('login_partner');

Route::post('save_agency', [App\Http\Controllers\FrontendController::class, 'save_agency'])
    ->name('save_agency');

Route::get( '/get-state/{country_id}', [FrontendController::class, 'get_states'] )
    ->name( 'get_states' );

Route::get( '/get-cities/{state_id}', [FrontendController::class, 'get_cities'] )
    ->name( 'get_cities' );

Route::get( '/candidate_details/{candidate}', [FrontendController::class, 'candidate_details'] )
    ->name( 'candidate_details' );

Route::post( '/send-helper-message/{candidate}', [FrontendController::class, 'send_helper_message'] )
    ->name( 'send_helper_message' );

Route::get( '/candidates', [FrontendController::class, 'candidates'] )
    ->name( 'candidates' );

Route::get( '/candidates/recommended', [FrontendController::class, 'recommended'])
    ->name( 'recommended' );
Route::get( '/candidates/applicant', [FrontendController::class, 'applicant'])
    ->name( 'applicant' );

Route::get( '/candidates/shortlist', [FrontendController::class, 'shortlist'])
    ->name( 'shortlist' );

Route::post('/shortlist-candidate', [FrontendController::class, 'shortlist_candidate'])->name('shortlist_candidate');
Route::post('/employer-action-candidate', [FrontendController::class, 'action'])->name('employer_action_candidate');

Route::get( '/jobs', [FrontendController::class, 'jobs'] )
    ->name( 'jobs' );

Route::get( '/job_details/{job}', [FrontendController::class, 'job_details'] )
    ->name( 'job_details' );

Route::get( '/jobs/recommended', [FrontendController::class, 'recommended_jobs'])
    ->name( 'recommended_jobs' );

Route::get( '/jobs/applicant', [FrontendController::class, 'applicant_jobs'])
    ->name( 'applicant_jobs' );

Route::get( '/agency&services', [FrontendController::class, 'agencies_services'])
    ->name( 'agencies_services' );

Route::get( '/training', [FrontendController::class, 'training_page'])
    ->name( 'training_page' );

Route::get( '/training_details/{user}', [FrontendController::class, 'training_details'])
    ->name( 'training_detail' );

//Route::view('/training_details/{training}','/frontend/pages/training/training_details')->name('training_detail');

Route::get( '/association', [FrontendController::class, 'association_page'])
    ->name( 'association_page' );

Route::get( '/association_details/{user}', [FrontendController::class, 'association_details'])
    ->name( 'association_details' );

//Route::view('/association_details','/frontend/pages/association/association_details');

Route::get( '/tips', [FrontendController::class, 'tips'])
    ->name( 'tips' );

Route::get('/fetch-more-tips', [FrontendController::class, 'fetchMoreTips']);

Route::get( '/tips_details/{tips}', [FrontendController::class, 'tips_details'])
    ->name( 'tips_details' );


Route::get( '/news', [FrontendController::class, 'news'])
    ->name( 'news' );

Route::get( '/news_details/{news}', [FrontendController::class, 'news_details'])
    ->name( 'news_details' );

Route::get( '/agency_details/{user}', [FrontendController::class, 'agency_details'])
    ->name( 'agency_details' );

Route::get( '/more_detail_service/{service}', [FrontendController::class, 'more_detail_service'])
    ->name( 'more_detail_service' );

Route::post( 'submit_agency_review', [FrontendController::class, 'submit_agency_review'])
    ->name( 'submit_agency_review' );



Route::get( '/about', [FrontendController::class, 'about'])
    ->name( 'about' );

Route::get( '/public_holiday', [FrontendController::class, 'public_holiday'])
    ->name( 'public_holiday' );

Route::get( '/pricing', [FrontendController::class, 'pricing'])
    ->name( 'pricing' );

Route::get('/get-manual-price', [FrontendController::class, 'getManualPrice']);

Route::get('/event', [FrontendController::class, 'event'])->name('event');

Route::get('/event_details/{event}', [FrontendController::class, 'event_details'])->name('event_details');

Route::get('/happy_helpers', [FrontendController::class, 'happy_helpers'])->name('happy_helpers');

Route::get('/faqs', [FrontendController::class, 'faqs'])->name('faqs');

Route::get('/terms', [FrontendController::class, 'terms'])->name('terms');

Route::get('/privacy', [FrontendController::class, 'privacy'])->name('privacy');

Route::get('/contact', [FrontendController::class, 'contact'])->name('contact');

Route::get('/happy_employers', [FrontendController::class, 'happy_employers'])->name('happy_employers');

//Route::view('/partner','/frontend/pages/partner/find_partner');
//Route::view('/partner_details','/frontend/pages/partner/partner_details');


Route::get( '/partner', [FrontendController::class, 'partner_page'])
    ->name( 'partner_page' );

Route::get( '/partner_details/{user}', [FrontendController::class, 'partner_details'])
    ->name( 'partner_details' );


Route::view('/dashboard','/dashboard/pages/dashboard');
Route::view('/post_resume','/dashboard/pages/post_resume');
Route::view('/profile','/dashboard/pages/profile');
Route::view('/employment_biodata','/dashboard/pages/employment_biodata');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


