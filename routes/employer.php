<?php

use App\Http\Controllers\Employer\DashboardController;
use App\Http\Controllers\Employer\JobController;
use App\Http\Controllers\Employer\MessageController;
use App\Http\Controllers\Employer\PaymentController;
use App\Http\Controllers\Employer\ProfileController;


Route::get('/dashboard', [DashboardController::class, 'index'])->name('employer_dashboard');

Route::get('/profile', [ProfileController::class, 'index'])->name('employer_profile');

Route::post( 'profile/update', [ProfileController::class, 'update'] )->name( 'employerProfileUpdate' );
Route::post('account_update_password', [ProfileController::class, 'account_update_password'])->name('account_update_password');

Route::get('post-job', [JobController::class, 'create'])->name('post_job');
Route::post('save-job', [JobController::class,'store'])->name('save-job');
Route::get('manage-jobs', [JobController::class, 'manage_jobs'])->name('manage_jobs');
Route::get('upgrade-package/{job_id}', [JobController::class, 'upgrade_package'])->name('upgrade_package');
Route::get( '/manage-jobs/edit/{job}', [JobController::class, 'edit'] )->name( 'job_edit' );
Route::post( '/manage-jobs/update/{job}', [JobController::class, 'update'] )->name( 'job_update' );
Route::get( '/manage-jobs/delete/{job}', [JobController::class, 'delete'] )->name( 'job_delete' );

//AJAX ROUTE -SAAUD
Route::get('/jobs/fetch', [JobController::class, 'fetchJobs'])->name('jobs.fetch');
Route::get('/job_status/{job}', [JobController::class, 'job_status'])->name('job_status');

Route::post('validate-job-step-1', [JobController::class, 'validateJobStepOne'])->name('validateJobStepOne');
Route::post('validate-job-step-2', [JobController::class, 'validateJobStepTwo'])->name('validateJobStepTwo');
Route::post('validate-job-step-3', [JobController::class, 'validateJobStepThree'])->name('validateJobStepThree');


Route::get('pay/stripe/{job}', [PaymentController::class, 'pay_stripe'])->name('pay_stripe');
Route::post('pay/stripe/payment/{job}', [PaymentController::class, 'pay_stripe_payment'])->name('pay_stripe_payment');

Route::get('support', [MessageController::class, 'support'])->name('employer_support');
Route::post('send_support_msg', [MessageController::class, 'send_support_msg'])->name('employer_send_support_msg');
Route::post('get_received_messages', [MessageController::class, 'get_received_messages'])->name('employer_received_messages');

Route::get('messages', [MessageController::class, 'messages'])->name('employer_messages');
Route::get('message/{receiver}', [MessageController::class, 'chat_index'])->name('employer_chat_index');
Route::post('send/message', [MessageController::class, 'send_message'])->name('employer_send_message');
Route::post('unread_messages_count', [MessageController::class, 'unread_messages_count'])->name('employer_unread_messages_count');
Route::post('received_messages', [MessageController::class, 'received_messages'])->name('employer_received_messages');
Route::get('mark-as-read/{receiver}', [MessageController::class, 'mark_as_read'])->name('employer_message_mark_as_read');



